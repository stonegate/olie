USE [msdb]
GO

/****** Object:  Job [Saturday Backup and Maintenance]    Script Date: 01/26/2015 13:41:14 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 01/26/2015 13:41:14 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Saturday Backup and Maintenance', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'STONEGATEMTG\Svcdevportaldb01', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Integrity Check]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Integrity Check', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC sp_msforeachdb ''DBCC CHECKDB(''''?'''')''', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Rebuild Indexes]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Rebuild Indexes', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @Database VARCHAR(255)   
DECLARE @Table VARCHAR(255)  
DECLARE @cmd NVARCHAR(500)  
DECLARE @fillfactor INT 

SET @fillfactor = 90 
SET QUOTED_IDENTIFIER ON

DECLARE DatabaseCursor CURSOR FOR  
SELECT name FROM master.dbo.sysdatabases   
WHERE name IN (''master'',''msdb'',''model'',''ABD-Management'',''HomeLendingExperts'',''LiveE3ProWH'',''StonegateMtgLive'')   
ORDER BY 1  

OPEN DatabaseCursor  

FETCH NEXT FROM DatabaseCursor INTO @Database  
WHILE @@FETCH_STATUS = 0  
BEGIN  

   SET @cmd = ''DECLARE TableCursor CURSOR FOR SELECT ''''['''' + table_catalog + ''''].['''' + table_schema + ''''].['''' + 
  table_name + '''']'''' as tableName FROM ['' + @Database + ''].INFORMATION_SCHEMA.TABLES 
  WHERE table_type = ''''BASE TABLE''''''   

   -- create table cursor  
   EXEC (@cmd)  
   OPEN TableCursor   

   FETCH NEXT FROM TableCursor INTO @Table   
   WHILE @@FETCH_STATUS = 0   
   BEGIN   

       IF (@@MICROSOFTVERSION / POWER(2, 24) >= 9)
       BEGIN
           -- SQL 2005 or higher command 
           SET @cmd = ''ALTER INDEX ALL ON '' + @Table + '' REBUILD WITH (FILLFACTOR = '' + CONVERT(VARCHAR(3),@fillfactor) + '')'' 
           EXEC (@cmd) 
       END
       ELSE
       BEGIN
          -- SQL 2000 command 
          DBCC DBREINDEX(@Table,'' '',@fillfactor)  
       END

       FETCH NEXT FROM TableCursor INTO @Table   
   END   

   CLOSE TableCursor   
   DEALLOCATE TableCursor  

   FETCH NEXT FROM DatabaseCursor INTO @Database  
END  
CLOSE DatabaseCursor   
DEALLOCATE DatabaseCursor', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Backup Database]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Backup Database', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.proc_BackupAnyDB
@DBName = ''master'', 
@Location = ''D:\SQLBackup\master\'', 
@FileName = ''master.bak'', 
@Name = ''master Backup'', --Name of the backup run
@Description = ''Weekly Backup'',
@BackupType = ''F'', --F = Full, D = Diff, T = Tran Log
@Reset = 1 -- 1 = Yes, 0 = No

GO

EXEC dbo.proc_BackupAnyDB
@DBName = ''model'', 
@Location = ''D:\SQLBackup\model\'', 
@FileName = ''model.bak'', 
@Name = ''model Backup'', --Name of the backup run
@Description = ''Weekly Backup'',
@BackupType = ''F'', --F = Full, D = Diff, T = Tran Log
@Reset = 1 -- 1 = Yes, 0 = No

GO

EXEC dbo.proc_BackupAnyDB
@DBName = ''msdb'', 
@Location = ''D:\SQLBackup\msdb\'', 
@FileName = ''msdb.bak'', 
@Name = ''msdb Backup'', --Name of the backup run
@Description = ''Weekly Backup'',
@BackupType = ''F'', --F = Full, D = Diff, T = Tran Log
@Reset = 1 -- 1 = Yes, 0 = No

GO', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Reset Backup Date]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Reset Backup Date', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'Declare @Date  varchar(100);
Declare @Sql    varchar(MAX);
Declare @TranSql    varchar(MAX);
declare @PathToObject varchar(100)
declare @OldFullFileName varchar(100), @NewFullFileName varchar(100)
declare @OldTranFileName varchar(100), @NewTranFileName varchar(100)
declare @DeleteFullFile varchar(100), @DeleteTranFile varchar(100)
declare @DeleteDate varchar(100)

set @PathToObject = ''D:\SQLBackup\master''
set @OldFullFileName = ''master.bak''

SELECT @Date= CONVERT(VARCHAR(10), GETDATE(), 112);
SELECT @DeleteDate= CONVERT(VARCHAR(10), GETDATE()-1, 112);

set @NewFullFileName = @Date + @OldFullFileName 

exec dbo.proc_ReplaceFileOrDirNames @pathToObject, @OldFullFileName, @NewFullFileName
Go

Declare @Date  varchar(100);
Declare @Sql    varchar(MAX);
Declare @TranSql    varchar(MAX);
declare @PathToObject varchar(100)
declare @OldFullFileName varchar(100), @NewFullFileName varchar(100)
declare @OldTranFileName varchar(100), @NewTranFileName varchar(100)
declare @DeleteFullFile varchar(100), @DeleteTranFile varchar(100)
declare @DeleteDate varchar(100)

set @PathToObject = ''D:\SQLBackup\model''
set @OldFullFileName = ''model.bak''

SELECT @Date= CONVERT(VARCHAR(10), GETDATE(), 112);
SELECT @DeleteDate= CONVERT(VARCHAR(10), GETDATE()-1, 112);

set @NewFullFileName = @Date + @OldFullFileName 

exec dbo.proc_ReplaceFileOrDirNames @pathToObject, @OldFullFileName, @NewFullFileName
Go

Declare @Date  varchar(100);
Declare @Sql    varchar(MAX);
Declare @TranSql    varchar(MAX);
declare @PathToObject varchar(100)
declare @OldFullFileName varchar(100), @NewFullFileName varchar(100)
declare @OldTranFileName varchar(100), @NewTranFileName varchar(100)
declare @DeleteFullFile varchar(100), @DeleteTranFile varchar(100)
declare @DeleteDate varchar(100)

set @PathToObject = ''D:\SQLBackup\msdb''
set @OldFullFileName = ''msdb.bak''

SELECT @Date= CONVERT(VARCHAR(10), GETDATE(), 112);
SELECT @DeleteDate= CONVERT(VARCHAR(10), GETDATE()-1, 112);

set @NewFullFileName = @Date + @OldFullFileName 

exec dbo.proc_ReplaceFileOrDirNames @pathToObject, @OldFullFileName, @NewFullFileName
Go', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Cleanup Backup Files]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Cleanup Backup Files', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'PowerShell', 
		@command=N'Dir D:\SQLBackup\master\* -recurse | where {$_.LastWriteTime �lt (get-date).AddDays(-35) } | % {del $_.FullName};
Dir D:\SQLBackup\model\* -recurse | where {$_.LastWriteTime �lt (get-date).AddDays(-35) } | % {del $_.FullName};
Dir D:\SQLBackup\msdb\* -recurse | where {$_.LastWriteTime �lt (get-date).AddDays(-35) } | % {del $_.FullName}', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Cleanup Backup History]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Cleanup Backup History', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE msdb
 GO
 DECLARE @DaysToKeepHistory DATETIME
 SET @DaysToKeepHistory = CONVERT(VARCHAR(10), DATEADD(dd, -7, GETDATE()), 101)
 EXEC sp_delete_backuphistory @DaysToKeepHistory
 GO', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Cleanup SQL Agent History]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Cleanup SQL Agent History', 
		@step_id=7, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE msdb
 GO
 DECLARE @DaysToKeepHistory DATETIME
 SET @DaysToKeepHistory = CONVERT(VARCHAR(10), DATEADD(dd, -120, GETDATE()), 101)
 EXEC sp_purge_jobhistory @job_name = N''Saturday Backup and Maintenance'', @oldest_date = @DaysToKeepHistory
 GO', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Saturday Backup and Maintenance', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=64, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20140122, 
		@active_end_date=99991231, 
		@active_start_time=210000, 
		@active_end_time=235959, 
		@schedule_uid=N'98363a8e-3f5d-44b5-ba6d-fa69f3cff2cf'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


