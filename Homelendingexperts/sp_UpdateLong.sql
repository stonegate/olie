USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateLong]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateLong]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateLong]
	-- Add the parameters for the stored procedure here
(
				@loanapplicationid int,
				@HometypeID int,
                @PropertyTypeID int,
                @LoanAmount money,
                @LoanReasonID int,
                @StateID int,
                @CurrentPropertyValue money,
                @YearID int,
                @CurrentMortageBal money,
                @MonthlyPayment money,
                @SecondMortage bit,
                @SecondMortageBal money,
                @ExistingPuchasePrice money,
                @vAddressLine2 varchar(500),
                @PurchasePrise money,
                @AppFName nvarchar(100),
                @AppLName nvarchar(100),
                @AppEmail nvarchar(100),
               
                @vAddressLine1 varchar(500),
                @vCity varchar(100),
                @AppStateID int,
                @AppZip nvarchar(100),
                @AppPrimaryPhone nvarchar(100),
                @AppSecondaryPhone nvarchar(100),
                @ContactTimeID int
				
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
update tblLoanApplication set

                HometypeID=@HometypeID,
                PropertyTypeID=@PropertyTypeID,
                LoanAmount=@LoanAmount,
                LoanReasonID=@LoanReasonID,
                StateID=@StateID,
                CurrentPropertyValue=@CurrentPropertyValue,
                YearID=@YearID,
                CurrentMortageBal=@CurrentMortageBal,
                MonthlyPayment=@MonthlyPayment,
                SecondMortage=@SecondMortage,
                SecondMortageBal=@SecondMortageBal,
                ExistingPuchasePrice=@ExistingPuchasePrice,
                vAddressLine2=@vAddressLine2,
                PurchasePrise=@PurchasePrise,
                AppFName=@AppFName,
                AppLName=@AppLName,
                AppEmail=@AppEmail,
           
                vAddressLine1=@vAddressLine1,
                vCity=@vCity,
                AppStateID=@AppStateID,
                AppZip=@AppZip,
                AppPrimaryPhone=@AppPrimaryPhone,
                AppSecondaryPhone=@AppSecondaryPhone,
                ContactTimeID=@ContactTimeID

		where	loanapplicationid=@loanapplicationid 


END

GO

