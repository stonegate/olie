USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanApplicationByAppID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanApplicationByAppID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanApplicationByAppID]
	-- Add the parameters for the stored procedure here

@LoanApplicationID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	
                            select *, tblPropertyType.Name AS PropertyTypeName, tblContactTime.ContactTime,    tblLoanApplication.ContactTimeID, tblHomeType.Hometype, tblCreditScore.CreditScoreValue, tblStateNylex.StateName,    tblLoanReason.LoanReason, tblProductType.name AS ProductTypeName, tblStateNylex_1.StateName AS AppStateName, tblYear.Year,    tblDownPaymentSource.SourceOfdownPayment from tblLoanApplication 
                LEFT JOIN   tblPropertyType ON tblLoanApplication.PropertyTypeID = tblPropertyType.PropertyTypeID 
                LEFT JOIN   tblContactTime ON tblLoanApplication.ContactTimeID = tblContactTime.ContactTimeID   
                LEFT JOIN   tblHomeType ON tblLoanApplication.HometypeID = tblHomeType.HometypeID 
                LEFT JOIN   tblCreditScore ON tblLoanApplication.CreditScoreID = tblCreditScore.CreditScoreID   
                LEFT JOIN   tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID   
                LEFT JOIN   tblLoanReason ON tblLoanApplication.LoanReasonID = tblLoanReason.LoanReasonID 
                LEFT JOIN   tblProductType ON tblLoanApplication.Producttypeid = tblProductType.producttypeid   
                LEFT JOIN   tblStateNylex AS tblStateNylex_1 ON tblLoanApplication.AppStateID = tblStateNylex_1.StateID    
                LEFT JOIN   tblYear ON tblLoanApplication.YearID = tblYear.YearID  
                LEFT OUTER JOIN   tblDownPaymentSource ON tblLoanApplication.DownPaymentSourceID = tblDownPaymentSource.DownPaymentSourceID 
                 where loanApplicationID=@loanApplicationID 

;

                select BI.*, LU.Description as HowLong ,LU1.Description as CoHowLong, ST.StateName + ' - ' + ST.Abbreviature as CoBowwowerState 
                from tblBorrowerinfo BI  
                left join tbllookups LU on BI.TimePeriod = LU.lookupid   
                left join tbllookups LU1 on BI.[Co-BorrowerTimePeriod] = LU1.lookupid 
                left join tblStateNylex ST on BI.[Co-BorrowerState]=ST.StateID
                where LU.Lookupname='TP' and LoanApplicationID = @LoanApplicationID
END

GO

