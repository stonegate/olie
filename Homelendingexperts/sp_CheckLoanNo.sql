USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckLoanNo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckLoanNo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_CheckLoanNo]
(
@loan_no varchar (max)
)
AS
BEGIN
select * from loandat where lt_loan_stats  IN ('Locked','Application Received','Submitted to Underwriting','In Underwriting','Loan Suspended','Approved') and loan_no = @loan_no 



END


GO

