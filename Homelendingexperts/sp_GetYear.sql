USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetYear]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetYear]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetYear]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	
                             DECLARE @max int 
                             set @max = DATEPART(year, getdate()) 
                             CREATE TABLE #temp (Year int) 
                             while @max >= 1909 
                             BEGIN 
                             insert #temp(Year) values(@max) 
                              set @max = @max - 1 
                              END 
                              SELECT Year from #temp 
                              drop table #temp 
END

GO

