USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Subscribe]') AND type in (N'U'))
DROP TABLE [dbo].[Subscribe]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subscribe](	  [ID] INT NOT NULL IDENTITY(1,1)	, [Name] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EMail] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Comments] NVARCHAR(2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL DEFAULT((1))	, [CreatedDate] DATETIME NOT NULL DEFAULT(getdate())	, [UpdatedDate] DATETIME NULL)USE [HomeLendingExperts]
GO

