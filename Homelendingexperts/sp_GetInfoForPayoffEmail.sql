USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetInfoForPayoffEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetInfoForPayoffEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetInfoForPayoffEmail]
(
@userid varchar (max),
@dbname varchar (max)
)
AS
BEGIN
declare @a varchar(max)
set @a='select ufirstname,ulastname,convert(varchar, payoffdate, 101) as payoffdate,description from tblpayoffstatement as payoff
join tblUsers as users on payoff.userid = users.userid 
left join' + @dbname + 'PayoffReason on payoff.payoffreasonid = payoffreason.code
 where users.userid ='+ @userid+''

exec(@a)
END

GO

