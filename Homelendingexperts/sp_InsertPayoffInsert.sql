USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertPayoffInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertPayoffInsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertPayoffInsert]
(
@userid varchar (max),
@payoffdate varchar (max),
@payoffreasonid varchar (max),
@isreadytocontact varchar (max)
)
AS
BEGIN
insert into tblpayoffstatement(userid,payoffdate,payoffreasonid,isreadytocontact)
values (@userid,@payoffdate,@payoffreasonid,@isreadytocontact)
END

GO

