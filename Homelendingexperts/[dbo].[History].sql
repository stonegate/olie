USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[History]') AND type in (N'U'))
DROP TABLE [dbo].[History]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[History](	  [Id] INT NOT NULL IDENTITY(1,1)	, [LoanId] INT NOT NULL	, [FName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [LName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [Status] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Network] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PayDate] DATETIME NULL	, [Amount] MONEY NULL	, [Paid] NCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DateEntered] DATETIME NULL	, [Notes] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ModifiedBy] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ModyfiedDate] DATETIME NULL DEFAULT(getdate())	, [lastupdateby] INT NULL	, [NetworkName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [NetworkPhone] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_History] PRIMARY KEY ([Id] ASC))USE [HomeLendingExperts]
GO

