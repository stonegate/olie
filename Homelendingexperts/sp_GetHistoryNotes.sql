USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHistoryNotes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHistoryNotes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetHistoryNotes]
	-- Add the parameters for the stored procedure here
	@loanid varchar(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT modyfieddate,notes,isnull(lastupdateby,0) lastupdateby FROM History  where loanid=@loanid order by id desc
    -- Insert statements for procedure here
	
END

GO

