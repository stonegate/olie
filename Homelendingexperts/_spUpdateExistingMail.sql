USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_spUpdateExistingMail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[_spUpdateExistingMail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[_spUpdateExistingMail]
	-- Add the parameters for the stored procedure here
	(
	@Name nvarchar(100),
	@Email nvarchar(100),

	@Comments nvarchar(1000)	
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Subscribe 
set IsActive = 1,Name=@Name,Comments=@Comments, UpdatedDate=GetDate()Where EMail = @Email 
               
              
                 
END

GO

