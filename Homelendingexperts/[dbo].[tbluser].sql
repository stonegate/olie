USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbluser]') AND type in (N'U'))
DROP TABLE [dbo].[tbluser]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbluser](	  [Uid] INT NOT NULL IDENTITY(1,1)	, [LoginName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LoginPass] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [UserName] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsAdmin] BIT NULL	, [IsActive] BIT NULL	, [maxloginattempt] INT NULL	, [lastlogintime] DATETIME NULL	, CONSTRAINT [PK_tbluser] PRIMARY KEY ([Uid] ASC))USE [HomeLendingExperts]
GO

