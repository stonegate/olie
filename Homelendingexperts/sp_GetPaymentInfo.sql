USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPaymentInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPaymentInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetPaymentInfo]
(
@loanid varchar(Max),
@TransactionDate varchar(Max)
)
AS
BEGIN

select  TransactionDate, TransactionAmt,description,EndPrincipalBal,InterestRate,
EscrowAmt,LateChargeAmt,OtherFundsAmt from history join TransactionCode as transcode on history.TransactionCode = transcode.Code
where loanid = @loanid and TransactionAmt > 0 
and (TransactionDate < @TransactionDate and TransactionDate > dateadd(year,-1,@TransactionDate))
order by history.TransactionDate desc
     
END

GO

