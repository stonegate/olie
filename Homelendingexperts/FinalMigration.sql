USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbltestimonials]') AND type in (N'U'))
DROP TABLE [dbo].[tbltestimonials]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbltestimonials](	  [ID] INT NOT NULL IDENTITY(1,1)	, [UName] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Description] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedDate] DATETIME NULL	, CONSTRAINT [PK_tbltestimonials] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblloanapp]') AND type in (N'U'))
DROP TABLE [dbo].[tblloanapp]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblloanapp](	  [LoanId] INT NOT NULL IDENTITY(1,1)	, [Fname] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Mname] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LName] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Email] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CEmail] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DOBM] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DOBDT] INT NULL	, [DOBYR] INT NULL	, [SSN] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Phone] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Suffix] VARCHAR(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [COFNAME] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [COMNAME] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [COLNAME] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [COEMAIL] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [COCEmail] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CODOBM] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CODOBDT] INT NULL	, [CODOBYR] INT NULL	, [COSSN] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [COPHONE] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [STADDR] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CITY] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MState] INT NULL	, [ZipCode] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RSTATUS] VARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RentAmt] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LMHOLDER] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [STADDR2] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CITY2] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [STATE2] INT NULL	, [COSuffix] VARCHAR(3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ZipCode2] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RLengthY2] INT NULL	, [RLengthM2] INT NULL	, [RSTATUS2] VARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RentAmt2] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LMHOLDER2] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AEMPL] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AOCCU] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AEMPPHONE] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AGMI] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ALEMY] INT NULL	, [ALEMM] INT NULL	, [AEMPL2] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AOCCU2] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AEMPPHONE2] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AGMI2] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ALEMY2] INT NULL	, [ALEMM2] INT NULL	, [CAEMPL] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CAOCCU] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CAEMPPHONE] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CAGMI] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CALEMY] INT NULL	, [CALEMM] INT NULL	, [CAEMPL2] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CAOCCU2] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CAEMPPHONE2] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CAGMI2] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CALEMY2] INT NULL	, [CALEMM2] INT NULL	, [CurrLoanMake] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CurrLoanYear] VARCHAR(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CurrLoanBalance] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CurrLoanMiles] INT NULL	, [OtherComments] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DigiSig] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SigDate] VARCHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsAckno] BIT NULL	, [DateEntered] DATETIME NULL	, [RLengthM] INT NULL	, [RlengthY] INT NULL	, [Status] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Network] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PayDate] DATETIME NULL	, [Amount] MONEY NULL	, [Paid] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [lastupdateby] INT NULL	, [NetworkName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [NetworkPhone] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblloanapp] PRIMARY KEY ([LoanId] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Status]') AND type in (N'U'))
DROP TABLE [dbo].[Status]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](	  [Id] INT NOT NULL IDENTITY(1,1)	, [Status] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[History]') AND type in (N'U'))
DROP TABLE [dbo].[History]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[History](	  [Id] INT NOT NULL IDENTITY(1,1)	, [LoanId] INT NOT NULL	, [FName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [LName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [Status] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Network] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PayDate] DATETIME NULL	, [Amount] MONEY NULL	, [Paid] NCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DateEntered] DATETIME NULL	, [Notes] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ModifiedBy] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ModyfiedDate] DATETIME NULL DEFAULT(getdate())	, [lastupdateby] INT NULL	, [NetworkName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [NetworkPhone] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_History] PRIMARY KEY ([Id] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbluser]') AND type in (N'U'))
DROP TABLE [dbo].[tbluser]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbluser](	  [Uid] INT NOT NULL IDENTITY(1,1)	, [LoginName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LoginPass] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [UserName] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsAdmin] BIT NULL	, [IsActive] BIT NULL	, [maxloginattempt] INT NULL	, [lastlogintime] DATETIME NULL	, CONSTRAINT [PK_tbluser] PRIMARY KEY ([Uid] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Subscribe]') AND type in (N'U'))
DROP TABLE [dbo].[Subscribe]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subscribe](	  [ID] INT NOT NULL IDENTITY(1,1)	, [Name] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EMail] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Comments] NVARCHAR(2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL DEFAULT((1))	, [CreatedDate] DATETIME NOT NULL DEFAULT(getdate())	, [UpdatedDate] DATETIME NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPropertyType]') AND type in (N'U'))
DROP TABLE [dbo].[tblPropertyType]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPropertyType](	  [ID] INT NOT NULL IDENTITY(1,1)	, [PropertyTypeID] INT NOT NULL	, [Name] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblStateNylex]') AND type in (N'U'))
DROP TABLE [dbo].[tblStateNylex]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblStateNylex](	  [ID] INT NOT NULL IDENTITY(1,1)	, [StateID] INT NULL	, [Abbreviature] NVARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [StateName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [StateOrder] INT NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblProductType]') AND type in (N'U'))
DROP TABLE [dbo].[tblProductType]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblProductType](	  [ID] INT NOT NULL IDENTITY(1,1)	, [producttypeid] INT NULL	, [name] NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [term] INT NULL	, [isarm] INT NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCreditScore]') AND type in (N'U'))
DROP TABLE [dbo].[tblCreditScore]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCreditScore](	  [ID] INT NOT NULL IDENTITY(1,1)	, [CreditScoreID] INT NULL	, [CreditScoreValue] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreditOrder] INT NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanReason]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanReason]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanReason](	  [LoanReasonID] INT NOT NULL IDENTITY(1,1)	, [LoanReason] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblHomeType]') AND type in (N'U'))
DROP TABLE [dbo].[tblHomeType]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblHomeType](	  [HometypeID] INT NOT NULL IDENTITY(1,1)	, [Hometype] NVARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDownPaymentSource]') AND type in (N'U'))
DROP TABLE [dbo].[tblDownPaymentSource]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDownPaymentSource](	  [DownPaymentSourceID] INT NOT NULL IDENTITY(1,1)	, [SourceOfdownPayment] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblContactTime]') AND type in (N'U'))
DROP TABLE [dbo].[tblContactTime]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblContactTime](	  [ContactTimeID] INT NOT NULL IDENTITY(1,1)	, [ContactTime] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblYear]') AND type in (N'U'))
DROP TABLE [dbo].[tblYear]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblYear](	  [YearID] INT NOT NULL IDENTITY(1,1)	, [Year] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbllookups]') AND type in (N'U'))
DROP TABLE [dbo].[tbllookups]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbllookups](	  [lookupid] BIGINT NOT NULL IDENTITY(1,1)	, [Lookupname] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Description] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tbllookups] PRIMARY KEY ([lookupid] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBorrowerInfo]') AND type in (N'U'))
DROP TABLE [dbo].[tblBorrowerInfo]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBorrowerInfo](	  [BorrowerID] INT NOT NULL IDENTITY(1,1)	, [LoanApplicationID] INT NULL	, [PlaceEmp] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CurrentAnualIncome] MONEY NULL	, [PhoneNum] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [TimePeriod] INT NULL	, [PriorEmp] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PriorAnualIncome] MONEY NULL	, [IsSelfEmp] BIT NULL	, [IsCredit] BIT NULL	, [CreditSSNNo] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreditDoBirth] DATETIME NULL	, [IsAuthorize] BIT NULL	, [IsCoapplicant] BIT NULL	, [CoAppFName] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CoAppLName] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerAdd1] NVARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerAdd2] NVARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerCity] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerState] INT NULL	, [Co-BorrowerZip] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerHomePhone] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerCellorWorkPhone] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerEmail] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerPlaceEmp] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerPostion] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerCurrentAnual] MONEY NULL	, [Co-BorrowerPhone] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerTimePeriod] INT NULL	, [Co-BorrowerPriorPlaceEmp] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerPriorAnual] MONEY NULL	, [Co-BorrowerIsSelfEmp] BIT NULL	, [Co-BorrowerSelfDoBirth] DATETIME NULL	, [Co-BorrowerSelfSSN] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsWorkingWithRealtor] BIT NULL	, [RealtorName] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RealtorPhone] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsBorrowersBankruptcy] BIT NULL	, [Bankruptcywhen] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsBorrowerResponsiblechild] BIT NULL	, [childsupportqty] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblBorrowerInfo] PRIMARY KEY ([BorrowerID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanApplication]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanApplication]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanApplication](	  [LoanApplicationID] INT NOT NULL IDENTITY(1,1)	, [LoanReasonID] INT NULL	, [HometypeID] INT NULL	, [PropertyTypeID] INT NULL	, [Producttypeid] INT NULL	, [StateID] INT NULL	, [CreditScoreID] INT NULL	, [AnnulIncomeBorrower] MONEY NULL	, [AssetBorrower] MONEY NULL	, [RetirementAssetsBorrower] MONEY NULL	, [PropertyLocated] BIT NULL	, [HomeOwned] BIT NULL	, [PurchasePrise] MONEY NULL	, [DownPaymentAmt] MONEY NULL	, [PartOfTotalAssets] BIT NULL	, [DownPaymentSourceID] INT NULL	, [HaveRealtor] BIT NULL	, [RealtorContactName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RealtorPhone] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RealtorEmail] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppFName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppLName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppStateID] INT NULL	, [AppZip] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppPrimaryPhone] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [AppSecondaryPhone] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppEmail] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ContactTimeID] INT NULL	, [CurrentPropertyValue] MONEY NULL	, [PropertyPurchaseMonth] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [YearID] INT NULL	, [ExistingPuchasePrice] MONEY NULL	, [CurrentMortageBal] MONEY NULL	, [MonthlyPayment] MONEY NULL	, [SecondMortage] BIT NULL	, [SecondMortageBal] MONEY NULL	, [DateEntered] DATETIME NULL DEFAULT(getdate())	, [ModifiedBy] INT NULL	, [ModifiedDate] DATETIME NULL	, [UserAppearUrl] NVARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [vCity] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [bSignAgg] BIT NULL	, [dSchClosingDate] DATETIME NULL	, [vEstRenovationAmt] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [vDownPayAmount] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [vAddressLine1] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [vAddressLine2] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsFullApp] BIT NULL DEFAULT((0))	, [ApplyType] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsLead360] BIT NULL DEFAULT((0))	, [CompIP] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MAName] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AssignedLO] VARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsWorkingWithRealtor] BIT NULL	, [PropertyRealtorCity] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsCashback] BIT NULL	, [RealtorName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RealtorPhoneNo] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LoanAmount] MONEY NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblstate]') AND type in (N'U'))
DROP TABLE [dbo].[tblstate]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblstate](	  [SId] INT NOT NULL IDENTITY(1,1)	, [StateAbbr] VARCHAR(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [StateName] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblstate] PRIMARY KEY ([SId] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_spUpdateExistingMail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[_spUpdateExistingMail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[_spUpdateExistingMail]
	-- Add the parameters for the stored procedure here
	(
	@Name nvarchar(100),
	@Email nvarchar(100),

	@Comments nvarchar(1000)	
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Subscribe 
set IsActive = 1,Name=@Name,Comments=@Comments, UpdatedDate=GetDate()Where EMail = @Email 
               
              
                 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertFinalApprove]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertFinalApprove]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertFinalApprove]
	-- Add the parameters for the stored procedure here
	(
@LoanApplicationID int, 
@PlaceEmp int, 
@CurrentAnualIncome money, 
@PhoneNum nvarchar(50), 
@TimePeriod int, 
@PriorEmp nvarchar(50), 
@PriorAnualIncome money, 
@IsSelfEmp bit,  
@IsCredit bit, 
@CreditSSNNo nvarchar(50),
@CreditDoBirth datetime,
@IsAuthorize bit, 
@IsCoapplicant bit, 
@CoAppFName nvarchar(50),
@CoAppLName nvarchar(50), 
@Co_BorrowerAdd1 nvarchar(200), 
@Co_BorrowerAdd2 nvarchar(200), 
@Co_BorrowerCity nvarchar(50), 
@Co_BorrowerState int, 
@Co_BorrowerZip nvarchar(50), 
@Co_BorrowerHomePhone nvarchar(50), 
@Co_BorrowerCellorWorkPhone nvarchar(50), 
@Co_BorrowerEmail nvarchar(50), 
@Co_BorrowerPlaceEmp nvarchar(50), 
@Co_BorrowerPostion nvarchar(50), 
@Co_BorrowerCurrentAnual money, 
@Co_BorrowerPhone nvarchar(50), 
@Co_BorrowerTimePeriod int, 
@Co_BorrowerPriorPlaceEmp money,
@Co_BorrowerPriorAnual money, 
@Co_BorrowerIsSelfEmp bit, 
@Co_BorrowerSelfDoBirth datetime, 
@Co_BorrowerSelfSSN nvarchar(50), 
@IsWorkingWithRealtor bit, 
@RealtorName nvarchar(50), 
@RealtorPhone nvarchar(50), 
@IsBorrowersBankruptcy bit,
@Bankruptcywhen nvarchar(50),
@IsBorrowerResponsiblechild bit,
@childsupportqty nvarchar(50)

	)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblBorrowerInfo
(
LoanApplicationID, 
PlaceEmp, 
CurrentAnualIncome, 
PhoneNum, 
TimePeriod, 
PriorEmp, 
PriorAnualIncome, 
IsSelfEmp,  
IsCredit, 
CreditSSNNo,
CreditDoBirth,
IsAuthorize, 
IsCoapplicant, 
CoAppFName,
CoAppLName, 
[Co-BorrowerAdd1], 
[Co-BorrowerAdd2], 
[Co-BorrowerCity], 
[Co-BorrowerState], 
[Co-BorrowerZip], 
[Co-BorrowerHomePhone], 
[Co-BorrowerCellorWorkPhone], 
[Co-BorrowerEmail], 
[Co-BorrowerPlaceEmp], 
[Co-BorrowerPostion], 
[Co-BorrowerCurrentAnual], 
[Co-BorrowerPhone], 
[Co-BorrowerTimePeriod], 
[Co-BorrowerPriorPlaceEmp],
[Co-BorrowerPriorAnual], 
[Co-BorrowerIsSelfEmp], 
[Co-BorrowerSelfDoBirth], 
[Co-BorrowerSelfSSN], 
IsWorkingWithRealtor, 
RealtorName, 
RealtorPhone, 
IsBorrowersBankruptcy,
Bankruptcywhen,
IsBorrowerResponsiblechild,
childsupportqty
) 
Values
(
@LoanApplicationID, 
@PlaceEmp, 
@CurrentAnualIncome, 
@PhoneNum, 
@TimePeriod, 
@PriorEmp, 
@PriorAnualIncome, 
@IsSelfEmp,  
@IsCredit, 
@CreditSSNNo,
@CreditDoBirth,
@IsAuthorize, 
@IsCoapplicant, 
@CoAppFName,
@CoAppLName, 
@Co_BorrowerAdd1, 
@Co_BorrowerAdd2, 
@Co_BorrowerCity, 
@Co_BorrowerState, 
@Co_BorrowerZip, 
@Co_BorrowerHomePhone, 
@Co_BorrowerCellorWorkPhone, 
@Co_BorrowerEmail, 
@Co_BorrowerPlaceEmp, 
@Co_BorrowerPostion, 
@Co_BorrowerCurrentAnual, 
@Co_BorrowerPhone, 
@Co_BorrowerTimePeriod, 
@Co_BorrowerPriorPlaceEmp,
@Co_BorrowerPriorAnual, 
@Co_BorrowerIsSelfEmp, 
@Co_BorrowerSelfDoBirth, 
@Co_BorrowerSelfSSN, 
@IsWorkingWithRealtor, 
@RealtorName, 
@RealtorPhone, 
@IsBorrowersBankruptcy,
@Bankruptcywhen,
@IsBorrowerResponsiblechild,
@childsupportqty

)      
              
                 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Authenticate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Authenticate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Authenticate]
	-- Add the parameters for the stored procedure here
@LoginName varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *  FROM tblUser WHERE LoginName=@LoginName
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Authenticate_tblUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Authenticate_tblUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Authenticate_tblUser]
	-- Add the parameters for the stored procedure here
	@LoginName varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT *  FROM tblUser WHERE LoginName=@LoginName 
    -- Insert statements for procedure here
	
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckLoanNo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckLoanNo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_CheckLoanNo]
(
@loan_no varchar (max)
)
AS
BEGIN
select * from loandat where lt_loan_stats  IN ('Locked','Application Received','Submitted to Underwriting','In Underwriting','Loan Suspended','Approved') and loan_no = @loan_no 



END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteLoan_LOANAPP]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteLoan_LOANAPP]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteLoan_LOANAPP]
	-- Add the parameters for the stored procedure here
	@loanid varchar(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM TBLLOANAPP WHERE loanid=@loanid
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteLoan_LoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteLoan_LoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteLoan_LoanApplication]
	-- Add the parameters for the stored procedure here
	@LoanApplicationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM tblLoanApplication WHERE LoanApplicationID =@LoanApplicationID
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchData_loanapp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchData_loanapp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchData_loanapp]

@loanid varchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from tblloanapp
Where loanid=@loanid
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchData_LoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchData_LoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchData_LoanApplication]
	-- Add the parameters for the stored procedure here
	@LoanApplicationID varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select * From tblLoanApplication where LoanApplicationID=@LoanApplicationID
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_generate_insertsStatement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_generate_insertsStatement]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create  PROC [dbo].[sp_generate_insertsStatement]
(
            @table_name varchar(776),                          -- The table/view for which the INSERT statements will be generated using the existing data
            @target_table varchar(776) = NULL,             -- Use this parameter to specify a different table name into which the data will be inserted
            @include_column_list bit = 1,                        -- Use this parameter to include/ommit column list in the generated INSERT statement
            @from varchar(800) = NULL,                         -- Use this parameter to filter the rows based on a filter condition (using WHERE)
            @include_timestamp bit = 0,                         -- Specify 1 for this parameter, if you want to include the TIMESTAMP/ROWVERSION column's data in the INSERT statement
            @debug_mode bit = 0,                                   -- If @debug_mode is set to 1, the SQL statements constructed by this procedure will be printed for later examination
            @owner varchar(64) = NULL,             -- Use this parameter if you are not the owner of the table
            @ommit_images bit = 0,                                -- Use this parameter to generate INSERT statements by omitting the 'image' columns
            @ommit_identity bit = 0,                   -- Use this parameter to ommit the identity columns
            @top int = NULL,                                 -- Use this parameter to generate INSERT statements only for the TOP n rows
            @cols_to_include varchar(8000) = NULL,      -- List of columns to be included in the INSERT statement
            @cols_to_exclude varchar(8000) = NULL,     -- List of columns to be excluded from the INSERT statement
            @disable_constraints bit = 0,             -- When 1, disables foreign key constraints and enables them after the INSERT statements
            @ommit_computed_cols bit = 0                    -- When 1, computed columns will not be included in the INSERT statement
            
)
AS
BEGIN

/***********************************************************************************************************
Procedure:      sp_generate_inserts  (Build 22) 

                                          
Purpose:          To generate INSERT statements from existing data. 
                        These INSERTS can be executed to regenerate the data at some other location.
                        This procedure is also useful to create a database setup, where in you can 
                        script your data along with your table definitions.

Written by:     Manoj Kumar Soni
                    
Tested on:       SQL Server 7.0 and SQL Server 2000

Date created: Sep 15th 2009 21:52 GMT

Email:                         manojsoni80@gmail.com

NOTE:             This procedure may not work with tables with too many columns.
                        Results can be unpredictable with huge text columns or SQL Server 2000's sql_variant data types
                        Whenever possible, Use @include_column_list parameter to ommit column list in the INSERT statement, for better results
                        IMPORTANT: This procedure is not tested with internation data (Extended characters or Unicode). If needed
                        you might want to convert the datatypes of character variables in this procedure to their respective unicode counterparts
                        like nchar and nvarchar
                        

Example 1:      To generate INSERT statements for table 'titles':
                        
                        EXEC sp_generate_inserts 'titles'

Example 2:      To ommit the column list in the INSERT statement: (Column list is included by default)
                        IMPORTANT: If you have too many columns, you are advised to ommit column list, as shown below,
                        to avoid erroneous results
                        
                        EXEC sp_generate_inserts 'titles', @include_column_list = 0

Example 3:      To generate INSERT statements for 'titlesCopy' table from 'titles' table:

                        EXEC sp_generate_inserts 'titles', 'titlesCopy'

Example 4:      To generate INSERT statements for 'titles' table for only those titles 
                        which contain the word 'Computer' in them:
                        NOTE: Do not complicate the FROM or WHERE clause here. It's assumed that you are good with T-SQL if you are using this parameter

                        EXEC sp_generate_inserts 'titles', @from = "from titles where title like '%Computer%'"

Example 5:      To specify that you want to include TIMESTAMP column's data as well in the INSERT statement:
                        (By default TIMESTAMP column's data is not scripted)

                        EXEC sp_generate_inserts 'titles', @include_timestamp = 1

Example 6:      To print the debug information:
  
                        EXEC sp_generate_inserts 'titles', @debug_mode = 1

Example 7:      If you are not the owner of the table, use @owner parameter to specify the owner name
                        To use this option, you must have SELECT permissions on that table

                        EXEC sp_generate_inserts Nickstable, @owner = 'Nick'

Example 8:      To generate INSERT statements for the rest of the columns excluding images
                        When using this otion, DO NOT set @include_column_list parameter to 0.

                        EXEC sp_generate_inserts imgtable, @ommit_images = 1

Example 9:      To generate INSERT statements excluding (ommiting) IDENTITY columns:
                        (By default IDENTITY columns are included in the INSERT statement)

                        EXEC sp_generate_inserts mytable, @ommit_identity = 1

Example 10:    To generate INSERT statements for the TOP 10 rows in the table:
                        
                        EXEC sp_generate_inserts mytable, @top = 10

Example 11:    To generate INSERT statements with only those columns you want:
                        
                        EXEC sp_generate_inserts titles, @cols_to_include = "'title','title_id','au_id'"

Example 12:    To generate INSERT statements by omitting certain columns:
                        
                        EXEC sp_generate_inserts titles, @cols_to_exclude = "'title','title_id','au_id'"

Example 13:    To avoid checking the foreign key constraints while loading data with INSERT statements:
                        
                        EXEC sp_generate_inserts titles, @disable_constraints = 1

Example 14:    To exclude computed columns from the INSERT statement:
                        EXEC sp_generate_inserts MyTable, @ommit_computed_cols = 1
***********************************************************************************************************/

SET NOCOUNT ON

--Making sure user only uses either @cols_to_include or @cols_to_exclude
IF ((@cols_to_include IS NOT NULL) AND (@cols_to_exclude IS NOT NULL))
            BEGIN
                        RAISERROR('Use either @cols_to_include or @cols_to_exclude. Do not use both the parameters at once',16,1)
                        RETURN -1 --Failure. Reason: Both @cols_to_include and @cols_to_exclude parameters are specified
            END

--Making sure the @cols_to_include and @cols_to_exclude parameters are receiving values in proper format
IF ((@cols_to_include IS NOT NULL) AND (PATINDEX('''%''',@cols_to_include) = 0))
            BEGIN
                        RAISERROR('Invalid use of @cols_to_include property',16,1)
                        PRINT 'Specify column names surrounded by single quotes and separated by commas'
                        PRINT 'Eg: EXEC sp_generate_inserts titles, @cols_to_include = "''title_id'',''title''"'
                        RETURN -1 --Failure. Reason: Invalid use of @cols_to_include property
            END

IF ((@cols_to_exclude IS NOT NULL) AND (PATINDEX('''%''',@cols_to_exclude) = 0))
            BEGIN
                        RAISERROR('Invalid use of @cols_to_exclude property',16,1)
                        PRINT 'Specify column names surrounded by single quotes and separated by commas'
                        PRINT 'Eg: EXEC sp_generate_inserts titles, @cols_to_exclude = "''title_id'',''title''"'
                        RETURN -1 --Failure. Reason: Invalid use of @cols_to_exclude property
            END


--Checking to see if the database name is specified along wih the table name
--Your database context should be local to the table for which you want to generate INSERT statements
--specifying the database name is not allowed
IF (PARSENAME(@table_name,3)) IS NOT NULL
            BEGIN
                        RAISERROR('Do not specify the database name. Be in the required database and just specify the table name.',16,1)
                        RETURN -1 --Failure. Reason: Database name is specified along with the table name, which is not allowed
            END

--Checking for the existence of 'user table' or 'view'
--This procedure is not written to work on system tables
--To script the data in system tables, just create a view on the system tables and script the view instead

IF @owner IS NULL
            BEGIN
                        IF ((OBJECT_ID(@table_name,'U') IS NULL) AND (OBJECT_ID(@table_name,'V') IS NULL)) 
                                    BEGIN
                                                RAISERROR('User table or view not found.',16,1)
                                                PRINT 'You may see this error, if you are not the owner of this table or view. In that case use @owner parameter to specify the owner name.'
                                                PRINT 'Make sure you have SELECT permission on that table or view.'
                                                RETURN -1 --Failure. Reason: There is no user table or view with this name
                                    END
            END
ELSE
            BEGIN
                        IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @table_name AND (TABLE_TYPE = 'BASE TABLE' OR TABLE_TYPE = 'VIEW') AND TABLE_SCHEMA = @owner)
                                    BEGIN
                                                RAISERROR('User table or view not found.',16,1)
                                                PRINT 'You may see this error, if you are not the owner of this table. In that case use @owner parameter to specify the owner name.'
                                                PRINT 'Make sure you have SELECT permission on that table or view.'
                                                RETURN -1 --Failure. Reason: There is no user table or view with this name                        
                                    END
            END

--Variable declarations
DECLARE                     @Column_ID int,                    
                        @Column_List varchar(8000), 
                        @Column_Name varchar(128), 
                        @Start_Insert varchar(786), 
                        @Data_Type varchar(128), 
                        @Actual_Values varchar(8000),         --This is the string that will be finally executed to generate INSERT statements
                        @IDN varchar(128)                 --Will contain the IDENTITY column's name in the table

--Variable Initialization
SET @IDN = ''
SET @Column_ID = 0
SET @Column_Name = ''
SET @Column_List = ''
SET @Actual_Values = ''

IF @owner IS NULL 
            BEGIN
                        SET @Start_Insert = 'INSERT INTO ' + '[' + RTRIM(COALESCE(@target_table,@table_name)) + ']' 
            END
ELSE
            BEGIN
                        SET @Start_Insert = 'INSERT ' + '[' + LTRIM(RTRIM(@owner)) + '].' + '[' + RTRIM(COALESCE(@target_table,@table_name)) + ']'                    
            END


--To get the first column's ID

SELECT            @Column_ID = MIN(ORDINAL_POSITION)    
FROM INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
WHERE           TABLE_NAME = @table_name AND
(@owner IS NULL OR TABLE_SCHEMA = @owner)



--Loop through all the columns of the table, to get the column names and their data types
WHILE @Column_ID IS NOT NULL
            BEGIN
                        SELECT            @Column_Name = QUOTENAME(COLUMN_NAME), 
                        @Data_Type = DATA_TYPE 
                        FROM INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
                        WHERE            ORDINAL_POSITION = @Column_ID AND 
                        TABLE_NAME = @table_name AND
                        (@owner IS NULL OR TABLE_SCHEMA = @owner)



                        IF @cols_to_include IS NOT NULL --Selecting only user specified columns
                        BEGIN
                                    IF CHARINDEX( '''' + SUBSTRING(@Column_Name,2,LEN(@Column_Name)-2) + '''',@cols_to_include) = 0 
                                    BEGIN
                                                GOTO SKIP_LOOP
                                    END
                        END

                        IF @cols_to_exclude IS NOT NULL --Selecting only user specified columns
                        BEGIN
                                    IF CHARINDEX( '''' + SUBSTRING(@Column_Name,2,LEN(@Column_Name)-2) + '''',@cols_to_exclude) <> 0 
                                    BEGIN
                                                GOTO SKIP_LOOP
                                    END
                        END

                        --Making sure to output SET IDENTITY_INSERT ON/OFF in case the table has an IDENTITY column
                        IF (SELECT COLUMNPROPERTY( OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + @table_name),SUBSTRING(@Column_Name,2,LEN(@Column_Name) - 2),'IsIdentity')) = 1 
                        BEGIN
                                    IF @ommit_identity = 0 --Determing whether to include or exclude the IDENTITY column
                                                SET @IDN = @Column_Name
                                    ELSE
                                                GOTO SKIP_LOOP                               
                        END
                        
                        --Making sure whether to output computed columns or not
                        IF @ommit_computed_cols = 1
                        BEGIN
                                    IF (SELECT COLUMNPROPERTY( OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + @table_name),SUBSTRING(@Column_Name,2,LEN(@Column_Name) - 2),'IsComputed')) = 1 
                                    BEGIN
                                                GOTO SKIP_LOOP                                                       
                                    END
                        END
                        
                        --Tables with columns of IMAGE data type are not supported for obvious reasons
                        IF(@Data_Type in ('image'))
                                    BEGIN
                                                IF (@ommit_images = 0)
                                                            BEGIN
                                                                        RAISERROR('Tables with image columns are not supported.',16,1)
                                                                        PRINT 'Use @ommit_images = 1 parameter to generate INSERTs for the rest of the columns.'
                                                                        PRINT 'DO NOT ommit Column List in the INSERT statements. If you ommit column list using @include_column_list=0, the generated INSERTs will fail.'
                                                                        RETURN -1 --Failure. Reason: There is a column with image data type
                                                            END
                                                ELSE
                                                            BEGIN
                                                            GOTO SKIP_LOOP
                                                            END
                                    END

                        --Determining the data type of the column and depending on the data type, the VALUES part of
                        --the INSERT statement is generated. Care is taken to handle columns with NULL values. Also
                        --making sure, not to lose any data from flot, real, money, smallmomey, datetime columns
                        SET @Actual_Values = @Actual_Values  +
                        CASE 
                                    WHEN @Data_Type IN ('char','varchar','nchar','nvarchar') 
                                                THEN 
                                                            'COALESCE('''''''' + REPLACE(RTRIM(' + @Column_Name + '),'''''''','''''''''''')+'''''''',''NULL'')'
                                    WHEN @Data_Type IN ('datetime','smalldatetime') 
                                                THEN 
                                                            'COALESCE('''''''' + RTRIM(CONVERT(char,' + @Column_Name + ',109))+'''''''',''NULL'')'
                                    WHEN @Data_Type IN ('uniqueidentifier') 
                                                THEN  
                                                            'COALESCE('''''''' + REPLACE(CONVERT(char(255),RTRIM(' + @Column_Name + ')),'''''''','''''''''''')+'''''''',''NULL'')'
                                    WHEN @Data_Type IN ('text','ntext') 
                                                THEN  
                                                            'COALESCE('''''''' + REPLACE(CONVERT(char(8000),' + @Column_Name + '),'''''''','''''''''''')+'''''''',''NULL'')'                                                 
                                    WHEN @Data_Type IN ('binary','varbinary') 
                                                THEN  
                                                            'COALESCE(RTRIM(CONVERT(char,' + 'CONVERT(int,' + @Column_Name + '))),''NULL'')'  
                                    WHEN @Data_Type IN ('timestamp','rowversion') 
                                                THEN  
                                                            CASE 
                                                                        WHEN @include_timestamp = 0 
                                                                                    THEN 
                                                                                                '''DEFAULT''' 
                                                                                    ELSE 
                                                                                                'COALESCE(RTRIM(CONVERT(char,' + 'CONVERT(int,' + @Column_Name + '))),''NULL'')'  
                                                            END
                                    WHEN @Data_Type IN ('float','real','money','smallmoney')
                                                THEN
                                                            'COALESCE(LTRIM(RTRIM(' + 'CONVERT(char, ' +  @Column_Name  + ',2)' + ')),''NULL'')' 
                                    ELSE 
                                                'COALESCE(LTRIM(RTRIM(' + 'CONVERT(char, ' +  @Column_Name  + ')' + ')),''NULL'')' 
                        END   + '+' +  ''',''' + ' + '
                        
                        --Generating the column list for the INSERT statement
                        SET @Column_List = @Column_List +  @Column_Name + ','           

                        SKIP_LOOP: --The label used in GOTO

                        SELECT            @Column_ID = MIN(ORDINAL_POSITION) 
                        FROM INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
                        WHERE            TABLE_NAME = @table_name AND 
                        ORDINAL_POSITION > @Column_ID AND
                        (@owner IS NULL OR TABLE_SCHEMA = @owner)


            --Loop ends here!
            END

--To get rid of the extra characters that got concatenated during the last run through the loop
SET @Column_List = LEFT(@Column_List,len(@Column_List) - 1)
SET @Actual_Values = LEFT(@Actual_Values,len(@Actual_Values) - 6)

IF LTRIM(@Column_List) = '' 
            BEGIN
                        RAISERROR('No columns to select. There should at least be one column to generate the output',16,1)
                        RETURN -1 --Failure. Reason: Looks like all the columns are ommitted using the @cols_to_exclude parameter
            END

--Forming the final string that will be executed, to output the INSERT statements
IF (@include_column_list <> 0)
            BEGIN
                        SET @Actual_Values = 
                                    'SELECT ' +  
                                    CASE WHEN @top IS NULL OR @top < 0 THEN '' ELSE ' TOP ' + LTRIM(STR(@top)) + ' ' END + 
                                    '''' + RTRIM(@Start_Insert) + 
                                    ' ''+' + '''(' + RTRIM(@Column_List) +  '''+' + ''')''' + 
                                    ' +''VALUES(''+ ' +  @Actual_Values  + '+'')''' + ' ' + 
                                    COALESCE(@from,' FROM ' + CASE WHEN @owner IS NULL THEN '' ELSE '[' + LTRIM(RTRIM(@owner)) + '].' END + '[' + rtrim(@table_name) + ']' + '(NOLOCK)')
            END
ELSE IF (@include_column_list = 0)
            BEGIN
                        SET @Actual_Values = 
                                    'SELECT ' + 
                                    CASE WHEN @top IS NULL OR @top < 0 THEN '' ELSE ' TOP ' + LTRIM(STR(@top)) + ' ' END + 
                                    '''' + RTRIM(@Start_Insert) + 
                                    ' '' +''VALUES(''+ ' +  @Actual_Values + '+'')''' + ' ' + 
                                    COALESCE(@from,' FROM ' + CASE WHEN @owner IS NULL THEN '' ELSE '[' + LTRIM(RTRIM(@owner)) + '].' END + '[' + rtrim(@table_name) + ']' + '(NOLOCK)')
            END     

--Determining whether to ouput any debug information
IF @debug_mode =1
            BEGIN
                        PRINT '/*****START OF DEBUG INFORMATION*****'
                        PRINT 'Beginning of the INSERT statement:'
                        PRINT @Start_Insert
                        PRINT ''
                        PRINT 'The column list:'
                        PRINT @Column_List
                        PRINT ''
                        PRINT 'The SELECT statement executed to generate the INSERTs'
                        PRINT @Actual_Values
                        PRINT ''
                        PRINT '*****END OF DEBUG INFORMATION*****/'
                        PRINT ''
            END
                        
PRINT '--INSERTs generated by ''sp_generate_insertsStatement'' stored procedure written by Manoj'
PRINT '--Build number: 22'
PRINT '--Problems/Suggestions? Contact Manojsoni80@gmail.com'
PRINT ''
PRINT 'SET NOCOUNT ON'
PRINT ''


--Determining whether to print IDENTITY_INSERT or not
IF (@IDN <> '')
            BEGIN
                        PRINT 'SET IDENTITY_INSERT ' + QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + QUOTENAME(@table_name) + ' ON'
                        PRINT 'GO'
                        PRINT ''
            END


IF @disable_constraints = 1 AND (OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + @table_name, 'U') IS NOT NULL)
            BEGIN
                        IF @owner IS NULL
                                    BEGIN
                                                SELECT            'ALTER TABLE ' + QUOTENAME(COALESCE(@target_table, @table_name)) + ' NOCHECK CONSTRAINT ALL' AS '--Code to disable constraints temporarily'
                                    END
                        ELSE
                                    BEGIN
                                                SELECT            'ALTER TABLE ' + QUOTENAME(@owner) + '.' + QUOTENAME(COALESCE(@target_table, @table_name)) + ' NOCHECK CONSTRAINT ALL' AS '--Code to disable constraints temporarily'
                                    END

                        PRINT 'GO'
            END

PRINT ''
PRINT 'PRINT ''Inserting values into ' + '[' + RTRIM(COALESCE(@target_table,@table_name)) + ']' + ''''


--All the hard work pays off here!!! You'll get your INSERT statements, when the next line executes!
EXEC (@Actual_Values)

PRINT 'PRINT ''Done'''
PRINT ''


IF @disable_constraints = 1 AND (OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + @table_name, 'U') IS NOT NULL)
            BEGIN
                        IF @owner IS NULL
                                    BEGIN
                                                SELECT            'ALTER TABLE ' + QUOTENAME(COALESCE(@target_table, @table_name)) + ' CHECK CONSTRAINT ALL'  AS '--Code to enable the previously disabled constraints'
                                    END
                        ELSE
                                    BEGIN
                                                SELECT            'ALTER TABLE ' + QUOTENAME(@owner) + '.' + QUOTENAME(COALESCE(@target_table, @table_name)) + ' CHECK CONSTRAINT ALL' AS '--Code to enable the previously disabled constraints'
                                    END

                        PRINT 'GO'
            END

PRINT ''
IF (@IDN <> '')
            BEGIN
                        PRINT 'SET IDENTITY_INSERT ' + QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + QUOTENAME(@table_name) + ' OFF'
                        PRINT 'GO'
            END

PRINT 'SET NOCOUNT OFF'


SET NOCOUNT OFF
RETURN 0 --Success. We are done!
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get1098DataForAllLoansFor2009]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get1098DataForAllLoansFor2009]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_Get1098DataForAllLoansFor2009]
(
@loanid varchar (max)
)
AS
BEGIN
select loanid,reportedTIN as TaxIDNumber,ReportedFirstname as firstmiddlename,ReportedLastName as lastname,
MailLine1 as addressline1,MailLine2 as addressline2,MAilCity as city,MailState as state,MailZip as zip,
ReportableAmount AS totalintcoll,PointsPaid,ARMIntReimbursed,PMIPaid,PrincipalBal,princollected,NegativeAmBal,AssistanceBal, 
BeginEscBal,EscDepAmt,TaxesPaid1 , TaxesPaid,InsurancePaid,EscDisbAmt AS OtherFundsBal,escrowbal,IntOnEscrow,
IOEWithholding from dbo.r_annual_stmt
where reportingyear = '2009' and loanid in (@loanid) order by reportingdate desc 


END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get1098DataForAllLoansFor2010]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get1098DataForAllLoansFor2010]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_Get1098DataForAllLoansFor2010]
(
@loanid varchar (max)
)
AS
BEGIN
select eyear.loanid,reportedTIN as TaxIDNumber,annualStmt.ReportedFirstname as firstmiddlename,
annualStmt.ReportedLastName as lastname,  annualStmt.MailLine1 as addressline1,annualStmt.MailLine2 as addressline2,
annualStmt.MAilCity as city,annualStmt.MailState as state,annualStmt.MailZip as zip,  eyear.TotalIntColl AS totalintcoll,
eyear.PointsPaid,annualStmt.ARMIntReimbursed,eyear.PMIPaid,annualStmt.PrincipalBal,annualStmt.princollected,
annualStmt.NegativeAmBal,annualStmt.AssistanceBal,
BeginEscBal,EscDepAmt,TaxesPaid1 , eyear.TaxesPaid,
annualStmt.InsurancePaid,annualStmt.EscDisbAmt AS OtherFundsBal,annualStmt.escrowbal,annualStmt.IntOnEscrow,annualStmt.IOEWithholding 
from dbo.r_annual_stmt annualStmt
Left outer join endofyear eyear
on annualStmt.loanid=eyear.loanid 
and year(eyear.ReportingDate) = year(annualStmt.ReportingDate)
where year(eyear.ReportingDate) = '2011' and
eyear.loanid in (@loanid) order by eyear.reportingdate desc 


END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAlertInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAlertInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetAlertInfo]
(
@userid varchar(Max),
@loanno varchar(Max)
)
AS
BEGIN
select * from tblServiceInfo where userid = @userid and loanno = @loanno
     
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetapplicationData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetapplicationData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--[sp_GetapplicationData] 1,50,'LoanApplicationID','([AppFName] like ''drew'')'  
CREATE PROCEDURE [dbo].[sp_GetapplicationData]   
(  
 @pageNum int  
,@pageSize int  
,@orderby varchar(2000)  
,@FilterExp varchar(2000)  
)AS  
  
SET NOCOUNT ON   
   
declare @lownum nvarchar(10)  
declare @highnum nvarchar(10)  
declare @sql nvarchar(4000)  
declare @sqlcount nvarchar(4000)   
set @lownum = convert(nvarchar(10), (@pagesize * (@pagenum - 1)))  
set @highnum = convert(nvarchar(10), (@pagesize * @pagenum))  

if @orderby = ''
set @orderby = 'DateEntered'


if @FilterExp = ''
Begin  
set @sql = 'select LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp, StateName   from (  
  select row_number() over (order by ' + @orderby  + ') as rownum  
         ,LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp,tblStateNylex.StateName   
  from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID    
  
 ) A WHERE RowNum > ' + @lownum + ' AND rownum <= ' + @highnum  
  print @sql
--WHERE rownum >  @pagesize * (@pageNum - 1) AND rownum <=  (@pagesize * @pageNum)  
end
else
Begin
set @sql = 'select LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp, StateName   from (  
  select row_number() over (order by ' + @orderby  + ') as rownum   
         ,LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp,tblStateNylex.StateName   
  from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID where ' + @FilterExp +' ) A WHERE RowNum > ' + @lownum + ' AND rownum <= ' + @highnum  
End  
print @sql
-- Execute the SQL query  

if @FilterExp <> ''
set @sqlcount = 'select count(loanapplicationID) as totalrec from tblLoanApplication  left join tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID where ' + @FilterExp         
else
set @sqlcount = 'select count(loanapplicationID) as totalrec from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID'  
  

EXEC sp_executesql @sql 
EXEC sp_executesql @sqlcount 
  




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetapplicationData1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetapplicationData1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--[sp_GetapplicationData1] 2,50,'AppFName',''  
CREATE PROCEDURE [dbo].[sp_GetapplicationData1]   
(  
 @pageNum int  
,@pageSize int  
,@orderby varchar(2000)  
,@FilterExp varchar(2000)
)AS  
  
SET NOCOUNT ON   

declare @DESCorASC varchar(50)     
declare @lownum nvarchar(10)  
declare @highnum nvarchar(10)  
declare @sql nvarchar(4000)  
declare @sqlcount nvarchar(4000)   
set @lownum = convert(nvarchar(10), (@pagesize * (@pagenum - 1)))  
set @highnum = convert(nvarchar(10), (@pagesize * @pagenum))  

if @orderby = ''
begin
	set @orderby = 'DateEntered'
	set @DESCorASC = 'DESC'
end
else
	if @orderby = 'DateEntered'
	begin
		set @DESCorASC = 'DESC'
	end
	else
		set @DESCorASC = 'ASC'
print @DESCorASC
if @FilterExp = ''
Begin  
set @sql = 'select LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp, StateName   from (  
  select row_number() over (order by ' + @orderby + ' ' + @DESCorASC + ') as rownum  
         ,LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp,tblStateNylex.StateName   
  from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID    
  
 ) A WHERE RowNum > ' + @lownum + ' AND rownum <= ' + @highnum  
  --print @sql
--WHERE rownum >  @pagesize * (@pageNum - 1) AND rownum <=  (@pagesize * @pageNum)  
end
else
Begin
set @sql = 'select LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp, StateName   from (  
  select row_number() over (order by ' + @orderby + ' ' + @DESCorASC + ') as rownum   
         ,LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp,tblStateNylex.StateName   
  from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID where ' + @FilterExp +' ) A WHERE RowNum > ' + @lownum + ' AND rownum <= ' + @highnum  
End  
--print @sql
-- Execute the SQL query  

if @FilterExp <> ''
set @sqlcount = 'select count(loanapplicationID) as totalrec from tblLoanApplication  left join tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID where ' + @FilterExp         
else
set @sqlcount = 'select count(loanapplicationID) as totalrec from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID'  
  

EXEC sp_executesql @sql
EXEC sp_executesql @sqlcount 
  



set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetContactTime]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetContactTime]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetContactTime]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT  ContactTimeID, ContactTime FROM  tblContactTime
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCreditScore]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCreditScore]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetCreditScore]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT ID, CreditScoreID, CreditScoreValue FROM tblCreditScore order by CreditOrder
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDataList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDataList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDataList]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

SELECT 
tblloanapp.LoanId,tblloanapp.Fname,tblloanapp.Mname,tblloanapp.LName,tblloanapp.Email,tblloanapp.CEmail, tblloanapp.DOBM,tblloanapp.DOBDT, tblloanapp.DOBYR, tblloanapp.SSN, tblloanapp.Phone, tblloanapp.Suffix, tblloanapp.COFNAME, tblloanapp.COMNAME,tblloanapp.COLNAME, tblloanapp.COEMAIL, tblloanapp.COCEmail, tblloanapp.CODOBM, tblloanapp.CODOBDT, tblloanapp.CODOBYR,tblloanapp.COSSN, tblloanapp.COPHONE, tblloanapp.STADDR, tblloanapp.CITY, tblloanapp.MState, tblloanapp.ZipCode, tblloanapp.RSTATUS,tblloanapp.RentAmt, tblloanapp.LMHOLDER, tblloanapp.STADDR2, tblloanapp.CITY2, tblloanapp.STATE2, tblloanapp.COSuffix, tblloanapp.ZipCode2,tblloanapp.RLengthY2, tblloanapp.RLengthM2, tblloanapp.RSTATUS2, tblloanapp.RentAmt2, tblloanapp.LMHOLDER2, tblloanapp.AEMPL,tblloanapp.AOCCU, tblloanapp.AEMPPHONE, tblloanapp.AGMI, tblloanapp.ALEMY, tblloanapp.ALEMM, tblloanapp.AEMPL2, tblloanapp.AOCCU2,tblloanapp.AEMPPHONE2, tblloanapp.AGMI2, tblloanapp.ALEMY2, tblloanapp.ALEMM2, tblloanapp.CAEMPL, tblloanapp.CAOCCU,tblloanapp.CAEMPPHONE, tblloanapp.CAGMI, tblloanapp.CALEMY, tblloanapp.CALEMM, tblloanapp.CAEMPL2, tblloanapp.CAOCCU2,tblloanapp.CAEMPPHONE2, tblloanapp.CAGMI2, tblloanapp.CALEMY2, tblloanapp.CALEMM2, tblloanapp.CurrLoanMake, tblloanapp.CurrLoanYear, tblloanapp.CurrLoanBalance, tblloanapp.CurrLoanMiles, tblloanapp.OtherComments, tblloanapp.DigiSig, tblloanapp.SigDate, tblloanapp.IsAckno,tblloanapp.DateEntered, tblloanapp.RLengthM, tblloanapp.RlengthY, tblloanapp.Status, tblloanapp.Network, tblloanapp.PayDate, tblloanapp.Amount, tblloanapp.Paid, tblloanapp.lastupdateby, tblloanapp.NetworkName, tblloanapp.NetworkPhone, tblstate.StateName FROM tblloanapp 
INNER JOIN tblstate ON tblloanapp.MState = tblstate.SId order by tblloanapp.loanid desc  
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDataList_loanapp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDataList_loanapp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDataList_loanapp]
	-- Add the parameters for the stored procedure here
	@loanid varchar(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM tblloanapp where loanid=@loanid

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDataListLoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDataListLoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDataListLoanApplication]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT     LoanApplicationID, LoanReasonID, HometypeID, Producttypeid, StateID, CreditScoreID, AnnulIncomeBorrower,LoanAmount, AssetBorrower, 
                RetirementAssetsBorrower, PropertyLocated, HomeOwned, PurchasePrise, DownPaymentAmt, PartOfTotalAssets, DownPaymentSourceID,  
                HaveRealtor, RealtorContactName, RealtorPhone, RealtorEmail, AppFName, AppLName, AppFName+','+ AppLName as AppFullNAme, AppStateID, AppZip, AppPrimaryPhone, AppSecondaryPhone,  
                AppEmail, CurrentPropertyValue, PropertyPurchaseMonth, YearID, ExistingPuchasePrice, CurrentMortageBal, MonthlyPayment, SecondMortage,  
                SecondMortageBal, DateEntered, ModifiedBy, ModifiedDate, UserAppearUrl, PropertyTypeID, PropertyTypeName, ContactTime, ContactTimeID,  
                Hometype, CreditScoreValue, StateName, LoanReason, ProductTypeName, AppStateName,Year,MAName,IsWorkingWithRealtor, PropertyRealtorCity,IsCashback,RealtorName,RealtorPhoneNo 
                FROM         (SELECT     tblLoanApplication.LoanApplicationID, tblLoanApplication.LoanReasonID, tblLoanApplication.HometypeID, tblLoanApplication.Producttypeid,
                tblLoanApplication.StateID, tblLoanApplication.CreditScoreID, tblLoanApplication.AnnulIncomeBorrower,LoanAmount, tblLoanApplication.AssetBorrower,
                tblLoanApplication.RetirementAssetsBorrower, tblLoanApplication.PropertyLocated, tblLoanApplication.HomeOwned, tblLoanApplication.PurchasePrise,  
                tblLoanApplication.DownPaymentAmt, tblLoanApplication.PartOfTotalAssets, tblLoanApplication.DownPaymentSourceID,  
                tblLoanApplication.HaveRealtor, tblLoanApplication.RealtorContactName, tblLoanApplication.RealtorPhone, tblLoanApplication.RealtorEmail,  
                tblLoanApplication.AppFName, tblLoanApplication.AppLName, tblLoanApplication.AppStateID, tblLoanApplication.AppZip,  
                tblLoanApplication.AppPrimaryPhone, tblLoanApplication.AppSecondaryPhone, tblLoanApplication.AppEmail, tblLoanApplication.CurrentPropertyValue,  
                tblLoanApplication.PropertyPurchaseMonth, tblLoanApplication.YearID, tblLoanApplication.ExistingPuchasePrice,  
                tblLoanApplication.CurrentMortageBal, tblLoanApplication.MonthlyPayment, tblLoanApplication.SecondMortage, tblLoanApplication.SecondMortageBal,  
                tblLoanApplication.DateEntered, tblLoanApplication.ModifiedBy, tblLoanApplication.ModifiedDate, tblLoanApplication.UserAppearUrl,  
                tblLoanApplication.PropertyTypeID, tblPropertyType.Name AS PropertyTypeName, tblContactTime.ContactTime, tblLoanApplication.ContactTimeID,  
                tblHomeType.Hometype, tblCreditScore.CreditScoreValue, tblStateNylex.StateName, tblLoanReason.LoanReason,  
                tblProductType.name AS ProductTypeName, tblStateNylex_1.StateName AS AppStateName, tblYear.Year,tblLoanApplication.MAName,tblLoanApplication.IsWorkingWithRealtor, tblLoanApplication.PropertyRealtorCity,tblLoanApplication.IsCashback,tblLoanApplication.RealtorName,tblLoanApplication.RealtorPhoneNo 
                FROM         tblLoanApplication INNER JOIN
                tblPropertyType ON tblLoanApplication.PropertyTypeID = tblPropertyType.PropertyTypeID INNER JOIN 
                tblContactTime ON tblLoanApplication.ContactTimeID = tblContactTime.ContactTimeID INNER JOIN 
                tblHomeType ON tblLoanApplication.HometypeID = tblHomeType.HometypeID INNER JOIN
                tblCreditScore ON tblLoanApplication.CreditScoreID = tblCreditScore.CreditScoreID INNER JOIN 
                tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID INNER JOIN 
                tblLoanReason ON tblLoanApplication.LoanReasonID = tblLoanReason.LoanReasonID INNER JOIN 
                tblProductType ON tblLoanApplication.Producttypeid = tblProductType.producttypeid INNER JOIN 
                tblStateNylex AS tblStateNylex_1 ON tblLoanApplication.AppStateID = tblStateNylex_1.StateID INNER JOIN 
                tblYear ON tblLoanApplication.YearID = tblYear.YearID) AS App order by LoanApplicationID desc 

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDataTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDataTable]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDataTable]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

SELECT     tblloanapp.LoanId, tblloanapp.Fname, tblloanapp.Mname, tblloanapp.LName, tblloanapp.Email, tblloanapp.CEmail, tblloanapp.DOBM,  tblloanapp.DOBDT, tblloanapp.DOBYR, tblloanapp.SSN, tblloanapp.Phone, tblloanapp.Suffix, tblloanapp.COFNAME, tblloanapp.COMNAME,tblloanapp.COLNAME, tblloanapp.COEMAIL, tblloanapp.COCEmail, tblloanapp.CODOBM, tblloanapp.CODOBDT, tblloanapp.CODOBYR,    tblloanapp.COSSN, tblloanapp.COPHONE, tblloanapp.STADDR, tblloanapp.CITY, tblloanapp.MState, tblloanapp.ZipCode, tblloanapp.RSTATUS,  tblloanapp.RentAmt, tblloanapp.LMHOLDER, tblloanapp.STADDR2, tblloanapp.CITY2, tblloanapp.STATE2, tblloanapp.COSuffix, tblloanapp.ZipCode2,  tblloanapp.RLengthY2, tblloanapp.RLengthM2, tblloanapp.RSTATUS2, tblloanapp.RentAmt2, tblloanapp.LMHOLDER2, tblloanapp.AEMPL,  tblloanapp.AOCCU, tblloanapp.AEMPPHONE, tblloanapp.AGMI, tblloanapp.ALEMY, tblloanapp.ALEMM, tblloanapp.AEMPL2, tblloanapp.AOCCU2,tblloanapp.AEMPPHONE2, tblloanapp.AGMI2, tblloanapp.ALEMY2, tblloanapp.ALEMM2, tblloanapp.CAEMPL, tblloanapp.CAOCCU,tblloanapp.CAEMPPHONE, tblloanapp.CAGMI, tblloanapp.CALEMY, tblloanapp.CALEMM, tblloanapp.CAEMPL2, tblloanapp.CAOCCU2,tblloanapp.CAEMPPHONE2, tblloanapp.CAGMI2, tblloanapp.CALEMY2, tblloanapp.CALEMM2, tblloanapp.CurrLoanMake, tblloanapp.CurrLoanYear,tblloanapp.CurrLoanBalance, tblloanapp.CurrLoanMiles, tblloanapp.OtherComments, tblloanapp.DigiSig, tblloanapp.SigDate, tblloanapp.IsAckno,tblloanapp.DateEntered, tblloanapp.RLengthM, tblloanapp.RlengthY, tblloanapp.Status, tblloanapp.Network, tblloanapp.PayDate, tblloanapp.Amount, tblloanapp.Paid, tblloanapp.lastupdateby, tblloanapp.NetworkName, tblloanapp.NetworkPhone, tblstate.StateName AS StateName1 ,tblstate_1.StateName as StateName2  FROM tblloanapp INNER JOIN tblstate ON tblloanapp.MState = tblstate.SId INNER JOIN tblstate AS tblstate_1 ON tblloanapp.STATE2 = tblstate_1.SId  ORDER BY tblloanapp.LoanId DESC
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDataTable1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDataTable1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDataTable1]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	
                            SELECT     LoanApplicationID, LoanReasonID, HometypeID, Producttypeid, StateID, CreditScoreID, AnnulIncomeBorrower, AssetBorrower,   
                RetirementAssetsBorrower, PropertyLocated, HomeOwned, PurchasePrise, DownPaymentAmt, PartOfTotalAssets, DownPaymentSourceID,   
                HaveRealtor, RealtorContactName, RealtorPhone, RealtorEmail, AppFName, AppLName, AppStateID, AppZip, AppPrimaryPhone, AppSecondaryPhone,
                AppEmail, CurrentPropertyValue, PropertyPurchaseMonth, YearID, ExistingPuchasePrice, CurrentMortageBal, MonthlyPayment, SecondMortage,
                SecondMortageBal, DateEntered, ModifiedBy, ModifiedDate, UserAppearUrl, PropertyTypeID, PropertyTypeName, ContactTime, ContactTimeID,  
                Hometype, CreditScoreValue, StateName, LoanReason, ProductTypeName, AppStateName, Year, SourceOfdownPayment 
                FROM         (SELECT     tblLoanApplication.LoanApplicationID, tblLoanApplication.LoanReasonID, tblLoanApplication.HometypeID, tblLoanApplication.Producttypeid,   
                tblLoanApplication.StateID, tblLoanApplication.CreditScoreID, tblLoanApplication.AnnulIncomeBorrower, tblLoanApplication.AssetBorrower,   
                tblLoanApplication.RetirementAssetsBorrower, tblLoanApplication.PropertyLocated, tblLoanApplication.HomeOwned,   
                tblLoanApplication.PurchasePrise, tblLoanApplication.DownPaymentAmt, tblLoanApplication.PartOfTotalAssets,
                tblLoanApplication.DownPaymentSourceID, tblLoanApplication.HaveRealtor, tblLoanApplication.RealtorContactName,
                tblLoanApplication.RealtorPhone, tblLoanApplication.RealtorEmail, tblLoanApplication.AppFName, tblLoanApplication.AppLName,
                tblLoanApplication.AppStateID, tblLoanApplication.AppZip, tblLoanApplication.AppPrimaryPhone, tblLoanApplication.AppSecondaryPhone,
                tblLoanApplication.AppEmail, tblLoanApplication.CurrentPropertyValue, tblLoanApplication.PropertyPurchaseMonth, 
                tblLoanApplication.YearID, tblLoanApplication.ExistingPuchasePrice, tblLoanApplication.CurrentMortageBal,  
                tblLoanApplication.MonthlyPayment, tblLoanApplication.SecondMortage, tblLoanApplication.SecondMortageBal,
                tblLoanApplication.DateEntered, tblLoanApplication.ModifiedBy, tblLoanApplication.ModifiedDate, tblLoanApplication.UserAppearUrl,  
                tblLoanApplication.PropertyTypeID, tblPropertyType.Name AS PropertyTypeName, tblContactTime.ContactTime,  
                tblLoanApplication.ContactTimeID, tblHomeType.Hometype, tblCreditScore.CreditScoreValue, tblStateNylex.StateName,
                tblLoanReason.LoanReason, tblProductType.name AS ProductTypeName, tblStateNylex_1.StateName AS AppStateName, tblYear.Year,
                tblDownPaymentSource.SourceOfdownPayment 
                FROM          tblLoanApplication LEFT JOIN 
                tblPropertyType ON tblLoanApplication.PropertyTypeID = tblPropertyType.PropertyTypeID LEFT JOIN  
                tblContactTime ON tblLoanApplication.ContactTimeID = tblContactTime.ContactTimeID LEFT JOIN  
                tblHomeType ON tblLoanApplication.HometypeID = tblHomeType.HometypeID LEFT JOIN
                tblCreditScore ON tblLoanApplication.CreditScoreID = tblCreditScore.CreditScoreID LEFT JOIN  
                tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID LEFT JOIN  
                tblLoanReason ON tblLoanApplication.LoanReasonID = tblLoanReason.LoanReasonID LEFT JOIN  
                tblProductType ON tblLoanApplication.Producttypeid = tblProductType.producttypeid LEFT JOIN  
                tblStateNylex AS tblStateNylex_1 ON tblLoanApplication.AppStateID = tblStateNylex_1.StateID LEFT JOIN  
                tblYear ON tblLoanApplication.YearID = tblYear.YearID LEFT OUTER JOIN  
                tblDownPaymentSource ON tblLoanApplication.DownPaymentSourceID = tblDownPaymentSource.DownPaymentSourceID) AS App order by LoanApplicationID desc  
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDownPaymentSource]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDownPaymentSource]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDownPaymentSource]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT DownPaymentSourceID, SourceOfdownPayment FROM tblDownPaymentSource
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetEscrowInfo2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetEscrowInfo2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetEscrowInfo2]
(
@loanid varchar(Max),
@TransactionDate varchar(Max)
)
AS
BEGIN

select  TransactionDate, description,nameline1,TransactionAmt,EndEscrowBal from history
left join TransactionCode as transcode on history.TransactionCode = transcode.Code
left join payee on payee.payeeid = history.payeeid
where loanid = @loanid
and TransactionAmt < 0
and history.TransactionDate <= @TransactionDate
and (TransactionDate < getdate() and TransactionDate > dateadd(year,-1,getdate()))
order by history.TransactionDate desc
     
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetEscrowInfo3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetEscrowInfo3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetEscrowInfo3]
(
@loanid varchar(Max),
@TransactionDate varchar(Max)
)
AS
BEGIN

select  TransactionDate, description,nameline1,TransactionAmt,EndEscrowBal from history
left join TransactionCode as transcode on history.TransactionCode = transcode.Code 
left join payee on payee.payeeid = history.payeeid
where loanid =@loanid
and TransactionAmt < 0
and (history.TransactionDate >= @TransactionDate and history.TransactionDate <=@TransactionDate)
order by history.TransactionDate desc
     
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHistory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetHistory]
	-- Add the parameters for the stored procedure here
	@loanid varchar(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM History  where loanid=@loanid order by id desc
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHistoryNotes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHistoryNotes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetHistoryNotes]
	-- Add the parameters for the stored procedure here
	@loanid varchar(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT modyfieddate,notes,isnull(lastupdateby,0) lastupdateby FROM History  where loanid=@loanid order by id desc
    -- Insert statements for procedure here
	
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHomeType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHomeType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetHomeType]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT HometypeID, Hometype FROM tblHomeType WHERE HometypeID IN (1,2,3)
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHowlong]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHowlong]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetHowlong]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT  * FROM  tbllookups where Lookupname='TP'
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetInfoForPayoffEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetInfoForPayoffEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetInfoForPayoffEmail]
(
@userid varchar (max),
@dbname varchar (max)
)
AS
BEGIN
declare @a varchar(max)
set @a='select ufirstname,ulastname,convert(varchar, payoffdate, 101) as payoffdate,description from tblpayoffstatement as payoff
join tblUsers as users on payoff.userid = users.userid 
left join' + @dbname + 'PayoffReason on payoff.payoffreasonid = payoffreason.code
 where users.userid ='+ @userid+''

exec(@a)
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetInfoForWelComeEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetInfoForWelComeEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetInfoForWelComeEmail]
(
@loanno varchar (max)
)
AS
BEGIN
 select * from tblServiceInfo as serviceInfo
join tblUsers on serviceInfo.userid = tblUsers.userid 
where serviceInfo.loanno = @loanno

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLastLoanRequest]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLastLoanRequest]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLastLoanRequest]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
select top 1 Appfname,AppLname,AppPrimaryPhone from tblloanapplication order by dateentered desc
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetList]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT     UName + '.' + CONVERT(varchar(20), CreatedDate, 101) + + ' ' + CONVERT(varchar(20), CreatedDate, 108) AS Name, Description from tbltestimonials order by id desc
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetList_Subscribe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetList_Subscribe]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetList_Subscribe] 
	-- Add the parameters for the stored procedure here
	@EMail nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT EMail from Subscribe where EMail=@EMail
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanApplication]
	-- Add the parameters for the stored procedure here

@LoanApplicationID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	SELECT     LoanApplicationID, LoanReasonID, HometypeID, Producttypeid, StateID, CreditScoreID, AnnulIncomeBorrower,LoanAmount, AssetBorrower,  
                RetirementAssetsBorrower, PropertyLocated, HomeOwned, PurchasePrise, DownPaymentAmt, PartOfTotalAssets, DownPaymentSourceID,   
                HaveRealtor, RealtorContactName, RealtorPhone, RealtorEmail, AppFName, AppLName,vAddressLine1,vAddressLine2, AppStateID, AppZip, AppPrimaryPhone, AppSecondaryPhone,   
                AppEmail, CurrentPropertyValue, PropertyPurchaseMonth, YearID, ExistingPuchasePrice, CurrentMortageBal, MonthlyPayment, SecondMortage,   
                SecondMortageBal, DateEntered, ModifiedBy, ModifiedDate, UserAppearUrl, PropertyTypeID, PropertyTypeName, ContactTime, ContactTimeID,
                Hometype, CreditScoreValue, StateName, LoanReason, ProductTypeName, AppStateName, Year, SourceOfdownPayment ,MAName,IsWorkingWithRealtor, PropertyRealtorCity,IsCashback,RealtorName,RealtorPhoneNo 
                FROM         (SELECT     tblLoanApplication.LoanApplicationID, tblLoanApplication.LoanReasonID, tblLoanApplication.HometypeID, tblLoanApplication.Producttypeid,   
                tblLoanApplication.StateID, tblLoanApplication.CreditScoreID, tblLoanApplication.AnnulIncomeBorrower, tblLoanApplication.LoanAmount, tblLoanApplication.AssetBorrower,   
                tblLoanApplication.RetirementAssetsBorrower, tblLoanApplication.PropertyLocated, tblLoanApplication.HomeOwned,   
                tblLoanApplication.PurchasePrise, tblLoanApplication.DownPaymentAmt, tblLoanApplication.PartOfTotalAssets,
                tblLoanApplication.DownPaymentSourceID, tblLoanApplication.HaveRealtor, tblLoanApplication.RealtorContactName,   
                tblLoanApplication.RealtorPhone, tblLoanApplication.RealtorEmail, tblLoanApplication.AppFName, tblLoanApplication.AppLName, tblLoanApplication.vAddressLine1, tblLoanApplication.vAddressLine2,  
                tblLoanApplication.AppStateID, tblLoanApplication.AppZip, tblLoanApplication.AppPrimaryPhone, tblLoanApplication.AppSecondaryPhone,   
                tblLoanApplication.AppEmail, tblLoanApplication.CurrentPropertyValue, tblLoanApplication.PropertyPurchaseMonth,   
                tblLoanApplication.YearID, tblLoanApplication.ExistingPuchasePrice, tblLoanApplication.CurrentMortageBal, 
                tblLoanApplication.MonthlyPayment, tblLoanApplication.SecondMortage, tblLoanApplication.SecondMortageBal,
                tblLoanApplication.DateEntered, tblLoanApplication.ModifiedBy, tblLoanApplication.ModifiedDate, tblLoanApplication.UserAppearUrl,   
                tblLoanApplication.PropertyTypeID, tblPropertyType.Name AS PropertyTypeName, tblContactTime.ContactTime,   
                tblLoanApplication.ContactTimeID, tblHomeType.Hometype, tblCreditScore.CreditScoreValue, tblStateNylex.StateName,   
                tblLoanReason.LoanReason, tblProductType.name AS ProductTypeName, tblStateNylex_1.StateName AS AppStateName, tblYear.Year,   
                tblDownPaymentSource.SourceOfdownPayment,tblLoanApplication.MAName,tblLoanApplication.IsWorkingWithRealtor, tblLoanApplication.PropertyRealtorCity,tblLoanApplication.IsCashback,tblLoanApplication.RealtorName,tblLoanApplication.RealtorPhoneNo   
                FROM          tblLoanApplication LEFT JOIN  
                tblPropertyType ON tblLoanApplication.PropertyTypeID = tblPropertyType.PropertyTypeID LEFT JOIN  
                tblContactTime ON tblLoanApplication.ContactTimeID = tblContactTime.ContactTimeID LEFT JOIN  
                tblHomeType ON tblLoanApplication.HometypeID = tblHomeType.HometypeID LEFT JOIN
                tblCreditScore ON tblLoanApplication.CreditScoreID = tblCreditScore.CreditScoreID LEFT JOIN  
                tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID LEFT JOIN  
                tblLoanReason ON tblLoanApplication.LoanReasonID = tblLoanReason.LoanReasonID LEFT JOIN  
                tblProductType ON tblLoanApplication.Producttypeid = tblProductType.producttypeid LEFT JOIN  
                tblStateNylex AS tblStateNylex_1 ON tblLoanApplication.AppStateID = tblStateNylex_1.StateID LEFT JOIN  
                tblYear ON tblLoanApplication.YearID = tblYear.YearID LEFT OUTER JOIN  
                tblDownPaymentSource ON tblLoanApplication.DownPaymentSourceID = tblDownPaymentSource.DownPaymentSourceID) AS App where loanApplicationID = @loanApplicationID
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanApplicationByAppID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanApplicationByAppID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanApplicationByAppID]
	-- Add the parameters for the stored procedure here

@LoanApplicationID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	
                            select *, tblPropertyType.Name AS PropertyTypeName, tblContactTime.ContactTime,    tblLoanApplication.ContactTimeID, tblHomeType.Hometype, tblCreditScore.CreditScoreValue, tblStateNylex.StateName,    tblLoanReason.LoanReason, tblProductType.name AS ProductTypeName, tblStateNylex_1.StateName AS AppStateName, tblYear.Year,    tblDownPaymentSource.SourceOfdownPayment from tblLoanApplication 
                LEFT JOIN   tblPropertyType ON tblLoanApplication.PropertyTypeID = tblPropertyType.PropertyTypeID 
                LEFT JOIN   tblContactTime ON tblLoanApplication.ContactTimeID = tblContactTime.ContactTimeID   
                LEFT JOIN   tblHomeType ON tblLoanApplication.HometypeID = tblHomeType.HometypeID 
                LEFT JOIN   tblCreditScore ON tblLoanApplication.CreditScoreID = tblCreditScore.CreditScoreID   
                LEFT JOIN   tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID   
                LEFT JOIN   tblLoanReason ON tblLoanApplication.LoanReasonID = tblLoanReason.LoanReasonID 
                LEFT JOIN   tblProductType ON tblLoanApplication.Producttypeid = tblProductType.producttypeid   
                LEFT JOIN   tblStateNylex AS tblStateNylex_1 ON tblLoanApplication.AppStateID = tblStateNylex_1.StateID    
                LEFT JOIN   tblYear ON tblLoanApplication.YearID = tblYear.YearID  
                LEFT OUTER JOIN   tblDownPaymentSource ON tblLoanApplication.DownPaymentSourceID = tblDownPaymentSource.DownPaymentSourceID 
                 where loanApplicationID=@loanApplicationID 

;

                select BI.*, LU.Description as HowLong ,LU1.Description as CoHowLong, ST.StateName + ' - ' + ST.Abbreviature as CoBowwowerState 
                from tblBorrowerinfo BI  
                left join tbllookups LU on BI.TimePeriod = LU.lookupid   
                left join tbllookups LU1 on BI.[Co-BorrowerTimePeriod] = LU1.lookupid 
                left join tblStateNylex ST on BI.[Co-BorrowerState]=ST.StateID
                where LU.Lookupname='TP' and LoanApplicationID = @LoanApplicationID
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanReason]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanReason]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanReason]
	-- Add the parameters for the stored procedure here
(
	@LoanReasonID int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT LoanReasonID=@LoanReasonID, LoanReason FROM tblLoanReason where LoanReasonID not in(1,2)
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanReasonNew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanReasonNew]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanReasonNew]
	-- Add the parameters for the stored procedure here
(
	@LoanReasonID int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT LoanReasonID= @LoanReasonID, LoanReason FROM tblLoanReason where loanReasonid  in(3,4)
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanReasonNewforUSDA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanReasonNewforUSDA]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanReasonNewforUSDA]
	-- Add the parameters for the stored procedure here
(
	@LoanReasonID int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT LoanReasonID=@LoanReasonID, LoanReason FROM tblLoanReason where loanReasonid in(3)
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetMAData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetMAData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetMAData]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select '--Select MA--' as uname ,0 as userid union select ufirstname + ' ' + ulastname as uname, userid from tblusers where (urole =2 or urole =3 or urole=10) and isactive=1 order by uname
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPaymentInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPaymentInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetPaymentInfo]
(
@loanid varchar(Max),
@TransactionDate varchar(Max)
)
AS
BEGIN

select  TransactionDate, TransactionAmt,description,EndPrincipalBal,InterestRate,
EscrowAmt,LateChargeAmt,OtherFundsAmt from history join TransactionCode as transcode on history.TransactionCode = transcode.Code
where loanid = @loanid and TransactionAmt > 0 
and (TransactionDate < @TransactionDate and TransactionDate > dateadd(year,-1,@TransactionDate))
order by history.TransactionDate desc
     
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPaymentInfo2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPaymentInfo2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetPaymentInfo2]
(
@loanid varchar(Max),
@TransactionDate varchar(Max)
)
AS
BEGIN

select TransactionDate, TransactionAmt,description,EndPrincipalBal,InterestRate,
EscrowAmt,LateChargeAmt,OtherFundsAmt from history join TransactionCode as transcode on history.TransactionCode = transcode.Code 
where loanid = @loanid and TransactionAmt > 0 
and history.TransactionDate <= @TransactionDate 
and (TransactionDate < getdate() and TransactionDate > dateadd(year,-1,getdate()))
order by history.TransactionDate desc
     
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPaymentInfo3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPaymentInfo3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetPaymentInfo3]
(
@loanid varchar(Max),
@TransactionDate varchar(Max)
)
AS
BEGIN

select TransactionDate, TransactionAmt,description,EndPrincipalBal,InterestRate,
EscrowAmt,LateChargeAmt,OtherFundsAmt from history join TransactionCode as transcode on history.TransactionCode = transcode.Code 
where loanid = @loanid and TransactionAmt > 0 
and (history.TransactionDate >= @TransactionDate and history.TransactionDate <=@TransactionDate)
order by history.TransactionDate desc 
     
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPayoffReasons]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPayoffReasons]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetPayoffReasons]

AS
BEGIN
select * from payoffreason
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetProductType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetProductType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductType]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT ID, producttypeid, name, term, isarm FROM  tblProductType WHERE id NOT IN (4)
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetProductType_clsloan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetProductType_clsloan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductType_clsloan]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ID, producttypeid, name, term, isarm FROM  tblProductType where id not in(3)

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPropertyType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPropertyType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetPropertyType]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT ID,PropertyTypeID,Name FROM tblPropertyType where id not in(4,5)
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetShortDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetShortDetails]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetShortDetails]
	-- Add the parameters for the stored procedure here

	
@LoanApplicationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM tblLoanApplication where LoanApplicationID=@LoanApplicationID

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetState]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  [dbo].[sp_GetState]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *  from tblState
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetState_clsloan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetState_clsloan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetState_clsloan]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT   ID, StateID, Abbreviature, StateName + ' - ' + Abbreviature as StatenameWithAbbr FROM tblStateNylex ORDER BY StatenameWithAbbr

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetState1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetState1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetState1]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT   ID, StateID, Abbreviature, StateName + ' - ' + Abbreviature as StatenameWithAbbr FROM tblStateNylex ORDER BY StateOrder
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStateAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStateAll]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetStateAll]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT   sID as StateID , StateName + ' - ' + StateAbbr as States FROM tblstate ORDER BY StateName
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStateAllNew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStateAllNew]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetStateAllNew]
	-- Add the parameters for the stored procedure here

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT   StateAbbr as StateID , StateName + ' - ' + StateAbbr as States FROM tblstate ORDER BY StateName

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStateID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStateID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetStateID]
	-- Add the parameters for the stored procedure here

	@Abbreviature nvarchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT StateID FROM tblStateNylex where Abbreviature=@Abbreviature

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStateIDNew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStateIDNew]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetStateIDNew]
	-- Add the parameters for the stored procedure here

	@Abbreviature nvarchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select stateid from tblStateNylex where Abbreviature = @Abbreviature

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStateName_tblState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStateName_tblState]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetStateName_tblState]
	-- Add the parameters for the stored procedure here

	@sid varchar(15)
	

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT StateName from tblState where SID=@sid
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetStatus]
	-- Add the parameters for the stored procedure here
	@id varchar(2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Status where id=@id
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUSDAProductType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUSDAProductType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUSDAProductType]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT top 1 ID, producttypeid, name, term, isarm FROM  tblProductType where id not in(3)

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUserName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUserName]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUserName]
	-- Add the parameters for the stored procedure here
(
	@uid varchar(5) ,
	@username varchar(20)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select UserName=@username from tbluser where uid=@uid
    -- Insert statements for procedure here
	
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUserName_tbluser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUserName_tbluser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUserName_tbluser]
	-- Add the parameters for the stored procedure here
	
@uid int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select UserName from tbluser where uid=@uid
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetYear]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetYear]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetYear]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	
                             DECLARE @max int 
                             set @max = DATEPART(year, getdate()) 
                             CREATE TABLE #temp (Year int) 
                             while @max >= 1909 
                             BEGIN 
                             insert #temp(Year) values(@max) 
                              set @max = @max - 1 
                              END 
                              SELECT Year from #temp 
                              drop table #temp 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert]
	-- Add the parameters for the stored procedure here
(
	@LoanReasonID int, 
@HometypeID int ,
@PropertyTypeID int, 
@Producttypeid int, 
@StateID int, 
@CreditScoreID int, 
@AnnulIncomeBorrower money,
@LoanAmount money, 
@AssetBorrower money, 
@RetirementAssetsBorrower money,
@MAName varchar(100),
@PropertyRealtorCity varchar(100),
@IsWorkingWithRealtor bit,
@IsCashback bit,
@RealtorName nvarchar(100),
@RealtorPhoneNo nvarchar(100) , 
@PropertyLocated bit, 
@HomeOwned bit, 
@PurchasePrise money, 
@DownPaymentAmt int, 
@PartOfTotalAssets bit,
 @DownPaymentSourceID int,
 @HaveRealtor bit,
 @RealtorContactName nvarchar(100),
 @RealtorPhone nvarchar(100) , 
@RealtorEmail nvarchar(100), 
@AppFName nvarchar(100), 
@AppLName nvarchar(100), 
@AppStateID int, 
@AppZip nvarchar(100), 
@AppPrimaryPhone nvarchar(100), 
@AppSecondaryPhone nvarchar(100), 
@AppEmail nvarchar(100), 
@ContactTimeID int, 
@CurrentPropertyValue money, 
@PropertyPurchaseMonth nvarchar(100), 
@YearID int,
 @ExistingPuchasePrice money,
 @CurrentMortageBal money, 
@MonthlyPayment money, 
@SecondMortage bit, 
@SecondMortageBal money,
@UserAppearUrl nvarchar(1000), 
@vCity varchar(100), 
@bSignAgg bit, 
@dSchClosingDate datetime, 
@vEstRenovationAmt nvarchar(100),
 @vDownPayAmount nvarchar(100), 
@vAddressLine1 nvarchar(500),
 @vAddressLine2 nvarchar(500), 
@ApplyType varchar(50) , 
@IsLead360 bit, 
@IsFullApp bit, 
@CompIP varchar(50)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
INSERT INTO tblLoanApplication

(
LoanReasonID , 
HometypeID  ,
PropertyTypeID , 
Producttypeid , 
StateID , 
CreditScoreID , 
AnnulIncomeBorrower,
LoanAmount , 
AssetBorrower , 
RetirementAssetsBorrower ,
MAName ,
PropertyRealtorCity ,
IsWorkingWithRealtor ,
IsCashback ,
RealtorName ,
RealtorPhoneNo , 
PropertyLocated , 
HomeOwned , 
PurchasePrise , 
DownPaymentAmt , 
PartOfTotalAssets,
 DownPaymentSourceID,
 HaveRealtor ,
 RealtorContactName ,
 RealtorPhone  , 
RealtorEmail , 
AppFName , 
AppLName , 
AppStateID , 
AppZip , 
AppPrimaryPhone , 
AppSecondaryPhone , 
AppEmail , 
ContactTimeID , 
CurrentPropertyValue , 
PropertyPurchaseMonth , 
YearID ,
 ExistingPuchasePrice ,
 CurrentMortageBal , 
MonthlyPayment , 
SecondMortage , 
SecondMortageBal ,
UserAppearUrl , 
vCity , 
bSignAgg , 
dSchClosingDate , 
vEstRenovationAmt ,
 vDownPayAmount , 
vAddressLine1 ,
 vAddressLine2 , 
ApplyType  , 
IsLead360 , 
IsFullApp , 
CompIP 
)
values
(
@LoanReasonID , 
@HometypeID  ,
@PropertyTypeID , 
@Producttypeid , 
@StateID , 
@CreditScoreID , 
@AnnulIncomeBorrower,
@LoanAmount , 
@AssetBorrower , 
@RetirementAssetsBorrower ,
@MAName ,
@PropertyRealtorCity ,
@IsWorkingWithRealtor ,
@IsCashback ,
@RealtorName ,
@RealtorPhoneNo , 
@PropertyLocated, 
@HomeOwned , 
@PurchasePrise , 
@DownPaymentAmt , 
@PartOfTotalAssets,
 @DownPaymentSourceID,
 @HaveRealtor ,
 @RealtorContactName ,
 @RealtorPhone  , 
@RealtorEmail , 
@AppFName , 
@AppLName , 
@AppStateID , 
@AppZip , 
@AppPrimaryPhone , 
@AppSecondaryPhone , 
@AppEmail , 
@ContactTimeID , 
@CurrentPropertyValue , 
@PropertyPurchaseMonth , 
@YearID ,
 @ExistingPuchasePrice ,
 @CurrentMortageBal , 
@MonthlyPayment , 
@SecondMortage , 
@SecondMortageBal ,
@UserAppearUrl , 
@vCity , 
@bSignAgg , 
@dSchClosingDate , 
@vEstRenovationAmt ,
 @vDownPayAmount , 
@vAddressLine1 ,
 @vAddressLine2 , 
@ApplyType  , 
@IsLead360 , 
@IsFullApp , 
@CompIP 
)
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_loanapp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_loanapp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert_loanapp]
	-- Add the parameters for the stored procedure here

(
@Fname varchar(45),
@Mname varchar(45),
@LName varchar(45),
@Email varchar(200),
@CEmail varchar(200),
@DOBM varchar(10),
@DOBDT int,
@DOBYR int,
@SSN varchar(45),
@Phone varchar(45),
@Suffix varchar(5),
@COFNAME varchar(45),
@COMNAME varchar(45),
@COLNAME varchar(45),
@COEMAIL varchar(200),
@COCEmail varchar(200),
@CODOBM varchar(10),
@CODOBDT int,
@CODOBYR int,
@COSSN varchar(45),
@COPHONE varchar(45),
@STADDR varchar(45),
@CITY varchar(45),
@MState int,
@ZipCode varchar(10),
@RSTATUS varchar(25),
@RentAmt varchar(100),
@LMHOLDER varchar(45),
@STADDR2 varchar(45),
@CITY2 varchar(45),
@STATE2 int,
@COSuffix varchar(45),
@ZipCode2 varchar(10),
@RLengthY2 int,
@RLengthM2 int,
@RSTATUS2 varchar(25),
@RentAmt2 varchar(100),
@LMHOLDER2 varchar(45),
@AEMPL varchar(100),
@AOCCU varchar(100),
@AEMPPHONE varchar(45),
@AGMI varchar(100),
@ALEMY int,
@ALEMM int,
@AEMPL2 varchar(100),
@AOCCU2 varchar(100),
@AEMPPHONE2 varchar(45),
@AGMI2 int,
@ALEMY2 int,
@ALEMM2 int,
@CAEMPL varchar(100),
@CAOCCU varchar(100),
@CAEMPPHONE varchar(45),
@CAGMI varchar(100),
@CALEMY int,
@CALEMM int,
@CAEMPL2 varchar(100),
@CAOCCU2 varchar(100),
@CAEMPPHONE2 varchar(45),
@CAGMI2 varchar(100),
@CALEMY2 int,
@CALEMM2 int,
@OtherComments varchar(1000),
@DigiSig varchar(100),
@SigDate varchar(15),
@IsAckno bit ,
@DateEntered datetime,
@RLengthM int,
@RlengthY int
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into tblloanapp(Fname,Mname,LName,Email,CEmail,DOBM,DOBDT,DOBYR,SSN,Phone,Suffix,COFNAME,COMNAME,COLNAME,COEMAIL,COCEmail,CODOBM,CODOBDT,CODOBYR,COSSN,COPHONE,STADDR,CITY,MState,ZipCode,RSTATUS,RentAmt,LMHOLDER,STADDR2,CITY2,STATE2,COSuffix,ZipCode2,RLengthY2,RLengthM2,RSTATUS2,RentAmt2,LMHOLDER2,AEMPL,AOCCU,AEMPPHONE,AGMI,ALEMY,ALEMM,AEMPL2,AOCCU2,AEMPPHONE2,AGMI2,ALEMY2,ALEMM2,CAEMPL,CAOCCU,CAEMPPHONE,CAGMI,CALEMY,CALEMM,CAEMPL2,CAOCCU2,CAEMPPHONE2,CAGMI2,CALEMY2,CALEMM2,OtherComments,DigiSig,SigDate,IsAckno,DateEntered,RLengthM,RlengthY)
 values(@Fname,@Mname,@LName,@Email,@CEmail,@DOBM,@DOBDT,@DOBYR,@SSN,@Phone,@Suffix,@COFNAME,@COMNAME,@COLNAME,@COEMAIL,@COCEmail,@CODOBM,@CODOBDT,@CODOBYR,@COSSN,@COPHONE,@STADDR,@CITY,@MState,@ZipCode,@RSTATUS,@RentAmt,@LMHOLDER,@STADDR2,@CITY2,@STATE2,@COSuffix,@ZipCode2,@RLengthY2,@RLengthM2,@RSTATUS2,@RentAmt2,@LMHOLDER2,@AEMPL,@AOCCU,@AEMPPHONE,@AGMI,@ALEMY,@ALEMM,@AEMPL2,@AOCCU2,@AEMPPHONE2,@AGMI2,@ALEMY2,@ALEMM2,@CAEMPL,@CAOCCU,@CAEMPPHONE,@CAGMI,@CALEMY,@CALEMM,@CAEMPL2,@CAOCCU2,@CAEMPPHONE2,@CAGMI2,@CALEMY2,@CALEMM2,@OtherComments,@DigiSig,@SigDate,@IsAckno,@DateEntered,@RLengthM,@RlengthY)
 

end

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_LoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_LoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert_LoanApplication]
	-- Add the parameters for the stored procedure here
(
	@LoanReasonID int, 
@HometypeID int ,
@PropertyTypeID int, 
@Producttypeid int, 
@StateID int, 
@CreditScoreID int, 
@AnnulIncomeBorrower money,
@LoanAmount money, 
@AssetBorrower money, 
@RetirementAssetsBorrower money,
@MAName varchar(100),
@PropertyRealtorCity varchar(100),
@IsWorkingWithRealtor bit,
@IsCashback bit,
@RealtorName nvarchar(100),
@RealtorPhoneNo nvarchar(100) , 
@PropertyLocated bit, 
@HomeOwned bit, 
@PurchasePrise money, 
@DownPaymentAmt int, 
@PartOfTotalAssets bit,
 @DownPaymentSourceID int,
 @HaveRealtor bit,
 @RealtorContactName nvarchar(100),
 @RealtorPhone nvarchar(100) , 
@RealtorEmail nvarchar(100), 
@AppFName nvarchar(100), 
@AppLName nvarchar(100), 
@AppStateID int, 
@AppZip nvarchar(100), 
@AppPrimaryPhone nvarchar(100), 
@AppSecondaryPhone nvarchar(100), 
@AppEmail nvarchar(100), 
@ContactTimeID int, 
@CurrentPropertyValue money, 
@PropertyPurchaseMonth nvarchar(100), 
@YearID int,
 @ExistingPuchasePrice money,
 @CurrentMortageBal money, 
@MonthlyPayment money, 
@SecondMortage bit, 
@SecondMortageBal money,
@UserAppearUrl nvarchar(1000), 
@vCity varchar(100), 
@bSignAgg bit, 
@dSchClosingDate datetime, 
@vEstRenovationAmt nvarchar(100),
 @vDownPayAmount nvarchar(100), 
@vAddressLine1 nvarchar(500),
 @vAddressLine2 nvarchar(500), 
@ApplyType varchar(50) , 
@IsLead360 bit, 

@CompIP varchar(50)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblLoanApplication

(
LoanReasonID , 
HometypeID  ,
PropertyTypeID , 
Producttypeid , 
StateID , 
CreditScoreID , 
AnnulIncomeBorrower,
LoanAmount , 
AssetBorrower , 
RetirementAssetsBorrower ,
MAName ,
PropertyRealtorCity ,
IsWorkingWithRealtor ,
IsCashback ,
RealtorName ,
RealtorPhoneNo , 
PropertyLocated , 
HomeOwned , 
PurchasePrise , 
DownPaymentAmt , 
PartOfTotalAssets,
 DownPaymentSourceID,
 HaveRealtor ,
 RealtorContactName ,
 RealtorPhone  , 
RealtorEmail , 
AppFName , 
AppLName , 
AppStateID , 
AppZip , 
AppPrimaryPhone , 
AppSecondaryPhone , 
AppEmail , 
ContactTimeID , 
CurrentPropertyValue , 
PropertyPurchaseMonth , 
YearID ,
 ExistingPuchasePrice ,
 CurrentMortageBal , 
MonthlyPayment , 
SecondMortage , 
SecondMortageBal ,
UserAppearUrl , 
vCity , 
bSignAgg , 
dSchClosingDate , 
vEstRenovationAmt ,
 vDownPayAmount , 
vAddressLine1 ,
 vAddressLine2 , 
ApplyType  , 
IsLead360 , 

CompIP 
)
values
(
@LoanReasonID , 
@HometypeID  ,
@PropertyTypeID , 
@Producttypeid , 
@StateID , 
@CreditScoreID , 
@AnnulIncomeBorrower,
@LoanAmount , 
@AssetBorrower , 
@RetirementAssetsBorrower ,
@MAName ,
@PropertyRealtorCity ,
@IsWorkingWithRealtor ,
@IsCashback ,
@RealtorName ,
@RealtorPhoneNo , 
@PropertyLocated, 
@HomeOwned , 
@PurchasePrise , 
@DownPaymentAmt , 
@PartOfTotalAssets,
 @DownPaymentSourceID,
 @HaveRealtor ,
 @RealtorContactName ,
 @RealtorPhone  , 
@RealtorEmail , 
@AppFName , 
@AppLName , 
@AppStateID , 
@AppZip , 
@AppPrimaryPhone , 
@AppSecondaryPhone , 
@AppEmail , 
@ContactTimeID , 
@CurrentPropertyValue , 
@PropertyPurchaseMonth , 
@YearID ,
 @ExistingPuchasePrice ,
 @CurrentMortageBal , 
@MonthlyPayment , 
@SecondMortage , 
@SecondMortageBal ,
@UserAppearUrl , 
@vCity , 
@bSignAgg , 
@dSchClosingDate , 
@vEstRenovationAmt ,
 @vDownPayAmount , 
@vAddressLine1 ,
 @vAddressLine2 , 
@ApplyType  , 
@IsLead360 , 
 
@CompIP 
)
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_Subscribe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_Subscribe]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert_Subscribe]
	-- Add the parameters for the stored procedure here
(
@Name nvarchar(100),
@EMail nvarchar(100),
@Comments nvarchar(2000)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Subscribe(Name,EMail,Comments) Values(@Name,@EMail,@Comments)

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_TESTIMONIALS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_TESTIMONIALS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert_TESTIMONIALS]
	-- Add the parameters for the stored procedure here
(
@Uname varchar(50),
@description varchar(50),
@createddate datetime
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
INSERT INTO TBLTESTIMONIALS(Uname,description,createddate) Values(@Uname,@description,@createddate)
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertAlert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertAlert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertAlert]
(

@UserId varchar (max),
@LoanNo varchar (max),
@IsSMSPaymentReminderAlert varchar (max),
@IsSMSPaymentPostedAlert varchar (max),
@IsSMSPaymentPastDue varchar (max),
@IsEmailPaymentReminderAlert varchar (max),
@IsEmailPaymentPostedAlert varchar (max),
@IsEmailPaymentPastDue varchar (max),
@IsPaymentReminderAlert varchar (max),
@IsPaymentPostedReminderAlert varchar (max),
@IsPaymentPastDue varchar (max),
@PaymentReminderDay varchar (max),
@PaymentPastDueReminderDay varchar (max)

)
AS
BEGIN
insert into tblServiceInfo (UserId,LoanNo,IsSMSPaymentReminderAlert,IsSMSPaymentPostedAlert,IsSMSPaymentPastDue,
IsEmailPaymentReminderAlert,IsEmailPaymentPostedAlert,IsEmailPaymentPastDue,IsPaymentReminderAlert,IsPaymentPostedReminderAlert,
IsPaymentPastDue,PaymentReminderDay,PaymentPastDueReminderDay) values 
(@UserId,@LoanNo,@IsSMSPaymentReminderAlert,@IsSMSPaymentPostedAlert,@IsSMSPaymentPastDue,@IsEmailPaymentReminderAlert,@IsEmailPaymentPostedAlert,@IsEmailPaymentPastDue,@IsPaymentReminderAlert,@IsPaymentPostedReminderAlert,@IsPaymentPastDue,@PaymentReminderDay,@PaymentPastDueReminderDay)
     
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLoanIdNew_LoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLoanIdNew_LoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertLoanIdNew_LoanApplication]
	-- Add the parameters for the stored procedure here
(
	@HometypeID int ,
			@PropertyTypeID int, 
			@StateID int, 
			@LoanReasonID int,

			@LoanAmount money, 
			@MAName varchar(100),
			@AppFName nvarchar(100), 
			@AppLName nvarchar(100), 
			@AppStateID int, 
			@AppZip nvarchar(100), 
			@AppPrimaryPhone nvarchar(100), 
			@AppEmail nvarchar(100), 
			@vCity varchar(100), 
 			@vDownPayAmount nvarchar(100), 
			@vAddressLine1 nvarchar(500),
			@AssignedLO varchar(250),
			@CompIP varchar(50)	
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblLoanApplication

(

	
			HometypeID ,
			PropertyTypeID ,
			StateID , 
			LoanReasonID ,

			LoanAmount , 
			MAName ,
			AppFName , 
			AppLName , 
			AppStateID , 
			AppZip , 
			AppPrimaryPhone , 
			AppEmail , 
			vCity , 
 			vDownPayAmount , 
			vAddressLine1 ,
			AssignedLO ,
			CompIP 
)
values
(

	
			@HometypeID ,
			@PropertyTypeID ,
			@StateID , 
			@LoanReasonID ,

			@LoanAmount , 
			@MAName ,
			@AppFName , 
			@AppLName , 
			@AppStateID , 
			@AppZip , 
			@AppPrimaryPhone , 
			@AppEmail , 
			@vCity , 
 			@vDownPayAmount , 
			@vAddressLine1 ,
			@AssignedLO ,
			@CompIP 
)
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertPayoffInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertPayoffInsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertPayoffInsert]
(
@userid varchar (max),
@payoffdate varchar (max),
@payoffreasonid varchar (max),
@isreadytocontact varchar (max)
)
AS
BEGIN
insert into tblpayoffstatement(userid,payoffdate,payoffreasonid,isreadytocontact)
values (@userid,@payoffdate,@payoffreasonid,@isreadytocontact)
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertPayoffUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertPayoffUpdate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertPayoffUpdate]
(
@userid varchar (max),
@payoffdate varchar (max),
@payoffreasonid varchar (max),
@isreadytocontact varchar (max)
)
AS
BEGIN
update tblpayoffstatement set payoffdate=@payoffdate,payoffreasonid=@payoffreasonid,isreadytocontact=@isreadytocontact where
userid=@userid

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IsRecordExists]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IsRecordExists]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_IsRecordExists]
(
@userid varchar (max)
)
AS
BEGIN
 select * from tblpayoffstatement where 
userid=@userid

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_LoanApplication] 
(
@LoanReasonID int ,
@HometypeID int,
@PropertyTypeID int,
@LoanAmount money ,
@vDownPayAmount varchar(100)	,
@StateID int ,
@AppFName nvarchar(100),
@AppLName nvarchar(100) ,
@AppEmail nvarchar(100),
@vAddressLine1 varchar(500),
@vCity varchar(100),
@AppStateID int,
@AppZip nvarchar(100),
@AppPrimaryPhone nvarchar(100),
@MAName varchar(100),
@AssignedLO varchar(250),
@CompIP varchar(50)
)
AS
BEGIN
       INSERT INTO tblLoanApplication(HometypeID,PropertyTypeID,LoanAmount,vDownPayAmount,LoanReasonID,StateID,AppFName,AppLName,AppEmail,vAddressLine1,vCity,AppStateID,AppZip,AppPrimaryPhone,MAName,AssignedLO, CompIP)      
 VALUES      
  (@HometypeID,@PropertyTypeID,@LoanAmount,@vDownPayAmount,@LoanReasonID,@StateID,@AppFName,@AppLName,@AppEmail,@vAddressLine1,@vCity,@AppStateID,@AppZip,@AppPrimaryPhone,@MAName,@AssignedLO,@CompIP)  
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_Loanapp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_Loanapp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Update_Loanapp]
	-- Add the parameters for the stored procedure here
	
(
@Fname varchar(45),
@Mname varchar(45),
@LName varchar(45),
@Email varchar(200),
@CEmail varchar(200),
@DOBM varchar(10),
@DOBDT int,
@DOBYR int,
@SSN varchar(45),
@Phone varchar(45),
@Suffix varchar(5),
@COFNAME varchar(45),
@COMNAME varchar(45),
@COLNAME varchar(45),
@COEMAIL varchar(200),
@COCEmail varchar(200),
@CODOBM varchar(10),
@CODOBDT int,
@CODOBYR int,
@COSSN varchar(45),
@COPHONE varchar(45),
@STADDR varchar(45),
@CITY varchar(45),
@MState int,
@ZipCode varchar(10),
@RSTATUS varchar(25),
@RentAmt varchar(100),
@LMHOLDER varchar(45),
@STADDR2 varchar(45),
@CITY2 varchar(45),
@STATE2 int,
@COSuffix varchar(45),
@ZipCode2 varchar(10),
@RLengthY2 int,
@RLengthM2 int,
@RSTATUS2 varchar(25),
@RentAmt2 varchar(100),
@LMHOLDER2 varchar(45),
@AEMPL varchar(100),
@AOCCU varchar(100),
@AEMPPHONE varchar(45),
@AGMI varchar(100),
@ALEMY int,
@ALEMM int,
@AEMPL2 varchar(100),
@AOCCU2 varchar(100),
@AEMPPHONE2 varchar(45),
@AGMI2 int,
@ALEMY2 int,
@ALEMM2 int,
@CAEMPL varchar(100),
@CAOCCU varchar(100),
@CAEMPPHONE varchar(45),
@CAGMI varchar(100),
@CALEMY int,
@CALEMM int,
@CAEMPL2 varchar(100),
@CAOCCU2 varchar(100),
@CAEMPPHONE2 varchar(45),
@CAGMI2 varchar(100),
@CALEMY2 int,
@CALEMM2 int,
@OtherComments varchar(1000),
@DigiSig varchar(100),
@SigDate varchar(15),
@IsAckno bit ,
@DateEntered datetime,
@RLengthM int,
@RlengthY int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

Update TBLLOANAPP set 
Fname=@Fname,Mname=@Mname,LName=@LName,Email=@Email,CEmail=@CEmail,DOBM=@DOBM,DOBDT=@DOBDT,DOBYR=@DOBYR,SSN=@SSN,Phone=@Phone,Suffix=@Suffix,COFNAME=@COFNAME,COMNAME=@COMNAME,COLNAME=@COLNAME,COEMAIL=@COEMAIL,COCEmail=@COCEmail,CODOBM=@CODOBM,CODOBDT=@CODOBDT,CODOBYR=@CODOBYR,COSSN=@COSSN,COPHONE=@COPHONE,STADDR=@STADDR,CITY=@CITY,MState=@MState,ZipCode=@ZipCode,RSTATUS=@RSTATUS,RentAmt=@RentAmt,LMHOLDER=@LMHOLDER,STADDR2=@STADDR2,CITY2=@CITY2,STATE2=@STATE2,COSuffix=@COSuffix,ZipCode2=@ZipCode2,RLengthY2=@RLengthY2,RLengthM2=@RLengthM2,RSTATUS2=@RSTATUS2,RentAmt2=@RentAmt2,LMHOLDER2=@LMHOLDER2,AEMPL=@AEMPL,AOCCU=@AOCCU,AEMPPHONE=@AEMPPHONE,AGMI=@AGMI,ALEMY=@ALEMY,ALEMM=@ALEMM,AEMPL2=@AEMPL2,AOCCU2=@AOCCU2,AEMPPHONE2=@AEMPPHONE2,AGMI2=@AGMI2,ALEMY2=@ALEMY2,ALEMM2=@ALEMM2,CAEMPL=@CAEMPL,CAOCCU=@CAOCCU,CAEMPPHONE=@CAEMPPHONE,CAGMI=@CAGMI,CALEMY=@CALEMY,CALEMM=@CALEMM,CAEMPL2=@CAEMPL2,CAOCCU2=@CAOCCU2,CAEMPPHONE2=@CAEMPPHONE2,CAGMI2=@CAGMI2,CALEMY2=@CALEMY2,CALEMM2=@CALEMM2,OtherComments=@OtherComments,DigiSig=@DigiSig,SigDate=@SigDate,IsAckno=@IsAckno,DateEntered=@DateEntered,RLengthM=@RLengthM,RlengthY=@RlengthY
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_LoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_LoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Update_LoanApplication]
	-- Add the parameters for the stored procedure here
	(
	@LoanReasonID int, 
@HometypeID int ,
@PropertyTypeID int, 
@Producttypeid int, 
@StateID int, 
@CreditScoreID int, 
@AnnulIncomeBorrower money,
@LoanAmount money, 
@AssetBorrower money, 
@RetirementAssetsBorrower money,
@MAName varchar(100),
@PropertyRealtorCity varchar(100),
@IsWorkingWithRealtor bit,
@IsCashback bit,
@RealtorName nvarchar(100),
@RealtorPhoneNo nvarchar(100) , 
@PropertyLocated bit, 
@HomeOwned bit, 
@PurchasePrise money, 
@DownPaymentAmt int, 
@PartOfTotalAssets bit,
 @DownPaymentSourceID int,
 @HaveRealtor bit,
 @RealtorContactName nvarchar(100),
 @RealtorPhone nvarchar(100) , 
@RealtorEmail nvarchar(100), 
@AppFName nvarchar(100), 
@AppLName nvarchar(100), 
@AppStateID int, 
@AppZip nvarchar(100), 
@AppPrimaryPhone nvarchar(100), 
@AppSecondaryPhone nvarchar(100), 
@AppEmail nvarchar(100), 
@ContactTimeID int, 
@CurrentPropertyValue money, 
@PropertyPurchaseMonth nvarchar(100), 
@YearID int,
 @vExistingPuchasePrice money,
 @CurrentMortageBal money, 
@MonthlyPayment money, 
@SecondMortage bit, 
@SecondMortageBal money,
@UserAppearUrl nvarchar(1000), 
@vCity varchar(100), 
@bSignAgg bit, 
@dSchClosingDate datetime, 
@vEstRenovationAmt nvarchar(100),
 @vDownPayAmount nvarchar(100), 
@vAddressLine1 nvarchar(500),
 @vAddressLine2 nvarchar(500), 
@ApplyType varchar(50) , 
@IsLead360 bit, 
@IsFullApp bit, 
@CompIP varchar(50),
@loanapplicationid int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update tblLoanApplication set 
LoanReasonID=@LoanReasonID , 
HometypeID=@HometypeID  ,
PropertyTypeID=@PropertyTypeID , 
Producttypeid=@Producttypeid , 
StateID=@StateID , 
CreditScoreID=@CreditScoreID , 
AnnulIncomeBorrower=@AnnulIncomeBorrower,
LoanAmount=@LoanAmount , 
AssetBorrower=@AssetBorrower , 
RetirementAssetsBorrower=@RetirementAssetsBorrower ,
MAName=@MAName ,
PropertyRealtorCity=@PropertyRealtorCity ,
IsWorkingWithRealtor=@IsWorkingWithRealtor ,
IsCashback=@IsCashback ,
RealtorName=@RealtorName ,
RealtorPhoneNo=@RealtorPhoneNo , 
PropertyLocated=@PropertyLocated , 
HomeOwned=@HomeOwned , 
PurchasePrise=@PurchasePrise , 
DownPaymentAmt=@DownPaymentAmt , 
PartOfTotalAssets=@PartOfTotalAssets,
 DownPaymentSourceID=@DownPaymentSourceID,
 HaveRealtor=@HaveRealtor ,
 RealtorContactName=@RealtorContactName ,
 RealtorPhone=@RealtorPhone  , 
RealtorEmail=@RealtorEmail , 
AppFName=@AppFName , 
AppLName=@AppLName , 
AppStateID=@AppStateID , 
AppZip=@AppZip , 
AppPrimaryPhone=@AppPrimaryPhone , 
AppSecondaryPhone=@AppSecondaryPhone , 
AppEmail=@AppEmail , 
ContactTimeID=@ContactTimeID , 
CurrentPropertyValue=@CurrentPropertyValue , 
PropertyPurchaseMonth=@PropertyPurchaseMonth , 
YearID=@YearID ,
 ExistingPuchasePrice=ExistingPuchasePrice ,
 CurrentMortageBal=CurrentMortageBal , 
MonthlyPayment=@MonthlyPayment , 
SecondMortage=@SecondMortage , 
SecondMortageBal=@SecondMortageBal ,
UserAppearUrl=@UserAppearUrl , 
vCity=@vCity , 
bSignAgg=@bSignAgg , 
dSchClosingDate=@dSchClosingDate , 
vEstRenovationAmt=@vEstRenovationAmt ,
 vDownPayAmount=@vDownPayAmount , 
vAddressLine1=@vAddressLine1 ,
 vAddressLine2=@vAddressLine2 , 
ApplyType=@ApplyType  , 
IsLead360=@IsLead360 , 
IsFullApp=@IsFullApp , 
CompIP=@CompIP 
where loanapplicationid=@loanapplicationid       
              
                 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_Subscribe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_Subscribe]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Update_Subscribe]
	-- Add the parameters for the stored procedure here
	@Email nvarchar(100)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Subscribe 
set IsActive = 0, UpdatedDate=GetDate()Where EMail = @Email 
               
              
                 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateAlert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateAlert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_UpdateAlert]
(
 @IsSMSPaymentReminderAlert varchar (max),
 @IsSMSPaymentPostedAlert varchar (max),
 @IsSMSPaymentPastDue varchar (max),
 @IsEmailPaymentReminderAlert varchar (max),
 @IsEmailPaymentPostedAlert  varchar (max),
 @IsEmailPaymentPastDue  varchar (max),
 @IsPaymentReminderAlert  varchar (max),
 @IsPaymentPostedReminderAlert  varchar (max),
 @IsPaymentPastDue varchar (max),
 @PaymentReminderDay varchar (max),
 @PaymentPastDueReminderDay varchar (max),
 @record_id  varchar (max)
)
AS
BEGIN
update tblServiceInfo set 

 IsSMSPaymentReminderAlert=@IsSMSPaymentReminderAlert,
 IsSMSPaymentPostedAlert=@IsSMSPaymentPostedAlert,
 IsSMSPaymentPastDue=@IsSMSPaymentPastDue,
 IsEmailPaymentReminderAlert=@IsEmailPaymentReminderAlert,
 IsEmailPaymentPostedAlert =@IsEmailPaymentPostedAlert,
 IsEmailPaymentPastDue =@IsEmailPaymentPastDue,
 IsPaymentReminderAlert =@IsPaymentReminderAlert,
 IsPaymentPostedReminderAlert =@IsPaymentPostedReminderAlert,
 IsPaymentPastDue=@IsPaymentPastDue,
 PaymentReminderDay=@PaymentReminderDay,
 PaymentPastDueReminderDay=@PaymentPastDueReminderDay,
 record_id =@record_id
     
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateLong]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateLong]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateLong]
	-- Add the parameters for the stored procedure here
(
				@loanapplicationid int,
				@HometypeID int,
                @PropertyTypeID int,
                @LoanAmount money,
                @LoanReasonID int,
                @StateID int,
                @CurrentPropertyValue money,
                @YearID int,
                @CurrentMortageBal money,
                @MonthlyPayment money,
                @SecondMortage bit,
                @SecondMortageBal money,
                @ExistingPuchasePrice money,
                @vAddressLine2 varchar(500),
                @PurchasePrise money,
                @AppFName nvarchar(100),
                @AppLName nvarchar(100),
                @AppEmail nvarchar(100),
               
                @vAddressLine1 varchar(500),
                @vCity varchar(100),
                @AppStateID int,
                @AppZip nvarchar(100),
                @AppPrimaryPhone nvarchar(100),
                @AppSecondaryPhone nvarchar(100),
                @ContactTimeID int
				
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
update tblLoanApplication set

                HometypeID=@HometypeID,
                PropertyTypeID=@PropertyTypeID,
                LoanAmount=@LoanAmount,
                LoanReasonID=@LoanReasonID,
                StateID=@StateID,
                CurrentPropertyValue=@CurrentPropertyValue,
                YearID=@YearID,
                CurrentMortageBal=@CurrentMortageBal,
                MonthlyPayment=@MonthlyPayment,
                SecondMortage=@SecondMortage,
                SecondMortageBal=@SecondMortageBal,
                ExistingPuchasePrice=@ExistingPuchasePrice,
                vAddressLine2=@vAddressLine2,
                PurchasePrise=@PurchasePrise,
                AppFName=@AppFName,
                AppLName=@AppLName,
                AppEmail=@AppEmail,
           
                vAddressLine1=@vAddressLine1,
                vCity=@vCity,
                AppStateID=@AppStateID,
                AppZip=@AppZip,
                AppPrimaryPhone=@AppPrimaryPhone,
                AppSecondaryPhone=@AppSecondaryPhone,
                ContactTimeID=@ContactTimeID

		where	loanapplicationid=@loanapplicationid 


END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateShortLoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateShortLoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateShortLoanApplication]
	-- Add the parameters for the stored procedure here
	@LoanApplicationID varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update tblLoanApplication set IsLead360=1, IsFullApp=1 WHERE LoanApplicationID =@LoanApplicationID
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateShortLoanApplication1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateShortLoanApplication1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateShortLoanApplication1]
	-- Add the parameters for the stored procedure here
	@LoanApplicationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update tblLoanApplication set IsLead360=1, IsFullApp=1 WHERE LoanApplicationID=@LoanApplicationID
END


GO

USE [msdb]
GO

/****** Object:  Job [Integrity Check]    Script Date: 01/26/2015 13:38:45 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 01/26/2015 13:38:45 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Integrity Check', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'STONEGATEMTG\tgrant', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Intergrity Check]    Script Date: 01/26/2015 13:38:45 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Intergrity Check', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC sp_msforeachdb ''DBCC CHECKDB(''''?'''')''', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Rebuild Indexes]    Script Date: 01/26/2015 13:38:45 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Rebuild Indexes', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @Database VARCHAR(255)   
DECLARE @Table VARCHAR(255)  
DECLARE @cmd NVARCHAR(500)  
DECLARE @fillfactor INT 

SET @fillfactor = 90 
SET QUOTED_IDENTIFIER ON

DECLARE DatabaseCursor CURSOR FOR  
SELECT name FROM master.dbo.sysdatabases   
WHERE name NOT IN (''tempdb'')   
ORDER BY 1  

OPEN DatabaseCursor  

FETCH NEXT FROM DatabaseCursor INTO @Database  
WHILE @@FETCH_STATUS = 0  
BEGIN  

   SET @cmd = ''DECLARE TableCursor CURSOR FOR SELECT ''''['''' + table_catalog + ''''].['''' + table_schema + ''''].['''' + 
  table_name + '''']'''' as tableName FROM ['' + @Database + ''].INFORMATION_SCHEMA.TABLES 
  WHERE table_type = ''''BASE TABLE''''''   

   -- create table cursor  
   EXEC (@cmd)  
   OPEN TableCursor   

   FETCH NEXT FROM TableCursor INTO @Table   
   WHILE @@FETCH_STATUS = 0   
   BEGIN   

       IF (@@MICROSOFTVERSION / POWER(2, 24) >= 9)
       BEGIN
           -- SQL 2005 or higher command 
           SET @cmd = ''ALTER INDEX ALL ON '' + @Table + '' REBUILD WITH (FILLFACTOR = '' + CONVERT(VARCHAR(3),@fillfactor) + '')'' 
           EXEC (@cmd) 
       END
       ELSE
       BEGIN
          -- SQL 2000 command 
          DBCC DBREINDEX(@Table,'' '',@fillfactor)  
       END

       FETCH NEXT FROM TableCursor INTO @Table   
   END   

   CLOSE TableCursor   
   DEALLOCATE TableCursor  

   FETCH NEXT FROM DatabaseCursor INTO @Database  
END  
CLOSE DatabaseCursor   
DEALLOCATE DatabaseCursor', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Integrity Check Schedule', 
		@enabled=0, 
		@freq_type=1, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20140417, 
		@active_end_date=99991231, 
		@active_start_time=223000, 
		@active_end_time=235959, 
		@schedule_uid=N'41b89723-4547-45a8-ad91-d8763c87a2af'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

USE [msdb]
GO

/****** Object:  Job [Saturday Backup and Maintenance]    Script Date: 01/26/2015 13:41:14 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 01/26/2015 13:41:14 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Saturday Backup and Maintenance', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'STONEGATEMTG\Svcdevportaldb01', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Integrity Check]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Integrity Check', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC sp_msforeachdb ''DBCC CHECKDB(''''?'''')''', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Rebuild Indexes]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Rebuild Indexes', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @Database VARCHAR(255)   
DECLARE @Table VARCHAR(255)  
DECLARE @cmd NVARCHAR(500)  
DECLARE @fillfactor INT 

SET @fillfactor = 90 
SET QUOTED_IDENTIFIER ON

DECLARE DatabaseCursor CURSOR FOR  
SELECT name FROM master.dbo.sysdatabases   
WHERE name IN (''master'',''msdb'',''model'',''ABD-Management'',''HomeLendingExperts'',''LiveE3ProWH'',''StonegateMtgLive'')   
ORDER BY 1  

OPEN DatabaseCursor  

FETCH NEXT FROM DatabaseCursor INTO @Database  
WHILE @@FETCH_STATUS = 0  
BEGIN  

   SET @cmd = ''DECLARE TableCursor CURSOR FOR SELECT ''''['''' + table_catalog + ''''].['''' + table_schema + ''''].['''' + 
  table_name + '''']'''' as tableName FROM ['' + @Database + ''].INFORMATION_SCHEMA.TABLES 
  WHERE table_type = ''''BASE TABLE''''''   

   -- create table cursor  
   EXEC (@cmd)  
   OPEN TableCursor   

   FETCH NEXT FROM TableCursor INTO @Table   
   WHILE @@FETCH_STATUS = 0   
   BEGIN   

       IF (@@MICROSOFTVERSION / POWER(2, 24) >= 9)
       BEGIN
           -- SQL 2005 or higher command 
           SET @cmd = ''ALTER INDEX ALL ON '' + @Table + '' REBUILD WITH (FILLFACTOR = '' + CONVERT(VARCHAR(3),@fillfactor) + '')'' 
           EXEC (@cmd) 
       END
       ELSE
       BEGIN
          -- SQL 2000 command 
          DBCC DBREINDEX(@Table,'' '',@fillfactor)  
       END

       FETCH NEXT FROM TableCursor INTO @Table   
   END   

   CLOSE TableCursor   
   DEALLOCATE TableCursor  

   FETCH NEXT FROM DatabaseCursor INTO @Database  
END  
CLOSE DatabaseCursor   
DEALLOCATE DatabaseCursor', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Backup Database]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Backup Database', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.proc_BackupAnyDB
@DBName = ''master'', 
@Location = ''D:\SQLBackup\master\'', 
@FileName = ''master.bak'', 
@Name = ''master Backup'', --Name of the backup run
@Description = ''Weekly Backup'',
@BackupType = ''F'', --F = Full, D = Diff, T = Tran Log
@Reset = 1 -- 1 = Yes, 0 = No

GO

EXEC dbo.proc_BackupAnyDB
@DBName = ''model'', 
@Location = ''D:\SQLBackup\model\'', 
@FileName = ''model.bak'', 
@Name = ''model Backup'', --Name of the backup run
@Description = ''Weekly Backup'',
@BackupType = ''F'', --F = Full, D = Diff, T = Tran Log
@Reset = 1 -- 1 = Yes, 0 = No

GO

EXEC dbo.proc_BackupAnyDB
@DBName = ''msdb'', 
@Location = ''D:\SQLBackup\msdb\'', 
@FileName = ''msdb.bak'', 
@Name = ''msdb Backup'', --Name of the backup run
@Description = ''Weekly Backup'',
@BackupType = ''F'', --F = Full, D = Diff, T = Tran Log
@Reset = 1 -- 1 = Yes, 0 = No

GO', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Reset Backup Date]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Reset Backup Date', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'Declare @Date  varchar(100);
Declare @Sql    varchar(MAX);
Declare @TranSql    varchar(MAX);
declare @PathToObject varchar(100)
declare @OldFullFileName varchar(100), @NewFullFileName varchar(100)
declare @OldTranFileName varchar(100), @NewTranFileName varchar(100)
declare @DeleteFullFile varchar(100), @DeleteTranFile varchar(100)
declare @DeleteDate varchar(100)

set @PathToObject = ''D:\SQLBackup\master''
set @OldFullFileName = ''master.bak''

SELECT @Date= CONVERT(VARCHAR(10), GETDATE(), 112);
SELECT @DeleteDate= CONVERT(VARCHAR(10), GETDATE()-1, 112);

set @NewFullFileName = @Date + @OldFullFileName 

exec dbo.proc_ReplaceFileOrDirNames @pathToObject, @OldFullFileName, @NewFullFileName
Go

Declare @Date  varchar(100);
Declare @Sql    varchar(MAX);
Declare @TranSql    varchar(MAX);
declare @PathToObject varchar(100)
declare @OldFullFileName varchar(100), @NewFullFileName varchar(100)
declare @OldTranFileName varchar(100), @NewTranFileName varchar(100)
declare @DeleteFullFile varchar(100), @DeleteTranFile varchar(100)
declare @DeleteDate varchar(100)

set @PathToObject = ''D:\SQLBackup\model''
set @OldFullFileName = ''model.bak''

SELECT @Date= CONVERT(VARCHAR(10), GETDATE(), 112);
SELECT @DeleteDate= CONVERT(VARCHAR(10), GETDATE()-1, 112);

set @NewFullFileName = @Date + @OldFullFileName 

exec dbo.proc_ReplaceFileOrDirNames @pathToObject, @OldFullFileName, @NewFullFileName
Go

Declare @Date  varchar(100);
Declare @Sql    varchar(MAX);
Declare @TranSql    varchar(MAX);
declare @PathToObject varchar(100)
declare @OldFullFileName varchar(100), @NewFullFileName varchar(100)
declare @OldTranFileName varchar(100), @NewTranFileName varchar(100)
declare @DeleteFullFile varchar(100), @DeleteTranFile varchar(100)
declare @DeleteDate varchar(100)

set @PathToObject = ''D:\SQLBackup\msdb''
set @OldFullFileName = ''msdb.bak''

SELECT @Date= CONVERT(VARCHAR(10), GETDATE(), 112);
SELECT @DeleteDate= CONVERT(VARCHAR(10), GETDATE()-1, 112);

set @NewFullFileName = @Date + @OldFullFileName 

exec dbo.proc_ReplaceFileOrDirNames @pathToObject, @OldFullFileName, @NewFullFileName
Go', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Cleanup Backup Files]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Cleanup Backup Files', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'PowerShell', 
		@command=N'Dir D:\SQLBackup\master\* -recurse | where {$_.LastWriteTime 僕t (get-date).AddDays(-35) } | % {del $_.FullName};
Dir D:\SQLBackup\model\* -recurse | where {$_.LastWriteTime 僕t (get-date).AddDays(-35) } | % {del $_.FullName};
Dir D:\SQLBackup\msdb\* -recurse | where {$_.LastWriteTime 僕t (get-date).AddDays(-35) } | % {del $_.FullName}', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Cleanup Backup History]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Cleanup Backup History', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE msdb
 GO
 DECLARE @DaysToKeepHistory DATETIME
 SET @DaysToKeepHistory = CONVERT(VARCHAR(10), DATEADD(dd, -7, GETDATE()), 101)
 EXEC sp_delete_backuphistory @DaysToKeepHistory
 GO', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Cleanup SQL Agent History]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Cleanup SQL Agent History', 
		@step_id=7, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE msdb
 GO
 DECLARE @DaysToKeepHistory DATETIME
 SET @DaysToKeepHistory = CONVERT(VARCHAR(10), DATEADD(dd, -120, GETDATE()), 101)
 EXEC sp_purge_jobhistory @job_name = N''Saturday Backup and Maintenance'', @oldest_date = @DaysToKeepHistory
 GO', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Saturday Backup and Maintenance', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=64, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20140122, 
		@active_end_date=99991231, 
		@active_start_time=210000, 
		@active_end_time=235959, 
		@schedule_uid=N'98363a8e-3f5d-44b5-ba6d-fa69f3cff2cf'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


USE [msdb]
GO

/****** Object:  Job [Saturday Backup and Maintenance]    Script Date: 01/26/2015 13:41:14 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 01/26/2015 13:41:14 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Saturday Backup and Maintenance', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'STONEGATEMTG\Svcdevportaldb01', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Integrity Check]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Integrity Check', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC sp_msforeachdb ''DBCC CHECKDB(''''?'''')''', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Rebuild Indexes]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Rebuild Indexes', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @Database VARCHAR(255)   
DECLARE @Table VARCHAR(255)  
DECLARE @cmd NVARCHAR(500)  
DECLARE @fillfactor INT 

SET @fillfactor = 90 
SET QUOTED_IDENTIFIER ON

DECLARE DatabaseCursor CURSOR FOR  
SELECT name FROM master.dbo.sysdatabases   
WHERE name IN (''master'',''msdb'',''model'',''ABD-Management'',''HomeLendingExperts'',''LiveE3ProWH'',''StonegateMtgLive'')   
ORDER BY 1  

OPEN DatabaseCursor  

FETCH NEXT FROM DatabaseCursor INTO @Database  
WHILE @@FETCH_STATUS = 0  
BEGIN  

   SET @cmd = ''DECLARE TableCursor CURSOR FOR SELECT ''''['''' + table_catalog + ''''].['''' + table_schema + ''''].['''' + 
  table_name + '''']'''' as tableName FROM ['' + @Database + ''].INFORMATION_SCHEMA.TABLES 
  WHERE table_type = ''''BASE TABLE''''''   

   -- create table cursor  
   EXEC (@cmd)  
   OPEN TableCursor   

   FETCH NEXT FROM TableCursor INTO @Table   
   WHILE @@FETCH_STATUS = 0   
   BEGIN   

       IF (@@MICROSOFTVERSION / POWER(2, 24) >= 9)
       BEGIN
           -- SQL 2005 or higher command 
           SET @cmd = ''ALTER INDEX ALL ON '' + @Table + '' REBUILD WITH (FILLFACTOR = '' + CONVERT(VARCHAR(3),@fillfactor) + '')'' 
           EXEC (@cmd) 
       END
       ELSE
       BEGIN
          -- SQL 2000 command 
          DBCC DBREINDEX(@Table,'' '',@fillfactor)  
       END

       FETCH NEXT FROM TableCursor INTO @Table   
   END   

   CLOSE TableCursor   
   DEALLOCATE TableCursor  

   FETCH NEXT FROM DatabaseCursor INTO @Database  
END  
CLOSE DatabaseCursor   
DEALLOCATE DatabaseCursor', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Backup Database]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Backup Database', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.proc_BackupAnyDB
@DBName = ''master'', 
@Location = ''D:\SQLBackup\master\'', 
@FileName = ''master.bak'', 
@Name = ''master Backup'', --Name of the backup run
@Description = ''Weekly Backup'',
@BackupType = ''F'', --F = Full, D = Diff, T = Tran Log
@Reset = 1 -- 1 = Yes, 0 = No

GO

EXEC dbo.proc_BackupAnyDB
@DBName = ''model'', 
@Location = ''D:\SQLBackup\model\'', 
@FileName = ''model.bak'', 
@Name = ''model Backup'', --Name of the backup run
@Description = ''Weekly Backup'',
@BackupType = ''F'', --F = Full, D = Diff, T = Tran Log
@Reset = 1 -- 1 = Yes, 0 = No

GO

EXEC dbo.proc_BackupAnyDB
@DBName = ''msdb'', 
@Location = ''D:\SQLBackup\msdb\'', 
@FileName = ''msdb.bak'', 
@Name = ''msdb Backup'', --Name of the backup run
@Description = ''Weekly Backup'',
@BackupType = ''F'', --F = Full, D = Diff, T = Tran Log
@Reset = 1 -- 1 = Yes, 0 = No

GO', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Reset Backup Date]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Reset Backup Date', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'Declare @Date  varchar(100);
Declare @Sql    varchar(MAX);
Declare @TranSql    varchar(MAX);
declare @PathToObject varchar(100)
declare @OldFullFileName varchar(100), @NewFullFileName varchar(100)
declare @OldTranFileName varchar(100), @NewTranFileName varchar(100)
declare @DeleteFullFile varchar(100), @DeleteTranFile varchar(100)
declare @DeleteDate varchar(100)

set @PathToObject = ''D:\SQLBackup\master''
set @OldFullFileName = ''master.bak''

SELECT @Date= CONVERT(VARCHAR(10), GETDATE(), 112);
SELECT @DeleteDate= CONVERT(VARCHAR(10), GETDATE()-1, 112);

set @NewFullFileName = @Date + @OldFullFileName 

exec dbo.proc_ReplaceFileOrDirNames @pathToObject, @OldFullFileName, @NewFullFileName
Go

Declare @Date  varchar(100);
Declare @Sql    varchar(MAX);
Declare @TranSql    varchar(MAX);
declare @PathToObject varchar(100)
declare @OldFullFileName varchar(100), @NewFullFileName varchar(100)
declare @OldTranFileName varchar(100), @NewTranFileName varchar(100)
declare @DeleteFullFile varchar(100), @DeleteTranFile varchar(100)
declare @DeleteDate varchar(100)

set @PathToObject = ''D:\SQLBackup\model''
set @OldFullFileName = ''model.bak''

SELECT @Date= CONVERT(VARCHAR(10), GETDATE(), 112);
SELECT @DeleteDate= CONVERT(VARCHAR(10), GETDATE()-1, 112);

set @NewFullFileName = @Date + @OldFullFileName 

exec dbo.proc_ReplaceFileOrDirNames @pathToObject, @OldFullFileName, @NewFullFileName
Go

Declare @Date  varchar(100);
Declare @Sql    varchar(MAX);
Declare @TranSql    varchar(MAX);
declare @PathToObject varchar(100)
declare @OldFullFileName varchar(100), @NewFullFileName varchar(100)
declare @OldTranFileName varchar(100), @NewTranFileName varchar(100)
declare @DeleteFullFile varchar(100), @DeleteTranFile varchar(100)
declare @DeleteDate varchar(100)

set @PathToObject = ''D:\SQLBackup\msdb''
set @OldFullFileName = ''msdb.bak''

SELECT @Date= CONVERT(VARCHAR(10), GETDATE(), 112);
SELECT @DeleteDate= CONVERT(VARCHAR(10), GETDATE()-1, 112);

set @NewFullFileName = @Date + @OldFullFileName 

exec dbo.proc_ReplaceFileOrDirNames @pathToObject, @OldFullFileName, @NewFullFileName
Go', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Cleanup Backup Files]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Cleanup Backup Files', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'PowerShell', 
		@command=N'Dir D:\SQLBackup\master\* -recurse | where {$_.LastWriteTime 僕t (get-date).AddDays(-35) } | % {del $_.FullName};
Dir D:\SQLBackup\model\* -recurse | where {$_.LastWriteTime 僕t (get-date).AddDays(-35) } | % {del $_.FullName};
Dir D:\SQLBackup\msdb\* -recurse | where {$_.LastWriteTime 僕t (get-date).AddDays(-35) } | % {del $_.FullName}', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Cleanup Backup History]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Cleanup Backup History', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE msdb
 GO
 DECLARE @DaysToKeepHistory DATETIME
 SET @DaysToKeepHistory = CONVERT(VARCHAR(10), DATEADD(dd, -7, GETDATE()), 101)
 EXEC sp_delete_backuphistory @DaysToKeepHistory
 GO', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Cleanup SQL Agent History]    Script Date: 01/26/2015 13:41:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Cleanup SQL Agent History', 
		@step_id=7, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE msdb
 GO
 DECLARE @DaysToKeepHistory DATETIME
 SET @DaysToKeepHistory = CONVERT(VARCHAR(10), DATEADD(dd, -120, GETDATE()), 101)
 EXEC sp_purge_jobhistory @job_name = N''Saturday Backup and Maintenance'', @oldest_date = @DaysToKeepHistory
 GO', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Saturday Backup and Maintenance', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=64, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20140122, 
		@active_end_date=99991231, 
		@active_start_time=210000, 
		@active_end_time=235959, 
		@schedule_uid=N'98363a8e-3f5d-44b5-ba6d-fa69f3cff2cf'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

USE [msdb]
GO

/****** Object:  Job [syspolicy_purge_history]    Script Date: 01/26/2015 13:43:32 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 01/26/2015 13:43:32 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'syspolicy_purge_history', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Verify that automation is enabled.]    Script Date: 01/26/2015 13:43:32 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Verify that automation is enabled.', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=1, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'IF (msdb.dbo.fn_syspolicy_is_automation_enabled() != 1)
        BEGIN
            RAISERROR(34022, 16, 1)
        END', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Purge history.]    Script Date: 01/26/2015 13:43:32 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Purge history.', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC msdb.dbo.sp_syspolicy_purge_history', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Erase Phantom System Health Records.]    Script Date: 01/26/2015 13:43:32 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Erase Phantom System Health Records.', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'PowerShell', 
		@command=N'if (''$(ESCAPE_SQUOTE(INST))'' -eq ''MSSQLSERVER'') {$a = ''\DEFAULT''} ELSE {$a = ''''};
(Get-Item SQLSERVER:\SQLPolicy\$(ESCAPE_NONE(SRVR))$a).EraseSystemHealthPhantomRecords()', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'syspolicy_purge_history_schedule', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20080101, 
		@active_end_date=99991231, 
		@active_start_time=20000, 
		@active_end_time=235959, 
		@schedule_uid=N'126e8453-9dc0-448c-937b-951d3071c79e'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

