USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Status]') AND type in (N'U'))
DROP TABLE [dbo].[Status]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](	  [Id] INT NOT NULL IDENTITY(1,1)	, [Status] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO

