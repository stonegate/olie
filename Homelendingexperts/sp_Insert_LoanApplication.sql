USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_LoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_LoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert_LoanApplication]
	-- Add the parameters for the stored procedure here
(
	@LoanReasonID int, 
@HometypeID int ,
@PropertyTypeID int, 
@Producttypeid int, 
@StateID int, 
@CreditScoreID int, 
@AnnulIncomeBorrower money,
@LoanAmount money, 
@AssetBorrower money, 
@RetirementAssetsBorrower money,
@MAName varchar(100),
@PropertyRealtorCity varchar(100),
@IsWorkingWithRealtor bit,
@IsCashback bit,
@RealtorName nvarchar(100),
@RealtorPhoneNo nvarchar(100) , 
@PropertyLocated bit, 
@HomeOwned bit, 
@PurchasePrise money, 
@DownPaymentAmt int, 
@PartOfTotalAssets bit,
 @DownPaymentSourceID int,
 @HaveRealtor bit,
 @RealtorContactName nvarchar(100),
 @RealtorPhone nvarchar(100) , 
@RealtorEmail nvarchar(100), 
@AppFName nvarchar(100), 
@AppLName nvarchar(100), 
@AppStateID int, 
@AppZip nvarchar(100), 
@AppPrimaryPhone nvarchar(100), 
@AppSecondaryPhone nvarchar(100), 
@AppEmail nvarchar(100), 
@ContactTimeID int, 
@CurrentPropertyValue money, 
@PropertyPurchaseMonth nvarchar(100), 
@YearID int,
 @ExistingPuchasePrice money,
 @CurrentMortageBal money, 
@MonthlyPayment money, 
@SecondMortage bit, 
@SecondMortageBal money,
@UserAppearUrl nvarchar(1000), 
@vCity varchar(100), 
@bSignAgg bit, 
@dSchClosingDate datetime, 
@vEstRenovationAmt nvarchar(100),
 @vDownPayAmount nvarchar(100), 
@vAddressLine1 nvarchar(500),
 @vAddressLine2 nvarchar(500), 
@ApplyType varchar(50) , 
@IsLead360 bit, 

@CompIP varchar(50)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblLoanApplication

(
LoanReasonID , 
HometypeID  ,
PropertyTypeID , 
Producttypeid , 
StateID , 
CreditScoreID , 
AnnulIncomeBorrower,
LoanAmount , 
AssetBorrower , 
RetirementAssetsBorrower ,
MAName ,
PropertyRealtorCity ,
IsWorkingWithRealtor ,
IsCashback ,
RealtorName ,
RealtorPhoneNo , 
PropertyLocated , 
HomeOwned , 
PurchasePrise , 
DownPaymentAmt , 
PartOfTotalAssets,
 DownPaymentSourceID,
 HaveRealtor ,
 RealtorContactName ,
 RealtorPhone  , 
RealtorEmail , 
AppFName , 
AppLName , 
AppStateID , 
AppZip , 
AppPrimaryPhone , 
AppSecondaryPhone , 
AppEmail , 
ContactTimeID , 
CurrentPropertyValue , 
PropertyPurchaseMonth , 
YearID ,
 ExistingPuchasePrice ,
 CurrentMortageBal , 
MonthlyPayment , 
SecondMortage , 
SecondMortageBal ,
UserAppearUrl , 
vCity , 
bSignAgg , 
dSchClosingDate , 
vEstRenovationAmt ,
 vDownPayAmount , 
vAddressLine1 ,
 vAddressLine2 , 
ApplyType  , 
IsLead360 , 

CompIP 
)
values
(
@LoanReasonID , 
@HometypeID  ,
@PropertyTypeID , 
@Producttypeid , 
@StateID , 
@CreditScoreID , 
@AnnulIncomeBorrower,
@LoanAmount , 
@AssetBorrower , 
@RetirementAssetsBorrower ,
@MAName ,
@PropertyRealtorCity ,
@IsWorkingWithRealtor ,
@IsCashback ,
@RealtorName ,
@RealtorPhoneNo , 
@PropertyLocated, 
@HomeOwned , 
@PurchasePrise , 
@DownPaymentAmt , 
@PartOfTotalAssets,
 @DownPaymentSourceID,
 @HaveRealtor ,
 @RealtorContactName ,
 @RealtorPhone  , 
@RealtorEmail , 
@AppFName , 
@AppLName , 
@AppStateID , 
@AppZip , 
@AppPrimaryPhone , 
@AppSecondaryPhone , 
@AppEmail , 
@ContactTimeID , 
@CurrentPropertyValue , 
@PropertyPurchaseMonth , 
@YearID ,
 @ExistingPuchasePrice ,
 @CurrentMortageBal , 
@MonthlyPayment , 
@SecondMortage , 
@SecondMortageBal ,
@UserAppearUrl , 
@vCity , 
@bSignAgg , 
@dSchClosingDate , 
@vEstRenovationAmt ,
 @vDownPayAmount , 
@vAddressLine1 ,
 @vAddressLine2 , 
@ApplyType  , 
@IsLead360 , 
 
@CompIP 
)
END

GO

