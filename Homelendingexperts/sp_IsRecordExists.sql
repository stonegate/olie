USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IsRecordExists]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IsRecordExists]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_IsRecordExists]
(
@userid varchar (max)
)
AS
BEGIN
 select * from tblpayoffstatement where 
userid=@userid

END

GO

