USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbltestimonials]') AND type in (N'U'))
DROP TABLE [dbo].[tbltestimonials]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbltestimonials](	  [ID] INT NOT NULL IDENTITY(1,1)	, [UName] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Description] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedDate] DATETIME NULL	, CONSTRAINT [PK_tbltestimonials] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO

