USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_Subscribe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_Subscribe]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert_Subscribe]
	-- Add the parameters for the stored procedure here
(
@Name nvarchar(100),
@EMail nvarchar(100),
@Comments nvarchar(2000)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Subscribe(Name,EMail,Comments) Values(@Name,@EMail,@Comments)

END

GO

