USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertFinalApprove]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertFinalApprove]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertFinalApprove]
	-- Add the parameters for the stored procedure here
	(
@LoanApplicationID int, 
@PlaceEmp int, 
@CurrentAnualIncome money, 
@PhoneNum nvarchar(50), 
@TimePeriod int, 
@PriorEmp nvarchar(50), 
@PriorAnualIncome money, 
@IsSelfEmp bit,  
@IsCredit bit, 
@CreditSSNNo nvarchar(50),
@CreditDoBirth datetime,
@IsAuthorize bit, 
@IsCoapplicant bit, 
@CoAppFName nvarchar(50),
@CoAppLName nvarchar(50), 
@Co_BorrowerAdd1 nvarchar(200), 
@Co_BorrowerAdd2 nvarchar(200), 
@Co_BorrowerCity nvarchar(50), 
@Co_BorrowerState int, 
@Co_BorrowerZip nvarchar(50), 
@Co_BorrowerHomePhone nvarchar(50), 
@Co_BorrowerCellorWorkPhone nvarchar(50), 
@Co_BorrowerEmail nvarchar(50), 
@Co_BorrowerPlaceEmp nvarchar(50), 
@Co_BorrowerPostion nvarchar(50), 
@Co_BorrowerCurrentAnual money, 
@Co_BorrowerPhone nvarchar(50), 
@Co_BorrowerTimePeriod int, 
@Co_BorrowerPriorPlaceEmp money,
@Co_BorrowerPriorAnual money, 
@Co_BorrowerIsSelfEmp bit, 
@Co_BorrowerSelfDoBirth datetime, 
@Co_BorrowerSelfSSN nvarchar(50), 
@IsWorkingWithRealtor bit, 
@RealtorName nvarchar(50), 
@RealtorPhone nvarchar(50), 
@IsBorrowersBankruptcy bit,
@Bankruptcywhen nvarchar(50),
@IsBorrowerResponsiblechild bit,
@childsupportqty nvarchar(50)

	)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblBorrowerInfo
(
LoanApplicationID, 
PlaceEmp, 
CurrentAnualIncome, 
PhoneNum, 
TimePeriod, 
PriorEmp, 
PriorAnualIncome, 
IsSelfEmp,  
IsCredit, 
CreditSSNNo,
CreditDoBirth,
IsAuthorize, 
IsCoapplicant, 
CoAppFName,
CoAppLName, 
[Co-BorrowerAdd1], 
[Co-BorrowerAdd2], 
[Co-BorrowerCity], 
[Co-BorrowerState], 
[Co-BorrowerZip], 
[Co-BorrowerHomePhone], 
[Co-BorrowerCellorWorkPhone], 
[Co-BorrowerEmail], 
[Co-BorrowerPlaceEmp], 
[Co-BorrowerPostion], 
[Co-BorrowerCurrentAnual], 
[Co-BorrowerPhone], 
[Co-BorrowerTimePeriod], 
[Co-BorrowerPriorPlaceEmp],
[Co-BorrowerPriorAnual], 
[Co-BorrowerIsSelfEmp], 
[Co-BorrowerSelfDoBirth], 
[Co-BorrowerSelfSSN], 
IsWorkingWithRealtor, 
RealtorName, 
RealtorPhone, 
IsBorrowersBankruptcy,
Bankruptcywhen,
IsBorrowerResponsiblechild,
childsupportqty
) 
Values
(
@LoanApplicationID, 
@PlaceEmp, 
@CurrentAnualIncome, 
@PhoneNum, 
@TimePeriod, 
@PriorEmp, 
@PriorAnualIncome, 
@IsSelfEmp,  
@IsCredit, 
@CreditSSNNo,
@CreditDoBirth,
@IsAuthorize, 
@IsCoapplicant, 
@CoAppFName,
@CoAppLName, 
@Co_BorrowerAdd1, 
@Co_BorrowerAdd2, 
@Co_BorrowerCity, 
@Co_BorrowerState, 
@Co_BorrowerZip, 
@Co_BorrowerHomePhone, 
@Co_BorrowerCellorWorkPhone, 
@Co_BorrowerEmail, 
@Co_BorrowerPlaceEmp, 
@Co_BorrowerPostion, 
@Co_BorrowerCurrentAnual, 
@Co_BorrowerPhone, 
@Co_BorrowerTimePeriod, 
@Co_BorrowerPriorPlaceEmp,
@Co_BorrowerPriorAnual, 
@Co_BorrowerIsSelfEmp, 
@Co_BorrowerSelfDoBirth, 
@Co_BorrowerSelfSSN, 
@IsWorkingWithRealtor, 
@RealtorName, 
@RealtorPhone, 
@IsBorrowersBankruptcy,
@Bankruptcywhen,
@IsBorrowerResponsiblechild,
@childsupportqty

)      
              
                 
END

GO

