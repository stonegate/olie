USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDownPaymentSource]') AND type in (N'U'))
DROP TABLE [dbo].[tblDownPaymentSource]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDownPaymentSource](	  [DownPaymentSourceID] INT NOT NULL IDENTITY(1,1)	, [SourceOfdownPayment] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO

