USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_LoanApplication] 
(
@LoanReasonID int ,
@HometypeID int,
@PropertyTypeID int,
@LoanAmount money ,
@vDownPayAmount varchar(100)	,
@StateID int ,
@AppFName nvarchar(100),
@AppLName nvarchar(100) ,
@AppEmail nvarchar(100),
@vAddressLine1 varchar(500),
@vCity varchar(100),
@AppStateID int,
@AppZip nvarchar(100),
@AppPrimaryPhone nvarchar(100),
@MAName varchar(100),
@AssignedLO varchar(250),
@CompIP varchar(50)
)
AS
BEGIN
       INSERT INTO tblLoanApplication(HometypeID,PropertyTypeID,LoanAmount,vDownPayAmount,LoanReasonID,StateID,AppFName,AppLName,AppEmail,vAddressLine1,vCity,AppStateID,AppZip,AppPrimaryPhone,MAName,AssignedLO, CompIP)      
 VALUES      
  (@HometypeID,@PropertyTypeID,@LoanAmount,@vDownPayAmount,@LoanReasonID,@StateID,@AppFName,@AppLName,@AppEmail,@vAddressLine1,@vCity,@AppStateID,@AppZip,@AppPrimaryPhone,@MAName,@AssignedLO,@CompIP)  
END

GO

