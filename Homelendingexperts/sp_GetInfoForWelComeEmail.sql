USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetInfoForWelComeEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetInfoForWelComeEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetInfoForWelComeEmail]
(
@loanno varchar (max)
)
AS
BEGIN
 select * from tblServiceInfo as serviceInfo
join tblUsers on serviceInfo.userid = tblUsers.userid 
where serviceInfo.loanno = @loanno

END

GO

