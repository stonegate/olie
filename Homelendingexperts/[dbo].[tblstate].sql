USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblstate]') AND type in (N'U'))
DROP TABLE [dbo].[tblstate]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblstate](	  [SId] INT NOT NULL IDENTITY(1,1)	, [StateAbbr] VARCHAR(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [StateName] VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblstate] PRIMARY KEY ([SId] ASC))USE [HomeLendingExperts]
GO

