USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get1098DataForAllLoansFor2010]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get1098DataForAllLoansFor2010]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_Get1098DataForAllLoansFor2010]
(
@loanid varchar (max)
)
AS
BEGIN
select eyear.loanid,reportedTIN as TaxIDNumber,annualStmt.ReportedFirstname as firstmiddlename,
annualStmt.ReportedLastName as lastname,  annualStmt.MailLine1 as addressline1,annualStmt.MailLine2 as addressline2,
annualStmt.MAilCity as city,annualStmt.MailState as state,annualStmt.MailZip as zip,  eyear.TotalIntColl AS totalintcoll,
eyear.PointsPaid,annualStmt.ARMIntReimbursed,eyear.PMIPaid,annualStmt.PrincipalBal,annualStmt.princollected,
annualStmt.NegativeAmBal,annualStmt.AssistanceBal,
BeginEscBal,EscDepAmt,TaxesPaid1 , eyear.TaxesPaid,
annualStmt.InsurancePaid,annualStmt.EscDisbAmt AS OtherFundsBal,annualStmt.escrowbal,annualStmt.IntOnEscrow,annualStmt.IOEWithholding 
from dbo.r_annual_stmt annualStmt
Left outer join endofyear eyear
on annualStmt.loanid=eyear.loanid 
and year(eyear.ReportingDate) = year(annualStmt.ReportingDate)
where year(eyear.ReportingDate) = '2011' and
eyear.loanid in (@loanid) order by eyear.reportingdate desc 


END

GO

