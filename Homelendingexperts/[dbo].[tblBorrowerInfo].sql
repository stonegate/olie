USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBorrowerInfo]') AND type in (N'U'))
DROP TABLE [dbo].[tblBorrowerInfo]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBorrowerInfo](	  [BorrowerID] INT NOT NULL IDENTITY(1,1)	, [LoanApplicationID] INT NULL	, [PlaceEmp] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CurrentAnualIncome] MONEY NULL	, [PhoneNum] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [TimePeriod] INT NULL	, [PriorEmp] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PriorAnualIncome] MONEY NULL	, [IsSelfEmp] BIT NULL	, [IsCredit] BIT NULL	, [CreditSSNNo] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreditDoBirth] DATETIME NULL	, [IsAuthorize] BIT NULL	, [IsCoapplicant] BIT NULL	, [CoAppFName] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CoAppLName] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerAdd1] NVARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerAdd2] NVARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerCity] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerState] INT NULL	, [Co-BorrowerZip] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerHomePhone] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerCellorWorkPhone] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerEmail] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerPlaceEmp] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerPostion] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerCurrentAnual] MONEY NULL	, [Co-BorrowerPhone] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerTimePeriod] INT NULL	, [Co-BorrowerPriorPlaceEmp] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Co-BorrowerPriorAnual] MONEY NULL	, [Co-BorrowerIsSelfEmp] BIT NULL	, [Co-BorrowerSelfDoBirth] DATETIME NULL	, [Co-BorrowerSelfSSN] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsWorkingWithRealtor] BIT NULL	, [RealtorName] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RealtorPhone] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsBorrowersBankruptcy] BIT NULL	, [Bankruptcywhen] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsBorrowerResponsiblechild] BIT NULL	, [childsupportqty] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblBorrowerInfo] PRIMARY KEY ([BorrowerID] ASC))USE [HomeLendingExperts]
GO

