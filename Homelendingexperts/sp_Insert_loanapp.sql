USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_loanapp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_loanapp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert_loanapp]
	-- Add the parameters for the stored procedure here

(
@Fname varchar(45),
@Mname varchar(45),
@LName varchar(45),
@Email varchar(200),
@CEmail varchar(200),
@DOBM varchar(10),
@DOBDT int,
@DOBYR int,
@SSN varchar(45),
@Phone varchar(45),
@Suffix varchar(5),
@COFNAME varchar(45),
@COMNAME varchar(45),
@COLNAME varchar(45),
@COEMAIL varchar(200),
@COCEmail varchar(200),
@CODOBM varchar(10),
@CODOBDT int,
@CODOBYR int,
@COSSN varchar(45),
@COPHONE varchar(45),
@STADDR varchar(45),
@CITY varchar(45),
@MState int,
@ZipCode varchar(10),
@RSTATUS varchar(25),
@RentAmt varchar(100),
@LMHOLDER varchar(45),
@STADDR2 varchar(45),
@CITY2 varchar(45),
@STATE2 int,
@COSuffix varchar(45),
@ZipCode2 varchar(10),
@RLengthY2 int,
@RLengthM2 int,
@RSTATUS2 varchar(25),
@RentAmt2 varchar(100),
@LMHOLDER2 varchar(45),
@AEMPL varchar(100),
@AOCCU varchar(100),
@AEMPPHONE varchar(45),
@AGMI varchar(100),
@ALEMY int,
@ALEMM int,
@AEMPL2 varchar(100),
@AOCCU2 varchar(100),
@AEMPPHONE2 varchar(45),
@AGMI2 int,
@ALEMY2 int,
@ALEMM2 int,
@CAEMPL varchar(100),
@CAOCCU varchar(100),
@CAEMPPHONE varchar(45),
@CAGMI varchar(100),
@CALEMY int,
@CALEMM int,
@CAEMPL2 varchar(100),
@CAOCCU2 varchar(100),
@CAEMPPHONE2 varchar(45),
@CAGMI2 varchar(100),
@CALEMY2 int,
@CALEMM2 int,
@OtherComments varchar(1000),
@DigiSig varchar(100),
@SigDate varchar(15),
@IsAckno bit ,
@DateEntered datetime,
@RLengthM int,
@RlengthY int
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into tblloanapp(Fname,Mname,LName,Email,CEmail,DOBM,DOBDT,DOBYR,SSN,Phone,Suffix,COFNAME,COMNAME,COLNAME,COEMAIL,COCEmail,CODOBM,CODOBDT,CODOBYR,COSSN,COPHONE,STADDR,CITY,MState,ZipCode,RSTATUS,RentAmt,LMHOLDER,STADDR2,CITY2,STATE2,COSuffix,ZipCode2,RLengthY2,RLengthM2,RSTATUS2,RentAmt2,LMHOLDER2,AEMPL,AOCCU,AEMPPHONE,AGMI,ALEMY,ALEMM,AEMPL2,AOCCU2,AEMPPHONE2,AGMI2,ALEMY2,ALEMM2,CAEMPL,CAOCCU,CAEMPPHONE,CAGMI,CALEMY,CALEMM,CAEMPL2,CAOCCU2,CAEMPPHONE2,CAGMI2,CALEMY2,CALEMM2,OtherComments,DigiSig,SigDate,IsAckno,DateEntered,RLengthM,RlengthY)
 values(@Fname,@Mname,@LName,@Email,@CEmail,@DOBM,@DOBDT,@DOBYR,@SSN,@Phone,@Suffix,@COFNAME,@COMNAME,@COLNAME,@COEMAIL,@COCEmail,@CODOBM,@CODOBDT,@CODOBYR,@COSSN,@COPHONE,@STADDR,@CITY,@MState,@ZipCode,@RSTATUS,@RentAmt,@LMHOLDER,@STADDR2,@CITY2,@STATE2,@COSuffix,@ZipCode2,@RLengthY2,@RLengthM2,@RSTATUS2,@RentAmt2,@LMHOLDER2,@AEMPL,@AOCCU,@AEMPPHONE,@AGMI,@ALEMY,@ALEMM,@AEMPL2,@AOCCU2,@AEMPPHONE2,@AGMI2,@ALEMY2,@ALEMM2,@CAEMPL,@CAOCCU,@CAEMPPHONE,@CAGMI,@CALEMY,@CALEMM,@CAEMPL2,@CAOCCU2,@CAEMPPHONE2,@CAGMI2,@CALEMY2,@CALEMM2,@OtherComments,@DigiSig,@SigDate,@IsAckno,@DateEntered,@RLengthM,@RlengthY)
 

end

GO

