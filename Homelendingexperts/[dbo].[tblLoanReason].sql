USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanReason]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanReason]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanReason](	  [LoanReasonID] INT NOT NULL IDENTITY(1,1)	, [LoanReason] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL)USE [HomeLendingExperts]
GO

