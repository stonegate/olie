USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLoanIdNew_LoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLoanIdNew_LoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertLoanIdNew_LoanApplication]
	-- Add the parameters for the stored procedure here
(
	@HometypeID int ,
			@PropertyTypeID int, 
			@StateID int, 
			@LoanReasonID int,

			@LoanAmount money, 
			@MAName varchar(100),
			@AppFName nvarchar(100), 
			@AppLName nvarchar(100), 
			@AppStateID int, 
			@AppZip nvarchar(100), 
			@AppPrimaryPhone nvarchar(100), 
			@AppEmail nvarchar(100), 
			@vCity varchar(100), 
 			@vDownPayAmount nvarchar(100), 
			@vAddressLine1 nvarchar(500),
			@AssignedLO varchar(250),
			@CompIP varchar(50)	
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblLoanApplication

(

	
			HometypeID ,
			PropertyTypeID ,
			StateID , 
			LoanReasonID ,

			LoanAmount , 
			MAName ,
			AppFName , 
			AppLName , 
			AppStateID , 
			AppZip , 
			AppPrimaryPhone , 
			AppEmail , 
			vCity , 
 			vDownPayAmount , 
			vAddressLine1 ,
			AssignedLO ,
			CompIP 
)
values
(

	
			@HometypeID ,
			@PropertyTypeID ,
			@StateID , 
			@LoanReasonID ,

			@LoanAmount , 
			@MAName ,
			@AppFName , 
			@AppLName , 
			@AppStateID , 
			@AppZip , 
			@AppPrimaryPhone , 
			@AppEmail , 
			@vCity , 
 			@vDownPayAmount , 
			@vAddressLine1 ,
			@AssignedLO ,
			@CompIP 
)
END

GO

