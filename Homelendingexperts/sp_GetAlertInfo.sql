USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAlertInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAlertInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetAlertInfo]
(
@userid varchar(Max),
@loanno varchar(Max)
)
AS
BEGIN
select * from tblServiceInfo where userid = @userid and loanno = @loanno
     
END

GO

