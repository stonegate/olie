USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetRecentConditionsReviewedDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetRecentConditionsReviewedDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =======================================================    
-- Author:  Ratan Gohil 
-- Create date: 11/01/2013  
-- Description: Procedure to Get Conditions Reviewed Date]  
-- =========================================================    
CREATE PROCEDURE [dbo].[udsp_GetRecentConditionsReviewedDate] -- '0000346672','D70F0DD48CDD443C8F0CC58849EE401F'
(    
@LoanNumber varchar(50),
@CustomeFieldPTPConditionReviewDate varchar(50),
@CustomeFieldPTPSubsequentReviewDate1 varchar(50),
@CustomeFieldPTPSubsequentReviewDate2 varchar(50),
@CustomeFieldPTPSubsequentReviewDate3 varchar(50)
)    
AS    
BEGIN       
SET NOCOUNT ON; 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
    
 select Top 1 mwlcustomfield.DateValue from mwlcustomfield 
 Inner Join mwlLoanApp on mwlLoanApp.ID = mwlcustomfield.LoanApp_ID 
 where 
 (mwlcustomfield.custfielddef_id = @CustomeFieldPTPConditionReviewDate 
	OR mwlcustomfield.custfielddef_id = @CustomeFieldPTPSubsequentReviewDate1 
	OR mwlcustomfield.custfielddef_id = @CustomeFieldPTPSubsequentReviewDate2 
	OR mwlcustomfield.custfielddef_id = @CustomeFieldPTPSubsequentReviewDate3)
 and mwlLoanApp.LoanNumber=@LoanNumber
 ORDER BY DateValue DESC
END



GO

