USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateCorrespodentSpecialistInE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateCorrespodentSpecialistInE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_UpdateCorrespodentSpecialistInE3]      
@loannumber varchar(15)  ,  
@CustName varchar(100),    
@CustN1 varchar(100),    
@CustN2 varchar(100),    
@CustN3 varchar(100),    
@CustN4 varchar(100),    
@CustD1 varchar(100),    
@CustD2 varchar(100),    
@CustD3 varchar(100),    
@CustD4 varchar(100),    
@CustT1 varchar(100),
@CustT2 varchar(100),    
@CustT3 varchar(100),    
@CustT4 varchar(100),    
@Result int output  
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
   
 --declare @trancount int;  
 --set @trancount = @@trancount;  
 --print 'trans - '+ cast(@trancount as varchar(10))  
   
 --BEGIN TRY  
  
 --if @trancount = 0  
 --begin transaction  
    
 declare @LoanApp_id varchar(100)   
 set @Result=0  
     
 select @LoanApp_id=ID from mwlLoanApp where loannumber =''+@loannumber+''    
 print @LoanApp_id    
     
 Declare @sql nvarchar(4000)     
 Declare @sql1 nvarchar(4000)        
 Declare @sql2 nvarchar(4000)        
 Declare @sql3 nvarchar(4000)        
 
  
   set @sql = ''     
   set @sql=' if not exists(select * From mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''    
   set @sql= @sql + @CustN1      
   set @sql= @sql + ''')    and  LoanApp_id in('''    
   set @sql= @sql + @LoanApp_id    
   set @sql= @sql +''') )  '    
   set @sql=@sql +' insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue)   values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustN1 +''','''+@CustName+''',Convert(varchar(12),getdate(),101))  '    
   set @sql=@sql +'    else '    
   set @sql=@sql +'   BEGIN  '    
   set @sql=@sql +'   if not exists(select * From mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustN2 +''') and  LoanApp_id in( '''+@LoanApp_id+''') )  '    
   set @sql=@sql +'     BEGIN '     
   set @sql=@sql +'        insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue)'    
   set @sql=@sql +'        values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustN2 +''','''+@CustName+''',Convert(varchar(12),getdate(),101))  '    
   set @sql=@sql +'     End  '    
   set @sql=@sql +'     else '    
   set @sql=@sql +'       BEGIN '     
   set @sql=@sql +'      if not exists(select * From mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustN3 +''') and  LoanApp_id in('''+@LoanApp_id+'''))  '    
   set @sql=@sql +'      Begin  '    
   set @sql=@sql +'       insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue)values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustN3 +''','''+@CustName+''','    
   set @sql=@sql +'      Convert(varchar(12),getdate(),101))  '    
   set @sql=@sql +'      End  '    
   set @sql=@sql +'      else '    
   set @sql=@sql +'      Begin  '    
   set @sql=@sql +'      if not exists(select * From mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustN4 +''') and  LoanApp_id in('''+@LoanApp_id+''') )  '    
   set @sql=@sql +'        begin '     
   set @sql=@sql +'       insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue)values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustN4 +''','''+@CustName+''',Convert(varchar(12),getdate(),101))  '    
   set @sql=@sql +'        end  '    
   set @sql=@sql +'      else  '    
   set @sql=@sql +'        begin '     
   set @sql=@sql +'       update mwlcustomfield set StringValue='''+@CustName+''',DateValue=Convert(varchar(12),getdate(),101) where CustFieldDef_ID in ('''+ @CustN4 +''') '    
   set @sql=@sql +'       and  LoanApp_id in('''+@LoanApp_id+''')  '    
   set @sql=@sql +'        end  '    
   set @sql=@sql +'    End' --else 3    
   set @sql=@sql +'     End '--else 2    
   set @sql=@sql +'   End '--else 1    
         
     print @sql     
     --exec sp_executesql @sql   
         
    print '4'  
      
    set @sql2=''   
    set @sql2=@sql2 + ' if not exists(select * From mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustD1+''') and  LoanApp_id in('''+@LoanApp_id+''') )  '  
    --set @sql2=@sql2 + ' insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue) '  
    set @sql2=@sql2 + ' insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,DateValue) ' 
    --set @sql2=@sql2 + ' values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustD1+''','''+@CustName+''',Convert(varchar(12),getdate(),101))  '  
    set @sql2=@sql2 + ' values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustD1+''',Convert(varchar(12),getdate(),101))  '  
    set @sql2=@sql2 + ' else BEGIN  if not exists(select * From mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustD2+''') and  LoanApp_id in('''+@LoanApp_id+''') ) '  
    set @sql2=@sql2 + ' begin  insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue) values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustD2+''','''+@CustName+''',Convert(varchar(12),getdate(),101))  end '  
    set @sql2=@sql2 + ' else BEGIN  if not exists(select * From mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustD3+''') and  LoanApp_id in('''+@LoanApp_id+''') )  begin '  
    set @sql2=@sql2 + ' insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue)values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustD3+''','''+@CustName+''',Convert(varchar(12),getdate(),101))  end '  
    set @sql2=@sql2 +  'else begin  if not exists(select * From mwlcustomfield  WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustD4+''') and  LoanApp_id in('''+@LoanApp_id+''') )  '  
    set @sql2=@sql2 +  'begin  insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue)values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustD4+''','''+@CustName+''',Convert(varchar(12),getdate(),101))  end  '  
    set @sql2=@sql2 +  'else  begin  update mwlcustomfield set StringValue='''+@CustName+''',DateValue=Convert(varchar(12),getdate(),101) where CustFieldDef_ID in ('''+ @CustD4+''') and  LoanApp_id in('''+@LoanApp_id+''')  end '  
    set @sql2=@sql2 +  ' end End End '  
  
  print @sql2  
      
  
    set @sql3=''   
    set @sql3=@sql3 + 'if not exists(select * From mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustT1+''') and  LoanApp_id in('''+@LoanApp_id+''') ) '  
    set @sql3=@sql3 + 'insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue)'  
    set @sql3=@sql3 + 'values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustT1+''',Convert(varchar,getdate(),8))  '   
      
  print @sql3  
 
        exec sp_executesql @sql  
        exec sp_executesql @sql2  
        exec sp_executesql @sql3  
  
         
      set @Result=1  
        
      
      print '5'  
      return @Result       
        
END


GO

