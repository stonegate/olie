USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPGetStoneGateUserData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPGetStoneGateUserData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SPGetStoneGateUserData] 
	@Prolendorid varchar(500)
AS
BEGIN
	select * from tblusers where loginidprolender=@Prolendorid
END

GO

