USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLoanNumberFromConditionID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetLoanNumberFromConditionID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Shwetal Shah>  
-- Create date: <03/07/13>  
-- Description: <Get LoanNUmber from ConditionID>  
-- =============================================  
CREATE PROCEDURE GetLoanNumberFromConditionID   
(  
@ConditionID varchar(35)  
)  
AS  
BEGIN   
  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
SET XACT_ABORT ON;  
  
 Select LoanNumber FROM mwlloanapp WHERE id = (Select ObjOwner_ID FROM mwlCondition WHERE ID = @ConditionID)  
    
END  
GO

