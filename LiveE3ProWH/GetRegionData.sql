USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRegionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetRegionData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/19/2012
-- Description:	Procedure for clspipelineE3 BLL
-- =============================================
CREATE procEDURE [dbo].[GetRegionData]


AS
BEGIN
	
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

 select Region, uLastName + ', ' + uFirstName AS FullName, uidprolender,e3userid,urole from tblusers 
where urole in(26) and IsActive=1 
END



GO

