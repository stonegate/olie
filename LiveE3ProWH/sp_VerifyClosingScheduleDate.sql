USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_VerifyClosingScheduleDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_VerifyClosingScheduleDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_VerifyClosingScheduleDate] 
(
@LoanNo varchar(max)
)
	
AS
BEGIN
	select SchDate from tblScheduleClosing where LoanNo =@LoanNo
END







GO

