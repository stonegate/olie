USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SelectNewsAndAnnouncementID_By_Retail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SelectNewsAndAnnouncementID_By_Retail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_SelectNewsAndAnnouncementID_By_Retail]              
(              
   
  
@NewsID int            
)              
AS              
BEGIN  
SELECT  
 * ,CreatedDate as modifieddate 
FROM Retail_tblNewsAndAnnouncement  
WHERE ID = @NewsID            
END

GO

