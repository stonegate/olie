USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_mwlBorrower]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_mwlBorrower]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		 Nitin
-- Create date: 10/05/2012
-- Description:	Procedure for clspipelineE3.cs bll 
-- =============================================
CREATE procEDURE [dbo].[sp_Get_mwlBorrower]
(
@LoanNumber bigint
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

   SELECT rtrim(FirstName) as borr_first FROM mwlBorrower as borr 
INNER JOIN mwlloanapp  loanapp ON loanapp.ID = borr.loanapp_id  
where LoanNumber =@LoanNumber
END


GO

