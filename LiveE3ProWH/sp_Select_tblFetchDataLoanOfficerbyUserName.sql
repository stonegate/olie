USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblFetchDataLoanOfficerbyUserName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblFetchDataLoanOfficerbyUserName]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblFetchDataLoanOfficerbyUserName]
(
@strUserName varchar(Max),
@urole varchar(Max)
)
as
begin
select * from tblusers 
where CONVERT(VARCHAR(1),ufirstname)+ ulastname =@strUserName and  isactive=1 And urole=@urole
end


GO

