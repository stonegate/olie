USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetBDMEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetBDMEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetBDMEmail]
@LoanNo varchar(500)
AS
BEGIN
	select lt_broker from loandat where loan_no=@LoanNo
END

GO

