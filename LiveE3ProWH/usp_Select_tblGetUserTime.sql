USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tblGetUserTime]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tblGetUserTime]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[usp_Select_tblGetUserTime]
(
@_userid varchar(Max),
@iLoginExpireDays varchar(Max)
)
as
begin
	select top 1 DATEADD(day,Convert(bigint,@iLoginExpireDays),LoginTime) UserLogintime,LogoutTime,* 
from tbltracking T 
where  userid = @_userid order by T.LoginTime desc
end



GO

