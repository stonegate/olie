USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateCondtionText]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateCondtionText]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Gourav  
-- Create date: 10/03/2012  
-- Description: Procedure for clsPipeline.cs bll    
-- =============================================  
create proc [dbo].[UpdateCondtionText]  
(  
  
@strCondText varchar(max) , 
@strRecordId varchar(max)  
)  
as  
begin  

update condits set cond_text = @strCondText where record_id = @strRecordId
end  


 
GO

