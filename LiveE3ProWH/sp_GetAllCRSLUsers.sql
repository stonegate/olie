USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAllCRSLUsers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAllCRSLUsers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetAllCRSLUsers] 
@CRSMUserid int
AS
BEGIN
	
	SET NOCOUNT ON;

	--Loans
	SELECT CASE  WHEN isActive = 1 THEN (uFirstName + ' ' + uLastName) ELSE (uFirstName + ' ' + uLastName + ' - InActive') END as username, tempLCMs.userid,users.urole from  
	(
		SELECT userid,urole from tblusers where urole = 41 and userid <> @CRSMUserid
		UNION ALL
		SELECT DISTINCT(Userid) as userid,40 as urole  FROM [dbo].[tblUserManagers] WHERE ManagerId = @CRSMUserid
	)as tempLCMs
	 INNER JOIN tblusers users on users.userid = tempLCMs.userid 
	 ORDER BY users.urole,username

	--Originators
	SELECT 
	CASE WHEN isActive = 1 THEN (uFirstName + ' ' + uLastName) ELSE (uFirstName + ' ' + uLastName + ' - InActive') END as username ,tempCRSuser.CRSMId ,tempCRSuser.CRSId, tempCRSuser.AuserId, tempCRSuser.RoleId  FROM 
	(
	SELECT CRSMUserId as CRSMId, ' ' as CRSId, AssociateUserId as AuserId ,AssociateRoleId as RoleId  from tblCRSMAssociation CRSM WHERE AssociateUserId IS NOT NULL
	UNION ALL
	SELECT '' as CRSMId, CRSUserId as CRSId, AssociateUserId as AuserId ,AssociateRoleId as RoleId from tblCRSAssociation CRS WHERE AssociateUserId IS NOT NULL --where CRS.CRSUserId in (select distinct(Userid) from [dbo].[tblUserManagers] where ManagerId = 164)
	) as tempCRSuser
   INNER JOIN tblusers users on users.userid = tempCRSuser.AuserId 

   -- Get associated clients for LC/LCM-+

   SELECT DISTINCT(Userid) FROM [dbo].[tblUserManagers] WHERE ManagerId = @CRSMUserid --@CRSMUserid
   
   -- Clients

   --Get PartnerCompnay names
   Select Distinct(partnercompanyid) , Companyname into #tempcompanynames from tblusers 

   SELECT CRSMUserId as CRSMId, ' ' as CRSId, AssociateUserId as AuserId ,AssociateRoleId as RoleId , ISNULL(CRSM.partnercompanyId,'') as partnercompanyId , ISNULL(users.CompanyName,'') as CompanyName from tblCRSMAssociation CRSM
   INNER JOIN #tempcompanynames users on users.partnercompanyId = CRSM.partnercompanyId 
    WHERE CRSM.partnercompanyId IS NOT NULL
   UNION ALL
   SELECT '' as CRSMId, CRSUserId as CRSId, AssociateUserId as AuserId ,AssociateRoleId as RoleId , ISNULL(CRS.partnercompanyId,'') as partnercompanyId,IsNULL(users.CompanyName,'') as CompanyName from tblCRSAssociation CRS
   INNER JOIN #tempcompanynames users on users.partnercompanyId = CRS.partnercompanyId  WHERE CRS.partnercompanyId IS NOT NULL

END


GO

