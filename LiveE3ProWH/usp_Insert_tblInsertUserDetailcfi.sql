USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Insert_tblInsertUserDetailcfi]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Insert_tblInsertUserDetailcfi]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Insert_tblInsertUserDetailcfi] 
	-- Add the parameters for the stored procedure here
	(
@_userid varchar(Max),
@clientIPAddress varchar(Max)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	INSERT INTO tblTracking (UserID,LoginTime,IPAddress) VALUES (@_userid,GETDATE(),@clientIPAddress)

END


GO

