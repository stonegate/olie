USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_UserStates]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Get_UserStates]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author, Somnath M. R.>
-- Create date: <Create Date, 09th Dec 2013>
-- Description:	<Description, Select from tblUserStates table>
-- EXEC Get_UserStates 16523
-- =============================================
CREATE PROCEDURE [dbo].[Get_UserStates]
	@UserId Int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
		US.UserStateId, 
		US.UserId, 
		US.StateId, 
		S.abbr 
	FROM [dbo].[tblUserStates] US
	INNER JOIN [dbo].[tblState] S ON S.state_id = US.StateId
	INNER JOIN [dbo].[tblUsers] U ON U.UserId = US.UserId
	WHERE US.UserId = @UserId
END

GO

