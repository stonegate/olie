USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_ActivDeActivClientsInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_ActivDeActivClientsInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Example for execute stored procedure       
--EXECUTE [udsp_ActivateMandatoryClientsInfo] '478452','True','10931' 
-- =============================================                        
-- Author:  Karan Gulati                        
-- Create date: 17/04/2013                        
-- Description: Activate Clients Details for Mandatory-Best Effort Delivery Task                        
-- =============================================             
    
             
CREATE PROCEDURE [dbo].[udsp_ActivDeActivClientsInfo]  
(
 --Parameter declaration 
@ID varchar(max),                    
@IsActive bit,   
@ModifiedBy int   
)               
AS                        
BEGIN   
SET XACT_ABORT ON;-- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error, 
				  -- the entire transaction is terminated and rolled back.    
				     
UPDATE [dbo].[tblManageMandatoryClientsInfo] WITH (UPDLOCK)                      
SET IsActive = @IsActive, ModifiedBy = @ModifiedBy, ModifiedDate=GETDATE()           
WHERE PartnerCompanyId in(SELECT     items
FROM         dbo.SplitString(@ID, ','))             
END 

GO

