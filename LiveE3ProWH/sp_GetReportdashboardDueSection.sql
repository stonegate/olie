USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetReportdashboardDueSection]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetReportdashboardDueSection]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Modified for the BRD - 78 (Underwriter Dashboard) by Tavant
-- Modified By Suvarna To fix JIRA 185 issue on 06/01/2014
--------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_GetReportdashboardDueSection] --'9687'--'126' jduel1980
	(@Userid VARCHAR(max))
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;--This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)              

	DECLARE @strid VARCHAR(max)

	SET @strid = (
			SELECT e3userid
			FROM tblusers
			WHERE userid = @Userid
			)

	SELECT TOP 4 *
	FROM --> For filteration
		(
		SELECT mwlBorrower.LastName AS BorrowerLastName
			,mwlLoanApp.LoanNumber
			,mwlLoanApp.DecisionStatus
		--	,mwlLoanApp.channeltype
			,CASE mwlLoanApp.TransType
				WHEN 'P'
					THEN 'Purchase'
				WHEN 'R'
					THEN 'Refinance'
				END AS TransType
			,mwlLoanApp.CurrentStatus AS LoanStatus
			,mwlLoanApp.OriginatorName AS BDM
			,CASE ISNULL(RTRIM(Offices.Company), '')
				WHEN ''
					THEN RTRIM(Corres.Company)
				ELSE RTRIM(Offices.Company)
				END AS TPOPartner
			,CONVERT(DATETIME, (
					CONVERT(VARCHAR(30), Cust.DateValue, 111) + ' ' + CASE 
						WHEN ISDATE(CustTime.stringvalue) = 1
							THEN CONVERT(VARCHAR(8), CustTime.stringvalue, 108)
						ELSE '00:00:00.000'
						END
					)) AS ConditionalDate
			,mwlLoanApp.LoanCompositeScore AS FICO
			,CONVERT(VARCHAR(30), Cust.DateValue, 101) AS LastSubmittedDate
			,CustTime.StringValue AS LastSubmittedTime
			,NULL AS [Ready for UW Review]
			,CASE 
				WHEN (
						SELECT COUNT(a.ID) AS TotalConditions
						FROM dbo.mwlCondition a
						WHERE DueBy = 'Prior to Closing'
							AND Category != 'LENDER'
							AND Category != 'NOTICE'
							AND a.objOwner_ID IN (
								SELECT ID
								FROM dbo.mwlLoanApp AS loan1
								WHERE loan1.LoanNumber = mwlLoanApp.LoanNumber
								)
						) = 0
					THEN 0
				WHEN (
						SELECT COUNT(a.ID) AS TotalCleared
						FROM dbo.mwlCondition a
						WHERE (
								DueBy = 'Prior to Closing'
								AND Category != 'LENDER'
								AND Category != 'NOTICE'
								)
							AND (
								CurrentState = 'CLEARED'
								OR CurrentState = 'SUBMITTED'
								OR CurrentState = 'Waived'
								)
							AND a.objOwner_ID IN (
								SELECT id
								FROM dbo.mwlLoanApp AS loan2
								WHERE loan2.LoanNumber = mwlLoanApp.Loannumber
								)
						) = 0
					THEN 0
				ELSE (
						SELECT COUNT(a.ID) AS TotalCleared
						FROM dbo.mwlCondition a
						WHERE (
								DueBy = 'Prior to Closing'
								AND Category != 'LENDER'
								AND Category != 'NOTICE'
								)
							AND (
								CurrentState = 'CLEARED'
								OR CurrentState = 'SUBMITTED'
								OR CurrentState = 'Waived'
								)
							AND a.objOwner_ID IN (
								SELECT ID
								FROM dbo.mwlLoanApp AS loan2
								WHERE loan2.LoanNumber = mwlLoanApp.LoanNumber
								)
						) * 100 / (
						SELECT COUNT(a.ID) AS TotalConditions
						FROM dbo.mwlCondition a
						WHERE (
								DueBy = 'Prior to Closing'
								AND Category != 'LENDER'
								AND Category != 'NOTICE'
								)
							AND a.objOwner_ID IN (
								SELECT ID
								FROM dbo.mwlLoanApp AS loan1
								WHERE loan1.LoanNumber = mwlLoanApp.LoanNumber
								)
						)
				END AS ConditionalStatus
			,CASE 
				WHEN LastCustDate2.DateValue <> '1900-01-01 00:00:00.000'
					THEN CONVERT(VARCHAR(30), LastCustDate2.DateValue, 101)
				ELSE NULL
				END AS ConditionReviewDate
			,LastCustTime2.stringvalue AS ConditionReviewTime --into #tempTPOCFILoans
		FROM mwlLoanApp
		INNER JOIN mwlBorrower ON LoanApp_ID = mwlLoanApp.ID
			AND RLABorrCoborr = 1
			AND Sequencenum = 1
		INNER JOIN mwlloandata ON mwlLoanData.ObjOwner_ID = mwlLoanApp.ID
		LEFT JOIN mwlInstitution ON mwlLoanApp.ID = mwlInstitution.ObjOwner_ID
			AND mwlInstitution.InstitutionType = 'BROKER'
		LEFT JOIN mwlLockRecord ON mwlLockRecord.LoanApp_ID = mwlLoanApp.ID
			AND mwlLockRecord.LockType = 'LOCK'
			AND mwlLockRecord.STATUS <> 'CANCELED'
			AND mwlLockRecord.STATUS = 'CONFIRMED'
		LEFT JOIN mwlUnderwritingSummary ON mwlUnderwritingSummary.LoanApp_ID = mwlLoanApp.ID
		LEFT JOIN mwaMWUser ON mwaMWUser.ID = mwlLoanApp.Originator_ID
		LEFT JOIN mwlAppStatus AS SubUW ON SubUW.LoanApp_ID = mwlLoanApp.ID
			AND SubUW.StatusDesc = '25 - Submitted to Underwriting'
		LEFT JOIN mwlAppStatus AS InUW ON InUW.LoanApp_ID = mwlLoanApp.ID
			AND InUW.StatusDesc = '29 - In Underwriting'
			AND mwlLoanApp.DecisionStatus = 'Not Decisioned'
		LEFT JOIN mwlAppStatus AS InSus ON InSus.LoanApp_ID = mwlLoanApp.ID
			AND InSus.StatusDesc = '29 - In Underwriting'
			AND mwlLoanApp.DecisionStatus = 'Suspended'
		LEFT JOIN mwlAppStatus AS InAppr ON InAppr.LoanApp_ID = mwlLoanApp.ID
			AND InAppr.StatusDesc = '29 - In Underwriting'
			AND mwlLoanApp.DecisionStatus = 'Conditionally Approved'
		LEFT JOIN mwlAppStatus AS AppDt ON AppDt.LoanApp_ID = mwlLoanApp.ID
			AND AppDt.StatusDesc = '01 - Registered'
		LEFT JOIN mwlAppStatus AS InUWDate ON InUWDate.LoanApp_ID = mwlLoanApp.ID
			AND InUWDate.StatusDesc = '29 - In Underwriting'
		LEFT JOIN mwlInstitution AS Offices ON Offices.ObjOwner_id = mwlLoanApp.id
			AND Offices.InstitutionType = 'Broker'
			AND Offices.objownerName = 'Broker'
		LEFT JOIN mwlInstitution AS COffices ON COffices.ObjOwner_id = mwlLoanApp.id
			AND COffices.InstitutionType = 'CORRESPOND'
			AND COffices.objownerName = 'Contacts'
		LEFT JOIN mwlInstitution AS BOffices ON BOffices.ObjOwner_id = mwlLoanApp.id
			AND BOffices.InstitutionType = 'Branch'
			AND BOffices.objownerName = 'BranchInstitution'
		LEFT JOIN mwlInstitution AS Corres ON Corres.ObjOwner_id = mwlLoanApp.id
			AND Corres.InstitutionType = 'CORRESPOND'
			AND Corres.objownerName = 'Correspondent'
		LEFT JOIN mwlcustomfield AS Cust ON Cust.loanapp_id = mwlLoanApp.id
			AND Cust.CustFieldDef_ID = '5261689813D9467BA3C980BF871289F1'
		LEFT JOIN -- 5261689813D9467BA3C980BF871289F1 for 100% Conditions Received Date
			mwlcustomfield AS CustTime ON CustTime.loanapp_id = mwlLoanApp.id
			AND CustTime.CustFieldDef_ID = '27414DB20AF34DB39BB6903551817FBA'
		LEFT JOIN -- 27414DB20AF34DB39BB6903551817FBA for 100% Conditions Received Time
			mwlcustomfield AS LastCustDate2 ON LastCustDate2.loanapp_id = mwlLoanApp.id
			AND LastCustDate2.CustFieldDef_ID = 'F745C50C06994DADA2959CBD985BE150' -- for Condition Review Date:
		--AND LastCustDate2.DateValue IS NULL AND LastCustDate2.StringValue IS NULL 
		LEFT JOIN mwlcustomfield AS LastCustTime2 ON LastCustTime2.loanapp_id = mwlLoanApp.id
			AND LastCustTime2.CustFieldDef_ID = '384C3F3E4BAD426A85DFD8091236A291' -- for Condition Review Time:
		--AND LastCustTime2.stringvalue IS NULL AND LastCustTime2.Datevalue IS NULL
		LEFT JOIN mwlcustomfield AS mwlcf1 ON mwlcf1.loanapp_id = mwlLoanApp.id
			AND mwlcf1.CustFieldDef_ID = '38F5E3FAFFB44635A3CD3295D5BAC02E' -- for Subsequent Review Date (1):
		LEFT JOIN mwlcustomfield AS mwlcf2 ON mwlcf2.loanapp_id = mwlLoanApp.id
			AND mwlcf2.CustFieldDef_ID = 'D840061DE5684F66A223E17720E9F9A4' -- for Subsequent Review Time (1):
		LEFT JOIN mwlcustomfield AS mwlcf3 ON mwlcf3.loanapp_id = mwlLoanApp.id
			AND mwlcf3.CustFieldDef_ID = '65581AAEF77B4B3FAC10E5E058D21D37' -- for Subsequent Review Date (2):
		LEFT JOIN mwlcustomfield AS mwlcf4 ON mwlcf4.loanapp_id = mwlLoanApp.id
			AND mwlcf4.CustFieldDef_ID = 'FCD37FA5FC5C4B2CABB62DF28F15C10D' -- for Subsequent Review Time (2):
		LEFT JOIN mwlcustomfield AS mwlcf5 ON mwlcf5.loanapp_id = mwlLoanApp.id
			AND mwlcf5.CustFieldDef_ID = '8627A410A4D24499B346FB92B60EBB58' -- for Subsequent Review Date (3):
		LEFT JOIN mwlcustomfield AS mwlcf6 ON mwlcf6.loanapp_id = mwlLoanApp.id
			AND mwlcf6.CustFieldDef_ID = '3AE386C609E74587A3E7BFC2639130A0' -- for Subsequent Review Time (3):
		LEFT JOIN mwlcustomfield AS mwlcf7 ON mwlcf7.loanapp_id = mwlLoanApp.id
			AND mwlcf7.CustFieldDef_ID = 'E5F8F6A987DA45C78654F1006B190098' -- for Subsequent Review Date (4):
		LEFT JOIN mwlcustomfield AS mwlcf8 ON mwlcf8.loanapp_id = mwlLoanApp.id
			AND mwlcf8.CustFieldDef_ID = 'B66D335B8CC74A7E81BFCA6DFCC13B49' -- for Subsequent Review Time (4):
		LEFT JOIN mwlcustomfield AS mwlcf9 ON mwlcf9.loanapp_id = mwlLoanApp.id
			AND mwlcf9.CustFieldDef_ID = '1B102002D1AB4E79B496D065B31A2034' -- for Subsequent Review Date (5):
		LEFT JOIN mwlcustomfield AS mwlcf10 ON mwlcf10.loanapp_id = mwlLoanApp.id
			AND mwlcf10.CustFieldDef_ID = '960A769016984CAE95A5CA0C617CBF95' -- for Subsequent Review Time (5):
		LEFT JOIN mwlcustomfield AS mwlcf11 ON mwlcf11.loanapp_id = mwlLoanApp.id
			AND mwlcf11.CustFieldDef_ID = 'B7607375552342BCB4C0B6C1A5DDE542' -- for Subsequent Review Date (6):
		LEFT JOIN mwlcustomfield AS mwlcf12 ON mwlcf12.loanapp_id = mwlLoanApp.id
			AND mwlcf12.CustFieldDef_ID = 'B063242E136C4E339559385BD57047B1' -- for Subsequent Review Time (6):
		WHERE (CurrentStatus IN ('29 - In Underwriting'))
			AND (
				mwlUnderwritingSummary.underwriter_id IN (
					SELECT items
					FROM dbo.SplitString(@strid, '')
					)
				)
			AND (Cust.DateValue IS NOT NULL)
			AND (CustTime.StringValue IS NOT NULL)
			AND (
				Cust.custfielddef_id IN (
					'5261689813D9467BA3C980BF871289F1'
					,'27414DB20AF34DB39BB6903551817FBA'
					)
				) -- 5261689813D9467BA3C980BF871289F1 for 100% Conditions Received Date
			-- 27414DB20AF34DB39BB6903551817FBA for 100% Conditions Received Time
			AND (
				(ISNULL(LastCustDate2.DateValue, '') = '')
				AND (ISNULL(LastCustTime2.StringValue, '') = '')
				OR (
					(
						CONVERT(DATETIME, (
								CONVERT(VARCHAR(30), LastCustDate2.DateValue, 111) + ' ' + CASE 
									WHEN ISDATE(LastCustTime2.StringValue) = 1
										THEN CONVERT(VARCHAR(8), LastCustTime2.StringValue, 108)
									ELSE '00:00:00.000'
									END
								))
						) < (
						CONVERT(DATETIME, (
								CONVERT(VARCHAR(30), Cust.DateValue, 111) + ' ' + CASE 
									WHEN ISDATE(CustTime.StringValue) = 1
										THEN CONVERT(VARCHAR(8), CustTime.StringValue, 108)
									ELSE '00:00:00.000'
									END
								))
						)
					)
				)
			AND (
				(ISNULL(mwlcf1.DateValue, '') = '')
				AND (ISNULL(mwlcf2.StringValue, '') = '')
				OR (
					(
						CONVERT(DATETIME, (
								CONVERT(VARCHAR(30), mwlcf1.DateValue, 111) + ' ' + CASE 
									WHEN ISDATE(mwlcf2.StringValue) = 1
										THEN CONVERT(VARCHAR(8), mwlcf2.StringValue, 108)
									ELSE '00:00:00.000'
									END
								))
						) < (
						CONVERT(DATETIME, (
								CONVERT(VARCHAR(30), Cust.DateValue, 111) + ' ' + CASE 
									WHEN ISDATE(CustTime.StringValue) = 1
										THEN CONVERT(VARCHAR(8), CustTime.StringValue, 108)
									ELSE '00:00:00.000'
									END
								))
						)
					)
				)
			AND (
				(ISNULL(mwlcf3.DateValue, '') = '')
				AND (ISNULL(mwlcf4.StringValue, '') = '')
				OR (
					(
						CONVERT(DATETIME, (
								CONVERT(VARCHAR(30), mwlcf3.DateValue, 111) + ' ' + CASE 
									WHEN ISDATE(mwlcf4.StringValue) = 1
										THEN CONVERT(VARCHAR(8), mwlcf4.StringValue, 108)
									ELSE '00:00:00.000'
									END
								))
						) < (
						CONVERT(DATETIME, (
								CONVERT(VARCHAR(30), Cust.DateValue, 111) + ' ' + CASE 
									WHEN ISDATE(CustTime.StringValue) = 1
										THEN CONVERT(VARCHAR(8), CustTime.StringValue, 108)
									ELSE '00:00:00.000'
									END
								))
						)
					)
				)
			AND (
				(ISNULL(mwlcf5.DateValue, '') = '')
				AND (ISNULL(mwlcf6.StringValue, '') = '')
				OR (
					(
						CONVERT(DATETIME, (
								CONVERT(VARCHAR(30), mwlcf5.DateValue, 111) + ' ' + CASE 
									WHEN ISDATE(mwlcf6.StringValue) = 1
										THEN CONVERT(VARCHAR(8), mwlcf6.StringValue, 108)
									ELSE '00:00:00.000'
									END
								))
						) < (
						CONVERT(DATETIME, (
								CONVERT(VARCHAR(30), Cust.DateValue, 111) + ' ' + CASE 
									WHEN ISDATE(CustTime.StringValue) = 1
										THEN CONVERT(VARCHAR(8), CustTime.StringValue, 108)
									ELSE '00:00:00.000'
									END
								))
						)
					)
				)
			AND (
				(ISNULL(mwlcf7.DateValue, '') = '')
				AND (ISNULL(mwlcf8.StringValue, '') = '')
				OR (
					(
						CONVERT(DATETIME, (
								CONVERT(VARCHAR(30), mwlcf7.DateValue, 111) + ' ' + CASE 
									WHEN ISDATE(mwlcf8.StringValue) = 1
										THEN CONVERT(VARCHAR(8), mwlcf8.StringValue, 108)
									ELSE '00:00:00.000'
									END
								))
						) < (
						CONVERT(DATETIME, (
								CONVERT(VARCHAR(30), Cust.DateValue, 111) + ' ' + CASE 
									WHEN ISDATE(CustTime.StringValue) = 1
										THEN CONVERT(VARCHAR(8), CustTime.StringValue, 108)
									ELSE '00:00:00.000'
									END
								))
						)
					)
				)
			AND (
				(ISNULL(mwlcf9.DateValue, '') = '')
				AND (ISNULL(mwlcf10.StringValue, '') = '')
				OR (
					(
						CONVERT(DATETIME, (
								CONVERT(VARCHAR(30), mwlcf9.DateValue, 111) + ' ' + CASE 
									WHEN ISDATE(mwlcf10.StringValue) = 1
										THEN CONVERT(VARCHAR(8), mwlcf10.StringValue, 108)
									ELSE '00:00:00.000'
									END
								))
						) < (
						CONVERT(DATETIME, (
								CONVERT(VARCHAR(30), Cust.DateValue, 111) + ' ' + CASE 
									WHEN ISDATE(CustTime.StringValue) = 1
										THEN CONVERT(VARCHAR(8), CustTime.StringValue, 108)
									ELSE '00:00:00.000'
									END
								))
						)
					)
				)
			AND (
				(ISNULL(mwlcf11.DateValue, '') = '')
				AND (ISNULL(mwlcf12.StringValue, '') = '')
				OR (
					(
						CONVERT(DATETIME, (
								CONVERT(VARCHAR(30), mwlcf11.DateValue, 111) + ' ' + CASE 
									WHEN ISDATE(mwlcf12.StringValue) = 1
										THEN CONVERT(VARCHAR(8), mwlcf12.StringValue, 108)
									ELSE '00:00:00.000'
									END
								))
						) < (
						CONVERT(DATETIME, (
								CONVERT(VARCHAR(30), Cust.DateValue, 111) + ' ' + CASE 
									WHEN ISDATE(CustTime.StringValue) = 1
										THEN CONVERT(VARCHAR(8), CustTime.StringValue, 108)
									ELSE '00:00:00.000'
									END
								))
						)
					)
				)
			AND(  mwlLoanApp.channeltype = 'RETAIL')
			AND DecisionStatus = 'Approved'

			UNION ALL 

		SELECT mwlBorrower.LastName AS BorrowerLastName
			,mwlLoanApp.LoanNumber
			,mwlLoanApp.DecisionStatus
		--	,mwlLoanApp.channeltype
			,CASE mwlLoanApp.TransType
				WHEN 'P'
					THEN 'Purchase'
				WHEN 'R'
					THEN 'Refinance'
				END AS TransType
			,mwlLoanApp.CurrentStatus AS LoanStatus
			,mwlLoanApp.OriginatorName AS BDM
			,CASE ISNULL(RTRIM(Offices.Company), '')
				WHEN ''
					THEN RTRIM(Corres.Company)
				ELSE RTRIM(Offices.Company)
				END AS TPOPartner
			,CONVERT(DATETIME, (
					CONVERT(VARCHAR(30), Cust.DateValue, 111) + ' ' + CASE 
						WHEN ISDATE(CustTime.stringvalue) = 1
							THEN CONVERT(VARCHAR(8), CustTime.stringvalue, 108)
						ELSE '00:00:00.000'
						END
					)) AS ConditionalDate
			,mwlLoanApp.LoanCompositeScore AS FICO
			,CONVERT(VARCHAR(30), Cust.DateValue, 101) AS LastSubmittedDate
			,CustTime.StringValue AS LastSubmittedTime
			,CASE WHEN CustFinalUWReview.YNValue = 1 THEN CustFinalUWReview.UpdatedOnDate ELSE CustUWReview.UpdatedOnDate end as [Ready for UW Review] 
			,CASE 
				WHEN (
						SELECT COUNT(a.ID) AS TotalConditions
						FROM dbo.mwlCondition a
						WHERE DueBy = 'Prior to Closing'
							AND Category != 'LENDER'
							AND Category != 'NOTICE'
							AND a.objOwner_ID IN (
								SELECT ID
								FROM dbo.mwlLoanApp AS loan1
								WHERE loan1.LoanNumber = mwlLoanApp.LoanNumber
								)
						) = 0
					THEN 0
				WHEN (
						SELECT COUNT(a.ID) AS TotalCleared
						FROM dbo.mwlCondition a
						WHERE (
								DueBy = 'Prior to Closing'
								AND Category != 'LENDER'
								AND Category != 'NOTICE'
								)
							AND (
								CurrentState = 'CLEARED'
								OR CurrentState = 'SUBMITTED'
								OR CurrentState = 'Waived'
								)
							AND a.objOwner_ID IN (
								SELECT id
								FROM dbo.mwlLoanApp AS loan2
								WHERE loan2.LoanNumber = mwlLoanApp.Loannumber
								)
						) = 0
					THEN 0
				ELSE (
						SELECT COUNT(a.ID) AS TotalCleared
						FROM dbo.mwlCondition a
						WHERE (
								DueBy = 'Prior to Closing'
								AND Category != 'LENDER'
								AND Category != 'NOTICE'
								)
							AND (
								CurrentState = 'CLEARED'
								OR CurrentState = 'SUBMITTED'
								OR CurrentState = 'Waived'
								)
							AND a.objOwner_ID IN (
								SELECT ID
								FROM dbo.mwlLoanApp AS loan2
								WHERE loan2.LoanNumber = mwlLoanApp.LoanNumber
								)
						) * 100 / (
						SELECT COUNT(a.ID) AS TotalConditions
						FROM dbo.mwlCondition a
						WHERE (
								DueBy = 'Prior to Closing'
								AND Category != 'LENDER'
								AND Category != 'NOTICE'
								)
							AND a.objOwner_ID IN (
								SELECT ID
								FROM dbo.mwlLoanApp AS loan1
								WHERE loan1.LoanNumber = mwlLoanApp.LoanNumber
								)
						)
				END AS ConditionalStatus
			,CASE 
				WHEN LastCustDate2.DateValue <> '1900-01-01 00:00:00.000'
					THEN CONVERT(VARCHAR(30), LastCustDate2.DateValue, 101)
				ELSE NULL
				END AS ConditionReviewDate
			,LastCustTime2.stringvalue AS ConditionReviewTime --into #tempTPOCFILoans
		FROM mwlLoanApp
		INNER JOIN mwlBorrower ON LoanApp_ID = mwlLoanApp.ID
			AND RLABorrCoborr = 1
			AND Sequencenum = 1
		INNER JOIN mwlloandata ON mwlLoanData.ObjOwner_ID = mwlLoanApp.ID
		LEFT JOIN mwlInstitution ON mwlLoanApp.ID = mwlInstitution.ObjOwner_ID
			AND mwlInstitution.InstitutionType = 'BROKER'
		LEFT JOIN mwlAppStatus AS AppDt ON AppDt.LoanApp_ID = mwlLoanApp.ID
			AND AppDt.StatusDesc = '01 - Registered'
		LEFT JOIN mwlInstitution AS Offices ON Offices.ObjOwner_id = mwlLoanApp.id
			AND Offices.InstitutionType = 'Broker'
			AND Offices.objownerName = 'Broker'
		LEFT JOIN mwlInstitution AS COffices ON COffices.ObjOwner_id = mwlLoanApp.id
			AND COffices.InstitutionType = 'CORRESPOND'
			AND COffices.objownerName = 'Contacts'
		LEFT JOIN mwlInstitution AS BOffices ON BOffices.ObjOwner_id = mwlLoanApp.id
			AND BOffices.InstitutionType = 'Branch'
			AND BOffices.objownerName = 'BranchInstitution'
		LEFT JOIN mwlInstitution AS Corres ON Corres.ObjOwner_id = mwlLoanApp.id
			AND Corres.InstitutionType = 'CORRESPOND'
			AND Corres.objownerName = 'Correspondent'
		LEFT JOIN mwlcustomfield AS Cust ON Cust.loanapp_id = mwlLoanApp.id
			AND Cust.CustFieldDef_ID = '5261689813D9467BA3C980BF871289F1'
		LEFT JOIN -- 5261689813D9467BA3C980BF871289F1 for 100% Conditions Received Date
			mwlcustomfield AS CustTime ON CustTime.loanapp_id = mwlLoanApp.id
			AND CustTime.CustFieldDef_ID = '27414DB20AF34DB39BB6903551817FBA'
		LEFT JOIN -- 27414DB20AF34DB39BB6903551817FBA for 100% Conditions Received Time
			mwlcustomfield AS LastCustDate2 ON LastCustDate2.loanapp_id = mwlLoanApp.id
			AND LastCustDate2.CustFieldDef_ID = 'F745C50C06994DADA2959CBD985BE150' -- for Condition Review Date:
		--AND LastCustDate2.DateValue IS NULL AND LastCustDate2.StringValue IS NULL 
		LEFT JOIN mwlcustomfield AS LastCustTime2 ON LastCustTime2.loanapp_id = mwlLoanApp.id
			AND LastCustTime2.CustFieldDef_ID = '384C3F3E4BAD426A85DFD8091236A291' -- for Condition Review Time:
		LEFT JOIN mwlcustomfield AS priorapploans ON priorapploans.loanapp_id = mwlLoanApp.id
			AND priorapploans.CustFieldDef_ID = 'C4D2D8038F614DF8A1B8A6622C99FC3E'
		left join mwlcustomfield as CustUWReview on CustUWReview.loanapp_id = mwlLoanApp.id  and CustUWReview.CustFieldDef_ID in (Select id from mwsCustFieldDef WITH (nolock) Where MWFieldNum = '9348')
		left join mwlcustomfield as CustFinalUWReview on CustFinalUWReview.loanapp_id = mwlLoanApp.id  and CustFinalUWReview.CustFieldDef_ID in (Select id from mwsCustFieldDef WITH (nolock) Where MWFieldNum = '9327') 
		WHERE (CurrentStatus IN ('29.1 - In UW Processing') AND ( (  (ISNULL(CustFinalUWReview.YNValue,'0') = '1') OR (ISNULL(CustUWReview.YNValue,'0') = '1')  )) )
		AND(  mwlLoanApp.channeltype <> 'RETAIL') and  priorapploans.StringValue='Prior Approved' 


		) Result

ORDER BY ConditionalDate ASC


END

GO

