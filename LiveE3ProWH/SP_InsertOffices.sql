USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_InsertOffices]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_InsertOffices]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
-- =============================================      
-- Author:  <Chandrika>      
-- Create date: <4-Oct-2012>      
-- Description: <Insert Offices>      
-- =============================================      
CREATE PROCEDURE [dbo].[SP_InsertOffices]      
 (      
@ltofficeid varchar(50),      
@prouid varchar(50),      
@userid int,     
@lt_usr_underwriter varchar(50),    
@name varchar(100)      
)      
AS      
BEGIN     
     
  Insert into tblOffices(lt_office_id,prouid,userid,lt_usr_underwriter,name) Values(@ltofficeid,@prouid,@userid,@lt_usr_underwriter,@name)    
      
END 
GO

