USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InserttblUserPasswordHistoryNew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InserttblUserPasswordHistoryNew]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Shwetal
-- Create date: 03/12/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE procEDURE [dbo].[sp_InserttblUserPasswordHistoryNew]
(
@UserID int
)
AS
BEGIN
SET NOCOUNT ON;

INSERT INTO tblUserPasswordHistory(UserID,Password) 
SELECT @UserID, upassword from tblUsers Where UserID = @UserID

END



GO

