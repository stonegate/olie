USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetAERegIndividualReportClosing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetAERegIndividualReportClosing]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Stonegate>  
-- Create date: <10/5/2012>  
-- Description: <Fetch AE Region Individual Report Closing>  
-- =============================================  
CREATE PROCEDURE	[dbo].[SpGetAERegIndividualReportClosing] 
(  
 @Puid nvarchar(max),
@Channel varchar(100),	  
 @CurrentStatus nvarchar(max)

)  
AS  
BEGIN  
  
Declare @SQL nvarchar(max)  
Set @SQL =''  
Set @SQL = 'select ''E3'' as DB,loanapp.DecisionStatus,loanapp.Channeltype as BusType, loanapp.ID as record_id,LoanNumber as loan_no,borr.lastname as BorrowerLastName,loanapp.DecisionStatus,loanapp.CurrentStatus as LoanStatus,DateAppSigned as SubmittedDate,Loandata.LoanProgramName as LoanProgram,
case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,borr.CompositeCreditScore as CreditScoreUsed,loanData.AdjustedNoteAmt as LoanAmount,
convert(varchar(35),EstCloseDate,101) AS ScheduleDate,LockStatus,loanapp.LockExpirationDate,UnderwriterName as UnderWriter,CloserName as Closer,CASE ISNULL(RTRIM(Institute.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Institute.Company) END as BrokerName, Institute.CustomDataOne as TitleCompany,
AppStatus.StatusDateTime as DocsSentDate,FundedStatus.StatusDateTime as FundedDate,'''' as LU_FINAL_HUD_STATUS, LockRecord.DeliveryOption 
 from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
    left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  Left join dbo.mwlInstitution as Institute on Institute.ObjOwner_id = loanapp.id and 
  InstitutionType = ''BROKER'' and objownerName=''BROKER'' Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and AppStatus.StatusDesc like ''%Docs Out%'' 
 Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''
  Left join mwlappstatus as FundedStatus on FundedStatus.LoanApp_id=loanapp.id  and FundedStatus.StatusDesc like ''%Funded''
  left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType = ''LOCK'' and  LockRecord.Status <> ''CANCELED'' 
  where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''   
and CurrentStatus IN (select * from dbo.SplitString('''+@CurrentStatus+''','','')) '  
   
if (@Channel != 'Retail')
begin
  Set @SQL = @SQL + ' and Originator_id in (select * from dbo.SplitString('''+ @Puid +''','',''))'   
end
if (@Channel = 'Retail')
begin
 Set @SQL = @SQL + ' and loanapp.ChannelType like ''%RETAIL%'' '  
END

  Set @SQL = @SQL + ' order by CurrentStatus '      

Print @SQL  
exec sp_executesql @SQL  
END  



GO

