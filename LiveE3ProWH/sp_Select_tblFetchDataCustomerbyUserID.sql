USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblFetchDataCustomerbyUserID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblFetchDataCustomerbyUserID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblFetchDataCustomerbyUserID]
(
@loannum varchar(max)
)
as
begin
SELECT loandat2.lt_usr_lorep, loandat2.sell_agnt_name 
FROM loandat INNER JOIN loandat2 ON loandat.record_id = loandat2.record_id 
where loandat.loan_no = @loannum
end


GO

