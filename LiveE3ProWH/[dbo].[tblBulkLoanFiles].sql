USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBulkLoanFiles]') AND type in (N'U'))
DROP TABLE [dbo].[tblBulkLoanFiles]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBulkLoanFiles](	  [BulkLoanFileId] UNIQUEIDENTIFIER NOT NULL	, [FileName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Type] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ChannelType] VARCHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Status] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CompanyName] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Comments] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ProcessStartedOn] DATETIME NULL	, [CreatedBy] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedOn] DATETIME NULL	, [ModifiedBy] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ModifiedOn] DATETIME NULL	, CONSTRAINT [PK_tblBulkLoanFiles] PRIMARY KEY ([BulkLoanFileId] ASC))USE [HomeLendingExperts]
GO

