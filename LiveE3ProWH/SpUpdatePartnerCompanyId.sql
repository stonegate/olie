USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpUpdatePartnerCompanyId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpUpdatePartnerCompanyId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpUpdatePartnerCompanyId]
	@PartnerCompanyId varchar(500),
	@UserId int,
	@Urole varchar(500)
AS
BEGIN
	update tblusers set PartnerCompanyid=@PartnerCompanyId where Userid=@UserId and urole=@Urole
END

GO

