USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetCountyForDOCSOLIE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetCountyForDOCSOLIE]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Mukesh Kumar  
-- Create date: 08/31/2012  
-- Description: To Get County on the Basis of State Code  
-- =============================================  
CREATE PROCEDURE [dbo].[udsp_GetCountyForDOCSOLIE]   
 -- Add the parameters for the stored procedure here  
 @StateCode varchar(2)  
AS  
BEGIN  
  -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)
 
    -- Insert statements for procedure here  
Select distinct ((convert(varchar(500),upper(substring(CountyName,1,1))+lower(substring(CountyName,2,499)))))as County  
 From mwsCounty where StateCode = @StateCode and  CountyName is not null  and CountyName <> '' order by County  
  
END  
  
  
GO

