USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tbl_Condition_Action]') AND type in (N'U'))
DROP TABLE [dbo].[Tbl_Condition_Action]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Condition_Action](	  [Condid] BIGINT NOT NULL IDENTITY(1,1)	, [loanno] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [condrecid] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [isViewDoc] BIT NULL	, [isCommitDoc] BIT NULL	, [isClearCondition] BIT NULL	, [isNotifyBroker] BIT NULL	, [isNotifyHistory] BIT NULL	, [isUnClearCondition] BIT NULL	, [isRejectDoc] BIT NULL	, [ClearedDate] DATETIME NULL	, CONSTRAINT [PK_Tbl_Condition_Action] PRIMARY KEY ([Condid] ASC))USE [HomeLendingExperts]
GO

