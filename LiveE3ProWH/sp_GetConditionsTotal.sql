USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetConditionsTotal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetConditionsTotal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/10/2012
-- Description:	Procedure for Get Conditions Totals
-- =============================================
CREATE PROCEDURE sp_GetConditionsTotal
(
@loan_no char(15)
)

AS
BEGIN
	
	SET NOCOUNT ON;
SELECT count(a.record_id) as TotalConditions   FROM dbo.condits a,lookups b 
 where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = 'aaw' and descriptn='PROCESSOR')
 and a.parent_id IN ( SELECT record_id FROM dbo.loandat  WHERE loan_no=@loan_no);
                SELECT count(a.record_id) as TotalCleared FROM dbo.condits a,lookups b  
where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = 'aaw' and descriptn='PROCESSOR')
and ISNULL(a.cond_recvd_date,a.COND_CLEARED_DATE) IS NOT NULL and a.parent_id IN 
( SELECT record_id FROM dbo.loandat  WHERE loan_no=@loan_no )
END

GO

