USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Getmwlborrower]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Getmwlborrower]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/06/2012
-- Description:	Procedure for clspipelineE3 BLL
-- =============================================
CREATE procEDURE [dbo].[sp_Getmwlborrower]
(
@firstname varchar(30),
@Lastname varchar(30)
)
AS
BEGIN
	
	SET NOCOUNT ON;
SELECT LoanApp_id FROM mwlborrower 
where firstname =@firstname 
and Lastname= @Lastname 
order by CreatedonDate Desc
END


GO

