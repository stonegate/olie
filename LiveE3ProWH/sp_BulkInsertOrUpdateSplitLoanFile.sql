USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_BulkInsertOrUpdateSplitLoanFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_BulkInsertOrUpdateSplitLoanFile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BulkInsertOrUpdateSplitLoanFile]
(
    @LoanFileId UNIQUEIDENTIFIER,
	@BulkLoanFileId UNIQUEIDENTIFIER,
	@FileName VARCHAR(200) = NULL,
    @Type VARCHAR(50) = NULL,
	@ChannelType VARCHAR(1) = NULL,
    @Status VARCHAR(50) = NULL,
	@FinalStatus VARCHAR(50) = NULL,
	@CompanyName VARCHAR(100) = NULL,
	@RetryCounter INT = 0,
	@RegistrationStatus BIT = 0,
    @Comments VARCHAR(1000) = NULL,
	@LoanNumber NVARCHAR(15) = NULL,
	@BorrowerLastName VARCHAR(100) = NULL,
	@PropertyAddress VARCHAR(100) = NULL,	
	@OriginatorLOSLoanNumber VARCHAR(100) = NULL,
	@TransactionType VARCHAR(100) = NULL,
	@SSNumber VARCHAR(100) = NULL,	
    @ProcessStartedOn DATETIME = NULL,
    @CreatedBy VARCHAR(100) = NULL,
    @CreatedOn DATETIME = NULL
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS(SELECT * FROM [dbo].[tblBulkSplitLoanFiles] with(NOLOCK) WHERE [LoanFileId] = @LoanFileId)
	BEGIN
    INSERT INTO [dbo].[tblBulkSplitLoanFiles]
           ([LoanFileId]
		   ,[BulkLoanFileId]
           ,[FileName]
           ,[Type]
		   ,[ChannelType]
           ,[Status]
		   ,[FinalStatus]
		   ,[CompanyName]
		   ,[RetryCounter]
	       ,[RegistrationStatus]
           ,[Comments]
		   ,[LoanNumber]
	       ,[BorrowerLastName]
	       ,[PropertyAddress]
		   ,[OriginatorLOSLoanNumber]
		   ,[TransactionType]
		   ,[SSNumber]
           ,[ProcessStartedOn]
           ,[CreatedBy]
           ,[CreatedOn])
     VALUES
           (@LoanFileId
		   ,@BulkLoanFileId
           ,@FileName
           ,@Type
		   ,@ChannelType
           ,@Status
		   ,@FinalStatus
		   ,@CompanyName
		   ,@RetryCounter
	       ,@RegistrationStatus
           ,@Comments
		   ,@LoanNumber
	       ,@BorrowerLastName
	       ,@PropertyAddress
		   ,@OriginatorLOSLoanNumber
		   ,@TransactionType
		   ,@SSNumber
           ,@ProcessStartedOn
           ,@CreatedBy
           ,@CreatedOn)
	END
	ELSE
	BEGIN	
	UPDATE [dbo].[tblBulkSplitLoanFiles]
	   SET [FileName] = ISNULL(@FileName,[FileName])
		  ,[Type] = ISNULL(@Type,[Type])
		  ,[ChannelType] = ISNULL(@ChannelType,[ChannelType])
		  ,[Status] = ISNULL(@Status,[Status])
		  ,[FinalStatus] = ISNULL(@FinalStatus,[FinalStatus])
		  ,[CompanyName] = ISNULL(@CompanyName,[CompanyName])
		  ,[RetryCounter] = ISNULL(@RetryCounter,RetryCounter)
	      ,[RegistrationStatus] = ISNULL(@RegistrationStatus,RegistrationStatus)
		  ,[Comments] = ISNULL(@Comments,Comments)
		  ,[LoanNumber] = ISNULL(@LoanNumber,LoanNumber)
	      ,[BorrowerLastName] = ISNULL(@BorrowerLastName,BorrowerLastName)
	      ,[PropertyAddress] = ISNULL(@PropertyAddress,PropertyAddress)		  
		  ,[OriginatorLOSLoanNumber] = ISNULL(@OriginatorLOSLoanNumber,OriginatorLOSLoanNumber)
		  ,[TransactionType] = ISNULL(@TransactionType,TransactionType)
		  ,[SSNumber] = ISNULL(@SSNumber,SSNumber)
		  ,[ProcessStartedOn] = ISNULL(@ProcessStartedOn,ProcessStartedOn)
		  ,[ModifiedBy] = ISNULL(@CreatedBy,CreatedBy)
		  ,[ModifiedOn] = ISNULL(@CreatedOn,CreatedOn)
	  WHERE [LoanFileId] = @LoanFileId
	END
END


GO

