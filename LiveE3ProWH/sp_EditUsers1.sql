USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EditUsers1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EditUsers1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




create PROCEDURE [dbo].[sp_EditUsers1] 
	-- Add the parameters for the stored procedure here
@userid int ,@ufirstname varchar(50),@ulastname varchar(50),@uemailid varchar(200),@experience nchar(100),
@phonenum nvarchar(20),@mobile_ph nvarchar(50) ,@address nvarchar(300),
@Address2 varchar(100),@city nvarchar(100),@state nvarchar(50),@zip nvarchar(10),
@fax nvarchar(50),@photoname nvarchar(50),@logoname nvarchar(50),
@facebookid nvarchar(50),@TwitterID nvarchar(50),@StateBelongTo nvarchar(100),
@BrokerID int,@bio ntext,@AreaOfExpertise ntext,
@BranchID int,@MI varchar(10),@ClientTestimonial ntext
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	
	SET NOCOUNT ON;
    -- update statements for procedure here
	begin
	update tblusers Set ufirstname = @ufirstname ,ulastname=@ulastname ,uemailid=@uemailid ,experience=@experience ,
phonenum=@phonenum ,mobile_ph=@mobile_ph  ,address=@address ,
Address2=@Address2 ,city=@city ,state=@state ,zip=@zip ,
fax=@fax ,photoname =@photoname ,logoname=@logoname ,
facebookid=@facebookid ,TwitterID=@TwitterID ,StateBelongTo=@StateBelongTo ,
BrokerID=@BrokerID ,bio=@bio ,AreaOfExpertise=@AreaOfExpertise ,
BranchID=@BranchID ,MI=@MI ,ClientTestimonial=@ClientTestimonial
where userid=@userid



	end
END




GO

