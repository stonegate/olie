USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_GetLoanProgram]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_GetLoanProgram]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_Select_GetLoanProgram] 
AS
BEGIN
select '0' as record_id, '--Select Program--' as prog_code ,'' as prog_desc union select record_id,prog_code,prog_desc from PROGINFO
END


GO

