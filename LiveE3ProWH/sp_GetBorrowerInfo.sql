USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBorrowerInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBorrowerInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Author,,Name>    
-- Create date: <30/8/2012>    
-- Description: <Description,,>    
-- =============================================    
CREATE PROCEDURE  [dbo].[sp_GetBorrowerInfo]    
  @LoanNumber VARCHAR(50)  
AS    
BEGIN    
   
   SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

select count(*)AS Total From mwlloanapp loanapp ,mwlNonBorrower Nonborr   
Where Nonborr.loanapp_id = loanapp.id And loanapp.loannumber = @LoanNumber  
  
select count(*) AS Total From mwlloanapp loanapp ,mwlBorrower borr   
Where borr.loanapp_id = loanapp.id And loanapp.loannumber = @LoanNumber    
END 
GO

