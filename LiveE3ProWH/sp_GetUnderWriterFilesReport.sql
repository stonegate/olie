USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUnderWriterFilesReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUnderWriterFilesReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
-- Author:		<Stonegate>
-- Create date: <10/5/2012>
-- Description:	<This will Get UnderWriter Files Report>
-- ======================================================

--exec sp_GetUnderWriterFilesReport 34, '5261689813D9467BA3C980BF871289F1', '27414DB20AF34DB39BB6903551817FBA', dfiles, ''
CREATE PROCEDURE [dbo].[sp_GetUnderWriterFilesReport] 
(
	@RoleID int,
	@SubmittedDateID varchar(500),
	@SubmittedDateTimeID varchar(500),
	@strFrom varchar(50),
	@strID varchar(2000)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)

Declare @SQL nvarchar(max)
Set @SQL = ''
Set @SQL ='SELECT ''E3'' as DB,Cust.DateValue as LastSubmittedDate,CustTime.StringValue as LastSubmittedTime ,InUWDate.StatusDateTime AS UW_RECVD_DATENew,
CONVERT(DECIMAL(15,3),mwlLoanData.LTV *100)LTV, mwlLoanApp.ID,Offices.Office as office, COffices.Office as coffice,
BOffices.Office as Boffice, LoanNumber AS loan_no,RTRIM(mwlBorrower.LastName) AS BorrowerLastName, mwlloanapp.ChannelType AS bustype, 
CurrentStatus as LoanStatus,
CASE TransType when NULL THEN '''' WHEN ''P'' THEN ''Purchase money first mortgage'' WHEN ''R'' THEN ''Refinance'' WHEN ''2'' THEN ''Purchase money second mortgage'' WHEN ''S'' THEN ''Second mortgage, any other purpose'' WHEN ''A'' THEN ''Assumption'' WHEN ''HOP'' THEN ''HELOC - other purpose'' WHEN ''HP'' THEN ''HELOC - purchase'' ELSE '''' END AS TransType, 
case mwlLoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,mwlLoanData.LoanProgDesc as prog_desc, mwlLoanData.AdjustedNoteAmt as loan_amt,
mwlLockRecord.Status AS LockStatus, mwlLockRecord.LockExpirationDate,mwlLockRecord.LockDateTime, Underwriter_ID AS lt_usr_underwriter,
UnderwriterName AS UnderWriter,CASE ISNULL(RTRIM(Offices.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Offices.Company) end AS BrokerName,
mwaMWUser.FirstName + '' '' + mwaMWUser.LastName AS LORep,SubUW.StatusDateTime AS UW_SUBMIT_DATE,InUW.StatusDateTime AS UW_RECVD_DATE,
ISNULL(InSus.StatusDateTime,''1900-01-01 00:00:00.000'') AS SUSP_DATE, ISNULL(InAppr.StatusDateTime,''1900-01-01 00:00:00.000'') AS APPR_DATE,
ISNULL(AppDt.StatusDateTime,''1900-01-01 00:00:00.000'') AS SubmittedDate,
convert(varchar(35),EstCloseDate,101) AS ScheduleDate, mwlInstitution.EMail AS brok_email, mwaMWUser.EMail AS lorep_email,
CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER'' 
and Category !=''NOTICE'' and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=mwlLoanApp.LoanNumber)) = 0 then 0
WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'')
and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2 WHERE loan2.LoanNumber=mwlLoanApp.Loannumber)) = 0 THEN 0 ELSE (SELECT count(a.ID) as TotalCleared
FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'' ) and a.objOwner_ID IN
(SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=mwlLoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') 
and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=mwlLoanApp.LoanNumber))END AS PER,
mwlLoanApp.ChannelType, mwlLoanApp.DecisionStatus, mwlLoanApp.OriginatorName, mwlLoanApp.CurrDecStatusDate
FROM mwlLoanApp
INNER JOIN mwlBorrower ON LoanApp_ID = mwlLoanApp.ID and RLABorrCoborr=1 and Sequencenum=1
INNER JOIN mwlLoanData ON mwlLoanData.ObjOwner_ID = mwlLoanApp.ID
LEFT JOIN mwlInstitution ON mwlLoanApp.ID = mwlInstitution.ObjOwner_ID and mwlInstitution.InstitutionType=''BROKER''
LEFT JOIN mwlLockRecord ON mwlLockRecord.LoanApp_ID = mwlLoanApp.ID and mwlLockRecord.LockType=''LOCK'' and  mwlLockRecord.Status<>''CANCELED''
LEFT JOIN mwlUnderwritingSummary ON mwlUnderwritingSummary.LoanApp_ID = mwlLoanApp.ID
LEFT JOIN mwaMWUser ON mwaMWUser.ID = mwlLoanApp.Originator_ID
LEFT JOIN mwlAppStatus AS SubUW ON SubUW.LoanApp_ID = mwlLoanApp.ID AND SubUW.StatusDesc=''25 - Submitted to Underwriting''
LEFT JOIN mwlAppStatus AS InUW ON InUW.LoanApp_ID = mwlLoanApp.ID AND InUW.StatusDesc=''29 - In Underwriting'' and mwlLoanApp.DecisionStatus=''Not Decisioned''
LEFT JOIN mwlAppStatus AS InSus ON InSus.LoanApp_ID = mwlLoanApp.ID AND InSus.StatusDesc=''29 - In Underwriting'' and mwlLoanApp.DecisionStatus=''Suspended''
LEFT JOIN mwlAppStatus AS InAppr ON InAppr.LoanApp_ID = mwlLoanApp.ID AND InAppr.StatusDesc=''29 - In Underwriting'' and mwlLoanApp.DecisionStatus=''Conditionally Approved''
LEFT JOIN mwlAppStatus AS AppDt ON AppDt.LoanApp_ID = mwlLoanApp.ID AND AppDt.StatusDesc=''01 - Registered''
LEFT JOIN mwlAppStatus AS InUWDate ON InUWDate.LoanApp_ID = mwlLoanApp.ID AND InUWDate.StatusDesc=''29 - In Underwriting''
Left join dbo.mwlInstitution as Offices on Offices.ObjOwner_id = mwlLoanApp.id and Offices.InstitutionType = ''Broker'' and Offices.objownerName=''Broker''
Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = mwlLoanApp.id and COffices.InstitutionType = ''CORRESPOND'' and COffices.objownerName=''Contacts''
Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = mwlLoanApp.id and BOffices.InstitutionType = ''Branch'' and BOffices.objownerName=''BranchInstitution''
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = mwlLoanApp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''
left join mwlcustomfield as Cust on Cust.loanapp_id = mwlLoanApp.id  and Cust.CustFieldDef_ID ='''+ @SubmittedDateID +''' 
left join mwlcustomfield as CustTime on CustTime.loanapp_id = mwlLoanApp.id and CustTime.CustFieldDef_ID ='''+ @SubmittedDateTimeID +'''
left join mwlcustomfield as CustUWReview on CustUWReview.loanapp_id = mwlLoanApp.id  and CustUWReview.CustFieldDef_ID in (Select id from mwsCustFieldDef WITH (nolock) Where MWFieldNum = ''9348'')
left join mwlcustomfield as CustFinalUWReview on CustFinalUWReview.loanapp_id = mwlLoanApp.id  and CustFinalUWReview.CustFieldDef_ID in (Select id from mwsCustFieldDef WITH (nolock) Where MWFieldNum = ''9327'') 
where '

if (@strFrom = 'afiles')
Begin
	Set @SQL = @SQL + ' ( CurrentStatus IN (''29 - In Underwriting'')  OR (CurrentStatus IN (''29.1 - In UW Processing'') AND ( (ISNULL(CustFinalUWReview.YNValue,''0'') = ''1'') OR (ISNULL(CustUWReview.YNValue,''0'') = ''1'') ) ) ) '
End
else if (@strFrom = 'dfiles' Or @strFrom = 'cs_dfiles')

Begin

	if(@strFrom = 'dfiles')
	Begin
		set @SQL = @SQL + 'isnull(mwlLoanApp.DecisionStatus,'''') <> '''' and'
	End

	if (@RoleID = 15 Or @RoleID = 17 Or @RoleID = 34)
	Begin
		Set @SQL = @SQL + ' ChannelType in(''CORRESPOND'',''BROKER'') and '
    End
	if (@RoleID = 10 Or @RoleID = 15 Or @RoleID = 34)
	Begin
		Set @SQL = @SQL + ' CurrentStatus IN (''29 - In Underwriting'',''29.1 - In UW Processing'') '
	End
	else
	Begin
		Set @SQL = @SQL + ' CurrentStatus IN (''29 - In Underwriting'') '
	End
End

else if (@strFrom = 'sfiles')
Begin
	Set @SQL = @SQL + ' CurrentStatus IN (''25 - Submitted to Underwriting'') '
End
if (len(@strID) = 0)
Begin
	Set @SQL = @SQL + ' and mwlUnderwritingSummary.underwriter_id in (select * from dbo.SplitString('''+@strID+''','','')) '
End
else
Begin
	Set @SQL = @SQL + ' and mwlUnderwritingSummary.underwriter_id in (select * from dbo.SplitString('''+@strID+''','','')) '
End
Set @SQL = @SQL + ' ORDER BY PER desc'

Print @SQL 
exec sp_executesql @SQL
--select @SQL

END

GO

