USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GFEVarianceCheck]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GFEVarianceCheck]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[USP_GFEVarianceCheck] --'0000412060'

(
      @LoanNumber VARCHAR(10)
)



AS

BEGIN


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
SELECT Row_Number() over (order by ToleranceCalc desc,ccl.GFESection,ccl.HudLineNum) RowId,ccl.HUDLineNum, ccl.HUDDesc, CAST(ISNULL(ghl.TotalAmt,0.00) as decimal(18,2)) AS GFE,  cast(ISNULL(ccl.TotalAmt,0.00) as decimal(18,2)) [HUD1],ToleranceCalc

 , CAST(CASE WHEN ISNULL(ghl.TotalAmt,0) =0 THEN 100   ELSE  isnull(((ISNULL(ccl.TotalAmt,0)-ghl.TotalAmt)*100)/ghl.TotalAmt,0) END as decimal(18,2)) Variance,ccl.IsComponent

FROM mwlLoanApp l

	  INNER JOIN mwlLoanData ld 

               ON l.ID=ld.ObjOwner_ID     

      INNER JOIN (select LoanData_ID,GFESection,ToleranceCalc,HudLineNum,HUDDesc,TotalAmt,IsComponent from mwlClosingCost where ltrim(rtrim(upper(ToleranceCalc)))  in ('ZERO','TENPCT') 
	  and (Isnull(IsComponent,'') = '' or IsComponent = 0)

				and	 isnull(TotalAmt,'')   <> ''

				UNION ALL

				select LoanData_ID,GFESection,ToleranceCalc,HudLineNum,HUDDesc,TotalAmt,IsComponent from mwlprepaid where ltrim(rtrim(upper(ToleranceCalc)))  in ('ZERO','TENPCT') 
				  and (Isnull(IsComponent,'') = '' or IsComponent = 0)

				and	 isnull(TotalAmt,'')   <> ''

				  ) ccl

                     ON ld.ID=ccl.LoanData_ID 

	  LEFT JOIN mwlGFELoanData gld 

               ON l.ID=gld.ObjOwner_ID 

      LEFT JOIN (select * from [mwlGFEHUDLine] where ltrim(rtrim(lower(GFEToleranceCalc))) in('zero','10%') 

				 	  and (Isnull(IsComponent,'') = '' or IsComponent = 0)
				  and isnull(TotalAmt,'') <> ''
				  )  ghl

               ON gld.ID=ghl.ObjOwner_ID AND ghl.HUDLineNum =ccl.HUDLineNum

	  WHERE  l.LoanNumber = @LoanNumber  

	  order by ToleranceCalc desc,ccl.GFESection,ccl.HudLineNum



END



GO

