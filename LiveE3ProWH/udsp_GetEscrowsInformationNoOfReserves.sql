USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetEscrowsInformationNoOfReserves]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetEscrowsInformationNoOfReserves]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[udsp_GetEscrowsInformationNoOfReserves]  
  @LoanNumber VARCHAR(10)               
AS               
BEGIN                                
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
	SET XACT_ABORT ON;  -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,         
						-- the entire transaction is terminated and rolled back.

	SELECT 	CityTaxes.pmtnumber                      AS CityPmtNumber, 
			CountryTaxes.pmtnumber                   AS CountyPmtNumber, 
			AssementAnnualAmt.pmtnumber              AS AssementPmtNumber, 
			hzdinsurace.pmtnumber					 AS HazardPmtNumber, 
			FloodInsuranceinPayment.pmtnumber        AS FloodPmtNumber, 
			mwlppe.otherppe1reserves                 AS Other1PmtNumber, 
			Other2Desc.pmtnumber                     AS Other2PmtNumber, 
			mwlppe.otherppe3reserves                 AS Other3PmtNumber, 
			mwlppe.otherppe4reserves                 AS Other4PmtNumber, 
			mwlppe.otherppe5reserves                 AS Other5PmtNumber
	FROM   mwlloanapp 
		   INNER JOIN dbo.mwlloandata loandata 
				   ON dbo.mwlloanapp.id = loandata.objowner_id 
				   AND loandata.Active = 1
		   LEFT JOIN mwlprepaid propertytax 
				  ON propertytax.loandata_id = loandata.id 
					 AND propertytax.hudlinenum = '1004'                  
					 AND propertytax.huddesc = 'Property taxes' 
		   LEFT JOIN mwlhudlinecomponent CityTaxes -- For CityPmtNumber
				  ON CityTaxes.objowner_id = propertytax.id 
					 AND CityTaxes.description = 'IQ - City taxes' 
		   LEFT JOIN mwlhudlinecomponent CountryTaxes -- For CountyPmtNumber
				  ON CountryTaxes.objowner_id = propertytax.id 
					 AND CountryTaxes.description = 'IQ - County taxes' 
		   LEFT JOIN mwlppe -- For Other1PmtNumber,Other3PmtNumber,Other4PmtNumber,Other5PmtNumber
				  ON dbo.mwlppe.loandata_id = loandata.id 
		   LEFT JOIN mwlprepaid hzdinsurace -- For HazardPmtNumber
				  ON hzdinsurace.loandata_id = loandata.id 
					 AND hzdinsurace.hudlinenum = '1002' 
		   LEFT JOIN mwlprepaid AssementAnnualAmt -- For AssementPmtNumber
				  ON AssementAnnualAmt.loandata_id = loandata.id 
					 AND AssementAnnualAmt.hudlinenum = '1005' 
		   LEFT JOIN mwlprepaid FloodInsuranceinPayment -- For FloodPmtNumber
				  ON FloodInsuranceinPayment.loandata_id = loandata.id 
					 AND FloodInsuranceinPayment.hudlinenum = '1006' 
		   LEFT JOIN mwlprepaid Other2Desc -- For Other2PmtNumber
				  ON Other2Desc.loandata_id = loandata.id 
					 AND Other2Desc.hudlinenum = '1008' 
	WHERE  loannumber = @LoanNumber            
END



GO

