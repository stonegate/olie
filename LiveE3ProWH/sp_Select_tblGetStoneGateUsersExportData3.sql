USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetStoneGateUsersExportData3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetStoneGateUsersExportData3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblGetStoneGateUsersExportData3]
(
@iUserid varchar(Max),
@oParterCompanyid varchar(Max)
)
as
begin
SELECT tblUsers.userid, tblUsers.ufirstname,tblUsers.MI, tblUsers.ulastname, tblUsers.companyname, tblUsers.uemailid, tblUsers.isactive, 
 tblUsers.PartnerCompanyid,tblUsers.userLoginid 
 FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID 
 LEFT OUTER JOIN tblroles Roles ON tblUsers.urole = Roles.id 
 where PartnerCompanyid=@oParterCompanyid and tblUsers.userid not in(@iUserid)
 order by  tblUsers.userid desc
end


GO

