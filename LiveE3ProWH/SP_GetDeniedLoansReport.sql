USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetDeniedLoansReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetDeniedLoansReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- Create Procedure Coding Starts Here  
 -- [SP_GetDeniedLoansReport] 0,'NOTICE','97 - Loan Denied','9/20/2012','10/20/2012', '93900C73BA31490FAEF120EDF66B6284,F4BC6BCB836D4251BA829A79DB70945A,039600DD5BFA4F5587510CBCC3266BF5,99AC435BD6FD4AC7983E3FDDF1739462,8E8C2405DD89423AB2CC4CBF8CC6DCBA,02DD0003B34B44E994AD7E788560E4A1,755A7C2174A74F9BA22E1882AF9C3984,74310032CA0749318FF1D027F400FE16,B143329312B4401A99483C777C1BBEA3,8D849C75B043476A96F85661849403E1,879408F1D31A4F9782565EC8FBD5E499,B8923DA174024045AA68662F4FC94CF0,FD9843C7C05E441CACE97CD09E1A82DC'
CREATE PROCEDURE [dbo].[SP_GetDeniedLoansReport]  
(  
@oRole int,  
@ConditionType nvarchar(2000),  
@LoanDenied nvarchar(2000),  
@strFromDate nvarchar(100),  
@strToDate nvarchar(100),  
@strID nVARCHAR(4000)  
)  
AS  
BEGIN 

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
 
Declare @SQL nvarchar(max)
Declare @SQL1 nvarchar(max)    
Set @SQL = ''  
Set @SQL = 'select ''E3'' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,
borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate,  
approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance''   
end as TransTYpe, case LoanData.FinancingType WHEN ''F'' then ''FHA'' WHEN ''C''  then ''CONVENTIONAL'' WHEN ''V'' then  ''VA'' End as LoanProgram,  
loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter  
,uEmail.Email as Underwriter_Email,CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' then RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,  
Lookups.DisplayString as CashOut,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate ,   
CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a  where DueBy=''Prior to Closing'' and Category !=''LENDER'' and  
Category !='''+ @ConditionType+''' and a.objOwner_ID IN (  SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))  
= 0 then 0 WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a   where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and  
Category !='''+ @ConditionType+''')and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'')  and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2    
WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 then 0   ELSE  (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where  
(DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !='''+ @ConditionType+''') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'') and a.objOwner_ID IN   
(SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !='''+ @ConditionType+''')  
and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER from mwlLoanApp  loanapp  
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  Inner join  dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id    
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id   
and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker'' left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id   
and  LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED'' Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and   
appStat.StatusDesc='''+ @ConditionType+''' Left join mwlApprovalStatus approvalStat  on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved''  
Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''  Left join   
dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and  Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''   
left join dbo.mwamwuser as uEmail on uSummary.UnderWriter_id = uEmail.id where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''     
and CurrentStatus  IN ('''+@LoanDenied+''') '  

if (@oRole= 11)  
Begin  
Set @SQL =  @SQL+'  and channeltype in (''RETAIL'',''BROKER'',''CORRESPOND'') '  
END  
else  
Begin  
Set @SQL =  @SQL+'    and channeltype in (''BROKER'',''CORRESPOND'') '  
end  
Set @SQL =  @SQL+' and appStat.StatusDateTime between '''+ @strFromDate +'''  and    DATEADD(d, 1, '''+ @strToDate +''')'  

if (@oRole != 0 and @oRole != 11)  
print '2'
Begin  
	print '1'
    if (LEN(@strID)=  0)  
    Begin  

        if (@oRole= 2)  
        Begin  

        Set @SQL1 =  @SQL1+' and (Originator_id  in (select * from dbo.SplitString('''+ @strID +''','','')))'  
       END  
        else if (@oRole= 3 or @oRole= 20 or @oRole= 21)  
        Begin  
       Set @SQL1 =  @SQL1+' and (loanapp.ID in (select * from dbo.SplitString('''+ @strID +''','','')))'  
       END  
        else  
        Begin  
      Set @SQL1 =  @SQL1+' and (Originator_id in (select * from dbo.SplitString('''+ @strID +''','','')))'  
       END  
   END  

    else  
    Begin  

        if (@oRole= 2)  
        Begin  
        Set @SQL1 =  @SQL1+' and (Originator_id in (select * from dbo.SplitString('''+ @strID +''','','')))'  
       END  
        else if (@oRole=3 or @oRole=20 or @oRole=21)  
        Begin  
        Set @SQL1 =  @SQL1+'    and (loanapp.ID in  (select * from dbo.SplitString('''+ @strID +''','','')))'  
       END  
        else  
        Begin  
         Set @SQL1 =  @SQL1+'   and (Originator_id in (select * from dbo.SplitString('''+ @strID +''','','')))'  
       END  
   END  
END  
            Set @SQL1 =  @SQL1+' order by appStat.StatusDateTime '  
Print @SQL 
Print @SQL1     
--EXECUTE sp_executesql @SQL 
exec (@SQL + @SQL1)
END   



GO

