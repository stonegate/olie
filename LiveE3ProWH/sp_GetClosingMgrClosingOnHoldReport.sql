USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetClosingMgrClosingOnHoldReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetClosingMgrClosingOnHoldReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================
-- Author:		<Stonegate>
-- Create date: <10/8/2012>
-- Description:	<This will Get Closing Manager Closing On Hold Report>
-- ====================================================================
CREATE PROCEDURE [dbo].[sp_GetClosingMgrClosingOnHoldReport] 
(
	@RoleID int,
	@strFrom varchar(50)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)

Declare @SQL nvarchar(max)
set @SQL=''
Set @SQL = 'Select ''E3'' as DB,loanapp.processorname,loanapp.DocumentPreparerName,loanapp.CurrDecStatusDate,LockRecord.LockDateTime,
Offices.Office as office,COffices.Office as coffice,loanapp.CurrDecStatusDate as Decisionstatusdate,loanapp.CurrPipeStatusDate as CurrentStatusDate ,
loanapp.ChannelType as ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,
LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,
'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS StatusDate,appReceived.StatusDateTime as AppDate,
CASE TransType when NULL THEN '''' WHEN ''P'' THEN ''Purchase money first mortgage'' WHEN ''R'' THEN ''Refinance'' WHEN ''2'' THEN ''Purchase money second mortgage'' WHEN ''S'' THEN ''Second mortgage, any other purpose'' WHEN ''A'' THEN ''Assumption'' WHEN ''HOP'' THEN ''HELOC - other purpose'' WHEN ''HP'' THEN ''HELOC - purchase'' ELSE '''' END AS TransType,
Case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN ''VA'' End as LoanProgram,
loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,
LockRecord.LockExpirationDate,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,
CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,Loandata.LoanProgDesc as ProgramDesc ,Cust.stringvalue as UnderwriterType,
CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'' and a.objOwner_ID IN (SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0
WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'')
And a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0 ELSE (SELECT count(a.ID) as TotalCleared
FROM dbo.mwlCondition a where(DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') 
and a.objOwner_ID IN (SELECT ID FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') 
and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER,
BOffices.Office as OFFICE_NAME1 from mwlLoanApp loanapp 
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and Sequencenum=1 
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id
and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK'' and LockRecord.Status<>''CANCELED''
Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = loanapp.id and COffices.InstitutionType = ''CORRESPOND'' and COffices.objownerName=''Contacts''
Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.InstitutionType = ''BRANCH'' and BOffices.objownerName=''BranchInstitution''
Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id '

if (@RoleID = 30)
Begin
	set @SQL = @SQL + ' and loanapp.CurrentStatus IN (''09 - Application Received'',''43 - Closing on Hold'') '
End
else
Begin
	set @SQL = @SQL + ' and appStat.StatusDesc=''01 - Registered'' '
End

set @SQL = @SQL + ' Left join dbo.mwlInstitution as Offices on Offices.ObjOwner_id = loanapp.id and Offices.InstitutionType = ''Broker'' and Offices.objownerName=''Broker'' '
set @SQL = @SQL + ' Left join mwlAppStatus appReceived on loanapp.ID=appReceived.LoanApp_id and appReceived.StatusDesc IN (''09 - Application Received'') '
set @SQL = @SQL + ' Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' '
set @SQL = @SQL + ' left join mwsCustFieldDef as mCust on  mCust.CustomFieldName like ''Correspondent Type%'' '
set @SQL = @SQL + ' left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID =mCust.id '

if (@strFrom = 'clonhold')
Begin
	set @SQL = @SQL + ' where CurrentStatus  IN (''43 - Closing on Hold'') '
End

set @SQL = @SQL + ' and loanapp.ChannelType in(''BROKER'',''Retail'') '
set @SQL = @SQL + ' and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' '
set @SQL = @SQL + ' ORDER BY PER desc '

Print @SQL
exec sp_executesql @SQL

END

GO

