USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUserRegionMapping]') AND type in (N'U'))
DROP TABLE [dbo].[tblUserRegionMapping]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserRegionMapping](	  [UserRegionId] BIGINT NOT NULL IDENTITY(1,1)	, [UserId] BIGINT NULL	, [RoleId] INT NULL	, [RegionId] BIGINT NULL	, [Region] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedBy] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedDate] DATETIME NULL	, [ModifiedBy] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ModifiedDate] DATETIME NULL	, [Status] VARCHAR(2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblUserRegionMapping] PRIMARY KEY ([UserRegionId] ASC))USE [HomeLendingExperts]
GO

