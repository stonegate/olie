USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPartnerCompanyFromOLIEDBForEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPartnerCompanyFromOLIEDBForEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE GetPartnerCompanyFromOLIEDBForEmail  
(  
@userID int  
)  
AS  
BEGIN  
   SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
SET XACT_ABORT ON;  
 Select CompanyName From tblusers Where userid = @userID  
   
END  

GO

