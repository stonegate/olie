USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAppSettings]') AND type in (N'U'))
DROP TABLE [dbo].[tblAppSettings]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAppSettings](	  [KeyId] SMALLINT NOT NULL IDENTITY(1,1)	, [KeyName] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [KeyValue] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Description] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedOnDate] DATETIME NULL	, [CreatedBy] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblAppSettings] PRIMARY KEY ([KeyId] ASC))USE [HomeLendingExperts]
GO

