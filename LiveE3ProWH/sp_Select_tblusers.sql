USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblusers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblusers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[sp_Select_tblusers]   
 -- Add the parameters for the stored procedure here  
  @userid int 
AS  
BEGIN  
 
 SELECT *, isnull(IsUserInNewProcess,0) as IsUserInNewProcess1,isnull(chkpricelockloan,0) as chkpricelockloan1 FROM [tblusers] 
WHERE userid= @userid

END



GO

