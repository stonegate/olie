USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetWholeSaleUserList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetWholeSaleUserList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Stonegate    
-- Create date: 10th Apr 2012    
-- Description: This will get user list to bind in the grid    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_GetWholeSaleUserList]    
(    
@sortField varchar(500),    
@sortOrder varchar(100),    
@whereClause varchar(1000),    
@PageNo int,     
@PageSize int,
@ActionID int,
@userId varchar(50)
)     
AS    
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

IF @ActionID = 1 
	BEGIN    
		IF @sortField  = ''                                
		  set @sortField = 'tblUsers.userid'    
		    
		SET @sortField = @sortField + ' ' + @sortOrder    
		  
		set @whereClause =  REPLACE(@whereClause,'[uurole]','tblroles.roles')                                      
		    
		Declare @sql nvarchar(4000)    
		    
		set @sql = ''                                
			 set @sql = 'Select count(*) from ( '    
			 set @sql =  @sql + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole,   
		tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,   
		tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime,   
		tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,tblUsers.PartnerCompanyid,tblusers.AEID,  
		IsUserInNewProcess,userLoginid   
		FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID LEFT OUTER JOIN tblRoles ON tblUsers.urole = tblRoles.ID  '    
		    
		IF @whereClause <> ''                                         
			BEGIN                                     
			set @sql = @sql  + ' Where ' + @whereClause                               
			END                        
		    
		set @sql = @sql + ' group by tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole,   
		tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,   
		tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime,   
		tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,tblUsers.PartnerCompanyid,tblusers.AEID,  
		IsUserInNewProcess,userLoginid '                                 
		 set @sql = @sql + ' ) as t2 '     
		    
		print @sql    
		exec sp_executesql @sql      
		    
		    
		set @sql = ''                                
			 set @sql = 'Select * from ( '     
		set @sql =  @sql + 'SELECT             
				ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole,   
		tblUsers.isadmin, Case When tblUsers.isactive = 1 Then ''Active'' Else ''Inactive'' End as status, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,   
		tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime,   
		tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,tblUsers.PartnerCompanyid,tblusers.AEID,  
		IsUserInNewProcess,userLoginid   
		FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID LEFT OUTER JOIN tblRoles ON tblUsers.urole = tblRoles.ID '    
		    
		IF @whereClause <> ''                                         
			BEGIN                                     
			set @sql = @sql  + ' Where ' + @whereClause                               
			END                        
		    
		set @sql = @sql + ' group by tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole,   
		tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,   
		tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime,   
		tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,tblUsers.PartnerCompanyid,tblusers.AEID,  
		IsUserInNewProcess,userLoginid '                                 
		set @sql = @sql + ' ) as t2 '    
		    
		set @sql = @sql + ' Where Row between (' + CAST( @PageNo AS NVARCHAR)  + ' - 1) * ' + Cast(@PageSize as nvarchar) + ' + 1 and ' + cast(@PageNo as nvarchar) + ' * ' + cast(@PageSize as nvarchar)    
		  
		print @sql                              
		exec sp_executesql @sql    
	END 
IF @ActionID = 2
	BEGIN
		Declare @PartnerCompID nvarchar(255)
	    Select @PartnerCompID = PartnerCompanyid from tblusers where userid = @userId		
		IF @sortField  = ''                                
		  set @sortField = 'tblUsers.userid'    
		    
		SET @sortField = @sortField + ' ' + @sortOrder    
		  
		set @whereClause =  REPLACE(@whereClause,'[uurole]','tblroles.roles')                                      
		    
		Declare @sqlnew nvarchar(4000)    
		    
		set @sqlnew = ''                                
			 set @sqlnew = 'Select count(*) from ( ' 
			 set @sqlnew =  @sqlnew + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole, tblUsers.isadmin, 
				tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent, tblUsers.upassword, tblUsers.maxloginattempt, 
				tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,
				tblUsers.PartnerCompanyid,tblusers.AEID,IsUserInNewProcess,userLoginid 
				FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID 
				LEFT OUTER JOIN tblRoles ON tblUsers.urole = tblRoles.ID 
				where tblUsers.PartnerCompanyid = ''' + @PartnerCompID + ''' And userid not in('+ @userId +') '    

			IF @whereClause <> ''                                         
			BEGIN                                     
			set @sqlnew = @sqlnew  + ' And ' + @whereClause                               
			END                        
		    
			set @sqlnew = @sqlnew + ' group by tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole, tblUsers.isadmin, 
				tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent, tblUsers.upassword, tblUsers.maxloginattempt, 
				tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,
				tblUsers.PartnerCompanyid,tblusers.AEID,IsUserInNewProcess,userLoginid '                                 
			set @sqlnew = @sqlnew + ' ) as t2 '
			print @sqlnew    
			exec sp_executesql @sqlnew 

			set @sqlnew = ''
			set @sqlnew = 'Select * from ( '     
			set @sqlnew =  @sqlnew + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole, tblUsers.isadmin, 
				Case When tblUsers.isactive = 1 Then ''Active'' Else ''Inactive'' End as status, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent, tblUsers.upassword, tblUsers.maxloginattempt, 
				tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,
				tblUsers.PartnerCompanyid,tblusers.AEID,IsUserInNewProcess,userLoginid 
				FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID 
				LEFT OUTER JOIN tblRoles ON tblUsers.urole = tblRoles.ID 
				where tblUsers.PartnerCompanyid = ''' + @PartnerCompID + ''' And userid not in('+ @userId +') '    

			IF @whereClause <> ''                                         
			BEGIN                                     
			set @sqlnew = @sqlnew  + ' And ' + @whereClause                               
			END                        
		    
			set @sqlnew = @sqlnew + ' group by tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole, tblUsers.isadmin, 
				tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent, tblUsers.upassword, tblUsers.maxloginattempt, 
				tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,
				tblUsers.PartnerCompanyid,tblusers.AEID,IsUserInNewProcess,userLoginid '                                 
			set @sqlnew = @sqlnew + ' ) as t2 '
			set @sqlnew = @sqlnew + ' Where Row between (' + CAST( @PageNo AS NVARCHAR)  + ' - 1) * ' + Cast(@PageSize as nvarchar) + ' + 1 and ' + cast(@PageNo as nvarchar) + ' * ' + cast(@PageSize as nvarchar)    
			print @sqlnew    
			exec sp_executesql @sqlnew 
	END 
   
END 

GO

