USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetContactGUID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetContactGUID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


     -- =============================================      
-- AUTHOR:  <Mukesh Kumar>      
-- CREATE DATE: <4/10/2013>      
-- DESCRIPTION: < Used for the "Select From Setup" Process. Script used to get the Originator Name,       
   --Originator GUID, Company GUID, Underwriting GUID>      
-- =============================================     
CREATE PROCEDURE [dbo].[GetContactGUID]      
       
 @CompanyGUID VARCHAR(50),      
 @FourthPartyGUID VARCHAR(50)      
      
AS      
 /* --------------------------------------------------------------------------------------      
       
 Name:       
  GetContactGUID.sql      
      
 Description:      
  Used for the "Select From Setup" Process. Script used to get the Originator Name,       
   Originator GUID, Company GUID, Underwriting GUID      
?      
 DDL information:      
  Database    - InterlinqE3      
  Table       - mwcContact, mwcInstitution, mwaMWUser      
?      
 Steps:      
  Step 0.0 - Test Data      
  Step 1.0 - Get the Originator Name, Originator GUID, Company GUID, Underwriting GUID      
       
 Change History:       
  2013/05/03   - Bultemeier, Shelby      
   Create Script      
       
 ---------------------------------------------------------------------------------------*/      
      
BEGIN      
      
 SET NOCOUNT ON;      
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)      
 SET XACT_ABORT ON;         -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,       
              -- the entire transaction is terminated and rolled back.      
      
   
 IF @FourthPartyGUID IS NULL      
 BEGIN      
  SELECT [mwcContact].[LastName]+', '+[mwcContact].[FirstName] AS [OriginatorName], [mwcContact].[ID] AS [OriginatorGUID], [Institution_ID] AS [CompanyGUID], NULL AS UnderWriterGUID, InstitutionType      
  FROM [dbo].[mwcContact]       
   INNER JOIN [dbo].[mwcInstitution] ON [mwcContact].[Institution_ID] = [mwcInstitution].[ID]      
  WHERE [mwcContact].[Institution_ID] = @CompanyGUID      
   AND Status = 'Active'      
  ORDER BY OriginatorName      
 END      
 ELSE      
 BEGIN      
  SELECT [mwcContact].[LastName]+', '+[mwcContact].[FirstName] AS [OriginatorName], [mwcContact].[ID] AS [OriginatorGUID], [Institution_ID] AS [CompanyGUID], [mwaMWUser].[ID] AS UnderWriterGUID, InstitutionType      
  FROM [dbo].[mwcContact]       
   INNER JOIN [dbo].[mwcInstitution] ON [mwcContact].[Institution_ID] = [mwcInstitution].[ID]      
   LEFT JOIN [dbo].[mwaMWUser] ON [mwaMWUser].FullName = '4th party originator'      
  WHERE [mwcContact].[Institution_ID] = @FourthPartyGUID      
   AND Status = 'Active'      
  ORDER BY OriginatorName      
 END      
      
END 



GO

