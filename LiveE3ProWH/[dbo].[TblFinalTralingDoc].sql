USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TblFinalTralingDoc]') AND type in (N'U'))
DROP TABLE [dbo].[TblFinalTralingDoc]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblFinalTralingDoc](	  [LoanDocId] BIGINT NOT NULL IDENTITY(1,1)	, [DocName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [userid] INT NULL	, [createddate] DATETIME NULL DEFAULT(getdate())	, [loan_no] VARCHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL DEFAULT((1))	, [Comments] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_TblFinalTralingDoc] PRIMARY KEY ([LoanDocId] ASC))USE [HomeLendingExperts]
GO

