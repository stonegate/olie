USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Reports_GetLoanDetails_ConditionalData_GetConditions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Reports_GetLoanDetails_ConditionalData_GetConditions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  

-- Author:  Stonegate  

-- Create date: April, 18 2012  

-- Description: This will give Loan details for the loan number passed  

--sp_Reports_GetLoanDetails_ConditionalData('0000308185')

--exec sp_Reports_GetLoanDetails_ConditionalData_GetConditions '0000395901'

-- modified Narayan for bugid OLIEPS-81

-- =============================================  

CREATE PROCEDURE [dbo].[sp_Reports_GetLoanDetails_ConditionalData_GetConditions]      

( 
@LoanNumber nvarchar(50)
)
AS
BEGIN 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads.
SET FMTONLY OFF
 Declare @StatusDesc nvarchar(200)      

Declare @CurrentStat nvarchar(200)      

Declare @iDecisionStatus int      

  Declare @lNmuber varchar(50)    

set @lNmuber = @LoanNumber    

Set @iDecisionStatus = 0      

select @StatusDesc =AppStatus.StatusDesc,@CurrentStat=CurrentStatus from mwlloanapp loanapp Left join mwlApprovalStatus AppStatus on loanapp.ID= AppStatus.LoanApp_id where loannumber= @LoanNumber      

Print 'status Desc: ' + @StatusDesc      

If @StatusDesc <> '' Or @StatusDesc is null      

Begin    

 Declare @Query nvarchar(2000)      

 Set @Query = 'SELECT Name as descriptn,DisplayOrder as id_value ,case DisplayOrder when ''4'' then ''1''  when ''1'' then ''2'' when ''3'' then ''4''  when ''2'' then ''3'' when ''5'' then ''5'' when ''9'' then ''6'' END AS ''Value''  FROM mwsConditionDueBy '      

 --If @CurrentStat <> '54 - Purchase Pending'

--Begin      

 --set @Query = @Query +' where Name <> ''Prior to Purchase'' '      

 --End      

--if (@CurrentStat = '54 - Purchase Pending' AND @CurrentStat <> '51 - Purchase Sub on Hold')

--begin

--Set @Query = @Query + ' where Name <> ''Purchase on Hold'''

--end

--if (@CurrentStat <> '51 - Purchase Sub on Hold' AND @CurrentStat <> '54 - Purchase Pending' )

--begin

--Set @Query = @Query + ' and Name <> ''Purchase on Hold'''

--end   

End      

Else      

Begin      

 Set @Query = 'SELECT Name as descriptn,DisplayOrder as id_value ,case DisplayOrder when ''4'' then ''1''  when ''1'' then ''2'' when ''3'' then ''4''  when ''2'' then ''3'' when ''5'' then ''5'' when ''9'' then ''6'' END AS ''Value''      

 FROM mwsConditionDueBy where Name = ''Prior to Underwriting'' '      

End      

      

--set @Query = '  select ''E3'' as DB,OrderNum as cond_no,a.ID as record_id,a.ObjOwner_Id as parent_id,a.Description as cond_text,      

--     CurrentState as STATUS,Category as descriptn ,DueBy,       

--     '' as cond_cleared_date,'' as  cond_recvd_date, b.DisplayOrder as id_value ,a.ShortName       

--     FROM dbo.mwlCondition a       

--     left outer join mwsConditionDueBy b on a.Dueby = b.Name       

--     Where '      

      

  --EXECUTE  (@Query)    
  --PRINT(@Query
EXECUTE sp_executesql @Query, N'@LoanNumber nvarchar(50)', @lNmuber    

END 

GO

