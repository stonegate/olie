USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GETDOOAllMandatoryFields]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GETDOOAllMandatoryFields]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GETDOOAllMandatoryFields]
(
@LoanNumber varchar(100) ,
@StepNumber bigint 
)

As

BEGIN

  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   

  SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,           

                     -- the entire transaction is terminated and rolled back.   
--Step 2

delete from tblDooMandatoryFieldRecords where LoanNumber = @LoanNumber 
and CASE WHEN @StepNumber = 1 THEN @StepNumber  
            WHEN @StepNumber = 2 THEN StepNumber
              WHEN @StepNumber = 3 THEN StepNumber 
            WHEN @StepNumber = 4 THEN StepNumber   
            WHEN @StepNumber = 5 THEN StepNumber   
            WHEN @StepNumber = 6 THEN StepNumber ELSE StepNumber END   = @StepNumber

IF @StepNumber IN (1 ,2 )
BEGIN


insert into tblDooMandatoryFieldRecords
SELECT 
            @LoanNumber LoanNumber,'2' StepNumber,
          'CondoDevname' FieldName,      
       mwlsubjectproperty.condodevname FieldValue,'N'

FROM   mwlsubjectproperty   

       INNER JOIN mwlloanapp   

       ON mwlsubjectproperty.loanapp_id = mwlloanapp.id   

          INNER JOIN mwlloandata ON mwlloanapp.id = mwlloandata.objowner_id and mwlloandata.active = 1      

WHERE  loannumber = @LoanNumber     and upper(mwlsubjectproperty.propertytype) IN ('CONDO','PUD') and isnull(CAST(mwlsubjectproperty.condodevname as varchar(Max)) ,'') = ''

END


DECLARE @intHudLineNum AS INT 

       SET @intHudLineNum = 901 -- Hud line number available in mwlprepaid  table and usefull for fetching interset From Date and TO Date which are showing at STEP7 closing information


       --Step 3
       IF @StepNumber IN (1 ,3 )
BEGIN




insert into tblDooMandatoryFieldRecords
       Select @LoanNUmber LoanNumber,'3' StepNumber,FieldName,FieldValue ,Conditions from 
       (
       Select * from 
       (
        SELECT               
                isnull(Convert(varchar(10),Instreset.FromDate,121),'') FromDate,
                isnull(Convert(varchar(10),Instreset.ToDate,121),'') ToDate,
                isnull(Convert(varchar(10),loanapp.DateofDocuments,121),'') DateofDocuments ,
                isnull(Convert(varchar(10),loandata.firstpaydate,121),'') DisbursementDate,
               'N' Conditions
        FROM    mwlloanapp loanapp

                INNER JOIN mwlloandata loandata ON loanapp.ID = loandata.ObjOwner_ID and  loandata.Active = 1

                LEFT JOIN dbo.mwlInstitution AS Institution ON Institution.ObjOwner_id = loanapp.id   AND Institution.InstitutionType = loanapp.ChannelType    


                LEFT JOIN mwlphonenumber Companyphone ON Institution.id = Companyphone.objowner_id

                                                AND Companyphone.phonetype = 'Business'     

                LEFT JOIN mwlphonenumber CompanyFax ON Institution.id = CompanyFax.objowner_id

                                                AND CompanyFax.phonetype = 'Fax'

                LEFT JOIN mwllookups lookups ON lookups.objectname = 'MwlLoanapp'

                                                AND FieldName = 'Transtype'

                                                AND Lookups.BOCode = loanapp.Transtype    

                 LEFT JOIN mwlprepaid Instreset ON Instreset.loandata_id = loandata.id

                                                AND Instreset.hudlinenum = @intHudLineNum

        WHERE   loanapp.LoanNumber = @LoanNumber

              ) A  unpivot(FieldValue for FieldName in(FromDate,ToDate,DateofDocuments,DisbursementDate)) P

              )B where (isnull(FieldValue,'') = ''  or (FieldValue='1900-01-01'))
              

              END
--Step 4 Start
		Declare @PhoneNumber VARCHAR(100)
              
    DECLARE @CompName VARCHAR(500)

       DECLARE @intMWFieldNum     AS INT 

       SET @intMWFieldNum = 9625 -- Customfield id 9625 is used for Title Policy Amount which show up at STEP4

       
              DECLARE @intNextTaxesDue AS INT 

              SET @intNextTaxesDue = 9842 --MWFieldNum pointing to "Next Taxes Due"

                           DECLARE @intHazardInsuranceDeductible AS INT 

              SET @intHazardInsuranceDeductible = 9627  --MWFieldNum pointing to "Hazard Insurance Deductible"

              IF @StepNumber IN (1 ,4 )
BEGIN

 SELECT  @CompName = institle.Company
        FROM    dbo.mwlLoanApp loanapp
                LEFT JOIN dbo.mwlInstitution institle ON institle.ObjOwner_ID = loanapp.ID
                                                         AND InstitutionType = 'TITLE'
                                                         AND ObjOwnerName = 'Contacts'
           LEFT JOIN ( SELECT  ID ,
                                    LoanApp_ID ,
                                    StringValue
                            FROM    mwlCustomfield
                            WHERE   CustFieldDef_ID = ( SELECT
                                                              ID
                                                        FROM  mwsCustFieldDef
                                                        WHERE MWFieldNum = @intMWFieldNum
                                                      )
                          ) mwCustFld ON mwCustFld.LoanApp_ID = loanapp.ID
                LEFT JOIN dbo.mwlInstitution CustServicer ON CustServicer.ObjOwner_ID = loanapp.ID
                                                             AND CustServicer.ObjOwnerName = 'Servicer'
     
        WHERE   LoanNumber = @LoanNumber
--End Get company name


--Get Company Phonenumber

			Set @PhoneNumber = 	
       ( SELECT top 1  mwlphonenumber.phonenumber 
        FROM    mwlLoanApp ,
                mwlInstitution ,
                mwlPhoneNumber
        WHERE   mwlLoanApp.ID = mwlInstitution.ObjOwner_ID
                AND mwlInstitution.ID = mwlPhoneNumber.ObjOwner_ID
                AND mwlloanapp.loannumber = @LoanNumber
                AND mwlinstitution.company = @CompName
                AND mwlphonenumber.phonetype = 'Business')
insert into tblDooMandatoryFieldRecords
       Select @LoanNUmber LoanNumber,'4' StepNumber,FieldName,FieldValue,Conditions 
 from 
       (
              Select *  from 
              (
                     SELECT       

                                  
                                  CAST(isnull(institle.Company,'') as varchar(Max)) Company,--

                                  CAST(isnull(institle.street,'') as varchar(Max)) street,--

                                  CAST(isnull(institle.city,'') as varchar(Max)) city,

                                  CAST(isnull(institle.[state],'') as varchar(Max)) [state],--

                                  CAST(isnull(institle.zipcode,'') as varchar(Max)) zipcode ,--

                                  CAST(isnull(institle.companyemail,'') as varchar(Max)) companyemail ,--

                                  CAST(isnull(institle.country,'') as varchar(Max)) country,--

                                  CAST(isnull(@PhoneNumber,'') as varchar(Max)) phonenumber,--

                                  CAST(isnull(loanapp.TitleOrderNumber,'') as varchar(Max)) AS CommitmentNo ,--

                                  CAST(isnull(CustServicer.Company,'') as varchar(Max)) AS ProposedInsured ,--

                                  CAST(isnull(Custinsuraceamt.CurrencyValue,'') as varchar(Max)) AS InsuranceAmount ,--

                                  CAST(isnull(CONVERT(VARCHAR(10),loanapp.TitleReportDate,121),'') as varchar(Max)) AS EffectiveDate ,--,--
                                  'N' Conditions 

                     FROM    dbo.mwlLoanApp loanapp

                                  LEFT JOIN dbo.mwlInstitution institle ON institle.ObjOwner_ID = loanapp.ID

                                                                                                       AND InstitutionType = 'TITLE'

                                                                                                       AND ObjOwnerName = 'Contacts'

                                  LEFT JOIN ( SELECT  ID ,

                                    LoanApp_ID ,

                                    CurrencyValue

                            FROM    mwlCustomfield

                            WHERE   CustFieldDef_ID = ( SELECT

                                                              ID

                                                        FROM  mwsCustFieldDef

                                                        WHERE MWFieldNum = @intMWFieldNum

                                                      )

                          ) Custinsuraceamt ON Custinsuraceamt.LoanApp_ID = loanapp.ID

                LEFT JOIN dbo.mwlInstitution CustServicer ON CustServicer.ObjOwner_ID = loanapp.ID

                AND CustServicer.ObjOwnerName = 'Servicer'

                        --   LEFT JOIN   mwlPhoneNumber MP ON CustServicer.ID = MP.ObjOwner_ID and institle.Company = CustServicer.Company AND MP.phonetype = 'Business'

                           WHERE   LoanNumber = @LoanNumber

              )      A UNPIVOT (FieldValue for FieldName IN (Company,street,city,[state],zipcode,companyemail,country,phonenumber,CommitmentNo,ProposedInsured,InsuranceAmount,EffectiveDate))P
              
       ) B where isnull(FieldValue,'') = '' or (FieldValue='1900-01-01')

       END 




              IF EXISTS (Select state From mwlsubjectproperty Where loanapp_id = (Select id From mwlloanapp Where loannumber = @LoanNumber) AND state in ('AK','AZ','CA','ID','NV','NM','OR','UT','WA') )

              BEGIN




              IF @StepNumber IN (1 ,4 )
BEGIN




	   SELECT  @CompName = institle.Company
				FROM    dbo.mwlLoanApp loanapp
								LEFT JOIN dbo.mwlInstitution institle ON institle.ObjOwner_ID = loanapp.ID
																														AND InstitutionType = 'SETTLEMT'
																															AND ObjOwnerName = 'SetAgent'
				WHERE   LoanNumber = @LoanNumber


			Set @PhoneNumber = 	 (SELECT top 1  mwlphonenumber.phonenumber 
              
        FROM    mwlLoanApp ,
                mwlInstitution ,
                mwlPhoneNumber
        WHERE   mwlLoanApp.ID = mwlInstitution.ObjOwner_ID
                AND mwlInstitution.ID = mwlPhoneNumber.ObjOwner_ID
                AND mwlloanapp.loannumber = @LoanNumber
                AND mwlinstitution.company = @CompName
                AND mwlphonenumber.phonetype = 'Business')


              insert into tblDooMandatoryFieldRecords
              Select  @LoanNUmber LoanNumber,'4' StepNumber,FieldName,FieldValue,Conditions           
 from 
              (
              Select * from 
              (
              SELECT  

                                         CAST(isnull(institle.Company,'') as varchar(Max)) Company,

                                         CAST(isnull(institle.street,'') as varchar(Max)) street,

                                         CAST(isnull(institle.city,'') as varchar(Max)) city,

                                         CAST(isnull(institle.[state],'') as varchar(Max)) state,

                                         CAST(isnull(institle.zipcode,'') as varchar(Max)) zipcode,

                                         CAST(isnull(institle.companyemail,'') as varchar(Max)) companyemail,

                                         CAST(isnull(institle.country,'')      as varchar(Max)) country,

                                         CAST(isnull(@PhoneNumber,'')   as varchar(Max)) phonenumber ,

                                         'N' Conditions                   

                           FROM    dbo.mwlLoanApp loanapp

                           LEFT JOIN dbo.mwlInstitution institle ON institle.ObjOwner_ID = loanapp.ID    AND InstitutionType = 'SETTLEMT'

                           AND ObjOwnerName = 'SetAgent'            
                           
                           LEFT JOIN  mwlPhoneNumber on  institle.ID = mwlPhoneNumber.ObjOwner_ID       AND mwlphonenumber.phonetype = 'Business'      

                           WHERE   LoanNumber = @LoanNumber
              ) A unpivot(FieldValue for FieldName in(Company,street,city,[state],zipcode,companyemail,country,phonenumber)) P
              ) B where isnull(FieldValue,'') = '' 

              END

              END 

              
--SELECT * FROM tblEscrowDueDates WHERE LoanNumber = @LoanNumber ORDER BY EscrowItem,[Order]

IF @StepNumber IN (1 ,5 )
BEGIN
insert into tblDooMandatoryFieldRecords
       Select @LoanNUmber LoanNumber,'5' StepNumber,FieldName,FieldValue,Conditions from 
       (
       Select * from 
       (

       Select * from (
       SELECT 

       
              CAST(isnull(mwlinstitution.company,'') as varchar(Max))company , 

              CAST(isnull(mwlinstitution.street,'') as varchar(Max)) street, 

              CAST(isnull(mwlinstitution.city,'') as varchar(Max))city , 

              CAST(isnull(mwlinstitution.[state],'') as varchar(Max)) [state], 

              CAST(isnull(mwlinstitution.zipcode,'') as varchar(Max)) zipcode, 

              CAST(isnull(mwlinstitution.country,'') as varchar(Max)) country, 

              CAST(isnull(Hzdphonenumber.phonenumber,'') as varchar(Max)) phonenumber,

              CAST(isnull(mwlhazard.policynumber,'') as varchar(Max)) policynumber,
              CAST(isnull(mwlhazard.hazardinscoverage,'') as varchar(Max)) hazardinscoverage,
              CAST( isnull(CONVERT(VARCHAR(10),prepaid.annualpayduedate,121),'') as varchar(Max)) annualpayduedate, 
              CAST(isnull(custfield.currencyvalue,'') as varchar(Max)) AS currencyvalue,
              CAST(isnull(CONVERT(varchar(10),TaxesNextPayment.datevalue,121),'') as varchar(Max)) AS TaxtNextPaymentDate,
              CAST(isnull(loandata.waiveescrows,'') as varchar(Max))     AS WaiveEscrows,
              CAST(isnull(CityTaxes.annualamt,0) +isnull(CountryTaxes.annualamt,0) +isnull(AssementAnnualAmt.annualamt,0) +isnull(hzdinsurace.annualamt,0) +isnull(FloodInsuranceinPayment.annualamt,0) +isnull(Other1Desc.annualamt,0) +isnull(Other2Desc.annualamt,0) +isnull(Other3Desc.annualamt,0) +isnull(Other4Desc.annualamt,0) +isnull(Other5Desc.annualamt,0) as varchar(Max)) AS  TotalAmt ,

              CAST(CASE WHEN isnull(loandata.waiveescrows ,'0') = '0' THEN 'Y' ELSE 'N' END     as varchar(Max)) AS Conditions

              --CAST(CityTaxes.annualamt  as varchar(Max))                    AS CityAnnualAmount, 
              --CAST(CountryTaxes.annualamt   as varchar(Max))                AS CoutryAnnualAmount,
              --CAST(AssementAnnualAmt. annualamt    as varchar(Max))         AS AssessmentAnnualAMount, 
              --CAST(hzdinsurace.annualamt   as varchar(Max))                      AS HazardAnnualAmount,       
              --CAST(FloodInsuranceinPayment.annualamt     as varchar(Max))   AS FloodAnnualAmount, 
              --CAST(Other1Desc.annualamt   as varchar(Max))                  AS Other1Amt, 
              --CAST(Other2Desc.annualamt      as varchar(Max))               AS Other2Amt, 
              --CAST(Other3Desc.annualamt          as varchar(Max))           AS Other3Amt, 
              --CAST(Other4Desc.annualamt     as varchar(Max))                AS Other4Amt, 
              --CAST(Other5Desc.annualamt      as varchar(Max))               AS Other5Amt,
              --CAST(CityTaxes.annualamt + CountryTaxes.annualamt + AssementAnnualAmt.annualamt+ hzdinsurace.annualamt+ FloodInsuranceinPayment.annualamt+ Other1Desc.annualamt+ Other2Desc.annualamt+ Other3Desc.annualamt+ Other4Desc.annualamt+ Other5Desc.annualamt  as varchar(Max)) AS  TotalAmt 


FROM   mwlloanapp 

       INNER JOIN dbo.mwlloandata loandata 

               ON dbo.mwlloanapp.id = loandata.objowner_id 

                        AND loandata.Active = 1

       LEFT JOIN mwlsubjectproperty 

              ON mwlsubjectproperty.loanapp_id = mwlloanapp.id 

       LEFT JOIN mwlhazard 

              ON mwlhazard.owner_id = mwlsubjectproperty.id 

       LEFT JOIN mwlinstitution 

              ON mwlinstitution.objowner_id = mwlhazard.id 

                 AND objownername = 'HazardInstitution' 

       LEFT JOIN mwlprepaid prepaid 

              ON prepaid.loandata_id = loandata.id 

                 AND prepaid.hudlinenum = '903' 

       -- HUD Line# 903 refers to Hazard insurance premium        

       LEFT JOIN mwlphonenumber Hzdphonenumber 

              ON mwlinstitution.id = Hzdphonenumber.objowner_id 

                 AND Hzdphonenumber.phonetype = 'Business' 

       LEFT JOIN mwlphonenumber HzdFaxnumber 

              ON mwlinstitution.id = HzdFaxnumber.objowner_id 

                 AND HzdFaxnumber.phonetype = 'Fax' 

       LEFT JOIN mwlcustomfield AS custfield 

              ON mwlloanapp.id = custfield.loanapp_id 

                 AND custfield.custfielddef_id = (SELECT id 

                                                  FROM   mwscustfielddef 

                                                  WHERE 

                     mwfieldnum = @intHazardInsuranceDeductible 

                                                 -- MWFieldNum pointing to "Hazard Insurance Deductible" 

                                                 ) 

       LEFT JOIN mwlprepaid propertytax 

              ON propertytax.loandata_id = loandata.id 

                 AND propertytax.hudlinenum = '1004' 

                 -- HUD Line# 1004 refers Property taxes 

                 AND propertytax.huddesc = 'Property taxes' 

       LEFT JOIN mwlhudlinecomponent CityTaxes 

              ON CityTaxes.objowner_id = propertytax.id 

                 AND CityTaxes.description = 'IQ - City taxes' 

       LEFT JOIN mwlhudlinecomponent CountryTaxes 

              ON CountryTaxes.objowner_id = propertytax.id 

                 AND CountryTaxes.description = 'IQ - County taxes' 

       LEFT JOIN mwlppe 

              ON dbo.mwlppe.loandata_id = loandata.id 

       LEFT JOIN mwlprepaid hzdinsurace 

              ON hzdinsurace.loandata_id = loandata.id 

                 AND hzdinsurace.hudlinenum = '1002' 

       LEFT JOIN mwlprepaid AssementAnnualAmt 

              ON AssementAnnualAmt.loandata_id = loandata.id 

                 AND AssementAnnualAmt.hudlinenum = '1005' 

      LEFT JOIN mwlprepaid FloodInsuranceinPayment 

              ON FloodInsuranceinPayment.loandata_id = loandata.id 

                 AND FloodInsuranceinPayment.hudlinenum = '1006' 

       -- HUD Line# 1006 refers Flood insurance reserves 

       LEFT JOIN mwlprepaid Other1Desc 

              ON Other1Desc.loandata_id = loandata.id 

                 AND Other1Desc.hudlinenum = '905' 

       -- HUD Line# 905 refers Other#1 Description 

       LEFT JOIN mwlprepaid Other2Desc 

              ON Other2Desc.loandata_id = loandata.id 

                 AND Other2Desc.hudlinenum = '1008' 

       -- HUD Line# 1008 refers Other#2 Description     

       LEFT JOIN mwlprepaid Other3Desc 

              ON Other3Desc.loandata_id = loandata.id 

                 AND Other3Desc.hudlinenum = '906' 

       -- HUD Line# 906 refers Other#3 Description           

       LEFT JOIN mwlprepaid Other4Desc 

              ON Other4Desc.loandata_id = loandata.id 

                 AND Other4Desc.hudlinenum = '907' 

       LEFT JOIN mwlprepaid Other5Desc 

              ON Other5Desc.loandata_id = loandata.id 

                 AND Other5Desc.hudlinenum = '908' 

       -- HUD Line# 908 refers Other#5 Description     

       LEFT JOIN mwlcustomfield AS TaxesNextPayment 

              ON mwlloanapp.id = TaxesNextPayment.loanapp_id 

                 AND TaxesNextPayment.custfielddef_id = (SELECT id 

                                                         FROM   mwscustfielddef 

                                                         WHERE 

                     mwfieldnum = @intNextTaxesDue 

                                                        -- Custom field ID for Next Taxes Due

                                                        ) 

       WHERE  loannumber = @LoanNumber      

       )C
       ) A unpivot(FieldValue  FOR  FieldName IN(Company,street,city,[state],zipcode,country,phonenumber,policynumber, hazardinscoverage, annualpayduedate,currencyvalue,TaxtNextPaymentDate,WaiveEscrows,TotalAmt))P
       ) B where (isnull(FieldValue,'') = ''  or  (FieldValue='1900-01-01')) or (FieldName = 'WaiveEscrows' and Conditions = 'Y')  or (FieldName = 'TotalAmt')

       END
--Get Company Name



DECLARE @SpecialFloodHazardArea AS BIT

select @SpecialFloodHazardArea = specialfloodhazardarea from mwlflood where owner_id =(select id from mwlsubjectproperty where loanapp_id = (select id from mwlloanapp where loannumber = @LoanNumber))

if(@SpecialFloodHazardArea = 1)

       BEGIN
       
       IF @StepNumber IN (1 ,5 )
BEGIN
insert into tblDooMandatoryFieldRecords
       Select @LoanNUmber LoanNumber,'5' StepNumber,FieldName,FieldValue,Conditions from 
       (
       Select * from 
       (
              SELECT CAST(isnull(Institution.company,'') as varchar(Max))                    AS Company,

                      CAST(isnull(Institution.street,'') as varchar(Max))                 AS Street,

                      CAST(isnull(Institution.City,'') as varchar(Max))                     AS City,

                      CAST(isnull(Institution.State,'')  as varchar(Max))                  AS State,

                     CAST(isnull(Institution.zipcode,'')   as varchar(Max))                  AS Zipcode,

                     CAST(isnull(Institution.country,'')          as varchar(Max))           AS Country,

                     CAST(isnull(Floodphonenumber.phonenumber,'') as varchar(Max)) AS PhoneNumber,

                     CAST(isnull(Flood.floodpolicynumber,'')             as varchar(Max))     AS FloodPolicyNumber,   

                     CAST(isnull(Flood.FloodInsCoverage,'') as varchar(Max))           AS FloodInsCoverage,

                     CAST(isnull(Convert(varchar(10),Flood.FloodCertDate,121),'')          as varchar(Max))           AS FloodCertDate,

                     CAST(isnull(Flood.FloodZoneId ,'')           as varchar(Max))           AS FloodZoneId,
                     'N' Conditions

              From mwlLoanApp LoanApp

                     INNER JOIN dbo.mwlLoanData LoanData ON LoanApp.ID = LoanData.ObjOwner_ID

                     Left join mwlSubjectProperty SubjectProperty on SubjectProperty.LoanApp_ID = LoanApp.ID

                     LEFT JOIN mwlflood Flood ON Flood.Owner_ID = SubjectProperty.ID            

                     LEFT JOIN mwlinstitution Institution on Institution.objOwner_id= Flood.id AND Institution.objownername = 'RelatedInstitution'

                     LEFT JOIN mwlPhoneNumber Floodphonenumber ON Institution.id=Floodphonenumber.ObjOwner_ID AND Floodphonenumber.phonetype ='Business'

                     LEFT JOIN mwlPhoneNumber FloodFaxnumber ON Institution.id=FloodFaxnumber.ObjOwner_ID AND FloodFaxnumber.phonetype ='Fax'

              Where LoanNumber = @LoanNumber
              ) A unpivot(FieldValue  FOR  FieldName IN(Company,Street,City,[State],Zipcode,Country,PhoneNumber,FloodPolicyNumber, FloodInsCoverage, FloodCertDate,FloodZoneId))P
              )B  where isnull(FieldValue,'') = ''  or  (FieldValue='1900-01-01')


              END 
       END

       IF @StepNumber IN (1 ,6 )
BEGIN
       insert into tblDooMandatoryFieldRecords
            SELECT @LoanNumber ,
                           '6' StepNumber,
                 'HudlineNum-' + Prepaid.HUDLinenum ,               
               null   BorrowerAmt        ,'N' Conditions 
                            
        FROM    mwlloanapp LoanApp ,
                mwlloandata LoanData ,
                mwlprepaid Prepaid
        WHERE   LoanApp.ID = LoanData.objOwner_ID
                           And LoanData.Active = 1
                AND LoanData.ID = Prepaid.loanDAta_ID
                AND LoanApp.LoanNumber = @LoanNumber
                AND CAST(Prepaid.HUDLinenum AS MONEY) >= 900 -- HUD Line series start from  
                AND CAST(Prepaid.HUDLinenum AS MONEY) <= 999 -- HUD Line series upto 
                           AND isnull(Prepaid.BorrowerAmt,0) > 0 and   isnull(Prepaid.Payee ,'') = ''  and Prepaid.HUDLinenum not in('902')
                           and upper(ltrim(rtrim(Prepaid.HUDDesc))) <> 'INTEREST'
                           UNION ALL

                           SELECT 
                            @LoanNumber,
                           '6' StepNumber,
                 'HudlineNum-' + ClosingCost.HUDLinenum ,
              null BorrowerAmt   , 'N' Conditions 
                           
        FROM    mwlloanapp LoanApp ,
                mwlloandata LoanData ,
                mwlclosingcost ClosingCost
        WHERE   LoanApp.ID = LoanData.objOwner_ID
                           And LoanData.Active = 1
                AND LoanData.ID = ClosingCost.loanDAta_ID
                AND LoanApp.LoanNumber = @LoanNumber
                AND CAST(ClosingCost.HUDLinenum AS MONEY) >= 800 -- HUD Line series start from 
                AND CAST(ClosingCost.HUDLinenum AS MONEY) <= 822 -- HUD Line series upto
                           AND isnull(ClosingCost.Payee,'') = '' AND  isnull(ClosingCost.BorrowerAmt ,0) > 0   --and 1 = 0
                           and (ClosingCost.HUDLinenum  between 815 and 818 or (upper(ltrim(rtrim(ClosingCost.HUDDesc))) in('CREDIT REPORT','APPRAISAL FEE') and upper(ltrim(rtrim(LoanApp.ChannelType)))  in('BROKER')) 
                           or upper(ltrim(rtrim(LoanApp.ChannelType)))  in('CORRESPOND'))
                           union all

                           SELECT  
                           @LoanNumber,
                           '6' StepNumber,
                'HudlineNum-' +ClosingCost.HUDLinenum ,
             null BorrowerAmt  , 'N' Conditions 
               
        FROM    mwlloanapp LoanApp ,
                mwlloandata LoanData ,
                mwlclosingcost ClosingCost
        WHERE   LoanApp.ID = LoanData.objOwner_ID and LoanData.Active = 1
                AND LoanData.ID = ClosingCost.loanDAta_ID
                AND LoanApp.LoanNumber = @LoanNumber
                AND CAST(ClosingCost.HUDLinenum AS MONEY) >= 1100 -- We need to get all hud line information greater then 1100 showing at Title Information
                AND CAST(ClosingCost.HUDLinenum AS MONEY) <= 1299 
				AND CAST(ClosingCost.HUDLinenum AS MONEY) != 1101
                AND isnull(ClosingCost.Payee,'') = '' AND  isnull(ClosingCost.BorrowerAmt ,0) > 0   --and 1 = 0

              END 
                     IF  Exists (Select 1 from tblDooMandatoryFieldRecords where LoanNumber = @LoanNumber and Conditions = 'Y' and  ( MandatoryFieldName = 'TotalAmt' and CAST(isnull(MandatoryFieldValue,0) as numeric(18,2)) > 0))
                     BEGIN

                     IF  NOT Exists (Select 1 from tblEscrowDueDates where LoanNumber = @LoanNumber)
                     BEGIN
                     IF @StepNumber IN (1 ,5 )
                     BEGIN
                     INSERT INTO tblDooMandatoryFieldRecords
                           Select @LoanNUmber,'5','DueDates',null,'N' 

                     END 

                     END 

                 END 

                 Select * from tblDooMandatoryFieldRecords where LoanNUmber = @LoanNumber
END




GO

