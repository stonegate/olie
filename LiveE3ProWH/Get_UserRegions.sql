USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_UserRegions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Get_UserRegions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author, Somnath M. R.>
-- Create date: <Create Date, 09th Dec 2013>
-- Description:	<Description, Select from tblUserRegions table>
-- EXEC Get_UserRegions 16527
-- =============================================
CREATE PROCEDURE [dbo].[Get_UserRegions]
	@UserId Int,
	@BusinessSource nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
		U.UserId, 
		R.Id, 
		R.RegionName, 
		R.RegionValue, 
		R.BusinessSource
	FROM [dbo].[tblUsers] U 
	LEFT OUTER JOIN [dbo].[tblUserRegions] UR ON U.UserId = UR.UserId
	INNER JOIN [dbo].[tblRegion] R ON R.Id = UR.RegionId
	WHERE UR.UserId = @UserId and (@BusinessSource IS NULL OR BusinessSource IN (SELECT * FROM dbo.SplitString(@BusinessSource, ',')))
END

GO

