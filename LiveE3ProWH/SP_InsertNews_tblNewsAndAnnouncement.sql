USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_InsertNews_tblNewsAndAnnouncement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_InsertNews_tblNewsAndAnnouncement]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procEDURE [dbo].[SP_InsertNews_tblNewsAndAnnouncement] 
(
     
@Title nvarchar(1000),   
@Body text,    
@IsActive bit,    
@IsNewsAnnouncement bit,
@CreatedBy int,
@CreatedDate datetime,    
@Role nvarchar(50),      
@NewsAnnousVisbleAd bit,     
@NewsAnnousVisbleDR bit,      
@NewsAnnousVisbleBR bit,     
@NewsAnnousVisbleLO bit,      
@NewsAnnousVisbleCU bit,      
@NewsAnnousVisbleRE bit,      
@NewsAnnousVisbleRM bit,
@NewsAnnousVisbleDP bit,
@NewsAnnousVisbleCP bit,
@NewsAnnousVisbleHY bit,
@NewsAnnousVisbleUM bit,
@NewsAnnousVisbleDCFI bit,
@NewsAnnousVisibleSCFI bit 
)
AS
BEGIN
       INSERT INTO tblNewsAndAnnouncement(Title,Body,IsActive,IsNewsAnnouncement,CreatedBy,CreatedDate,Role,NewsAnnousVisbleAd,NewsAnnousVisbleDR,NewsAnnousVisbleBR,NewsAnnousVisbleLO,NewsAnnousVisbleCU,NewsAnnousVisbleRE,NewsAnnousVisbleRM,NewsAnnousVisbleDP,NewsAnnousVisbleCP,NewsAnnousVisbleHY,NewsAnnousVisbleUM,NewsAnnousVisbleDCFI,NewsAnnousVisibleSCFI)      
 
VALUES      
										(@Title,@Body,@IsActive,@IsNewsAnnouncement,@CreatedBy,@CreatedDate,@Role,@NewsAnnousVisbleAd,@NewsAnnousVisbleDR,@NewsAnnousVisbleBR,@NewsAnnousVisbleLO,@NewsAnnousVisbleCU,@NewsAnnousVisbleRE,@NewsAnnousVisbleRM,@NewsAnnousVisbleDP,@NewsAnnousVisbleCP,@NewsAnnousVisbleHY,@NewsAnnousVisbleUM,@NewsAnnousVisbleDCFI,@NewsAnnousVisibleSCFI)  
END





GO

