USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetMandatoryDeliveryoption]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetMandatoryDeliveryoption]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================          

-- Author:  Sushant/Rajkumar/Gourav          

-- Create date: 9th July 2013                  

-- Description: This will get Delivery Option of loan            

-- =============================================  



Create PROCEDURE [dbo].[sp_GetMandatoryDeliveryoption]                  

(                  

@LoanNumber varchar(500)                  

)                   

AS                  

BEGIN

 /* --------------------------------------------------------------------------------------      

       

 Name:       

  [sp_GetMandatoryDeliveryoption].sql      

      

 Description:      

  This will get Delivery Option of loan          

?      

     

  

       

 ---------------------------------------------------------------------------------------*/   

SET NOCOUNT ON;

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Per E3)      

SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,       

-- the entire transaction is terminated and rolled back. 



-- Step 0.0 - Test Data      

--DECLARE @LoanNumber VARCHAR(500)      

--SET @LoanNumber = '0000324220' 



-- Step 1.0 - get Delivery Option of loan  



SELECT deliveryoption

FROM mwlLockRecord LockRecord

LEFT JOIN mwlLoanApp loanapp ON LockRecord.LoanApp_ID = loanapp.id

	WHERE LoanApp.LoanNumber = @LoanNumber 

		AND deliveryoption <> '' 

		AND deliveryoption = 'Mandatory'      

 

END 

GO

