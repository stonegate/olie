USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdatemwlLoanApp_ChannelType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdatemwlLoanApp_ChannelType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  Shyamal    
-- Create date: 14/12/2012(DD/MM/YYYY)    
-- Description: Procedure for clspipelineE3 BLL  to Update Channel type  
-- =============================================    
create PROCEDURE [dbo].[sp_UpdatemwlLoanApp_ChannelType]    
(    
@ID varchar(32)  ,  
@ChannelType varchar(10)  
)    
AS    
BEGIN    
     
SET NOCOUNT ON;    
UPDATE mwlLoanApp 
WITH (UPDLOCK)   
SET ChannelType=@ChannelType     
WHERE ID=@ID    
      
END    




GO

