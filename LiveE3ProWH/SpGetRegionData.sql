USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetRegionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetRegionData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nirav>
-- Create date: <10/4/2012>
-- Description:	<Fetch All region Data>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetRegionData] 
	
AS
BEGIN
	select * from tbl_region order by regionid
END

GO

