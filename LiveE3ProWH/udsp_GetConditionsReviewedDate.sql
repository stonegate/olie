USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetConditionsReviewedDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetConditionsReviewedDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================    
-- Author:  Amit Saini    
-- Create date: 10/04/2013  
-- Description: Procedure to Get Conditions Reviewed Date]  
-- =========================================================    
CREATE PROCEDURE [dbo].[udsp_GetConditionsReviewedDate] -- '0000346672','D70F0DD48CDD443C8F0CC58849EE401F'
(    
@LoanNumber varchar(50),
@CustomeFieldPTPConditionReviewDate varchar(50)
)    
AS    
BEGIN       
SET NOCOUNT ON; 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
    
 select mwlcustomfield.DateValue from mwlcustomfield 
 Inner Join mwlLoanApp on mwlLoanApp.ID = mwlcustomfield.LoanApp_ID 
 where mwlcustomfield.custfielddef_id = @CustomeFieldPTPConditionReviewDate and mwlLoanApp.LoanNumber=@LoanNumber
END

GO

