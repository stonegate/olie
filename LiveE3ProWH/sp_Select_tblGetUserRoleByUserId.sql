USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetUserRoleByUserId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetUserRoleByUserId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblGetUserRoleByUserId]
(
@iUserId varchar(Max)
)
as
begin
select urole from tblusers where userid=@iUserId and IsActive=1
end


GO

