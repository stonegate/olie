USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_update_tblUpdateStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_update_tblUpdateStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_update_tblUpdateStatus]
(
@userid varchar(Max)
)
as
begin
	update top (1)  tblTracking 
	set logintime = getdate() 
	where id = 
	(
	select top 1 id from tblTracking 
	where userid = @userid order by logintime desc
	)
end


GO

