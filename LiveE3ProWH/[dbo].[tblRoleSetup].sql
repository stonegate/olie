USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRoleSetup]') AND type in (N'U'))
DROP TABLE [dbo].[tblRoleSetup]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRoleSetup](	  [RoleSetupID] INT NOT NULL IDENTITY(1,1)	, [RoleID] INT NULL	, [TabID] INT NULL	, [TabOrder] INT NULL	, [isActive] BIT NULL	, [TPO] BIT NULL	, [CFI] BIT NULL	, CONSTRAINT [PK_tblRoleSetup] PRIMARY KEY ([RoleSetupID] ASC))USE [HomeLendingExperts]
GO

