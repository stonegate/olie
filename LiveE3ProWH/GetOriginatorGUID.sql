USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetOriginatorGUID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetOriginatorGUID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetOriginatorGUID]
@AcctExecName VARCHAR(100)
AS

/* --------------------------------------------------------------------------------------

Name:
GetOriginatorGUID.sql

Description:
Used for the "Select From Setup" Process. Script used to get the Originator GUID based
upon the Account Executive.

DDL information:
Database - InterlinqE3
Table - mwaMWUser

Steps:
Step 0.0 - Test Data
Step 1.0 - Get OriginatorGUID

Change History:
2013/05/03 - Bultemeier, Shelby
Create Script

---------------------------------------------------------------------------------------*/

BEGIN
--DECLARE @AcctExecName VARCHAR(50)

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Per E3)
SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,
-- the entire transaction is terminated and rolled back.

-- Step 0.0 - Test Data
--DECLARE @AcctExecName VARCHAR(100)
--SET @AcctExecName = 'Bob Mink'

-- Step 1.0 - Get OriginatorGUID
SELECT [ID] AS [OriginatorGUID],[FullName]
FROM [dbo].[mwaMWUser]
WHERE FullName = @AcctExecName

END


GO

