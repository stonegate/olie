USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBDMData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBDMData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		 Abhishek
-- Create date: 10/18/2012
-- Description:	Procedure for clsloanRegister.cs bll  
-- =============================================
CREATE Procedure [dbo].[sp_GetBDMData]

AS
BEGIN
select ufirstname + ' ' + ulastname as uname, userid from tblusers where (urole =26 or urole=8 or urole=19) and isactive=1 order by ufirstname
end



GO

