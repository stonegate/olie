USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_UpdateUnClearedConditionStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_UpdateUnClearedConditionStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_UpdateUnClearedConditionStatus]
(
@record_id int
)
AS
BEGIN
	
	SET NOCOUNT ON;

 update condits  set COND_CLEARED_DATE = NULL where record_id =@record_id
END


GO

