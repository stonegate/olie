USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertBlitzdocsLoanRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertBlitzdocsLoanRegister]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_InsertBlitzdocsLoanRegister]
(
@Comments varchar(500),
@userid int,
@Docname varchar(500),
@loan_no Char(15)
)
as
begin
	Insert into tblloandoc(Comments,userid,DocName,loan_no)
values(@Comments ,
@userid ,
@Docname ,
@loan_no 
)
end


GO

