USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetEmailID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetEmailID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetEmailID] 
	@UserId int=null
AS
BEGIN
	select upassword,uemailid,ufirstname +' '+ ISNULL(MI,'') +' '+ ulastname as UserName,Roles  from tblusers u left outer join tblroles r on r.Id=u.urole where userid =@UserId
END

GO

