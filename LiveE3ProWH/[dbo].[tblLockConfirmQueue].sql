USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLockConfirmQueue]') AND type in (N'U'))
DROP TABLE [dbo].[tblLockConfirmQueue]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLockConfirmQueue](	  [LoanApp_ID] VARCHAR(32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [StatusID] INT NULL DEFAULT((1))	, [DateCreated] DATETIME NOT NULL DEFAULT(getdate())	, [Id] INT NOT NULL IDENTITY(1,1)	, [RetryCount] INT NOT NULL DEFAULT((0))	, CONSTRAINT [pk_tblLockConfirmQueue_pid] PRIMARY KEY ([Id] ASC))USE [HomeLendingExperts]
GO

