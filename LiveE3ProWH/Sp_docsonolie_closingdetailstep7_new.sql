USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_docsonolie_closingdetailstep7_new]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_docsonolie_closingdetailstep7_new]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here
  
-- =============================================          
-- Author:  Mitesh R Rajyaguru          
-- Create date: Jun, 06 2013          
-- Description: This will give First Payment Date for Closing Details Date Comparision  
-- =============================================          
CREATE PROCEDURE [dbo].[Sp_docsonolie_closingdetailstep7_new] 
(
	@LoanNumber VARCHAR(10)
) 
AS 
    SET TRANSACTION isolation level READ uncommitted 

  BEGIN 
      SELECT loandata.firstpaydate AS DisbursementDate 
      FROM   mwlloanapp loanapp 
             INNER JOIN mwlloandata loandata 
                     ON loanapp.id = loandata.objowner_id 
                        AND loandata.active = 1 
      WHERE  loanapp.loannumber = @LoanNumber 
  END 

-- Create Procedure Coding ENDS Here

GO

