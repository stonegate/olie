USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblselectGetUserRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblselectGetUserRole]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblselectGetUserRole]
(
@strRole varchar(Max)
)
as
begin
 select Roles from tblroles  where id=@strRole
end


GO

