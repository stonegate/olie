USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_select_tblloanregLRLK]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_select_tblloanregLRLK]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_select_tblloanregLRLK]




as
begin
	select LR.*,LK.Description as TranscationType from tblloanreg LR

Left outer join tbllookups LK on LR.TransType=LK.Lookupid ORDER BY CreatedDate DESC
end

GO

