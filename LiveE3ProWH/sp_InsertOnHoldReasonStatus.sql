USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertOnHoldReasonStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertOnHoldReasonStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Stonegate>
-- Create date: <10/9/2012>
-- Description:	<This will Insert On Hold Reason Status>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertOnHoldReasonStatus]
(
	@NewId varchar(500) =null,
	@LoanAppId varchar(100) = null,
	@LoanNo varchar(200)=null
	
)
AS
BEGIN

If  not exists(select * From mwlappstatus where LoanApp_id in(@LoanAppId) and  StatusDesc='17 - Submission On Hold')
begin
insert into mwlappstatus(ID,LoanApp_id,StatusDesc,StatusDateTime)
	values 
	(
       REPLACE(NEWID(),'-',''),@LoanAppId,'17 - Submission On Hold',Getdate()
	)
	--comment for no need to pass like condition
	--update mwlloanapp set CurrentStatus='17 - Submission On Hold' ,CurrPipeStatusDate=getdate() where loannumber = '%' + @LoanNo + '%'--
	update mwlloanapp set CurrentStatus='17 - Submission On Hold' ,CurrPipeStatusDate=getdate() where loannumber =  @LoanNo
end 

END

GO

