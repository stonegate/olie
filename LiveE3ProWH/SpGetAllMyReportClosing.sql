USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetAllMyReportClosing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetAllMyReportClosing]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetAllMyReportClosing]           
(            
 @Puid nvarchar(max),        
 @Role varchar(50),          
 @CurrentStatus nvarchar(max)          
)            
AS            
BEGIN            
            
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

Declare @SQL nvarchar(max)            
Set @SQL ='' 
if (@Role = '0' Or @Role = '34')
	Begin
		set @SQL= @SQL + ' select top 100 '
	End
else
	Begin
		set @SQL= @SQL + ' select '
	End              
Set @SQL = @SQL + ' ''E3'' as DB,loanapp.DecisionStatus,loanapp.Channeltype as BusType, loanapp.ID as record_id,LoanNumber as loan_no,borr.lastname as BorrowerLastName,loanapp.DecisionStatus,loanapp.CurrentStatus as LoanStatus,DateAppSigned as SubmittedDate,Loandata.LoanProgramName as LoanProgram,        
case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,borr.CompositeCreditScore as CreditScoreUsed,CASE WHEN loanData.AdjustedNoteAmt IS NULL THEN ''0''  WHEN loanData.AdjustedNoteAmt =  '''' THEN ''0'' ELSE loanData.AdjustedNoteAmt END   as LoanAmount,        
convert(varchar(35),EstCloseDate,101) AS ScheduleDate,LockStatus,loanapp.LockExpirationDate,UnderwriterName as UnderWriter,CloserName as Closer,
Institute.Company as BrokerName, Institute.CustomDataOne as TitleCompany,        
AppStatus.StatusDateTime as DocsSentDate,FundedStatus.StatusDateTime as FundedDate,'''' as LU_FINAL_HUD_STATUS, LockRecord.DeliveryOption  ,BOffices.Office TPORegion         
from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active=1         
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id 
 Left join dbo.mwlInstitution as Institute on Institute.ObjOwner_id = loanapp.id and         
InstitutionType = loanapp.Channeltype  
AND Institute.Company is not null
Left join (Select distinct Office,ObjOwner_id from dbo.mwlInstitution Where InstitutionType = ''Branch'' and objownerName=''BranchInstitution'') as BOffices on BOffices.ObjOwner_id = loanapp.id  
Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and AppStatus.StatusDesc like ''%Docs Out%''         
Left join mwlappstatus as FundedStatus on FundedStatus.LoanApp_id=loanapp.id  and FundedStatus.StatusDesc like ''%Funded''
left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType = ''LOCK'' and  LockRecord.Status <> ''CANCELED''         
where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''          
and CurrentStatus IN (select * from dbo.SplitString('''+@CurrentStatus+''','','')) '            
          
 if (@Role = '1' and @Role = '2')            
  Begin            
    Set @SQL = @SQL + ' and loanapp.ChannelType<>''RETAIL'' '            
  End            
  if (@Role != '0')          
 begin        
  if (len(@Puid) = 0)            
  Begin            
   --broker        
   if (@Role = '3' or @Role = '20' or @Role = '21')         
   begin        
     Set @SQL = @SQL + ' and (MainTable.ID in (select * from dbo.SplitString('''+ @Puid +''','',''))) '          
   end        
   else        
   begin        
    Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+ @Puid +''','',''))) '          
   end        
  End           
  else        
   begin        
    -- broker        
    if (@Role = '3' or @Role = '20' or @Role = '21')         
    begin        
     Set @SQL = @SQL + ' and (MainTable.ID in (select * from dbo.SplitString('''+ @Puid +''','',''))) '           
    end        
    -- Director of Production        
    if (@Role = '12' or @Role = '11')         
    begin        
     if (@Role = '12')         
     begin        
      Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+ @Puid +''','',''))) '          
     end        
     else        
     begin        
      Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+ @Puid +''','','')) or Loanapp.ChannelType =''Retail'') '        
     end        
             
    end        
    else        
     begin        
      Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+ @Puid +''','',''))) '          
     end        
         
   end         
 end        
Set @SQL = @SQL + ' order by CurrentStatus'        
           
        
          
Print @SQL            
exec sp_executesql @SQL            
END 



GO

