USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetUnderwriterChannelType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetUnderwriterChannelType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author: Shyamal Gajjar
-- Create date: 18-12-2012
-- Description: Select Channeltype for Underwriter /UW TEam LEad
-- =============================================
CREATE PROCEDURE [dbo].[SpGetUnderwriterChannelType]
@userid int
AS
BEGIN
select brokertype as Channeltype from tblUsers where userid=@userid
END

GO

