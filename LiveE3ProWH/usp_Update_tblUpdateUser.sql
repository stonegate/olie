USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Update_tblUpdateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Update_tblUpdateUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 
CREATE proc [dbo].[usp_Update_tblUpdateUser]                  
(                  
@ufirstname varchar(Max)=null                  
,@ulastname varchar(Max)=null                  
,@uemailid varchar(Max)=null                  
,@StateBelongTo varchar(Max)=null                  
,@urole varchar(Max)=null                  
,@isadmin varchar(Max)=null               
,@isPublish varchar(max)=null                 
,@isactive varchar(Max)=null                  
,@uidprolender varchar(Max)=null                  
,@uparent varchar(Max)=null                  
,@upassword varchar(Max)=null                  
,@mobile_ph varchar(Max)=null                  
,@phonenum varchar(Max)=null                  
,@loginidprolender varchar(Max)=null                  
,@carrierid varchar(Max)=null                  
,@MI varchar(Max)=null                  
,@companyname varchar(Max)=0                  
,@AEID varchar(Max)=0                  
,@Region varchar(Max)=0                  
,@E3Userid varchar(Max)=0                  
,@PartnerCompanyid varchar(Max)=0                  
,@PasswordExpDate varchar(Max)=0                  
,@chkRegisterloan varchar(Max)=0                  
,@chkQuickPricer varchar(Max)=0                  
,@chkpricelockloan varchar(Max)=0                  
,@chkSubmitloan varchar(Max)=0                  
,@chkSubmitcondition varchar(Max)=0                  
,@chkorderappraisal varchar(Max)=0                  
,@chkorderfhano varchar(Max)=0                  
,@chkscheduleclosingdate varchar(Max)=0                  
,@chkupdateloan varchar(Max)=0                  
,@chkaccuratedocs varchar(Max)=0                  
,@chkSubmitClosingPackage varchar(Max)=0                  
,@PartnerType varchar(Max)=0                  
,@chkOrderTaxTranscript varchar(Max)=0                  
,@userid varchar(Max)=0                  
,@UWTeamleadid int =null                
,@bitIsKatalyst bit      
,@IsForthPartyOrigination bit =0       
,@IsemergingBanker varchar(2) = null  
,@IsC3CCI bit = null  
,@chkLockConfirmation bit = null --  By tavant 215-BRD
,@chkPurchaseAdvice bit = null -- By tavant 129 BRD      
)                  
as                  
begin                  
BEGIN TRY

BEGIN TRAN


/*Start Added By Tavant For BRD198*/
Declare @RegionNew VARCHAR(MAX)  
IF  @urole = 22 OR @urole = 27 OR @urole = 28 
BEGIN
SET @RegionNew = @Region
Set @Region = ''
END

/*END Added By Tavant For BRD198*/


UPDATE tblUsers WITH (UPDLOCK)                   
		SET ufirstname = @ufirstname                  
		,ulastname = @ulastname                  
		,uemailid = @uemailid                  
		,StateBelongTo = @StateBelongTo                  
		,urole = @urole                  
		,isadmin = @isadmin                  
		,isactive = @isactive                  
		,uidprolender = @uidprolender                  
		,uparent = @uparent                  
		,mobile_ph = @mobile_ph                  
		,phonenum = @phonenum                        
		,loginidprolender = @loginidprolender                  
		,carrierid = @carrierid                  
		,MI = @MI                  
		,companyname = @companyname                  
		,AEID = @AEID                  
		,Region = @Region                  
		,E3Userid = @E3Userid                  
		,PartnerCompanyid = @PartnerCompanyid                  
		,PasswordExpDate = @PasswordExpDate                  
		,chkRegisterloan = @chkRegisterloan                  
		,chkQuickPricer = @chkQuickPricer                  
		,chkpricelockloan = @chkpricelockloan                  
		,chkSubmitloan = @chkSubmitloan                  
		,chkSubmitcondition = @chkSubmitcondition                  
		,chkorderappraisal = @chkorderappraisal                  
		,chkorderfhano = @chkorderfhano                  
		,chkscheduleclosingdate = @chkscheduleclosingdate                  
		,chkupdateloan = @chkupdateloan                  
		,chkaccuratedocs=@chkaccuratedocs                  
		,chkSubmitClosingPackage = @chkSubmitClosingPackage       
		--,partnertype = @PartnerType           -- Modified for SMCPS197
		,chkOrderTaxTranscript = @chkOrderTaxTranscript         
		,UWTeamleadid=@UWTeamleadid               
		,isPublish=@isPublish                
		,iskatalyst= @bitIsKatalyst      
		,IsFouthPartyOrigination= @IsForthPartyOrigination    
		--,IsEmergingBanker  = @IsemergingBanker  
		,IsC3Integration  = @IsC3CCI    
		,chkLockConfirmation = @chkLockConfirmation -- By tavant 215-BRD
		,chkPurchaseAdvice  = @chkPurchaseAdvice    --   -- By tavant 129 BRD      
  Where userid =@userid            
  
  
  
	IF @urole = 20 OR @urole = 21 
		BEGIN
		-- Added By Tavnt Team On 05-12-2013 for Emerging Banker
			 if exists (Select 1 from tblEmergingBankers where PartnerCompanyId = @PartnerCompanyid  and Status = 'Y' and Flag='EB' and IsEmergingBanker <> @IsemergingBanker)
		BEGIN

				 Update tblEmergingBankers
				 SET IsEmergingBanker = @IsemergingBanker,
						 ModifiedBy = @userid,
						 ModifiedDate = getdate()
				where PartnerCompanyId = @PartnerCompanyid   and Status = 'Y' and Flag = 'EB'
		 END

			 ELSE
					 BEGIN 
					 if NOT exists (Select 1 from tblEmergingBankers where PartnerCompanyId = @PartnerCompanyid  and Status = 'Y' and Flag='EB')
					 BEGIN
				Insert  into tblEmergingBankers (PartnerCompanyId,CompanyName,RoleId,IsEmergingBanker,Flag,Status,CreatedBy,CreatedDate)
					 VALUES (@PartnerCompanyid,@companyname,@urole,@IsemergingBanker,'EB','Y',@userid,getdate())
					END
			 END 

		END 

		--Added By Tavant For BRD198
	IF @urole = 22 OR @urole = 27 OR @urole = 28
	BEGIN

			IF EXISTS (SELECT 1 FROM tblUserRegionMapping where UserId = @userid and [Status] = 'N' and Region in(Select items  from dbo.SplitString(@RegionNew,',')))
			BEGIN

			UPDATE tblUserRegionMapping
			Set [Status] = 'Y',
				ModifiedBy = @userid,
				ModifiedDate = getdate()
			WHERE UserId = @userid and Status = 'N' and Region in(Select items  from dbo.SplitString(@RegionNew,','))

			END

			IF EXISTS (SELECT 1 FROM tblUserRegionMapping where UserId = @userid and [Status] = 'Y' and Region NOT IN (Select items  from dbo.SplitString(@RegionNew,',')))
			BEGIN


			UPDATE tblUserRegionMapping
			Set [Status] = 'N',
				ModifiedBy = @userid,
				ModifiedDate = getdate()
			WHERE UserId = @userid and Status = 'Y' and Region NOT IN (Select items  from dbo.SplitString(@RegionNew,','))
			
				
			END 

			IF EXISTS(SELECT 1 FROM dbo.SplitString(@RegionNew,',') WHERE ltrim(rtrim(items)) not in (SELECT ltrim(rtrim(Region)) FROM tblUserRegionMapping WHERE UserId = @userid))
			BEGIN

			INSERT INTO tblUserRegionMapping SELECT @UserId,@urole,1,items,@userid,getdate(),null,null,'Y'
				FROM dbo.SplitString(@RegionNew,',') WHERE ltrim(rtrim(items)) not in (SELECT ltrim(rtrim(Region)) FROM tblUserRegionMapping WHERE UserId = @userid)

			END

	END

   COMMIT TRAN

END TRY


	  BEGIN CATCH

	  ROLLBACK TRAN 
	  END CATCH
END 


GO

