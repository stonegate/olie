USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetTabsList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetTabsList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		< author name >
-- Create date: 25/10/2012
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[udsp_GetTabsList] 
	-- Add the parameters for the stored procedure here
	@uRole varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  select tbs.TabName, tbs.ControlName  from tblRoleSetup rs join tbltabs tbs on rs.TabID=tbs.TabID where rs.isActive = 1 and rs.RoleID =@uRole order by TabOrder
END

GO

