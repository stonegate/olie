USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUnderWriterManagerAgedReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUnderWriterManagerAgedReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:		<Stonegate>
-- Create date: <10/6/2012>
-- Description:	<This will Get Underwriter Manager Aged Report>
-- =====================================================================
CREATE PROCEDURE [dbo].[sp_GetUnderWriterManagerAgedReport]
(
	@RoleID int,
	@SubmittedDateID varchar(150),
	@SubmittedDateTimeID varchar(150),
	@strFrom varchar(50),
	@strID varchar(2000),
	@strCreteria varchar(5)	
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)

Declare @SQL nvarchar(max)
Set @SQL = ''
Set @SQL ='SELECT ''E3'' as DB,mwlLoanApp.ID as record_id,Cust.DateValue as LastSubmittedDate,CustTime.StringValue as LastSubmittedTime,
mwlLoanApp.CurrPipeStatusDate AS UW_RECVD_DATENew,mwlLoanApp.ID,Offices.Office as office,Corres.Office as coffice,
BOffices.Office as Boffice,CONVERT(DECIMAL(15,3),mwlLoanData.LTV *100)LTV,mwlLoanApp.LoanNumber AS loan_no,
RTRIM(mwlBorrower.LastName) AS BorrowerLastName,mwlloanapp.ChannelType AS bustype, CurrentStatus as LoanStatus,
CASE TransType when NULL THEN '''' WHEN ''P'' THEN ''Purchase money first mortgage'' WHEN ''R'' THEN ''Refinance'' WHEN ''2'' THEN ''Purchase money second mortgage'' WHEN ''S'' THEN ''Second mortgage, any other purpose'' WHEN ''A'' THEN ''Assumption'' WHEN ''HOP'' THEN ''HELOC - other purpose'' WHEN ''HP'' THEN ''HELOC - purchase'' ELSE '''' END AS TransType,
case mwlLoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN ''VA'' End as LoanProgram, 
mwlLoanData.LoanProgDesc as prog_desc, mwlLoanData.AdjustedNoteAmt as loan_amt,
mwlLockRecord.Status AS LockStatus, mwlLockRecord.LockExpirationDate, mwlLockRecord.LockDateTime,Underwriter_ID AS lt_usr_underwriter,
UnderwriterName AS UnderWriter, CASE ISNULL(RTRIM(Offices.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Offices.Company) end AS BrokerName,
mwaMWUser.FirstName + '' '' + mwaMWUser.LastName AS LORep,
mwlLoanApp.CurrPipeStatusDate AS UW_SUBMIT_DATE,convert(varchar(35),EstCloseDate,101) AS ScheduleDate,mwaMWUser.EMail AS lorep_email,
CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'' and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=mwlLoanApp.LoanNumber)) = 0 then 0
WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'' )
and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=mwlLoanApp.Loannumber)) = 0 THEN 0 ELSE (SELECT count(a.ID) as TotalCleared
FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') and a.objOwner_ID 
IN(SELECT ID FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=mwlLoanApp.LoanNumber)) * 100 /(SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') 
and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=mwlLoanApp.LoanNumber))END AS PER,
mwlLoanApp.ChannelType, mwlLoanApp.DecisionStatus, mwlLoanApp.OriginatorName, mwlLoanApp.CurrDecStatusDate,
'''' as LU_FINAL_HUD_STATUS FROM mwlLoanApp INNER JOIN mwlBorrower ON LoanApp_ID = mwlLoanApp.ID and RLABorrCoborr=1 and Sequencenum=1
INNER JOIN mwlLoanData ON mwlLoanData.ObjOwner_ID = mwlLoanApp.ID
LEFT JOIN mwlLockRecord ON mwlLockRecord.LoanApp_ID = mwlLoanApp.ID and mwlLockRecord.LockType=''LOCK'' and mwlLockRecord.Status<>''CANCELED''
LEFT JOIN mwlUnderwritingSummary ON mwlUnderwritingSummary.LoanApp_ID = mwlLoanApp.ID
LEFT JOIN mwaMWUser ON mwaMWUser.ID = mwlLoanApp.Originator_ID
LEFT JOIN mwlAppStatus AS SubUW ON SubUW.LoanApp_ID = mwlLoanApp.ID AND SubUW.StatusDesc=''25 - Submitted to Underwriting''
LEFT JOIN mwlAppStatus AS InUWDate ON InUWDate.LoanApp_ID = mwlLoanApp.ID AND InUWDate.StatusDesc=''29 - In Underwriting''
Left join dbo.mwlInstitution as Offices on Offices.ObjOwner_id = mwlLoanApp.id and Offices.InstitutionType = ''Broker'' and Offices.objownerName=''Broker''
Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = mwlLoanApp.id and BOffices.InstitutionType = ''Branch'' and BOffices.objownerName=''BranchInstitution''
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = mwlLoanApp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''
left join mwlcustomfield as Cust on Cust.loanapp_id = mwlLoanApp.id and Cust.CustFieldDef_ID ='''+ @SubmittedDateID +'''
left join mwlcustomfield as CustTime on CustTime.loanapp_id = mwlLoanApp.id and CustTime.CustFieldDef_ID ='''+ @SubmittedDateTimeID +'''
Where '

if (@strFrom = 'aged_afiles')
Begin
	if (@RoleID = 15 Or @RoleID = 17)
	Begin
		Set @SQL = @SQL + ' ChannelType in(''CORRESPOND'',''BROKER'') and '
    End	
	Set @SQL = @SQL + ' CurrentStatus IN (''29 - In Underwriting'') '	
    if (@strFrom = 'aged_afiles')
    Begin
		Set @SQL = @SQL + ' and (datediff(day,mwlLoanApp.CurrPipeStatusDate,getdate())>=15) '
    End
End
else if (@strFrom = 'aged_sfiles')
Begin
	Set @SQL = @SQL + ' CurrentStatus IN (''25 - Submitted to Underwriting'') '
	if (@strFrom = 'aged_sfiles')
    Begin
		Set @SQL = @SQL + ' and (datediff(day,mwlLoanApp.CurrPipeStatusDate,getdate())>=15) '
    End
End
else if (@strFrom = 'aged_InUwProcfiles')
Begin
	if (@RoleID = 15 Or @RoleID = 17)
	Begin
		Set @SQL = @SQL + ' ChannelType in(''CORRESPOND'',''BROKER'') and '
    End	
	Set @SQL = @SQL + ' CurrentStatus IN (''29.1 - In UW Processing'') '	
    
	Set @SQL = @SQL + ' and (datediff(day,mwlLoanApp.CurrPipeStatusDate,getdate())>=15) '
End

if (@strFrom <> 'aged_sfiles')
Begin
	if (Len(@strID) = 0)
    Begin
		if (@strCreteria <> 'A')
		Begin
			Set @SQL = @SQL + ' and mwlUnderwritingSummary.underwriter_id in (select * from dbo.SplitString('''+@strID+''','','')) '
        End
    End
    else
    Begin
		if (@strCreteria <> 'A')
		Begin
			Set @SQL = @SQL + ' and mwlUnderwritingSummary.underwriter_id in (select * from dbo.SplitString('''+@strID+''','','')) '
        End
    End
End
if (@strFrom = 'aged_afiles' OR @strFrom = 'aged_InUwProcfiles')
Begin
	Set @SQL = @SQL + ' ORDER BY mwlLoanApp.CurrPipeStatusDate asc '
End
else if (@strFrom = 'aged_sfiles')
Begin
	Set @SQL = @SQL + ' ORDER BY mwlLoanApp.CurrPipeStatusDate asc '
End
else
Begin
	Set @SQL = @SQL + ' ORDER BY PER desc '
End
Print @SQL        
exec sp_executesql @SQL     
END

GO

