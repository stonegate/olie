USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Slecttblusers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Slecttblusers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 
-- Description:	Procedure for clspipelineE3 BLL
-- =============================================
CREATE procEDURE [dbo].[sp_Slecttblusers]
(
@userloginid varchar(50)
)
AS
BEGIN
	
	SET NOCOUNT ON;

  select * from tblusers where userloginid =@userloginid
END

GO

