USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RegLoan_GetCounty]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RegLoan_GetCounty]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_RegLoan_GetCounty]
(
@StateCode varchar(10)
)
AS
BEGIN
	select distinct ((convert(varchar(500),upper(substring(CountyName,1,1))+lower(substring(CountyName,2,499)))))as County From mwsCounty where StateCode= @StateCode and  CountyName is not null  and CountyName <> '' order by County
END




GO

