USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tblGetTabsList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tblGetTabsList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Gourav  
-- Create date: 10/03/2012  
-- Description: Procedure for clsUser.cs bll    
-- =============================================  
CREATE proc [dbo].[usp_Select_tblGetTabsList]  
(  
@iUserRole int
)  
as  
begin  
 select tbs.TabName, tbs.ControlName    
 from tblRoleSetup rs join tbltabs tbs on rs.TabID=tbs.TabID   
 where rs.isActive = 1 and rs.TPO=1 and rs.RoleID = @iUserRole order by TabOrder  
 -- rs.TPO=1 Added By Tavant Team for DOO1.2 BRD-099
end  
  
  

GO

