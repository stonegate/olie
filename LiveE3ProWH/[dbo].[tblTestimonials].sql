USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTestimonials]') AND type in (N'U'))
DROP TABLE [dbo].[tblTestimonials]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTestimonials](	  [TestimonialID] BIGINT NOT NULL IDENTITY(1,1)	, [UName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [City] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [State] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Testimonial] TEXT(16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [SubmittedDate] DATETIME NULL	, [UserID] INT NOT NULL	, [UpdatedDt] DATETIME NULL	, CONSTRAINT [PK_tblTestimonials] PRIMARY KEY ([TestimonialID] ASC))USE [HomeLendingExperts]
GO

