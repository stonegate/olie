USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Insert_tblInsertTempUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Insert_tblInsertTempUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[usp_Insert_tblInsertTempUser]
(
@refuserid varchar(50)
,@tempuserid varchar(50)
,@temppswd varchar(50)
,@createddate varchar(50)
)
as
begin
INSERT INTO tblTempUser(refuserid,tempuserid,temppswd,createddate) Values
(
@refuserid,
@tempuserid,
@temppswd,
@createddate
)
end


GO

