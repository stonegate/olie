USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblManageMandatoryClientsInfo]') AND type in (N'U'))
DROP TABLE [dbo].[tblManageMandatoryClientsInfo]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblManageMandatoryClientsInfo](	  [ID] INT NOT NULL IDENTITY(1,1)	, [PartnerCompanyId] NVARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ClientCompanyName] VARCHAR(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [NetWorth] MONEY NULL	, [MandatoryPercentage] MONEY NULL	, [TotalMandatoryLimit] MONEY NULL	, [MandatoryLimitBal] MONEY NULL	, [IsActive] BIT NULL	, [CreatedBy] INT NULL	, [CreatedDate] DATETIME NULL	, [ModifiedBy] INT NULL	, [ModifiedDate] DATETIME NULL	, CONSTRAINT [PK_tblManageMandatoryClientsInfo] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO

