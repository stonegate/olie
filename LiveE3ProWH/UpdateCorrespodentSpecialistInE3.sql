USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateCorrespodentSpecialistInE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateCorrespodentSpecialistInE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateCorrespodentSpecialistInE3]    
@loannumber varchar(15)  ,
@CustName varchar(100),  
@CustN1 varchar(100),  
@CustN2 varchar(100),  
@CustN3 varchar(100),  
@CustN4 varchar(100),  
@CustD1 varchar(100),  
@CustD2 varchar(100),  
@CustD3 varchar(100),  
@CustD4 varchar(100),  
@CustT1 varchar(100) , 
@Result int output
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
 
 declare @trancount int;
 set @trancount = @@trancount;
 print 'trans - '+ cast(@trancount as varchar(10))
 
 BEGIN TRY

 if @trancount = 0
 begin transaction
  
	declare @LoanApp_id varchar(100) 
	set @Result=0
	  
	select @LoanApp_id=ID from InterlinqE3.dbo.mwlLoanApp where loannumber =''+@loannumber+''  
	print @LoanApp_id  
	  
	Declare @sql nvarchar(4000)   
	Declare @sql1 nvarchar(4000)      
	Declare @sql2 nvarchar(4000)      
	Declare @sql3 nvarchar(4000)      

	Declare @CondDate datetime     
	Declare @AssignDate datetime    

	set @CondDate=null
	set @AssignDate=null

   
		select top 1 @CondDate = mwlConditionState.updatedondate  from InterlinqE3.dbo.mwlLoanApp WITH (NOLOCK) , InterlinqE3.dbo.mwlCondition WITH (NOLOCK) , InterlinqE3.dbo.mwlConditionState WITH (NOLOCK)  where mwlLoanApp.ID = mwlCondition.ObjOwner_ID AND mwlCondition.ID = mwlConditionState.ObjOwner_ID AND mwlLoanApp.LoanNumber = '' + @loannumber + '' and  mwlConditionState.state = 'REJECTED' 
		order by mwlConditionState.updatedondate desc  
		  
		print @CondDate                    
		print '1'         
		 
		select  @AssignDate = isnull(CASE when (Cust4.updatedondate IS null) THEN ( CASE when (Cust3.updatedondate IS null ) THEN ( case when (Cust2.updatedondate IS null ) then ( case when (Cust1.updatedondate IS null ) then NULL else Cust1.updatedondate end ) else Cust2.updatedondate end ) else Cust3.updatedondate End ) else Cust4.updatedondate End,'')  from InterlinqE3.dbo.mwlLoanApp as la WITH (NOLOCK)  left join InterlinqE3.dbo.mwlcustomfield as Cust1 WITH (NOLOCK)  on  la.id=Cust1.Loanapp_id and Cust1.CustFieldDef_ID =''+ @CustD1 +'' left join InterlinqE3.dbo.mwlcustomfield as Cust2 WITH (NOLOCK)  on  la.id=Cust2.Loanapp_id and Cust2.CustFieldDef_ID =''+ @CustD2 +'' left join InterlinqE3.dbo.mwlcustomfield as Cust3 WITH (NOLOCK)  on  la.id=Cust3.Loanapp_id and Cust3.CustFieldDef_ID =''+ @CustD3 +'' left join InterlinqE3.dbo.mwlcustomfield as Cust4 WITH (NOLOCK)  on  la.id=Cust4.Loanapp_id and Cust4.CustFieldDef_ID =''+ @CustD4 +'' where la.loannumber=''+ @loannumber +'' order by Cust4.updatedondate,Cust3.updatedondate,Cust2.updatedondate,Cust1.updatedondate desc


		set @sql1= 'CondDate'
		set @sql1=@sql1+ '-'+cast(len(isnull(cast(@CondDate as varchar(30)),''))as varchar(30))+'-'+ convert(varchar(20),@CondDate,100) 
		print @sql1

		set @sql1=''
		set @sql1='AssginDate'
		print @sql1+ '-'+ convert(varchar(20),@AssignDate,100)

		print '2'

		if (len(isnull(cast(@CondDate as varchar(30)),'')) = 0 and convert(varchar(20),@AssignDate,100)<>'Jan  1 1900 12:00AM')
		begin
		print 'check condition date'
		--set @Result=0  
		print @Result
		--return
		goto lbexit 
		end

		if (len(isnull(cast(@CondDate as varchar(30)),'')) = 0)
		begin
		set @CondDate='Jan  1 1900 12:00AM'
		end

		if (len(isnull(@AssignDate,'')) = 0)
		begin
		set @AssignDate='1900-01-01 00:00:00.000'
		print @AssignDate
		end

		--if (len(isnull(cast(@CondDate as varchar(30)),'')) <> 0 and len(isnull(@AssignDate,''))<>0)  
		if (@CondDate  <> 'Jan  1 1900 12:00AM' and @AssignDate<>'Jan  1 1900 12:00AM')  
		begin
			if (@CondDate  < @AssignDate)
			begin
				print 'compare date'
				--set @Result=0  
				print @Result
				--return 
				goto lbexit 
			end	
		end
		     
		print '3'

		 set @sql = ''   
		 set @sql=' if not exists(select * From InterlinqE3.dbo.mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''  
		 set @sql= @sql + @CustN1    
		 set @sql= @sql + ''')    and  LoanApp_id in('''  
		 set @sql= @sql + @LoanApp_id  
		 set @sql= @sql +''') )  '  
		 set @sql=@sql +' insert into InterlinqE3.dbo.mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue)   values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustN1 +''','''+@CustName+''',Convert(varchar(12),getdate(),101))  '  
		 set @sql=@sql +'    else '  
		 set @sql=@sql +'   BEGIN  '  
		 set @sql=@sql +'   if not exists(select * From InterlinqE3.dbo.mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustN2 +''') and  LoanApp_id in( '''+@LoanApp_id+''') )  '  
		 set @sql=@sql +'     BEGIN '   
		 set @sql=@sql +'        insert into InterlinqE3.dbo.mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue)'  
		 set @sql=@sql +'        values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustN2 +''','''+@CustName+''',Convert(varchar(12),getdate(),101))  '  
		 set @sql=@sql +'     End  '  
		 set @sql=@sql +'     else '  
		 set @sql=@sql +'       BEGIN '   
		 set @sql=@sql +'      if not exists(select * From InterlinqE3.dbo.mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustN3 +''') and  LoanApp_id in('''+@LoanApp_id+'''))  '  
		 set @sql=@sql +'      Begin  '  
		 set @sql=@sql +'       insert into InterlinqE3.dbo.mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue)values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustN3 +''','''+@CustName+''','  
		 set @sql=@sql +'      Convert(varchar(12),getdate(),101))  '  
		 set @sql=@sql +'      End  '  
		 set @sql=@sql +'      else '  
		 set @sql=@sql +'      Begin  '  
		 set @sql=@sql +'      if not exists(select * From InterlinqE3.dbo.mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustN4 +''') and  LoanApp_id in('''+@LoanApp_id+''') )  '  
		 set @sql=@sql +'        begin '   
		 set @sql=@sql +'       insert into InterlinqE3.dbo.mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue)values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustN4 +''','''+@CustName+''',Convert(varchar(12),getdate(),101))  '  
		 set @sql=@sql +'        end  '  
		 set @sql=@sql +'      else  '  
		 set @sql=@sql +'        begin '   
		 set @sql=@sql +'       update InterlinqE3.dbo.mwlcustomfield set StringValue='''+@CustName+''',DateValue=Convert(varchar(12),getdate(),101) where CustFieldDef_ID in ('''+ @CustN4 +''') '  
		 set @sql=@sql +'       and  LoanApp_id in('''+@LoanApp_id+''')  '  
		 set @sql=@sql +'        end  '  
		 set @sql=@sql +'    End' --else 3  
		 set @sql=@sql +'     End '--else 2  
		 set @sql=@sql +'   End '--else 1  
		     
		   print @sql   
		   --exec sp_executesql @sql 
		     
		  print '4'
		  
		  set @sql2='' 
		  set @sql2=@sql2 + ' if not exists(select * From InterlinqE3.dbo.mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustD1+''') and  LoanApp_id in('''+@LoanApp_id+''') )  '
		  set @sql2=@sql2 + ' insert into InterlinqE3.dbo.mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue) '
		  set @sql2=@sql2 + ' values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustD1+''','''+@CustName+''',Convert(varchar(12),getdate(),101))  '
		  set @sql2=@sql2 + ' else BEGIN  if not exists(select * From InterlinqE3.dbo.mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustD2+''') and  LoanApp_id in('''+@LoanApp_id+''') ) '
		  set @sql2=@sql2 + ' begin  insert into InterlinqE3.dbo.mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue) values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustD2+''','''+@CustName+''',Convert(varchar(12),getdate(),101))  end '
		  set @sql2=@sql2 + ' else BEGIN  if not exists(select * From InterlinqE3.dbo.mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustD3+''') and  LoanApp_id in('''+@LoanApp_id+''') )  begin '
		  set @sql2=@sql2 + ' insert into InterlinqE3.dbo.mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue)values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustD3+''','''+@CustName+''',Convert(varchar(12),getdate(),101))  end '
		  set @sql2=@sql2 +  'else begin  if not exists(select * From InterlinqE3.dbo.mwlcustomfield  WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustD4+''') and  LoanApp_id in('''+@LoanApp_id+''') )  '
		  set @sql2=@sql2 +  'begin  insert into InterlinqE3.dbo.mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue,DateValue)values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustD4+''','''+@CustName+''',Convert(varchar(12),getdate(),101))  end  '
		  set @sql2=@sql2 +  'else  begin  update InterlinqE3.dbo.mwlcustomfield set StringValue='''+@CustName+''',DateValue=Convert(varchar(12),getdate(),101) where CustFieldDef_ID in ('''+ @CustD4+''') and  LoanApp_id in('''+@LoanApp_id+''')  end '
		  set @sql2=@sql2 +  ' end End End '

		print @sql2
		   --exec sp_executesql @sql2 

		  set @sql3='' 
		  set @sql3=@sql3 + 'if not exists(select * From InterlinqE3.dbo.mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID in ('''+ @CustT1+''') and  LoanApp_id in('''+@LoanApp_id+''') ) '
		  set @sql3=@sql3 + 'insert into InterlinqE3.dbo.mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue)'
		  set @sql3=@sql3 + 'values  (replace(newid(),''-'',''''), '''+@LoanApp_id+''','''+ @CustT1+''',Convert(varchar,getdate(),8))  ' 
		  
		print @sql3
		   --exec sp_executesql @sql3
		
		--declare @mainsql nvarchar(max)
		--set @mainsql=@sql+';'+@sql2+';'+@sql3
		--exec sp_executesql @mainsql
        exec sp_executesql @sql
        exec sp_executesql @sql2
        exec sp_executesql @sql3

       
      set @Result=1
      
      lbexit:
      COMMIT
      print '5'
      return @Result      
      
END TRY

BEGIN CATCH

  -- Whoops, there was an error

  IF @@TRANCOUNT > 0

	 ROLLBACK
	 
      --set @Result=0
      return @Result
      
END CATCH

END  
GO

