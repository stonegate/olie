USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_mwlUnderwritingSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_mwlUnderwritingSummary]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/07/2012
-- Description:	Procedure for clspipelineE3 BLL
-- =============================================
CREATE PROCEDURE [dbo].[sp_mwlUnderwritingSummary]
(
@ID varchar(32),
@LoanApp_ID varchar(32),
@Underwriter_ID varchar(32),
@UnderwriterName varchar(115)
)
AS
BEGIN

	SET NOCOUNT ON;

  
IF NOT EXISTS 
(SELECT LoanApp_ID FROM mwlUnderwritingSummary WHERE LoanApp_ID=@LoanApp_ID)
 INSERT INTO mwlUnderwritingSummary (ID,LoanApp_ID,Underwriter_ID,UnderwriterName) 
VALUES (@ID,@LoanApp_ID,@Underwriter_ID,@UnderwriterName)
  ELSE UPDATE mwlUnderwritingSummary 
SET Underwriter_ID=@Underwriter_ID,
UnderwriterName=@UnderwriterName
 WHERE LoanApp_ID=@LoanApp_ID
                  
END


GO

