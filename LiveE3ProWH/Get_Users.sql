USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_Users]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Get_Users]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author, Somnath M. R.>
-- Create date: <Create Date, 17th Dec 2013>
-- Description:	<Description, Select from tblUsers table>
-- EXEC Get_Users 2
-- =============================================
CREATE PROCEDURE [dbo].[Get_Users]
	@RoleId Int = Null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
		U.ufirstname + ' ' + U.ulastname As UserName,
		U.userid,
		U.region
	FROM
		[dbo].[tblUsers] U 
	Where  (@RoleId IS NULL OR U.urole = @RoleId)
		AND U.IsActive = 1
END

GO

