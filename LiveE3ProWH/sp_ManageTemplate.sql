USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ManageTemplate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ManageTemplate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_ManageTemplate]      
(                
@ActionID int,                
@Title nvarchar(1000),                
@Body text,                
@IsActive bit=0,                
@IsManageTemplate bit=0,                 
@Role nvarchar(500),      
@RoleR nvarchar(500),               
@RoleC nvarchar(500),               
@ManageTempVisbleAd bit=0,                
@ManageTempVisbleDR bit=0,                
@ManageTempVisbleBR bit=0,                
@ManageTempVisbleLO bit=0,                
@ManageTempVisbleCU bit=0,                
@ManageTempVisbleRE bit=0,                
@ManageTempVisbleRM bit=0,                
@ManageTempVisbleDP bit=0,                
@ManageTempVisbleCP bit=0,                
@ManageTempVisbleHY bit=0,                
@ManageTempVisbleUM bit=0,                
@ManageTempVisbleDCFI bit=0,                
@ManageTempVisibleSCFI bit=0,                
@ModifiedBy int,                
@ModifiedDate datetime,                
@NewsID int --Used for update and delete purpose                
)                
AS                
BEGIN
SET NOCOUNT ON;                
  

If (@ActionID = 1) -- Insert                
Begin
INSERT INTO tblManageTemplate (Title, Body, IsActive, IsManageTemplate, CreatedBy, CreatedDate,
Role, RoleC, RoleR, ManageTempVisbleAd, ManageTempVisbleDR, ManageTempVisbleBR, ManageTempVisbleLO,
ManageTempVisbleCU, ManageTempVisbleRE, ManageTempVisbleRM, ManageTempVisbleDP,
ManageTempVisbleCP, ManageTempVisbleHY, ManageTempVisbleUM, ManageTempVisbleDCFI,
ManageTempVisibleSCFI, ModifiedBy, ModifiedDate)
	VALUES (@Title, @Body, @IsActive, @IsManageTemplate, @ModifiedBy, @ModifiedDate, @Role, @RoleC, @RoleR,
	 @ManageTempVisbleAd, @ManageTempVisbleDR, @ManageTempVisbleBR, @ManageTempVisbleLO, @ManageTempVisbleCU,
	  @ManageTempVisbleRE, @ManageTempVisbleRM, @ManageTempVisbleDP, @ManageTempVisbleCP, @ManageTempVisbleHY, @ManageTempVisbleUM,
	   @ManageTempVisbleDCFI, @ManageTempVisibleSCFI, @ModifiedBy, @ModifiedDate)  
  

--INSERT INTO retail_tblmanagetemplate (Title, Body, IsActive, IsManageTemplate, CreatedBy, CreatedDate,  
--Role, RoleR, ManageTempVisbleAd, ManageTempVisbleDR, ManageTempVisbleBR, ManageTempVisbleLO,  
--ManageTempVisbleCU, ManageTempVisbleRE, ManageTempVisbleRM, ManageTempVisbleDP,  
--ManageTempVisbleCP, ManageTempVisbleHY, ManageTempVisbleUM, ManageTempVisbleDCFI,  
--ManageTempVisibleSCFI, ModifiedBy, ModifiedDate)  
-- VALUES (@Title, @Body, @IsActive, @IsManageTemplate, @ModifiedBy, @ModifiedDate, @Role, @RoleR, @ManageTempVisbleAd, 
--@ManageTempVisbleDR, @ManageTempVisbleBR, @ManageTempVisbleLO, @ManageTempVisbleCU, 
--@ManageTempVisbleRE, @ManageTempVisbleRM, @ManageTempVisbleDP,
-- @ManageTempVisbleCP, @ManageTempVisbleHY, @ManageTempVisbleUM, @ManageTempVisbleDCFI, @ManageTempVisibleSCFI, @ModifiedBy, @ModifiedDate)

SELECT
	1                
END                

If (@ActionID = 2) -- Update                
Begin
UPDATE tblManageTemplate WITH (UPDLOCK)
SET	Title = @Title,
	Body = @Body,
	IsActive = @IsActive,
	IsManageTemplate = @IsManageTemplate,
	Role = @Role,
	RoleC = @RoleC,
	RoleR = @RoleR,
	ManageTempVisbleAd = @ManageTempVisbleAd,
	ManageTempVisbleDR = @ManageTempVisbleDR,
	ManageTempVisbleBR = @ManageTempVisbleBR,
	ManageTempVisbleLO = @ManageTempVisbleLO,
	ManageTempVisbleCU = @ManageTempVisbleCU,
	ManageTempVisbleRE = @ManageTempVisbleRE,
	ManageTempVisbleRM = @ManageTempVisbleRM,
	ManageTempVisbleDP = @ManageTempVisbleDP,
	ManageTempVisbleCP = @ManageTempVisbleCP,
	ManageTempVisbleHY = @ManageTempVisbleHY,
	ManageTempVisbleUM = @ManageTempVisbleUM,
	ManageTempVisbleDCFI = @ManageTempVisbleDCFI,
	ManageTempVisibleSCFI = @ManageTempVisibleSCFI,
	ModifiedBy = @ModifiedBy,
	ModifiedDate = @ModifiedDate
WHERE ID = @NewsID

--UPDATE retail_tblmanagetemplate WITH (UPDLOCK)  
--SET Title = @Title,  
-- Body = @Body,  
-- IsActive = @IsActive,  
-- IsManageTemplate = @IsManageTemplate,  
-- Role = @Role,  
-- RoleR = @RoleR,  
-- ManageTempVisbleAd = @ManageTempVisbleAd,  
-- ManageTempVisbleDR = @ManageTempVisbleDR,  
-- ManageTempVisbleBR = @ManageTempVisbleBR,  
-- ManageTempVisbleLO = @ManageTempVisbleLO,  
-- ManageTempVisbleCU = @ManageTempVisbleCU,  
-- ManageTempVisbleRE = @ManageTempVisbleRE,  
-- ManageTempVisbleRM = @ManageTempVisbleRM,  
-- ManageTempVisbleDP = @ManageTempVisbleDP,  
-- ManageTempVisbleCP = @ManageTempVisbleCP,  
-- ManageTempVisbleHY = @ManageTempVisbleHY,  
-- ManageTempVisbleUM = @ManageTempVisbleUM,  
-- ManageTempVisbleDCFI = @ManageTempVisbleDCFI,  
-- ManageTempVisibleSCFI = @ManageTempVisibleSCFI,  
-- ModifiedBy = @ModifiedBy,  
-- ModifiedDate = @ModifiedDate  
--WHERE ID = @NewsID  


SELECT
	1                
END                

If (@ActionID = 3) -- Select                
Begin
SELECT
	*
FROM tblManageTemplate
WHERE ID = @NewsID  
--SELECT  
-- *  
--FROM retail_tblmanagetemplate  
--WHERE ID = @NewsID             
END                

END

GO

