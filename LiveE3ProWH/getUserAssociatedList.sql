USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getUserAssociatedList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[getUserAssociatedList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getUserAssociatedList] 
@userId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

	SELECT DISTINCT userid
		,u.ufirstname + ' ' + u.ulastname AS userName
	FROM tblusers u
	LEFT JOIN tblcrsmassociation crsm ON crsm.crsmuserid = u.userid
	LEFT JOIN tblcrsassociation crs ON crs.crsuserid = u.userid
	WHERE crsm.associateuserid = @userId
		OR crs.associateuserid = @userId

	SELECT u.ufirstname + ' ' + u.ulastname AS userName
	FROM tblusers u
	WHERE userid = @userId
END


GO

