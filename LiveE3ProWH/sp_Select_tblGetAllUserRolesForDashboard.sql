USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetAllUserRolesForDashboard]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetAllUserRolesForDashboard]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblGetAllUserRolesForDashboard]
as
begin
select * from tblroles where Id not in(23)
end


GO

