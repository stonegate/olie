USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetRetailOfficeIDs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetRetailOfficeIDs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Stonegate>
-- Create date: <10/9/2012>
-- Description:	<This will Get Retail Office IDs>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRetailOfficeIDs]

AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

Select 
	distinct(isnull(office,''))office 
From 
	mwlinstitution inner join mwlloanapp on mwlloanapp.ID = mwlinstitution.objOwner_id 
where 
	InstitutionType = 'Branch' and objownerName='BranchInstitution' 
	and ChannelType='Retail' and office like '%Retail%'
	and office is not null and office <> '' 
	and CurrentStatus IN ('01 - Registered','05 - Application Taken','03 - Appt Set to Review Disclosures','04 - Disclosures Sent','07 - Disclosures Received','09 - Application Received',
	'13 - File Intake','17 - Submission On Hold','19 - Disclosures','25 - Submitted to Underwriting','29 - In Underwriting','33 - Cleared to Close','37 - Pre-Closing','41 - In Closing','43 - Closing on Hold','45 - Docs Out','50 - Closing Package Received',
	'09 - Application Recieved', '50 - Closing Package Recieved','20 - Disclosures On Hold','21 - In Processing',
	'58 - Cleared for Funding','62 - Funded','10 - Application on Hold','22 - Appraisal Received','15 - Appraisal Ordered')

END

GO

