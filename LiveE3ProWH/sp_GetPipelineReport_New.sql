USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPipelineReport_New]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPipelineReport_New]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--sp_GetPipelineReport 0,'C4D2D8038F614DF8A1B8A6622C99FC3E','FD0C2122181F432FB5C8A73E060D10CD',''
-- =============================================
-- Author:		Stonegate
-- Create date: 10/3/2012
-- Description:	Get pipeline report
-- Modify date:
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetPipelineReport_New] (
	@RoleID INT
	,@CorrespondentType NVARCHAR(2000)
	,@CustomField NVARCHAR(2000)
	,@StrIDs NVARCHAR(max)
	,@ChannelType NVARCHAR(max) = NULL
	)
AS
BEGIN
	--GetAllMyPipelineReport
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;-- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	DECLARE @SQL VARCHAR(max)
	DECLARE @SQL1 VARCHAR(max)
	DECLARE @SQL2 VARCHAR(max)
	DECLARE @InternalUsers VARCHAR(500)

	SELECT @InternalUsers = Roles
	FROM tblQuickLinks
	WHERE ID = 18 --Lock Confirmation

	SET @SQL1 = ''
	SET @SQL = ''

	IF (
			@RoleID = 0
			OR @RoleID = 34
			)
	BEGIN
		SET @SQL = @SQL + ' select top 100 '
	END
	ELSE
	BEGIN
		SET @SQL = @SQL + ' select top 6000 '
	END

	SET @RoleID = (
			CASE 
				WHEN @RoleID = '38'
					THEN '0'
				ELSE @RoleID
				END
			)
	SET @SQL = @SQL + 
		' ''E3'' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.Channeltype as BusType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,
loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,
borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate,
approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,
case when loanData.LoanProgDesc is null then ''Incomplete'' else loanData.LoanProgDesc end as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,
CustFNMA.YNValue,LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,uEmail.Email as Underwriter_Email,
CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,
CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate, 
CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'' and a.objOwner_ID IN 
(SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0 
WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'')
and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0 ELSE 
(SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where(DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') 
IN(SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions 
FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and a.objOwner_ID IN (SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER,
case cust.stringvalue WHEN ''--Select One--'' then '''' else cust.stringvalue end as UnderwriterType '

	IF (
			@RoleID = 2
			OR @RoleID = 27
			)
	BEGIN
		SET @SQL = @SQL + ',isnull(Cust.StringValue,'''') as  CorrespondentType'
	END

	SET @SQL = @SQL + ',LockRecord.DeliveryOption,BOffices.Office TPORegion,loanapp.Originator_id AEId '

	IF (@RoleID = 0)
	BEGIN
		SET @SQL = @SQL + ',CASE WHEN (COALESCE(LockRecord.Status,'''') =''CANCELED'' OR COALESCE(LockRecord.Status,'''') =''CONFIRMED'' OR COALESCE(LockRecord.Status,'''') =''EXPIRED'' )  THEN 1 ELSE 0 END As  viewdoc'
	END
	ELSE IF (
			@RoleID = 1
			OR @RoleID = 2
			OR @RoleID = 8
			OR @RoleID = 12
			OR @RoleID = 32
			OR @RoleID = 34
			) --Internal users
	BEGIN
		IF (
				@RoleID IN (
					SELECT *
					FROM dbo.splitstring(@InternalUsers, ',')
					)
				)
		BEGIN
			SET @SQL = @SQL + ',CASE WHEN (COALESCE(LockRecord.Status,'''') =''CANCELED'' OR COALESCE(LockRecord.Status,'''') =''CONFIRMED'' OR COALESCE(LockRecord.Status,'''') =''EXPIRED'' )  THEN 1 ELSE 0 END As  viewdoc'
		END
		ELSE
		BEGIN
			SET @SQL = @SQL + ', 0 As  viewdoc'
		END
	END
	ELSE IF (
			@RoleID = 3
			OR @RoleID = 20
			OR @RoleID = 21
			) --External users 
	BEGIN
		SET @SQL = @SQL + ',CASE WHEN (COALESCE(LockRecord.Status,'''') =''CANCELED'' OR COALESCE(LockRecord.Status,'''') =''CONFIRMED'' OR COALESCE(LockRecord.Status,'''') =''EXPIRED'' )  THEN 1 ELSE 0 END As  viewdoc'
	END
	ELSE
		SET @SQL = @SQL + ', 0 As  viewdoc'

	SET @SQL = @SQL + 'from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id 
and (Broker.InstitutionType = loanapp.channeltype or Broker.objownerName=loanapp.channeltype) left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' 
Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc=''01 - Registered'' 
Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved'' Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  
and ObjectName=''mwlLoanData'' and fieldname=''refipurpose'' 
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and (Corres.InstitutionType = loanapp.channeltype or Corres.objownerName=loanapp.channeltype) 
Left join (Select distinct Office,ObjOwner_id from dbo.mwlInstitution Where InstitutionType = ''Branch'' and objownerName=''BranchInstitution'') as BOffices on BOffices.ObjOwner_id = loanapp.id  
left join dbo.mwamwuser as uEmail on uSummary.UnderWriter_id = uEmail.id 
left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID = ''' + @CorrespondentType + '''
left join mwlcustomfield as CustFNMA on CustFNMA.loanapp_id = loanapp.id and CustFNMA.CustFieldDef_ID = ''' + @CustomField + ''' '
	--If (@RoleID = 2)
	--Begin
	--	set @SQL = @SQL + ' left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID =''C4D2D8038F614DF8A1B8A6622C99FC3E'' '
	--End
	SET @SQL = @SQL + ' where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' and '

	IF (@RoleID = 16)
	BEGIN
		SET @SQL = @SQL + ' CurrentStatus IN (''01 - Registered'',''05 - Application Taken'', ''03 - Appt Set to Review Disclosures'', ''04 - Disclosures Sent'',''07 - Disclosures Received'',''09 - Application Received'',''13 - File Intake'',''17 - Submission On Hold'',''19 - Disclosures'',''20 - Disclosures On Hold'',''21 - In Processing'',''25 - Submitted to Underwriting'',''29 - In Underwriting''),''33 - Cleared to Close'',''37 - Pre-Closing'',''41 - In Closing'',''43 - Closing on Hold'',''45 - Docs Out'')'
	END
	ELSE
	BEGIN
		SET @SQL = @SQL + ' CurrentStatus IN (''01 - Registered'',''05 - Application Taken'',''03 - Appt Set to Review Disclosures'',''04 - Disclosures Sent'', ''07 - Disclosures Received'',''09 - Application Received'',''13 - File Intake'',''17 - Submission On Hold'',''19 - Disclosures'',''20 - Disclosures On Hold'',''21 - In Processing'',''25 - Submitted to Underwriting'',''29 - In Underwriting'',''15 - Appraisal Ordered'',''22 - Appraisal Received'',''10 - Application on Hold'')'
	END

	IF (@RoleID <> 0)
	BEGIN
		IF (len(@StrIDs) = 0)
		BEGIN
			IF (
					(
						@RoleID = 1
						OR @RoleID = 2
						OR @RoleID = 8
						OR @RoleID = 32
						)
					AND (
						@ChannelType <> ''
						AND @ChannelType IS NOT NULL
						)
					)
			BEGIN
				SET @SQL = @SQL + ' and Channeltype IN (''' + @ChannelType + ''')'
			END

			IF (@RoleID = 13)
			BEGIN
				SET @SQL1 = @SQL1 + ' and loandat.LT_Broker_Rep in (select * from dbo.SplitString(''' + @StrIDs + ''','',''))'
			END
					--else if (@RoleID = 2)
					--      Begin
					--	set @SQL1 = @SQL1 + ' and (Originator_id in (select * from dbo.SplitString('''+@StrIDs+''','','')))'
					--      End
			ELSE IF (
					@RoleID = 3
					OR @RoleID = 20
					OR @RoleID = 21
					)
			BEGIN
				IF (@RoleID = 20)
				BEGIN
					SET @SQL1 = @SQL1 + ' and Channeltype=''CORRESPOND'''
				END

				SET @SQL1 = @SQL1 + ' and (Corres.CompanyEmail IN(''' + @StrIDs + ''') OR Broker.CompanyEmail IN(''' + @StrIDs + '''))'
			END
			ELSE
			BEGIN
				SET @SQL1 = @SQL1 + ' and (Originator_id in (select * from dbo.SplitString(''' + @StrIDs + ''','','')))'
			END
		END
		ELSE
		BEGIN
			IF (@RoleID = 13)
			BEGIN
				SET @SQL1 = @SQL1 + ' and loandat.LT_Broker_Rep in (select * from dbo.SplitString(''' + @StrIDs + ''','',''))'
			END
					--else if (@RoleID = 2)
					--      Begin
					--set @SQL1 = @SQL1 + ' and (Originator_id in (select * from dbo.SplitString('''+@StrIDs+''','','')))'
					--End
			ELSE IF (
					@RoleID = 3
					OR @RoleID = 20
					OR @RoleID = 21
					)
			BEGIN
				IF (@RoleID = 20)
				BEGIN
					SET @SQL1 = @SQL1 + ' and Channeltype=''CORRESPOND'''
				END

				SET @SQL1 = @SQL1 + ' and (Corres.CompanyEmail IN(''' + @StrIDs + ''') OR Broker.CompanyEmail IN(''' + @StrIDs + '''))'
			END
			ELSE IF (
					(
						@RoleID = 1
						OR @RoleID = 2
						OR @RoleID = 8
						OR @RoleID = 32
						)
					AND (
						@ChannelType <> ''
						AND @ChannelType IS NOT NULL
						)
					)
			BEGIN
				SET @SQL = @SQL + ' and Channeltype IN (select * from dbo.SplitString(''' + @ChannelType + ''','',''))  and (Originator_id in (select * from dbo.SplitString(''' + @StrIDs + ''','','')))'
			END
			ELSE
			BEGIN
				SET @SQL1 = @SQL1 + ' and (Originator_id in (select * from dbo.SplitString(''' + @StrIDs + ''','','')))'
			END
		END
	END

	SET @SQL1 = @SQL1 + @SQL1 + ' order by Per desc'

	PRINT (@SQL + @SQL1)

	--exec sp_executesql (@SQL +  @SQL1)
	EXEC (@SQL + @SQL1)
END


GO

