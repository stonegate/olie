USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Insert_tblUserInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Insert_tblUserInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author: Gourav                  

-- Create date: 10/03/2012                  

-- Description: Procedure for clsUser.cs bll                  

-- =============================================                  

-- =============================================                  

-- Change Author: Shyamal                   

-- Create date: 18/12/2012                  

-- Description: Procedure is changed for reflect newly added field 'UWTeamleadid'                  

-- =============================================                  

-- =============================================                  

-- Change Author: Rajesh Mund                  

-- Create date: 07/12/2012                  

-- Description: Procedure is changed for reflect newly added field 'isPublish'                  

      

CREATE Procedure [dbo].[usp_Insert_tblUserInfo]                  

(                  

 @ufirstname varchar(Max)=null                  

,@ulastname varchar(Max)=null                  

,@uemailid varchar(Max)=null                  

,@StateBelongTo varchar(Max)=null                  

,@urole varchar(Max)=null                  

,@isadmin varchar(Max)=null                  

,@isactive varchar(Max)=null                  

,@uidprolender varchar(Max)=null                  

,@uparent varchar(Max)=null                  

,@upassword varchar(Max)=null                  

,@mobile_ph varchar(Max)=null                  

,@phonenum varchar(Max)=null                  

,@loginidprolender varchar(Max)=null                  

,@carrierid varchar(Max)=null                  

,@MI varchar(Max)=null                  

,@companyname varchar(Max)=null                  

,@AEID varchar(Max)=null                  

,@Region varchar(Max)=null                  

,@E3Userid varchar(Max)=null                  

,@PartnerCompanyid varchar(Max)=null                  

,@BrokerType varchar(Max)=null                  

,@PasswordExpDate varchar(Max)=null                  

,@chkRegisterloan varchar(Max)=null                  

,@chkQuickPricer varchar(Max)=null                  

,@chkpricelockloan varchar(Max)=null                  

,@chkSubmitloan varchar(Max)=null                  

,@chkSubmitcondition varchar(Max)=null                  

,@chkorderappraisal varchar(Max)=null                  

,@chkorderfhano varchar(Max)=null                  

,@chkscheduleclosingdate varchar(Max)=null                  

,@chkupdateloan varchar(Max)=null                  

,@chkaccuratedocs varchar(Max)=null                  

,@OrgSystem varchar(Max)=null                  

,@chkSubmitClosingPackage varchar(Max)=null                  

,@PartnerType varchar(Max)=null                  

,@CreatedBy varchar(Max)=null                  

,@IsUserInNewProcess varchar(Max)=null                  

,@chkOrderTaxTranscript varchar(Max)=null                  

,@isPublish varchar(Max)=null                  

,@UWTeamleadid int =null                  

,@bitIsKatalyst bit =0    

,@IsForthPartyOrigination bit =0    

,@IsemergingBanker varchar(2) = null 

,@IsC3CCI bit = null 

,@chkLockConfirmation bit = null --  By tavant 215-BRD

,@chkPurchaseAdvice bit = null -- By tavant 129 BRD

,@lastUpdatedBy varchar(Max)=null    
)                  

as                  

begin    



SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

----UROLE VARIABLE DECLARATION----------    

DECLARE @Broker Int    

SET @Broker = 3    

DECLARE @Correspondent Int    

SET @Correspondent = 20    

DECLARE @Hybrid Int    

SET @Hybrid = 21    

/*Start Added By Tavant For BRD198*/

Declare @RegionNew VARCHAR(MAX)  



IF  @urole = 22 OR @urole = 27 OR @urole = 28 

BEGIN



SET @RegionNew = @Region

Set @Region = ''



END

/*END Added By Tavant For BRD198*/

If(@urole = @Broker OR @urole = @Correspondent OR @urole = @Hybrid)    

Begin    



Select @bitIsKatalyst = Case When ISNULL((Select Count(UserID) from tblusers WITH(NOLOCK) where PartnerCompanyId = @PartnerCompanyid and ISNULL(IsKatalyst,0) <> 1),0) = 0 Then 1 Else 0 End  

  

End    



BEGIN TRY



BEGIN TRAN



Insert into tblusers                   

(                  

 ufirstname

,ulastname                  

,uemailid                  

,StateBelongTo                  

,urole                  

,isadmin                  

,isactive                  

,uidprolender                  

,uparent                  

,upassword                  

,mobile_ph                  

,phonenum                  

,loginidprolender                  

,carrierid                  

,MI                  

,companyname                  

,AEID                

,Region                  

,E3Userid                  

,PartnerCompanyid                  

,BrokerType                  

,PasswordExpDate                  

,chkRegisterloan                  

,chkQuickPricer                  

,chkpricelockloan                  

,chkSubmitloan                  

,chkSubmitcondition                  

,chkorderappraisal                  

,chkorderfhano                  

,chkscheduleclosingdate                  

,chkupdateloan                  

,chkaccuratedocs                  

,OrgSystem                  

,chkSubmitClosingPackage                  

,PartnerType                  

,CreatedBy                  

,IsUserInNewProcess                  

,chkOrderTaxTranscript                  

,isPublish                  

,UWTeamleadid                 

,Iskatalyst    

,IsFouthPartyOrigination

,IsC3Integration  

,chkLockConfirmation --  By tavant 215-BRD  

,chkPurchaseAdvice   -- by tavant 129 BRD

,createdAt   -- By tavant 253 BRD 

,lastUpdatedBy -- By tavant 253 BRD 

,LastUpdateAt -- By tavant 253 BRD 
)                  

Values                  

(                  

 @ufirstname                  

,@ulastname                  

,@uemailid                  

,@StateBelongTo                  

,@urole                  

,@isadmin                  

,@isactive                  

,@uidprolender                  

,@uparent                  

,@upassword                  

,@mobile_ph                  

,@phonenum                  

,@loginidprolender           

,@carrierid                  

,@MI                  

,@companyname                  

,@AEID                  

,@Region                  

,@E3Userid                  

,@PartnerCompanyid                  

,@BrokerType                  

,@PasswordExpDate                  

,@chkRegisterloan                  

,@chkQuickPricer             

,@chkpricelockloan                  

,@chkSubmitloan                  

,@chkSubmitcondition                  

,@chkorderappraisal                  

,@chkorderfhano                  

,@chkscheduleclosingdate                  

,@chkupdateloan                  

,@chkaccuratedocs                  

,@OrgSystem                  

,@chkSubmitClosingPackage                  

,@PartnerType                  

,@CreatedBy                  

,@IsUserInNewProcess                  

,@chkOrderTaxTranscript                  

,@isPublish                  

,@UWTeamleadid                

,@bitIsKatalyst     

,@IsForthPartyOrigination

,@IsC3CCI    

,@chkLockConfirmation --  By tavant 215-BRD   

,@chkPurchaseAdvice  -- By tavant 129 BRD 

,getdate() -- By tavant 253 BRD 

,@lastUpdatedBy -- By tavant 253 BRD 

,getdate() -- By tavant 253 BRD 

)                  

           

Declare @UserId bigint  

Declare @IsRoleAdmin bigint

Set @UserId = (select scope_Identity())

Set @IsRoleAdmin = (select top 1 urole from tblUsers where userId = @CreatedBy)



IF @urole = 20 OR @urole = 21 

BEGIN



-- Added By Tavnt Team On 05-12-2013 for Emerging Banker

if @IsRoleAdmin = '0' and @isadmin = '1'



BEGIN



	 if exists (Select 1 from tblEmergingBankers where PartnerCompanyId = @PartnerCompanyid  and Status = 'Y' and Flag='EB')

	 

	 BEGIN

		 Update tblEmergingBankers

		 SET IsEmergingBanker = @IsemergingBanker,

				 ModifiedBy = scope_Identity(),

				 ModifiedDate = getdate()

		where PartnerCompanyId = @PartnerCompanyid  and Status = 'Y' and Flag='EB'

	 END



	 ELSE

	

	BEGIN 

		Insert  into tblEmergingBankers (PartnerCompanyId,CompanyName,RoleId,IsEmergingBanker,Flag,Status,CreatedBy,CreatedDate)

		VALUES (@PartnerCompanyid,@companyname,@urole,@IsemergingBanker,'EB','Y',scope_Identity(),getdate())

	END 

END



END 

-- Added By Tavnt Team On for BRD198

IF   @urole = 22 OR @urole = 27 OR @urole = 28 

BEGIN



INSERT INTO tblUserRegionMapping SELECT @UserId,@urole,1,items,@CreatedBy,getdate(),null,null,'Y'

from dbo.SplitString(@RegionNew,',')



END





COMMIT TRAN



Select @UserId



END TRY



BEGIN CATCH



   ROLLBACK TRAN



END CATCH

              

END 

GO

