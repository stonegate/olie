USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdatetblLoanReg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdatetblLoanReg]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create procEDURE [dbo].[sp_UpdatetblLoanReg]   
 -- Add the parameters for the stored procedure here  
(
@LoanNumber varchar(MAX),
@loanregid int
)
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
update tblLoanReg set loannumber =@LoanNumber
				 where loanregid = @loanregid
END



GO

