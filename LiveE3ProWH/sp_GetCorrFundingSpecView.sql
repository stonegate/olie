USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCorrFundingSpecView]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCorrFundingSpecView]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================
-- Author:		<Stonegate>
-- Create date: <10/8/2012>
-- Description:	<This will Get Corr. Funding Spec. View>
-- ======================================================
CREATE PROCEDURE [dbo].[sp_GetCorrFundingSpecView] 
(
	@CustomField varchar(250),
	@CustomeFieldCFSName varchar(250),
	@CustomFieldN1 varchar(250),
	@CustomFieldN2 varchar(250),
	@CustomFieldN3 varchar(250),
	@CustomFieldN4 varchar(250),
	@SubmittedDateID varchar(250),
	@SubmittedDateTimeID varchar(250),
	@strFrom varchar(50)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)

Declare @SQL nvarchar(max)
set @SQL=''
Set @SQL = 'Select ''E3'' as DB,CustDate.DateValue as LastSubmittedDate,CustTime.StringValue as LastSubmittedTime ,Offices.Office as office,
Corres.Company as coname,BOffices.Office as Boffice,loanapp.CurrDecStatusDate as Decisionstatusdate,loanapp.CurrPipeStatusDate as CurrentStatusDate,
loanapp.ChannelType as ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,
LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,
CASE TransType when NULL THEN '''' WHEN ''P'' THEN ''Purchase money first mortgage'' WHEN ''R'' THEN ''Refinance'' WHEN ''2'' THEN ''Purchase money second mortgage'' WHEN ''S'' THEN ''Second mortgage, any other purpose'' 
WHEN ''A'' THEN ''Assumption'' WHEN ''HOP'' THEN ''HELOC - other purpose'' WHEN ''HP'' THEN ''HELOC - purchase'' ELSE '''' END AS TransType,
Case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,
loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockDatetime as LockDatetime,LockRecord.LockExpirationDate,
UnderwriterName as UnderWriter,Broker.Company as BrokerName,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,Loandata.LoanProgDesc as ProgramDesc,
CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'' 
and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0
WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') 
and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'')
and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0 ELSE (SELECT count(a.ID) as TotalCleared
FROM dbo.mwlCondition a where (DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') 
and a.objOwner_ID IN (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100/(SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a 
where(DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1
WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER,
BOffices.Office as OFFICE_NAME1,isnull(Cust.StringValue,'''') as CorrespondentType,
CASE when (CustN4.StringValue IS null or CustN4.StringValue=''Other'') THEN (CASE when(CustN3.StringValue IS null or CustN3.StringValue=''Other'') 
THEN (case when (CustN2.StringValue IS null or CustN2.StringValue=''Other'') then (case when (CustN1.StringValue IS null or CustN1.StringValue=''Other'') then '''' 
else CustN1.StringValue end ) else CustN2.StringValue end ) else CustN3.StringValue End ) else CustN4.StringValue End as CorrespodentSpecialist,
CASE when (CFSName.StringValue IS null or CFSName.StringValue=''Other'') THEN '''' else CFSName.StringValue End  as  CorrespodentFSpecialist
from mwlLoanApp loanapp Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and borr.Sequencenum=1 
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id 
Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id
and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id 
and LockRecord.LockType=''LOCK'' and LockRecord.Status<>''CANCELED''
Left join dbo.mwlInstitution as Offices on Offices.ObjOwner_id = loanapp.id and Offices.InstitutionType = ''Broker'' 
and Offices.objownerName=''Broker''
Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = loanapp.id and COffices.InstitutionType = ''CORRESPOND'' and COffices.objownerName=''Contacts''
Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.InstitutionType = ''Branch'' and BOffices.objownerName=''BranchInstitution''
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''
left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id and Cust.CustFieldDef_ID ='''+ @CustomField +'''
left join mwlcustomfield as CFSName on CFSName.loanapp_id = loanapp.id  and CFSName.CustFieldDef_ID ='''+ @CustomeFieldCFSName +'''
left join mwlcustomfield as CustN1 on CustN1.loanapp_id = loanapp.id  and CustN1.CustFieldDef_ID ='''+ @CustomFieldN1 +'''
left join mwlcustomfield as CustN2 on CustN2.loanapp_id = loanapp.id  and CustN2.CustFieldDef_ID ='''+ @CustomFieldN2 +'''
left join mwlcustomfield as CustN3 on CustN3.loanapp_id = loanapp.id  and CustN3.CustFieldDef_ID ='''+ @CustomFieldN3 +'''
left join mwlcustomfield as CustN4 on CustN4.loanapp_id = loanapp.id  and CustN4.CustFieldDef_ID ='''+ @CustomFieldN4 +'''
left join mwlcustomfield as CustDate on CustDate.loanapp_id = loanapp.id and CustDate.CustFieldDef_ID ='''+ @SubmittedDateID +'''
left join mwlcustomfield as CustTime on CustTime.loanapp_id = loanapp.id  and CustTime.CustFieldDef_ID ='''+ @SubmittedDateTimeID +''' '
                 
if (@strFrom = 'pur')
Begin
	set @SQL = @SQL + ' where CurrentStatus  IN (''54 - Purchase Pending'') '
End
else if (@strFrom = 'cfunding')
Begin
	set @SQL = @SQL + ' where CurrentStatus  IN (''58 - Cleared for Funding'') '
End
set @SQL = @SQL + ' and ChannelType in(''CORRESPOND'') '
set @SQL = @SQL + ' and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' '

Print @SQL
exec sp_executesql @SQL

END

GO

