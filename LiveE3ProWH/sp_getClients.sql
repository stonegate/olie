USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getClients]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getClients]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_getClients] 

	@aeIds varchar(Max)=null 

AS

BEGIN

	SET NOCOUNT ON;
		select distinct #tempTable.*
			   , case when (crs.partnercompanyid is not null or crsm.partnercompanyid is not null) then 'YES' else 'NO' end as Associated

		from 
		(
		select 
		 MAX(aeid) as aeid
		,u.partnercompanyid as partnercompanyid
		,MAX(u.companyname) as companyname

		from tblusers u
		where aeid in (select * from dbo.splitstring(@aeIds,','))
		and u.partnercompanyid <> ''
		group by partnercompanyid) #tempTable 

		left join tblCRSAssociation crs on #tempTable.partnercompanyid = crs.partnercompanyid
		left join tblCRSMAssociation crsm on #tempTable.partnercompanyid = crsm.partnercompanyid
END


GO

