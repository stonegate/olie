USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_delete_tblNewsAndAnnouncement_Retail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_delete_tblNewsAndAnnouncement_Retail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_delete_tblNewsAndAnnouncement_Retail]        
(        
@ID int )        
AS        
BEGIN

DELETE FROM Retail_tblNewsAndAnnouncement
WHERE ID = @ID          

END

GO

