USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetRoelUserId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetRoelUserId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nirav>
-- Create date: <10/4/2012>
-- Description:	<fetch User Role by E3Userid>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetRoelUserId]
	@E3UserId varchar(500)
AS
BEGIN
	select urole from tblusers where E3userid in (@E3UserId)
END

GO

