USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_BulkGetFileNameByLoanNo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_BulkGetFileNameByLoanNo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROCEDURE [dbo].[sp_BulkGetFileNameByLoanNo] 
 (
	@LoanNo VARCHAR(20)
 )
AS
  BEGIN
      SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
      -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
      SELECT [FileName] FROM tblBulkSplitLoanFiles with(NOLOCK)
      WHERE LoanNumber = @LoanNo
  END

GO

