USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_InsertclsForm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_InsertclsForm]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_InsertclsForm]      
(      
@Description varchar(200),      
@FileName varchar(200),      
@Isnews bit,      
@CateID int,  
@dttime datetime    
)      
AS      
BEGIN      
      
Insert into tblCFIForms(Description,FileName,dttime,isnews,CateID)      
           Values(@Description,@FileName,getdate(),@Isnews,@CateID)      
end
GO

