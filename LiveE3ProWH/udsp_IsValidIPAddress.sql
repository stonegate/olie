USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_IsValidIPAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_IsValidIPAddress]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mukesh Kumar
-- Create date: 09/27/2012
-- Description:	To Check BlackListed IPAddress
-- =============================================
CREATE PROCEDURE [dbo].[udsp_IsValidIPAddress] 
	-- Add the parameters for the stored procedure here
	@ipAddress varchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select Count(ipaddress) From tblLoghistory Where  ipaddress = @ipAddress
END

GO

