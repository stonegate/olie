USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetStateData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetStateData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetStateData] 

AS
BEGIN
	select * from tblStateNylex where Statename!='Massachusetts' and Statename!='New York' order by StateName
END


GO

