USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDOOErrorLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDOOErrorLog]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Kanva>    
-- Create date: <5/10/2013>    
-- Description: <This will fetch error log with XML request & Response>   
-- Modified By: Mitesh R Rajyaguru 
-- Modified Date: <6/14/2013> - Add Request Field
-- =============================================    
CREATE PROCEDURE [dbo].[sp_GetDOOErrorLog]  
(    
 @PartnerCompID varchar(100)  
)     
AS    
BEGIN    

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
		SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,         
                   -- the entire transaction is terminated and rolled back.

    
IF( @PartnerCompID IS NULL 
     OR @PartnerCompID = '' ) 
  BEGIN 
      SELECT TOP 100 id, 
                     loannumber, 
                     errordesc, 
                     errortime, 
                     response, 
                     request 
      FROM   tbldooerrorlog 
      ORDER  BY errortime DESC 
  END 
ELSE 
  BEGIN 
      SELECT TOP 100 id, 
                     loannumber, 
                     errordesc, 
                     errortime, 
                     response, 
                     request 
      FROM   tbldooerrorlog 
      WHERE  partnercompid = @PartnerCompID 
      ORDER  BY errortime DESC 
  END 
  
END 



-- Create Procedure Coding ENDS Here

GO

