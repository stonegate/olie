USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpPartnerCompanyId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpPartnerCompanyId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nirav>
-- Create date: <10/4/2012>
-- Description:	<FetchPartner Comapany Id>
-- =============================================
CREATE PROCEDURE [dbo].[SpPartnerCompanyId] 
	@UserId int
AS
BEGIN
	Select PartnerCompanyid from tblusers where userid in(@UserId)
END

GO

