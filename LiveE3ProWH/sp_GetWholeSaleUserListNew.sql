USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetWholeSaleUserListNew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetWholeSaleUserListNew]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetWholeSaleUserListNew]          
(          
@sortField varchar(500),          
@sortOrder varchar(100),          
@whereClause varchar(1000),          
@PageNo int,           
@PageSize int,      
@ActionID int,      
@userId varchar(50),
@UserRole int = null  
)           
AS          
BEGIN 

IF @ActionID = 1         
 BEGIN 
 Declare @CRSM nvarchar(100),@CRS nvarchar(100),@NewRole nvarchar(100)
 set  @CRSM=41  
 set  @CRS=40          
  IF @sortField  = ''                                      
    set @sortField = 'tblUsers.userid'          
            
  SET @sortField = @sortField + ' ' + @sortOrder          
          
  set @whereClause =  REPLACE(@whereClause,'[uurole]','tblroles.roles')                                            
            
  Declare @sql nvarchar(4000)          
            
  set @sql = ''                                      
    set @sql = 'Select count(*) from ( '          
    set @sql =  @sql + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,isnull(IsKatalyst,0)IsKatalyst,tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole,         
  tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,         
  tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime,         
  tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,tblUsers.PartnerCompanyid,tblusers.AEID,        
  IsUserInNewProcess,userLoginid,tblUsers.LastUpdatedBy,tblUsers.LastUpdateAt,temp.ufname      
  FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID LEFT OUTER JOIN tblRoles ON tblUsers.urole = tblRoles.ID    
  left JOIN
  (
  SELECT userLoginid as ufname,userid from tblusers 
  ) Temp on temp.userid = tblusers.Lastupdatedby ' 
  
   IF @whereClause <> ''                                               
   BEGIN                                           
   set @sql = @sql  + ' Where ' + @whereClause                                     
   END       

   IF (@UserRole = 18 OR @UserRole = 41)
   BEGIN
   IF @UserRole = 18
   set @NewRole=@CRSM
   ELSE
   set @NewRole=@CRS
   
   IF @whereClause <> ''       
   set @sql = @sql + ' and urole= ' + @NewRole        
   ELSE IF @whereClause = ''      
   set @sql = @sql + ' Where urole= ' + @NewRole       
   END              
            
  set @sql = @sql + ' group by tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole,         
  tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,         
  tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime,         
  tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,tblUsers.PartnerCompanyid,tblusers.AEID,        
  IsUserInNewProcess,userLoginid,IsKatalyst,tblUsers.LastUpdatedBy,tblUsers.LastUpdateAt,ufname '                                       
   set @sql = @sql + ' ) as t2 '

  print @sql          
  exec sp_executesql @sql            
            
            
  set @sql = ''                                      
    set @sql = 'Select * from ( '           
  set @sql =  @sql + 'SELECT                   
    ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,tblUsers.userid,isnull(IsKatalyst,0)IsKatalyst, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole,         
  tblUsers.isadmin, Case When tblUsers.isactive = 1 Then ''Active'' Else ''Inactive'' End as status, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,         
  tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime,         
  tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,tblUsers.PartnerCompanyid,tblusers.AEID,        
  IsUserInNewProcess,userLoginid,LastUpdatedBy,tblUsers.LastUpdateAt,temp.ufname,
  case when (crs.associateuserid is not null or crsm.associateuserid is not null) then ''YES'' else ''NO'' end as Associated        
  FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID LEFT OUTER JOIN tblRoles ON tblUsers.urole = tblRoles.ID 
  left join tblCRSAssociation crs on tblUsers.aeid = crs.associateuserid
  left join tblCRSMAssociation crsm on tblUsers.aeid = crsm.associateuserid
  left JOIN
  (
  SELECT userLoginid as ufname,userid from tblusers 
  ) Temp on temp.userid = tblusers.Lastupdatedby '
  
   IF @whereClause <> ''                                               
   BEGIN                                           
   set @sql = @sql  + ' Where ' + @whereClause                                     
   END       

   IF (@UserRole = 18 OR @UserRole = 41)
   BEGIN
   IF @UserRole = 18
   set @NewRole=@CRSM
   ELSE
   set @NewRole=@CRS
   
   IF @whereClause <> ''       
   set @sql = @sql + ' and urole= ' + @NewRole        
   ELSE IF @whereClause = ''      
   set @sql = @sql + ' Where urole= ' + @NewRole       
   END   
                                                                                  
  set @sql = @sql + ' group by tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole,         
  tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,      
  tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime,         
  tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,tblUsers.PartnerCompanyid,tblusers.AEID,        
  IsUserInNewProcess,userLoginid,IsKatalyst,tblUsers.LastUpdatedBy,tblUsers.LastUpdateAt,temp.ufname,crs.associateuserid,crsm.associateuserid '                                       
  set @sql = @sql + ' ) as t2 ' 
                   
  set @sql = @sql + ' Where Row between (' + CAST( @PageNo AS NVARCHAR)  + ' - 1) * ' + Cast(@PageSize as nvarchar) + ' + 1 and ' + cast(@PageNo as nvarchar) + ' * ' + cast(@PageSize as nvarchar)
  
  IF (@UserRole = 18 OR @UserRole = 41)
  BEGIN
  set @sql = @sql  + ' ORDER BY LastUpdateAt DESC'
  END  
    
  print @sql                                    
  exec sp_executesql @sql          
 END       
IF @ActionID = 2      
 BEGIN      
  Declare @PartnerCompID nvarchar(255)      
     Select @PartnerCompID = PartnerCompanyid from tblusers where userid = @userId        
  IF @sortField  = ''                                      
    set @sortField = 'tblUsers.userid'          
            
  SET @sortField = @sortField + ' ' + @sortOrder          
          
  set @whereClause =  REPLACE(@whereClause,'[uurole]','tblroles.roles')                                            
            
  Declare @sqlnew nvarchar(4000)          
            
  set @sqlnew = ''                                      
    set @sqlnew = 'Select count(*) from ( '       
    set @sqlnew =  @sqlnew + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,tblUsers.userid,isnull(IsKatalyst,0)IsKatalyst, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole, tblUsers.isadmin,       
    tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent, tblUsers.upassword, tblUsers.maxloginattempt,       
    tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,      
    tblUsers.PartnerCompanyid,tblusers.AEID,IsUserInNewProcess,userLoginid        
    FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID       
    LEFT OUTER JOIN tblRoles ON tblUsers.urole = tblRoles.ID       
    where tblUsers.PartnerCompanyid = ''' + @PartnerCompID + ''' And userid not in('+ @userId +') '          
      
   IF @whereClause <> ''                                               
   BEGIN                                           
   set @sqlnew = @sqlnew  + ' And ' + @whereClause                                     
   END                              
            
   set @sqlnew = @sqlnew + ' group by tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole, tblUsers.isadmin,       
    tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent, tblUsers.upassword, tblUsers.maxloginattempt,       
    tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,      
    tblUsers.PartnerCompanyid,tblusers.AEID,IsUserInNewProcess,userLoginid,IsKatalyst '                                       
   set @sqlnew = @sqlnew + ' ) as t2 '      
   print @sqlnew          
   exec sp_executesql @sqlnew       
      
   set @sqlnew = ''      
   set @sqlnew = 'Select * from ( '           
   set @sqlnew =  @sqlnew + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,tblUsers.userid,isnull(IsKatalyst,0)IsKatalyst, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole, tblUsers.isadmin,       
    Case When tblUsers.isactive = 1 Then ''Active'' Else ''Inactive'' End as status, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent, tblUsers.upassword, tblUsers.maxloginattempt,       
    tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,      
    tblUsers.PartnerCompanyid,tblusers.AEID,IsUserInNewProcess,userLoginid       
    FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID       
    LEFT OUTER JOIN tblRoles ON tblUsers.urole = tblRoles.ID       
    where tblUsers.PartnerCompanyid = ''' + @PartnerCompID + ''' And userid not in('+ @userId +') '          
      
   IF @whereClause <> ''                                          
   BEGIN                                           
   set @sqlnew = @sqlnew  + ' And ' + @whereClause                                     
   END                              
            
   set @sqlnew = @sqlnew + ' group by tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole, tblUsers.isadmin,       
    tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent, tblUsers.upassword, tblUsers.maxloginattempt,       
    tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,      
    tblUsers.PartnerCompanyid,tblusers.AEID,IsUserInNewProcess,userLoginid,IsKatalyst '                                       
   set @sqlnew = @sqlnew + ' ) as t2 '      
   set @sqlnew = @sqlnew + ' Where Row between (' + CAST( @PageNo AS NVARCHAR)  + ' - 1) * ' + Cast(@PageSize as nvarchar) + ' + 1 and ' + cast(@PageNo as nvarchar) + ' * ' + cast(@PageSize as nvarchar)          
   print @sqlnew          
   exec sp_executesql @sqlnew  
     
 END       
         
END


GO

