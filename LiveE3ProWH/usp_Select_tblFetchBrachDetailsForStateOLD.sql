USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tblFetchBrachDetailsForStateOLD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tblFetchBrachDetailsForStateOLD]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[usp_Select_tblFetchBrachDetailsForStateOLD]
(
@strAbbr varchar(50)
)
as
begin

select * from tblusers where (StatebelongTo like '%' + @strAbbr + '%') 
and urole = 8 and isactive = 1

select * from tblusers where (StatebelongTo like '%' + @strAbbr + '%')  
and urole = 2 and isactive = 1

end


GO

