USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchDatatblformRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchDatatblformRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 
-- Description:	Procedure for clsForm BLL
-- =============================================
Create procEDURE [dbo].[sp_FetchDatatblformRoles]
(
@formid int
)
AS
BEGIN
	
	SET NOCOUNT ON;

Select uroleid from tblformRoles where formid =@formid
END



GO

