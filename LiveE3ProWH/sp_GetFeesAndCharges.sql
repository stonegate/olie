USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetFeesAndCharges]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetFeesAndCharges]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  <Kanva Patel>        
-- Create date: <23rd Aug 2012>        
-- Modified Date: <4th Dec 2012>       
-- Description: <This will get Fees & Charges Details for Docs On OLIE project step-3>    
-- 800,822,900 & 999 are HUD Line number series that we need to display on UI
-- As per BRD we need to display 800 to 822 HUD lines and 900 series on Fees & Charges    
-- =============================================        
CREATE PROCEDURE [dbo].[sp_GetFeesAndCharges]
    (
      @LoanNumber VARCHAR(10)
    )
AS 
          
    BEGIN        
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
		SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,         
                   -- the entire transaction is terminated and rolled back.
        SELECT  LoanApp.LoanNumber ,
                ClosingCost.HUDLinenum ,
                ClosingCost.Payee ,
                ClosingCost.BorrowerAmt ,
                ClosingCost.BorrowerPOC ,
                LoanApp.ChannelType ,
                ClosingCost.HUDDesc ,
                CAST(ClosingCost.HUDLinenum AS MONEY) AS lineorder
        FROM    mwlloanapp LoanApp ,
                mwlloandata LoanData ,
                mwlclosingcost ClosingCost
        WHERE   LoanApp.ID = LoanData.objOwner_ID
				And LoanData.Active = 1
                AND LoanData.ID = ClosingCost.loanDAta_ID
                AND LoanApp.LoanNumber = @LoanNumber
                AND CAST(ClosingCost.HUDLinenum AS MONEY) >= 800 -- HUD Line series start from 
                AND CAST(ClosingCost.HUDLinenum AS MONEY) <= 822 -- HUD Line series upto

--Earlier was commented as per given new mapping sheet by SMC
--Now uncommented as per meeting Dt: 4/29/2013 with Jeff, SMC, Brian and all 
        UNION
        SELECT  LoanApp.LoanNumber ,
                Prepaid.HUDLinenum ,
                Prepaid.Payee ,
                Prepaid.BorrowerAmt ,
                Prepaid.BorrowerPOC ,
                LoanApp.ChannelType ,
                Prepaid.HUDDesc ,
                CAST(Prepaid.HUDLinenum AS MONEY) AS lineorder
        FROM    mwlloanapp LoanApp ,
                mwlloandata LoanData ,
                mwlprepaid Prepaid
        WHERE   LoanApp.ID = LoanData.objOwner_ID
				And LoanData.Active = 1
                AND LoanData.ID = Prepaid.loanDAta_ID
                AND LoanApp.LoanNumber = @LoanNumber
                AND CAST(Prepaid.HUDLinenum AS MONEY) >= 900 -- HUD Line series start from  
                AND CAST(Prepaid.HUDLinenum AS MONEY) <= 999 -- HUD Line series upto 
        ORDER BY lineorder        
    END 

-- Create Procedure Coding ENDS Here

GO

