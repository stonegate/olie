USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RegLoan_GetBrokerInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RegLoan_GetBrokerInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Stonegate
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_RegLoan_GetBrokerInfo]
(
@CompanyName nvarchar(200)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	select * from mwcInstitution where Companyemail= @CompanyName and Status='ACTIVE'
END



GO

