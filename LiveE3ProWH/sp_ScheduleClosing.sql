USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ScheduleClosing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ScheduleClosing]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================    
-- Author:  Chandresh Patel    
-- Create date: 10/30/2012    
-- Description: Procedure for clsPipeline BLL    
-- =============================================    
CREATE PROCEDURE [dbo].sp_ScheduleClosing     
(@LoanNo varchar(20)=null,    
@Doc varchar(500)=null,    
@Comments varchar(2000)=null,     
@Userid int)    
AS    
BEGIN    
     
 SET NOCOUNT ON;    
    
 insert into tblScheduleClosing (LoanNo, SchDate, FeeSheet,userid,SubmitedDate, Comments,IsFromLoanSuspendedDoc) values 
(@LoanNo, '',@Doc,@UserID,getdate(),@Comments,1)   
END    
    
GO

