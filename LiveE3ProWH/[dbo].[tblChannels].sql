USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblChannels]') AND type in (N'U'))
DROP TABLE [dbo].[tblChannels]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblChannels](	  [ChannelId] INT NOT NULL	, [Name] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [Description] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Order] TINYINT NOT NULL	, [CreatedBy] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [CreatedOn] DATETIME NOT NULL	, [ModifiedBy] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ModifiedOn] DATETIME NULL	, [BusinessSource] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblChannels] PRIMARY KEY ([ChannelId] ASC))USE [HomeLendingExperts]
GO

