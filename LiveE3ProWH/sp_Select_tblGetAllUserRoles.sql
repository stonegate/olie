USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetAllUserRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetAllUserRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Select_tblGetAllUserRoles]      
as      
begin
--select * from tblroles    
SELECT
	*
FROM tblRoles
WHERE Id NOT IN (25, 26, 4)      
end

GO

