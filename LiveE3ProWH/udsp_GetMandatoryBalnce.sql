USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetMandatoryBalnce]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetMandatoryBalnce]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Example for execute stored procedure       
--EXECUTE [udsp_GetMandatoryBalnce] '478452'  
-- =============================================          
-- Author:  Sonu Gupta          
-- Create date:04/10/2013
-- Name:udsp_GetMandatoryBalnce.sql        
-- Description: Get Total Mandatory Limit Balance         
-- =============================================          
CREATE PROCEDURE [dbo].[udsp_GetMandatoryBalnce] 
 (
 --Parameter declaration           
 @PartnerCompanyID VARCHAR(40) 
 )         
AS          
BEGIN   
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,         
                   -- the entire transaction is terminated and rolled back.
                   
SELECT     TotalMandatoryLimit
FROM         tblManageMandatoryClientsInfo
WHERE     (PartnerCompanyID = @PartnerCompanyID)      

END     
GO

