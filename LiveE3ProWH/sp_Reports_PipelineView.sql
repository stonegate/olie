USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Reports_PipelineView]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Reports_PipelineView]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Reports_PipelineView]	
	@UserID INT
	,@UserRole INT
	,@CustFieldDef VARCHAR(100)
	,@OrignatorID VARCHAR(4000)
	,@CurrentStatus VARCHAR(4000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;-- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	DECLARE @InternalUsers VARCHAR(500)

	SELECT @InternalUsers = Roles
	FROM tblQuickLinks
	WHERE ID = 18 --Lock Confirmation

	IF (@UserRole = 0)
	BEGIN
		SELECT sc.SchDate
			,loanapp.Originator_id
			,'E3' AS DB
			,loanapp.casenum AS FHANumber
			,loanapp.ChannelType
			,loanapp.Channeltype AS BusType
			,loanapp.OriginatorName
			,loanapp.currentStatus AS LoanStatus
			,loanapp.DecisionStatus
			,loanapp.ID AS record_id
			,'' AS LT_USR_ASSGNDRAWER
			,loanapp.LoanNumber AS loan_no
			,CloserName AS Closer
			,borr.lastname AS BorrowerLastName
			,loanapp.CurrentStatus AS LoanStatus
			,'' AS LU_FINAL_HUD_STATUS
			,appStat.StatusDateTime AS SubmittedDate
			,approvalStat.StatusDateTime AS DateApproved
			,CASE 
				WHEN TransType IS NULL
					THEN ''
				WHEN TransTYpe = 'P'
					THEN 'Purchase'
				WHEN TransTYpe = 'R'
					THEN 'Refinance'
				END AS TransTYpe
			--,LookupsPro.DisplayString AS LoanProgram 
			,COALESCE(loanData.LoanProgDesc ,'') as LoanProgram			
			,loanData.AdjustedNoteAmt AS loan_amt
			,LockRecord.STATUS AS LockStatus
			,CustFNMA.YNValue
			,LockRecord.LockExpirationDate
			,LockRecord.LockDateTime
			,UnderwriterName AS UnderWriter
			,uEmail.Email AS Underwriter_Email
			,Lookups.DisplayString AS CashOut
			,CAST(borr.CompositeCreditScore AS CHAR(5)) AS CreditScoreUsed
			,convert(VARCHAR(35), EstCloseDate, 101) AS ScheduleDate
			,CASE 
				WHEN (
						SELECT count(a.ID) AS TotalConditions
						FROM dbo.mwlCondition a
						WHERE DueBy = 'Prior to Closing'
							AND Category != 'LENDER'
							AND Category != 'NOTICE'
							AND a.objOwner_ID IN (
								SELECT ID
								FROM dbo.mwlLoanApp AS loan1
								WHERE loan1.LoanNumber = LoanApp.LoanNumber
								)
						) = 0
					THEN 0
				WHEN (
						SELECT count(a.ID) AS TotalCleared
						FROM dbo.mwlCondition a
						WHERE (
								DueBy = 'Prior to Closing'
								AND Category != 'LENDER'
								AND Category != 'NOTICE'
								)
							AND (
								CurrentState = 'CLEARED'
								OR CurrentState = 'SUBMITTED'
								OR CurrentState = 'Waived'
								)
							AND a.objOwner_ID IN (
								SELECT id
								FROM dbo.mwlLoanApp AS loan2
								WHERE loan2.LoanNumber = LoanApp.Loannumber
								)
						) = 0
					THEN 0
				ELSE (
						SELECT count(a.ID) AS TotalCleared
						FROM dbo.mwlCondition a
						WHERE (
								DueBy = 'Prior to Closing'
								AND Category != 'LENDER'
								AND Category != 'NOTICE'
								)
							AND (
								CurrentState = 'CLEARED'
								OR CurrentState = 'SUBMITTED'
								OR CurrentState = 'Waived'
								)
							AND a.objOwner_ID IN (
								SELECT ID
								FROM dbo.mwlLoanApp AS loan2
								WHERE loan2.LoanNumber = LoanApp.LoanNumber
								)
						) * 100 / (
						SELECT count(a.ID) AS TotalConditions
						FROM dbo.mwlCondition a
						WHERE (
								DueBy = 'Prior to Closing'
								AND Category != 'LENDER'
								AND Category != 'NOTICE'
								)
							AND a.objOwner_ID IN (
								SELECT ID
								FROM dbo.mwlLoanApp AS loan1
								WHERE loan1.LoanNumber = LoanApp.LoanNumber
								)
						)
				END AS PER
			,CASE cust.stringvalue
				WHEN '--Select One--'
					THEN ''
				ELSE cust.stringvalue
				END AS UnderwriterType
			,'' AS BrokerName
			,LockRecord.DeliveryOption
			,CASE 
				WHEN (
						@UserRole IN (
							SELECT *
							FROM dbo.splitstring(@InternalUsers, ',')
							)
						)
					THEN CASE 
							WHEN (
									COALESCE(LockRecord.STATUS, '') = 'CANCELED'
									OR COALESCE(LockRecord.STATUS, '') = 'CONFIRMED'
									OR COALESCE(LockRecord.STATUS, '') = 'EXPIRED'
									)
								THEN 1
							ELSE 0
							END
				ELSE 0
				END viewdoc
		FROM mwlLoanApp loanapp
		INNER JOIN mwlBorrower AS borr ON borr.loanapp_id = loanapp.id
		INNER JOIN dbo.mwlloandata AS loanData ON loanData.ObjOwner_id = loanapp.id
		LEFT JOIN dbo.mwlUnderwritingSummary AS uSummary ON uSummary.LoanApp_id = loanapp.id
		--Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = 'BROKER' and Broker.objownerName='Broker'
		LEFT JOIN dbo.mwlLockRecord AS LockRecord ON LockRecord.LoanApp_ID = loanapp.id
			AND LockRecord.LockType = 'LOCK'
		LEFT JOIN mwlAppStatus appStat ON loanapp.ID = appStat.LoanApp_id
			AND appStat.StatusDesc = '01 - Registered'
		LEFT JOIN mwlApprovalStatus approvalStat ON loanapp.ID = approvalStat.LoanApp_id
			AND approvalStat.StatusDesc = 'Approved'
		LEFT JOIN mwlLookups Lookups ON LoanData.refipurpose = Lookups.BOCode
			AND ObjectName = 'mwlLoanData'
			AND fieldname = 'refipurpose'
		--Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = 'CORRESPOND' and Corres.objownerName='Correspondent'  
		LEFT JOIN dbo.mwamwuser AS uEmail ON uSummary.UnderWriter_id = uEmail.id
		--left join mwsCustFieldDef as mCust on  mCust.CustomFieldName like'Correspondent Type%'  
		LEFT JOIN mwlcustomfield AS Cust ON Cust.loanapp_id = loanapp.id
			AND Cust.CustFieldDef_ID = 'C4D2D8038F614DF8A1B8A6622C99FC3E'
		LEFT JOIN mwlLookups LookupsPro ON LoanData.FinancingType = LookupsPro.BOCode
			AND LookupsPro.ObjectName = 'mwlLoanData'
			AND LookupsPro.fieldname = 'FinancingType'
		LEFT JOIN mwlcustomfield AS CustFNMA ON CustFNMA.loanapp_id = loanapp.id
			AND CustFNMA.CustFieldDef_ID = @CustFieldDef
		LEFT JOIN tblScheduleClosing sc ON sc.loanno = loanapp.LoanNumber		
		WHERE borr.sequencenum = 1
			AND loanapp.LoanNumber IS NOT NULL
			AND loanapp.LoanNumber <> ''
			AND CurrentStatus IN (
				SELECT *
				FROM dbo.SplitString(@CurrentStatus, ',')
				)
		--and (Originator_id in (select * from dbo.SplitString(@OrignatorID,','))) 
		ORDER BY Per DESC, LockRecord.DateTimeAdded desc
	END

	---sales director role
	IF (
			@UserRole = 26
			OR @UserRole = 25
			)
	BEGIN
		SELECT loanapp.Originator_id
			,sc.SchDate
			,'E3' AS DB
			,loanapp.casenum AS FHANumber
			,loanapp.ChannelType
			,loanapp.Channeltype AS BusType
			,loanapp.OriginatorName
			,loanapp.currentStatus AS LoanStatus
			,loanapp.DecisionStatus
			,loanapp.ID AS record_id
			,'' AS LT_USR_ASSGNDRAWER
			,loanapp.LoanNumber AS loan_no
			,CloserName AS Closer
			,borr.lastname AS BorrowerLastName
			,loanapp.CurrentStatus AS LoanStatus
			,'' AS LU_FINAL_HUD_STATUS
			,appStat.StatusDateTime AS SubmittedDate
			,approvalStat.StatusDateTime AS DateApproved
			,CASE 
				WHEN TransType IS NULL
					THEN ''
				WHEN TransTYpe = 'P'
					THEN 'Purchase'
				WHEN TransTYpe = 'R'
					THEN 'Refinance'
				END AS TransTYpe
			--,LookupsPro.DisplayString AS LoanProgram
			,COALESCE(loanData.LoanProgDesc ,'') as LoanProgram
			,loanData.AdjustedNoteAmt AS loan_amt
			,LockRecord.STATUS AS LockStatus
			,CustFNMA.YNValue
			,LockRecord.LockExpirationDate
			,LockRecord.LockDateTime
			,UnderwriterName AS UnderWriter
			,uEmail.Email AS Underwriter_Email
			,CASE ISNULL(RTRIM(Broker.Company), '')
				WHEN ''
					THEN ISNULL(RTRIM(Corres.Company), '')
				ELSE ISNULL(RTRIM(Broker.Company), '')
				END AS BrokerName
			,Lookups.DisplayString AS CashOut
			,CAST(borr.CompositeCreditScore AS CHAR(5)) AS CreditScoreUsed
			,convert(VARCHAR(35), EstCloseDate, 101) AS ScheduleDate
			,CASE 
				WHEN (
						SELECT count(a.ID) AS TotalConditions
						FROM dbo.mwlCondition a
						WHERE DueBy = 'Prior to Closing'
							AND Category != 'LENDER'
							AND Category != 'NOTICE'
							AND a.objOwner_ID IN (
								SELECT ID
								FROM dbo.mwlLoanApp AS loan1
								WHERE loan1.LoanNumber = LoanApp.LoanNumber
								)
						) = 0
					THEN 0
				WHEN (
						SELECT count(a.ID) AS TotalCleared
						FROM dbo.mwlCondition a
						WHERE (
								DueBy = 'Prior to Closing'
								AND Category != 'LENDER'
								AND Category != 'NOTICE'
								)
							AND (
								CurrentState = 'CLEARED'
								OR CurrentState = 'SUBMITTED'
								OR CurrentState = 'Waived'
								)
							AND a.objOwner_ID IN (
								SELECT id
								FROM dbo.mwlLoanApp AS loan2
								WHERE loan2.LoanNumber = LoanApp.Loannumber
								)
						) = 0
					THEN 0
				ELSE (
						SELECT count(a.ID) AS TotalCleared
						FROM dbo.mwlCondition a
						WHERE (
								DueBy = 'Prior to Closing'
								AND Category != 'LENDER'
								AND Category != 'NOTICE'
								)
							AND (
								CurrentState = 'CLEARED'
								OR CurrentState = 'SUBMITTED'
								OR CurrentState = 'Waived'
								)
							AND a.objOwner_ID IN (
								SELECT ID
								FROM dbo.mwlLoanApp AS loan2
								WHERE loan2.LoanNumber = LoanApp.LoanNumber
								)
						) * 100 / (
						SELECT count(a.ID) AS TotalConditions
						FROM dbo.mwlCondition a
						WHERE (
								DueBy = 'Prior to Closing'
								AND Category != 'LENDER'
								AND Category != 'NOTICE'
								)
							AND a.objOwner_ID IN (
								SELECT ID
								FROM dbo.mwlLoanApp AS loan1
								WHERE loan1.LoanNumber = LoanApp.LoanNumber
								)
						)
				END AS PER
			,CASE cust.stringvalue
				WHEN '--Select One--'
					THEN ''
				ELSE cust.stringvalue
				END AS UnderwriterType
			,LockRecord.DeliveryOption
			,CASE 
				WHEN (
						@UserRole IN (
							SELECT *
							FROM dbo.splitstring(@InternalUsers, ',')
							)
						)
					THEN CASE 
							WHEN (
									COALESCE(LockRecord.STATUS, '') = 'CANCELED'
									OR COALESCE(LockRecord.STATUS, '') = 'CONFIRMED'
									OR COALESCE(LockRecord.STATUS, '') = 'EXPIRED'
									)
								THEN 1
							ELSE 0
							END
				ELSE 0
				END viewdoc
		FROM mwlLoanApp loanapp
		INNER JOIN mwlBorrower AS borr ON borr.loanapp_id = loanapp.id
		INNER JOIN dbo.mwlloandata AS loanData ON loanData.ObjOwner_id = loanapp.id
		LEFT JOIN dbo.mwlUnderwritingSummary AS uSummary ON uSummary.LoanApp_id = loanapp.id
		LEFT JOIN dbo.mwlInstitution AS Broker ON Broker.ObjOwner_id = loanapp.id
			AND Broker.InstitutionType = 'BROKER'
			AND Broker.objownerName = 'Broker'
		LEFT JOIN dbo.mwlLockRecord AS LockRecord ON LockRecord.LoanApp_ID = loanapp.id
			AND LockRecord.LockType = 'LOCK' --and  LockRecord.Status<>'CANCELED'  
		LEFT JOIN mwlAppStatus appStat ON loanapp.ID = appStat.LoanApp_id
			AND appStat.StatusDesc = '01 - Registered'
		LEFT JOIN mwlApprovalStatus approvalStat ON loanapp.ID = approvalStat.LoanApp_id
			AND approvalStat.StatusDesc = 'Approved'
		LEFT JOIN mwlLookups Lookups ON LoanData.refipurpose = Lookups.BOCode
			AND ObjectName = 'mwlLoanData'
			AND fieldname = 'refipurpose'
		LEFT JOIN dbo.mwlInstitution AS Corres ON Corres.ObjOwner_id = loanapp.id
			AND Corres.InstitutionType = 'CORRESPOND'
			AND Corres.objownerName = 'Correspondent'
		LEFT JOIN dbo.mwamwuser AS uEmail ON uSummary.UnderWriter_id = uEmail.id
		--left join mwsCustFieldDef as mCust on  mCust.CustomFieldName like'Correspondent Type%'  
		LEFT JOIN mwlcustomfield AS Cust ON Cust.loanapp_id = loanapp.id
			AND Cust.CustFieldDef_ID = 'C4D2D8038F614DF8A1B8A6622C99FC3E'
		LEFT JOIN mwlLookups LookupsPro ON LoanData.FinancingType = LookupsPro.BOCode
			AND LookupsPro.ObjectName = 'mwlLoanData'
			AND LookupsPro.fieldname = 'FinancingType'
		LEFT JOIN mwlcustomfield AS CustFNMA ON CustFNMA.loanapp_id = loanapp.id
			AND CustFNMA.CustFieldDef_ID = ''
		LEFT JOIN tblScheduleClosing sc ON sc.loanno = loanapp.LoanNumber		
		WHERE borr.sequencenum = 1
			AND loanapp.LoanNumber IS NOT NULL
			AND loanapp.LoanNumber <> ''
			AND CurrentStatus IN (
				SELECT *
				FROM dbo.SplitString(@CurrentStatus, ',')
				)
			AND (
				Originator_id IN (
					SELECT *
					FROM dbo.SplitString(@OrignatorID, ',')
					)
				)
		--and (Originator_id in (select * from dbo.SplitString(@OrignatorID,','))) 
		ORDER BY Per DESC, LockRecord.DateTimeAdded desc
	END
	ELSE
	BEGIN
		SELECT sc.SchDate
			,loanapp.Originator_id
			,'E3' AS DB
			,loanapp.casenum AS FHANumber
			,loanapp.ChannelType
			,loanapp.Channeltype AS BusType
			,loanapp.OriginatorName
			,loanapp.currentStatus AS LoanStatus
			,loanapp.DecisionStatus
			,loanapp.ID AS record_id
			,'' AS LT_USR_ASSGNDRAWER
			,loanapp.LoanNumber AS loan_no
			,CloserName AS Closer
			,borr.lastname AS BorrowerLastName
			,loanapp.CurrentStatus AS LoanStatus
			,'' AS LU_FINAL_HUD_STATUS
			,appStat.StatusDateTime AS SubmittedDate
			,approvalStat.StatusDateTime AS DateApproved
			,CASE 
				WHEN TransType IS NULL
					THEN ''
				WHEN TransTYpe = 'P'
					THEN 'Purchase'
				WHEN TransTYpe = 'R'
					THEN 'Refinance'
				END AS TransTYpe
			--,LookupsPro.DisplayString AS LoanProgram
			,COALESCE(loanData.LoanProgDesc ,'') as LoanProgram
			,loanData.AdjustedNoteAmt AS loan_amt
			,LockRecord.STATUS AS LockStatus
			,CustFNMA.YNValue
			,LockRecord.LockExpirationDate
			,LockRecord.LockDateTime
			,UnderwriterName AS UnderWriter
			,uEmail.Email AS Underwriter_Email
			,Lookups.DisplayString AS CashOut
			,CAST(borr.CompositeCreditScore AS CHAR(5)) AS CreditScoreUsed
			,convert(VARCHAR(35), EstCloseDate, 101) AS ScheduleDate
			,CASE 
				WHEN (
						SELECT count(a.ID) AS TotalConditions
						FROM dbo.mwlCondition a
						WHERE DueBy = 'Prior to Closing'
							AND Category != 'LENDER'
							AND Category != 'NOTICE'
							AND a.objOwner_ID IN (
								SELECT ID
								FROM dbo.mwlLoanApp AS loan1
								WHERE loan1.LoanNumber = LoanApp.LoanNumber
								)
						) = 0
					THEN 0
				WHEN (
						SELECT count(a.ID) AS TotalCleared
						FROM dbo.mwlCondition a
						WHERE (
								DueBy = 'Prior to Closing'
								AND Category != 'LENDER'
								AND Category != 'NOTICE'
								)
							AND (
								CurrentState = 'CLEARED'
								OR CurrentState = 'SUBMITTED'
								OR CurrentState = 'Waived'
								)
							AND a.objOwner_ID IN (
								SELECT id
								FROM dbo.mwlLoanApp AS loan2
								WHERE loan2.LoanNumber = LoanApp.Loannumber
								)
						) = 0
					THEN 0
				ELSE (
						SELECT count(a.ID) AS TotalCleared
						FROM dbo.mwlCondition a
						WHERE (
								DueBy = 'Prior to Closing'
								AND Category != 'LENDER'
								AND Category != 'NOTICE'
								)
							AND (
								CurrentState = 'CLEARED'
								OR CurrentState = 'SUBMITTED'
								OR CurrentState = 'Waived'
								)
							AND a.objOwner_ID IN (
								SELECT ID
								FROM dbo.mwlLoanApp AS loan2
								WHERE loan2.LoanNumber = LoanApp.LoanNumber
								)
						) * 100 / (
						SELECT count(a.ID) AS TotalConditions
						FROM dbo.mwlCondition a
						WHERE (
								DueBy = 'Prior to Closing'
								AND Category != 'LENDER'
								AND Category != 'NOTICE'
								)
							AND a.objOwner_ID IN (
								SELECT ID
								FROM dbo.mwlLoanApp AS loan1
								WHERE loan1.LoanNumber = LoanApp.LoanNumber
								)
						)
				END AS PER
			,CASE cust.stringvalue
				WHEN '--Select One--'
					THEN ''
				ELSE cust.stringvalue
				END AS UnderwriterType
			,LockRecord.DeliveryOption
			,0 AS viewdoc
		FROM mwlLoanApp loanapp
		INNER JOIN mwlBorrower AS borr ON borr.loanapp_id = loanapp.id
		INNER JOIN dbo.mwlloandata AS loanData ON loanData.ObjOwner_id = loanapp.id
		LEFT JOIN dbo.mwlUnderwritingSummary AS uSummary ON uSummary.LoanApp_id = loanapp.id
		--Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = 'BROKER' and Broker.objownerName='Broker'
		LEFT JOIN dbo.mwlLockRecord AS LockRecord ON LockRecord.LoanApp_ID = loanapp.id
			AND LockRecord.LockType = 'LOCK'
			--AND LockRecord.STATUS <> 'CANCELED'
		LEFT JOIN mwlAppStatus appStat ON loanapp.ID = appStat.LoanApp_id
			AND appStat.StatusDesc = '01 - Registered'
		LEFT JOIN mwlApprovalStatus approvalStat ON loanapp.ID = approvalStat.LoanApp_id
			AND approvalStat.StatusDesc = 'Approved'
		LEFT JOIN mwlLookups Lookups ON LoanData.refipurpose = Lookups.BOCode
			AND ObjectName = 'mwlLoanData'
			AND fieldname = 'refipurpose'
		LEFT JOIN dbo.mwamwuser AS uEmail ON uSummary.UnderWriter_id = uEmail.id
		LEFT JOIN mwlcustomfield AS Cust ON Cust.loanapp_id = loanapp.id
			AND Cust.CustFieldDef_ID = 'C4D2D8038F614DF8A1B8A6622C99FC3E'
		LEFT JOIN mwlLookups LookupsPro ON LoanData.FinancingType = LookupsPro.BOCode
			AND LookupsPro.ObjectName = 'mwlLoanData'
			AND LookupsPro.fieldname = 'FinancingType'
		LEFT JOIN mwlcustomfield AS CustFNMA ON CustFNMA.loanapp_id = loanapp.id
			AND CustFNMA.CustFieldDef_ID = @CustFieldDef
		LEFT JOIN tblScheduleClosing sc ON sc.loanno = loanapp.LoanNumber		
		WHERE borr.sequencenum = 1
			AND loanapp.LoanNumber IS NOT NULL
			AND loanapp.LoanNumber <> ''
			AND CurrentStatus IN (
				SELECT *
				FROM dbo.SplitString(@CurrentStatus, ',')
				)
			AND (
				Originator_id IN (
					SELECT *
					FROM dbo.SplitString(@OrignatorID, ',')
					)
				)
		ORDER BY Per DESC, LockRecord.DateTimeAdded desc
	END
END

GO

