USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getAssociatedUsers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getAssociatedUsers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_getAssociatedUsers] @crsmIds NVARCHAR(max)
	,@userid NVARCHAR(max)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sql NVARCHAR(max)

	SET @sql = ''
	SET @sql = @sql + 'SELECT distinct u.userid

		,urole

		,CASE 

			WHEN isactive = 1

				THEN ufirstname + '' '' + ulastname

			ELSE ufirstname + '' '' + ulastname + ''- in active''

			END AS UserName

	FROM tblusers u'

	IF @userid = 2
	BEGIN
		--print @sql
		SET @sql = @sql + ' 
			inner join tblusermanagers us on u.userid = us.userid
			inner join tblcrsmassociation cs on cs.associateuserid = us.managerid 
			where cs.crsmuserid in (select * from dbo.SplitString(''' + @crsmIds + ''','',''))
			and u.urole =  
			' + @userid
	END
	ELSE IF @userid = 26
	BEGIN
		SET @sql = @sql + ' 
			inner join tblcrsmassociation cs on u.userid = cs.AssociateUserId
			where cs.crsmuserid in (select * from dbo.SplitString(''' + @crsmIds + ''','',''))
			and u.urole =  
			' + @userid
	END

	SET @sql = @sql + ' order by userName asc'

	PRINT @sql

	EXEC (@sql)
END

GO

