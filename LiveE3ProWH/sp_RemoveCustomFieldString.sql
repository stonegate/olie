USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RemoveCustomFieldString]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RemoveCustomFieldString]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nitin>
-- Create date: <>
-- Description:	<Procedure for clsOrderFHAE3>
-- =============================================
CREATE PROCEDURE [dbo].[sp_RemoveCustomFieldString]
(
@LoanApp_id varchar(32)

)
AS
BEGIN
	
	SET NOCOUNT ON;
DELETE FROM mwlcustomfield WHERE LoanApp_id=@LoanApp_id


END


GO

