USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBorrowerLastName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBorrowerLastName]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  sp_GetBorrowerLastName
	-- Add the parameters for the stored procedure here

@loanNo varchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

select cb.borr_last from loandat LD inner join cobo Cb on ld.record_id = cb.parent_id  where ld.loan_no = @loanNo

END

GO

