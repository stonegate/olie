USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetBorrowerLastNameWithUwMail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetBorrowerLastNameWithUwMail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  

CREATE PROCEDURE [dbo].[SpGetBorrowerLastNameWithUwMail]     
(      
 @LoanNumber varchar(max)    
)      
AS      
BEGIN      
      SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

SELECT rtrim(borr.LastName) as borr_last,uEmail.Email as Uemail,CurrentStatus,isnull(DecisionStatus,'')DecisionStatus   FROM mwlBorrower as borr 
INNER JOIN mwlloanapp  loanapp ON loanapp.ID = borr.loanapp_id 
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id 
left join dbo.mwamwuser as uEmail on uSummary.UnderWriter_id = uEmail.id 
 where  borr.sequencenum=1 and LoanNumber = @LoanNumber
    
END  

GO

