USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpLastLoginUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpLastLoginUpdate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SpLastLoginUpdate] 
	@UserId int,
	@lastlogintime datetime =null,
	@maxloginattempt int
AS
BEGIN
	update tblusers set lastlogintime = @lastlogintime,maxloginattempt =@maxloginattempt where userid =@UserId
END

GO

