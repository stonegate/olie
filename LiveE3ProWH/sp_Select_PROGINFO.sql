USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_PROGINFO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_PROGINFO]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		 Nitin
-- Create date: 10/04/2012
-- Description:	Procedure for clsloanRegister.cs bll  
-- =============================================
CREATE Proc [dbo].[sp_Select_PROGINFO]
as
begin

select '0' as record_id, '--Select Program--' as prog_code ,'' as prog_desc union select record_id,prog_code,prog_desc from PROGINFO
end

GO

