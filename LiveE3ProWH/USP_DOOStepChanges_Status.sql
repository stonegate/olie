USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_DOOStepChanges_Status]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_DOOStepChanges_Status]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_DOOStepChanges_Status]
(
@Flag varchar(10),
@LoanNumber varchar(30),
@CreatedBy varchar(50) =null,
@StepNumber bigint=null
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 


IF @Flag = 'IU'
BEGIN
if Exists (Select 1 from tblDOOStepChangeStatus where LoanNumber =@LoanNumber)
BEGIN
Update tblDOOStepChangeStatus
	SET	 Step1 = CASE WHEN @StepNumber = 1 THEN 1 ELSE Step1 END ,
		 Step2 = CASE WHEN @StepNumber = 2 THEN 1 ELSE Step2 END ,
		 Step3 = CASE WHEN @StepNumber = 3 THEN 1 ELSE Step3 END ,
		 Step4 = CASE WHEN @StepNumber = 4 THEN 1 ELSE Step4 END ,
		 Step5 = CASE WHEN @StepNumber = 5 THEN 1 ELSE Step5 END ,
		 Step6 = CASE WHEN @StepNumber = 6 THEN 1 ELSE Step6 END ,
		 Step7 = CASE WHEN @StepNumber = 7 THEN 1 ELSE Step7 END ,
		 ModifiedBy = @CreatedBy,
		 ModifiedDate = getdate()
		 WHERE LoanNumber = @LoanNumber and [Status] = 'Y'
END
ELSE
BEGIN

Declare @Propertytype VARCHAR(100) 
Set @Propertytype = (Select Top 1 propertytype from  mwlloanapp l INNER JOIN mwlsubjectproperty S ON S.loanapp_id = l.id  where l.LoanNumber = @LoanNumber)
INSERT INTO tblDOOStepChangeStatus (LoanNumber,Step1,Step2,Step3,Step4,Step5,Step6,Step7,[Status],CreatedDate,CreatedBy) VALUES
(
@LoanNumber,
CASE WHEN @StepNumber = 1 THEN 1 ELSE 1 END,
CASE WHEN @StepNumber = 2 THEN 1 ELSE CASE WHEN UPPER(ltrim(rtrim(@Propertytype))) ='CONDO' or upper(ltrim(rtrim(@Propertytype))) ='PUD' THEN 0 ELSE 1 END END,
CASE WHEN @StepNumber = 3 THEN 1 ELSE 0 END,
CASE WHEN @StepNumber = 4 THEN 1 ELSE 0 END,
CASE WHEN @StepNumber = 5 THEN 1 ELSE 0 END,
CASE WHEN @StepNumber = 6 THEN 1 ELSE 0 END,
CASE WHEN @StepNumber = 7 THEN 1 ELSE 1 END,
'Y',getdate(),@CreatedBy
)
END
END

ELSE if @Flag ='select'
BEGIN
IF  EXISTS(Select 1 from tblDOOStepChangeStatus where LoanNumber =@LoanNumber and [Status] = 'Y')
BEGIN


Select * from tblDOOStepChangeStatus where LoanNumber = @LoanNumber
and (Step1+Step2+Step3+Step4+Step5+Step6+Step7)  <> 7 and [Status] = 'Y'

END
ELSE
BEGIN

Select 'N' [Status]

END 

END

Else if @Flag = 'check'
BEGIN

if not exists (select 1 from tblDooMandatoryFieldRecords where isnull(MandatoryFieldValue,'') = '' and LoanNumber = @LoanNumber)
BEGIN
delete from  tblDooMandatoryFieldRecords where LoanNumber = @LoanNumber
END 
Else
Select distinct  StepNumber from tblDooMandatoryFieldRecords  where isnull(MandatoryFieldValue,'') = '' and LoanNumber = @LoanNumber

END 

END





GO

