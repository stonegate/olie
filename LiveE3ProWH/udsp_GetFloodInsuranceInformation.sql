USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetFloodInsuranceInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetFloodInsuranceInformation]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Abhishek Rastogi>
-- Create date: <09/13/2012>
-- Description:	< This will return the value for "Docs On OLIE Step-5 Flood Insurance Information" on the basis of LoanNumber>
-- =============================================
CREATE PROCEDURE [dbo].[udsp_GetFloodInsuranceInformation] 
	-- Add the parameters for the stored procedure here
	@LoanNumber varchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)

SELECT mwlinstitution.company,mwlinstitution.street,mwlinstitution.street2,mwlinstitution.City,mwlinstitution.State,
mwlinstitution.zipcode,mwlinstitution.country,Hzdphonenumber.phonenumber,HzdFaxnumber.phonenumber AS FaxNumber,
mwlinstitution.companyemail,mwlflood.floodpolicynumber,AnnualAmount.annualamt,AnnualDueDate.AnnualPayDueDate,
custfield.CurrencyValue From mwlLoanApp 
INNER JOIN dbo.mwlLoanData loandata ON dbo.mwlLoanApp.ID = loandata.ObjOwner_ID
Left join mwlSubjectProperty on mwlSubjectProperty.LoanApp_ID = mwlLoanApp.ID
LEFT JOIN mwlflood ON dbo.mwlFlood.Owner_ID = dbo.mwlSubjectProperty.ID
LEFT JOIN dbo.mwlHazard ON dbo.mwlHazard.Owner_ID = dbo.mwlSubjectProperty.ID
LEFT JOIN mwlinstitution on mwlinstitution.objOwner_id= mwlHazard.id AND objownername = 'HazardInstitution'
LEFT JOIN mwlPhoneNumber Hzdphonenumber ON mwlinstitution.id=Hzdphonenumber.ObjOwner_ID AND Hzdphonenumber.phonetype ='Business'
LEFT JOIN mwlPhoneNumber HzdFaxnumber ON mwlinstitution.id=HzdFaxnumber.ObjOwner_ID AND HzdFaxnumber.phonetype ='Fax'


LEFT JOIN mwlPrepaid AnnualAmount ON AnnualAmount.LoanData_ID=loandata.ID AND AnnualAmount.HUDDesc ='Flood insurance premium'
LEFT JOIN mwlPrepaid AnnualDueDate ON AnnualDueDate.LoanData_ID=loandata.ID AND AnnualDueDate.HUDDesc ='Flood insurance premium'
--Live
LEFT JOIN mwlCustomField custfield ON custfield.LoanApp_ID= dbo.mwlLoanApp.ID AND CustFieldDef_ID = '4C47AA1F90FE43E0B5DBCF90BE1398D9'
--Test
--LEFT JOIN mwlCustomField custfield ON custfield.LoanApp_ID= dbo.mwlLoanApp.ID AND CustFieldDef_ID = '4CC5A8CBD88544DF828EFEA106028352'

Where LoanNumber=@LoanNumber


END



GO

