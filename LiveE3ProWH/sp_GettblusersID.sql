USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GettblusersID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GettblusersID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/07/2012
-- Description:	Procedure for clspipelineE3 BLL
-- =============================================
CREATE procEDURE [dbo].[sp_GettblusersID]
(
@userid int
)
AS
BEGIN

	SET NOCOUNT ON;
Select uidprolender from tblusers where userid=@userid
END


GO

