USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_BulkCheckforDuplicateLoan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_BulkCheckforDuplicateLoan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_BulkCheckforDuplicateLoan] (
	@FileID VARCHAR(200) = NULL
	,@BorrowerLastName VARCHAR(100) = NULL
	,@PropertyStreetAddress VARCHAR(200) = NULL
	,@PropertyCity VARCHAR(20) = NULL
	,@PropertyState VARCHAR(10) = NULL
	,@PropertyZip VARCHAR(10) = NULL
	,@OriginatorLOSLoanNumber VARCHAR(10) = NULL
	,@TransactionType VARCHAR(10) = NULL
	,@SSNumber VARCHAR(10) = NULL
	,@ELPMLoanCustomFieldKey UNIQUEIDENTIFIER = NULL
	)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;-- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads.    

	DECLARE @matchecount INT
	DECLARE @matchString VARCHAR(300)
	DECLARE @tFileID VARCHAR(200) = dbo.TRIM(@FileID)
	DECLARE @tBorrowerLastName VARCHAR(100) = dbo.TRIM(@BorrowerLastName)
	DECLARE @tPropertyStreetAddress VARCHAR(200) = dbo.TRIM(@PropertyStreetAddress)
	DECLARE @tPropertyCity VARCHAR(20) = dbo.TRIM(@PropertyCity)
	DECLARE @tPropertyState VARCHAR(10) = dbo.TRIM(@PropertyState)
	DECLARE @tPropertyZip VARCHAR(10) = dbo.TRIM(@PropertyZip)
	DECLARE @tOriginatorLOSLoanNumber VARCHAR(10) = dbo.TRIM(@OriginatorLOSLoanNumber)
	DECLARE @tTransactionType VARCHAR(10) = dbo.TRIM(@TransactionType)
	DECLARE @tSSNumber VARCHAR(10) = dbo.TRIM(@SSNumber)

	IF (@BorrowerLastName IS NULL)
		AND (@PropertyStreetAddress IS NULL)
		AND (@PropertyCity IS NULL)
		AND (@PropertyState IS NULL)
		AND (@PropertyZip IS NULL)
		AND (@OriginatorLOSLoanNumber IS NULL)
		AND (@TransactionType IS NULL)
		AND (@SSNumber IS NULL)
	BEGIN
		SELECT 'Success' AS RegistrationStatus
	END
	ELSE
	BEGIN
		SET @matchecount = 0
	END

	SELECT TOP 1 @matchecount = matchecount
		,@matchString = matchstring
	FROM (
		SELECT LoanNumber
			,CASE 
				WHEN (borr.[lastname] = @tBorrowerLastName)
					THEN 1
				ELSE 0
				END + CASE 
				WHEN PropertyAddress.[Street] LIKE @tPropertyStreetAddress + '%'
					AND @PropertyStreetAddress <> ''
					AND PropertyAddress.[City] = @tPropertyCity
					AND PropertyAddress.[State] = @tPropertyState
					AND PropertyAddress.[ZipCode] = @tPropertyZip
					THEN 1
				ELSE 0
				END +
				-- CASE 
				--WHEN  @tOriginatorLOSLoanNumber in (SELECT LCF.StringValue FROM LoanCustomField LCF
				--	INNER JOIN LoanFieldDefinition LFD ON LFD.LoanFieldDefinitionId = LCF.LoanFieldDefinitionId
				--	WHERE LFD.FieldKey = @ELPMLoanCustomFieldKey AND OwnerId = loanapp.[LoanNumber]) 
				--	THEN 1
				--ELSE 0
				--END + 
				CASE 
				WHEN borr.TaxIDType = 'SSN'
					AND borr.TaxIDNum = @tSSNumber
					THEN 1
				ELSE 0
				END + CASE 
				WHEN TransType = @tTransactionType
					THEN 1
				ELSE 0
				END AS matchecount
			,CASE 
				WHEN (borr.[lastname] = @tBorrowerLastName)
					THEN 'Last Name, '
				ELSE ''
				END + 
				CASE 
				WHEN PropertyAddress.[Street] LIKE @tPropertyStreetAddress + '%'
					AND @PropertyStreetAddress <> ''
					AND PropertyAddress.[City] = @tPropertyCity
					AND PropertyAddress.[State] = @tPropertyState
					AND PropertyAddress.[ZipCode] = @tPropertyZip
					THEN 'Address, '
				ELSE ''
				END + 
				--CASE 
				--WHEN @tOriginatorLOSLoanNumber in (SELECT LCF.StringValue FROM LoanCustomField LCF
				--	INNER JOIN LoanFieldDefinition LFD ON LFD.LoanFieldDefinitionId = LCF.LoanFieldDefinitionId
				--	WHERE LFD.FieldKey = @ELPMLoanCustomFieldKey AND OwnerId = loanapp.[LoanNumber]) 
				--	THEN 'LOS LoanNumber, '
				--ELSE ''
				--END + 
				CASE 
				WHEN borr.TaxIDType = 'SSN'
					AND borr.TaxIDNum = @tSSNumber
					THEN 'SSN, '
				ELSE ''
				END + CASE 
				WHEN TransType = @tTransactionType
					THEN 'TransType'
				ELSE ''
				END AS matchstring
		FROM mwlLoanApp loanapp WITH (NOLOCK)
		INNER JOIN mwlSubjectProperty AS PropertyAddress WITH (NOLOCK) ON PropertyAddress.LoanApp_id = loanapp.id
		INNER JOIN mwlBorrower AS borr WITH (NOLOCK) ON borr.loanapp_id = loanapp.id
			AND borr.sequencenum = 1
		INNER JOIN mwlloandata AS loanData WITH (NOLOCK) ON loanData.ObjOwner_id = loanapp.id
		WHERE LoanNumber IS NOT NULL
			AND LoanNumber <> ''
			AND loanapp.[currentStatus] IN (
				'01 - Registered'
				,'02 - Assumption In Process'
				,'03 - Appt Set to Review Discls'
				,'04 - Disclosures Sent'
				,'05 - Application Taken'
				,'06 - Disclosures Out'
				,'07 - Disclosures Received'
				,'09 - Application Recieved'
				,'10 - Application on Hold'
				,'13 - File Intake'
				,'14 - Prop Inspection Waived'
				,'15 - Appraisal Ordered'
				,'17 - Submission On Hold'
				,'18 - New Construction On Hold'
				,'19 - Disclosures'
				,'20 - Disclosures On Hold'
				,'21 - In Processing'
				,'22 - Appraisal Received'
				,'25 - Submitted to Underwriting'
				,'29 - In Underwriting'
				,'30.3 - Re-disclosure Received'
				,'30.2 - Re-disclosure Out'
				,'31 - Counteroffer'
				,'33 - Cleared to Close'
				,'34 - DOO Closing Approved'
				,'37 - Pre-Closing'
				,'41 - In Closing'
				,'43 - Closing on Hold'
				,'45 - Docs Out'
				,'46 - Assumption Docs Out'
				,'47 - Assumption Complete'
				,'50 - Closing Package Received'
				,'51 - Purchase Sub On Hold'
				,'52 - Closing Package Reviewed'
				,'53 - Closed Package Reviewed'
				,'54 - Purchase Pending'
				,'55 - Condition Review'
				,'56 - Purch Conditions Received'
				,'57 - Sent to Funding'
				,'58 - Cleared for Funding'
				,'60 - Closed'
				,'62 - Funded'
				)
			AND (
				borr.[lastname] = @tBorrowerLastName
				OR (
					PropertyAddress.[Street] LIKE @tPropertyStreetAddress + '%'
					AND @PropertyStreetAddress <> ''
					AND PropertyAddress.[City] = @tPropertyCity
					AND PropertyAddress.[State] = @tPropertyState
					AND PropertyAddress.[ZipCode] = @tPropertyZip
					)
				OR loanapp.[LoanNumber] = @tOriginatorLOSLoanNumber
				OR (
					borr.TaxIDNum = @tSSNumber
					AND borr.TaxIDType = 'SSN'
					)
				OR TransType = @tTransactionType
				)
		) T
	ORDER BY T.matchecount DESC

	--if  @tOriginatorLOSLoanNumber in (SELECT LCF.StringValue FROM LoanCustomField LCF
	--				INNER JOIN LoanFieldDefinition LFD ON LFD.LoanFieldDefinitionId = LCF.LoanFieldDefinitionId
	--				inner join mwlloanapp loanapp on loanapp.loannumber = lcf.ownerid
	--				WHERE LFD.FieldKey = @ELPMLoanCustomFieldKey )
	--				begin
	--				set @matchString = @matchString+ ', LOS LoanNumber '
	--				set @matchecount = @matchecount +1
	--				end

	IF (
			@matchecount = 0
			OR @matchecount = 1
			OR @matchecount = 2
			)
	BEGIN
		SELECT 'Success' AS RegistrationStatus
			,@matchString AS Matches
	END

	IF (
			@matchecount = 3
			OR @matchecount = 4
			)
	BEGIN
		SELECT 'Suspend' AS RegistrationStatus
			,@matchString AS Matches
		UPDATE [dbo].[tblBulkSplitLoanFiles] SET DuplicateMatches = @matchString
		WHERE LoanFileID = @FileID
	END

	IF (@matchecount = 5)
	BEGIN
		SELECT 'Blocked' AS RegistrationStatus
			,@matchString AS Matches
		UPDATE [dbo].[tblBulkSplitLoanFiles] SET DuplicateMatches = @matchString
		WHERE LoanFileID = @FileID
	END

	--select @matchecount
	
END

GO

