USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_DooAuditLog]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_DooAuditLog]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DooAuditLog](	  [ID] BIGINT NOT NULL IDENTITY(1,1)	, [LoanNumber] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [ErrorTime] DATETIME NULL DEFAULT(getdate())	, [ErrorMessage] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [XMLMessage] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EndUserMessage] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PartnerCompanyID] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [UserId] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ExpiryDate] DATETIME NULL	, CONSTRAINT [PK__tbl_DooAuditLog__43CBF196] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO

