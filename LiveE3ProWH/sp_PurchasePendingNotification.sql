USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PurchasePendingNotification]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PurchasePendingNotification]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                
-- Author:  Stonegate             
-- Create date:                 
-- Description: This will give Email Address of Broker,BDM,Proceesor.              
--[sp_PurchasePendingNotification]('2D82DE52ED944519B3CB31242ECCFCBE')              
--exec [sp_PurchasePendingNotification] '2D82DE52ED944519B3CB31242ECCFCBE'              
-- =============================================                
CREATE PROCEDURE [dbo].[sp_PurchasePendingNotification]              
(                     
@CustField54MemoType nvarchar(MAX)            
)              
AS              
BEGIN     

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads.      

SELECT Loannumber, ISNULL(muser.Email, '') AS BDMEmail, borr.lastname AS BLastName, Broker.Email AS BrokerEmail, ISNULL(Cust.MemoValue, '')AS ProcessorEmail FROM dbo.mwlloanapp AS LoanApp LEFT OUTER JOIN dbo.mwamwuser AS muser ON LoanApp.Originator_id = muser.id LEFT OUTER JOIN dbo.mwlBorrower AS borr ON borr.loanapp_id = loanapp.id AND borr.sequencenum = 1 INNER JOIN dbo.mwlCustomField AS Cust ON LoanApp.ID = Cust.LoanApp_id AND Cust.CustFieldDef_ID = @CustField54MemoType LEFT OUTER JOIN dbo.mwlInstitution AS Broker ON Broker.ObjOwner_id = loanapp.id AND Broker.InstitutionType = 'BROKER' AND Broker.objownerName = 'Broker' WHERE(CurrentStatus = '54 - Purchase Pending') AND (borr.sequencenum = 1) AND (Loannumber IS NOT NULL) AND (Loannumber <> '') 
AND (DATEDIFF(day,LoanApp.CurrPipeStatusDate, GETDATE())=0) AND (Cust.MemoValue IS NOT NULL)
END  

GO

