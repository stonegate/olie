USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TblLockhistory]') AND type in (N'U'))
DROP TABLE [dbo].[TblLockhistory]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblLockhistory](	  [ID] BIGINT NOT NULL IDENTITY(1,1)	, [LoanNumber] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Userid] BIGINT NULL	, [BrokerEmailid] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedDate] DATETIME NULL	, [ModifyDate] DATETIME NULL	, [SendEmail] BIT NULL DEFAULT((0))	, [LockDateInE3] DATETIME NULL	, [UpdateCounter] INT NULL DEFAULT((0))	, CONSTRAINT [PK_TblLockhistory] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO

