USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_GetLoanDocument]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_GetLoanDocument]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_Select_GetLoanDocument]
(
@loanRegId bigint
)
AS
BEGIN
select rtrim(Filename)Filename,borrLast from tblloanreg where loanregid in (@loanRegId)

select rtrim(DocName)DocName from tblloandoc where uloanregid in (@loanRegId)
END


GO

