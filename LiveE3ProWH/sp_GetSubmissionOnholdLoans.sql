USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetSubmissionOnholdLoans]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetSubmissionOnholdLoans]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Vipul Thacker
-- Create date: 01 Sep 2011
-- Modified date: 
-- Description:	To all loans which are on submission on hold 
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetSubmissionOnholdLoans] 
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

--List of loans submission on hold and send notification after seven(7) days.
--Here format is MM/DD/YYY
select ChannelType, Loannumber,LoanApp.ID, currentstatus,AppStatus.StatusDateTime, dateadd(dd,7,AppStatus.StatusDateTime) as NotificationDate, 
Originator_id,muser.Email as BDMEmail,borr.lastname as BLastName,Broker.Email as BrokerEmail
from [INTERLINQE3].dbo.mwlloanapp  LoanApp
inner join [INTERLINQE3].dbo.mwlappstatus AppStatus
on LoanApp.ID=AppStatus.LoanApp_id and StatusDesc like '%17 - Submission On Hold%'
Left join [INTERLINQE3].dbo.mwamwuser muser on LoanApp.Originator_id=muser.id
Left join [INTERLINQE3].dbo.mwlBorrower as borr on borr.loanapp_id = loanapp.id  and  borr.sequencenum=1
Left join [INTERLINQE3].dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id
and Broker.InstitutionType = 'BROKER' and Broker.objownerName='Broker'
where CurrentStatus like '%17 - Submission On Hold%'
and dateadd(dd,7,convert(varchar(10), AppStatus.StatusDateTime,101))= convert(varchar(10), getdate(),101)
 order by AppStatus.StatusDateTime desc

END





GO

