USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InserttblLoanReg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InserttblLoanReg]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create procEDURE [dbo].[sp_InserttblLoanReg] 
 -- Add the parameters for the stored procedure here  
(
@borrLast varchar(MAX),
@transType VARCHAR(MAX),
@loanAmt varchar(MAX),
@loanPgm varchar(MAX),
@userid varchar(MAX),
@filetype varchar(MAX),
@filename varchar(MAX)
)
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
Insert into tblLoanReg(borrLast,transType,loanAmt,loanPgm,userid,filetype,filename) 
               Values(@borrLast,@transType,@loanAmt,@loanPgm,@userid,@filetype,@filename) 
END

GO

