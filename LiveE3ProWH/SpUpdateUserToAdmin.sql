USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpUpdateUserToAdmin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpUpdateUserToAdmin]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nirav>
-- Create date: <10/4/2012>
-- Description:	<Fetch All region Data>
-- =============================================
CREATE PROCEDURE [dbo].[SpUpdateUserToAdmin]
	@UserId int,
	@Val varchar(100)
AS
BEGIN
	update tblusers Set IsUserinNewProcess = 1, IsAdmin =@Val Where userid =@UserId
END

GO

