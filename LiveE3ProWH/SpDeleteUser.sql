USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpDeleteUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpDeleteUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpDeleteUser]
	@UserId int
AS
BEGIN
	update tblusers Set isactive = 0 Where userid = @UserId
END

GO

