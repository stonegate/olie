USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DocsOnOLIE_GetEscrowMatrix]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DocsOnOLIE_GetEscrowMatrix]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================================================  
-- Author:  <Kanva>  
-- Create date: <10th Dec 2012>  
-- Description: <This will get data to fill in Matrix of Escrow step as per DOO version 1.0>  
-- ================================================================================================  
CREATE PROCEDURE [dbo].[sp_DocsOnOLIE_GetEscrowMatrix]
    (
      @LoanNumber VARCHAR(10)
    )
AS 
    BEGIN  
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
		SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,         
                   -- the entire transaction is terminated and rolled back.
        SELECT  CAST(Month AS INT) AS sequencenum ,
                CASE WHEN citytaxesmonthlyamt IS NULL THEN 0.00
                     ELSE citytaxesmonthlyamt
                END AS citytaxesmonthlyamt ,
                CASE WHEN countytaxesmonthlyamt IS NULL THEN 0.00
                     ELSE countytaxesmonthlyamt
                END AS countytaxesmonthlyamt ,
                CASE WHEN assessmentsmonthlyamt IS NULL THEN 0.00
                     ELSE assessmentsmonthlyamt
                END AS assessmentsmonthlyamt ,
                CASE WHEN hazardinsmonthlyamt IS NULL THEN 0.00
                     ELSE hazardinsmonthlyamt
                END AS hazardinsmonthlyamt ,
                CASE WHEN floodinsmonthlyamt IS NULL THEN 0.00
                     ELSE floodinsmonthlyamt
                END AS floodinsmonthlyamt ,
                CASE WHEN otherppe1monthlyamt IS NULL THEN 0.00
                     ELSE otherppe1monthlyamt
                END AS otherppe1monthlyamt ,
                CASE WHEN otherppe2monthlyamt IS NULL THEN 0.00
                     ELSE otherppe2monthlyamt
                END AS otherppe2monthlyamt ,
                CASE WHEN otherppe3monthlyamt IS NULL THEN 0.00
                     ELSE otherppe3monthlyamt
                END AS otherppe3monthlyamt ,
                CASE WHEN otherppe4monthlyamt IS NULL THEN 0.00
                     ELSE otherppe4monthlyamt
                END AS otherppe4monthlyamt ,
                CASE WHEN otherppe5monthlyamt IS NULL THEN 0.00
                     ELSE otherppe5monthlyamt
                END AS otherppe5monthlyamt ,
                Aggr.ID
        FROM    mwlLoanApp LoanApp ,
                mwlLoanData LoanData ,
                mwlPPE PPE ,
                mwlaggregateescrow Aggr 
        WHERE   LoanApp.ID = LoanData.objOwner_ID
				AND LoanData.Active = 1
                AND LoanData.ID = PPE.LoanData_ID
                AND PPE.ID = Aggr.PPE_ID
                AND LoanApp.LoanNumber = @LoanNumber
        ORDER BY sequencenum  
   
    END

    SET ANSI_NULLS ON
    SET QUOTED_IDENTIFIER ON


-- Create Procedure Coding ENDS Here

GO

