USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Inserttblusers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Inserttblusers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Nitin  
-- Create date: 10/08/2012  
-- Description: Procedure for clspipelineE3 BLL  
-- =============================================  
CREATE procEDURE [dbo].[sp_Inserttblusers]  
(@ufirstname varchar(50)=null,  
@ulastname varchar(50)=null,  
@uemailid varchar(200)=null,  
@urole int,  
@isadmin bit,  
@isactive bit,
@uidprolender varchar(200)=null,  
@uparent int,  
@upassword varchar(200)=null,  
@mobile_ph nvarchar(50)=null,  
@loginidprolender nvarchar(300)=null,  
@carrierid int,  
@experience nchar(100)=null,  
@phonenum nvarchar(50)=null,  
@address varchar(300)=null,  
@address2 varchar(100)=null,  
@state nvarchar(50)=null,  
@zip nvarchar(10)=null,  
@fax nvarchar(50)=null,  
@photoname nvarchar(50)=null,  
@brokerid int=null,  
@bio ntext=null,  
@areaofexpertise ntext=null,  
@ClientTestimonial ntext=null,  
@city nvarchar(100)=null,  
@MI varchar(10)=null,  
@CreatedBy int,  
@IsUserInNewProcess bit)  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
  
    Insert into tblusers  
(ufirstname,ulastname,uemailid,urole,isadmin,  
isactive,uidprolender,uparent,upassword,mobile_ph,  
loginidprolender,carrierid,experience,phonenum,address,  
address2,state,zip,fax,photoname,  
brokerid,bio,areaofexpertise,ClientTestimonial,city,  
MI,CreatedBy,IsUserInNewProcess)   
Values(@ufirstname,@ulastname,@uemailid,@urole,@isadmin,  
@isactive,@uidprolender,@uparent,@upassword,@mobile_ph,  
@loginidprolender,@carrierid,@experience,@phonenum,@address,  
@address2,@state,@zip,@fax,@photoname,  
@brokerid,@bio,@areaofexpertise,@ClientTestimonial,@city,  
@MI,@CreatedBy,@IsUserInNewProcess)  
END  
  
GO

