USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_GetFallOutReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_GetFallOutReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_GetFallOutReport]
@Year varchar(500) ,
@oRole int,
@ChannelType varchar(100)='',
@strTeamOpuids nvarchar(max)='',
@BrokerName varchar(2000)='',
@strID nvarchar(max) = ''
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

Declare @SQL nvarchar(max)
Set @SQL = ''
     Set @SQL = ' SELECT ' + @Year + '  AS ARYear,
 month(LockRecord.LockDateTime) AS LockMonth, convert(decimal(12,0),round(sum(isnull(loanData.AdjustedNoteAmt,0)),0)) AS TotalLockedVolume,
 convert(decimal(12,0),round(sum(isnull(Closed.AdjustedNoteAmt, isnull(Closed.AdjustedNoteAmt,0))),0)) AS ClosedVolume,convert(decimal(12,0),round(sum(isnull(NotClosed.AdjustedNoteAmt, isnull(NotClosed.AdjustedNoteAmt,0))),0)) AS NotClosedVolume,
 convert(decimal(12,0),round(SUM(isnull(ActiveStatusLoandat.AdjustedNoteAmt, isnull(ActiveStatusLoandat.AdjustedNoteAmt,0))),0)) ActiveStatusVolume, 
 case when ISNULL(SUM(isnull(Closed.AdjustedNoteAmt, Closed.AdjustedNoteAmt)),0) + ISNULL(SUM(isnull(NotClosed.AdjustedNoteAmt, NotClosed.AdjustedNoteAmt)),0) > 0 THEN convert(decimal(12,0),ROUND((ISNULL(sum(isnull(Closed.AdjustedNoteAmt, Closed.AdjustedNoteAmt)),0) * 100)/ (ISNULL(sum(isnull(Closed.AdjustedNoteAmt, Closed.AdjustedNoteAmt)),0) + ISNULL(sum(isnull(NotClosed.AdjustedNoteAmt, NotClosed.AdjustedNoteAmt)),0)),0)) ELSE 0 END AS ''% Closed'' 
 FROM mwlLoanApp  AS MainTable 
 Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = MainTable.id  and loanData.Active=1
 CROSS APPLY(select top  1 LockDateTime from mwlLockRecord as L1 where L1.LoanApp_ID = MainTable.id  and  L1.LockType=''LOCK''  order by L1.LockDateTime asc )LockRecord 
 CROSS APPLY(select top  1 convert (varchar(10),LockExpirationDate,101)LockExpirationDate  from mwlLockRecord as L2 where L2.LoanApp_ID = MainTable.id  and  L2.LockType=''LOCK''  order by L2.LockExpirationDate desc )LockExpRecord 
 LEFT JOIN  mwlloandata Closed ON Closed.ObjOwner_id = MainTable.ID 
 and Closed.Active=1  and (MainTable.CurrentStatus IN (''62 - Funded'',''66 - In Shipping'',''70 - Loan Shipped'',''74 - Investor Suspense'',''78 - Loan Sold'',''82 - Final Docs Complete'',''64 - Out for Correction'',''68 - Allocated''))
LEFT JOIN  mwlloandata NotClosed ON NotClosed.ObjOwner_id = MainTable.ID  '

if (@oRole = 20 or @oRole = 21)
begin
Set @SQL =@SQL + ' AND NotClosed.Active=1 and (MainTable.CurrentStatus IN (''97 - Loan Denied'', ''96 - Loan Rescinded'',''95 - Denied for Purchase'',''99 - Loan Withdrawn'',''98 - Loan Canceled'') OR (LockExpRecord.LockExpirationDate < Getdate() AND MainTable.CurrentStatus IN (''01 - Registered'',''05 - Application Taken'',''09 - Application Received'',''13 - File Intake'',''17 - Submission On Hold'',''19 - Disclosures'',''20 - Disclosures On Hold'',''21 - In Processing'',''25 - Submitted to Underwriting'',''29 - In Underwriting'',''33 - Cleared to Close'',''37 - Pre-Closing'',''41 - In Closing'',''43 - Closing on Hold'',''45 - Docs Out'',''50 - Closing Package Received'',''50 - Closing Package Recieved'',''51 - Purchase Sub On Hold'',''54 - Purchase Pending'',''58 - Cleared for Funding'',''52 - Closing Package Reviewed'' ,''53 - Credit Package Reviewed'' ,''56 - Purch Conditions Received'',''60 - Closed'', ''53 - Credit Package Reviewed'' ,''RET - Credit Pulled'' ,''03 - Appt Set to Review Disclosures'' ,''04 - Disclosures Sent'', ''07 - Disclosures Received'',''10 - Application on Hold'',''15 - Appraisal Ordered'',''22 - Appraisal Received'',''18 - New Construction On Hold'',''90 - Funding Reversed'',''14 - Prop Inspection Waived'',''RET - Credit Pulled w/ App'',''RET - Registered as Prospect'',''07 - Disclosures Received'',''03 - Appt Set to Review Discls''))) 
	  LEFT JOIN  mwlloandata ActiveStatusLoandat ON ActiveStatusLoandat.ObjOwner_id = MainTable.ID AND ActiveStatusLoandat.Active=1 AND LockExpRecord.LockExpirationDate >= getdate() AND MainTable.CurrentStatus  IN ('''',''01 - Registered'',''05 - Application Taken'',''09 - Application Recieved'',''13 - File Intake'',''17 - Submission On Hold'',''19 - Disclosures'',''20 - Disclosures On Hold'',''21 - In Processing'',''25 - Submitted to Underwriting'',''29 - In Underwriting'',''33 - Cleared to Close'',''37 - Pre-Closing'',''41 - In Closing'',''43 - Closing on Hold'',''45 - Docs Out'',''50 - Closing Package Received'',''51 - Purchase Sub On Hold'',''50 - Closing Package Recieved'',''54 - Purchase Pending'',''58 - Cleared for Funding'',''52 - Closing Package Reviewed'' ,''53 - Credit Package Reviewed'' ,''RET - Credit Pulled'' ,''03 - Appt Set to Review Disclosures'' ,''04 - Disclosures Sent'', ''07 - Disclosures Received'',''10 - Application on Hold'',''15 - Appraisal Ordered'',''22 - Appraisal Received'',''18 - New Construction On Hold'',''56 - Purch Conditions Received'',''14 - Prop Inspection Waived'',''RET - Credit Pulled w/ App'',''RET - Registered as Prospect'',''07 - Disclosures Received'',''03 - Appt Set to Review Discls'')'
end
else
begin
Set @SQL =@SQL + ' AND NotClosed.Active=1 and (MainTable.CurrentStatus IN (''96 - Loan Rescinded'',''95 - Denied for Purchase'',''97 - Loan Denied'', ''99 - Loan Withdrawn'',''98 - Loan Canceled'') OR LockExpRecord.LockExpirationDate < Getdate() AND MainTable.CurrentStatus IN (''01 - Registered'',''05 - Application Taken'',''09 - Application Received'',''13 - File Intake'',''17 - Submission On Hold'',''19 - Disclosures'',''20 - Disclosures On Hold'',''21 - In Processing'',''25 - Submitted to Underwriting'',''29 - In Underwriting'',''33 - Cleared to Close'',''37 - Pre-Closing'',''41 - In Closing'',''43 - Closing on Hold'',''45 - Docs Out'',''50 - Closing Package Received'',''54 - Purchase Pending'',''58 - Cleared for Funding'',''52 - Closing Package Reviewed'',''53 - Credit Package Reviewed'',''56 - Purch Conditions Received'',''60 - Closed'',''RET - Credit Pulled'',''03 - Appt Set to Review Disclosures'',''04 - Disclosures Sent'',''07 - Disclosures Received'',''10 - Application on Hold'',''15 - Appraisal Ordered'',''22 - Appraisal Received'',''18 - New Construction On Hold'',''90 - Funding Reversed'',''14 - Prop Inspection Waived'',''RET - Credit Pulled w/ App'',''RET - Registered as Prospect'',''51 - Purchase Sub On Hold'',''07 - Disclosures Received''))  
	  LEFT JOIN  mwlloandata ActiveStatusLoandat ON ActiveStatusLoandat.ObjOwner_id = MainTable.ID AND ActiveStatusLoandat.Active=1 AND LockExpRecord.LockExpirationDate >=  getdate() AND MainTable.CurrentStatus IN ('''',''01 - Registered'',''05 - Application Taken'',''09 - Application Recieved'',''13 - File Intake'',''17 - Submission On Hold'',''19 - Disclosures'',''20 - Disclosures On Hold'',''21 - In Processing'',''25 - Submitted to Underwriting'',''29 - In Underwriting'',''33 - Cleared to Close'',''37 - Pre-Closing'',''41 - In Closing'',''43 - Closing on Hold'',''45 - Docs Out'',''50 - Closing Package Received'',''54 - Purchase Pending'',''58 - Cleared for Funding'',''52 - Closing Package Reviewed'',''53 - Credit Package Reviewed'',''RET - Credit Pulled'',''03 - Appt Set to Review Disclosures'',''04 - Disclosures Sent'', ''07 - Disclosures Received'',''10 - Application on Hold'',''15 - Appraisal Ordered'',''22 - Appraisal Received'',''18 - New Construction On Hold'',''56 - Purch Conditions Received'',''RET - Credit Pulled w/ App'',''RET - Registered as Prospect'',''51 - Purchase Sub On Hold'',''07 - Disclosures Received'',''03 - Appt Set to Review Discls'',''95 - Denied for Purchase'') '
end       
Set @SQL =@SQL +' Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = MainTable.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
 Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = MainTable.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' 
 WHERE YEAR(LockRecord.LockDateTime) = '''+  @Year +''''

if  (@ChannelType = 'broker' )
begin
Set @SQL =@SQL + ' and MainTable.ChannelType=''BROKER'''
end
if  (@ChannelType = 'retail' )
begin
Set @SQL =@SQL + ' and MainTable.ChannelType=''RETAIL'''
end
if  (@ChannelType = 'correspond' )
begin
Set @SQL =@SQL + ' and MainTable.ChannelType=''correspond'''
end
if (@oRole=1)
begin
	Set @SQL =@SQL + ' and MainTable.ChannelType<>''RETAIL'''
end
if (@strTeamOpuids != '')
begin
	Set @SQL =@SQL + ' and Originator_id in (select * from dbo.SplitString('''+@strTeamOpuids+''','',''))'
	if (@oRole=12)
	begin
		Set @SQL =@SQL + ' select * from dbo.SplitString('''+@strTeamOpuids+''','','')'
	end
end
if(@BrokerName!='')
begin
		DECLARE @TEMPSTR NVARCHAR(1000)
		SET @TEMPSTR = @BrokerName

		IF(CHARINDEX('--',@TEMPSTR) = 0)
		BEGIN
		Set @SQL =@SQL + '  and (Broker.Company=''' + @BrokerName + ''' or Corres.Company=''' + @BrokerName + ''')'
		END
end
else
	begin
		if (@oRole != 0 and @strTeamOpuids = '')
		begin
			if(len(@strID)  = 0)
			begin
				if(@oRole = 3 or @oRole = 20 or @oRole = 21)
				begin
					Set @SQL =@SQL + ' and (MainTable.ID in (select * from dbo.SplitString('''+@strID+''','','')))'
				end
				else
				begin
					Set @SQL =@SQL + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','','')))'
				end
				
			end
			else
			begin
				if(@oRole = 3 or @oRole = 20 or @oRole = 21)
				begin
					Set @SQL =@SQL + ' and (MainTable.ID in (select * from dbo.SplitString('''+@strID+''','','')))'
				end
				else if (@oRole = 12)
				begin
					Set @SQL =@SQL + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','','')))'
				end
				else
					begin
					Set @SQL =@SQL + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','','')))'
					end
			end
		end
	end
if (@oRole = 12)
begin
	if(len(@strID)  = 0)
	begin
		Set @SQL =@SQL + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','','')))'
	end
	else
	begin
		Set @SQL =@SQL + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','','')))'
	end
end
Set @SQL =@SQL + ' and MainTable.CurrentStatus IN (''56 - Purch Conditions Received'',''60 - Closed'',''RET - Credit Pulled'',''22 - Appraisal Received'',''90 - Funding Reversed'' ,''01 - Registered'',''05 - Application Taken'',''09 - Application Received'',''13 - File Intake'',''17 - Submission On Hold'',''19 - Disclosures'',''20 - Disclosures On Hold'',''21 - In Processing'',''25 - Submitted to Underwriting'',''29 - In Underwriting'',''33 - Cleared to Close'',''37 - Pre-Closing'',''41 - In Closing'',''43 - Closing on Hold'',''45 - Docs Out'',''50 - Closing Package Received'',''54 - Purchase Pending'',''58 - Cleared for Funding'',''52 - Closing Package Reviewed'' ,''53 - Credit Package Reviewed'' ,''RET - Credit Pulled'' ,''03 - Appt Set to Review Disclosures'' ,''04 - Disclosures Sent'',''07 - Disclosures Received'',''10 - Application on Hold'',''15 - Appraisal Ordered'',''22 - Appraisal Received'',''18 - New Construction On Hold'',''96 - Loan Rescinded'',''95 - Denied for Purchase'',''97 - Loan Denied'',''99 - Loan Withdrawn'',''98 - Loan Canceled'',''62 - Funded'',''66 - In Shipping'', ''70 - Loan Shipped'',''74 - Investor Suspense'',''78 - Loan Sold'',''82 - Final Docs Complete'',''64 - Out for Correction'',''RET - Credit Pulled w/ App'',''RET - Registered as Prospect'',''51 - Purchase Sub On Hold'')'
Set @SQL =@SQL + ' group by month(LockRecord.LockDateTime) '
Set @SQL =@SQL + ' order by month(LockRecord.LockDateTime)'
Print @SQL 
exec sp_executesql @SQL
END


GO

