USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblEmail]') AND type in (N'U'))
DROP TABLE [dbo].[tblEmail]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmail](	  [ID] INT NOT NULL IDENTITY(1,1)	, [Role] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Subject] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EmailBodyText] TEXT(16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedBy] INT NULL	, [CreatedDate] DATETIME NULL	, [ModifiedBy] INT NULL	, [ModifiedDate] DATETIME NULL	, [IsSent] BIT NULL	, [SentDateTime] DATETIME NULL)USE [HomeLendingExperts]
GO

