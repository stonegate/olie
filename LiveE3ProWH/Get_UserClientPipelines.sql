USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_UserClientPipelines]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Get_UserClientPipelines]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Get_UserClientPipelines]
	@AEIds NVARCHAR(MAX),
	@Channels NVARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT DISTINCT
		U.CompanyName As ClientName
	FROM [dbo].[tblUsers] U
	WHERE U.AEID IN (SELECT * FROM dbo.SplitString(@AEIds, ','))
		AND (@Channels IS NULL OR (U.BrokerType IN (SELECT * FROM dbo.SplitString(@Channels, ','))))
		AND U.IsActive = 1

END

GO

