USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetClientCompanyName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetClientCompanyName]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Example for execute stored procedure       
--EXECUTE [udsp_GetClientCompanyName] '478452'          
-- =============================================        
-- Author: Ashwani        
-- Create date: 04/03/2013 
-- Name:udsp_GetClientCompanyName.sql      
-- Description: Retrives Company Details       
-- =============================================        
CREATE PROCEDURE [dbo].[udsp_GetClientCompanyName] 
(
 --Parameter declaration         
 @PCompanyId VARCHAR(40)  
 )      
 AS        
 BEGIN       
 SET NOCOUNT ON;  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
 SET XACT_ABORT ON;  
   
SELECT     CompanyName AS ClientCompanyName, PartnerCompanyID
FROM         tblUsers
WHERE     (PartnerCompanyID = @PCompanyId)  
END   

GO

