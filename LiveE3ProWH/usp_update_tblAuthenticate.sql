USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_update_tblAuthenticate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_update_tblAuthenticate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[usp_update_tblAuthenticate]
(
@tempid varchar(Max),
@refuserid varchar(Max)
)
as
begin
update tblTempUser set IsActive = 0 
where tempid = @tempid

update tblusers set IsActive = 0 where userid = @refuserid

end




GO

