USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetUserIdByPartnerComapanyID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetUserIdByPartnerComapanyID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nirav>
-- Create date: <10/4/2012>
-- Description:	<Fetch UserRole By UserId>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetUserIdByPartnerComapanyID]
	@PartnerCompanyId varchar(500)
AS
BEGIN
	SELECT userid from tblusers where  PartnerCompanyid=@PartnerCompanyId and (IsUserinNewProcess <> '1' or IsUserinNewProcess is null)  order by  tblUsers.userid desc
END

GO

