USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetIsUserInNewProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetIsUserInNewProcess]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetIsUserInNewProcess] 
	@UserRole varchar(500)
AS
BEGIN
	select isnull(IsUserInNewProcess,0) as IsUserInNewProcess from tblusers where urole in (  @UserRole  )
END

GO

