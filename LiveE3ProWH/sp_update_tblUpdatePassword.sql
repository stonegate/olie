USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_update_tblUpdatePassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_update_tblUpdatePassword]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_update_tblUpdatePassword]
(
@sUserid varchar(Max),
@userPswd varchar(Max)
)
as
begin
UPDATE tblusers SET upassword=@userPswd where userid=@sUserid
end


GO

