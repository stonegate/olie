USE [HomeLendingExperts]
								GO
								SET ANSI_NULLS ON
								GO
								SET QUOTED_IDENTIFIER ON
								GO
-- =============================================
-- Author:		Vipul Thakkar
-- Create date: 04 OCT 10
-- Description:	To get the Broker name
-- =============================================
CREATE FUNCTION [dbo].[GetLockRecordFromE3] 
(
	@LoanApp_ID varchar(50),
	@Year varchar(50)
)
RETURNS varchar(50)
AS
BEGIN
DECLARE @LockInfo VARCHAR(1000) 

--SELECT @BrokerName = COALESCE(@BrokerName,'') + ufirstname + ', '  
--FROM 
--(
if @Year= '2012'
set @LockInfo = (select top 1 ID from mwlLockRecord as L1 
 where   L1.LoanApp_ID =@LoanApp_ID and 
 L1.LockType='LOCK' order by L1.LockDateTime asc )
--) as temp
--SELECT @LockInfo AS BrokerNames 
return @LockInfo
--16896E5D4CFF4D56AFE9D5AA2A2BC980
END

 GO

