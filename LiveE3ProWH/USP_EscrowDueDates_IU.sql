USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_EscrowDueDates_IU]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_EscrowDueDates_IU]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[USP_EscrowDueDates_IU]
(
	@LoanNumber AS varchar(15),
	@DueDatesXML AS Text,		
	@UserName AS varchar(30)
)
AS 
BEGIN 


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
	DELETE from tblEscrowDueDates where LoanNumber = @LoanNumber

	DECLARE @DocHandle int		
	EXEC sp_xml_preparedocument @DocHandle OUTPUT, @DueDatesXML

	INSERt INTO tblEscrowDueDates 
	SELECT @LoanNumber, Item, DueDate, [Order], @UserName, GETDATE(), NULL, NULL FROM OPENXML (@DocHandle, '/B/A',2)
	WITH (Item varchar(20),	DueDate datetime, [Order] int)

	EXEC sp_xml_removedocument @DocHandle

END




GO

