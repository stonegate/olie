USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetRegionalManager]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetRegionalManager]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetRegionalManager] 
	@StateId varchar(500)
AS
BEGIN
	select userid,ufirstname, ulastname, uemailid, urole, phonenum, photoname,mobile_ph from tblusers where branchid in (@StateId) AND urole=8
END

GO

