USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_UpUWSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_UpUWSummary]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



  
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE Sp_UpUWSummary  
 @LoanAppId varchar(500) ='',  
 @UnderWriterId varchar(500)='',  
 @UnderWriterName varchar(500)=''  
AS  
BEGIN  
   
if  NOT EXISTS(SELECT LoanApp_ID FROM mwlUnderwritingSummary WHERE LoanApp_ID=@LoanAppId)  
begin  
INSERT INTO mwlUnderwritingSummary (ID,LoanApp_ID,Underwriter_ID,UnderwriterName)  
VALUES (replace(newid(),'-',''),@LoanAppId,@UnderWriterId,@UnderWriterName)  
end  
else  
begin  
 UPDATE mwlUnderwritingSummary SET Underwriter_ID=@UnderWriterId,UnderwriterName=@UnderWriterName WHERE LoanApp_ID=@LoanAppId  
end  
  
END  
GO

