USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetRegionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetRegionData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetRegionData] 

AS
BEGIN
select Region, uLastName + ', ' + uFirstName AS FullName, uidprolender,e3userId,urole from tblusers where urole in(2,8,19) and IsActive=1
END







GO

