USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_tblRegLoanHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_tblRegLoanHistory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
  
-- =============================================  
-- Author:  <CHANDRESH PATEL>  
-- Create date: <10/31/2012>  
-- Description: <XML FILE INSERT>  
-- =============================================  
create PROCEDURE [dbo].[sp_insert_tblRegLoanHistory]  
(  
@XMLFile XML,  
@Userid int  
)  
AS  
BEGIN       
       
  Insert into tblRegLoanHistory(userid,xmlfilename,createddate)   
Values(@Userid,@XMLFile,getdate())      
        
END   
  
  
  
  
GO

