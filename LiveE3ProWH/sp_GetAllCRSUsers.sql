USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAllCRSUsers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAllCRSUsers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_GetAllCRSUsers] 
@CRSUserid int = null
AS
BEGIN
	
	SET NOCOUNT ON;

	--Loans
	SELECT CASE  WHEN isActive = 1 THEN (uFirstName + ' ' + uLastName) ELSE (uFirstName + ' ' + uLastName + ' - InActive') END as username, userid,users.urole from  
	tblusers users  where users.urole=40 and users.userid <> @CRSUserid
	 ORDER BY users.urole,username

	--Originators
	SELECT 
	CASE WHEN isActive = 1 THEN (uFirstName + ' ' + uLastName) ELSE (uFirstName + ' ' + uLastName + ' - InActive') END as username  ,tempCRSuser.CRSId, tempCRSuser.AuserId, tempCRSuser.RoleId  FROM 
	(
 
 	SELECT  CRSUserId as CRSId, AssociateUserId as AuserId ,AssociateRoleId as RoleId from tblCRSAssociation CRS WHERE AssociateUserId IS NOT NULL --where CRS.CRSUserId in (select distinct(Userid) from [dbo].[tblUserManagers] where ManagerId = 164)
	) as tempCRSuser
   INNER JOIN tblusers users on users.userid = tempCRSuser.AuserId 

      SELECT '' FROM [dbo].[tblUserManagers] 

	--Get PartnerCompnay names
   Select Distinct(partnercompanyid) , Companyname into #tempcompanynames from tblusers 

  
   SELECT  CRSUserId as CRSId, AssociateUserId as AuserId ,AssociateRoleId as RoleId , ISNULL(CRS.partnercompanyId,'') as partnercompanyId, ISNULL(users.CompanyName,'') as CompanyName from tblCRSAssociation CRS
   INNER JOIN #tempcompanynames users on users.partnercompanyId = CRS.partnercompanyId  WHERE CRS.partnercompanyId IS NOT NULL

END

GO

