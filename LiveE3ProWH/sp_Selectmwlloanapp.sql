USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Selectmwlloanapp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Selectmwlloanapp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[sp_Selectmwlloanapp]   
 -- Add the parameters for the stored procedure here  
(
@LoanNumber varchar(MAX)
)
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
  
    -- Insert statements for procedure here  
select * from mwlloanapp 
where CurrentStatus  IN ('01 - Registered','1 - Registered','3 - Application Received','7 - Submitted to Underwriting','8 - In Underwriting')
and LoanNumber =@LoanNumber
END


GO

