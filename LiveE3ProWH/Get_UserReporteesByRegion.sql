USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_UserReporteesByRegion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Get_UserReporteesByRegion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author, Somnath M. R.>
-- Create date: <Create Date, 27th Dec 2013>
-- Description:	<Description, Select from tblUserManagers table by Region>
-- EXEC Get_UserReporteesByRegion 2, '1'
-- =============================================
CREATE PROCEDURE [dbo].[Get_UserReporteesByRegion]
	@ReporteeRoleId int,
	@RegionIds VarChar(Max) = NULL,
	@IsActive Bit = Null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT DISTINCT 
		U.UserId, 
		case U.IsActive
			WHEN  '1'
			THEN ufirstname + ' ' + ulastname
			WHEN '0'
			THEN ufirstname + ' ' + ulastname +'- In Active'
		END  As UserName,  
		U.E3Userid,
		U.IsActive 
	FROM [dbo].[tblUsers] U 
	LEFT OUTER JOIN [dbo].[tblUserRegions] UR ON U.UserId = UR.UserId
	LEFT OUTER JOIN (SELECT CFIUsers.UserId, R.Id AS RegionId FROM tblUsers CFIUsers
					INNER JOIN tblRegion R ON R.RegionValue IN (SELECT * FROM dbo.SplitString(CFIUsers.Region, ','))
					WHERE urole IN (26)) CFIRegion ON U.UserId  = CFIRegion.UserId
	WHERE 
		U.urole =  @ReporteeRoleId
		--IN (SELECT * FROM dbo.SplitString(@ReporteeRoleId, ','))
		AND (@RegionIds IS NULL 
			OR UR.RegionId IN (SELECT * FROM dbo.SplitString(@RegionIds, ',')) 
			OR CFIRegion.RegionId IN (SELECT * FROM dbo.SplitString(@RegionIds, ',')))
		AND (@IsActive Is NULL OR U.IsActive = @IsActive)
END


GO

