USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsUp_AddLockDateQueE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsUp_AddLockDateQueE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 


--EXECUTE sp_InsUp_AddLockDateQueE3 '507BC9BF44D64004B269ED1F7DF41F31',9604,'05/11/2013'
-- =============================================
-- Author: Krunal Patel
-- Create date: 10/05/2013
-- Description: 20164-Add Lock Question to Correspondent Loan Registration Page
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsUp_AddLockDateQueE3]
(
--Parameter declaration
@loanGuid varchar(50),
@MWFieldNum int,
@dtLockDate datetime
)
AS
BEGIN
--Variable Declaration
 
Declare @StrCustFieldDefLoanID varchar(50);

-- Declaration of variable and assign

-- Custome FieldName = Borr Lock with Correspondent
SELECT @StrCustFieldDefLoanID=ID From mwsCustFieldDef with(NOLOCK) WHERE MWFieldNum=@MWFieldNum;

--Start
if not exists(select ID From mwlcustomfield with(NOLOCK) where CustFieldDef_ID in (@StrCustFieldDefLoanID)
and LoanApp_id in (@loanGuid)) 
	BEGIN  
		-- INSERT value in mwlcustomfield table
		INSERT INTO mwlcustomfield (ID,LoanApp_id,CustFieldDef_ID,DateValue) VALUES 
		(replace(newid(),'-',''),@loanGuid,
		@StrCustFieldDefLoanID,@dtLockDate)  
	END  
ELSE
	BEGIN 
 -- UPDATE value in mwlcustomfield table
		UPDATE mwlcustomfield SET 
			DateValue = @dtLockDate 
			WHERE CustFieldDef_ID= @StrCustFieldDefLoanID and LoanApp_id= @loanGuid  
	END  
END

GO

