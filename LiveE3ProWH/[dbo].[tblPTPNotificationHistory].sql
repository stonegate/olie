USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPTPNotificationHistory]') AND type in (N'U'))
DROP TABLE [dbo].[tblPTPNotificationHistory]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPTPNotificationHistory](	  [Id] INT NOT NULL IDENTITY(1,1)	, [LoanNo] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Flg] INT NULL	, [MailTo] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MailCC] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [InsertDate] DATETIME NULL DEFAULT(getdate())	, CONSTRAINT [PK_tblPTPNotificationHistory] PRIMARY KEY ([Id] ASC))USE [HomeLendingExperts]
GO

