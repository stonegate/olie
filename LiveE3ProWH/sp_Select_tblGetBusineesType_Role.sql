USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetBusineesType_Role]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetBusineesType_Role]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 
-- Description:	GetBusineesType_Role for clsPipeline.cs
-- =============================================
CREATE PROCEDURE [dbo].[sp_Select_tblGetBusineesType_Role]
( 
	@oRole varchar(Max)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	if(@oRole='1' or @oRole='8' or @oRole='25' or @oRole='26')
	begin
		select id as id_value,case DisplayString when 'Broker' then 'Wholesale' else DisplayString end as descriptn,BoCode as ChannelType from mwllookups where objectName='mwlloanapp' and Fieldname='ChannelType' and DisplayString<>'Retail'
	end
	else
	begin
		select id as id_value,case DisplayString when 'Broker' then 'Wholesale' else DisplayString end as descriptn,BoCode as ChannelType from mwllookups where objectName='mwlloanapp' and Fieldname='ChannelType'
	end
END

GO

