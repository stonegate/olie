USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBrokerApplication]') AND type in (N'U'))
DROP TABLE [dbo].[tblBrokerApplication]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBrokerApplication](	  [BrokerAppID] BIGINT NOT NULL IDENTITY(1,1)	, [CompanyName] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [Address] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [City] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [State] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [Zip] VARCHAR(5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [PhoneNumber] VARCHAR(12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [PrimaryContactName] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [PrimaryContactEmail] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [DateFounded] DATETIME NOT NULL	, [RequestedType] VARCHAR(5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [IsActive] BIT NOT NULL DEFAULT((1))	, [CreatedDt] DATETIME NOT NULL DEFAULT(getdate())	, CONSTRAINT [PK_tblBrokerApplication] PRIMARY KEY ([BrokerAppID] ASC))USE [HomeLendingExperts]
GO

