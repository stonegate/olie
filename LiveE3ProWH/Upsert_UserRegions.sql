USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Upsert_UserRegions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Upsert_UserRegions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author, Somnath M. R.>
-- Create date: <Create Date, 09th Dec 2013>
-- Description:	<Description, Insert into tblUserRegions table>
-- EXEC Upsert_UserRegions '1', 16524, 1
-- =============================================
CREATE PROCEDURE [dbo].[Upsert_UserRegions]
	@RegionIds NVARCHAR(MAX),
	@UserId INT,
	@CreatedBy [NVARCHAR](50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	INSERT INTO [dbo].[tblUserRegions] ([UserId], [RegionId], [CreatedBy], [CreatedOn])
		SELECT @UserId, Id.Items, @CreatedBy, GetDate() FROM [dbo].SplitString(@RegionIds, ',') Id
		WHERE 
			Id.Items NOT IN (SELECT UC.RegionId FROM [dbo].[tblUserRegions] UC WHERE UC.UserId = @UserId)

	DELETE FROM [dbo].[tblUserRegions] 
	WHERE
		UserId = @UserId 
		AND RegionId NOT IN (SELECT Id.Items FROM [dbo].SplitString(@RegionIds, ',') Id)
END

GO

