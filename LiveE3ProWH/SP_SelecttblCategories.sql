USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SelecttblCategories]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SelecttblCategories]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

Create PROCEDURE [dbo].[SP_SelecttblCategories]

AS
BEGIN
	   select CatID, CateName from tblCategories
             
END

GO

