USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchStateNameFromAbbr]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchStateNameFromAbbr]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[sp_FetchStateNameFromAbbr]
-- Add the parameters for the stored procedure here
@sAbbrivation VARCHAR(100)


AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

-- Insert statements for procedure here


BEGIN

select name from dbo.tblState where abbr = @sAbbrivation

END


END
GO

