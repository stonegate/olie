USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetMyReportClosing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetMyReportClosing]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================    
-- Author:  <Stonegate>    
-- Create date: <10/5/2012>    
-- Description: <Fetch My Report Closing>    
-- =============================================    
CREATE PROCEDURE [dbo].[SpGetMyReportClosing]   
(    
 @Puid varchar(100),  
 @CurrentStatus nvarchar(max)  
  
)    
AS    
BEGIN

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;     
    
Declare @SQL nvarchar(max)    
Set @SQL =''    
Set @SQL = 'select ''E3'' as DB,loanapp.Channeltype as BusType,loanapp.DecisionStatus, loanapp.ID as record_id,LoanNumber as loan_no,borr.lastname as BorrowerLastName,loanapp.DecisionStatus,loanapp.CurrentStatus as LoanStatus,DateAppSigned as SubmittedDate,Loandata.LoanProgramName as LoanProgram,
case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,borr.CompositeCreditScore as CreditScoreUsed,loanData.AdjustedNoteAmt as LoanAmount,
 convert(varchar(35),EstCloseDate,101) AS ScheduleDate,LockStatus,loanapp.LockExpirationDate,UnderwriterName as UnderWriter,CloserName as Closer,CASE ISNULL(RTRIM(Institute.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Institute.Company) END as BrokerName, Institute.CustomDataOne as TitleCompany,
 AppStatus.StatusDateTime as DocsSentDate,FundedStatus.StatusDateTime as FundedDate,'''' as LU_FINAL_HUD_STATUS, LockRecord.DeliveryOption ,loanapp.MWBranchCode TPORegion
 from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
  left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  Left join dbo.mwlInstitution as Institute on Institute.ObjOwner_id = loanapp.id and 
  InstitutionType = ''BROKER'' and objownerName=''BROKER'' Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and AppStatus.StatusDesc like ''%Docs Out%''
     Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''
   Left join mwlappstatus as FundedStatus on FundedStatus.LoanApp_id=loanapp.id  and FundedStatus.StatusDesc like ''%Funded''
   left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType = ''LOCK'' and  LockRecord.Status <> ''CANCELED'' 
 where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''
and CurrentStatus IN (select * from dbo.SplitString('''+@CurrentStatus+''','','')) '    
     
 if (len(@Puid) = 0)    
  Begin    
    Set @SQL = @SQL + ' and (Originator_id in ('''+ @Puid +''') '    
  End    
 else   
   Begin    
   Set @SQL = @SQL + ' and (Originator_id in ('''+ @Puid +''')) '    
   End    
  Set @SQL = @SQL + ' order by CurrentStatus'
Print @SQL    
exec sp_executesql @SQL    
END 


GO

