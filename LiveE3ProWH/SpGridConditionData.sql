USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGridConditionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGridConditionData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Nirav>
-- Create date: <10/8/2012>
-- Description:	<fetch Grid condition data  of loan number>
-- =============================================
CREATE PROCEDURE [dbo].[SpGridConditionData] 
	@LoanNo varchar(500)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	 select AppStatus.StatusDesc,CurrentStatus,* from mwlloanapp loanapp Left join mwlApprovalStatus AppStatus on loanapp.ID= AppStatus.LoanApp_id where loannumber=@LoanNo
END

GO

