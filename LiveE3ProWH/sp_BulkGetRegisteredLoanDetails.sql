USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_BulkGetRegisteredLoanDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_BulkGetRegisteredLoanDetails]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_BulkGetRegisteredLoanDetails]
@Status varchar(50) = 'SUCCESS',
@filter nvarchar(max) = NULL,
@sortfield nvarchar(50) = '[Registration Date] DESC',
@pageindex tinyint = 0,
@pagesize smallint = 50,
@userId nvarchar(50)
as
begin
Declare @sql nvarchar(max) 
Declare @sqlsuspend nvarchar(max) 
declare @startpage int
declare @endpage int
declare @sortpagefilter nvarchar(max)
declare @UnRegisteredLoansRetainDays varchar(10)
declare @RegisteredLoansRetainDays varchar(10)
set @startpage = @pageindex * @pagesize
set @endpage = (@pageindex * @pagesize) + @pagesize

set @UnRegisteredLoansRetainDays =  10 --(select KeyValue from tblAppsettings where KeyName='UnRegisteredLoansRetainDays')
--set @RegisteredLoansRetainDays = (select KeyValue from tblAppsettings where KeyName='RegisteredLoansRetainDays')



   IF (@Status = 'SUCCESS') 
   BEGIN
	SET @SQL ='	SELECT LoanFileID, 
	BSL.LoanNumber as ''Loan Number'' ,
		BLF.CreatedOn as ''Upload Date And Time'' ,
		loan.[CreatedOnDate] as ''Registration Date'',
		BSL.BorrowerLastName as ''Borrower LastName'',
		BSL.PropertyAddress as ''Property Address'' , 
		CASE when ( BSL.Type = ''TSV'' AND BLF.Type IS NULL) THEN BSL.[FileName] else NULL END as ''Datalink'',
		'''' as ''File Name'',
		BSL.Comments as  ''Reason'',
		BSL.FileName,
	--	BSL.FinalStatus,
		''' +@Status+''' as ''Status''
	    INTO #tempRecords
		FROM [tblBulkSplitLoanFiles]  BSL
		INNER JOIN [tblBulkLoanFiles] BLF on BSL.BulkLoanFileId = BLF.BulkLoanFileId
		LEFT JOIN [mwlLoanapp] loan on loan.LoanNumber=BSL.LoanNumber 
		--LEFT JOIN 
		--(
		--	select LoanNumber , Currentstatus from mwlloanapp where currentstatus in (''93 - Loan Incomplete'' , ''94 - Approved Not Accepted'' , ''95 - Denied for Purchase'' , ''96 - Loan Rescinded'' , ''97 - Loan Denied'' , ''98 - Loan Canceled'' , ''99 - Loan Withdrawn'')
		--)tempcancelloans on tempcancelloans.LoanNumber = loan.LoanNumber
--		 INNER join
--		(
--		select LoanNumber from [mwlLoanapp] where CurrentStatus in (''01 - Registered'',''02 - Assumption In Process'',''03 - Appt Set to Review Discls'',''04 - Disclosures Sent'',''05 - Application Taken'',''06 - Disclosures Out'',''07 - Disclosures Received'',''09 - Application Received'',''10 - Application On Hold'',''13 - File Intake'',''14 - Prop Inspection Waived'',''15 - Appraisal Ordered'',''17 - Submission On Hold'')
		--CASE WHEN ISNUMERIC(Substring(loan.CurrentStatus,1,2)) = 1 THEN CAST( substring(loan.CurrentStatus,1,2) AS INT) ELSE 0 END as ''LoanStatus'' from mwlloanapp loan
--		)temp17statuslessloans on temp17statuslessloans.LoanNumber = BSL.LoanNumber 
		where 
		(BSL.Status = '''+@Status+''' OR BSL.FinalStatus = '''+@Status+''' ) AND BSL.[RegistrationStatus] = 0 
		AND loan.Currentstatus not in (''93 - Loan Incomplete'' , ''94 - Approved Not Accepted'' , ''95 - Denied for Purchase'' , ''96 - Loan Rescinded'' , ''97 - Loan Denied'' , ''98 - Loan Canceled'' , ''99 - Loan Withdrawn'')
		AND loan.currentstatus in (''01 - Registered'',''02 - Assumption In Process'',''03 - Appt Set to Review Discls'',''04 - Disclosures Sent'',''05 - Application Taken'',''06 - Disclosures Out'',''07 - Disclosures Received'',''09 - Application Received'',''10 - Application On Hold'',''13 - File Intake'',''14 - Prop Inspection Waived'',''15 - Appraisal Ordered'',''17 - Submission On Hold'')
		AND BSL.CreatedBy = '''+@userId+''''
		
	END
	ELSE
	BEGIN
	SET @SQL = 'SELECT LoanFileID, 
		BSL.LoanNumber as ''Loan Number'' ,
		BLF.CreatedOn as ''Upload Date And Time'' ,
		loan.[CreatedOnDate] as ''Registration Date'',
		BSL.BorrowerLastName as ''Borrower LastName'',
		BSL.PropertyAddress as ''Property Address'' , 
		CASE when (BSL.Type = ''TSV'' AND BLF.Type IS NULL) THEN BSL.[FileName] else NULL END as ''Datalink'',
		CASE WHEN (BSL.Type IS NULL AND BSL.Status = ''FAIL'' AND BSL.FinalStatus = ''FAIL'') THEN BLF.FileName ELSE '''' END as  ''File Name'',
		BSL.Comments as  ''Reason'',
		BSL.FileName,
		
		''' +@Status+''' as ''Status''
	--	BSL.FinalStatus
		INTO #tempRecords
		FROM [tblBulkSplitLoanFiles]  BSL
		INNER JOIN [tblBulkLoanFiles] BLF on BSL.BulkLoanFileId = BLF.BulkLoanFileId
    	LEFT JOIN [mwlLoanapp] loan on loan.LoanNumber=BSL.LoanNumber
		where (BLF.CreatedOn between  dbo.fun_BulkGetNthWorkingDay('+  @UnRegisteredLoansRetainDays+ ') AND GETDATE()) 
		AND BSL.CreatedBy = '''+@userId+'''
		AND '
   
		IF (@Status = 'FAIL')
		 SET @SQL = @SQL + '( BSL.Status = '''+@Status+'''  OR BSL.FinalStatus = '''+@Status+''' OR BSL.Status = ''BLOCKED'' ) '
		ELSE
		 SET @SQL = @SQL + '( BSL.Status = '''+@Status+'''  AND BSL.FinalStatus IS NULL ) '
		
	END

IF(@filter = '' OR @filter IS  NULL)
	SET @filter = ''
else
	SET @filter = 'WHERE ' + @filter    

IF(@endpage = 0)
	SET @sortpagefilter = '' 
else		
	SET @sortpagefilter = ' where finaltemp.RowNum > ' + CONVERT(VARCHAR(10), @startpage) +' and finaltemp.RowNum <= ' + convert(varchar(10),@endpage )

SET @sql = @sql + ' select  * from (  select   ROW_NUMBER() OVER (ORDER BY '+ CONVERT(VARCHAR(100), @sortfield) + ')  as RowNum,  * from #tempRecords '+ @filter + ') as finaltemp ' + @sortpagefilter
SET @sql = @sql  + ' select Count(*) as count from #tempRecords ' + @filter

print @sql
EXECUTE sp_executesql @SQL

--	select dbo.fun_BulkGetNthWorkingDay(10) 

end

-- [dbo].[sp_BulkGetRegisteredLoanDetails] 'SUCCESS', '([Upload Date And Time]  between ''4/28/2014'' and ''5/14/2014'')'  ,'[Registration Date] DESC',1,2
-- [dbo].[sp_BulkGetRegisteredLoanDetails] 'SUCCESS', ''  ,'[Registration Date] DESC',0,50,10789
--( [Upload Date And Time]  between '4/28/2014' and '6/12/2014'))
  

GO

