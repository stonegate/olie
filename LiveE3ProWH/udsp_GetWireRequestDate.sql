USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetWireRequestDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetWireRequestDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE udsp_GetWireRequestDate 
	@LoanApp_ID varchar(200)
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT top 1 DateValue 
		FROM mwlcustomfield 
	WHERE LoanApp_id=@LoanApp_ID AND CustFieldDef_ID='B39D9531F20A40A4B9E7AA878DCB40FA'   
	ORDER BY UpdatedOnDate DESC 
END

GO

