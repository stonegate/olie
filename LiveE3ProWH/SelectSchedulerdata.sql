USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SelectSchedulerdata]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SelectSchedulerdata]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SelectSchedulerdata](
@purpose varchar(50),
@CTC varchar(50)
)
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

    select @purpose =  (case TransType when NULL then '' when 'P' then 'Purchase' when 'R' then 'Refinance' when '2' then 'Purchase money second mortgage' when 'S' then 'Second mortgage, any other purpose' when 'A' then 'Assumption' when 'HOP' then 'HELOC - other purpose' when 'HP' then 'HELOC - purchase' else '' end) from [INTERLINQE3].[dbo].mwlLoanApp where LoanNumber is not null and LoanNumber <> ''  and Loannumber='0000307859'
	select @CTC =  (case loanapp.currentStatus when '33 - Cleared to Close' then 'CTC'  else 'Not CTC' end) from [INTERLINQE3].[dbo].mwlLoanApp loanapp Inner join [INTERLINQE3].[dbo].mwlBorrower as borr on borr.loanapp_id = loanapp.id Inner join [INTERLINQE3].[dbo].mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''  and Loannumber='0000307859'
select @purpose,@CTC,borr.lastname as BorrowerLastName,loanapp.ChannelType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus,loanapp.ID as record_id,LoanNumber as loan_no,CloserName as Closer,loanapp.CurrentStatus as LoanStatus,loanData.AdjustedNoteAmt as loan_amt,CASE ISNULL(RTRIM(Broker.Company),'') WHEN '' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,dateadd(d,-2,EstCloseDate) AS ScheduleStartDate, convert(varchar(35),EstCloseDate,101) AS ScheduleEndDate,BranchOffice.Office  as Office from [INTERLINQE3].[dbo].mwlLoanApp loanapp Inner join [INTERLINQE3].[dbo].mwlBorrower as borr on borr.loanapp_id = loanapp.id Inner join [INTERLINQE3].[dbo].mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id  Left join [INTERLINQE3].[dbo].mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = 'BROKER' and Broker.objownerName='Broker' Left join [INTERLINQE3].[dbo].mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = 'CORRESPOND' and Corres.objownerName='Correspondent' Left join [INTERLINQE3].[dbo].mwlInstitution as BranchOffice on BranchOffice.ObjOwner_id = loanapp.id and BranchOffice.InstitutionType = 'BRANCH' and BranchOffice.objownerName='BranchInstitution' where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''  and Loannumber='0000307859'

GO

