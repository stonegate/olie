USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetRefPurpose]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetRefPurpose]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetRefPurpose] 
	
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	select  *from mwlLookups where ObjectName='mwlloandata' and FieldName='RefiPurpose' and  (displayString is  not null  and displayString <> '')
END

GO

