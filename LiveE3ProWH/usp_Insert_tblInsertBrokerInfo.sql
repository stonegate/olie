USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Insert_tblInsertBrokerInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Insert_tblInsertBrokerInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- =============================================  
-- Author:  Gourav  
-- Create date: 10/03/2012  
-- Description: Procedure for clsUser.cs bll    
-- =============================================  
CREATE proc [dbo].[usp_Insert_tblInsertBrokerInfo]  
(  
@ufirstname varchar(Max)=null  
,@ulastname varchar(Max)=null  
,@uemailid varchar(Max)=null  
,@urole varchar(Max)=null  
,@isadmin varchar(Max)=null  
,@isactive varchar(Max)=null  
,@uidprolender varchar(Max)=null  
,@uparent varchar(Max)=null  
,@upassword varchar(Max)=null  
,@mobile_ph varchar(Max)=null  
,@phonenum varchar(Max)=null  
,@loginidprolender varchar(Max)=null  
,@carrierid varchar(Max)=null  
,@MI varchar(Max)=null  
,@companyname varchar(Max)=null  
,@AEID varchar(Max)=null  
,@Region varchar(Max)=null  
,@E3Userid varchar(Max)=null  
,@PartnerCompanyid varchar(Max)=null  
,@BrokerType varchar(Max)=null  
,@PasswordExpDate varchar(Max)=null  
,@CreatedBy varchar(Max)=null  
,@IsUserInNewProcess varchar(Max)=null  
)  
as  
begin  
Insert into tblusers  
(  
ufirstname  
,ulastname  
,uemailid  
,urole  
,isadmin  
,isactive  
,uidprolender  
,uparent  
,upassword  
,mobile_ph  
,phonenum  
,loginidprolender  
,carrierid  
,MI  
,companyname  
,AEID  
,Region  
,E3Userid  
,PartnerCompanyid  
,BrokerType  
,PasswordExpDate  
,CreatedBy  
,IsUserInNewProcess  
)  
Values  
(  
@ufirstname  
,@ulastname  
,@uemailid  
,@urole  
,@isadmin  
,@isactive  
,@uidprolender  
,@uparent  
,@upassword  
,@mobile_ph  
,@phonenum  
,@loginidprolender  
,@carrierid  
,@MI  
,@companyname  
,@AEID  
,@Region  
,@E3Userid  
,@PartnerCompanyid  
,@BrokerType  
,@PasswordExpDate  
,@CreatedBy  
,@IsUserInNewProcess  
)  
end  
  
  
GO

