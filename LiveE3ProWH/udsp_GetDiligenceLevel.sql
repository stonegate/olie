USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetDiligenceLevel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetDiligenceLevel]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
--Example for execute stored procedure             
--EXECUTE [udsp_GetDiligenceLevel] '0000366451','9492'      
-- =============================================            
-- Author:  vishal sharma    
-- Create date: 07/03/2013         
-- Name:udsp_GetDiligenceLevel.sql      
-- Description: Get Diligence Level for Closing Package Reviewed loan.          
 
-- =============================================                     
--EXECUTE [udsp_GetDiligenceLevel] '0000366451','9492','53 - Closed Package Reviewed'
-- =============================================            
-- Author:  vishal sharma    
-- Modified date: 20/08/2013         
-- Name:udsp_GetDiligenceLevel.sql      
-- Description: Add additional parameter for managing Loan statuses.  

-- =============================================                     
--EXECUTE [udsp_GetDiligenceLevel] '0000366451','9492','53 - Closed Package Reviewed'
-- =============================================            
-- Author:  Ratan Gohil 
-- Modified date: 10/30/2013         
-- Name:udsp_GetDiligenceLevel.sql      
-- Description: removed default value from diligencelevel.  


-- =============================================           
CREATE PROCEDURE [dbo].[udsp_GetDiligenceLevel]              
(            
@LoanNumber VARCHAR(10) ,        
@MWFieldNumDiligenceLevel VARCHAR(10),  
@LoanStatus VARCHAR(70)           
)                    
AS                      
BEGIN               
SET NOCOUNT ON;        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)        
SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,               
                 
DECLARE @custDiligenceLevelID AS VARCHAR(50)            
SET @custDiligenceLevelID = (SELECT ID FROM mwsCustFieldDef WHERE MWFieldNum = @MWFieldNumDiligenceLevel)  -- Custom Field Name "Diligence Level", Custom Field Number "9490"       
SELECT StringValue AS DilligenceLevel            
FROM mwlLoanApp WITH (NOLOCK)            
       INNER JOIN mwlAppStatus WITH (NOLOCK) ON mwlLoanApp.ID = mwlAppStatus.LoanApp_ID            
       INNER JOIN (SELECT LoanApp_ID, MAX(StatusDateTime) AS StatusDateTime FROM mwlAppStatus WITH (NOLOCK) GROUP BY LoanApp_ID)X            
             ON mwlAppStatus.StatusDateTime = X.StatusDateTime AND mwlAppStatus.LoanApp_ID = X.LoanApp_ID            
       LEFT JOIN mwlCustomField WITH (NOLOCK) ON mwlLoanApp.ID = mwlCustomField.LoanApp_ID             
       AND mwlCustomField.CustFieldDef_ID = @custDiligenceLevelID            
WHERE StatusDesc IN (select items from dbo.SplitString(@LoanStatus,','))  AND mwlLoanApp.LoanNumber=@LoanNumber            
END 



GO

