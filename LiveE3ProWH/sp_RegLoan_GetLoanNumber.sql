USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RegLoan_GetLoanNumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RegLoan_GetLoanNumber]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_RegLoan_GetLoanNumber]
(
@LoanID nvarchar(500)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	select LoanNumber from mwlLoanApp where id = @LoanID
END



GO

