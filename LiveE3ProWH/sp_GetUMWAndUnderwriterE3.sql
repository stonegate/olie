USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUMWAndUnderwriterE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUMWAndUnderwriterE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author : <Stonegate>
-- Create date : <25/4/2013>
-- Create by : <Shayamal Gajjar>
-- Description : <This sp is give information about getting underwriter, Underwriter manager,Underwriter team lead from Web site >
-- Updated By : Shayamal Gajjar
-- Description : Addition of underwriter team lead cretria in SP which is login user.i.e adding of
-- @userid as extra parameter
-- =============================================
--sp_GeptUMWAndUnderwriterE3 9664
CREATE PROCEDURE [dbo].[sp_GetUMWAndUnderwriterE3]
@userid INT
AS
SET TRANSACTION isolation level READ uncommitted;

BEGIN
DECLARE @ChannelType VARCHAR(20)
DECLARE @UnderWriter INT

SET @UnderWriter = 10

DECLARE @UnderWriterMgr INT

SET @UnderWriterMgr = 11

SELECT @ChannelType = brokertype
FROM tblusers
WHERE userid = @userid

SELECT e3userid,
Ltrim(Rtrim(ufirstname)) + ' ' + ulastname AS Underwriter
FROM tblusers
WHERE ( urole = @UnderWriter
OR urole = @UnderWriterMgr
OR userid = @userid )
---@userid is login user id and which is underwriter team lead
AND isactive = 1
-- showing only active user in list of Underwriter team lead.
AND brokertype = @ChannelType
ORDER BY Ltrim( Rtrim(ufirstname)) + ' ' + ulastname
END


 

/****** Object:  StoredProcedure [dbo].[sp_GetUMWAndUnderwriterE3]    Script Date: 08/05/2013 12:37:38 ******/
SET ANSI_NULLS ON


GO

