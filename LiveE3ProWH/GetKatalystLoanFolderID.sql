USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetKatalystLoanFolderID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetKatalystLoanFolderID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Vipul,,Thacker>
-- Create date: <10 March 2013>
-- Description:	<This Stored procedure used for check whether Katalsyt loan folder existis or not in E3 DB>
---Modify By: <Vipul, Thacker>
-- Modify date: <04 April 2013>
-- =============================================
CREATE PROCEDURE [dbo].[GetKatalystLoanFolderID]
(
	@LoanNumber varchar(20)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
	
	Select StringValue from mwlCustomField  WITH (nolock) where LoanApp_ID in (Select ID from mwlLoanApp  WITH (nolock) where LoanNumber = @LoanNumber) 
		and CustFieldDef_ID = (Select ID from mwsCustFieldDef  WITH (nolock) Where MWFieldNum=9553)
	
	
END


GO

