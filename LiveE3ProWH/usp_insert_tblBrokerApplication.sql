USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_insert_tblBrokerApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_insert_tblBrokerApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_insert_tblBrokerApplication](
@CompanyName VARCHAR(100),
@Address VARCHAR(200),
@City VARCHAR(100),
@State VARCHAR(50),
@Zip VARCHAR(5),
@PhoneNumber VARCHAR(12),
@PrimaryContactName VARCHAR(100),
@PrimaryContactEmail VARCHAR(50),
@DateFounded DATETIME,
@RequestedType VARCHAR(5),
@BrokerAppID bigint OUTPUT)
AS
BEGIN
 INSERT INTO tblBrokerApplication (CompanyName,Address,City,State,Zip,PhoneNumber,PrimaryContactName,
  PrimaryContactEmail,DateFounded,RequestedType) VALUES
  (@CompanyName,@Address,@City,@State,@Zip,@PhoneNumber,@PrimaryContactName,
  @PrimaryContactEmail,@DateFounded,@RequestedType)
  
 SET @BrokerAppID = SCOPE_IDENTITY() 
END


GO

