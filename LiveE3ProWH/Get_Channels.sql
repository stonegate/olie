USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_Channels]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Get_Channels]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author, Somnath M. R.>
-- Create date: <Create Date, 17th Dec 2013>
-- Description:	<Description, Select from tblChannels table>
-- EXEC Get_Channels
-- =============================================
CREATE PROCEDURE [dbo].[Get_Channels]
@BusinessSource nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
		C.ChannelId,
		C.Name,
		C.BusinessSource
	FROM
		[dbo].[tblChannels] C
	WHERE
		@BusinessSource IS NULL OR BusinessSource IN (SELECT * FROM dbo.SplitString(@BusinessSource, ','))
END

GO

