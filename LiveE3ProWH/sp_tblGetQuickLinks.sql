USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_tblGetQuickLinks]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_tblGetQuickLinks]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 
-- Description:	Procedure for clsQuickLinkcs bll  
-- =============================================
Create proc [dbo].[sp_tblGetQuickLinks]
(
@ID varchar(Max)
)
as
SELECT * FROM tblQuickLinks WHERE ID=@ID 

GO

