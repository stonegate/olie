USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetKatalystFolderLoan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetKatalystFolderLoan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Shwetal,,Shah>
-- Create date: <03 March 2013>
-- Description:	<This Stored procedure used for Search katalsyt loans from E3 DB>
---Modify By: <Vipul, Thacker>
-- Modify date: <08 April 2013>
-- ============================================= 
CREATE PROCEDURE [dbo].[GetKatalystFolderLoan]
(
@LoanNumber varchar(20),
@BorrowerLastName varchar(200)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 


If(ISNULL(@BorrowerLastName,'') <> '')
Begin
	Select 'E3' as DB,mwlLoanApp.ID,mwlLoanApp.LoanNumber,mwlBorrower.LastName as BorrowerLastName,mwlLoanData.AdjustedNoteAmt as loan_amt
	,mwlAppStatus.StatusDateTime AS SubmittedDate
	from mwlLoanApp WITH(NOLOCK) 
	Inner Join (Select LoanApp_ID,StringValue from mwlcustomField WITH(NOLOCK)  Where CustFieldDef_ID = (Select ID From mwsCustFieldDef Where MWFieldNum = 9553)) as KatInfo
		 ON KatInfo.LoanApp_ID = mwlLoanApp.ID
	Inner Join (Select LoanApp_ID, LastName From mwlBorrower WITH(NOLOCK)  Where SequenceNum=1) mwlBorrower On mwlBorrower.LoanApp_ID = mwlLoanApp.ID
	Inner join dbo.mwlLoanData WITH(NOLOCK)  ON mwlLoanData.ObjOwner_id = mwlLoanApp.ID
	INNER join mwlAppStatus WITH(NOLOCK)  ON mwlAppStatus.LoanApp_id=mwlLoanApp.ID and mwlAppStatus.StatusDesc='01 - Registered'
	where mwlBorrower.LastName like ''+@BorrowerLastName+'%'
End
Else
Begin
	Select 'E3' as DB,mwlLoanApp.ID,mwlLoanApp.LoanNumber,mwlBorrower.LastName as BorrowerLastName,mwlLoanData.AdjustedNoteAmt as loan_amt
	,mwlAppStatus.StatusDateTime AS SubmittedDate
	from mwlLoanApp WITH(NOLOCK) 
	Inner Join (Select LoanApp_ID,StringValue from mwlcustomField WITH(NOLOCK)  Where CustFieldDef_ID = (Select ID From mwsCustFieldDef WITH(NOLOCK)  Where MWFieldNum = 9553)) as KatInfo
		 ON KatInfo.LoanApp_ID = mwlLoanApp.ID
	Inner Join (Select LoanApp_ID, LastName From mwlBorrower WITH(NOLOCK)  Where SequenceNum=1) mwlBorrower On mwlBorrower.LoanApp_ID = mwlLoanApp.ID
	Inner join dbo.mwlLoanData WITH(NOLOCK)  ON mwlLoanData.ObjOwner_id = mwlLoanApp.ID
	INNER join mwlAppStatus WITH(NOLOCK)  ON mwlAppStatus.LoanApp_id=mwlLoanApp.ID and mwlAppStatus.StatusDesc='01 - Registered'
	where mwlLoanApp.LoanNumber like ''+@LoanNumber+'%'
End


END


GO

