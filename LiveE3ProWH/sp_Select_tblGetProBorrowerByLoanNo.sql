USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetProBorrowerByLoanNo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetProBorrowerByLoanNo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Gourav  
-- Create date: 10/03/2012  
-- Description: Procedure for clsUser.cs bll    
-- =============================================  
CREATE proc [dbo].[sp_Select_tblGetProBorrowerByLoanNo]  
(  
@strRecID varchar(Max)  
)  
as  
begin  
select borr_last from cobo where parent_id in(select record_id from loandat where loan_no=@strRecID)
end  


GO

