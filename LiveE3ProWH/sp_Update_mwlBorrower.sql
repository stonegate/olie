USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_mwlBorrower]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_mwlBorrower]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		 Nitin
-- Create date: 10/05/2012
-- Description:	Procedure for clspipelineE3.cs bll 
-- =============================================
CREATE procEDURE [dbo].[sp_Update_mwlBorrower]
(@CAIVRnum varchar(10),
@ID varchar(32)
)
AS
BEGIN
	
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE mwlBorrower SET CAIVRnum=@CAIVRnum where ID =@ID
END


GO

