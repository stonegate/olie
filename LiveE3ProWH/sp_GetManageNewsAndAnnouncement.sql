USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetManageNewsAndAnnouncement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetManageNewsAndAnnouncement]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Stonegate    
-- Create date: <9/27/2012>    
-- Description: <This procedure will Manage News and Anno. Section>    
--    Param @Action Id = 1 for Insert data    
--    Param @Action Id = 2 for Update data    
--    Param @Action Id = 3 for Select data    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_GetManageNewsAndAnnouncement]    
(    
 @ActionID int,    
 @Title nvarchar(1000),    
 @Body text,    
 @IsActive bit=0,    
 @IsNewsAnnouncement bit=0,     
 @Role nvarchar(Max),    
 @RoleC nvarchar(Max),  
 @RoleR nvarchar(Max),  
 @NewsAnnousVisbleAd bit=0,    
 @NewsAnnousVisbleDR bit=0,    
 @NewsAnnousVisbleBR bit=0,    
 @NewsAnnousVisbleLO bit=0,    
 @NewsAnnousVisbleCU bit=0,    
 @NewsAnnousVisbleRE bit=0,    
 @NewsAnnousVisbleRM bit=0,    
 @NewsAnnousVisbleDP bit=0,    
 @NewsAnnousVisbleCP bit=0,    
 @NewsAnnousVisbleHY bit=0,    
 @NewsAnnousVisbleDCFI bit=0,    
 @NewsAnnousVisibleSCFI bit=0,    
 @ModifiedBy int,    
 @ModifiedDate datetime,    
 @NewsID int --Used for update and delete purpose    
)    
AS    
BEGIN    
SET NOCOUNT ON;    
    
    
If (@ActionID = 1) -- Insert    
Begin    
Insert into     
 tblNewsAndAnnouncement    
  (    
   Title,Body,IsActive,IsNewsAnnouncement,CreatedBy,CreatedDate,    
   Role,RoleC,RoleR,NewsAnnousVisbleAd,NewsAnnousVisbleDR,NewsAnnousVisbleBR,NewsAnnousVisbleLO,    
   NewsAnnousVisbleCU,NewsAnnousVisbleRE,NewsAnnousVisbleRM,NewsAnnousVisbleDP,    
   NewsAnnousVisbleCP,NewsAnnousVisbleHY,NewsAnnousVisbleDCFI,NewsAnnousVisibleSCFI,ModifiedBy,ModifiedDate    
  )     
 Values    
  (    
    @Title,@Body,@IsActive,@IsNewsAnnouncement ,@ModifiedBy,@ModifiedDate,    
    @Role,@RoleC,@RoleR,@NewsAnnousVisbleAd,@NewsAnnousVisbleDR,@NewsAnnousVisbleBR,@NewsAnnousVisbleLO,    
    @NewsAnnousVisbleCU,@NewsAnnousVisbleRE,@NewsAnnousVisbleRM,@NewsAnnousVisbleDP,    
    @NewsAnnousVisbleCP,@NewsAnnousVisbleHY,@NewsAnnousVisbleDCFI,    
    @NewsAnnousVisibleSCFI,@ModifiedBy,@ModifiedDate    
  )    
Select 1    
END    
    
If (@ActionID = 2) -- Update    
Begin    
Update     
 tblNewsAndAnnouncement    WITH (UPDLOCK)   
 Set    
   Title = @Title, Body = @Body,IsActive = @IsActive,IsNewsAnnouncement = @IsNewsAnnouncement,    
   Role = @Role,RoleC = @RoleC,RoleR = @RoleR,NewsAnnousVisbleAd = @NewsAnnousVisbleAd,NewsAnnousVisbleDR = @NewsAnnousVisbleDR,    
   NewsAnnousVisbleBR = @NewsAnnousVisbleBR, NewsAnnousVisbleLO = @NewsAnnousVisbleLO,    
   NewsAnnousVisbleCU = @NewsAnnousVisbleCU,NewsAnnousVisbleRE = @NewsAnnousVisbleRE,NewsAnnousVisbleRM = @NewsAnnousVisbleRM,    
   NewsAnnousVisbleDP = @NewsAnnousVisbleDP, NewsAnnousVisbleCP = @NewsAnnousVisbleCP, NewsAnnousVisbleHY = @NewsAnnousVisbleHY,    
   NewsAnnousVisbleDCFI = @NewsAnnousVisbleDCFI,NewsAnnousVisibleSCFI = @NewsAnnousVisibleSCFI,ModifiedBy = @ModifiedBy,ModifiedDate = @ModifiedDate    
 Where     
   ID = @NewsID     
    
Select 1    
END    
    
If (@ActionID = 3) -- Select    
Begin    
 Select * From tblNewsAndAnnouncement Where ID = @NewsID    
END    
    
END

GO

