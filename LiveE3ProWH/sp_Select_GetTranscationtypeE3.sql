USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_GetTranscationtypeE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_GetTranscationtypeE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[sp_Select_GetTranscationtypeE3]
as
begin
	select 0 as lookupid, '--Select Type--' as Description union select lookupid,Description  
from tbllookups where lookupname ='TT'
end

GO

