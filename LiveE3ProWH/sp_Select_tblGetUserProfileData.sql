USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetUserProfileData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetUserProfileData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblGetUserProfileData]
(
@iUserID varchar(Max)
)
as
begin
SELECT userid, ufirstname,ulastname, ufirstname +', '+ ulastname AS FullName, experience, bio, AreaOfExpertise,uemailid,phone, 
 mobile_ph,fax,ADDRESS, STATE + ' , ' + zip AS FullAddress, photos, 
 STATE, zip, facebookid, twiterid, ClientTestimonial FROM dbo.tblUsers 
 Where userid=@iUserID
end


GO

