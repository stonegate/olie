USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoginfromRecordID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoginfromRecordID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE sp_GetLoginfromRecordID  
 -- Add the parameters for the stored procedure here  
 @recordId  varchar(max)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
    -- Insert statements for procedure here  
  
select LOGIN as username from offices where record_id = @recordId   
  
END  
GO

