USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertBlitzdocsLoanDocs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertBlitzdocsLoanDocs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Name  
-- Create date:   
-- Description:   
-- =============================================  
CREATE PROCEDURE [dbo].[sp_InsertBlitzdocsLoanDocs]  
(  
@uloanregid bigint  
,@Docname varchar(max),  
@Comments varchar(Max)  
,@userid bigint  
,@createddate datetime ,
@loan_No varchar(max) 
)  
AS  
BEGIN  
Insert into tblloandoc(uloanregid,Docname,Comments,userid,loan_no,createddate) Values(  
@uloanregid,@Docname,@Comments,@userid,@createddate,@loan_No 
)  
END  

GO

