USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLoanPrograms]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetLoanPrograms]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Author,,Name>    
-- Create date: <30/8/2012>    
-- Description: <Description,,>    
-- =============================================    
CREATE PROCEDURE  [dbo].[GetLoanPrograms]    
    
AS    
BEGIN    

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

 Select Id,loanProgramName from mwpLoanProgram where Broker = '1' and Correspondent = '1' and expirationDate>=getdate() order by loanProgramName 

END 

GO

