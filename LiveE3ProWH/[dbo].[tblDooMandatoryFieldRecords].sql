USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDooMandatoryFieldRecords]') AND type in (N'U'))
DROP TABLE [dbo].[tblDooMandatoryFieldRecords]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDooMandatoryFieldRecords](	  [RowId] BIGINT NOT NULL IDENTITY(1,1)	, [LoanNumber] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [StepNumber] VARCHAR(9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MandatoryFieldName] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MandatoryFieldValue] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Conditions] VARCHAR(2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblDooMandatoryFieldRecords] PRIMARY KEY ([RowId] ASC))USE [HomeLendingExperts]
GO

