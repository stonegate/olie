USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetCountries]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetCountries]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Mukesh Kumar  
-- Create date: 08/31/2012  
-- Description: To Get Countries List  
-- =============================================  
CREATE PROCEDURE [dbo].[udsp_GetCountries]   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 SELECT cntdesc From tblCountry order by cntdesc  
END  
  
  
  

GO

