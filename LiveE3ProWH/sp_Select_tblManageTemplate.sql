USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblManageTemplate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblManageTemplate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Select_tblManageTemplate]           
 

-- Add the parameters for the stored procedure here          
           


AS          
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from          
-- interfering with SELECT statements.          
SET NOCOUNT ON;

-- Insert statements for procedure here          

SELECT
	*
FROM [tblManageTemplate]

ORDER BY id DESC        
END

GO

