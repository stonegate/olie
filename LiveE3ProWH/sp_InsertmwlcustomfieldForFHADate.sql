USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertmwlcustomfieldForFHADate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertmwlcustomfieldForFHADate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Vipul  
-- Create date: 12/21/2012    
-- Description: Procedure for clspipelineE3 BLL    
-- =============================================    
    
CREATE procEDURE [dbo].[sp_InsertmwlcustomfieldForFHADate]    
(    
  
@LoanApp_ID varchar(32),    
@CustFieldDef_ID varchar(32),    
@DateValue DATETIME  
    
)    
AS    
BEGIN    
    
 SET NOCOUNT ON;    
if not exists(select * From mwlcustomfield where LoanApp_id in(@LoanApp_ID) and CustFieldDef_ID=@CustFieldDef_ID)    
begin     
Insert into mwlcustomfield(ID,LoanApp_ID,CustFieldDef_ID,DateValue)       
Values(replace(newid(),'-',''),@LoanApp_ID,@CustFieldDef_ID,@DateValue)      
END    
ELSE    
BEGIN    
UPDATE mwlcustomfield SET DateValue=@DateValue WHERE LoanApp_id=@LoanApp_ID AND CustFieldDef_ID=@CustFieldDef_ID    
END    
  
END    
GO

