USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpDeleteNewsAnnounceMent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpDeleteNewsAnnounceMent]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpDeleteNewsAnnounceMent] 
	@Id varchar(500)
AS
BEGIN
	delete from tblNewsAndAnnouncement Where ID IN ( @Id)
END

GO

