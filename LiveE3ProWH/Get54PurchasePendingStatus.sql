USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get54PurchasePendingStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Get54PurchasePendingStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE Procedure [dbo].[Get54PurchasePendingStatus]  
(  
@LoanNumber varchar(max)  
)  
AS  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

(  
select CurrentStatus from mwlLoanApp where CurrentStatus ='54 - Purchase Pending' and LoanNumber=@LoanNumber  
)   
GO

