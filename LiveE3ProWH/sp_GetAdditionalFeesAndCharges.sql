USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAdditionalFeesAndCharges]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAdditionalFeesAndCharges]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- Author:  <Kanva Patel>      
-- Create date: <18th Sep 2012>      
-- Description: <This will get Additional Fees & Charges Details for Docs On OLIE project step-6>
-- 1300,2000,201 & 212 are HUD Line number series that we need to display on UI
-- As per BRD we need to display 1300 to 1308 HUD lines and 201 to 212 series on Additional Fees & Charges       
-- =============================================      
CREATE PROCEDURE [dbo].[sp_GetAdditionalFeesAndCharges]
    (
      @LoanNumber VARCHAR(10)
    )
AS 
    
    BEGIN      
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
	SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,         
                   -- the entire transaction is terminated and rolled back.


        SELECT  LoanApp.LoanNumber ,
                ClosingCost.borroweramt ,
                ClosingCost.HUDLinenum ,
                ClosingCost.Payee ,  
                ClosingCost.BorrowerPOC ,
                ClosingCost.HUDDesc ,
                CAST(ClosingCost.HUDLinenum AS MONEY) AS lineorder ,
                LoanApp.TransType AS TransactionType
        FROM    mwlloanapp LoanApp ,
                mwlloandata LoanData ,
                mwlclosingcost ClosingCost
        WHERE   LoanApp.ID = LoanData.objOwner_ID
				AND LoanData.Active = 1 
                AND LoanData.ID = ClosingCost.loanDAta_ID
                AND LoanApp.LoanNumber = @LoanNumber
                AND CAST(ClosingCost.HUDLinenum AS MONEY) >= 1300 -- HUD Line series start from 
                AND CAST(ClosingCost.HUDLinenum AS MONEY) <= 2000 -- HUD Line series upto
        UNION
        SELECT  LoanApp.LoanNumber ,
                OtheCred.borroweramt ,
                OtheCred.HUDLinenum ,
                OtheCred.Payee ,  
                OtheCred.BorrowerPOC ,
                OtheCred.HUDDesc ,
                CAST(OtheCred.HUDLinenum AS MONEY) AS lineorder ,
                LoanApp.TransType AS TransactionType
        FROM    mwlloanapp LoanApp ,
                mwlloandata LoanData ,
                mwlothercredit OtheCred
        WHERE   LoanApp.ID = LoanData.objOwner_ID
				AND LoanData.Active = 1 
                AND LoanData.ID = OtheCred.loanDAta_ID
                AND LoanApp.LoanNumber = @LoanNumber
                AND CAST(OtheCred.HUDLinenum AS MONEY) >= 201-- HUD Line series start from 
                AND CAST(OtheCred.HUDLinenum AS MONEY) <= 212-- HUD Line series upto
        ORDER BY lineorder     
    END

-- Create Procedure Coding ENDS Here

GO

