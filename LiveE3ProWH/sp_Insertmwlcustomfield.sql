USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insertmwlcustomfield]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insertmwlcustomfield]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Nitin  
-- Create date: 10/06/2012  
-- Description: Procedure for clspipelineE3 BLL  
-- =============================================  
  
CREATE procEDURE [dbo].[sp_Insertmwlcustomfield]  
(  
--@ID varchar(32),  
@LoanApp_ID varchar(32),  
@CustFieldDef_ID varchar(32),  
@StringValue varchar(256)  
  
)  
AS  
BEGIN  
  
SET NOCOUNT ON;  
if not exists(select * From mwlcustomfield where LoanApp_id in(@LoanApp_ID) and CustFieldDef_ID=@CustFieldDef_ID)
begin 
Insert into mwlcustomfield(ID,LoanApp_ID,CustFieldDef_ID,StringValue)   
Values(replace(newid(),'-',''),@LoanApp_ID,@CustFieldDef_ID,@StringValue)  
END
ELSE
BEGIN
UPDATE mwlcustomfield SET StringValue=@StringValue WHERE LoanApp_id=@LoanApp_ID AND CustFieldDef_ID=@CustFieldDef_ID
END
END  
  
  
GO

