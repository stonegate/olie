USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanAppid]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanAppid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author: <Nitin>
-- Create date: <>
-- Description: <Procedure for clsOrderFHAE3>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanAppid]
(
@LoanNo varchar(32)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
SET NOCOUNT ON;

SELECT Id FROM Mwlloanapp WHERE LoanNumber =@LoanNo


END
GO

