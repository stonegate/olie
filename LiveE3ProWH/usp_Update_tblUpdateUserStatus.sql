USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Update_tblUpdateUserStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Update_tblUpdateUserStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_Update_tblUpdateUserStatus]
(
@_userid varchar(50)
)
as
begin
	update tblusers set IsActive = 0 where userid = @_userid
end



GO

