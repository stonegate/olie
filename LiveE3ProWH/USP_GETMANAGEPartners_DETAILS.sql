USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GETMANAGEPartners_DETAILS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GETMANAGEPartners_DETAILS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================

-- Author:		<Abhishek Karn>

-- Create date: <08-01-2014>

-- Description:	<This is to Get Details Related to Manage Partners and  DOO Email Notification Setting >

-- =============================================

CREATE PROCEDURE [dbo].[USP_GETMANAGEPartners_DETAILS]

(

@Flag varchar(20),

@PartnerCompanyId varchar(100) = null,

@RoleId int  = 1,

@ControlName varchar(30)  = '',

@ChannelType varchar(10)  = 'TPO',

@UserId  bigint = 1,

@LoanNumber varchar(15) = ''
)

AS

BEGIN


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
	if(@Flag = 'AllDetails')

	BEGIN



		Select distinct TU.PartnerCompanyId,

	   (Select top 1 T.CompanyName from tblUsers T  where TU.PartnerCompanyId = T.PartnerCompanyId) CompanyName

	   ,

	   CASE WHEN urole ='3' THEN 'BROKER' WHEN urole = '20' THEN 'CORRESPOND' ELSE 'HYBRID' END ChannelType,

	   PartnerType,CAST(isnull(CASE WHEN IsEmergingBanker = 'Y' THEN '1' ELSE '0' END ,'0') as  bit) IsDooEmail,

	   cast(0 as bit) IsEPF,

	   (Select top 1 T.OrgSystem from tblUsers T  where TU.PartnerCompanyId = T.PartnerCompanyId  order by T.OrgSystem desc) OrgSystem

	   --,OrgSystem 

	   from tblUsers TU

	   LEFT JOIN (Select * from tblEmergingBankers where Flag = 'DOO' and Status = 'Y') DOO

	   ON TU.PartnerCompanyId = DOO.PartnerCompanyId

	    where 

	   chkaccuratedocs = 1  and  isnull(PartnerType,'') <> '' 

	   and urole in(3,20,21)  order by 2

	END



	   ELSE IF @Flag = 'PartnerDetails'

	   BEGIN

		Select distinct TU.PartnerCompanyId,

	   (Select top 1 T.CompanyName from tblUsers T  where TU.PartnerCompanyId = T.PartnerCompanyId) CompanyName

	   ,

	   CASE WHEN urole ='3' THEN 'BROKER' WHEN urole = '20' THEN 'CORRESPOND' ELSE 'HYBRID' END ChannelType,

	   PartnerType,CAST(isnull(CASE WHEN IsEmergingBanker = 'Y' THEN '1' ELSE '0' END ,'0') as  bit) IsDooEmail,  '0' IsEPF,

	   (Select top 1 T.OrgSystem from tblUsers T  where TU.PartnerCompanyId = T.PartnerCompanyId  order by T.OrgSystem desc) OrgSystem,urole	   

	   from tblUsers TU

	   LEFT JOIN (Select * from tblEmergingBankers where Flag = 'DOO' and Status = 'Y') DOO

	   ON TU.PartnerCompanyId = DOO.PartnerCompanyId

	   where  chkaccuratedocs = 1  and  isnull(PartnerType,'') <> '' and urole in(3,20,21) and  TU.PartnerCompanyId = @PartnerCompanyId

	   END

	   ELSE IF @Flag = 'IsDOOEmail'

	   BEGIN

			IF(@RoleId != 3 AND @RoleId != 21)
			BEGIN				
				select top 1 @PartnerCompanyId = companyemail from mwlinstitution where ObjOwnerName='Broker' AND isnull(companyemail,'') <> '' AND ObjOwner_id = (select ID from mwlloanapp where loannumber = @LoanNumber) 
			END

			Declare @IsDOOEmail varchar(2) 

			SET	   @IsDOOEmail = (Select IsEmergingBanker   from tblEmergingBankers DOO where Flag = 'DOO' and Status = 'Y' and PartnerCompanyId = @PartnerCompanyId)

			Select  isnull(@IsDOOEmail,'N') IsDOOEmail

	   END


	   ELSE IF @Flag = 'ISPermission'

	   BEGIN

				Declare @IsPermission varchar(10) 

				Declare @TabId int

				SET @TabId = (Select top 1 TabId from tblTabs where ltrim(rtrim(ControlName)) = ltrim(rtrim(@ControlName)))

				SET @IsPermission = (Select RoleSetupId from tblRoleSetup where RoleId = @RoleId and TabId = @TabId)		

				Select isnull(CASE WHEN @IsPermission > 0 THEN 'Y' ELSE 'N' END ,'N') IsPermission

	   END



	   ELSE IF @Flag = 'C3Details'

	   BEGIN

Select PartnerCompanyId,CompanyName,ChannelType,PartnerType,CAST(Max(CAST(IsDooEmail as int)) as bit) IsDooEmail,cast(Max(cast(ISEPF as int)) as bit) IsEpf,OrgSystem,urole from 
(
	 Select distinct TU.PartnerCompanyId,

	   (Select top 1 T.CompanyName from tblUsers T  where TU.PartnerCompanyId = T.PartnerCompanyId) CompanyName,

	   CASE WHEN urole ='3' THEN 'BROKER' WHEN urole = '20' THEN 'CORRESPOND' ELSE 'HYBRID' END ChannelType,

		   

	   isnull((Select top 1 T.PartnerType from tblUsers T  where TU.PartnerCompanyId = T.PartnerCompanyId  order by T.PartnerType desc),'') PartnerType,

	   CAST(isnull(CASE WHEN IsEmergingBanker  = 'Y' and Flag = 'C3CCI' THEN '1' ELSE '0' END ,'0') as  bit) IsDooEmail,
	   CAST(isnull(CASE WHEN IsEmergingBanker  = 'Y'  and Flag = 'EPF'   THEN '1' ELSE '0' END ,'0') as  bit) ISEPF,

	   isnull((Select top 1 T.OrgSystem from tblUsers T  where TU.PartnerCompanyId = T.PartnerCompanyId  order by T.OrgSystem desc),'') OrgSystem,urole

	   from (Select distinct  PartnerCompanyId,urole,PartnerType from tblUsers where isnull(PartnerType,'') <> ''  and urole in(3,20,21) ) TU

	   LEFT  JOIN (Select * from tblEmergingBankers where Flag IN('C3CCI','EPF') and Status = 'Y' and IsEmergingBanker = 'Y') C3

	   ON TU.PartnerCompanyId = C3.PartnerCompanyId

	    where    urole in(3,20,21) 
) A  group by PartnerCompanyId,CompanyName,ChannelType,PartnerType,OrgSystem,urole   order by 5 desc ,6 desc ,2

	   END


	   ELSE IF @Flag = 'C3PartnerDetails'

	   BEGIN

	   Select PartnerCompanyId,CompanyName,ChannelType,PartnerType,CAST(Max(CAST(IsDooEmail as int)) as bit) IsDooEmail,cast(Max(cast(ISEPF as int)) as bit) IsEpf,OrgSystem,urole from 
(
	   Select distinct TU.PartnerCompanyId, (Select top 1 T.CompanyName from tblUsers T  where TU.PartnerCompanyId = T.PartnerCompanyId) CompanyName,

	   CASE WHEN urole ='3' THEN 'BROKER' WHEN urole = '20' THEN 'CORRESPOND' ELSE 'HYBRID' END ChannelType,

	   isnull((Select top 1 T.PartnerType from tblUsers T  where TU.PartnerCompanyId = T.PartnerCompanyId  order by T.PartnerType desc),'') PartnerType,

	    CAST(isnull(CASE WHEN IsEmergingBanker  = 'Y' and Flag = 'C3CCI' THEN '1' ELSE '0' END ,'0') as  bit) IsDooEmail,

	    CAST(isnull(CASE WHEN IsEmergingBanker  = 'Y'  and Flag = 'EPF'   THEN '1' ELSE '0' END ,'0') as  bit) ISEPF,

	    isnull((Select top 1 T.OrgSystem from tblUsers T  where TU.PartnerCompanyId = T.PartnerCompanyId  order by T.OrgSystem desc),'') OrgSystem,urole

	    from (Select distinct  PartnerCompanyId,urole,PartnerType from tblUsers where isnull(PartnerType,'') <> ''  and urole in(3,20,21) ) TU

	    LEFT  JOIN (Select * from tblEmergingBankers where Flag IN('C3CCI','EPF') and Status = 'Y' and IsEmergingBanker = 'Y') C3

	    ON TU.PartnerCompanyId = C3.PartnerCompanyId  where  urole in(3,20,21)  and  TU.PartnerCompanyId = @PartnerCompanyId 
		
) A  group by PartnerCompanyId,CompanyName,ChannelType,PartnerType,OrgSystem,urole   order by 2

	    END


	   ELSE IF @Flag = 'IsC3CCI'

	   BEGIN



	   Declare @ISC3CCI varchar(2) 

	   SET	   @ISC3CCI = (Select IsEmergingBanker   from tblEmergingBankers DOO where Flag = 'C3CCI' and Status = 'Y' and PartnerCompanyId = @PartnerCompanyId)

	   Select  isnull(@ISC3CCI,'N') ISC3CCI

	   END



	   ELSE IF @Flag = 'IsUserC3CCI'

	   BEGIN



	   Declare @ISCompanyC3CCI varchar(2) 

	   SET	   @ISCompanyC3CCI = (Select IsEmergingBanker from tblEmergingBankers C3CCI where Flag = 'C3CCI' and Status = 'Y' and PartnerCompanyId = @PartnerCompanyId)

	   Declare @ISUserC3CCI varchar(2) 

	   SET	   @ISUserC3CCI = ( SELECT  CASE WHEN IsC3Integration  = 1 THEN 'Y' ELSE 'N' END from tblUsers  where UserId = @UserId)

	   Select isnull(@ISCompanyC3CCI,'N')+';'+isnull(@ISUserC3CCI,'N') C3Status



	   END


	   
	   ELSE IF @Flag = 'EPF'
	   BEGIN

	   SELECT IsEmergingBanker FROM tblEmergingBankers WHERE PartnerCompanyId = @PartnerCompanyId and  Status = 'Y' and flag='EPF'
	  
	   END

END



GO

