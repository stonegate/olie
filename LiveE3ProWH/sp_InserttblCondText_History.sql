USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InserttblCondText_History]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InserttblCondText_History]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/06/2012
-- Description:	Procedure for clspipelineE3 BLL
-- =============================================
CREATE procEDURE [dbo].[sp_InserttblCondText_History]
(
@Loan_No char(15),
@Cond_Text varchar(1000),
@FileName varchar(200),
@dttime datetime,
@condrecid varchar(50)
)
AS
BEGIN
	
	SET NOCOUNT ON;
Insert into tblCondText_History(Loan_No,Cond_Text,FileName,dttime,condrecid)
values (@Loan_No,@Cond_Text,@FileName,@dttime,@condrecid)
END



GO

