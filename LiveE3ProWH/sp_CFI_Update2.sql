USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CFI_Update2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CFI_Update2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ahishek
-- Create date: 10/18/2012
-- Description:	
-- =============================================
CREATE procedure [dbo].[sp_CFI_Update2]
(
@userid varchar(Max)
)
as
begin
update top (1)  tblTracking set logintime = getdate() where id = (select top 1 id from tblTracking where userid = @userid order by logintime desc)
end



GO

