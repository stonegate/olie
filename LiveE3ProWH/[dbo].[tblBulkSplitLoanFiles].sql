USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBulkSplitLoanFiles]') AND type in (N'U'))
DROP TABLE [dbo].[tblBulkSplitLoanFiles]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBulkSplitLoanFiles](	  [LoanFileId] UNIQUEIDENTIFIER NOT NULL	, [BulkLoanFileId] UNIQUEIDENTIFIER NULL	, [FileName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Type] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ChannelType] VARCHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Status] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FinalStatus] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CompanyName] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RetryCounter] INT NULL	, [RegistrationStatus] BIT NULL	, [Comments] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DuplicateMatches] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LoanNumber] NVARCHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [BorrowerLastName] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PropertyAddress] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [OriginatorLOSLoanNumber] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [TransactionType] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SSNumber] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ProcessStartedOn] DATETIME NULL	, [CreatedBy] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedOn] DATETIME NULL	, [ModifiedBy] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ModifiedOn] DATETIME NULL	, CONSTRAINT [PK_tblBulkSplitLoanFiles] PRIMARY KEY ([LoanFileId] ASC))ALTER TABLE [dbo].[tblBulkSplitLoanFiles] WITH CHECK ADD CONSTRAINT [FK__tblBulkSp__BulkL__54AC64D5] FOREIGN KEY([BulkLoanFileId]) REFERENCES [dbo].[tblBulkLoanFiles] ([BulkLoanFileId])ALTER TABLE [dbo].[tblBulkSplitLoanFiles] CHECK CONSTRAINT [FK__tblBulkSp__BulkL__54AC64D5]USE [HomeLendingExperts]
GO

