USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchTabList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchTabList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_FetchTabList] 
	-- Add the parameters for the stored procedure here
	@urole int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here



	begin
	select tbs.TabName, tbs.ControlName  from tblRoleSetup rs join tbltabs tbs on rs.TabID=tbs.TabID where rs.isActive = 1 and rs.CFI=1 and rs.RoleID = @urole order by TabOrder
	-- rs.CFI=1 Added By Tavant Team for DOO1.2 BRD-099
	end
END


GO

