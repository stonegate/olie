USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCorrManagerView_PurchasePending]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCorrManagerView_PurchasePending]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================       
-- Author:  Ratan Gohil      
-- Create date: 11/01/2013   
-- Name:sp_GetCorrManagerView_PurchasePending.sql
-- Description: To bind the Purchase Pending loans.  
-- =============================================   
     
CREATE PROCEDURE [dbo].[sp_GetCorrManagerView_PurchasePending]
(                
@sortField varchar(500),                
@sortOrder varchar(100),                
@whereClause varchar(1000),                
@PageNo int,                 
@PageSize int,          
          
@CustomField nvarchar(500),          
@CustomFieldN1 nvarchar(500),          
@CustomFieldN2 nvarchar(500),          
@CustomFieldN3 nvarchar(500),        
@CustomFieldN4 nvarchar(500),         
@CustSendToFundingDate nvarchar(500),          
@SubmittedDateID nvarchar(500),          
@SubmittedDateTimeID nvarchar(500),          
          
@CurrentStatus nvarchar(500),          
@URL_From  nvarchar(500), --queryString i.e.agedpursub,agedcloreceived,agedcloreviewed ect.         
--newlyadded        
@CustCorrSpecialistName varchar(50),      
@OriginatorId nvarchar(max)=null,        
@Userole varchar(50)=null,        
@ShowAllData int, -- If 1 than display all data and no count        
 @CustomFieldD1 varchar(250),          
 @CustomFieldD2 varchar(250),          
 @CustomFieldD3 varchar(250),          
 @CustomFieldD4 varchar(250),
 @BranchCode  varchar(250)=null, 
 @userid varchar(50) = null    
)                 
AS
BEGIN     
 /* --------------------------------------------------------------------------------------      
       
 Name:       
  [sp_GetCorrManagerView_pur].sql      
      
 Description:      
  This will get Purchase pending loans          
 
 
 ---------------------------------------------------------------------------------------*/   
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Per E3)      
SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,       
-- the entire transaction is terminated and rolled back. 

-- Step 0.0 - Test Data      
-- sp_GetCorrManagerView_pur '','DESC','','1','50','C4D2D8038F614DF8A1B8A6622C99FC3E','D868983C9162445685B53CFCE4A60628','A0145D5EEA6F40D185806411A3449A25','422F278A1F58445790C4117CDCCFFB97','8DC22470F74841F4880F319268CE67A5','854E49E41BA34B73A3DA46527C97C274','8C9E6032337B40FBA02EB62E98E98BFC','F66B0117FA394644A15C92186D70859A','54 - Purchase Pending','pur','','F4BC6BCB836D4251BA829A79DB70945A','12','1','D70F0DD48CDD443C8F0CC58849EE401F','F1E9047583034EE0A245293DCAE84D95','854652206CE0493384DDE414E3B46E93','94CDFF2EFC16443F90491E494B7FCAD9' 

-- Step 1.0 - get Purchase Pending loans 


DECLARE @custDiligenceLevelID AS VARCHAR(50)  

Declare @ClearPurDateDays smallint 
Declare @NewLoansRangeDate DateTime  
Declare @ClearPurDateDaysOld smallint  
SET @ClearPurDateDays = (select top 1 KeyValue from tblAppSettings where KeyName='ClearedPurDateDays') 	
SET @ClearPurDateDaysOld = (select  top 1 KeyValue from tblAppSettings where KeyName='ClearedPurDateDaysOld')
SET @NewLoansRangeDate = CAST ((select  top 1 KeyValue from tblAppSettings where KeyName='NewloansRangeDate') AS DATETIME)
              
SET @custDiligenceLevelID = (SELECT ID FROM mwsCustFieldDef WHERE MWFieldNum = '9492')    
       


if @Userole = '25'
begin
set @BranchCode='cfi'
end                    

IF @URL_From <> 'sendTofundingMgr' and @URL_From <> 'sendTofunding' and @URL_From <> 'agedpursub' and @URL_From <> 'agedcloreceived' and @URL_From <> 'agedcloreviewed' and @URL_From <> 'agedpurpending'        
begin
	SELECT distinct Loanapp_id into #FundingAppStatus  from mwlAppStatus with (nolock) where StatusDesc='57 - Sent to Funding'                       
end

Declare @sql nvarchar(max)  

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#tempdata') AND TYPE IN (N'U'))
	begin
			drop table #tempdata
    end         
               
set @sql = ''                                            
     set @sql = 'Select * into #tempdata from ( '                 
set @sql =  @sql + 'SELECT ''E3'' as DB,CustDate.DateValue as LastSubmittedDate,    
(CASE when (LockRecord.DeliveryOption = ''Mandatory'' ) THEN Null else lap2.StatusDateTime End )as StatusDateTime,  
    (CASE when (LockRecord.DeliveryOption = ''Mandatory'' ) THEN Null else 
	(case when LockRecord.LockExpirationDate > DATEADD(day,'+ cast(@ClearPurDateDays as nvarchar) + ',lap2.StatusDateTime ) then  LockRecord.LockExpirationDate
	when ( lap2.StatusDateTime >'''+ convert(varchar(10), @NewLoansRangeDate, 101)  +''') then DATEADD(day,'+ cast(@ClearPurDateDays as nvarchar) + ',lap2.StatusDateTime ) else DATEADD(day, ' + cast(@ClearPurDateDaysOld as nvarchar )+',lap2.StatusDateTime) end ) End )    
 as ClearedForPurchaseDate,      
  (CASE when (LockRecord.DeliveryOption = ''Mandatory'' ) THEN Null else ISNULL(CustCRD.DateValue,CustCRD.DateValue) End ) as ConditionReviewDate,
  -- ISNULL(CustCRD4.DateValue,ISNULL(CustCRD3.DateValue,ISNULL(CustCRD2.DateValue,ISNULL(CustCRD.DateValue,CustCRD.DateValue)))) End ) as ConditionReviewDate,
  
CustTime.StringValue as LastSubmittedTime ,Offices.Office as office, Corres.Company as coname,        
BOffices.Office as Boffice,loanapp.CurrDecStatusDate as Decisionstatusdate,loanapp.CurrPipeStatusDate as CurrentStatusDate ,  loanapp.ChannelType as ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,        
CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,        
CASE TransType when NULL THEN '''' WHEN ''P'' THEN ''Purchase money first mortgage'' WHEN ''R'' THEN ''Refinance'' WHEN ''2'' THEN ''Purchase money second mortgage'' WHEN ''S'' THEN ''Second mortgage, any other purpose'' WHEN ''A'' THEN ''Assumption'' WHEN   
''HOP'' THEN ''HELOC - other purpose'' WHEN ''HP'' THEN ''HELOC - purchase'' ELSE '''' END AS TransType,        
mwllookups.displaystring,-- Newly added        
Case LoanData.FinancingType WHEN ''F'' THEN ''FHA''WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN ''VA'' End as LoanProgram,        
loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockDatetime as LockDatetime,LockRecord.LockExpirationDate,LockRecord.DeliveryOption,        
UnderwriterName as UnderWriter,Broker.Company as BrokerName,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,        
Loandata.LoanProgDesc as ProgramDesc,        
CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE''         
and a.objOwner_ID IN (SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0        
WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'')         
and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0 ELSE (SELECT count(a.ID) as TotalCleared         
FROM dbo.mwlCondition a where (DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') and a.objOwner_ID IN         
(SELECT ID FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') and a.objOwner_ID   
IN ( SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER        
,BOffices.Office as OFFICE_NAME1,isnull(Cust.StringValue,'''') as CorrespondentType ,       
-- ,CASE when (CustN4.StringValue IS null or CustN4.StringValue=''Other'') THEN ( CASE when (CustN3.StringValue IS null or CustN3.StringValue=''Other'') THEN ( case when (CustN2.StringValue IS null or CustN2.StringValue=''Other'') then ( 
 CASE when (CustN1.StringValue  IS null or CustN1.StringValue=''Other'') then '''' else CustN1.StringValue end as CorrespodentSpecialist ,
 -- else CustN2.StringValue end ) else CustN3.StringValue End ) else CustN4.StringValue End as CorrespodentSpecialist,
 COALESCE(Upper(Diligence.StringValue),'''') AS DilligenceLevel '      
        
        
  IF @URL_From = 'closing' Or @URL_From = 'creview' Or @URL_From = 'pursub'      
 Begin        
  set @sql =  @sql + ' ,CASE when (CorrespondentSpecialistName.StringValue IS null or CorrespondentSpecialistName.StringValue=''Other'') THEN '''' else CorrespondentSpecialistName.StringValue End as CorrespodentFSpecialist  '         
 End      
 ELSE      
 Begin        
  set @sql =  @sql + ' ,'''' as CorrespodentFSpecialist '         
 End      
       
    
    
 IF @URL_From = 'pur'    
 Begin    
set @sql =  @sql + ' ,(case when ((select top 1 CASE when (Cust4.updatedondate IS null) THEN (CASE when (Cust3.updatedondate IS null ) THEN (case when (Cust2.updatedondate IS null ) then (case when (Cust1.updatedondate IS null ) then NULL else Cust1.updatedondate end )  
 else       
Cust2.updatedondate end )          
else Cust3.updatedondate End ) else Cust4.updatedondate End as AssignedDatetime from mwlloanapp as la           
left join mwlcustomfield as Cust1 on la.id=Cust1.Loanapp_id and Cust1.CustFieldDef_ID = '''+@CustomFieldD1+'''      
left join mwlcustomfield as Cust2 on  la.id=Cust2.Loanapp_id and  Cust2.CustFieldDef_ID = '''+ @CustomFieldD2+'''          
left join mwlcustomfield as Cust3 on  la.id=Cust3.Loanapp_id and  Cust3.CustFieldDef_ID = '''+@CustomFieldD3+'''       
left join mwlcustomfield as Cust4 on  la.id=Cust4.Loanapp_id and Cust4.CustFieldDef_ID = '''+@CustomFieldD4+'''    
     
  
where la.loannumber=loanapp.LoanNumber) < (select  top 1 mwlConditionState.updatedondate from mwlLoanApp, mwlCondition, mwlConditionState where mwlLoanApp.ID = mwlCondition.ObjOwner_ID           
AND mwlCondition.ID = mwlConditionState.ObjOwner_ID          
AND mwlLoanApp.LoanNumber = loanapp.LoanNumber and mwlConditionState.state = ''REJECTED'' )) then 1 else 0 end) as IsRejected '         
 End     

 IF((@Userole='12' or @Userole='25')  and @BranchCode!='')
 begin    
	set @sql =  @sql + ' from mwlLoanApp loanapp Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and loanapp.mwbranchCode like ''%'+ @BranchCode +'%'' and borr.Sequencenum=1 Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id '
 end
 else IF(@Userole='12')
 begin    
	set @sql =  @sql + ' from mwlLoanApp loanapp Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and (loanapp.mwbranchCode like ''%tpo%'' or loanapp.mwbranchCode like ''%cfi%'') and borr.Sequencenum=1 Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id '
 end
 else
 begin
	set @sql =  @sql + ' from mwlLoanApp loanapp Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and borr.Sequencenum=1 Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id'          
 end
 
 set @sql =  @sql + ' left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id         
and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
left join (select LockExpirationDate,loanapp_id,[Status],DeliveryOption,LockDatetime from mwlLockRecord  where LockType=''LOCK'' and [Status] = ''CONFIRMED'' and [Status] <>''CANCELED'') LockRecord
on LockRecord.loanapp_id= loanapp.id 
--left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK'' and LockRecord.Status<>''CANCELED''         
Left join dbo.mwlInstitution as Offices on Offices.ObjOwner_id = loanapp.id and Offices.InstitutionType = ''Broker'' and Offices.objownerName=''Broker''         
Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = loanapp.id and COffices.InstitutionType = ''CORRESPOND'' and COffices.objownerName=''Contacts''         
left join mwllookups on mwllookups.Bocode = loanapp.transtype and mwllookups.objectname = ''mwlloanapp'' and mwllookups.fieldname = ''TransType''        
Left join (select * from dbo.mwlInstitution where InstitutionType = ''Branch'' and objownerName=''BranchInstitution'' ) as BOffices on BOffices.ObjOwner_id = loanapp.id
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''        
left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id and Cust.CustFieldDef_ID =''' +@CustomField+ '''        
left join mwlcustomfield as CustN1 on CustN1.loanapp_id = loanapp.id and CustN1.CustFieldDef_ID =''' +@CustomFieldN1+ '''        
left join mwlcustomfield as CustN2 on CustN2.loanapp_id = loanapp.id and CustN2.CustFieldDef_ID =''' +@CustomFieldN2+ '''        
left join mwlcustomfield as CustN3 on CustN3.loanapp_id = loanapp.id and CustN3.CustFieldDef_ID =''' +@CustomFieldN3+ '''        
left join mwlcustomfield as CustN4 on CustN4.loanapp_id = loanapp.id and CustN4.CustFieldDef_ID =''' +@CustomFieldN4+ '''        
left join mwlcustomfield as CustDate on CustDate.loanapp_id = loanapp.id and CustDate.CustFieldDef_ID =''' +@SubmittedDateID+ '''        
left join mwlcustomfield as CustTime on CustTime.loanapp_id = loanapp.id and CustTime.CustFieldDef_ID =''' +@SubmittedDateTimeID+ '''   


left join mwlcustomfield as CustCRD on CustCRD.Loanapp_id = loanapp.id and CustCRD.CustFieldDef_ID = '''+@CustomFieldD1+'''   
left join mwlcustomfield as CustCRD2 on CustCRD2.Loanapp_id = loanapp.id and CustCRD2.CustFieldDef_ID = '''+@CustomFieldD2+'''   
left join mwlcustomfield as CustCRD3 on CustCRD3.Loanapp_id = loanapp.id and CustCRD3.CustFieldDef_ID = '''+@CustomFieldD3+'''   
left join mwlcustomfield as CustCRD4 on CustCRD4.Loanapp_id = loanapp.id and CustCRD4.CustFieldDef_ID = '''+@CustomFieldD4+'''   



inner join (SELECT MIN(StatusDateTime) as StatusDateTime,Loanapp_id,StatusDesc from mwlAppStatus group by Loanapp_id,StatusDesc) lap2 on  lap2.Loanapp_id=loanapp.id and StatusDesc=''54 - Purchase Pending'' 

left join #FundingAppStatus as FundingAppStatus on  FundingAppStatus.Loanapp_id=loanapp.id 


LEFT JOIN mwlCustomField as Diligence ON Diligence.loanapp_id = loanapp.id AND Diligence.CustFieldDef_ID = '''+@custDiligenceLevelID+''' '  
        
          
IF @URL_From <> 'sendTofundingMgr' AND @URL_From <> 'sendTofunding' AND @URL_From <> 'agedpursub' AND @URL_From <> 'agedcloreceived'  AND @URL_From <> 'agedcloreviewed' AND @URL_From <> 'agedpurpending'        
Begin          
 --set @sql =  @sql + ' where CurrentStatus  IN (''' +@CurrentStatus+ ''') '          
 set @sql =  @sql + ' Where CurrentStatus  IN (select * from dbo.SplitString('''+@CurrentStatus +''','','')) 
 
 AND FundingAppStatus.Loanapp_id IS NULL  

 '          
END          
        
IF @URL_From = 'sendTofundingMgr' OR @URL_From = 'sendTofunding'        
Begin        
 set @sql =  @sql + ' and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' '        
End        
Else        
Begin        
 set @sql =  @sql + ' and ChannelType in(''CORRESPOND'') and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' 
 '           
End        
 ----for director of wholesale        
if @Userole ='1'         
begin        
  set @sql =  @sql + ' and ChannelType in(''CORRESPOND'') and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' and   (Originator_id in (select * from dbo.SplitString('''+@OriginatorId +''','','')))'         
end   
  else   
 if @Userole ='12'          
 begin                  
   set @sql =  @sql + ' and ChannelType in(''CORRESPOND'',''RETAIL'',''BROKER'')'                  
 end                           
         
else   
if @Userole ='2'         
 begin                  
   set @sql =  @sql + ' and ChannelType in(''CORRESPOND'') and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''                   
  and   (Originator_id in (select distinct E3userid from tblusers where urole=2 and userid in ('''+@userid+''')))'               
 end               
else   
if ((@Userole ='8' OR  @Userole ='32' ) and @OriginatorId <> '')        
 begin                  
   set @sql =  @sql + ' and ChannelType in(''CORRESPOND'') and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''                   
  and   (Originator_id in (select * from dbo.SplitString('''+@OriginatorId +''','','')))'               
 end    
 else if ((@Userole ='8' OR  @Userole ='32' ) and @OriginatorId = null)
 begin
  set @sql =  @sql + ' and ChannelType in(''CORRESPOND'') and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' '
  end     
                 

--IF @whereClause <> ''                                                     
--BEGIN  
--PRINT @whereClause                                               
--   set @sql = @sql  + ' AND ' + @whereClause                                           
--END                                  
                         
 set @sql = @sql + ' group by loanapp.mwbranchCode, LockRecord.DeliveryOption,CustDate.DateValue,Offices.Office,Corres.Company,BOffices.Office,loanapp.CurrDecStatusDate , lap2.StatusDateTime, CustCRD.DateValue, CustCRD2.DateValue,CustCRD3.DateValue,CustCRD4.DateValue,
 loanapp.CurrPipeStatusDate,loanapp.ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID,LoanNumber,CloserName,borr.lastname,          
 loanapp.CurrentStatus,mwllookups.DisplayString,TransType,LoanData.FinancingType,loanData.AdjustedNoteAmt,LockRecord.Status,LockRecord.LockDatetime,LockRecord.LockExpirationDate, UnderwriterName,          
 Broker.Company,borr.CompositeCreditScore ,Loandata.LoanProgDesc,CustTime.StringValue,Cust.StringValue,CustN1.StringValue,CustN2.StringValue,CustN3.StringValue,CustN4.StringValue,LockRecord.DeliveryOption,Diligence.StringValue '        
IF @URL_From = 'sendTofundingMgr' OR @URL_From = 'sendTofunding'        
Begin        
 set @sql =  @sql + ',SendtoFundingDate.DateValue'        
End        
 IF @URL_From = 'closing' Or @URL_From = 'creview' Or @URL_From = 'pursub'        
Begin        
 set @sql =  @sql + ',CorrespondentSpecialistName.StringValue'        
End          
  set @sql = @sql + ' )as t2' 

    
	  if @URL_From <> 'reg' and @URL_From <> 'cleared'
    BEGIN
		Set @sql = @sql + ' Delete #tempdata where DeliveryOption = ''Mandatory'' '
	END

Set @sql = @sql + ' ;WITH q AS (
  Select loan_no,  ROW_NUMBER() OVER (PARTITION BY loan_no ORDER BY LockDateTime desc) RN
   from #tempdata    
)
Delete q where RN > 1;'

Declare @whereClauseTemp varchar(max)
Set @whereClauseTemp = ''

IF @whereClause <> ''                                                     
BEGIN  
PRINT @whereClause                                               
  set @whereClauseTemp = ' Where ' + @whereClause                                           
END  

  Set @sql = @sql + ' ;WITH q AS (
  Select loan_no, loan_amt, ROW_NUMBER() OVER (PARTITION BY loan_no ORDER BY LockDateTime desc) RN
   from #tempdata' + @whereClauseTemp + ')
   Select count(1),ceiling(sum(loan_amt)) as TotalAmt, ceiling(sum(loan_amt)/count(*)) as [avg] from q
where RN = 1; '


IF @sortField  = ''                                            
  set @sortField = 'CurrentStatusDate'     
                 
             
SET @sortField = ' ORDER BY ' + @sortField + ' ' + @sortOrder   

set @sql = @sql + ' ;WITH q As ( SELECT ROW_NUMBER() OVER (ORDER BY @@rowcount) AS [ROW],* From #tempdata ' + @whereClauseTemp + ' ) Select * from q ' 

          
   
IF @ShowAllData = 0

	BEGIN
	--	IF @whereClauseTemp <> ''
	--		set @sql = @sql + ' AND '
	--	ELSE 
	--	   set @sql = @sql + ''

		set @sql = @sql + ' Where [ROW] between (' + CAST( @PageNo AS NVARCHAR)  + ' - 1) * ' + Cast(@PageSize as nvarchar) + ' + 1 and ' + cast(@PageNo as nvarchar) + ' * ' + cast(@PageSize as nvarchar)			
	END

Else

BEGIN	
	set @sql = @sql + ''
END

set @sql  = @sql + @sortField

exec sp_executesql @sql

IF @URL_From <> 'sendTofundingMgr' and @URL_From <> 'sendTofunding' and @URL_From <> 'agedpursub' and @URL_From <> 'agedcloreceived' and @URL_From <> 'agedcloreviewed' and @URL_From <> 'agedpurpending'        
begin
	Drop table #FundingAppStatus 
end

                
END


GO

