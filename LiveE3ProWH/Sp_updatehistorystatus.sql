USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_updatehistorystatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_updatehistorystatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateHistoryStatus]    Script Date: 06/20/2013 19:16:10 ******/

CREATE PROCEDURE [dbo].[Sp_updatehistorystatus] @SessionID VARCHAR(10) 
AS 
  BEGIN        
      SET xact_abort ON; 

      UPDATE tbldootolerancehistory WITH (UPDLOCK)
      SET    isactive = 0 
      WHERE  sessionid = @SessionID 
  END        

-- Create Procedure Coding ENDS Here

GO

