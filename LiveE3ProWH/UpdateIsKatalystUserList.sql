USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateIsKatalystUserList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateIsKatalystUserList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================        
-- Author:  Shwetal Shah        
-- Create date: 10/03/2013        
-- Description: Assign katalsyt On/Off at Partner Company id          
-- =============================================   
CREATE PROCEDURE [dbo].[UpdateIsKatalystUserList]
(
@Val bit,
@UserID int
)
AS
BEGIN

	Update tblUsers WITH (UPDLOCK) Set IsKatalyst = @Val Where PartnerCompanyId = (select PartnerCompanyID from tblusers where userid = @UserID)
	
END



GO

