USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCRSMAssociation]') AND type in (N'U'))
DROP TABLE [dbo].[tblCRSMAssociation]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCRSMAssociation](	  [ID] UNIQUEIDENTIFIER NOT NULL	, [CRSMUserId] INT NOT NULL	, [AssociateUserId] INT NULL	, [AssociateRoleId] INT NULL	, [PartnerCompanyId] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedBy] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedDate] DATETIME NULL)USE [HomeLendingExperts]
GO

