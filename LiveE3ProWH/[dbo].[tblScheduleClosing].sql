USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblScheduleClosing]') AND type in (N'U'))
DROP TABLE [dbo].[tblScheduleClosing]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblScheduleClosing](	  [ScheduleID] INT NOT NULL IDENTITY(1,1)	, [LoanNo] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SchDate] DATETIME NULL	, [FeeSheet] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [HomeownerInsurance] VARCHAR(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [TitleCommitment] VARCHAR(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [InsuredClosingProtectionLetter] VARCHAR(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RateLockConfirmation] VARCHAR(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Invoices] VARCHAR(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FinalLocked] VARCHAR(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Userid] INT NULL	, [IsActive] BIT NULL DEFAULT('True')	, [SubmitedDate] DATETIME NULL	, [Comments] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsFromLoanSuspendedDoc] BIT NULL DEFAULT((0))	, [Time] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RushStatus] BIT NOT NULL DEFAULT((0))	, [ReDrawStatus] BIT NOT NULL DEFAULT((0))	, [AppStatus] BIT NULL DEFAULT((0)))USE [HomeLendingExperts]
GO

