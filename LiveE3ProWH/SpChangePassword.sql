USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpChangePassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpChangePassword]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SpChangePassword] 
	@UserId int,
	@ChangePassowed nvarchar(1000)
AS
BEGIN
	update tblusers set upassword =@ChangePassowed where userid = @UserId
END
GO

