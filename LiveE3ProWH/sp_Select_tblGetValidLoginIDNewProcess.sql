USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetValidLoginIDNewProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetValidLoginIDNewProcess]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblGetValidLoginIDNewProcess]
(
@sUserpassword varchar(Max)
,@sEmail varchar(Max)
)
as
begin
select userid from tblusers where upassword=@sUserpassword and uemailid = @sEmail and IsActive=1
end


GO

