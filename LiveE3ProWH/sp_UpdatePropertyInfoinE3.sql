USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdatePropertyInfoinE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdatePropertyInfoinE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Stonegate>
-- Create date: <10/9/2012>
-- Description:	<This will Update Property InfoinE3>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdatePropertyInfoinE3]
(
	@strAddress varchar(500),
	@strCity varchar(200),
	@strState varchar(200),
	@strZip varchar(25),
	@strLoanappId varchar(100)
)
AS
BEGIN

Update 
	mwlSubjectProperty 
set 
	street = @strAddress,
	City = @strCity,
	State = @strState,
	zipCode = @strZip 
where 
	LoanApp_id = @strLoanappId

END

GO

