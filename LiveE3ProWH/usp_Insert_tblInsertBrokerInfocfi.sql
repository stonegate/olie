USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Insert_tblInsertBrokerInfocfi]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Insert_tblInsertBrokerInfocfi]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Insert_tblInsertBrokerInfocfi]
	-- Add the parameters for the stored procedure here
	(  
@ufirstname varchar(Max)=null  
,@ulastname varchar(Max)=null  
,@uemailid varchar(Max)=null  
,@urole varchar(Max)=null  
,@isadmin varchar(Max)=null  
,@isactive varchar(Max)=null  
,@uidprolender varchar(Max)=null  
,@uparent varchar(Max)=null  
,@upassword varchar(Max)=null  
,@mobile_ph varchar(Max)=null  
,@phonenum varchar(Max)=null  
,@loginidprolender varchar(Max)=null  
,@carrierid varchar(Max)=null  
,@MI varchar(Max)=null  
,@companyname varchar(Max)=null  
,@AEID varchar(Max)=null  
,@Region varchar(Max)=null  
,@E3Userid varchar(Max)=null  
,@PartnerCompanyid varchar(Max)=null  
,@BrokerType varchar(Max)=null  
,@PasswordExpDate varchar(Max)=null  
)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into tblusers  
(  
ufirstname  
,ulastname  
,uemailid  
,urole  
,isadmin  
,isactive  
,uidprolender  
,uparent  
,upassword  
,mobile_ph  
,phonenum  
,loginidprolender  
,carrierid  
,MI  
,companyname  
,AEID  
,Region  
,E3Userid  
,PartnerCompanyid  
,BrokerType  
,PasswordExpDate  

)  
Values  
(  
@ufirstname  
,@ulastname  
,@uemailid  
,@urole  
,@isadmin  
,@isactive  
,@uidprolender  
,@uparent  
,@upassword  
,@mobile_ph  
,@phonenum  
,@loginidprolender  
,@carrierid  
,@MI  
,@companyname  
,@AEID  
,@Region  
,@E3Userid  
,@PartnerCompanyid  
,@BrokerType  
,@PasswordExpDate  

)  
END


GO

