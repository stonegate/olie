USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_UpdateclsForm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_UpdateclsForm]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_UpdateclsForm]
(
@Description varchar(200)= null,
@FileName varchar(200)= null,
@Isnews bit = null,
@CateID int = null,
@Formid int= null
)
AS
BEGIN
 Update tblCFIForms Set Description =@Description, FileName = @FileName,
	   isnews = @Isnews, CateID = @CateID 
	  Where formID  = @Formid
End

GO

