USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TblOnHoldReason]') AND type in (N'U'))
DROP TABLE [dbo].[TblOnHoldReason]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblOnHoldReason](	  [LoanNumber] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [OnHoldReason] VARCHAR(2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [userid] BIGINT NULL	, [Date] DATETIME NULL DEFAULT(getdate()))USE [HomeLendingExperts]
GO

