USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUserStates]') AND type in (N'U'))
DROP TABLE [dbo].[tblUserStates]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserStates](	  [UserStateId] INT NOT NULL IDENTITY(1,1)	, [UserId] INT NOT NULL	, [StateId] INT NOT NULL	, [CreatedBy] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [CreatedOn] DATETIME NOT NULL	, [ModifiedBy] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ModifiedOn] DATETIME NULL	, CONSTRAINT [PK_tblUserStates] PRIMARY KEY ([UserStateId] ASC))ALTER TABLE [dbo].[tblUserStates] WITH CHECK ADD CONSTRAINT [FK_tblUserStates_tblState] FOREIGN KEY([StateId]) REFERENCES [dbo].[tblState] ([state_id])ALTER TABLE [dbo].[tblUserStates] CHECK CONSTRAINT [FK_tblUserStates_tblState]ALTER TABLE [dbo].[tblUserStates] WITH CHECK ADD CONSTRAINT [FK_tblUserStates_tblUsers] FOREIGN KEY([UserId]) REFERENCES [dbo].[tblUsers] ([userid])ALTER TABLE [dbo].[tblUserStates] CHECK CONSTRAINT [FK_tblUserStates_tblUsers]USE [HomeLendingExperts]
GO

