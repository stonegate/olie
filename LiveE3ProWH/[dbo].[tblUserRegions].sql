USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUserRegions]') AND type in (N'U'))
DROP TABLE [dbo].[tblUserRegions]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserRegions](	  [UserRegionId] INT NOT NULL IDENTITY(1,1)	, [UserId] INT NOT NULL	, [RegionId] INT NOT NULL	, [CreatedBy] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [CreatedOn] DATETIME NOT NULL	, [ModifiedBy] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ModifiedOn] DATETIME NULL	, CONSTRAINT [PK_tblUserRegions] PRIMARY KEY ([UserRegionId] ASC))ALTER TABLE [dbo].[tblUserRegions] WITH CHECK ADD CONSTRAINT [FK_tblUserRegions_tblRegion] FOREIGN KEY([RegionId]) REFERENCES [dbo].[tblRegion] ([Id])ALTER TABLE [dbo].[tblUserRegions] CHECK CONSTRAINT [FK_tblUserRegions_tblRegion]ALTER TABLE [dbo].[tblUserRegions] WITH CHECK ADD CONSTRAINT [FK_tblUserRegions_tblUsers] FOREIGN KEY([UserId]) REFERENCES [dbo].[tblUsers] ([userid])ALTER TABLE [dbo].[tblUserRegions] CHECK CONSTRAINT [FK_tblUserRegions_tblUsers]USE [HomeLendingExperts]
GO

