USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_LoanRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_LoanRegister]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_Insert_LoanRegister]
(
@borrLast varchar(100),
@transType int,
@loanAmt numeric(14,2),
@loanPgm varchar(20),
@userid int,
@filetype varchar(50) ,
@filename varchar(100)
)
as
begin
	Insert into tblLoanReg(borrLast,transType,loanAmt,loanPgm,userid,filetype,filename)
values(@borrLast,
@transType,
@loanAmt,
@loanPgm ,
@userid,
@filetype ,
@filename
)
end

GO

