USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetUserRolesForConversion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetUserRolesForConversion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblGetUserRolesForConversion]
as
begin
select * from tblroles where id not in (4,5,6,7,9,13,3,20,21,23)
end


GO

