USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateLockConfirmQueue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateLockConfirmQueue]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_UpdateLockConfirmQueue]
(
	@Id bigint,
	@LoanAppid varchar(50),
	@StatusID INT
)
as
begin
update tblLockConfirmQueue set StatusID=@StatusID where LoanApp_ID =@LoanAppid and Id=@Id
end

GO

