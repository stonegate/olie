USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetNewsAnnouncementList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetNewsAnnouncementList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetNewsAnnouncementList]
	
AS
BEGIN
	select * from tblNewsAndAnnouncement order by createddate desc
END

GO

