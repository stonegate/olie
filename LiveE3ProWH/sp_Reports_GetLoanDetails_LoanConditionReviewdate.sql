USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Reports_GetLoanDetails_LoanConditionReviewdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Reports_GetLoanDetails_LoanConditionReviewdate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                  
-- Author:  Sonu Gupta                  
-- Create date: January, 21 2013                  
-- Description: This will give LoanConditionReviewdate for the loan number and CustomeFieldConditionReviewDate passed        --exec [sp_Reports_GetLoanDetails_LoanConditionReviewdate] '0000340749 ', 'D70F0DD48CDD443C8F0CC58849EE401F'                 
-- =============================================                  
         
 CREATE      
 PROCEDURE [dbo].[sp_Reports_GetLoanDetails_LoanConditionReviewdate]                  
(                  
@LoanNumber nvarchar(50),              
@CustomeFieldConditionReviewDate nvarchar(50)               
)                  
AS                  
BEGIN                  
SET NOCOUNT ON; 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
SET XACT_ABORT ON;                 
select DateValue from mwlcustomfield Inner Join mwlLoanApp on mwlLoanApp.ID = mwlcustomfield.LoanApp_ID where mwlcustomfield.custfielddef_id =@CustomeFieldConditionReviewDate  and mwlLoanApp.LoanNumber=@LoanNumber              
               
END 
GO

