USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_insert_tblusers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_insert_tblusers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[SP_insert_tblusers](
@ufirstname VARCHAR(100),
@ulastname VARCHAR(200),
@uemailid VARCHAR(100),
@StateBelongTo VARCHAR(50),
@urole int,
@isadmin bit,
@isactive bit,
@upassword VARCHAR(50),
@UserLoginID varchar(50),
@carrierid VARCHAR(5),

@CompanyName varchar(50),
@AEID Varchar(50),
@Region varchar(50),
@E3UserID varchar(50),
@PartnerCompanyid varchar(50), 

@CreatedBy varchar(50),
@loginidE3 varchar(50),
@IsUserInNewProcess bit,
@Userid int OUTPUT)
AS
BEGIN
 INSERT INTO tblusers (ufirstname,ulastname,uemailid,StateBelongTo,urole,isadmin,isactive,
  upassword,UserLoginID,carrierid,CompanyName,AEID,Region,E3UserID,PartnerCompanyid,PasswordExpDate,CreatedBy,loginidProlender,IsUserInNewProcess )
 VALUES
  (@ufirstname,@ulastname,@uemailid,@StateBelongTo,@urole,@isadmin,@isactive,
  @upassword,@UserLoginID,@carrierid,@CompanyName,@AEID,@Region,@E3UserID,@PartnerCompanyid,getdate(),@CreatedBy,@loginidE3,@IsUserInNewProcess)
  
 SET @Userid = SCOPE_IDENTITY() 
END


GO

