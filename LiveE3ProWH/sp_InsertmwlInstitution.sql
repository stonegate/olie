USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertmwlInstitution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertmwlInstitution]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Nitin
-- Create date: 10/06/2012
-- Description:	Procedure for clspipelineE3 BLL
-- =============================================
CREATE procEDURE [dbo].[sp_InsertmwlInstitution]
(

@ObjOwner_id varchar(1000) =null,
@Office varchar(100) =null,
@CompanyEmail varchar(500)=null,
@Company varchar(200)=null,
@FullAddress varchar(1000)=null,
@Street varchar(100)=null,
@Street2 varchar(100)=null,
@City varchar(100)=null,
@State varchar(100)=null,
@ZipCode varchar(100)=null,
@institutiontype varchar(100)=null,
@objOwnerName varchar(20)=null
)
AS
BEGIN
	
	SET NOCOUNT ON;
Insert into mwlInstitution
(ID,ObjOwner_id,Office,CompanyEmail,Company,FullAddress,Street,Street2,City,State,ZipCode,institutiontype,objOwnerName)
 Values(replace(newid(),'-',''),@ObjOwner_id,@Office,@CompanyEmail,@Company,@FullAddress,@Street,@Street2,@City,@State,@ZipCode,@institutiontype,@objOwnerName)
   
END



GO

