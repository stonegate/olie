USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUserDetails]') AND type in (N'U'))
DROP TABLE [dbo].[tblUserDetails]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserDetails](	  [UserDetailsID] INT NOT NULL IDENTITY(1,1)	, [UsersID] INT NULL	, [ChangeUsername] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SecurityQuestion1] INT NULL	, [Answer1] VARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SecurityQuestion2] INT NULL	, [Answer2] VARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SecurityQuestion3] INT NULL	, [Answer3] VARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MaxSecurityAttempt] INT NULL	, [lastlogintime] DATETIME NULL	, CONSTRAINT [PK_tblUserDetails] PRIMARY KEY ([UserDetailsID] ASC))USE [HomeLendingExperts]
GO

