USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetFundingReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetFundingReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Stonegate>
-- Create date: <10/5/2012>
-- Description:	<This will generate Funding Report>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetFundingReport] 
(
	@RoleID INT
	,@strFromDate VARCHAR(20) = NULL
	,@strToDate VARCHAR(20) = NULL
	,@strChannel VARCHAR(30) = NULL
	,@strTeamOPuids NVARCHAR(max) = NULL
	,@strID NVARCHAR(max) = NULL
	,@company NVARCHAR(max) = NULL
	)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;--This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)

	DECLARE @SQL NVARCHAR(max)

	SET @SQL = ''
	SET @SQL = 
		'Select ''E3'' as DB, loanapp.ChannelType as BusType,loanapp.ID as record_id,LoanNumber as loan_no,borr.lastname as BorrowerName,loanapp.DecisionStatus,loanapp.CurrentStatus as LoanStatus,
case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,
case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as LoanPurpose,CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed,
LoanData.AdjustedNoteAmt as LoanAmount, CONVERT(DECIMAL(15,2),(ISNULL(LoanData.LTV,0) * 100))LTV,CONVERT(DECIMAL(10,3),(LoanData.NoteRate * 100)) as int_rate,LoanData.ShortProgramName as prog_code,
CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as brok_name ,CASE ISNULL(RTRIM(convert(varchar(35), AppStatus2.StatusDateTime,101)),'''') WHEN '''' THEN RTRIM(convert(varchar(35),AppStatus.StatusDateTime,101)) ELSE RTRIM(convert(varchar(35),AppStatus2.StatusDateTime,101)) END as FundedDate,'''' as  LU_FINAL_HUD_STATUS
from mwlLoanApp loanapp Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active=1
Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' 
Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc=''62 - Funded'' and AppStatus.sequencenum=1
Left join mwlappstatus as AppFunding on AppFunding.LoanApp_id=loanapp.id and AppFunding.StatusDesc=''90 - Funding Reversed''
Left join mwlappstatus as AppStatus2 on AppStatus2.LoanApp_id=loanapp.id and AppStatus2.StatusDesc=''62 - Funded'' and AppStatus2.sequencenum = 2 
and AppStatus2.StatusDateTime > AppFunding.StatusDateTime
where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''
and ( AppStatus.StatusDateTime between '''+ @strFromDate + ''' and DATEADD(d, 1, ''' + @strToDate + ''') or AppFunding.StatusDateTime between ''' + @strFromDate + ''' and DATEADD(d, 1, ''' + @strToDate + ''') or AppStatus2.StatusDateTime between ''' + @strFromDate + ''' and DATEADD(d, 1, ''' + @strToDate + ''') ) '

	IF (@strChannel = 'broker')
	BEGIN
		SET @SQL = @SQL + ' and loanapp.ChannelType=''BROKER'' '
	END

	IF (@strChannel = 'retail')
	BEGIN
		SET @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL'' '
	END

	IF (@strChannel = 'correspond')
	BEGIN
		SET @SQL = @SQL + ' and loanapp.ChannelType=''CORRESPOND'' '
	END

	IF (@strTeamOPuids <> '')
	BEGIN
		SET @SQL = @SQL + ' and Originator_id in(select * from dbo.SplitString(''' + @strTeamOPuids + ''','','')) order by AppStatus.StatusDateTime '
	END
	ELSE
	BEGIN
		IF (@RoleID <> 0)
		BEGIN
			IF (LEN(@strID) = 0)
			BEGIN
				IF (@RoleID = 13)
				BEGIN
					SET @SQL = @SQL + ' and LD.LT_Broker_Rep in (select * from dbo.SplitString(''' + @strID + ''','','')) order by LD2.doc_funded_date '
				END

				IF (@RoleID = 3	OR @RoleID = 20	OR @RoleID = 21)
				BEGIN
					SET @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString(''' + @strID + ''','',''))) '
				END
				ELSE
				BEGIN
					SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @strID + ''','',''))) order by AppStatus.StatusDateTime '
				END
			END
			ELSE
			BEGIN
				IF (@RoleID = 13)
				BEGIN
					SET @SQL = @SQL + ' and LD.LT_Broker_Rep in (select * from dbo.SplitString(''' + @strID + ''','','')) order by LD2.doc_funded_date '
				END
				ELSE IF (@RoleID = 3 OR @RoleID = 20 OR @RoleID = 21)
				BEGIN
					SET @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString(''' + @strID + ''','',''))) '
				END
				ELSE IF (@RoleID = 12)
				BEGIN
				     IF (@company <> '' AND @company IS NOT NULL)
				     BEGIN
					      SET @SQL = @SQL + 'and (Broker.company IN(' + @company + ') OR Corres.company IN(' + @company + '))'
				     END
					SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) ) '
				END
				ELSE
				BEGIN
					SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')))  '
				END
			END

			IF (@RoleID = 32 OR @RoleID = 1	OR @RoleID = 8 OR @RoleID = 2)
			BEGIN
				IF (@company <> '' AND @company IS NOT NULL)
				BEGIN
					
					SET @SQL = @SQL + 'and (Broker.company IN (' + @company + ') OR Corres.company IN (' + @company + '))'
									END
			END
		END
	END

	PRINT @SQL

	EXEC sp_executesql @SQL
END

GO

