USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetReportClosingForSearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetReportClosingForSearch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetReportClosingForSearch]
(          
@Puid nvarchar(max),      
@Role varchar(50),        
@CurrentStatus nvarchar(max),
@ChannelType  nvarchar(max) = NULL,
@SearchText varchar(100)
)          
AS          
BEGIN          

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

Declare @SQL nvarchar(max)
SET @SQL = ''

SET @Role  =  (CASE WHEN @Role = '38' THEN '0' ELSE @Role END)
SET @SQL = ' select distinct top 100 ''E3'' as DB,PROPERTY.STATE AS PropertyState,Loandata.loanprogramname as CustLoanProgram,CustEscrowAmt.currencyValue AS EscrowAmt,loanapp.ChannelType,loanapp.OriginatorName,loanapp.DecisionStatus, loanapp.ID as record_id,LoanNumber as loan_no,borr.lastname as BorrowerLastName,loanapp.DecisionStatus,loanapp.CurrentStatus as LoanStatus,DateAppSigned as 
SubmittedDate,Loandata.LoanProgramName as LoanProgram,  
 case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,borr.CompositeCreditScore as CreditScoreUsed,loanData.AdjustedNoteAmt as LoanAmount,  
 convert(varchar(35),EstCloseDate,101) AS ScheduleDate,LockStatus,loanapp.LockExpirationDate,UnderwriterName as UnderWriter,CloserName as Closer,Institute.Company as BrokerName, Institute.CustomDataOne as TitleCompany,
 AppStatus.StatusDateTime as DocsSentDate,FundedStatus.StatusDateTime as FundedDate,'''' as LU_FINAL_HUD_STATUS,loanapp.Channeltype as BusType,Cust.stringvalue as UnderwriterType , LockRecord.DeliveryOption,  
 CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'' AND Category !=''Correspondent - 1'' and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0 
 WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'' AND Category !=''Correspondent - 1'' )  and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'' )  
 and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE (SELECT count(a.ID) as TotalCleared 
  FROM dbo.mwlCondition a where (DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'' AND Category !=''Correspondent - 1'' ) and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'' ) and a.objOwner_ID IN    
  (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID)as TotalConditions FROM dbo.mwlCondition a where ( DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'' AND Category  !=''Correspondent - 1'' ) and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER, loanapp.Originator_id AEId 
  ,BOffices.Office TPORegion  
  from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
  Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id   
  left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
Left join dbo.mwlInstitution as Institute on Institute.ObjOwner_id = loanapp.id and 
InstitutionType = loanapp.Channeltype
Left join (Select distinct Office,ObjOwner_id from dbo.mwlInstitution Where InstitutionType = ''Branch'' and objownerName=''BranchInstitution'') as BOffices on BOffices.ObjOwner_id = loanapp.id  
Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and AppStatus.StatusDesc like ''%Docs Out%''   
Left join mwlappstatus as FundedStatus on FundedStatus.LoanApp_id=loanapp.id  and FundedStatus.StatusDesc like ''%Funded''   
left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID =''C4D2D8038F614DF8A1B8A6622C99FC3E'' 
LEFT JOIN mwlsubjectproperty Property ON PROPERTY.Loanapp_id=loanapp.id
left join mwlcustomfield as CustEscrowAmt on CustEscrowAmt.loanapp_id = loanapp.id And CustEscrowAmt.CustFieldDef_ID =''8460093CE5984EDB8652D11E8F8AEE5A''
left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType = ''LOCK'' and  LockRecord.Status <> ''CANCELED'' 
where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' and CurrentStatus IN (select * from dbo.SplitString(''' + @CurrentStatus + ''','','')) '   

if (@Role != '0')        
begin      
if (len(@Puid) = 0)          
Begin     
if ((@Role = '1' OR @Role = '2' OR @Role = '8' OR @Role = '32') AND (@ChannelType <> '' AND @ChannelType IS NOT NULL))
begin
	SET @SQL = @SQL + ' and Channeltype IN (select * from dbo.SplitString('''+@ChannelType+''','','')) ' 
end      
if (@Role = '13' )       
begin
SET @SQL = @SQL + ' and loandat.LT_Broker_Rep in (select * from dbo.SplitString(''' + @Puid + ''','',''))'  
end      
else if   (@Role = '2' )   

begin
SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @Puid + ''','','')) '        
end   
else if   (@Role = '3' or @Role = '20' or @Role = '21' )   
begin
SET @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString(''' + @Puid + ''','',''))) '    
end   
else  
begin
SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @Puid + ''','',''))) '  
end  
End         
else      
begin      
if (@Role = '13')       
begin
SET @SQL = @SQL + ' and loandat.LT_Broker_Rep in (select * from dbo.SplitString(''' + @Puid + ''','',''))'      
end     

else if   (@Role = '3' or @Role = '20' or @Role = '21' )   
begin    
IF( @Role = '20')
BEGIN
SET @SQL = @SQL + ' and Channeltype=''CORRESPOND'''
END
SET @SQL = @SQL + ' and (Institute.CompanyEmail IN(''' + @Puid + ''') OR Institute.CompanyEmail IN(''' + @Puid + '''))'
end    
ELSE
IF ((@Role = '1' OR @Role = '2' OR @Role = '8' OR @Role = '32' ) AND (@ChannelType <> '' AND @ChannelType IS NOT NULL))
BEGIN
	SET @SQL = @SQL + ' and Channeltype IN (select * from dbo.SplitString('''+@ChannelType+''','','')) and (Originator_id in (select * from dbo.SplitString('''+@Puid+''','','')))' 
END    
else  
begin
SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @Puid + ''','',''))) '  
end    
end   
end
SET @SQL = @SQL + ' and (loanapp.LoanNumber like ''%' + @SearchText + '%'' OR borr.lastname like ''%' + @SearchText + '%'' 
OR loanapp.CurrentStatus like ''%' + @SearchText + '%'' OR TransType like ''%' + @SearchText + '%'' 
OR Institute.CustomDataOne =''' + @SearchText + ''') order by CurrentStatus desc'

print @SQL
EXEC sp_executesql @SQL        
END



GO

