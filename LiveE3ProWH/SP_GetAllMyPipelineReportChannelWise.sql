USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetAllMyPipelineReportChannelWise]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetAllMyPipelineReportChannelWise]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

CREATE PROCEDURE [dbo].[SP_GetAllMyPipelineReportChannelWise] 
(
@iUserID int,
@Channel varchar(max),
@ConditionType varchar(500),
@Waived varchar(500),
@Registered varchar(500),
@oRole varchar(500),
@oRoleId varchar(max) = null,
@StatusString nvarchar(max)
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

Declare @SQL varchar(8000)
Set @SQL = ''
Set @SQL = ' select ''E3'' as DB,loanapp.Channeltype as BusType, loanapp.DecisionStatus,
             loanapp.casenum as FHANumber,loanapp.ID as record_id,'' as LT_USR_ASSGNDRAWER,
             LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,
             loanapp.CurrentStatus as LoanStatus,'' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS
             SubmittedDate,approvalStat.StatusDateTime as DateApproved,case when TransType is null 
             then '' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' 
             then ''Refinance'' end as TransTYpe,LookupsPro.DisplayString as LoanProgram,
             loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,
             LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter, 
             CASE ISNULL(RTRIM(Broker.Company),'') WHEN '' THEN RTRIM(Corres.Company) ELSE
             RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut
             ,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,
             convert(varchar(35),EstCloseDate,101) AS ScheduleDate , CASE WHEN (SELECT count(a.ID) as 
             TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and
             Category !=''LENDER'' 
             and Category !='+@ConditionType+'
             and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE
             loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0 WHEN (SELECT count(a.ID) as TotalCleared FROM 
             dbo.mwlCondition a where (DueBy= ''Prior to Closing'' and Category !=''LENDER''
             and Category !=' +@ConditionType +')
             and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or 
             CurrentState='+ @Waived+')
             and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE 
             loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE (SELECT count(a.ID) as TotalCleared 
             FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER''
             and Category !='+@ConditionType +') 
             and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or 
             CurrentState='+@Waived+') and a.objOwner_ID IN 
             (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) 
             * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where 
             ( DueBy=''Prior to Closing'' and Category !=''LENDER'' and
              Category !='+@ConditionType +') 
              and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE 
              loan1.LoanNumber=LoanApp.LoanNumber))END AS PER from mwlLoanApp  loanapp
              Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  Inner join dbo.mwlloandata as
              loanData on loanData.ObjOwner_id = loanapp.id  left join dbo.mwlUnderwritingSummary as uSummary 
              on uSummary.LoanApp_id = loanapp.id  Left join dbo.mwlInstitution as Broker on
              Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and
              Broker.objownerName=''Broker''left join dbo.mwlLockRecord as LockRecord 
             on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' and
             LockRecord.Status<>''CANCELED'' Left join mwlAppStatus appStat on 
             loanapp.ID=appStat.LoanApp_id and 
             appStat.StatusDesc='+@Registered+'
             Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and
             approvalStat.StatusDesc =''Approved'' Left outer join mwlLookups Lookups on 
             LoanData.refipurpose=Lookups.BOCode and ObjectName=''mwlLoanData'' and fieldname=''refipurpose'' 
             Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and
             Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''
              Left outer join mwlLookups LookupsPro on LoanData.FinancingType=LookupsPro.BOCode 
            and LookupsPro.ObjectName=''mwlLoanData'' and LookupsPro.fieldname=''FinancingType''  
             where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''  and 
            loanapp.ChannelType like ''%RETAIL%'' and 
            CurrentStatus  IN (select * from dbo.SplitString('+@StatusString+','',''))'''
           if (@oRole= '12')
              Begin  
                set  @SQL   = @SQL+' and (Originator_id in 
                (select * from dbo.SplitString('+@oRoleId+','','')) )'
             end
          set  @SQL   = @SQL +'(DueBy=''Prior to Closing'' and Category !=''LENDER''
          and Category !=' +@ConditionType + ')  
          and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or 
          CurrentState='+ @Waived+')'

Print @SQL 
exec sp_executesql @SQL
END

-- Create Procedure Coding ENDS Here

GO

