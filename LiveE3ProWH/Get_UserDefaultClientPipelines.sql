USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_UserDefaultClientPipelines]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Get_UserDefaultClientPipelines]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Get_UserDefaultClientPipelines]

	@AEIds NVARCHAR(MAX),

	@Uroles NVARCHAR(MAX) = NULL

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;	

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT DISTINCT	

		U.CompanyName As ClientName

	FROM [dbo].[tblUsers] U

	WHERE U.AEID IN (SELECT * FROM dbo.SplitString(@AEIds, ','))

		AND (@Uroles IS NULL OR (U.urole IN (SELECT * FROM dbo.SplitString(@Uroles, ','))))

		AND U.IsActive = 1

END


GO

