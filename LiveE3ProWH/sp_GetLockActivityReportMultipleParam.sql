USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLockActivityReportMultipleParam]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLockActivityReportMultipleParam]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:    <Stonegate>
-- Create date: <10/5/2012>
-- Description:	<This will Get Lock Activity Report>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLockActivityReportMultipleParam] 
(
	@RoleID INT
	,@strTeamOPuids NVARCHAR(max) = NULL
	,@strID NVARCHAR(max) = NULL
	,@strFromDate VARCHAR(50) = NULL
	,@strToDate VARCHAR(50) = NULL
	,@ChannelType VARCHAR(max) = NULL
	,@company VARCHAR(max)=null
	)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;--This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)

	DECLARE @SQL NVARCHAR(max)

	SET @SQL = ''
	SET @SQL = 
		'Select ''E3'' as DB, loanapp.ID as record_id,loanapp.Channeltype as BusType,LoanNumber as loan_no,
borr.lastname as BorrowerLastName,CurrentStatus as LoanStatus,convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,
LoanData.LoanProgramName as Prog_desc,
case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as Purpose,
CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed,(Loandata.NoteRate * 100) as Int_rate,
loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,convert(varchar(40),LockRecord.LockExpirationDate,101) as LockExpires,
LockRecord.LockDateTime,convert(varchar(35),LockRecord.LockDateTime,101) as Lockdate,UnderwriterName as UnderWriter,CloserName as Closer,
CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,'''' as FundedDate,'''' as ReceivedInUW,
LoanData.ShortProgramName as prog_code
from mwlLoanApp  loanapp
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED''
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''
where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' '

    if(@RoleID = 32 OR @RoleID = 1 OR @RoleID = 8 OR @RoleID = 2 OR @RoleID = 12)
	BEGIN
	IF(@ChannelType <> ''	and @ChannelType IS NOT NULL	)
	Begin
	SET @SQL = @SQL + ' and (loanapp.ChannelType IN(select * from dbo.SplitString(''' + @ChannelType + ''','','')))'
	ENd
	if(@company <> '' and @company is not null)
	Begin
	SET @SQL = @SQL + ' and (Broker.company IN('+ @company +') OR Corres.company IN('+ @company +'))'
	end
	END

	--IF ((@ChannelType <> ''	OR @ChannelType IS NOT NULL	)AND (@RoleID = 32 OR @RoleID = 1 OR @RoleID = 8 OR @RoleID = 2))
	--BEGIN
	--	SET @SQL = @SQL + ' and (loanapp.ChannelType IN(select * from dbo.SplitString(''' + @ChannelType + ''','','')))'
	--END

	IF (@strTeamOPuids <> '')
	BEGIN
		SET @SQL = @SQL + ' and Originator_id in (select * from dbo.SplitString(''' + @strTeamOPuids + ''','','')) '
	END
	ELSE
	BEGIN
		IF (@RoleID != 0)
		BEGIN
			IF (LEN(@strID) = 0)
			BEGIN
				IF (@RoleID = 13)
				BEGIN
					SET @SQL = @SQL + ' where loandat.LT_Broker_Rep in 
                                 (select * from dbo.SplitString(''' + @strID + ''','','')) and locks.LOCK_DATE between' + @strFromDate + ' and ' + @strToDate + ' '
				END
				ELSE IF (@RoleID = 3 OR @RoleID = 20 OR @RoleID = 21)
				BEGIN
					SET @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString(''' + @strID + ''','','')) '
				END
				ELSE
				BEGIN
					SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @strID + ''','',''))) '
				END
			END
			ELSE
			BEGIN
				IF (@RoleID = 13)
				BEGIN
					SET @SQL = @SQL + ' where loandat.LT_Broker_Rep in (select * from dbo.SplitString(''' + @strID + ''','',''))
                                    and locks.LOCK_DATE between' + @strFromDate + ' and ' + @strToDate + ' '
				END
				ELSE IF (@RoleID = 3 OR @RoleID = 20 OR @RoleID = 21)
				BEGIN
					SET @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString(''' + @strID + ''','',''))) '
				END
				ELSE IF (@RoleID = 12)
				BEGIN
					SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) ) '
				END
				ELSE
				BEGIN
					SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @strID + ''','',''))) '
				END
			END
		END
	END

	SET @SQL = @SQL + ' and LockRecord.LockDateTime between''' + @strFromDate + ''' and ''' + @strToDate + ''' '

	PRINT @SQL

	EXEC sp_executesql @SQL
END

GO

