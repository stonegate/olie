USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetNewConditionByDueByKatalyst]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetNewConditionByDueByKatalyst]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Vipul,,Thacker>
-- Create date: <10 March 2013>
-- Description:	<This Stored procedure used for Get conditition type from E3 DB >
---Modify By: <Vipul, Thacker>
-- Modify date: <09 April 2013>
-- =============================================
CREATE PROCEDURE [dbo].[GetNewConditionByDueByKatalyst]
(
@LoanNo varchar(25),
@DueBy varchar(100)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	SELECT Top(1) ID,Description as cond_text FROM dbo.mwlCondition WITH (nolock) Where ObjOwner_ID = (Select Id from mwlLoanApp WITH (nolock) where LoanNumber = @LoanNo)
		And DueBy = @DueBy and CurrentState = 'NEW' and Category <> 'Lender'
		
END


GO

