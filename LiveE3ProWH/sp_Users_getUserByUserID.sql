USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Users_getUserByUserID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Users_getUserByUserID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Vipul,,Thacker>
-- Create date: <10 March 2013>
-- Description:	<This Stored procedure used for check for Katalayst flag>
---Modify By: <Vipul, Thacker>
-- Modify date: <04 April 2013>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Users_getUserByUserID]      
 -- Add the parameters for the stored procedure here      
 @UserID int      
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
      
    -- Insert statements for procedure here      
 select chkpricelockloan,chkQuickPricer,chkSubmitloan, chkSubmitcondition,chkRegisterloan, chkorderappraisal, chkorderfhano, chkscheduleclosingdate, chkupdateloan, chkSubmitClosingPackage,iskatalyst chkIsKatalyst ,
 chklockconfirmation,chkPurchaseAdvice  -- Added by tavant For the BRD 129 and 215
 From tblusers  WITH (nolock) where userid=@UserID      
END 


GO

