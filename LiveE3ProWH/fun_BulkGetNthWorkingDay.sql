USE [HomeLendingExperts]
								GO
								SET ANSI_NULLS ON
								GO
								SET QUOTED_IDENTIFIER ON
								GO

-- =============================================
-- Author:		<suvarna>
-- Create date: <05/05/2014>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fun_BulkGetNthWorkingDay] (@nthWorkingDay INT)
RETURNS DATETIME
AS
BEGIN
	DECLARE @NthBusinessDate VARCHAR(20)
	DECLARE @StartDate DATETIME
		,@EndDate DATETIME


			SELECT @StartDate = convert (varchar(10) , getdate(), 110),
				@EndDate = DATEADD(mm, -1, @StartDate)  --PRINT @StartDate;PRINT @EndDate;;
			;WITH Yak(theDate) AS (
					SELECT @StartDate
					
					UNION ALL
					
					SELECT DATEADD(DAY, -1, theDate)
					FROM Yak
					WHERE theDate > @EndDate
					)
			SELECT @NthBusinessDate=y.theDate
			FROM Yak AS y
			LEFT JOIN (
				SELECT theDate AS WorkDate
					,ROW_NUMBER() OVER (
						 ORDER BY theDate DESC
						) AS nthBusinessDay
				FROM Yak
				WHERE DATENAME(WEEKDAY, theDate) NOT IN (
						'Saturday'
						,'Sunday'
						)
					AND thedate NOT IN (
						SELECT Convert(VARCHAR(10), HolidayDate, 112) AS HolidayDate 
						FROM [tblHolidayList]
						WHERE HolidayDate BETWEEN @EndDate
								AND  @StartDate
						)
				) AS w ON w.WorkDate = y.theDate
			WHERE w.nthBusinessDay = @nthWorkingDay
			ORDER BY y.theDate DESC
			OPTION (MAXRECURSION 0)
			
			Return @NthBusinessDate
END


GO

