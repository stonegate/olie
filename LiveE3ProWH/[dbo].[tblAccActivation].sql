USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAccActivation]') AND type in (N'U'))
DROP TABLE [dbo].[tblAccActivation]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAccActivation](	  [Id] INT NOT NULL IDENTITY(1,1)	, [UStatus] BIT NULL	, [UDate] DATETIME NULL	, CONSTRAINT [PK_tblAccActivation] PRIMARY KEY ([Id] ASC))USE [HomeLendingExperts]
GO

