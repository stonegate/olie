USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblFetchDataTeambyLoanOfficer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblFetchDataTeambyLoanOfficer]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblFetchDataTeambyLoanOfficer]
(
@iBrokerID varchar(max),
@urole varchar(Max)
)
as
begin
select *, 'btnAddClick(' + convert(nvarchar,userid) + ')' as Cmd from tblusers 
where isActive=1 and BrokerID =@iBrokerID And urole=@urole
end


GO

