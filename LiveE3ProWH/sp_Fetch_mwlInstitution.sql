USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Fetch_mwlInstitution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Fetch_mwlInstitution]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procEDURE  [dbo].[sp_Fetch_mwlInstitution]

AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

   select Office from mwlInstitution 
			where InstitutionType in('CORRESPOND','Broker','Branch') 
				and office is not null 
				and office <> ''	
			    group by office
END


GO

