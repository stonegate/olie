USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SelectFile_tblLoanReg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SelectFile_tblLoanReg]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_SelectFile_tblLoanReg]
(@userid int,
@filename varchar(100)
)
as
begin
	select count(*) TotalFile from tblLoanReg where userid =@userid and filename=@filename
 
end




GO

