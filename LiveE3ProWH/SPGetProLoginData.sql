USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPGetProLoginData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPGetProLoginData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Stonegate>  
-- Create date: <10/5/2012>  
-- Description: <Fetch Login Data>  
-- =============================================  
CREATE PROCEDURE [dbo].[SPGetProLoginData]   
 @LodingId varchar(500)  
AS  
BEGIN  
 select * from users where [users].Login =@LodingId  
END  
GO

