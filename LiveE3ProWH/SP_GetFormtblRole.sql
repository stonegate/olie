USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetFormtblRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetFormtblRole]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create Procedure Coding Starts Here

CREATE PROCEDURE [dbo].[SP_GetFormtblRole]--'3,20,21'
(
@id varchar(Max)
)
AS
BEGIN

SELECT STUFF((SELECT ', ' + roles FROM tblroles 
WHERE id in (select * from SplitString(@id,','))
FOR XML PATH('')),1,1,'') AS Roles 
END

GO

