USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_tblUserGuide]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_tblUserGuide]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  CREATE proc [dbo].[sp_Update_tblUserGuide]    
(    
@ID varchar(50),    
@IsActive bit    
)    
as    
begin    
 update tblUserGuide WITH (UPDLOCK) set IsActive = @IsActive where ID = @ID    
end    

GO

