USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_GetTranscationtype]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_GetTranscationtype]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_Select_GetTranscationtype] 
AS
BEGIN
	select 0 as lookupid, '--Select Type--' as Description union select lookupid,Description  from tbllookups where lookupname ='TT'	
END


GO

