USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_UpdateUserStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_UpdateUserStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		< author name >
-- Create date: 25/10/2012
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[udsp_UpdateUserStatus] 
@userID varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

update tblusers Set isactive = '1' Where userid = @userID
update top (1)  tblTracking set logintime = getdate() where id = (select top 1 id from tblTracking where userid = @userID order by logintime desc)
END

GO

