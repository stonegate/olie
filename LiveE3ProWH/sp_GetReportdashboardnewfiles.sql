USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetReportdashboardnewfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetReportdashboardnewfiles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetReportdashboardnewfiles]--'3755'--143-- '3755'--'2852'--'2852'           
(      
@Userid varchar(max)      
)             
as              
begin   

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
                   
  declare @strid varchar(max)      
  set @strid=(select e3userid from tblusers where userid=@userid)      
      
select DISTINCT top 4      
   mwlBorrower.LastName AS BorrowerLastName,            
     mwlLoanApp.LoanNumber,            
     mwlLoanApp.DecisionStatus,            
     CASE mwlLoanApp.TransType            
      WHEN 'P' THEN 'Purchase'            
      WHEN 'R' THEN 'Refinance'            
     END AS TransType,            
     mwlLoanApp.CurrentStatus AS LoanStatus,            
     mwlLoanApp.OriginatorName AS BDM,            
     CASE ISNULL(RTRIM(Offices.Company),'')            
      WHEN '' THEN RTRIM(Corres.Company)            
      ELSE RTRIM(Offices.Company)            
     END AS TPOPartner,            
     --AppDt.StatusDateTime AS SubmittedDate,            
     InUWDate.StatusDateTime as InUnderwritingDate,      
     mwlLoanApp.LoanCompositeScore AS FICO        
   FROM mwlLoanApp        
   INNER JOIN mwlBorrower ON LoanApp_ID = mwlLoanApp.ID and RLABorrCoborr=1 and Sequencenum=1        
   INNER JOIN mwlLoanData ON mwlLoanData.ObjOwner_ID = mwlLoanApp.ID        
   LEFT JOIN mwlInstitution ON mwlLoanApp.ID = mwlInstitution.ObjOwner_ID and mwlInstitution.InstitutionType='BROKER'        
   LEFT JOIN mwlLockRecord ON mwlLockRecord.LoanApp_ID = mwlLoanApp.ID and mwlLockRecord.LockType='LOCK' and  mwlLockRecord.Status<>'CANCELED'        
   LEFT JOIN mwlUnderwritingSummary ON mwlUnderwritingSummary.LoanApp_ID = mwlLoanApp.ID        
   LEFT JOIN mwaMWUser ON mwaMWUser.ID = mwlLoanApp.Originator_ID        
   LEFT JOIN mwlAppStatus AS SubUW ON SubUW.LoanApp_ID = mwlLoanApp.ID AND SubUW.StatusDesc='25 - Submitted to Underwriting'        
   --LEFT JOIN mwlAppStatus AS AppDt ON AppDt.LoanApp_ID = mwlLoanApp.ID AND AppDt.StatusDesc='01 - Registered'        
   LEFT JOIN mwlAppStatus AS InUWDate ON InUWDate.LoanApp_ID = mwlLoanApp.ID AND InUWDate.StatusDesc='29 - In Underwriting'        
   Left join dbo.mwlInstitution as Offices on Offices.ObjOwner_id = mwlLoanApp.id and Offices.InstitutionType = 'Broker' and Offices.objownerName='Broker'        
   --Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = mwlLoanApp.id and COffices.InstitutionType = 'CORRESPOND' and COffices.objownerName='Contacts'        
   Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = mwlLoanApp.id and BOffices.InstitutionType = 'Branch' and BOffices.objownerName='BranchInstitution'        
   Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = mwlLoanApp.id and Corres.InstitutionType = 'CORRESPOND' and Corres.objownerName='Correspondent'       
   left join mwlcustomfield as Cust on Cust.loanapp_id = mwlLoanApp.id  and Cust.CustFieldDef_ID ='5261689813D9467BA3C980BF871289F1'      
   left join mwlcustomfield as CustTime on CustTime.loanapp_id = mwlLoanApp.id and CustTime.CustFieldDef_ID ='27414DB20AF34DB39BB6903551817FBA'      
   where  CurrentStatus IN ('29 - In Underwriting')  AND (mwlLoanApp.DecisionStatus is null OR mwlLoanApp.DecisionStatus='') --not in ('Suspended','Approved','Denied')      
   --and mwlLoanApp.TransType='P'   
   and mwlUnderwritingSummary.underwriter_id in (select * from dbo.SplitString(@strid,''))                
      order BY InUnderwritingDate asc
end 


GO

