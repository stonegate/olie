USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpUpdateBDM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpUpdateBDM]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpUpdateBDM] 
	@UserId int,
	@AEId varchar(500)
AS
BEGIN
	update tblusers Set AEID = @AEId where userid =@UserId
END

GO

