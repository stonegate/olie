USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetProBrokarLORepId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetProBrokarLORepId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblGetProBrokarLORepId]
(
@sBrokarRepLoginID varchar(Max)
)
as
begin
select record_id  from BROK_LOGINS where login=@sBrokarRepLoginID
end


GO

