USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_Reports_GetLoanDetails_LoanConditionReviewdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_Reports_GetLoanDetails_LoanConditionReviewdate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                
-- Author:  Sonu Gupta                
-- Create date: January, 21 2013                
-- Description: This will give LoanConditionReviewdate for the loan number and CustomeFieldConditionReviewDate passed         
--exec [udsp_Reports_GetLoanDetails_LoanConditionReviewdate] '0000340749 ', 'D70F0DD48CDD443C8F0CC58849EE401F'               
-- =============================================                
       
 CREATE    
 PROCEDURE [dbo].[udsp_Reports_GetLoanDetails_LoanConditionReviewdate]                
(                
@LoanNumber nvarchar(50),            
@CustomeFieldConditionReviewDate nvarchar(50)             
)                
AS                
BEGIN                
      SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
	          
select DateValue from mwlcustomfield Inner Join mwlLoanApp on mwlLoanApp.ID = mwlcustomfield.LoanApp_ID where mwlcustomfield.custfielddef_id =@CustomeFieldConditionReviewDate  and mwlLoanApp.LoanNumber=@LoanNumber            
             
END 
GO

