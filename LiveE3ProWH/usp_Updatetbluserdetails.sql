USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Updatetbluserdetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Updatetbluserdetails]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/09/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[usp_Updatetbluserdetails]
(
@SecurityQuestion1 int,
@Answer1 varchar(250),
@SecurityQuestion2 int,
@Answer2 varchar(250),
@SecurityQuestion3 int,
@Answer3 varchar(250),
@UsersID int
)
as
begin
	update tbluserdetails Set 
                SecurityQuestion1=@SecurityQuestion1,
                Answer1=@Answer1,
                SecurityQuestion2=@SecurityQuestion2,
                Answer2=@Answer2,
                SecurityQuestion3=@SecurityQuestion3,
                Answer3=@Answer3
                 Where UsersID = @UsersID
end




GO

