USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUserRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUserRole]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Stonegate
-- Create date: 10/22/2012
-- Description:	Get User Role
-- =============================================
CREATE PROCEDURE sp_GetUserRole
(
@id int
)as
begin

select * from tblroles where id=@id

end

GO

