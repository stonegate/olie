USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetPropertyInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetPropertyInformation]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Mukesh Kumar>    
-- Create date: <08/29/2012>    
-- Description: < This will return the value for "Docs On OLIE Step-2 Property Information" on the basis of LoanNumber>    
-- =============================================    
CREATE PROCEDURE [dbo].[udsp_GetPropertyInformation]     
 @LoanNumber varchar(10)    
AS    
BEGIN   
  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   
  SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,           
                   -- the entire transaction is terminated and rolled back.   
  
SELECT mwlsubjectproperty.id,   
       mwlsubjectproperty.loanapp_id,   
       mwlloanapp.loannumber,   
       mwlsubjectproperty.street,   
       mwlsubjectproperty.street2,   
       mwlsubjectproperty.city,   
       mwlsubjectproperty.state,   
       mwlsubjectproperty.zipcode,   
       mwlsubjectproperty.county,   
       mwlsubjectproperty.country,   
       mwlsubjectproperty.appraisedvalue,   
       mwlsubjectproperty.propertytype,   
       mwlsubjectproperty.condodevname,   
       mwlsubjectproperty.longlegal,   
       mwlloanapp.transtype,
	   mwlloandata.SalesPrice   
FROM   mwlsubjectproperty   
       INNER JOIN mwlloanapp   
       ON mwlsubjectproperty.loanapp_id = mwlloanapp.id   
	   INNER JOIN mwlloandata ON mwlloanapp.id = mwlloandata.objowner_id and mwlloandata.active = 1 	
WHERE  loannumber = @LoanNumber    
END  
   
  
-- Create Procedure Coding ENDS Here  
GO

