USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetFNMAValue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetFNMAValue]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here
CREATE PROCEDURE [dbo].[SP_GetFNMAValue]
		(
@strLoanNumber nvarchar(200),
@CustomField nvarchar(200)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	select YNValue, *from mwlloanApp loanapp Left join mwlcustomfield as 
    CustFNMA  on CustFNMA.loanapp_id = loanapp.id  and 
    CustFNMA.CustFieldDef_ID=@CustomField where LoanNumber =@strLoanNumber
END

GO

