USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GETRegionalReorganizationsDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_GETRegionalReorganizationsDetails]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[USP_GETRegionalReorganizationsDetails]
(
@Flag varchar(20) ,
@UserId bigint = null
)
--sp_Select_tblGetRegions
AS
BEGIN
if @Flag = 'Region'
BEGIN
select   distinct Office Region  from mwcInstitution 
where isnull(Office,'') <> ''  
and InstitutionType = 'BRANCH'  and Office not like '%Ret%'

order by 1 desc


END 


if @Flag = 'UsersRegion'
BEGIN

SELECT Region FROM tblUserRegionMapping WHERE UserId = @UserId and [Status] = 'Y'

END 


if @Flag = 'AllRegion'
BEGIN

SELECT Region,'E' [RegionStatus]  FROM tblUserRegionMapping WHERE UserId = @UserId and [Status] = 'Y'
Union
select   distinct Office,'N'  from mwcInstitution 
where isnull(Office,'') <> ''  
and InstitutionType = 'BRANCH'  and Office not like '%Ret%'
and Office NOT IN(SELECT Region  FROM tblUserRegionMapping WHERE UserId = @UserId and [Status] = 'Y')

order by 1

END 

END


GO

