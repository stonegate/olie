USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdatemwlloanappCurrentStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdatemwlloanappCurrentStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***** JIRA 116 *****/

CREATE PROCEDURE [dbo].[sp_UpdatemwlloanappCurrentStatus]

(

@LoanNumber  varchar(15),

@CurrentStatus varchar(30)

)



AS

BEGIN



BEGIN TRY

UPDATE mwlLoanApp

SET CurrentStatus=@CurrentStatus

 WHERE LTRIM(RTRIM(LoanNumber)) = LTRIM(RTRIM(@LoanNumber))



 END TRY



 BEGIN CATCH

 

    DECLARE @Msg nvarchar(MAX)

    SELECT @Msg=Error_Message();

    RaisError('Error Occured: %s', 20, 101,@Msg); --With Log;



 END CATCH



END

GO

