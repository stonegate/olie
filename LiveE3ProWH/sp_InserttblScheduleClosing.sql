USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InserttblScheduleClosing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InserttblScheduleClosing]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/06/2012
-- Description:	Procedure for clspipelineE3 BLL
-- =============================================
CREATE procEDURE [dbo].[sp_InserttblScheduleClosing]
(
@LoanNo varchar(50),
@SchDate datetime,
@FeeSheet varchar(100),
@userid int,
@SubmitedDate datetime,
@Comments varchar(1000),
@IsFromLoanSuspendedDoc int

)
AS
BEGIN

	SET NOCOUNT ON;
insert into tblScheduleClosing 
(LoanNo, SchDate, FeeSheet,userid,SubmitedDate, Comments,IsFromLoanSuspendedDoc ) 

values (@LoanNo, @SchDate, @FeeSheet,@userid,@SubmitedDate, @Comments,@IsFromLoanSuspendedDoc) 

END


GO

