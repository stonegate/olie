USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBranchOffice]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBranchOffice]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sushant>
-- Create date: <10/11/2012>
-- Description:	<ClsUserE3.cs>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetBranchOffice]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

    -- Insert statements for procedure here
select  distinct office from mwlInstitution where InstitutionType = 'Branch' and objownerName='BranchInstitution'  and office<>'' order by office
END


GO

