USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_DeleteMandatoryClientsInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_DeleteMandatoryClientsInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Example for execute stored procedure       
--EXECUTE [udsp_DeleteMandatoryClientsInfo] '478452' 
-- =============================================            
-- Author:  Karan Gulati            
-- Create date: 15/04/2013 
-- Name:udsp_DeleteMandatoryClientsInfo.sql       
-- Description: Delete Clients Details for Mandatory-Best Effort Delivery Task            
-- =============================================            
CREATE PROCEDURE [dbo].[udsp_DeleteMandatoryClientsInfo]              
(
  --Parameter declaration   
@ID varchar(max) 
)        
AS            
BEGIN 
SET NOCOUNT ON;            
SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error, 
		           -- the entire transaction is terminated and rolled back.  
		                 
DELETE FROM tblManageMandatoryClientsInfo
WHERE     (PartnerCompanyId IN
                          (SELECT     items
                            FROM          dbo.SplitString(@ID, ',')))                             
END   
GO

