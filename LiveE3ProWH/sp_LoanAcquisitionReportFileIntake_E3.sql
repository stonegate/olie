USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LoanAcquisitionReportFileIntake_E3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LoanAcquisitionReportFileIntake_E3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Vipul Thakkar    
-- Create date: 04, OCT 2012    
-- Description: This will give Loan Information for Loan Acq and Manager role File intake tab  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_LoanAcquisitionReportFileIntake_E3] 
	-- Add the parameters for the stored procedure here


@UserRole int,
@strCorrespondentType varchar(100),
@strApplicationrecStatus VARCHAR(100),
@strConditionType VARCHAR(100),
@CurrentStatus 	varchar(4000),
@strFrom varchar(100)=NULL
AS
BEGIN
    if (@UserRole = 14 OR @UserRole = 16) AND  @strFrom ='file'

begin
    select 'E3' as DB,loanapp.processorname,loanapp.DocumentPreparerName,loanapp.CurrDecStatusDate,LockRecord.LockDateTime,   
                     COffices.Office as coffice,   
                     loanapp.CurrDecStatusDate as Decisionstatusdate,loanapp.CurrPipeStatusDate as CurrentStatusDate ,loanapp.ChannelType as ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID as record_id,'' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'' as LU_FINAL_HUD_STATUS,   
                     appReceived.StatusDateTime as AppDate,   
                     CASE TransType when NULL THEN '' WHEN 'P' THEN 'Purchase money first mortgage' WHEN 'R' THEN 'Refinance' WHEN '2' THEN 'Purchase money second mortgage' WHEN 'S' THEN 'Second mortgage, any other purpose' WHEN 'A' THEN 'Assumption' WHEN 'HOP' THEN 'HELOC - other purpose' WHEN 'HP' THEN 'HELOC - purchase' ELSE '' END AS TransType,   
                     case LoanData.FinancingType WHEN 'F' THEN 'FHA' WHEN 'C' THEN 'CONVENTIONAL' WHEN 'V' THEN  'VA' End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,   
                     LockRecord.LockExpirationDate,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'') WHEN '' THEN RTRIM(COffices.Company) ELSE RTRIM(Broker.Company) END as BrokerName,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,Loandata.LoanProgDesc as ProgramDesc ,Cust.stringvalue as UnderwriterType,   
                     CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy='Prior to Closing' and Category !='LENDER' and Category !=@strConditionType and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0   
                      WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER' and Category !=@strConditionType)  and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived')   
                     and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE (SELECT count(a.ID) as TotalCleared    
                     FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER' and Category !=@strConditionType) and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived') and a.objOwner_ID IN    
                     (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy='Prior to Closing' and Category !='LENDER' and Category !=@strConditionType) and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER    
                     ,BOffices.Office as OFFICE_NAME1    
                     from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and Sequencenum=1  Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id     
                     left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id    
                     Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = 'BROKER' and Broker.objownerName='Broker'    
                     left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id  and  LockRecord.LockType='LOCK' and  LockRecord.Status<>'CANCELED'   
                     Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = loanapp.id and COffices.InstitutionType = 'CORRESPOND' and COffices.objownerName='Contacts'    
                     Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.InstitutionType = 'BRANCH' and BOffices.objownerName='BranchInstitution'      
                     Left join mwlAppStatus appReceived on loanapp.ID=appReceived.LoanApp_id and appReceived.StatusDesc IN (@strApplicationrecStatus)    

                     left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID = @strCorrespondentType   
                     
                       where CurrentStatus  IN (@CurrentStatus)   
                             and loanapp.ChannelType in('CORRESPOND','BROKER')   
                             and isnull(Cust.stringvalue,'')<>'Delegated'  
                            and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '' 
                    ORDER BY PER desc              
                      
                        
                             
        end                     
                   
      ELSE
              begin     
                               
					select 'E3' as DB,loanapp.processorname,loanapp.DocumentPreparerName,loanapp.CurrDecStatusDate,LockRecord.LockDateTime,   
                     COffices.Office as coffice,   
                     loanapp.CurrDecStatusDate as Decisionstatusdate,loanapp.CurrPipeStatusDate as CurrentStatusDate ,loanapp.ChannelType as ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID as record_id,'' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'' as LU_FINAL_HUD_STATUS,   
                     appReceived.StatusDateTime as AppDate,   
                     CASE TransType when NULL THEN '' WHEN 'P' THEN 'Purchase money first mortgage' WHEN 'R' THEN 'Refinance' WHEN '2' THEN 'Purchase money second mortgage' WHEN 'S' THEN 'Second mortgage, any other purpose' WHEN 'A' THEN 'Assumption' WHEN 'HOP' THEN 'HELOC - other purpose' WHEN 'HP' THEN 'HELOC - purchase' ELSE '' END AS TransType,   
                     case LoanData.FinancingType WHEN 'F' THEN 'FHA' WHEN 'C' THEN 'CONVENTIONAL' WHEN 'V' THEN  'VA' End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,   
                     LockRecord.LockExpirationDate,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'') WHEN '' THEN RTRIM(COffices.Company) ELSE RTRIM(Broker.Company) END as BrokerName,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,Loandata.LoanProgDesc as ProgramDesc ,Cust.stringvalue as UnderwriterType,   
                     CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy='Prior to Closing' and Category !='LENDER' and Category !=@strConditionType and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0   
                      WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER' and Category !=@strConditionType)  and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived')   
                     and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE (SELECT count(a.ID) as TotalCleared    
                     FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER' and Category !=@strConditionType) and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived') and a.objOwner_ID IN    
                     (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy='Prior to Closing' and Category !='LENDER' and Category !=@strConditionType) and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER    
                     ,BOffices.Office as OFFICE_NAME1    
                     from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and Sequencenum=1  Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id     
                     left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id    
                     Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = 'BROKER' and Broker.objownerName='Broker'    
                     left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id  and  LockRecord.LockType='LOCK' and  LockRecord.Status<>'CANCELED'   
                     Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = loanapp.id and COffices.InstitutionType = 'CORRESPOND' and COffices.objownerName='Contacts'    
                     Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.InstitutionType = 'BRANCH' and BOffices.objownerName='BranchInstitution'      
                     Left join mwlAppStatus appReceived on loanapp.ID=appReceived.LoanApp_id and appReceived.StatusDesc IN ('" + clsLoanStatusE3.GetStringValue(LoanStatusE3.ApplicationReceived) + "','" + clsLoanStatusE3.GetStringValue(LoanStatusE3.ApplicationRecieved) + "')    

                     left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID = '" + strCorrespondentCustField + "'    
                     
                   where
                             borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '' 
                    ORDER BY PER desc             
                    END
                    
                    
                    END
                    
                    
GO

