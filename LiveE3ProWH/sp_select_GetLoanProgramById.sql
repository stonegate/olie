USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_select_GetLoanProgramById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_select_GetLoanProgramById]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Name  
-- Create date:   
-- Description:   
-- =============================================  
CREATE PROCEDURE [dbo].[sp_select_GetLoanProgramById]
(  
@recordId varchar(Max)
)  
AS  
BEGIN 
select record_id,prog_code,prog_desc from PROGINFO where record_id=@recordId
END  

GO

