USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_GetLockStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_GetLockStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_Select_GetLockStatus] 
AS
BEGIN
	select '0' as id_value, '--Select LockStatus--' as descriptn union  select id_value ,descriptn from lookups where lookup_id='AAT' and descriptn in('REGISTERED','LOCKED')
END


GO

