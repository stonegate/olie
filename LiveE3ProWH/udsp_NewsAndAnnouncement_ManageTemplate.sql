USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_NewsAndAnnouncement_ManageTemplate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_NewsAndAnnouncement_ManageTemplate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  
  
        
-- =============================================                    
-- Author:  Mukesh Kumar                    
-- Create date: 01/31/2013                    
-- Description: To Get the News & Announcement from Manage Template Table                    
-- =============================================                    
CREATE proc [dbo].[udsp_NewsAndAnnouncement_ManageTemplate]    --0                
        
-- Add the parameters for the stored procedure here                    
        
@uRole varchar(2)                    
        
AS                    
BEGIN        
-- SET NOCOUNT ON added to prevent extra result sets from                    
-- interfering with SELECT statements.                    
SET NOCOUNT ON;        
        
SELECT        
 [ID],        
 [Title],        
 [Body],        
 [IsActive],        
 [IsManageTemplate] AS [IsNewsAnnouncement],        
 [CreatedBy],        
 [CreatedDate],        
 '' AS IPAddress,        
 [Role],        
 [RoleC],  
 [RoleR],  
 [ManageTempVisbleAd] AS [NewsAnnousVisbleAd],        
 [ManageTempVisbleDR] AS [NewsAnnousVisbleDR],        
 [ManageTempVisbleBR] AS [NewsAnnousVisbleBR],        
 [ManageTempVisbleLO] AS [NewsAnnousVisbleLO],        
 [ManageTempVisbleCU] AS [NewsAnnousVisbleCU],        
 [ManageTempVisbleRE] AS [NewsAnnousVisbleRE],        
 [ManageTempVisbleRM] AS [NewsAnnousVisbleRM],        
 [ManageTempVisbleDP] AS [NewsAnnousVisbleDP],        
 [ManageTempVisbleCP] AS [NewsAnnousVisbleCP],        
 [ManageTempVisbleHY] AS [NewsAnnousVisbleHY],        
 [ManageTempVisbleDCFI] AS [NewsAnnousVisbleDCFI],        
 [ManageTempVisibleSCFI] AS [NewsAnnousVisbleSCFI],        
 [ManageTempVisbleUM] AS [NewsAnnousVisbleUM],        
 [ModifiedBy],        
 [ModifiedDate]        
 ,convert(bit,0) AS NewsAnnousVisbleAM,        
  convert(bit,0) AS NewsAnnousVisbleDM,        
  convert(bit,0) AS NewsAnnousVisbleROM,        
  convert(bit,0) AS NewsAnnousVisbleCO,        
  convert(bit,0) AS NewsAnnousVisbleCRM,        
  convert(bit,0) AS NewsAnnousVisbleLP,        
  convert(bit,0) AS NewsAnnousVisbleCL,        
  convert(bit,0) AS NewsAnnousVisbleRTL,        
  CC_ID,        
  SiteStatus                    
   ,[Body] as News                    
      ,[IsManageTemplate]                
      ,'M' as Popup         
        
FROM [tblManageTemplate]        
WHERE PATINDEX('%,' + @uRole + ',%', ','+role+',') > 0  and isactive=1      
--and IsManageTemplate!=0        
ORDER BY id DESC                  
END

GO

