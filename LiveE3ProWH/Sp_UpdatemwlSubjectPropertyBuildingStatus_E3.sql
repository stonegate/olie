USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_UpdatemwlSubjectPropertyBuildingStatus_E3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_UpdatemwlSubjectPropertyBuildingStatus_E3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- Author:		<Rakesh Mohanty>
-- Create date: <07/11/2013>
-- Description:	<This will Update BuildingStatus of mwlSubjectProperty inE3>
-- =============================================
CREATE procedure [dbo].[Sp_UpdatemwlSubjectPropertyBuildingStatus_E3]
(
@LoanAppid varchar(100),
@BuildingStatus varchar(10) =null
)

AS
BEGIN
	BEGIN TRY
	Update [dbo].[mwlSubjectProperty] set BuildingStatus =@BuildingStatus where LoanApp_id = @LoanAppid
	END TRY
	BEGIN CATCH
		 Declare @Msg nvarchar(max)
		 Select @Msg=Error_Message();
		 RaisError('Building status update failed: %s', 20, 101,@Msg);
	END CATCH

END

GO

