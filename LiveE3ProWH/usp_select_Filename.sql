USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_select_Filename]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_select_Filename]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_select_Filename]
(
@loanregid int

)
as
begin
	select rtrim(Filename)Filename,borrLast from tblloanreg where loanregid=@loanregid;select rtrim(DocName)DocName from tblloandoc where uloanregid =@loanregid
end

GO

