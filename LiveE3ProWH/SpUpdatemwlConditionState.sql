USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpUpdatemwlConditionState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpUpdatemwlConditionState]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

     
CREATE PROCEDURE [dbo].[SpUpdatemwlConditionState]       
(        
 @condrecid varchar(MAX)   
)        
AS        
BEGIN        
        
update mwlConditionState  set STATE='NEW', StateDateTime = NULL 
where OBJOWNER_ID =@condrecid;
UPDATE mwlCondition SET CURRENTSTATE='NEW'where ID=@condrecid
        
END 


GO

