USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetReportForLoanAcqManager]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetReportForLoanAcqManager]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Stonegate>
-- Create date: <10/4/2012>
-- Description:	<This will Get Report For LoanAcqManager.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetReportForLoanAcqManager]
(
	@RoleID int
)	
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

Declare @SQL nvarchar(max)
set @SQL = ''

Set @SQL = 'select ''E3'' as DB,BOffices.Office as OFFICE_NAME1,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,
LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate,
approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,
case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,
LockRecord.Status as LockStatus,LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,
CASE ISNULL(RTRIM(Institute.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Institute.Company) END as BrokerName,Lookups.DisplayString as CashOut,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate,
CASE WHEN conditions.TotalConditions IS NULL then 0 WHEN conditions.TotalConditions = 0 then 0 WHEN conditions.TotalCleared = 0 THEN 0 ELSE conditions.TotalCleared * 100 / conditions.TotalConditions END AS PER 
from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
Left join dbo.mwlInstitution as Institute on Institute.ObjOwner_id = loanapp.id
and Institute.InstitutionType = ''BROKER'' and Institute.objownerName=''Broker''and Institute.Company is not null  

 left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.locktype=''Lock'' and  LockRecord.Status<>''CANCELED''
Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id 
and appStat.StatusDesc IN (''09 - Application Recieved'')  
Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved'' Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''

Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' and  Corres.Company is not null  

Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.InstitutionType = ''BRANCH'' and BOffices.objownerName=''BranchInstitution''
LEFT JOIN (SELECT COUNT(ID) AS TotalConditions, SUM(CASE WHEN (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') THEN 1 ELSE 0 END) AS TotalCleared, objOwner_ID
FROM dbo.mwlCondition WHERE DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'' GROUP BY objOwner_ID) as conditions on conditions.objOwner_ID = loanapp.id
where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and '

If (@RoleID = 16)
Begin
	Set @SQL = @SQL + ' CurrentStatus  IN (''01 - Registered'',''05 - Application Taken'', ''03 - Appt Set to Review Disclosures'', ''04 - Disclosures Sent'', ''07 - Disclosures Received'' ,''09 - Application Received'',''13 - File Intake'',''17 - Submission On Hold'',''19 - Disclosures'',''20 - Disclosures On Hold'',''25 - Submitted to Underwriting'',''29 - In Underwriting'',''29.1 - In UW Processing'',''33 - Cleared to Close'',''37 - Pre-Closing'',''41 - In Closing'',''43 - Closing on Hold'',''45 - Docs Out'', ''08 - In GFE Review'')'
End
Else
Begin
	Set @SQL = @SQL + ' CurrentStatus  IN (''01 - Registered'',''05 - Application Taken'', ''03 - Appt Set to Review Disclosures'', ''04 - Disclosures Sent'', ''07 - Disclosures Received'' ,''09 - Application Received'',''13 - File Intake'',''17 - Submission On Hold'',''19 - Disclosures'',''20 - Disclosures On Hold'',''25 - Submitted to Underwriting'',''29 - In Underwriting'',''29.1 - In UW Processing'')'
End
Set @SQL = @SQL + ' and loanapp.ChannelType in(''BROKER'',''CORRESPOND'') '
Set @SQL = @SQL + ' order by Per desc, loan_no desc'

Print @SQL
exec sp_executesql @SQL	
END

GO

