USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_update_tblScheduleClosing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_update_tblScheduleClosing]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[sp_update_tblScheduleClosing] --'0000316695','2-2-2012 00:00:00:000','','','','','','','','4486','',1        
(        
@LoanNo varchar(50),        
@SchDate datetime,        
@FeeSheet varchar(100),
@Userid int,

@Comments varchar(1000)
       
)         
AS         
BEGIN          
 IF EXISTS(SELECT ScheduleID FROM tblScheduleClosing WHERE LoanNo=@LoanNo)        
 BEGIN           
   IF @FeeSheet <> '' AND @Comments <> '' 
   BEGIN            
    UPDATE tblScheduleClosing         
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet,        
             
     SubmitedDate = GETDATE(),
         comments=@Comments
        
     
     WHERE LoanNo = @LoanNo          
   END        
       
       end
END 
GO

