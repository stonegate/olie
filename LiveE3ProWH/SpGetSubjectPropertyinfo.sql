USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetSubjectPropertyinfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetSubjectPropertyinfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

     
CREATE PROCEDURE [dbo].[SpGetSubjectPropertyinfo]       
(        
 @LoanApp_id varchar(MAX)   
)        
AS        
BEGIN        
       SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
	    
select *from mwlSubjectProperty  where LoanApp_id =@LoanApp_id
        
END 


GO

