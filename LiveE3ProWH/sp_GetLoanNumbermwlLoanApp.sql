USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanNumbermwlLoanApp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanNumbermwlLoanApp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procEDURE [dbo].[sp_GetLoanNumbermwlLoanApp]  

(  

@id varchar(100)  

)  

AS  

BEGIN  

   

   SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

  

select * from mwlLoanApp where id =@id  

END  


GO

