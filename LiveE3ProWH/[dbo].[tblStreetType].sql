USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblStreetType]') AND type in (N'U'))
DROP TABLE [dbo].[tblStreetType]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblStreetType](	  [StreetID] INT NOT NULL IDENTITY(1,1)	, [Type] NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Code] NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblStreetType_1] PRIMARY KEY ([StreetID] ASC))USE [HomeLendingExperts]
GO

