USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetRetailOperationReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetRetailOperationReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================    
-- Author:  Krunal Patel
-- Create date: 04 July 2013            
-- Description: Retail Operation Pipeline Management Task Grid Bind SP
-- =============================================

CREATE PROCEDURE [dbo].[SP_GetRetailOperationReport]              
(             
	@CurrentStatus varchar(1000),         
	@StatusDescr varchar(2000),
	@OriginatorID varchar(100)
)                
AS                
BEGIN                

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error	
	  
   DECLARE @CustomFieldID varchar(40)     
   SELECT @CustomFieldID = ID from mwscustfielddef where MWFieldNum = 9973 -- Custom Field Name "Approval Expiration Date", Custom Field Number "9973"
	  
	if(@OriginatorID  <> '')
	Begin
	
	Select 'E3' as DB,loanapp.EstCloseDate,
	(select top 1 DateValue  from mwlCustomField where custFieldDef_ID=@CustomFieldID
	and loanapp.ID=mwlCustomField.LoanApp_ID 
	order by datevalue desc) as ApprovalExpDate,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,
	loanapp.processorname as ProcessorName,loanapp.currentStatus as LoanStatus, loanapp.CurrPipeStatusDate, loanapp.DecisionStatus,
	loanapp.ID as	record_id,'' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.firstname as BorrowerFirstName,
	borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate,
	approvalStat.StatusDateTime as DateApproved,
	case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe,
	LoanData.LoanProgDesc as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,
	LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,RTRIM(Broker.Company) as BrokerName,Lookups.DisplayString as CashOut,CAST(borr.CompositeCreditScore AS CHAR(5)) as	CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate , 
	CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy='Prior to Closing' and Category !='LENDER' 
	and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0
	WHEN 
		(SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER')  
		and (CurrentState='CLEARED' OR CurrentState='SUBMITTED') and a.objOwner_ID IN 
			(SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   
				ELSE 
				(SELECT count(a.ID) as TotalCleared 
					FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER') and (CurrentState='CLEARED' 
						OR CurrentState='SUBMITTED') and a.objOwner_ID IN 
				(SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / 
				(SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy='Prior to Closing' and Category !='LENDER') 
					and a.objOwner_ID IN 
				(SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER 

	from mwlLoanApp  loanapp  
	Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
	Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active=1
	left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
	Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = loanapp.ChannelType
	left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType='LOCK' 
	and  LockRecord.Status<>'CANCELED' 
	Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc=@StatusDescr
	Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved' 
	Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName='mwlLoanData' and fieldname='refipurpose'  	 
	where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''   

	AND loanapp.ChannelType like '%RETAIL%'   AND CurrentStatus  IN (select items from dbo.SplitString(@CurrentStatus,','))  	 
	AND  loanapp.processor_id in (select items from dbo.SplitString(@OriginatorID,','))        
	ORDER BY Per desc
	END
	
ELSE

	Select 'E3' as DB,loanapp.EstCloseDate,
	(select top 1 DateValue  from mwlCustomField where custFieldDef_ID= @CustomFieldID
	and loanapp.ID=mwlCustomField.LoanApp_ID  order by datevalue desc) as ApprovalExpDate,loanapp.casenum as FHANumber,loanapp.ChannelType,
	loanapp	.OriginatorName,loanapp.processorname as ProcessorName,loanapp.currentStatus as LoanStatus, loanapp.CurrPipeStatusDate, 
	loanapp.DecisionStatus,loanapp.ID as record_id,'' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,
	borr.firstname as BorrowerFirstName,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'' as LU_FINAL_HUD_STATUS,
	appStat.StatusDateTime AS SubmittedDate,approvalStat.StatusDateTime as DateApproved,case when TransType is null then '' when 
	TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe,LoanData.LoanProgDesc as LoanProgram,
	loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockExpirationDate,LockRecord.LockDateTime,
	UnderwriterName as UnderWriter,RTRIM(Broker.Company) as BrokerName,Lookups.DisplayString as CashOut,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, 
	convert(varchar(35),EstCloseDate,101) AS ScheduleDate , CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a 
	where DueBy='Prior to Closing' and Category !='LENDER' and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  
	WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0 WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a 
	where (DueBy='Prior to Closing' and Category !='LENDER')  and (CurrentState='CLEARED' 
	OR CurrentState='SUBMITTED')
	and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   
	ELSE (SELECT count(a.ID) as TotalCleared 
	FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER') and (CurrentState='CLEARED' OR CurrentState='SUBMITTED') 
	and a.objOwner_ID IN (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / 
	(SELECT count(a.ID) as	TotalConditions FROM dbo.mwlCondition a where ( DueBy='Prior to Closing' and Category !='LENDER') 
	and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER 
	from mwlLoanApp  loanapp  
	Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
	Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active=1
	left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
	Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = loanapp.ChannelType 
	left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType='LOCK' 
	and  LockRecord.Status<>'CANCELED' 
	Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc=@StatusDescr
	Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved' 
	Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName='mwlLoanData' and fieldname='refipurpose'  	 
	left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID 
	and CustFieldDef_id in (select Id from mwsCustFieldDef where  MWFieldNum = 9966)
	where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''   
	AND loanapp.ChannelType like '%RETAIL%'   AND CurrentStatus  IN (select items from dbo.SplitString(@CurrentStatus,',')) 
	ORDER BY Per desc
	END




GO

