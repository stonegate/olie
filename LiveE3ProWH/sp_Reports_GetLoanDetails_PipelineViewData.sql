USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Reports_GetLoanDetails_PipelineViewData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Reports_GetLoanDetails_PipelineViewData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Stonegate  
-- Create date: April, 18 2012  
-- Description: This will give Loan details for the loan number passed  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_Reports_GetLoanDetails_PipelineViewData]  
(  
@LoanNumber nvarchar(50)
)  
AS  
BEGIN  
 
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

   select top 1 ml.DisplayString as BUSTYPE,loanapp.casenum as FHANumber,'E3' as DB,loanapp.ID as record_id,loanapp.DecisionStatus,  
    LoanNumber as LOAN_NO, PropertyAddress.Street as Prop_Addr ,   
    PropertyAddress.City + ',' +  ',' +  PropertyAddress.State + ',' + PropertyAddress.Zipcode as Prop_Addr1,  
    loanData.AdjustedNoteAmt as LOAN_AMT,  
    CONVERT(DECIMAL(10,3),loanData.NoteRate * 100) as INT_RATE,CONVERT(DECIMAL(15,3),loanData.LTV *100)LTV ,CONVERT(DECIMAL(10,3),loanData.CLTV *100) CLTV,loanapp.CurrentStatus as LT_LOAN_STATS,  
    RTRIM(LoRep.LastName) + ' ' + RTRIM(LoRep.FirstName) AS INTERV_NAME , LoRep.Email AS INTERV_EMAIL,PhoneNo.PhoneNumber as INTERV_MOBILE,PhoneNo.PhoneNumber as INTERV_PH,      
     uSummary.ApprvlExpirDate as APPR_EXP_DATE,Appraiser.FirstName + ' ' + Appraiser.LastName as LT_ORIG_APPRAIS,  
      
    RTRIM(borr.lastname) + ', ' + RTRIM(borr.FirstName) as BORR_AKA1,  
    AppDocument.DateOrder AS DOC_REQ_DATE,AppDocument.ExhibitDate AS DOC_SENT_DATE,AppDocument.DateReceived AS DOC_BACK_DATE, CASE ISNULL(RTRIM(Broker.Company),'') WHEN '' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as  BROK_NAME,  
    '' as App_Date,DateAppSigned as App_Recvd_Date,ReorderDate as DOC_REVIEWED_DATE,LoanData.LoanProgramName as LoanProgram,Loandata.LoanProgDesc as PROG_DESC,    
      
    Offices.Office as OFFICE_NAME1,case TransType when NULL then '' when 'P' then 'Purchase money first mortgage' when 'R' then 'Refinance' when '2' then 'Purchase money second mortgage' when 'S' then 'Second mortgage, any other purpose' when 'A' then 'As
sumption' when 'HOP' then 'HELOC - other purpose' when 'HP' then 'HELOC - purchase' else '' end as PURPOSE,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,  
    convert(varchar(35),EstCloseDate,101) AS ScheduleDate,LockStatus,LoanApp.LockExpirationDate,UnderwriterName as UnderWriter,uEmail.Email as Underwriter_Email,   
      
    DocumentPreparerName as DOCDRAWER,Institute.Title as LT_ESCROW_CO,  
    Institute.Company as  Comp_Name,              
    case PropertyAddress.PropertyType when NULL then '' when 'SINGLE FAM' then 'Single family dwelling' when '2-4 FAMILY' then '2-4 family dwelling' 
		when 'MULTI-FAM' then 'Multi-family' when 'CONDO' then 'Condominium' when 'TOWNHOUSE' then 'Townhouse' when 'PUD' then 'PUD' when 'OTHER' then 'Other' when 'CO-OP APT' then 'Co-operative apartment' when 'COMM RESID' then 'Home & business combined' when 'COMM NON' then 'Commercial - non-residential' when 'FARM' then 'Farm' when 'LAND' then 'Land' 
		when 'MIXED-RES' then 'Mixed use - residential' when 'MOBILE' then 'Manufactured/Mobile home' else '' end  as PROPTYPE,  
  
    lAmortType.DisplayString as AMORTTYPE,Lookups.DisplayString as LOANTYPE,loanapp.ChannelType as BUSTYPE1,loanData.DocumentationType as DOCTYPE,  
    '' as DocsSentDate,'' as FundedDate,'' as UW_SUBMIT_DATE ,'' as UW_RECVD_DATE,'' as SUSP_DATE, '' as DENIED_DATE ,'' as APPR_DATE,''as DOC_FUNDED_DATE ,
	LoanData.ShortProgramName as PROG_CODE,MIData.IMPOUNDS as IMPOUNDS,uSummary.FHAApprovalType as ApprovalType,'' as INTERESTONLY,
	'' as CONFORMING,loanData.PrepayPenalty as PREPAYPEN, HelocData.LienPosition as Lien ,  
    LockRecord.Buydown,OccupencyLookup.DisplayString as OCCUPANCY   
    from mwlLoanApp loanapp  
    inner join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id   
    Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and borr.sequencenum=1  
    Inner join mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id   
      
    left join mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id    
    Left join mwlInstitution as Institute on Institute.ObjOwner_id = loanapp.id and Institute.InstitutionType = 'Title'  
    Left join mwlRealEstate as RealEstate on RealEstate.LoanApp_id = loanapp.id    
    left join mwaMWUser as LoRep on LoRep.ID = loanapp.Originator_ID    
      
    Left join mwlInstitution as Appraiser on Appraiser.ObjOwner_id = loanapp.id and Appraiser.InstitutionType = 'APPRAISER' and Appraiser.objownerName='Contacts'   
    Left join mwlAppDocument as AppDocument on AppDocument.loanapp_id = loanapp.id and AppDocument.InstitutionType = 'BROKER'   
    Left join mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = 'BROKER' and Broker.objownerName='Broker'   
    Left join mwlInstitution as UnderWriter on UnderWriter.ObjOwner_id = loanapp.id and UnderWriter.InstitutionType = 'UNDERWR' and UnderWriter.objownerName='Contacts'   
    Left join mwlInstitution as Offices on Offices.ObjOwner_id = loanapp.id and Offices.InstitutionType = 'BRANCH' and Offices.objownerName='BranchInstitution'    
    left join mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType='LOCK' and  LockRecord.Status<>'CANCELED'   
    Left outer join mwlLookups Lookups on LoanData.financingtype=Lookups.BOCode and Lookups.objectName='mwlLoanData' and Lookups.FieldName='FinancingType'   
    Left outer join mwlLookups OccupencyLookup on LoanData.Occupancy=OccupencyLookup.BOCode and OccupencyLookup.objectName='mwlLoanData' and OccupencyLookup.FieldName='Occupancy'  
    left join mwlMIData as MIData on MIData.LoanData_ID = loanData.id   
    left join mwlHELOCData as HelocData on HelocData.LoanApp_id = loanapp.id   
    left join mwaPhoneNumber as PhoneNo on PhoneNo.objOwner_id = LoRep.id   
    left join mwlSubjectProperty as PropertyAddress on PropertyAddress.LoanApp_id = loanapp.id   
    left join mwllookups lAmortType on loanData.AmortType=lAmortType.BoCode and lAmortType.objectName='mwlloandata' and lAmortType.fieldname='AmortType'   
    left outer join mwlLookups as ml on ml.bocode=ChannelType and ml.objectName='mwlLoanApp'   
    left join mwamwuser as uEmail on uSummary.UnderWriter_id = uEmail.id    
    Left join mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = 'CORRESPOND' and Corres.objownerName='Correspondent'  
    WHERE LoanNumber = @LoanNumber 
END  
  
  

GO

