USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_FetchList_tblNewsAndAnnouncementByRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_FetchList_tblNewsAndAnnouncementByRole]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[udsp_FetchList_tblNewsAndAnnouncementByRole]        
(        
@IsNewsAnnouncement bit,  
@uRole varchar(max)        
        
)        
as        
begin        
  --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News ,CONVERT(bit,0) as [IsManageTemplate],'N' as Popup  from tblNewsAndAnnouncement         
  SELECT Top 5 * FROM   
(  
SELECT * FROM   
(  
SELECT ID,Title,Body,IsActive,IsNewsAnnouncement,CreatedBy,CreatedDate,IPAddress,Role,RoleC,RoleR,NewsAnnousVisbleAd,  
NewsAnnousVisbleDR,NewsAnnousVisbleBR ,NewsAnnousVisbleLO,NewsAnnousVisbleCU,NewsAnnousVisbleRE,convert(bit,0) AS NewsAnnousVisbleRM  
,convert(bit,0) AS NewsAnnousVisbleDP,convert(bit,0) AS NewsAnnousVisbleCP,convert(bit,0) AS NewsAnnousVisbleHY ,convert(bit,0) AS NewsAnnousVisbleDCFI,convert(bit,0) AS NewsAnnousVisibleSCFI  
,convert(bit,0) AS NewsAnnousVisbleUM,convert(bit,0) AS ModifiedBy,CreatedDate AS ModifiedDate,NewsAnnousVisbleAM,NewsAnnousVisbleDM ,NewsAnnousVisbleROM  
,NewsAnnousVisbleCO,NewsAnnousVisbleCRM,NewsAnnousVisbleLP,NewsAnnousVisbleCL,NewsAnnousVisbleRTL,CC_ID,SiteStatus   
,SUBSTRING(body,0,500)+'...............' as news,Convert(bit,0) as 'IsManageTemplate','N' as Popup   
FROM Retail_tblNewsAndAnnouncement) as T1   
UNION ALL   
SELECT * FROM   
(  
SELECT ID,Title,Body,IsActive,IsNewsAnnouncement,CreatedBy,CreatedDate,'' AS IPAddress,Role,RoleC,RoleR,NewsAnnousVisbleAd,NewsAnnousVisbleDR,   
NewsAnnousVisbleBR,NewsAnnousVisbleLO,NewsAnnousVisbleCU,NewsAnnousVisbleRE,NewsAnnousVisbleRM,NewsAnnousVisbleDP ,  
NewsAnnousVisbleCP,NewsAnnousVisbleHY,NewsAnnousVisbleDCFI,NewsAnnousVisibleSCFI,NewsAnnousVisbleUM,ModifiedBy ,  
ModifiedDate,convert(bit,0) AS NewsAnnousVisbleAM,convert(bit,0) AS NewsAnnousVisbleDM,convert(bit,0) AS NewsAnnousVisbleROM,'' as NewsAnnousVisbleCO   
,'' as NewsAnnousVisbleCRM,'' as NewsAnnousVisbleLP,'' as NewsAnnousVisbleCL,'' as NewsAnnousVisbleRTL,CC_ID,SiteStatus   
,SUBSTRING(body,0,500)+'...............' as news,Convert(bit,0) as 'IsManageTemplate','N' as Popup   
FROM tblNewsAndAnnouncement  
)   
as T2  
)  
as tblNewsAndAnnouncement  
  
  where  IsActive=1 and IsNewsAnnouncement=@IsNewsAnnouncement   
  and PATINDEX('%,' + @uRole + ',%', ','+RoleC+',') > 0       
 order by createddate desc  
         
end

GO

