USE [HomeLendingExperts]
								GO
								SET ANSI_NULLS ON
								GO
								SET QUOTED_IDENTIFIER ON
								GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetFormRole]
(
--added by vipu thakkar on 27 oct
	-- Add the parameters for the function here
	@formid int
)
RETURNS varchar(200)
AS
BEGIN
declare @retstr varchar(8000) 
select  @retstr =  COALESCE(@retstr + ',','') + convert(varchar(10),uroleid) 
from tblFormRoles where formid=@formid
--select @retstr as uroleid
	
	RETURN  @retstr

END

GO

