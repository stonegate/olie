USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_HolidayList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Get_HolidayList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================  

-- Author:  <Muruga Nagaraju>  

-- Create date: <27/05/2014>  

-- Description: <This wil give you all the Holiday list>  

-- =============================================  

CREATE PROCEDURE [dbo].[Get_HolidayList] --'f,sf'
(
@HolidayType varchar(10)
)

AS

BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT 
holidayDate,HolidayName
FROM  tblHolidayList 
WHERE Status='Y' and holidayDate >=Getdate() and HolidayType in(Select items from dbo.splitString(@HolidayType,','))
  
END


GO

