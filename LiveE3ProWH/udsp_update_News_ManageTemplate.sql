USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_update_News_ManageTemplate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_update_News_ManageTemplate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Mukesh Kumar  
-- Create date: 01/31/2013  
-- Description: To Update Date  
-- =============================================  
CREATE PROCEDURE [dbo].[udsp_update_News_ManageTemplate]   
 

-- Add the parameters for the stored procedure here  
@id int,  
@body varchar(max),  
@modifiedBy int,  
@modifiedDate datetime  
AS  
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;
UPDATE tblManageTemplate
SET	Body = @body,
	ModifiedBy = @modifiedBy,
	ModifiedDate = @modifiedDate
WHERE ID = @id  
      


END

GO

