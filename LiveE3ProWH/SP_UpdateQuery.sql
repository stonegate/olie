USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_UpdateQuery]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_UpdateQuery]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Shital,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_UpdateQuery]
(
@TableName varchar(500),  
@Updatestring nVarchar(max) ,
@UpdateSourceColumn Varchar(max) ,
@iUpdateID int = null,
@UpadeteVal nvarchar(max)= null, 
@Flag varchar(50)
)
AS
BEGIN
Declare @SQL nvarchar(max)  
set @SQL = ''   
    set @SQL = 'Update  '+ @TableName +' set '+@Updatestring+'  where '+@UpdateSourceColumn+ ' ='
if @Flag = 'UpdateId'
   Begin
    set @SQL = @SQL + convert(varchar(max),@iUpdateID )
    End
else if @Flag = 'UpdateValue'
    BEGIN
    set @SQL = @SQL + ''''+@UpadeteVal+''''
    END
exec sp_executesql @SQL  	
END







GO

