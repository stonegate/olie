USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_UpdateLoanStatusforCorr]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_UpdateLoanStatusforCorr]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here
Create PROCEDURE [dbo].[SP_UpdateLoanStatusforCorr]
(
@PurchaseConditionReceived nvarchar(500),
@LoanId nvarchar(200),
@strLoanNo nvarchar(250))

AS
BEGIN
	  if not exists(select * From mwlappstatus where LoanApp_id in
(@LoanId) and StatusDesc=@PurchaseConditionReceived)
                         insert into mwlappstatus(ID,LoanApp_id,StatusDesc,StatusDateTime)values 
                         (replace(newid(),'-',''),@LoanId, @PurchaseConditionReceived,getdate())
    update mwlloanapp set CurrentStatus=@PurchaseConditionReceived,
           CurrPipeStatusDate=getdate() where loannumber =@strLoanNo 
END



-- Create Procedure Coding ENDS Here

GO

