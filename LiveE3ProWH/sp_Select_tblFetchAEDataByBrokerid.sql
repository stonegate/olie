USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblFetchAEDataByBrokerid]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblFetchAEDataByBrokerid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblFetchAEDataByBrokerid]
(
@sRecordid varchar(Max)
)
as
begin
select isnull(AEID,0)AEID from tblusers 
where (Uidprolender =@sRecordid or E3Userid=@sRecordid)
end


GO

