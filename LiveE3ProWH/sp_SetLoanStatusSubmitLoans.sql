USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SetLoanStatusSubmitLoans]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SetLoanStatusSubmitLoans]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  <Author,,Name>      
-- Create date: <Create Date,,>      
-- Description: <Description,,>      
-- =============================================      
-- Author:  <Author,,Name>      
-- Create date: <Create Date,,>      
-- Description: <Description,,>      
-- =============================================      
CREATE PROCEDURE [dbo].[sp_SetLoanStatusSubmitLoans]      
(       
 @LoanNumber varchar(20),
 @URole int = null   
)      
AS      
BEGIN      
      
Declare @LoanAppID varchar(200)      
    set @LoanAppID = (select id from mwlloanapp where loannumber=@LoanNumber)      

Declare @SetGFEStatus bit
Declare @ChannelType varchar(10)
--Declare @UWType varchar(256)
declare @StatusDesc varchar(40)

Select @SetGFEStatus=0

-- Set Default Status to Application Received.
Select @StatusDesc='09 - Application Received'

select @ChannelType=channeltype from dbo.mwlLoanApp where LoanNumber=@LoanNumber 
--select @UWType=StringValue from mwlcustomfield where CustFieldDef_ID ='C4D2D8038F614DF8A1B8A6622C99FC3E' 
--	and loanapp_id in(select ID as loanapp_id from mwlloanapp where LoanNumber=@LoanNumber)


-- Set GFE status for BROKER
if (@ChannelType='BROKER' ) AND (@URole=0 OR @URole=2 OR @URole=3 OR @URole=8 OR @URole=21 OR @URole=26 )
begin
	Select @SetGFEStatus=1
end
--else 
---- Set GFE status for Correspond Prior Approved loans
--begin
--	if @ChannelType='CORRESPOND' AND @UWType='Prior Approved'
--	begin
--		Select @SetGFEStatus=1
--	end
--end

if @SetGFEStatus=1
begin
	Select @StatusDesc = '08 - In GFE Review'
end


if not exists(select * From mwlappstatus where LoanApp_id in(@LoanAppID)       
    and StatusDesc=@StatusDesc)      
Insert into mwlappstatus(ID,LoanApp_id,StatusDesc,StatusDateTime)values      
                        (replace(newid(),'-',''),@LoanAppID,@StatusDesc,getdate())      
                              
Update       
 mwlloanapp       
 Set CurrentStatus=@StatusDesc,CurrPipeStatusDate=getdate()       
    Where loannumber = @LoanNumber      

        
END 

GO

