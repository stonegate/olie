USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_delete_tblBrokerApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_delete_tblBrokerApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[usp_delete_tblBrokerApplication](
@BrokerAppIDs VARCHAR(500) )
AS
BEGIN
 DECLARE @query VARCHAR(1000)
 
 SET @query = 'UPDATE dbo.tblBrokerApplication SET IsActive=0 WHERE BrokerAppID IN (' + @BrokerAppIDs + ')'
 
 EXECUTE (@query)
END


GO

