USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Updatemwlloanapp1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Updatemwlloanapp1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/08/2012
-- Description:	Procedure for clspipelineE3 BLL
-- =============================================
CREATE PROCEDURE [dbo].[sp_Updatemwlloanapp1]
(@loannumber varchar(15),
@CurrentStatus varchar(30)
)
AS
BEGIN
	
	SET NOCOUNT ON;
 update mwlloanapp set 
CurrentStatus=@CurrentStatus
where LoanNumber=@loannumber
END


GO

