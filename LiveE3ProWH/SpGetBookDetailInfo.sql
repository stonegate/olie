USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetBookDetailInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetBookDetailInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetBookDetailInfo] 
	@Id int
AS
BEGIN
	select *  from BookDetail where BookID= @Id
END

GO

