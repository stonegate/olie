USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFNMLookup]') AND type in (N'U'))
DROP TABLE [dbo].[tblFNMLookup]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFNMLookup](	  [LineId] INT NOT NULL	, [FieldCode] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [DataStream] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [Pos] INT NOT NULL	, [Len] INT NOT NULL	, [SegOcc] VARCHAR(3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [Information] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [LineLength] INT NOT NULL	, [IsActive] BIT NOT NULL)USE [HomeLendingExperts]
GO

