USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetTab]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetTab]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetTab] 
	 @UserRole int
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)

	select tbs.TabName, tbs.ControlName  from tblRoleSetup rs join tbltabs tbs on rs.TabID=tbs.TabID where rs.isActive = 1 and rs.RoleID = @UserRole

    
END

GO

