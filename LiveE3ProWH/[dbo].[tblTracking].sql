USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTracking]') AND type in (N'U'))
DROP TABLE [dbo].[tblTracking]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTracking](	  [Id] BIGINT NOT NULL IDENTITY(1,1)	, [UserID] INT NOT NULL	, [LoginTime] DATETIME NOT NULL	, [LogoutTime] DATETIME NULL	, [IPAddress] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblTracking] PRIMARY KEY ([Id] ASC))USE [HomeLendingExperts]
GO

