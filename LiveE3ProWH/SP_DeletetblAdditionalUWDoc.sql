USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DeletetblAdditionalUWDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_DeletetblAdditionalUWDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

Create PROCEDURE [dbo].[SP_DeletetblAdditionalUWDoc]
	(
@ctid int
)
AS
BEGIN
   delete from tblAdditionalUWDoc where ctid = @ctid
             
END


GO

