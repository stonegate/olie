USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetMandatoryCorrespondent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetMandatoryCorrespondent]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Example for execute stored procedure       
--EXECUTE [udsp_GetMandatoryCorrespondent] '478452' 
-- =============================================      
-- Author:  Sonu Gupta      
-- Create date: 04/10/2013   
-- Name:udsp_GetMandatoryCorrespondent.sql
-- Description: Get Correspondent with Mandatory Designation     
-- =============================================      
CREATE PROCEDURE [dbo].[udsp_GetMandatoryCorrespondent] 
(
 --Parameter declaration            
@PartnerCompanyID VARCHAR(40) 
)     
AS  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
SET XACT_ABORT ON;  
    
BEGIN      
SELECT     ID FROM         tblManageMandatoryClientsInfo
WHERE     (PartnerCompanyId = @PartnerCompanyID) AND (IsActive = 'True') 
END 

GO

