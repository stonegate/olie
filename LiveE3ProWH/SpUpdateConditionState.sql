USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpUpdateConditionState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpUpdateConditionState]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nirav>
-- Create date: <10/9/2012>
-- Description:	<Update record to the Condition State>
--getDate()
-- =============================================
CREATE PROCEDURE [dbo].[SpUpdateConditionState]
	@ConRecId varchar(500)
AS
BEGIN
	Declare @date dateTime
	set @date =getdate()
	update mwlConditionState  set STATE='SUBMITTED', StateDateTime = @date where OBJOWNER_ID =@ConRecId
UPDATE mwlCondition SET CURRENTSTATE='SUBMITTED' where ID=@ConRecId
END	

GO

