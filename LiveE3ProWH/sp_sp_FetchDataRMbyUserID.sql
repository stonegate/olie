USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_sp_FetchDataRMbyUserID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_sp_FetchDataRMbyUserID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_sp_FetchDataRMbyUserID] 
	-- Add the parameters for the stored procedure here
@iUserID INT


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


	BEGIN
	select * from tblusers where userid in (@iUserID) And urole=8
	
	END
	
	
END
GO

