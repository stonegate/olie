USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Reports_GetLoanDetails_LoanDateTracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Reports_GetLoanDetails_LoanDateTracking]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Stonegate  
-- Create date: April, 18 2012  
-- Description: This will give Loan details for the loan number passed  
-- Modified by : Suvarna on 17/12/2013 - Added clear purchase pending calculation logic
-- =============================================  
CREATE PROCEDURE [dbo].[sp_Reports_GetLoanDetails_LoanDateTracking]  
(  
@LoanNumber nvarchar(50)
)  
AS  
BEGIN  

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	Declare @ClearPurDateDays smallint 
	Declare @NewLoansRangeDate DateTime  
	Declare @FirstPurPendDate  DateTime 
	Declare @ClearPurPendingDate DateTime
	Declare @LastPurPendDate DateTime
	Declare @ClearPurDateDaysOld smallint

	

	SET @ClearPurDateDays = (select top 1 KeyValue from tblAppSettings where KeyName='ClearedPurDateDays') 	
	SET @ClearPurDateDaysOld = (select  top 1 KeyValue from tblAppSettings where KeyName='ClearedPurDateDaysOld')
	SET @NewLoansRangeDate = CAST ((select  top 1 KeyValue from tblAppSettings where KeyName='NewloansRangeDate') AS DATETIME)
   SET  @FirstPurPendDate = (select  MIN(StatusDateTime) from mwlAppStatus  where loanapp_id in(Select id from MwlLoanapp where LoanNumber=@LoanNumber) and StatusDesc in ('54 - Purchase Pending') group by Loanapp_id,StatusDesc)
--	SET @LastPurPendDate = (select  Max(StatusDateTime) from mwlAppStatus  where loanapp_id in(Select id from MwlLoanapp where LoanNumber=@LoanNumber) and StatusDesc in ('54 - Purchase Pending') group by Loanapp_id,StatusDesc)

    if(@FirstPurPendDate > @NewLoansRangeDate )
	BEGIN
	   SET @ClearPurPendingDate = (select DATEADD( day , @ClearPurDateDays , @FirstPurPendDate))
	END
	else
	   SET @ClearPurPendingDate = (select DATEADD( day , @ClearPurDateDaysOld , @FirstPurPendDate))


     select ID,	LoanApp_ID,	StatusDesc,	StatusMWCode,
	 CASE WHEN left(ltrim(rtrim(StatusDesc)),2) = '54' THEN  @FirstPurPendDate  ELSE 	StatusDateTime END as StatusDateTime,
	 SequenceNum,HMDACode,	LPCode,	UpdatedOnDate,	UpdatedByUser,	CreatedOnDate,	CreatedByUser,	
	 @ClearPurPendingDate as ClearPurPendingDate from mwlAppStatus  
	 where loanapp_id in(Select id from MwlLoanapp where LoanNumber=@LoanNumber)


END  
  
  

GO

