USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_PROGINFO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_PROGINFO]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		 Nitin
-- Create date: 10/04/2012
-- Description:	Procedure for clsloanRegister.cs bll  
-- =============================================
Create Proc [dbo].[sp_Get_PROGINFO]
(@record_id char(5))
as
begin

select record_id,prog_code,prog_desc from PROGINFO where record_id=@record_id
end

GO

