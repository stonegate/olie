USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetE3UserId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetE3UserId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetE3UserId]
	@UserId int
AS
BEGIN
	select isnull(e3userid,'') as  e3userid  from tblusers where userid =@UserId
END

GO

