USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ManageDOOToleranceHistory_New]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ManageDOOToleranceHistory_New]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================          
-- Author:  Mitesh R Rajyaguru          
-- Create date: Jun, 06 2013          
-- Description: This procedure will use for saving $100 tolerance history
-- =============================================  
 
CREATE procedure [dbo].[sp_ManageDOOToleranceHistory_New]  
@StepNo int,  
@LoanNo varchar(10),  
@UserID int,  
@SessionID varchar(10),  
@HUDLineNo varchar(5),  
@ActualValue money,  
@UpdatedValue money,  
@imagebkbtn bit  
as  
begin  
   
 SET XACT_ABORT ON;  
  
        IF EXISTS(SELECT hudlineno 
                FROM   tbldootolerancehistory WITH (NOLOCK)
                WHERE  loanno = @LoanNo 
                       AND hudlineno = @HUDLineNo) 
        BEGIN 
            IF @imagebkbtn = 1 
              -- If user has clicked the back button than this is set to 1  
              BEGIN 
                  UPDATE tbldootolerancehistory WITH (updlock) 
                  SET    updatedvalue = @UpdatedValue 
                  WHERE  hudlineno = @HUDLineNo 
                         AND loanno = @LoanNo 
              END 
            ELSE 
              BEGIN 
                  UPDATE tbldootolerancehistory WITH (updlock) 
                  SET    updatedvalue = @UpdatedValue 
                  WHERE  hudlineno = @HUDLineNo 
                         AND loanno = @LoanNo 
              END 
        END 
      ELSE 
        BEGIN 
            INSERT INTO tbldootolerancehistory 
                        (stepno, 
                         loanno, 
                         userid, 
                         sessionid, 
                         hudlineno, 
                         actualvalue, 
                         updatedvalue) 
            VALUES      (@StepNo, 
                         @LoanNo, 
                         @UserID, 
                         @SessionID, 
                         @HUDLineNo, 
                         @ActualValue, 
                         @UpdatedValue) 
        END 
end        

-- Create Procedure Coding ENDS Here

GO

