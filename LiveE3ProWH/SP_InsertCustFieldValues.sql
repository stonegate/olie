USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_InsertCustFieldValues]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_InsertCustFieldValues]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create Procedure Coding Starts Here

CREATE PROCEDURE [dbo].[SP_InsertCustFieldValues]
	(
@strLoanid nvarchar(500),
@CustFieldDef nvarchar(max),
@strMSRValue nvarchar(500)
)
AS
BEGIN

         if not exists(select * From mwlcustomfield where LoanApp_ID in(@strLoanid)
         and CustFieldDef_ID in (@CustFieldDef) )
        Insert into mwlcustomfield(ID,LoanApp_ID,CustFieldDef_ID,StringValue) Values(
        replace(newid(),'-',''),@strLoanid,@CustFieldDef,@strMSRValue)

        update mwlcustomfield set StringValue=@strMSRValue  where LoanApp_ID =@strLoanid 
        and CustFieldDef_ID=@CustFieldDef

END


GO

