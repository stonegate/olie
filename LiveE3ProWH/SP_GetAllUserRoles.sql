USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetAllUserRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetAllUserRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GetAllUserRoles]    
    
AS
BEGIN    
      
select case id when '25' then '1' when '26' then '2' when '3' then '3'     
when '20' then '4' when '21' then '5' when '37' then '6' end as Nid,* from tblroles    
 where id in (25,26,3,20,21,37) order by Nid asc    
END

GO

