USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_UpdateClearedStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_UpdateClearedStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_UpdateClearedStatus]
(
@lt_cond_cleared_by char(5),
@record_id int
)
AS
BEGIN
	
	SET NOCOUNT ON;
update condits  set COND_CLEARED_DATE = Getdate(),
lt_cond_cleared_by =@lt_cond_cleared_by 
where record_id =@record_id

END


GO

