USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Insert_tblInsertAdminUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Insert_tblInsertAdminUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  Gourav        
-- Create date: 10/03/2012        
-- Description: Procedure for clsUser.cs bll          
-- =============================================        
CREATE proc [dbo].[usp_Insert_tblInsertAdminUser]        
(        
@ufirstname varchar(Max)=null        
,@ulastname varchar(Max)=null        
,@uemailid varchar(Max)=null        
,@urole varchar(Max)=null        
,@isadmin varchar(Max)=null        
,@isactive varchar(Max)=null        
,@uparent varchar(Max)=null        
,@upassword varchar(Max)=null        
,@mobile_ph varchar(Max)=null        
,@phonenum varchar(Max)=null        
,@MI varchar(Max)=null        
,@PasswordExpDate varchar(Max)=null        
,@CreatedBy varchar(Max)=null        
,@IsUserInNewProcess varchar(Max)=null    
,@bitIsKatalyst bit =0          
)        
as        
begin        
Insert into tblusers        
(        
ufirstname        
,ulastname        
,uemailid        
,urole        
,isadmin        
,isactive        
,uparent        
,upassword        
,mobile_ph        
,phonenum        
,MI        
,PasswordExpDate        
,CreatedBy        
,IsUserInNewProcess        
,Iskatalyst      
)        
Values        
(        
@ufirstname        
,@ulastname        
,@uemailid        
,@urole        
,@isadmin        
,@isactive        
,@uparent        
,@upassword        
,@mobile_ph        
,@phonenum        
,@MI        
,@PasswordExpDate        
,@CreatedBy        
,@IsUserInNewProcess      
,@bitIsKatalyst         
)        
end 

GO

