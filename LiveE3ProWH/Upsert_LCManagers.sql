USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Upsert_LCManagers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Upsert_LCManagers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Upsert_LCManagers] @UserId NVARCHAR(MAX)
	,@ManagerIds NVARCHAR(MAX)
	,@CreatedBy [NVARCHAR] (50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	INSERT INTO [dbo].[tblUserManagers] (
		[UserId]
		,[ManagerId]
		,[CreatedBy]
		,[CreatedOn]
		)
	SELECT @UserId
		,Id.Items
		,@CreatedBy
		,GetDate()
	FROM [dbo].SplitString(@ManagerIds, ',') Id
	WHERE Id.Items NOT IN (
			SELECT UM.ManagerId
			FROM [dbo].[tblUserManagers] UM
			WHERE UM.UserId = @UserId
			)

	DELETE
	FROM [dbo].[tblUserManagers]
	WHERE UserId = @UserId
		AND ManagerId NOT IN (
			SELECT Id.Items
			FROM [dbo].SplitString(@ManagerIds, ',') Id
			)
END


GO

