USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBDMDatacfi]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetBDMDatacfi]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[GetBDMDatacfi]  
 -- Add the parameters for the stored procedure here  
   
AS  
BEGIN  

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
select ufirstname + ' ' + ulastname as uname, userid from tblusers where (urole =2 or urole=8 or urole=19) and isactive=1 order by ufirstname   
END  
GO

