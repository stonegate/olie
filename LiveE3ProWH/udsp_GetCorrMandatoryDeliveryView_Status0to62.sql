USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetCorrMandatoryDeliveryView_Status0to62]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetCorrMandatoryDeliveryView_Status0to62]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:  Reetu Sanghi

-- Create date: 28th April 2014        

-- Description: This will get : Correspondent Specialist, Correspondent Funding Specialist and Correspondent Manager Report

-- =============================================        

CREATE PROCEDURE [dbo].[udsp_GetCorrMandatoryDeliveryView_Status0to62]

(        

@sortField varchar(500),        

@sortOrder varchar(100),        

@whereClause varchar(1000),        

@PageNo int,         

@PageSize int,  

  

@CustomField nvarchar(500),  

@CustomFieldN1 nvarchar(500),  

@CustomFieldN2 nvarchar(500),  

@CustomFieldN3 nvarchar(500),

@CustomFieldN4 nvarchar(500), 

@CustSendToFundingDate nvarchar(500),  

@SubmittedDateID nvarchar(500),  

@SubmittedDateTimeID nvarchar(500),  

  

@CurrentStatus nvarchar(500),  

@URL_From  nvarchar(500), --queryString i.e.agedpursub,agedcloreceived,agedcloreviewed ect. 

--newlyadded

@OriginatorId nvarchar(max)=null,

@Userole varchar(50)=null,

@ShowAllData int, -- If 1 than display all data and no count

@CustomFieldN5 nvarchar(100),

@CustomFieldN6 nvarchar(100),

@CustomSendToFunding nvarchar(100)

)         

AS        

BEGIN  



SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

  

IF @sortField  = ''                                    

  set @sortField = 'loanapp.CurrPipeStatusDate'        

        

SET @sortField = @sortField + ' ' + @sortOrder           

        

Declare @sql nvarchar(max)   

If @ShowAllData = 0

Begin

	set @sql = ''                                    

		 set @sql = 'Select count(*),ceiling(sum(loan_amt)) as TotalAmt, ceiling(sum(loan_amt)/count(*)) as avg  from ('

		 set @sql =  @sql + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ')AS Row,

	''E3'' as DB,CustDate.DateValue as LastSubmittedDate,CustTime.StringValue as LastSubmittedTime ,Offices.Office as office, Corres.Company as coname,

	BOffices.Office as Boffice,loanapp.CurrDecStatusDate as Decisionstatusdate,loanapp.CurrPipeStatusDate as CurrentStatusDate,

	loanapp.ChannelType as ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,

	CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,

	CASE TransType when NULL THEN '''' WHEN ''P'' THEN ''Purchase money first mortgage'' WHEN ''R'' THEN ''Refinance'' WHEN ''2'' THEN ''Purchase money second mortgage'' WHEN ''S'' THEN ''Second mortgage, any other purpose'' WHEN ''A'' THEN ''Assumption'' 
	WHEN ''HOP'' THEN ''HELOC - other purpose'' WHEN ''HP'' THEN ''HELOC - purchase'' ELSE '''' END AS TransType,

	mwllookups.displaystring,

	Case LoanData.FinancingType WHEN ''F'' THEN ''FHA''WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN ''VA'' End as LoanProgram,

	loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockDatetime as LockDatetime,LockRecord.LockExpirationDate,LockRecord.DeliveryOption,

	UnderwriterName as UnderWriter,Broker.Company as BrokerName,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,

	Loandata.LoanProgDesc as ProgramDesc,

	CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'' 

	and a.objOwner_ID IN (SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0

	WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') 

	and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0 ELSE (SELECT count(a.ID) as TotalCleared 

	FROM dbo.mwlCondition a where (DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') and a.objOwner_ID IN 

	(SELECT ID FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER

	,BOffices.Office as OFFICE_NAME1,isnull(Cust.StringValue,'''') as CorrespondentType

	,CASE when (ISNULL(CustN1.StringValue, '''') ='''' or CustN1.StringValue=''Other'') then '''' else CustN1.StringValue End  as CorrespodentSpecialist

	,ISNULL(CustN6.StringValue, ISNULL(CustN5.StringValue, ISNULL(CustN4.StringValue, ISNULL(CustN3.StringValue, ISNULL(CustN2.StringValue, ISNULL(CustN1.StringValue, ''Other'')))))) AS CorrespodentReview

	,Case when(ISNULL(CustFunding.StringValue, '''') ='''' or CustFunding.StringValue=''Other'') then '''' else CustFunding.StringValue End as CorrespodentFSpecialist '

	

	IF @URL_From = 'sendTofundingMgr' Or @URL_From = 'sendTofunding'

	Begin

		set @sql =  @sql + ' ,SendtoFundingDate.DateValue as SentToFundDate ' 

	End

	set @sql =  @sql + ' from mwlLoanApp loanapp Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and borr.Sequencenum=1 Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 

	left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id 

	and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id 

	and LockRecord.LockType=''LOCK'' and LockRecord.Status<>''CANCELED'' 

	Left join dbo.mwlInstitution as Offices on Offices.ObjOwner_id = loanapp.id and Offices.InstitutionType = ''Broker'' and Offices.objownerName=''Broker''

	Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = loanapp.id and COffices.InstitutionType = ''CORRESPOND'' and COffices.objownerName=''Contacts''

	left join mwllookups on mwllookups.Bocode = loanapp.transtype and mwllookups.objectname = ''mwlloanapp'' and mwllookups.fieldname = ''TransType''

	Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.InstitutionType = ''Branch'' and BOffices.objownerName=''BranchInstitution''

	Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''

	left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id and Cust.CustFieldDef_ID =''' +@CustomField+ '''

	left join mwlcustomfield as CustN1 on CustN1.loanapp_id = loanapp.id and CustN1.CustFieldDef_ID =''' +@CustomFieldN1+ '''

	left join mwlcustomfield as CustN2 on CustN2.loanapp_id = loanapp.id and CustN2.CustFieldDef_ID =''' +@CustomFieldN2+ '''

	left join mwlcustomfield as CustN3 on CustN3.loanapp_id = loanapp.id and CustN3.CustFieldDef_ID =''' +@CustomFieldN3+ '''

	left join mwlcustomfield as CustN4 on CustN4.loanapp_id = loanapp.id and CustN4.CustFieldDef_ID =''' +@CustomFieldN4+ '''

	left join mwlcustomfield as CustN5 on CustN5.loanapp_id = loanapp.id and CustN5.CustFieldDef_ID =''' +@CustomFieldN5+ '''

	left join mwlcustomfield as CustN6 on CustN6.loanapp_id = loanapp.id and CustN6.CustFieldDef_ID =''' +@CustomFieldN6+ '''

	left join mwlcustomfield as CustFunding on CustFunding.loanapp_id = loanapp.id and CustFunding.CustFieldDef_ID =''' +@CustomSendToFunding+ '''

	left join mwlcustomfield as CustDate on CustDate.loanapp_id = loanapp.id and CustDate.CustFieldDef_ID =''' +@SubmittedDateID+ '''

	left join mwlcustomfield as CustTime on CustTime.loanapp_id = loanapp.id and CustTime.CustFieldDef_ID =''' +@SubmittedDateTimeID+ ''' '

	

	set @sql =  @sql + ' where CurrentStatus between ''01 - Registered'' and ''62 - Funded'' '



	set @sql =  @sql + ' and LockRecord.DeliveryOption = ''Mandatory'' '



	IF @URL_From = 'sendTofundingMgr' OR @URL_From = 'sendTofunding'

	Begin

		set @sql =  @sql + ' and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' '

	End

	Else if (@Userole ='2' or @Userole ='8' or @Userole ='32')

	begin

		 set @sql =  @sql + 'and ChannelType in(''CORRESPOND'') and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and   (Originator_id in (select * from dbo.SplitString('''+@OriginatorId +''','','')))'

	end

	Else

	Begin

		set @sql =  @sql + ' and ChannelType in(''CORRESPOND'') and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' '	

	End

	  ----for director of wholesale

	if (@Userole ='1')

	begin

	  set @sql =  @sql + ' and ChannelType in(''CORRESPOND'') and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' 	and   (Originator_id in (select * from dbo.SplitString('''+@OriginatorId +''','','')))'

	end       

	IF @whereClause <> ''  

		BEGIN                                         

		set @sql = @sql  + ' AND ' + @whereClause                                   

		END                            

	set @sql = @sql + ') as t2 '         



	print @sql        

	exec sp_executesql @sql         

End       

set @sql = ''                                    

     set @sql = 'Select CASE  WHEN LoanStatus = ''54 - Purchase Pending'' 

									THEN CorrespodentSpecialist

							 WHEN LoanStatus = ''55 - Condition Review''

								 THEN CorrespodentReview

							 WHEN LoanStatus = ''57 - Sent to Funding''

								THEN CorrespodentFSpecialist END as AssignTO 

,* from ( '         

set @sql =  @sql + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ')AS Row,

''E3'' as DB,CustDate.DateValue as LastSubmittedDate,CustTime.StringValue as LastSubmittedTime ,Offices.Office as office, Corres.Company as coname,

BOffices.Office as Boffice,loanapp.CurrDecStatusDate as Decisionstatusdate,loanapp.CurrPipeStatusDate as CurrentStatusDate ,

loanapp.ChannelType as ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,

CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,

CASE TransType when NULL THEN '''' WHEN ''P'' THEN ''Purchase money first mortgage'' WHEN ''R'' THEN ''Refinance'' WHEN ''2'' THEN ''Purchase money second mortgage'' WHEN ''S'' THEN ''Second mortgage, any other purpose'' WHEN ''A'' THEN ''Assumption'' WHEN ''HOP'' THEN ''HELOC - other purpose'' WHEN ''HP'' THEN ''HELOC - purchase'' ELSE '''' END AS TransType,

mwllookups.displaystring,-- Newly added

Case LoanData.FinancingType WHEN ''F'' THEN ''FHA''WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN ''VA'' End as LoanProgram,

loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockDatetime as LockDatetime,CONVERT(varchar,LockRecord.LockDatetime,101) as LockDatetimeStr,LockRecord.LockExpirationDate,CONVERT(varchar,LockRecord.LockExpirationDate,101) as LockExpirationStr,LockRecord.DeliveryOption,

UnderwriterName as UnderWriter,Broker.Company as BrokerName,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,

Loandata.LoanProgDesc as ProgramDesc,

CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'' 

and a.objOwner_ID IN (SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0

WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') 

and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0 ELSE (SELECT count(a.ID) as TotalCleared 

FROM dbo.mwlCondition a where (DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') and a.objOwner_ID IN 

(SELECT ID FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER

,BOffices.Office as OFFICE_NAME1,isnull(Cust.StringValue,'''') as CorrespondentType

,CASE when (ISNULL(CustN1.StringValue, '''') ='''' or CustN1.StringValue=''Other'') then '''' else CustN1.StringValue End  as CorrespodentSpecialist

,ISNULL(CustN6.StringValue, ISNULL(CustN5.StringValue, ISNULL(CustN4.StringValue, ISNULL(CustN3.StringValue, ISNULL(CustN2.StringValue, ISNULL(CustN1.StringValue, ''Other'')))))) AS CorrespodentReview

,Case when(ISNULL(CustFunding.StringValue, '''') ='''' or CustFunding.StringValue=''Other'') then '''' else CustFunding.StringValue End as CorrespodentFSpecialist'



IF @URL_From = 'sendTofundingMgr' Or @URL_From = 'sendTofunding'

Begin

	set @sql =  @sql + ' ,SendtoFundingDate.DateValue as SentToFundDate ' 

End

set @sql =  @sql + ' from mwlLoanApp loanapp Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and borr.Sequencenum=1 Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 

left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id 

and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK'' and LockRecord.Status<>''CANCELED'' 

Left join dbo.mwlInstitution as Offices on Offices.ObjOwner_id = loanapp.id and Offices.InstitutionType = ''Broker'' and Offices.objownerName=''Broker'' 

Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = loanapp.id and COffices.InstitutionType = ''CORRESPOND'' and COffices.objownerName=''Contacts'' 

left join mwllookups on mwllookups.Bocode = loanapp.transtype and mwllookups.objectname = ''mwlloanapp'' and mwllookups.fieldname = ''TransType''

Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.InstitutionType = ''Branch'' and BOffices.objownerName=''BranchInstitution''

Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''

left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id and Cust.CustFieldDef_ID =''' +@CustomField+ '''

left join mwlcustomfield as CustN1 on CustN1.loanapp_id = loanapp.id and CustN1.CustFieldDef_ID =''' +@CustomFieldN1+ '''

left join mwlcustomfield as CustN2 on CustN2.loanapp_id = loanapp.id and CustN2.CustFieldDef_ID =''' +@CustomFieldN2+ '''

left join mwlcustomfield as CustN3 on CustN3.loanapp_id = loanapp.id and CustN3.CustFieldDef_ID =''' +@CustomFieldN3+ '''

left join mwlcustomfield as CustN4 on CustN4.loanapp_id = loanapp.id and CustN4.CustFieldDef_ID =''' +@CustomFieldN4+ '''

left join mwlcustomfield as CustN5 on CustN5.loanapp_id = loanapp.id and CustN5.CustFieldDef_ID =''' +@CustomFieldN5+ '''

left join mwlcustomfield as CustN6 on CustN6.loanapp_id = loanapp.id and CustN6.CustFieldDef_ID =''' +@CustomFieldN6+ '''

left join mwlcustomfield as CustFunding on CustFunding.loanapp_id = loanapp.id and CustFunding.CustFieldDef_ID =''' +@CustomSendToFunding+ '''

left join mwlcustomfield as CustDate on CustDate.loanapp_id = loanapp.id and CustDate.CustFieldDef_ID =''' +@SubmittedDateID+ '''

left join mwlcustomfield as CustTime on CustTime.loanapp_id = loanapp.id and CustTime.CustFieldDef_ID =''' +@SubmittedDateTimeID+ ''' '

  

 

set @sql =  @sql + ' where CurrentStatus between ''01 - Registered'' and ''62 - Funded'' '



 set @sql =  @sql + ' and LockRecord.DeliveryOption = ''Mandatory'' ' 



IF @URL_From = 'sendTofundingMgr' OR @URL_From = 'sendTofunding'

Begin

	set @sql =  @sql + ' and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' '

End

Else if (@Userole ='2' or @Userole ='8' or @Userole ='32')

begin

	set @sql =  @sql + 'and ChannelType in(''CORRESPOND'') and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and   (Originator_id in (select * from dbo.SplitString('''+@OriginatorId +''','','')))'

end 

Else

Begin

	set @sql =  @sql + ' and ChannelType in(''CORRESPOND'') and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' '	

End

 ----for director of wholesale

if @Userole ='1'

begin

  set @sql =  @sql + ' and ChannelType in(''CORRESPOND'') and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' and   (Originator_id in (select * from dbo.SplitString('''+@OriginatorId +''','','')))' 

end         

IF @whereClause <> ''                                             

BEGIN                                         

    set @sql = @sql  + ' AND ' + @whereClause                                   

END                          

                 

 set @sql = @sql + ' group by CustDate.DateValue,Offices.Office,Corres.Company,BOffices.Office,loanapp.CurrDecStatusDate ,  

 loanapp.CurrPipeStatusDate,loanapp.ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID,LoanNumber,CloserName,borr.lastname,  

 loanapp.CurrentStatus,mwllookups.DisplayString,TransType,LoanData.FinancingType,loanData.AdjustedNoteAmt,LockRecord.Status,LockRecord.LockDatetime,LockRecord.LockExpirationDate,LockRecord.DeliveryOption, UnderwriterName,  

 Broker.Company,borr.CompositeCreditScore ,Loandata.LoanProgDesc,CustTime.StringValue,Cust.StringValue,CustN1.StringValue,CustN2.StringValue,CustN3.StringValue,CustN4.StringValue,CustN5.StringValue,CustN6.StringValue,CustFunding.StringValue '

IF @URL_From = 'sendTofundingMgr' OR @URL_From = 'sendTofunding'

Begin

	set @sql =  @sql + ',SendtoFundingDate.DateValue'

End

  

  set @sql = @sql + ' )as t2'        

If @ShowAllData = 0

Begin

	set @sql = @sql + ' Where Row between (' + CAST( @PageNo AS NVARCHAR)  + ' - 1) * ' + Cast(@PageSize as nvarchar) + ' + 1 and ' + cast(@PageNo as nvarchar) + ' * ' + cast(@PageSize as nvarchar)        

End        

print @sql                                  

exec sp_executesql @sql        

        

END 


GO

