USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetStoneGateUsersExportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetStoneGateUsersExportData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblGetStoneGateUsersExportData]
as
begin
SELECT tblUsers.userid, tblUsers.ufirstname,tblUsers.MI, tblUsers.ulastname, tblUsers.companyname, tblUsers.uemailid,tblUsers.upassword as [Password], tblUsers.isactive, tblUsers.uidprolender, 
tblUsers.maxloginattempt,tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender, Carrier.Carrier,tblUsers.address, 
tblUsers.city,tblUsers.state,tblUsers.zip,tblUsers.phonenum,tblUsers.Address2,tblUsers.experience,tblUsers.fax,
tblUsers.photoname,tblUsers.logoname,tblUsers.FaceBookID,tblUsers.TwitterID,tblUsers.bio,
tblUsers.AreaOfExpertise,tblUsers.userLoginid,
Case When tblUsers.BranchID=0 then '' when tblUsers.BranchID=1 then 'Fishers, IN 46038' when tblUsers.BranchID=2 then 'Indianapolis, IN 46227' when tblUsers.BranchID=3 then 'Mansfield, OH 44901' 
when tblUsers.BranchID=4 then 'Delaware, OH 43015' when tblUsers.BranchID=5 then 'Overland Park, KS 66210' end as Branch,roles as uurole
 ,BDMUser.UfirstName + ' ' + BDMUser.uLastname as BDM, tblUsers.PartnerCompanyid,case when tblUsers.isadmin=1 then 'Yes' else 'No' end as IsAdmin, tblUsers.SignedUpDate as SignUpDate
 FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID 
 LEFT OUTER JOIN tblroles Roles ON tblUsers.urole = Roles.id 
 left outer join tblusers BDMUser on tblusers.AEID=BDMUser.Userid
 order by  tblUsers.userid desc
end


GO

