USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateLoanProgram_E3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateLoanProgram_E3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_UpdateLoanProgram_E3] 
	-- Add the parameters for the stored procedure here
@LoanAppID varchar(50)=NULL,
@strCustFieldDefLoanID varchar(100),
@strLoanProgram varchar(200)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


	BEGIN
	if not exists(select * From mwlcustomfield where CustFieldDef_ID in (@strCustFieldDefLoanID) and  LoanApp_id in(@strCustFieldDefLoanID))
	 insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue)values 
	 (replace(newid(),'-',''),@LoanAppID,@strCustFieldDefLoanID,@strLoanProgram)ELSE
	 update mwlcustomfield set StringValue =@strLoanProgram  WHERE
	  CustFieldDef_ID=@strCustFieldDefLoanID
	   and LoanApp_id=@LoanAppID
	
	END
	
	
END
GO

