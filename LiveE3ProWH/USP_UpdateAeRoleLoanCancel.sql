USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_UpdateAeRoleLoanCancel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_UpdateAeRoleLoanCancel]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_UpdateAeRoleLoanCancel]
(
@LoanNumber  varchar(15),
@CurrentStatus varchar(30)
)

AS
BEGIN

BEGIN TRY
update mwlLoanApp
set CurrentStatus=@CurrentStatus
 where ltrim(rtrim(LoanNumber)) = ltrim(rtrim(@LoanNumber))

 END TRY

 BEGIN CATCH
 
    Declare @Msg nvarchar(max)
    Select @Msg=Error_Message();
    RaisError('Error Occured: %s', 20, 101,@Msg);

 END CATCH

END 


GO

