USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetEscrowsInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetEscrowsInformation]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                 
-- Author:<Gourav Goel>                     
-- Create date: <09/11/2012>                 
-- Description:  < This will return the value for "Docs On OLIE Step-5 Escrows Information" on the basis of LoanNumber>                 
-- Update by : Kanva (For DOO 1.1)
-- Update on: 12/27/2012 
-- MWFieldNum = 9627 - Hazard Insurance Deductible
-- MWFieldNum = 9842 - Next Taxes Due
-- =============================================                 
CREATE PROCEDURE [dbo].[udsp_GetEscrowsInformation]  
  @LoanNumber VARCHAR(10)               
AS               
  BEGIN                                
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
		SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,         
                   -- the entire transaction is terminated and rolled back.
		DECLARE @intHazardInsuranceDeductible AS INT 
		SET @intHazardInsuranceDeductible = 9627  --MWFieldNum pointing to "Hazard Insurance Deductible"
		DECLARE @intNextTaxesDue AS INT 
		SET @intNextTaxesDue = 9842 --MWFieldNum pointing to "Next Taxes Due"
  SELECT mwlloanapp.dateofdocuments, 
		mwlinstitution.company, 
		mwlinstitution.street, 
		mwlinstitution.street2, 
		mwlinstitution.city, 
		mwlinstitution.state, 
		mwlinstitution.zipcode, 
		mwlinstitution.country, 
		mwlinstitution.companyemail, 
		Hzdphonenumber.phonenumber, 
		HzdFaxnumber.phonenumber                 AS FaxNumber, 
		mwlhazard.policynumber, 
		mwlhazard.hazardinscoverage, 
		prepaid.annualpayduedate, 
		custfield.currencyvalue                  AS currencyvalue,
		loandata.firstpaydate                    AS DisbursementDate, 
		TaxesNextPayment.datevalue               AS TaxtNextPaymentDate,
		CityTaxes.includeinpayment               AS CityIncludeInPayment, 
		CityTaxes.annualamt                      AS CityAnnualAmount, 
		mwlppe.citytaxesallocevery, 
		mwlppe.citytaxesstartingin, 
		CityTaxes.pmtnumber                      AS CityPmtNumber, 
		CountryTaxes.includeinpayment            AS CountryIncludePayment, 
		CountryTaxes.annualamt                   AS CoutryAnnualAmount, 
		CountryTaxes.pmtnumber                   AS CountryPmtNumber, 
		mwlppe.countytaxallocevery, 
		mwlppe.countytaxstartingin, 
		CountryTaxes.pmtnumber                   AS CountyPmtNumber, 
		AssementAnnualAmt.includeinpayment       AS AssessmentIncludeInPayment, 
		AssementAnnualAmt. annualamt             AS AssessmentAnnualAMount, 
		mwlppe.assessallocevery, 
		mwlppe.assessstartingin, 
		AssementAnnualAmt.pmtnumber              AS AssementPmtNumber, 
		hzdinsurace.includeinpayment             AS HazardIncludeInPayment, 
		hzdinsurace.annualamt   				 AS HazardAnnualAmount,       
		mwlppe.hazardinsallocevery, 
		mwlppe.hazardinsstartingin, 
		hzdinsurace.pmtnumber					 AS HazardPmtNumber, 
		FloodInsuranceinPayment.includeinpayment AS FloodIncludeInPayment, 
		FloodInsuranceinPayment.annualamt        AS FloodAnnualAmount, 
		mwlppe.floodinsallocevery                AS FloodTaxesAllocEvery, 
		mwlppe.floodinsstartingin                AS FloodTaxesStartingin, 
		FloodInsuranceinPayment.pmtnumber        AS FloodPmtNumber, 
		Other1Desc.annualamt                     AS Other1Amt, 
		Other1Desc.huddesc                       AS Other1Desc, 
		mwlppe.otherppe1allocevery               AS Other1DueEvery, 
		mwlppe.otherppe1startingin               AS Other1StartingIn, 
		Other1Desc.includeinpayment              AS Other1IncludeIn, 
		mwlppe.otherppe1reserves                 AS Other1PmtNumber, 
		Other2Desc.annualamt                     AS Other2Amt, 
		Other2Desc.huddesc                       AS Other2Desc, 
		mwlppe.otherppe2allocevery               AS Other2DueEvery, 
		mwlppe.otherppe2startingin               AS Other2StartingIn, 
		Other2Desc.includeinpayment              AS Other2IncludeIn, 
		Other2Desc.pmtnumber                     AS Other2PmtNumber, 
		Other3Desc.annualamt                     AS Other3Amt, 
		Other3Desc.huddesc                       AS Other3Desc, 
		mwlppe.otherppe3allocevery               AS Other3DueEvery, 
		mwlppe.otherppe3startingin               AS Other3StartingIn, 
		Other3Desc.includeinpayment              AS Other3IncludeIn, 
		mwlppe.otherppe3reserves                 AS Other3PmtNumber, 
		Other4Desc.annualamt                     AS Other4Amt, 
		Other4Desc.huddesc                       AS Other4Desc, 
		mwlppe.otherppe4allocevery               AS Other4DueEvery, 
		mwlppe.otherppe4startingin               AS Other4StartingIn, 
		Other4Desc.includeinpayment              AS Other4IncludeIn, 
		mwlppe.otherppe4reserves                 AS Other4PmtNumber, 
		Other5Desc.annualamt                     AS Other5Amt, 
		Other5Desc.huddesc                       AS Other5Desc, 
		mwlppe.otherppe5allocevery               AS Other5DueEvery, 
		mwlppe.otherppe5startingin               AS Other5StartingIn, 
		Other5Desc.includeinpayment              AS Other5IncludeIn,
		mwlppe.otherppe5reserves                 AS Other5PmtNumber,			
		loandata.waiveescrows					 AS WaiveEscrows
FROM   mwlloanapp 
       INNER JOIN dbo.mwlloandata loandata 
               ON dbo.mwlloanapp.id = loandata.objowner_id 
			   AND loandata.Active = 1
       LEFT JOIN mwlsubjectproperty 
              ON mwlsubjectproperty.loanapp_id = mwlloanapp.id 
       LEFT JOIN mwlhazard 
              ON mwlhazard.owner_id = mwlsubjectproperty.id 
       LEFT JOIN mwlinstitution 
              ON mwlinstitution.objowner_id = mwlhazard.id 
                 AND objownername = 'HazardInstitution' 
       LEFT JOIN mwlprepaid prepaid 
              ON prepaid.loandata_id = loandata.id 
                 AND prepaid.hudlinenum = '903' 
       -- HUD Line# 903 refers to Hazard insurance premium        
       LEFT JOIN mwlphonenumber Hzdphonenumber 
              ON mwlinstitution.id = Hzdphonenumber.objowner_id 
                 AND Hzdphonenumber.phonetype = 'Business' 
       LEFT JOIN mwlphonenumber HzdFaxnumber 
              ON mwlinstitution.id = HzdFaxnumber.objowner_id 
                 AND HzdFaxnumber.phonetype = 'Fax' 
       LEFT JOIN mwlcustomfield AS custfield 
              ON mwlloanapp.id = custfield.loanapp_id 
                 AND custfield.custfielddef_id = (SELECT id 
                                                  FROM   mwscustfielddef 
                                                  WHERE 
                     mwfieldnum = @intHazardInsuranceDeductible 
                                                 -- MWFieldNum pointing to "Hazard Insurance Deductible" 
                                                 ) 
       LEFT JOIN mwlprepaid propertytax 
              ON propertytax.loandata_id = loandata.id 
                 AND propertytax.hudlinenum = '1004' 
                 -- HUD Line# 1004 refers Property taxes 
                 AND propertytax.huddesc = 'Property taxes' 
       LEFT JOIN mwlhudlinecomponent CityTaxes 
              ON CityTaxes.objowner_id = propertytax.id 
                 AND CityTaxes.description = 'IQ - City taxes' 
       LEFT JOIN mwlhudlinecomponent CountryTaxes 
              ON CountryTaxes.objowner_id = propertytax.id 
                 AND CountryTaxes.description = 'IQ - County taxes' 
       LEFT JOIN mwlppe 
              ON dbo.mwlppe.loandata_id = loandata.id 
       LEFT JOIN mwlprepaid hzdinsurace 
              ON hzdinsurace.loandata_id = loandata.id 
                 AND hzdinsurace.hudlinenum = '1002' 
       LEFT JOIN mwlprepaid AssementAnnualAmt 
              ON AssementAnnualAmt.loandata_id = loandata.id 
                 AND AssementAnnualAmt.hudlinenum = '1005' 
       LEFT JOIN mwlprepaid FloodInsuranceinPayment 
              ON FloodInsuranceinPayment.loandata_id = loandata.id 
                 AND FloodInsuranceinPayment.hudlinenum = '1006' 
       -- HUD Line# 1006 refers Flood insurance reserves 
       LEFT JOIN mwlprepaid Other1Desc 
              ON Other1Desc.loandata_id = loandata.id 
                 AND Other1Desc.hudlinenum = '905' 
       -- HUD Line# 905 refers Other#1 Description 
       LEFT JOIN mwlprepaid Other2Desc 
              ON Other2Desc.loandata_id = loandata.id 
                 AND Other2Desc.hudlinenum = '1008' 
       -- HUD Line# 1008 refers Other#2 Description     
       LEFT JOIN mwlprepaid Other3Desc 
              ON Other3Desc.loandata_id = loandata.id 
                 AND Other3Desc.hudlinenum = '906' 
       -- HUD Line# 906 refers Other#3 Description           
       LEFT JOIN mwlprepaid Other4Desc 
              ON Other4Desc.loandata_id = loandata.id 
                 AND Other4Desc.hudlinenum = '907' 
       LEFT JOIN mwlprepaid Other5Desc 
              ON Other5Desc.loandata_id = loandata.id 
                 AND Other5Desc.hudlinenum = '908' 
       -- HUD Line# 908 refers Other#5 Description     
       LEFT JOIN mwlcustomfield AS TaxesNextPayment 
              ON mwlloanapp.id = TaxesNextPayment.loanapp_id 
                 AND TaxesNextPayment.custfielddef_id = (SELECT id 
                                                         FROM   mwscustfielddef 
                                                         WHERE 
                     mwfieldnum = @intNextTaxesDue 
                                                        -- Custom field ID for Next Taxes Due
                                                        ) 
	WHERE  loannumber = @LoanNumber      
  --Get Escrow DueDate   
	SELECT * FROM tblEscrowDueDates WHERE LoanNumber = @LoanNumber ORDER BY EscrowItem,[Order]
  --End Get Escrow DueDate
	--Start Get Countries Added for DOO2.0
	SELECT cntdesc From tblCountry order by cntdesc  
	--End Get Countries Added for DOO2.0
	--Check id Building is in a special flood hazard Area
	DECLARE @SpecialFloodHazardArea AS BIT
	select @SpecialFloodHazardArea = specialfloodhazardarea from mwlflood
	where owner_id =(select id from mwlsubjectproperty 
										where loanapp_id = (select id from mwlloanapp 
																				where loannumber = @LoanNumber))
	select @SpecialFloodHazardArea AS specialfloodhazardarea
	--End Check id Building is in a special flood hazard Area
	--Get Flood Insurance Company Information
	if(@SpecialFloodHazardArea = 1)
	BEGIN
		SELECT  Institution.company			AS Company,
			Institution.street				AS Street,
			Institution.street2				AS Street2,
			Institution.City				AS City,
			Institution.State				AS State,
			Institution.zipcode				AS Zipcode,
			Institution.country				AS Country,
			Floodphonenumber.phonenumber	AS PhoneNumber,
			FloodFaxnumber.phonenumber		AS FaxNumber,
			Institution.companyemail		AS CompanyEmail,
			Flood.floodpolicynumber			AS FloodPolicyNumber,	
			Flood.FloodInsCoverage			AS FloodInsCoverage,
			Flood.FloodCertDate				AS FloodCertDate,
			Flood.FloodZoneId				AS FloodZoneId
		From mwlLoanApp LoanApp
			INNER JOIN dbo.mwlLoanData LoanData ON LoanApp.ID = LoanData.ObjOwner_ID
			Left join mwlSubjectProperty SubjectProperty on SubjectProperty.LoanApp_ID = LoanApp.ID
			LEFT JOIN mwlflood Flood ON Flood.Owner_ID = SubjectProperty.ID		
			LEFT JOIN mwlinstitution Institution on Institution.objOwner_id= Flood.id AND Institution.objownername = 'RelatedInstitution'
			LEFT JOIN mwlPhoneNumber Floodphonenumber ON Institution.id=Floodphonenumber.ObjOwner_ID AND Floodphonenumber.phonetype ='Business'
			LEFT JOIN mwlPhoneNumber FloodFaxnumber ON Institution.id=FloodFaxnumber.ObjOwner_ID AND FloodFaxnumber.phonetype ='Fax'
		Where LoanNumber = @LoanNumber
	END
	--End Get Flood Insurance Company Information
-- Create Procedure Coding ENDS Here
END


GO

