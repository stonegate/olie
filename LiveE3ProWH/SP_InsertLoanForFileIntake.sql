USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_InsertLoanForFileIntake]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_InsertLoanForFileIntake]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

Create PROCEDURE [dbo].[SP_InsertLoanForFileIntake]
	(
@strLoanNumber nvarchar(200),
@iUserId int
)
AS
BEGIN
	Insert into tblFileIntake(LoanNumber,IsEmailSent,UserId,AddDate) 
    Values (@strLoanNumber,'1',@iUserId, getdate())

END

GO

