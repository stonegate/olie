USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLockConfirmQueue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLockConfirmQueue]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetLockConfirmQueue]
as
begin
select Id,LoanApp_ID,StatusID,RetryCount,DateCreated from tblLockConfirmQueue where statusid=1 and RetryCount < 5
order by datecreated
END


GO

