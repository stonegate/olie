USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblFetchQuickLinks]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblFetchQuickLinks]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 
-- Description:	GetBusineesType_Role for clsPipeline.cs
-- =============================================
create PROCEDURE [dbo].[sp_Select_tblFetchQuickLinks]
AS
BEGIN
select * From tblQuickLinks
END


GO

