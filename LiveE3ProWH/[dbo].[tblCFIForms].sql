USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCFIForms]') AND type in (N'U'))
DROP TABLE [dbo].[tblCFIForms]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCFIForms](	  [formID] INT NOT NULL IDENTITY(1,1)	, [Description] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FileName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [dttime] DATETIME NULL	, [isnews] BIT NULL	, [CateID] INT NULL DEFAULT((0)))USE [HomeLendingExperts]
GO

