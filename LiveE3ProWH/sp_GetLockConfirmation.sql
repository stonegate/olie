USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLockConfirmation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLockConfirmation]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_GetLockConfirmation]      
@LoanNumber varchar(50)      
as
begin      


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

 select LoanApp.LoanNumber,LockRecord.LockDateTime,CONVERT(DECIMAL(10,2),LockRecord.AppraisedValue) as AppraisedValue,      
 LockRecord.Occupancy,LockRecord.PropertyType,LockRecord.Units,LockRecord.RefiPurpose ,      
 CONVERT(DECIMAL(10,2),LockRecord.AdjustedNoteAmt) as AdjustedNoteAmt,CONVERT(DECIMAL(10,3),LockRecord.LTV * 100) as LTV,LockRecord.LoanProgramName,      
 LockRecord.TransactionType,convert(DECIMAL(10,3),LockRecord.Margin) as Margin,convert(DECIMAL(10,3),LockRecord.LockRate * 100) as LockRate,      
 LockRecord.LockDays,LockRecord.LockExpirationDate,borr.LastName,borr.FirstName,borr.CompositeCreditScore ,      
 CONVERT(DECIMAL(10,2),LoanData.MIPremiumAmt) as MIPremiumAmt,LoanData.OtherFinMonthly,LoanData.WaiveEscrows,      
 SubjectProperty.Street,SubjectProperty.Street2,SubjectProperty.City,SubjectProperty.State,SubjectProperty.Zipcode,      
 convert(DECIMAL(10,3),PricingResult.LenderClosingPrice * 100) as LenderClosingPrice , CASE Midata.PremiumSource WHEN 'BORROWER' THEN 'Borrower Paid' WHEN 'LENDER' THEN 'Lender Paid' ELSE '' END AS PremiumSource,LockRecord.DeliveryOption    
 from mwlLoanApp LoanApp      
 Inner join mwlBorrower as borr on borr.loanapp_id = LoanApp.id AND BORR.SEQUENCENUM=1          
 Left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = LoanApp.id       
 and locktype='LOCK'      
 inner join mwlLoanData as LoanData on  LoanData .ObjOwner_id =LoanApp.id      
 inner join mwlSubjectProperty as SubjectProperty on SubjectProperty.LoanApp_ID  =LoanApp.id      
 LEFT OUTER JOIN mwlPricingResult as PricingResult ON PricingResult.ObjOwner_ID=LockRecord.ID      
   LEFT OUTER JOIN mwlmidata as Midata ON Midata.LoanData_id=LoanData.ID     
 where LoanApp.LoanNumber=@LoanNumber      
end

GO

