USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteUnderwriter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteUnderwriter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:      <Chandrika>    
-- Create date: <4-Sept-2012>    
-- Description: <Delete Underwriter>    
-- =============================================  
CREATE PROCEDURE [dbo].[sp_DeleteUnderwriter]   
@UserID int  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
 -- Insert statements for procedure here  
 BEGIN  
   
 delete from tblOffices Where lt_office_id ='' and lt_usr_underwriter <> '' and userid =@UserID  
   
 END  
   
   
END
GO

