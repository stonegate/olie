USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpFetchStateNameFromAbbr]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpFetchStateNameFromAbbr]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpFetchStateNameFromAbbr]
	@Abbr varchar(1000)=null
AS
BEGIN
	 select name from dbo.tblState where abbr =@Abbr
END

GO

