USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDooErrorLog]') AND type in (N'U'))
DROP TABLE [dbo].[tblDooErrorLog]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDooErrorLog](	  [ID] INT NOT NULL IDENTITY(1,1)	, [LoanNumber] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ErrorDesc] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ErrorTime] DATETIME NULL DEFAULT(getdate())	, [Request] XML NULL	, [Response] XML NULL	, [PartnerCompID] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblDooErrorLog] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO

