USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Delete_tblDeletePermanent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Delete_tblDeletePermanent]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Delete_tblDeletePermanent]
(
@iUserID varchar(Max)
)
AS
BEGIN
	-- Delete the users from dependent tables first
DELETE FROM tblUserChannels WHERE UserId = @iUserID
DELETE FROM tblUserManagers WHERE ManagerId = @iUserID
DELETE FROM tblUserRegions WHERE UserId = @iUserID
DELETE FROM tblUserStates WHERE UserId = @iUserID

Delete From tblusers where userid = @iUserID
IF EXISTS(SELECT * FROM tblCRSAssociation WHERE CRSUserId=@iUserID)
      DELETE FROM tblCRSAssociation WHERE CRSUserId = @iUserID 
IF EXISTS(SELECT * FROM tblCRSMAssociation WHERE CRSMUserId=@iUserID)
      DELETE FROM tblCRSMAssociation WHERE CRSMUserId = @iUserID 
END

GO

