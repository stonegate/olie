USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpInsertPriorToPurchase]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpInsertPriorToPurchase]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Create date: <10/9/2012>  
-- Description: <Insert record to the PriorToPurchase>  
-- =============================================  
CREATE PROCEDURE [dbo].[SpInsertPriorToPurchase]   
 @LoanNo varchar(200),  
 @ConditionText nvarchar(2000),  
 @FileName varchar(1000)=null,  
 @ConRecId varchar(500)=null,  
 @ProlendDocDesc nvarchar(1000)=null,  
 @UserId int  
AS  
BEGIN  
 Insert into tblCondText_History(Loan_No,Cond_Text,FileName,dttime,condrecid,ProlendImageDesc,Userid,PriorToPurchaseDate)   
 Values(@LoanNo,@ConditionText,@FileName,getdate(),@ConRecId,@ProlendDocDesc,@UserId,getdate())  
END  
GO

