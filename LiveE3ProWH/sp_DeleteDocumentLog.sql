USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteDocumentLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteDocumentLog]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/19/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
Create proc [dbo].[sp_DeleteDocumentLog]
(
@loanregid int,
@ctid int,
@Scheduleid int,
@loandocid int

)
as
begin

update tblLoanReg set IsDeletedFromDocLog = 0 
where loanregid = @loanregid ; 
update tblCondText_History set IsActive= 0 
where ctid = @ctid ; 
update tblScheduleClosing set IsActive= 0
 where Scheduleid = @Scheduleid ; 
update tblloandoc set IsDeletedFromDocLog = 0 
where loandocid=@loandocid
end



GO

