USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetGridConditionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetGridConditionData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
            
-- =============================================            
-- Author:  <Nirav>            
-- Create date: <10/8/2012>            
-- Description: <fetch Grid condition data  of loan number>            
-- =============================================            
CREATE PROCEDURE [dbo].[SpGetGridConditionData]-- '0000345507', ''        
 @LoanNo varchar(500),            
 @LoanStatus varchar(1000),            
 @DecisionStatus varchar(1000)            
AS            
BEGIN           
	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads.  

 Declare @SQL nvarchar(max)                
Set @SQL =''                
Set @SQL = ' SELECT Name as descriptn,DisplayOrder as id_value ,case DisplayOrder when ''4'' then ''1''  when ''1'' then ''2'' when ''3'' then ''4''  when ''2'' then ''3'' when ''5'' then ''5'' when ''9'' then ''6''END AS ''Value''             
             FROM mwsConditionDueBy '          
                       
     if (@DecisionStatus ='0')            
       begin            
         Set @SQL = @SQL + ' where  Name = ''Prior to Underwriting'''           
                  
       end          
               
                 
     --if (@DecisionStatus ='1')            
     --  begin            
     --    if (@LoanStatus <> '')            
     --     begin            
     --       if (@LoanStatus <> '54 - Purchase Pending')            
     --        begin            
     --         Set @SQL = @SQL + ' where Name <> ''Prior to Purchase'''            
     --        end     
     --         if (@LoanStatus = '54 - Purchase Pending' AND @LoanStatus <> '51 - Purchase Sub on Hold')            
     --        begin            
     --         Set @SQL = @SQL + ' where Name <> ''Purchase on Hold'''            
     --        end        
     --        if (@LoanStatus <> '51 - Purchase Sub on Hold' AND @LoanStatus <> '54 - Purchase Pending' )            
     --        begin            
     --         Set @SQL = @SQL + ' and Name <> ''Purchase on Hold'''            
     --        end         
                      
     --      if (@LoanStatus = '54 - Purchase Pending' or @LoanStatus = '58 - Cleared for Funding' or @LoanStatus ='56 - Purch Conditions Received')            
     --        begin            
     --         Set @SQL = @SQL + ' '            
     --        end            
     --    end            
     --  end            
        
        
 Set @SQL = @SQL + ' select ''E3'' as DB,OrderNum as cond_no,a.ID as record_id,a.ObjOwner_Id as parent_id,a.Description as cond_text,            
CurrentState as STATUS,Category as descriptn ,DueBy,             
a.CreatedOnDate as cond_cleared_date,'''' as  cond_recvd_date, b.DisplayOrder as id_value ,a.ShortName FROM dbo.mwlCondition a             
left outer join mwsConditionDueBy b on a.Dueby = b.Name Where '            
--if (@DecisionStatus ='1')            
--begin            
-- if (@LoanStatus <> '')            
-- begin            
--  if (@LoanStatus <> '54 - Purchase Pending')            
--  begin          
          
--   Set @SQL = @SQL + ' DueBy <> ''Prior to Purchase'' and '            
--  end            
--  if (@LoanStatus <> '51 - Purchase Sub on Hold')            
--    begin            
           
--     Set @SQL = @SQL + ' DueBy <> ''Purchase on Hold'' and'            
--    end     
            
--  if (@LoanStatus = '54 - Purchase Pending' or @LoanStatus = '58 - Cleared for Funding' or @LoanStatus ='56 - Purch Conditions Received')            
--  begin            
--   Set @SQL = @SQL + ' '            
--  end            
-- end            
--end            
 if (@DecisionStatus ='0')            
begin            
 Set @SQL = @SQL + ' DueBy = ''Prior to Underwriting'' and  '            
end            
Set @SQL = @SQL + ' a.ObjOwner_Id IN ( SELECT id FROM dbo.mwlLoanApp WHERE loannumber='''+ @LoanNo +''')'            
Set @SQL = @SQL + ' ORDER BY OrderNum asc'            
exec sp_executesql @SQL             
Print @SQL                
          
            
END   

GO

