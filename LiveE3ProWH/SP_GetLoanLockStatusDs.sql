USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetLoanLockStatusDs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetLoanLockStatusDs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

CREATE PROCEDURE [dbo].[SP_GetLoanLockStatusDs]
(
@strLoanNo nvarchar(250)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

   select top 1 lockRecord.status,lockRecord.lockdatetime as lockdate 
    from mwlloanapp as loanApp 
   left join mwlLockRecord as lockRecord on loanApp.id = lockRecord.loanApp_ID 
   where loannumber = @strLoanNo
   order by lockRecord.createdOnDate desc 	

END

-- Create Procedure Coding ENDS Here

GO

