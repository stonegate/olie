USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetProUserDataByRecID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetProUserDataByRecID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetProUserDataByRecID]
	@RecId varchar(500)
AS
BEGIN
	select * from users where record_id =@RecId
END

GO

