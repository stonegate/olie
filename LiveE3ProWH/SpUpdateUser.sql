USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpUpdateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpUpdateUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpUpdateUser]
	@UserId int=0,
	@UFirstname varchar(500)=null,
	@ULastName  varchar(500)=null,
	@UEmailId varchar(500)=null,
	@StateBelongTo varchar(500)=null,
	@Urole int=0,
	@IsAdmin int=0,
	@UidProlender varchar(200)=null,
	@Uparent int=0,
	@MobilePh nvarchar(500)=null,
	@Phonenum nvarchar(500)=null,
	@LoginIdProlender varchar(500)=null,
	@Carrerid int =null,
	@MI varchar(500),
	@CompanyName varchar(500) =null,
	@AEID int,
	@Region varchar(500)=null,
	@Upassword nvarchar(500)
AS
BEGIN
update tblusers Set 
ufirstname = @UFirstname,
ulastname =@ULastName,
uemailid=@UEmailId,
StateBelongTo =@StateBelongTo,
urole =@Urole,
isadmin =@IsAdmin,
uidprolender =@UidProlender,
uparent =@Uparent,
mobile_ph = @MobilePh,
phonenum =@Phonenum,
loginidprolender =@LoginIdProlender,
carrierid = @Carrerid,
MI =@MI,
companyname = @CompanyName,
AEID =@AEID,
Region=@Region,
upassword =@Upassword
Where userid =@UserId
END

GO

