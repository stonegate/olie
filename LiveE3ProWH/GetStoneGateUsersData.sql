USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetStoneGateUsersData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetStoneGateUsersData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  Gourav      
-- Create date: 10/03/2012      
-- Description: Procedure for clsUser.cs bll     
--  Modify by : Shyamal Gajjar    
-- Modify Date :  15/03/2013    
--Modify by : Shayaml    
---Description: Procedure is changed for reflect newly added field 'IsKatalsyt'        
-- =============================================    
          
CREATE PROCEDURE [dbo].[GetStoneGateUsersData]       
 -- Add the parameters for the stored procedure here      
 @urole int,      
 @userid int      
       
AS
BEGIN      
   -- SET NOCOUNT ON added to prevent extra result sets from      
   -- interfering with SELECT statements.      
   SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
   SET NOCOUNT ON;      
      
DECLARE @ADMINROLE INT  
SET   @ADMINROLE=0  
  
DECLARE @DIRECTOROFCFI INT  
SET   @DIRECTOROFCFI=25  
  
DECLARE @SALESDIRECTOR INT  
SET   @SALESDIRECTOR=26  
  
DECLARE @BROKER INT  
SET   @BROKER=3  
  
DECLARE @CORRESPONDENT INT  
SET   @CORRESPONDENT=20  
  
DECLARE @HYBRID INT  
SET   @HYBRID=21  

DECLARE @MANDATORYDELIVERYADMIN INT  
SET   @MANDATORYDELIVERYADMIN=37
   -- Insert statements for procedure here      
  if @urole = @ADMINROLE       
        
        
     begin      
      SELECT tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole,   
       tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent, tblUsers.upassword,  
       tblUsers.maxloginattempt, tblUsers.Lastlogintime,    
       tblUsers.mobile_ph,isnull(tblusers.IsKatalyst,0) IsKatalyst, tblUsers.loginidprolender,   
       tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,tblUsers.PartnerCompanyid,tblusers.AEID,  
       IsUserInNewProcess,tblusers.userLoginid FROM tblUsers  WITH (nolock)    
       LEFT OUTER JOIN Carrier WITH ( NOLOCK )   ON tblUsers.carrierid = Carrier.ID   
       LEFT OUTER JOIN tblRoles WITH ( NOLOCK )    ON tblUsers.urole = tblRoles.ID   
       where urole in(@DIRECTOROFCFI,@SALESDIRECTOR,@MANDATORYDELIVERYADMIN) 
       or (urole in(@BROKER,@CORRESPONDENT,@HYBRID) and PartnerType='CFI')  
       order by  tblUsers.userid desc      
     end      
        
  else      
  
        begin      
        
      SELECT tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole,   
                     tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent, tblUsers.upassword,   
                     tblUsers.maxloginattempt, tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender,   
                     tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles,tblUsers.PartnerCompanyid,tblusers.AEID,  
                     IsUserInNewProcess,userLoginid ,isnull(tblusers.IsKatalyst,0) IsKatalyst   
                     FROM tblUsers  WITH (nolock)  LEFT OUTER JOIN Carrier WITH (nolock) ON tblUsers.carrierid = Carrier.ID  
                     LEFT OUTER JOIN tblRoles WITH (nolock) ON tblUsers.urole = tblRoles.ID       
      where PartnerCompanyid in(Select PartnerCompanyid from tblusers WITH (nolock) where       
      userid =@userid)       
      and (userid not in(@userid) or (CreatedBy =@userid))   
                     and tblusers.urole=@urole and PartnerType='CFI'   
                     order by  tblUsers.userid desc      
    end      
END

GO

