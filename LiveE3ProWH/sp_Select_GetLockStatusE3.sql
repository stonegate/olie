USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_GetLockStatusE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_GetLockStatusE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[sp_Select_GetLockStatusE3]
as
begin
select '0' as id_value, '--Select LockStatus--' as descriptn union  select id_value ,descriptn from lookups where lookup_id='AAT' and descriptn in('REGISTERED','LOCKED')
end

GO

