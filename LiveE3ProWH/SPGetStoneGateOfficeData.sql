USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPGetStoneGateOfficeData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPGetStoneGateOfficeData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Stonegate>  
-- Create date: <10/5/2012>  
-- Description: <Get all Stonegate Office Data>  
-- =============================================  
CREATE PROCEDURE [dbo].[SPGetStoneGateOfficeData]   
  @LodingId varchar(500)  
AS  
BEGIN  
 select * from tblOffices where userid =@LodingId  
END  
GO

