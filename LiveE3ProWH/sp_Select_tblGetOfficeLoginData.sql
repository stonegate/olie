USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetOfficeLoginData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetOfficeLoginData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblGetOfficeLoginData]
(
@sOfficeid varchar(Max),
@sProuid varchar(Max)
,@iUserid varchar(Max)
)
as
begin
select count (*) as Total from tbloffices 
where lt_office_id =@sOfficeid 
and prouid =@sProuid and userid =@iUserid
end



GO

