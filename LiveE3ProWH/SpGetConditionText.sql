USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetConditionText]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetConditionText]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================      
-- Author:  <Stonegate>      
-- Create date: <10/5/2012>      
-- Description: <Get Condition text data>      
-- =============================================      
CREATE PROCEDURE [dbo].[SpGetConditionText]     
(      
 @RecordId varchar(max)    
      
)      
AS      
BEGIN      
   
   SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
      
Declare @SQL nvarchar(max)      
Set @SQL =''      
Set @SQL = ' SELECT Description as cond_text FROM dbo.mwlCondition    
 WHERE ID in (select * from dbo.SplitString(''' +@RecordId+''','',''))'    
    
    
Print @SQL      
exec sp_executesql @SQL      
END   
GO

