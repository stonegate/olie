USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_UserReportees]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Get_UserReportees]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author, Somnath M. R.>
-- Create date: <Create Date, 09th Dec 2013>
-- Description:	<Description, Select from tblUserManagers table>
-- EXEC Get_UserReportees '16527'
-- =============================================
CREATE PROCEDURE [dbo].[Get_UserReportees]
	@ManagerId VarChar(Max),
	@ChannelIds VarChar(Max) = NULL,
	@ReporteeRoleIds VarChar(Max) = NULL,
	@IsActive Bit = Null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT DISTINCT 
		UM.UserId, 
		U.ufirstname + ' ' + U.ulastname As UserName, 
		U.E3Userid,
		U.urole 
	FROM [dbo].[tblUserManagers] UM
	INNER JOIN [dbo].[tblUsers] U ON U.UserId = UM.UserId
	INNER JOIN [dbo].[tblUserChannels] UC ON UC.UserId = UM.UserId OR @ChannelIds IS NULL
	WHERE 
		UM.ManagerId IN (SELECT * FROM dbo.SplitString(@ManagerId, ','))
		AND (@ChannelIds IS NULL OR UC.ChannelId IN (SELECT * FROM dbo.SplitString(@ChannelIds, ',')))
		AND (@ReporteeRoleIds IS NULL OR U.urole IN (SELECT * FROM dbo.SplitString(@ReporteeRoleIds, ',')))
		AND (@IsActive Is NULL OR U.IsActive = @IsActive)
END


GO

