USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Upsert_UserChannels]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Upsert_UserChannels]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author, Somnath M. R.>
-- Create date: <Create Date, 09th Dec 2013>
-- Description:	<Description, Insert into tblUserChannels table>
-- EXEC Upsert_UserChannels '1', 16523, 1
-- =============================================
CREATE PROCEDURE [dbo].[Upsert_UserChannels]
	@ChannelIds NVARCHAR(MAX), 
	@UserId INT,
	@CreatedBy [NVARCHAR](50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	INSERT INTO [dbo].[tblUserChannels] ([UserId], [ChannelId], [CreatedBy], [CreatedOn])
		SELECT @UserId, Id.Items, @CreatedBy, GetDate() FROM [dbo].SplitString(@ChannelIds, ',') Id
		WHERE 
			Id.Items NOT IN (SELECT UC.ChannelId FROM [dbo].[tblUserChannels] UC WHERE UC.UserId = @UserId)

	DELETE FROM [dbo].[tblUserChannels] 
	WHERE
		UserId = @UserId 
		AND ChannelId NOT IN (SELECT Id.Items FROM [dbo].SplitString(@ChannelIds, ',') Id)
END

GO

