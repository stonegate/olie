USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetSecurityQueandAnswer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetSecurityQueandAnswer]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblGetSecurityQueandAnswer]
(
@sUserid varchar(Max)
)
as
begin
select * From tblUserDetails ud  inner join tblSecurityQuestions sc 
on ud.SecurityQuestion2=sc.QuestionId 
where UsersID =@sUserid

select userloginid from tblusers where userid=@sUserid
end



GO

