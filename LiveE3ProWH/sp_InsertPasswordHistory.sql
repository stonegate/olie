USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertPasswordHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertPasswordHistory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Stonegate
-- Create date: 10/05/2012
-- Description:	Get Activation Status
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertPasswordHistory]
@UserId int,
@Password nvarchar(80)
as
begin

INSERT INTO tblUserPasswordHistory(UserID,Password) Values(@UserId,@Password)

end

GO

