USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_C3ClearCapitalValidating_Loan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_C3ClearCapitalValidating_Loan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create Procedure  [dbo].[USP_C3ClearCapitalValidating_Loan]
(
@PartnerCompanyID varchar(100) ='0',
@LoanNumber varchar(100) = '0',
@Role bigint = 0
)
AS
BEGIN
 
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
 CREATE TABLE #tempLoan
 (
 DB VARCHAR(100),
 companyEmail VARCHAR(100),
 company VARCHAR(100),
 loan_no VARCHAR(100),
 CurrentStatus VARCHAR(100),
 InstitutionType VARCHAR(100),
 LoanStatus VARCHAR(100),
 C3Status VARCHAR(100),
 BorrowerLastName VARCHAR(100),
 YNValue VARCHAR(100),
 DecisionStatus VARCHAR(100),
 UnderwriterType VARCHAR(100),
 loan_amt VARCHAR(100),
 SubmittedDate VARCHAR(100)
 )

INSERT INTO #tempLoan Select distinct 'E3' DB,
CAST(companyEmail as varchar(100)) companyEmail,
company,
M.LoanNumber loan_no ,
M.CurrentStatus,
InstitutionType,
left(M.CurrentStatus,2)  LoanStatus,
isnull(IsEmergingBanker,'N') C3Status,
borr.lastname BorrowerLastName,
1 YNValue,
M.DecisionStatus,
'NA' UnderwriterType,
loanData.AdjustedNoteAmt,
appStat.StatusDateTime AS SubmittedDate
 
from (Select * from mwlinstitution   where CASE WHEN @Role in(3,20,21) THEN ltrim(rtrim(companyEmail)) ELSE '1' END = CASE WHEN @Role in(3,20,21) THEN @PartnerCompanyID ELSE '1' END 
AND  InstitutionType in('CORRESPOND','BROKER')) I 
INNER JOIN (Select * from mwlloanapp where ltrim(rtrim(LoanNumber)) Like @LoanNumber +'%')  M ON M.Id = I.objowner_id
INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id,lastname from mwlBorrower group by loanapp_id,lastname) as borr on borr.loanapp_id = M.id
INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id from mwlBorrower group by loanapp_id) as borrS on borr.loanapp_id = borrs.loanapp_id and  borrs.SequenceNum = borr.SequenceNum
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = M.id
Left join mwlAppStatus appStat on M.ID=appStat.LoanApp_id and appStat.StatusDesc='01 - Registered' 
Left join mwlApprovalStatus approvalStat on M.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved'

LEFT JOIN (Select * from tblEmergingBankers where Flag = 'C3CCI'   and IsEmergingBanker = 'Y' and CASE WHEN @Role in(3,20,21) THEN PartnerCompanyId ELSE '1' END  = CASE WHEN @Role in(3,20,21) THEN @PartnerCompanyID ELSE '1' END ) E
ON I.companyEmail = E.PartnerCompanyID

Select * from #tempLoan where 
LoanStatus < CASE WHEN InstitutionType = 'Broker' THEN  33 ELSE 62 END 

drop table #tempLoan

END



GO

