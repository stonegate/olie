USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_delete_tblManageTemplate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_delete_tblManageTemplate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_delete_tblManageTemplate]      
(      
@ID int )      
AS      
BEGIN

DELETE FROM tblManageTemplate
WHERE ID = @ID      

END

GO

