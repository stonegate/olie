USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_UPDATEtblusers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_UPDATEtblusers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

Create PROCEDURE [dbo].[SP_UPDATEtblusers]
	(
@userid int
)
AS
BEGIN
UPDATE tblusers SET IsActive=1 where userid=@userid
END	


GO

