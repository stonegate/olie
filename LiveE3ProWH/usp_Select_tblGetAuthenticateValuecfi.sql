USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tblGetAuthenticateValuecfi]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tblGetAuthenticateValuecfi]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Select_tblGetAuthenticateValuecfi] 
	-- Add the parameters for the stored procedure here
	(
@psCellNumber  varchar(Max)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT userid,urole,isUserInNewProcess ,UserLoginid FROM tblUsers 
WHERE  (uemailid = @psCellNumber or UserLoginid = @psCellNumber )

END


GO

