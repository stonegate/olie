USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdatetblLoanDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdatetblLoanDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create procEDURE [dbo].[sp_UpdatetblLoanDoc]  
 -- Add the parameters for the stored procedure here  
(
@loan_no Char(15),
@uloanregid int
)
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
update tblLoanDoc set loan_no =@loan_no
				where uloanregid = @uloanregid
END



GO

