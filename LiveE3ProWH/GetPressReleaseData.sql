USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPressReleaseData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPressReleaseData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure GetPressReleaseData
AS
BEGIN
select PressReleaseContent,PressReleaseDate,PressReleasePath from [dbo].tblPressRelease where Isactive = 1
END

GO

