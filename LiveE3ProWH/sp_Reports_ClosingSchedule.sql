USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Reports_ClosingSchedule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Reports_ClosingSchedule]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Example for execute stored procedure       
--EXECUTE [sp_Reports_ClosingSchedule] 4552,25,'00DD7C8357D8435B846BA7365ADB79DC,835CA8553B674C6D88C7EE0EF7702D01,12EC5ACED7394C64B711BB11DC7DFF2A,F9AAAB7F17BD41EC89C9E42D1D66E661,FD9843C7C05E441CACE97CD09E1A82DC,B2828AD1551643279638682B8282C674,4F32984D1FB24153B12DE5B0FADC9B8E,045C46EA53AE48DCAAC4D375FECF20C5,771590652B5B4F679CB20B2A1FC8F543,9E383127EFAC445E9C4C33241E91B145,9E383127EFAC445E9C4C33241E91B145,9E383127EFAC445E9C4C33241E91B145,F63ECB2C53F84374883B8C2BB6A094FD,9E383127EFAC445E9C4C33241E91B145,9E383127EFAC445E9C4C33241E91B145,9E383127EFAC445E9C4C33241E91B145,9E383127EFAC445E9C4C33241E91B145,9E383127EFAC445E9C4C33241E91B145,9E383127EFAC445E9C4C33241E91B145','33 - Cleared to Close,37 - Pre-Closing,41 - In Closing,43 - Closing on Hold,45 - Docs Out,50 - Closing Package Received,50 - Closing Package Recieved,54 - Purchase Pending,58 - Cleared for Funding,60 - Closed,90 - Funding Reversed,52 - Closing Package Reviewed,53 - Credit Package Reviewed,56 - Purch Conditions Received,51 - Purchase Sub On Hold,57 - Sent to Funding'
-- =============================================
-- Author:<Author Name>
-- Create date: <Create Date>
-- Description: Retrieve Closing Schedule Loan Reports 
-- Name:sp_Reports_ClosingSchedule.sql

-- =============================================
CREATE PROCEDURE [dbo].[sp_Reports_ClosingSchedule] 
	-- Add the parameters for the stored procedure here

@UserID int,
@UserRole int,
@OrignatorID varchar(4000),
@CurrentStatus 	varchar(4000)
	
AS
BEGIN

SET NOCOUNT ON;-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
SET XACT_ABORT ON;-- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,         
                   -- the entire transaction is terminated and rolled back.

if @UserRole = 0
begin
select 'E3' as DB,loanapp.ChannelType,loanapp.Originator_id,loanapp.OriginatorName,loanapp.DecisionStatus, loanapp.ID as record_id,LoanNumber as loan_no,borr.lastname as BorrowerLastName,loanapp.DecisionStatus,loanapp.CurrentStatus as LoanStatus,DateAppSigned as SubmittedDate,loanData.LoanProgramName as LoanProgram, case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe,borr.CompositeCreditScore as CreditScoreUsed,loanData.AdjustedNoteAmt as LoanAmount, convert(varchar(35),EstCloseDate,101) AS ScheduleDate,LockStatus,loanapp.LockExpirationDate,CloserName as Closer,Institute.Company as BrokerName, Institute.CustomDataOne as TitleCompany, AppStatus.StatusDateTime as DocsSentDate,FundedStatus.StatusDateTime as FundedDate,'' as LU_FINAL_HUD_STATUS,loanapp.Channeltype as BusType,Cust.stringvalue as UnderwriterType,LockRecord.DeliveryOption, CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy='Prior to Purchase' and Category !='LENDER' and Category !='NOTICE' and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0  WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy='Prior to Purchase' and Category !='LENDER' and Category !='NOTICE')  and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived' ) and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE (SELECT count(a.ID) as TotalCleared  FROM dbo.mwlCondition a where (DueBy='Prior to Purchase' and Category !='LENDER' and Category !='NOTICE') and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived' ) and a.objOwner_ID IN  (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy='Prior to Purchase' and Category !='LENDER' and Category !='NOTICE') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER from mwlLoanApp  loanapp Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id   
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
Left join dbo.mwlInstitution as Institute on Institute.ObjOwner_id = loanapp.id and  InstitutionType = 'BROKER' 
and objownerName='BROKER' Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id and AppStatus.StatusDesc like '%Docs Out%' Left join mwlappstatus as FundedStatus on FundedStatus.LoanApp_id=loanapp.id  and FundedStatus.StatusDesc 
like '%Funded'  
left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID ='C4D2D8038F614DF8A1B8A6622C99FC3E' left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType='LOCK' and LockRecord.Status<>'CANCELED'
 where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''
  and  CurrentStatus  IN (select * from dbo.SplitString(@CurrentStatus,',')) 

order by CurrentStatus
end

else
begin

select 'E3' as DB,loanapp.Originator_id,loanapp.ChannelType,loanapp.OriginatorName,loanapp.DecisionStatus, loanapp.ID as record_id,LoanNumber as loan_no,borr.lastname as BorrowerLastName,loanapp.DecisionStatus,loanapp.CurrentStatus as LoanStatus,DateAppSigned as SubmittedDate,loanData.LoanProgramName as LoanProgram, case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe,borr.CompositeCreditScore as CreditScoreUsed,
loanData.AdjustedNoteAmt as LoanAmount, convert(varchar(35),EstCloseDate,101) AS ScheduleDate,LockStatus,
loanapp.LockExpirationDate,CloserName as Closer,LockRecord.DeliveryOption,
CASE ISNULL(RTRIM(Institute.Company),'') WHEN '' THEN RTRIM(Corres.Company) ELSE RTRIM(Institute.Company) END as BrokerName,
Institute.CustomDataOne as TitleCompany, AppStatus.StatusDateTime as DocsSentDate,FundedStatus.StatusDateTime as FundedDate,'' as LU_FINAL_HUD_STATUS,loanapp.Channeltype as BusType,Cust.stringvalue as UnderwriterType,
 CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy='Prior to Purchase' and Category !='LENDER' and Category !='NOTICE' and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0  WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy='Prior to Purchase' and Category !='LENDER' and Category !='NOTICE')  and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived' ) and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE (SELECT count(a.ID) as TotalCleared  FROM dbo.mwlCondition a where (DueBy='Prior to Purchase' and Category !='LENDER' and Category !='NOTICE') and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived' ) and a.objOwner_ID IN  (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy='Prior to Purchase' and Category !='LENDER' and Category !='NOTICE') and 
 a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER from mwlLoanApp  loanapp  
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id   
Left join dbo.mwlInstitution as Institute on Institute.ObjOwner_id = loanapp.id and  InstitutionType = 'BROKER' 
and objownerName='BROKER' 
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = 'CORRESPOND' and 
Corres.objownerName='Correspondent'  
Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  
and AppStatus.StatusDesc like '%Docs Out%'  Left join mwlappstatus as FundedStatus on 
FundedStatus.LoanApp_id=loanapp.id  and FundedStatus.StatusDesc like '%Funded'  
left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID ='C4D2D8038F614DF8A1B8A6622C99FC3E' left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType='LOCK' and 
LockRecord.Status<>'CANCELED'
where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''
and  CurrentStatus  IN (select * from dbo.SplitString(@CurrentStatus,',')) 
and (Originator_id in (select * from dbo.SplitString(@OrignatorID,','))) 
order by CurrentStatus

end
END



GO

