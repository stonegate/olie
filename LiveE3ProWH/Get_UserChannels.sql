USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_UserChannels]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Get_UserChannels]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author, Somnath M. R.>
-- Create date: <Create Date, 09th Dec 2013>
-- Description:	<Description, Select from tblUserChannels table>
-- EXEC Get_UserChannels 16527
-- =============================================
CREATE PROCEDURE [dbo].[Get_UserChannels]
	@UserId Int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
		UC.UserChannelId, 
		UC.UserId, 
		C.ChannelId, 
		C.Name, 
		C.BusinessSource
	FROM [dbo].[tblUserChannels] UC
	INNER JOIN [dbo].[tblChannels] C ON C.ChannelId = UC.ChannelId
	INNER JOIN [dbo].[tblUsers] U ON U.UserId = UC.UserId
	WHERE UC.UserId = @UserId
END

GO

