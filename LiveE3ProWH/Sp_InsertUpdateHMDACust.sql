USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_InsertUpdateHMDACust]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_InsertUpdateHMDACust]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Author,,Name>    
-- Create date: <28/12/2012>    
-- Description: <Insert/Update HMDA customfield into mwlcustomfield  table>    
-- =============================================    
CREATE PROCEDURE [dbo].[Sp_InsertUpdateHMDACust]     
 @Loannumber nvarchar(500),    
 @AUCustFieldDef_ID nvarchar(500)='',    
 @ADCustFieldDef_ID nvarchar(500)=''    
    
AS    
BEGIN    
DECLARE @dt AS DATETIME
SET @dt = GETDATE()
 Declare  @LoanAppId varchar(500)    
 set @LoanAppId = (select id from mwlloanapp where loannumber=@Loannumber)    
 print @LoanAppId    
 -- HMDA Aging Date     
 if not exists(select * From mwlcustomfield where CustFieldDef_ID in (@ADCustFieldDef_ID) and  LoanApp_ID =@LoanAppId)        
 BEGIN        
 INSERT INTO mwlcustomfield                       
 (ID,LoanApp_ID,CustFieldDef_ID,Stringvalue,datevalue)                      
  VALUES                      
 (replace(newid(),'-',''),@LoanAppId,@ADCustFieldDef_ID,'',@dt)              
    
 END        
 ELSE        
 BEGIN        
  UPDATE mwlcustomfield SET DateValue=@dt WHERE CustFieldDef_ID in (@ADCustFieldDef_ID) and  LoanApp_ID =@LoanAppId    
 END     
    
 --HMDA Aging user    
 if not exists(select * From mwlcustomfield where CustFieldDef_ID in (@AUCustFieldDef_ID) and  LoanApp_ID =@LoanAppId)        
 BEGIN        
 INSERT INTO mwlcustomfield                       
 (ID,LoanApp_ID,CustFieldDef_ID,Stringvalue)                      
  VALUES                      
 (replace(newid(),'-',''),@LoanAppId,@AUCustFieldDef_ID,'OLIE')              
    
 END        
 ELSE        
 BEGIN        
 UPDATE mwlcustomfield SET Stringvalue='OLIE' WHERE CustFieldDef_ID in (@AUCustFieldDef_ID) and  LoanApp_ID =@LoanAppId        
 END        
    
END

GO

