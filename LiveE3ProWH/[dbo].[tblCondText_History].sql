USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCondText_History]') AND type in (N'U'))
DROP TABLE [dbo].[tblCondText_History]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCondText_History](	  [ctID] INT NOT NULL IDENTITY(1,1)	, [loan_no] CHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL DEFAULT(' ')	, [cond_text] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL DEFAULT(' ')	, [FileName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [dttime] DATETIME NULL	, [condrecid] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [isCommited] BIT NULL DEFAULT((0))	, [ProlendImageDesc] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Userid] INT NULL	, [IsActive] BIT NULL DEFAULT('True')	, [SubmittedDate] DATETIME NULL	, [PriorToPurchaseDate] DATETIME NULL	, CONSTRAINT [PK_tblCondText_History] PRIMARY KEY ([ctID] ASC))USE [HomeLendingExperts]
GO

