USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUnderWriterLoanReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUnderWriterLoanReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================
-- Author:		<Stonegate>
-- Create date: <10/5/2012>
-- Description:	<This will Get Underwriter Loan Report>
-- =====================================================
CREATE PROCEDURE [dbo].[sp_GetUnderWriterLoanReport] 
(
	@strID varchar(2000)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

Declare @SQL nvarchar(max)
Set @SQL = ''
Set @SQL = '
SELECT loandat.record_id, loan_no, RTRIM(cobo.borr_last)  AS BorrowerLastName,lt_loan_stats AS LoanStatus,
case LU_FINAL_HUD_STATUS when '''' then ''Pending'' when NULL then ''Pending'' else ''Approved'' end as LU_FINAL_HUD_STATUS,
app_date AS SubmittedDate,appr_date AS DateApproved, lookups.descriptn AS TransType,lookupprogram.descriptn as LoanProgram,loan_amt, 
CASE WHEN lookupsLock.descriptn =''CANCELED'' THEN ''LOAN FALLS OFF PIPELINE'' WHEN lookupsLock.descriptn =''INACTIVE'' THEN ''LOAN FALLS OFF PIPELINE'' ELSE lookupsLock.descriptn END AS LockStatus,
locks.lock_exp_date AS LockExpirationDate, RTRIM(users.first_name) + '' '' + users.last_name AS UnderWriter,brokers.brok_name as BrokerName,
CASE WHEN (SELECT count(a.record_id) as TotalConditions FROM dbo.condits a,lookups b  where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = ''aaw'' and descriptn=''PROCESSOR'') and a.parent_id IN (SELECT record_id FROM dbo.loandat as loan1  WHERE loan1.loan_no=loandat.loan_no)) = 0 then 0
WHEN(SELECT count(a.record_id) as TotalCleared FROM dbo.condits a,lookups b where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = ''aaw'' and  descriptn=''PROCESSOR'') and ISNULL(a.cond_recvd_date,a.COND_CLEARED_DATE) IS NOT NULL and a.parent_id IN ( SELECT record_id 
FROM dbo.loandat as loan2  WHERE loan2.loan_no=loandat.loan_no)) = 0 THEN 0
ELSE(SELECT count(a.record_id) as TotalCleared FROM dbo.condits a,lookups b where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = ''aaw'' and descriptn=''PROCESSOR'') and ISNULL(a.cond_recvd_date,a.COND_CLEARED_DATE) IS NOT NULL and a.parent_id IN ( SELECT record_id 
FROM dbo.loandat as loan2  WHERE loan2.loan_no=loandat.loan_no)) * 100 / (SELECT count(a.record_id) as TotalConditions FROM dbo.condits a,lookups b  where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = ''aaw'' and descriptn=''PROCESSOR'') and a.parent_id IN (SELECT record_id FROM dbo.loandat as loan1  WHERE loan1.loan_no=loandat.loan_no))END AS PER,
convert(varchar(35),loandat2.DOC_EST_DATE,101) AS ScheduleDate
FROM loandat INNER JOIN
loandat2 ON dbo.loandat.record_id = dbo.loandat2.record_id INNER JOIN
cobo ON loandat.record_id = cobo.parent_id LEFT JOIN
locks ON loandat.record_id = locks.parent_id LEFT JOIN 
brokers ON loandat.lt_broker = brokers.record_id LEFT JOIN 
users ON lt_usr_underwriter = users.record_id LEFT JOIN
proginfo ON loandat.lt_program = proginfo.record_id INNER JOIN
lookups ON loandat.lu_purpose = lookups.id_Value and lookups.lookup_id = ''AAE'' INNER JOIN
lookups as lookups2 ON loandat.lu_loan_type = lookups2.id_Value and lookups2.lookup_id = ''ADC'' INNER JOIN
lookups AS lookups3 ON loandat.lu_loan_type = lookups3.id_value AND lookups3.lookup_id = ''ACY'' INNER JOIN
lookups AS lookupprogram ON loandat.lu_loan_type = lookupprogram.id_value AND lookupprogram.lookup_id = ''AAC'' INNER JOIN
lookups AS lookupsLock ON locks.LU_LOCK_STAT = lookupsLock.id_value AND lookupsLock.lookup_id = ''AAT''
where lt_loan_stats  = ''Approved''
and ''Cleared'' not in
(SELECT CASE WHEN (a.COND_ADDED_DATE IS NOT NULL AND (a.COND_RECVD_DATE IS NULL AND a.COND_RECVD_DATE IS NULL)) THEN ''Open''
WHEN ((a.COND_ADDED_DATE IS NOT NULL AND a.COND_RECVD_DATE IS NOT NULL )) THEN ''Received''
WHEN (a.COND_CLEARED_DATE IS NOT NULL) THEN ''Cleared''  ELSE ''OPEN'' END 
FROM dbo.condits a,lookups b  where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = ''aaw'')
and a.parent_id IN (SELECT loan1.record_id FROM dbo.loandat as loan1  WHERE loan1.loan_no=loandat.loan_no)) '

if (len(@strID) = 0)
Begin
	Set @SQL = @SQL + ' and loandat.lt_usr_underwriter in (select * from dbo.SplitString('''+@strID+''','','')) '
End
else
Begin
	Set @SQL = @SQL + ' and loandat.lt_usr_underwriter in (select * from dbo.SplitString('''+@strID+''','','')) '
End
Set @SQL = @SQL + ' AND cobo.primary_rec=1 '
Set @SQL = @SQL + ' ORDER BY PER '

Print @SQL 
exec sp_executesql @SQL
END

GO

