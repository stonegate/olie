USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateLoanProgramE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateLoanProgramE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Vipul Thacker>    
-- Create date: <21-03-2013>    
-- Description: <For Update the Loan Program in E3>    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_UpdateLoanProgramE3]  
 @LoanAppId varchar(1000),    
   
  @strLoanProgram varchar(1000)    
AS    
BEGIN    
DECLARE  @strCustFieldDefLoanID VARCHAR(50);  
--//get customfield value of loan program  
SELECT @strCustFieldDefLoanID =ID FROM dbo.mwsCustFieldDef WITH (nolock)  WHERE mwfieldnum='9623'  
PRINT @strCustFieldDefLoanID  
  
 if not exists(select * From mwlcustomfield WITH (nolock) where     
  CustFieldDef_ID =@strCustFieldDefLoanID    
  and  LoanApp_id=@LoanAppId    
  )    
      
     insert into mwlcustomfield    
     (    
   ID,    
   LoanApp_id,    
   CustFieldDef_ID,    
   StringValue    
     )    
     values    
     (    
     (replace(newid(),'-','')),    
   @LoanAppId,    
            @strCustFieldDefLoanID,    
      @strLoanProgram    
     )     
    Else    
     update mwlcustomfield     WITH (UPDLOCK)
     set StringValue=@strLoanProgram      
     where CustFieldDef_ID=@strCustFieldDefLoanID and LoanApp_id =@LoanAppId    
               
        
                        
    
     
END 


GO

