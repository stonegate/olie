USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFNMLog]') AND type in (N'U'))
DROP TABLE [dbo].[tblFNMLog]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFNMLog](	  [UserId] INT NOT NULL	, [LineId] INT NOT NULL	, [LoanNumber] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [LoanStatus] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FNMName] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EditType] NVARCHAR(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [OriginalLine] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [ModifiedLine] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [ModifiedBy] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [ModifiedDateTime] DATETIME NOT NULL)USE [HomeLendingExperts]
GO

