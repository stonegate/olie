USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetTempCreatedDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetTempCreatedDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

CREATE PROCEDURE [dbo].[SP_GetTempCreatedDate]
	(
@refuserid int
)
AS
BEGIN
SELECT top 1 isnull(temp.createdDate,getdate()) as createdDate  FROM tblTempUser temp 
left join tblusers users on  temp.refuserid= users.userid 
WHERE (refuserid =@refuserid)
order by tempid desc
END

GO

