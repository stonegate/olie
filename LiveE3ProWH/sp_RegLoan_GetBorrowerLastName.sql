USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RegLoan_GetBorrowerLastName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RegLoan_GetBorrowerLastName]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_RegLoan_GetBorrowerLastName]
(
@LoanNo varchar(50)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	SELECT rtrim(LastName) as borr_last FROM mwlBorrower as borr INNER JOIN mwlloanapp  loanapp ON loanapp.ID = borr.loanapp_id  where  borr.sequencenum=1  and LoanNumber = @LoanNo
END

GO

