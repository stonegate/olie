USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCorrSpecialistView_Test]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCorrSpecialistView_Test]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:  Stonegate
-- Create date: 12th July 2012        
-- Description: This will get Correspondant Specialist Report  
-- =============================================        
CREATE PROCEDURE [dbo].[sp_GetCorrSpecialistView_Test]        
(        
@sortField varchar(500),        
@sortOrder varchar(100),        
@whereClause varchar(1000),        
@PageNo int,         
@PageSize int,  
  
@CustomField nvarchar(500),  
@CustomFieldN1 nvarchar(500),  
@CustomFieldN2 nvarchar(500),  
@CustomFieldN3 nvarchar(500),  
@CustSendToFundingDate nvarchar(500),  
@SubmittedDateID nvarchar(500),  
@SubmittedDateTimeID nvarchar(500),  
  
@CurrentStatus nvarchar(500),  
@URL_From  nvarchar(500), --queryString i.e.agedpursub,agedcloreceived,agedcloreviewed etc.
@ShowAll int
)         
AS        
BEGIN      

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
  
IF @sortField  = ''                                    
  set @sortField = 'loanapp.CurrPipeStatusDate'        
        
SET @sortField = @sortField + ' ' + @sortOrder           
        
Declare @sql nvarchar(max)   
Declare @sql1 nvarchar(max)  

Declare @sql2 nvarchar(max)  

Declare @sqln1 nvarchar(max)  
Declare @sqln2 nvarchar(max)  
Declare @sqln3 nvarchar(max)  
Declare @sqln4 nvarchar(max)  

set @sqln1=''
set @sqln2=''
set @sqln3=''
set @sqln4=''

If @ShowAll = 0
Begin
	set @sql = ''                                    
		 set @sql = 'Select count(*),ceiling(sum(loan_amt)) as TotalAmt, ceiling(sum(loan_amt)/count(*)) as avg  from ('
		 set @sql =  @sql + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ')AS Row,
	''E3'' as DB,CustDate.DateValue as LastSubmittedDate,CustTime.StringValue as LastSubmittedTime ,Offices.Office as office,
	Corres.Company as coname,BOffices.Office as Boffice,loanapp.CurrDecStatusDate as Decisionstatusdate,
	loanapp.CurrPipeStatusDate as CurrentStatusDate ,loanapp.ChannelType as ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID as record_id,
	'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,
	loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,
	CASE TransType when NULL THEN '''' WHEN ''P'' THEN ''Purchase money first mortgage''
	WHEN ''R'' THEN ''Refinance'' WHEN ''2'' THEN ''Purchase money second mortgage'' WHEN ''S'' THEN ''Second mortgage, any other purpose'' WHEN ''A'' THEN ''Assumption''
	WHEN ''HOP'' THEN ''HELOC - other purpose'' WHEN ''HP'' THEN ''HELOC - purchase'' ELSE '''' END AS TransType,
	mwllookups.displaystring,'
	set @sqln1 ='N1  case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,
	loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockDatetime as LockDatetime,
	LockRecord.LockExpirationDate, UnderwriterName as UnderWriter,
	Broker.Company as BrokerName,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,Loandata.LoanProgDesc as ProgramDesc,
	CASE WHEN (SELECT count(a.ID) as TotalConditions FROM mwlCondition a WITH (nolock) where DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE''
	and a.objOwner_ID IN (SELECT ID FROM mwlLoanApp as loan1 WITH (nolock)  WHERE loan1.LoanNumber=LoanApp.LoanNumber))=0 then 0
	WHEN (SELECT count(a.ID) as TotalCleared FROM mwlCondition a WITH (nolock) where (DueBy=''Prior to Purchase'' and Category !=''LENDER''
	and Category !=''NOTICE'')and(CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'')
	and a.objOwner_ID IN (SELECT id FROM mwlLoanApp  as loan2 WITH (nolock)  WHERE loan2.LoanNumber=LoanApp.Loannumber))=0 THEN 0 ELSE
	(SELECT count(a.ID) as TotalCleared FROM mwlCondition a WITH (nolock) where (DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED''
	OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'')and a.objOwner_ID IN (SELECT ID FROM mwlLoanApp as loan2 WITH (nolock) '
	
 set  @sqln2='N2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100/(SELECT count(a.ID) as TotalConditions FROM mwlCondition a  WITH (nolock)  where(DueBy=''Prior to Purchase''
	and Category !=''LENDER'' and Category !=''NOTICE'') and a.objOwner_ID IN (SELECT ID FROM mwlLoanApp as loan1 WITH (nolock) 
	WHERE loan1.LoanNumber=LoanApp.LoanNumber))END
	AS PER,BOffices.Office as OFFICE_NAME1,isnull(Cust.StringValue,'''') as CorrespondentType, CASE when (CustN1.StringValue IS null or CustN1.StringValue=''Other'')
	THEN(case when(CustN2.StringValue IS null or CustN2.StringValue=''Other'')then (case when (CustN3.StringValue IS null or CustN3.StringValue=''Other'') then ''''
	else CustN3.StringValue end) else CustN2.StringValue end )else CustN1.StringValue End as CorrespodentSpecialist,'''' as CorrespodentFSpecialist from mwlLoanApp loanapp WITH (nolock) 
	Inner join mwlBorrower as borr on borr.loanapp_id=loanapp.id and borr.Sequencenum=1 Inner join mwlloandata as loanData on loanData.ObjOwner_id=loanapp.id left join mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id=loanapp.id
	Left join mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType=''BROKER''
	and Broker.objownerName=''Broker'' left join mwlLockRecord as LockRecord on LockRecord.LoanApp_ID=loanapp.id and LockRecord.LockType=''LOCK''
	and LockRecord.Status<>''CANCELED''Left join mwlInstitution as Offices on Offices.ObjOwner_id=loanapp.id'
	set @sqln3 = 'N3  and Offices.InstitutionType = ''Broker'' and Offices.objownerName=''Broker'' Left join mwlInstitution as COffices on COffices.ObjOwner_id = loanapp.id
	and COffices.InstitutionType=''CORRESPOND''  left join mwllookups on mwllookups.Bocode = loanapp.transtype and mwllookups.objectname = ''mwlloanapp'' and mwllookups.fieldname = ''TransType''
	and COffices.objownerName=''Contacts'' Left join mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and
	BOffices.InstitutionType=''Branch'' and BOffices.objownerName=''BranchInstitution'' Left join mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id
	and Corres.InstitutionType=''CORRESPOND'' and Corres.objownerName=''Correspondent'' left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id and
	Cust.CustFieldDef_ID =''' +@CustomField+ ''' left join mwlcustomfield as CustN1 on CustN1.loanapp_id = loanapp.id '
	set @sql1 = ' N4 and CustN1.CustFieldDef_ID =''' +@CustomFieldN1+ ''' left join mwlcustomfield as CustN2 on CustN2.loanapp_id = loanapp.id
	and CustN2.CustFieldDef_ID =''' +@CustomFieldN2+ ''' left join mwlcustomfield as CustN3 on CustN3.loanapp_id = loanapp.id  
	and CustN3.CustFieldDef_ID =''' +@CustomFieldN3+ ''' left join mwlcustomfield as CustDate on CustDate.loanapp_id = loanapp.id
	and CustDate.CustFieldDef_ID =''' +@SubmittedDateID+ ''' left join mwlcustomfield as CustTime on CustTime.loanapp_id = loanapp.id
	and CustTime.CustFieldDef_ID =''' +@SubmittedDateTimeID+ ''' '
	  
	IF @URL_From = 'sendTofundingMgr'  
	Begin  
	 set @sql1 =  @sql1 + ' inner join mwlcustomfield as SendtoFundingDate on SendtoFundingDate.loanapp_id = loanapp.id  and CustTime.CustFieldDef_ID =''' + @CustSendToFundingDate + ''' '  
	 set @sql1 =  @sql1 + ' where CurrentStatus IN (''54 - Purchase Pending'',''58 - Cleared for Funding'',''62 - Funded'',''66 - In Shipping'') '  
	End  
	IF @URL_From = 'sendTofunding'  
	Begin  
	 set @sql1 =  @sql1 + ' inner join mwlcustomfield as SendtoFundingDate on SendtoFundingDate.loanapp_id = loanapp.id  and CustTime.CustFieldDef_ID =''' + @CustSendToFundingDate + ''' '  
	 set @sql1 =  @sql1 + ' where CurrentStatus IN (''54 - Purchase Pending'',''58 - Cleared for Funding'',''62 - Funded'',''66 - In Shipping'') '  
	End  
	IF @URL_From = 'agedpursub'  
	Begin  
	 set @sql1 =  @sql1 + ' where CurrentStatus  IN (''51 - Purchase Sub On Hold'' ) '  
	 set @sql1 =  @sql1 + ' and convert (varchar(10),loanapp.CurrPipeStatusDate + 20,101) >=convert (varchar(10), getdate() ,101) '  
	End  
	IF @URL_From = 'agedcloreceived'  
	Begin  
	 set @sql1 =  @sql1 + ' where CurrentStatus  IN (''50 - Closing Package Received'' ) '  
	 set @sql1 =  @sql1 + ' and convert (varchar(10),loanapp.CurrPipeStatusDate + 20,101) >=convert (varchar(10), getdate() ,101) '  
	End  
	IF @URL_From = 'agedcloreviewed'  
	Begin  
	 set @sql1 =  @sql1 + ' where CurrentStatus  IN (''52 - Closing Package Reviewed'' ) '  
	 set @sql1=  @sql1 + ' and convert (varchar(10),loanapp.CurrPipeStatusDate + 20,101) >=convert (varchar(10), getdate() ,101) '  
	End  
	IF @URL_From = 'agedpurpending'  
	Begin  
	 set @sql1 =  @sql1 + ' where CurrentStatus  IN (''54 - Purchase Pending'' ) '  
	 set @sql1 = @sql1 + ' and convert (varchar(10),loanapp.CurrPipeStatusDate + 20,101) >=convert (varchar(10), getdate() ,101) '  
	End
	  
	IF (@URL_From <> 'sendTofundingMgr' OR @URL_From <> 'sendTofunding' OR @URL_From <> 'agedpursub' OR @URL_From <> 'agedcloreceived'  OR @URL_From <> 'agedcloreviewed' OR @URL_From <> 'agedpurpending')  
	Begin  
		--set @sql1 =  @sql1 + ' where CurrentStatus  IN (''' +@CurrentStatus+ ''') '   
		set @sql1 =  @sql1 + ' Where CurrentStatus  IN (select * from dbo.SplitString('''+@CurrentStatus +''','','')) '  
	END  
	  
	set @sql1 =  @sql1 + ' and ChannelType in(''CORRESPOND'') and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' '  
	        
	IF @whereClause <> ''                                             
		BEGIN                                         
		set @sql1 = @sql1  + ' AND ' + @whereClause                                   
		END                            
	        
	set @sql1 = @sql1 + ') as t2 '         
			set @sql2 =  @sql + @sql1



   
print @sql
print @sqln1
print @sqln2
print @sqln3
print @sql1
--	print @sql2        
	exec sp_executesql @sql2
End        
set @sql = ''                                    
     set @sql = 'Select * from ( '         
set @sql =  @sql + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,''E3'' as DB,CustDate.DateValue as LastSubmittedDate,
CustTime.StringValue as LastSubmittedTime ,Offices.Office as office,Corres.Company as coname,
BOffices.Office as Boffice,loanapp.CurrDecStatusDate as Decisionstatusdate,
loanapp.CurrPipeStatusDate as CurrentStatusDate ,loanapp.ChannelType as ChannelType,
loanapp.DecisionStatus,OriginatorName,loanapp.ID as record_id,
'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,
loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,
CASE TransType when NULL THEN '''' WHEN ''P'' THEN ''Purchase money first mortgage'' WHEN ''R'' THEN ''Refinance'' WHEN ''2''
THEN ''Purchase money second mortgage'' WHEN ''S'' THEN ''Second mortgage, any other purpose'' WHEN ''A'' THEN ''Assumption'' WHEN ''HOP'' THEN ''HELOC - other purpose'' WHEN ''HP'' THEN ''HELOC - purchase'' ELSE '''' END AS TransType,
mwllookups.displaystring,--Commented
case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,
loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockDatetime as LockDatetime,
LockRecord.LockExpirationDate, UnderwriterName as UnderWriter,
Broker.Company as BrokerName,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,Loandata.LoanProgDesc as ProgramDesc , CASE WHEN (SELECT count(a.ID) as TotalConditions FROM mwlCondition a WITH (nolock) where DueBy=''Prior to Purchase''
and Category !=''LENDER'' and Category !=''NOTICE''
and a.objOwner_ID IN (SELECT ID FROM mwlLoanApp as loan1 WITH (nolock)  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0  WHEN (SELECT count(a.ID) as TotalCleared FROM mwlCondition a WITH (nolock) 
where (DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'')  and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'')
and a.objOwner_ID IN (SELECT id FROM mwlLoanApp  as loan2 WITH (nolock) WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE (SELECT count(a.ID) as TotalCleared  FROM mwlCondition a  WITH (nolock) where (DueBy=''Prior to Purchase''
and Category !=''LENDER''
and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') and a.objOwner_ID IN  (SELECT ID FROM mwlLoanApp as loan2 WITH (nolock) 
WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 /(SELECT count(a.ID) as TotalConditions FROM mwlCondition a WITH (nolock) where ( DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'')
and a.objOwner_ID IN ( SELECT ID FROM mwlLoanApp as loan1  WITH (nolock) 
WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER ,BOffices.Office as OFFICE_NAME1,isnull(Cust.StringValue,'''') as  CorrespondentType  , CASE when (CustN1.StringValue IS null or CustN1.StringValue=''Other'')  
THEN (case when (CustN2.StringValue IS null or CustN2.StringValue=''Other'') then ( case when (CustN3.StringValue IS null or CustN3.StringValue=''Other'') then ''''  
else CustN3.StringValue end ) else CustN2.StringValue end ) else CustN1.StringValue End  as  CorrespodentSpecialist,'''' as CorrespodentFSpecialist  from mwlLoanApp  loanapp WITH (nolock)   
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and borr.Sequencenum=1 Inner join mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id  
left join mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  Left join mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER''  
and Broker.objownerName=''Broker''left join mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id  and  LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED''  
Left join mwlInstitution as Offices on Offices.ObjOwner_id = loanapp.id and Offices.InstitutionType = ''Broker'' and Offices.objownerName=''Broker'' Left join mwlInstitution as COffices on COffices.ObjOwner_id = loanapp.id  
and COffices.InstitutionType = ''CORRESPOND'' left join mwllookups on mwllookups.Bocode = loanapp.transtype and mwllookups.objectname = ''mwlloanapp'' and mwllookups.fieldname = ''TransType'''
set @sql =  @sql + 'and COffices.objownerName=''Contacts'' Left join mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.InstitutionType = ''Branch'' and BOffices.objownerName=''BranchInstitution''
Left join mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''    
left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and   
Cust.CustFieldDef_ID =''' +@CustomField+ ''' left join mwlcustomfield as CustN1 on CustN1.loanapp_id = loanapp.id    
and CustN1.CustFieldDef_ID =''' +@CustomFieldN1+ ''' left join mwlcustomfield as CustN2 on CustN2.loanapp_id = loanapp.id    
and CustN2.CustFieldDef_ID =''' +@CustomFieldN2+ ''' left join mwlcustomfield as CustN3 on CustN3.loanapp_id = loanapp.id    
and CustN3.CustFieldDef_ID =''' +@CustomFieldN3+ ''' left join mwlcustomfield as CustDate on CustDate.loanapp_id = loanapp.id    
and CustDate.CustFieldDef_ID =''' +@SubmittedDateID+ ''' left join mwlcustomfield as CustTime on CustTime.loanapp_id = loanapp.id    
and CustTime.CustFieldDef_ID =''' +@SubmittedDateTimeID+ ''' '  
        
IF @URL_From = 'sendTofundingMgr'  
Begin  
 set @sql =  @sql + ' inner join mwlcustomfield as SendtoFundingDate on SendtoFundingDate.loanapp_id = loanapp.id  and CustTime.CustFieldDef_ID =''' + @CustSendToFundingDate + ''' '  
 set @sql =  @sql + ' where CurrentStatus  IN  (''54 - Purchase Pending'',''58 - Cleared for Funding'',''62 - Funded'',''66 - In Shipping'') '  
End  
IF @URL_From = 'sendTofunding'  
Begin  
 set @sql =  @sql + ' inner join mwlcustomfield as SendtoFundingDate on SendtoFundingDate.loanapp_id = loanapp.id  and CustTime.CustFieldDef_ID =''' + @CustSendToFundingDate + ''' '  
 set @sql =  @sql + ' where CurrentStatus  IN  (''54 - Purchase Pending'',''58 - Cleared for Funding'',''62 - Funded'',''66 - In Shipping'') '  
End  
IF @URL_From = 'agedpursub'  
Begin  
 set @sql =  @sql + ' where CurrentStatus  IN (''51 - Purchase Sub On Hold'' ) '  
 set @sql =  @sql + ' and convert (varchar(10),loanapp.CurrPipeStatusDate + 20,101) >=convert (varchar(10), getdate() ,101) '  
End  
IF @URL_From = 'agedcloreceived'  
Begin  
 set @sql =  @sql + ' where CurrentStatus  IN (''50 - Closing Package Received'' ) '  
 set @sql =  @sql + ' and convert (varchar(10),loanapp.CurrPipeStatusDate + 20,101) >=convert (varchar(10), getdate() ,101) '  
End  
IF @URL_From = 'agedcloreviewed'  
Begin  
 set @sql =  @sql + ' where CurrentStatus  IN (''52 - Closing Package Reviewed'' ) '  
 set @sql =  @sql + ' and convert (varchar(10),loanapp.CurrPipeStatusDate + 20,101) >=convert (varchar(10), getdate() ,101) '  
End  
IF @URL_From = 'agedpurpending'  
Begin  
 set @sql =  @sql + ' where CurrentStatus  IN (''54 - Purchase Pending'' ) '  
 set @sql =  @sql + ' and convert (varchar(10),loanapp.CurrPipeStatusDate + 20,101) >=convert (varchar(10), getdate() ,101) '  
End  
  
  
IF (@URL_From <> 'sendTofundingMgr' OR @URL_From <> 'sendTofunding' OR @URL_From <> 'agedpursub' OR @URL_From <> 'agedcloreceived'  OR @URL_From <> 'agedcloreviewed' OR @URL_From <> 'agedpurpending')  
Begin  
		--set @sql1 =  @sql1 + ' where CurrentStatus  IN (''' +@CurrentStatus+ ''') '   
		set @sql1 =  @sql1 + ' Where CurrentStatus  IN (select * from dbo.SplitString('''+@CurrentStatus +''','','')) '  
END  
  
set @sql =  @sql + ' and ChannelType in(''CORRESPOND'') and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' '  
        
IF @whereClause <> ''                                             
    BEGIN                                         
    set @sql = @sql  + ' AND ' + @whereClause                                   
    END                            
  
IF @URL_From = 'sendTofundingMgr' OR  @URL_From = 'sendTofunding'  
Begin  
 set @sql = @sql + ' group by CustDate.DateValue,Offices.Office,Corres.Company,BOffices.Office,loanapp.CurrDecStatusDate ,  
 loanapp.CurrPipeStatusDate,loanapp.ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID,LoanNumber,CloserName,borr.lastname,  
 loanapp.CurrentStatus,mwllookups.DisplayString,TransType,LoanData.FinancingType,loanData.AdjustedNoteAmt,LockRecord.Status,LockRecord.LockDatetime,LockRecord.LockExpirationDate, UnderwriterName,  
 Broker.Company,borr.CompositeCreditScore ,Loandata.LoanProgDesc,CustTime.StringValue,mwlcustomfield.StringValue,Cust.StringValue,CustN1.StringValue,CustN2.StringValue,CustN3.StringValue '  
 set @sql = @sql + ' )as t2'  
End  
Else  
Begin  
 set @sql = @sql + ' group by CustDate.DateValue,Offices.Office,Corres.Company,BOffices.Office,loanapp.CurrDecStatusDate ,  
 loanapp.CurrPipeStatusDate,loanapp.ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID,LoanNumber,CloserName,borr.lastname,  
 loanapp.CurrentStatus,mwllookups.DisplayString,TransType,LoanData.FinancingType,loanData.AdjustedNoteAmt,LockRecord.Status,LockRecord.LockDatetime,LockRecord.LockExpirationDate, UnderwriterName,  
 Broker.Company,borr.CompositeCreditScore ,Loandata.LoanProgDesc,CustTime.StringValue,Cust.StringValue,CustN1.StringValue,CustN2.StringValue,CustN3.StringValue '  
  set @sql = @sql + ' )as t2'        
End
If @ShowAll = 0
Begin       
	set @sql = @sql + ' Where Row between (' + CAST( @PageNo AS NVARCHAR)  + ' - 1) * ' + Cast(@PageSize as nvarchar) + ' + 1 and ' + cast(@PageNo as nvarchar) + ' * ' + cast(@PageSize as nvarchar)        
End   
--print @sql                                  
exec sp_executesql @sql        
        
END 






GO

