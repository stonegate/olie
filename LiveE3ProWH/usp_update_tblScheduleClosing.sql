USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_update_tblScheduleClosing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_update_tblScheduleClosing]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[usp_update_tblScheduleClosing] --'0000316695','2-2-2012 00:00:00:000','','','','','','','','4486','',1        
(        
@LoanNo varchar(50),        
@SchDate datetime,        
@FeeSheet varchar(100)        
,@HomeownerInsurance Varchar(150)        
,@TitleCommitment Varchar(150)        
,@InsuredClosingProtectionLetter Varchar(150)        
,@RateLockConfirmation Varchar(150)        
,@Invoices Varchar(150)        
,@FinalLocked Varchar(150)        
,@Userid int,           
@Comments varchar(1000),        
@IsFromLoanSuspendedDoc bit,    
@time varchar(Max)
,@AppStatus bit
,@RushStatus bit
,@ReDrawStatus bit          
)         
AS         
BEGIN          
 IF EXISTS(SELECT ScheduleID FROM tblScheduleClosing WHERE LoanNo=@LoanNo)        
 BEGIN           
   IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter<>'' and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing   WITH (UPDLOCK)           
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,TitleCommitment =@TitleCommitment        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE()    
     ,[Time]=@time      
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet = '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing    WITH (UPDLOCK)          
     SET SchDate = @SchDate         
     ,HomeownerInsurance =@HomeownerInsurance        
     ,TitleCommitment =@TitleCommitment        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END          
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance ='' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing   WITH (UPDLOCK)           
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,TitleCommitment =@TitleCommitment        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE() ,[Time]=@time          
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment='' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing     WITH (UPDLOCK)         
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE()  ,[Time]=@time         
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter=''   and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing     WITH (UPDLOCK)         
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,TitleCommitment =@TitleCommitment        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE() ,[Time]=@time          
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation='' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN           
    UPDATE tblScheduleClosing    WITH (UPDLOCK)          
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,TitleCommitment =@TitleCommitment        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE() ,[Time]=@time          
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation<>'' and @Invoices='' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing      WITH (UPDLOCK)        
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,TitleCommitment =@TitleCommitment        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked=''        
   BEGIN            
    UPDATE tblScheduleClosing      WITH (UPDLOCK)        
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,TitleCommitment =@TitleCommitment        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,Comments = @Comments,           
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
        
   else IF @FeeSheet = '' AND @Comments = '' and @HomeownerInsurance <>'' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing    WITH (UPDLOCK)          
     SET SchDate = @SchDate         
     ,HomeownerInsurance =@HomeownerInsurance        
     ,TitleCommitment =@TitleCommitment        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments = '' and @HomeownerInsurance ='' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing   WITH (UPDLOCK)           
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,TitleCommitment =@TitleCommitment        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,SubmitedDate = GETDATE() ,[Time]=@time          
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance ='' and @TitleCommitment='' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing   WITH (UPDLOCK)           
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE()  ,[Time]=@time         
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment='' and @InsuredClosingProtectionLetter=''   and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing    WITH (UPDLOCK)          
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE()  ,[Time]=@time         
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter=''   and @RateLockConfirmation='' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing    WITH (UPDLOCK)          
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,TitleCommitment =@TitleCommitment        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE()  ,[Time]=@time         
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation='' and @Invoices='' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing    WITH (UPDLOCK)          
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,TitleCommitment =@TitleCommitment        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE()   ,[Time]=@time        
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation<>'' and @Invoices='' and @FinalLocked=''        
   BEGIN            
    UPDATE tblScheduleClosing    WITH (UPDLOCK)          
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,TitleCommitment =@TitleCommitment        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Comments = @Comments,           
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet = '' AND @Comments = '' and @HomeownerInsurance ='' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing     WITH (UPDLOCK)         
     SET SchDate = @SchDate         
     ,TitleCommitment =@TitleCommitment        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments = '' and @HomeownerInsurance ='' and @TitleCommitment='' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing    WITH (UPDLOCK)          
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance ='' and @TitleCommitment='' and @InsuredClosingProtectionLetter=''   and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing   WITH (UPDLOCK)           
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment='' and @InsuredClosingProtectionLetter=''   and @RateLockConfirmation='' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing    WITH (UPDLOCK)          
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE() ,[Time]=@time          
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter=''   and @RateLockConfirmation='' and @Invoices='' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing    WITH (UPDLOCK)          
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,TitleCommitment =@TitleCommitment        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation='' and @Invoices='' and @FinalLocked=''        
   BEGIN            
    UPDATE tblScheduleClosing     WITH (UPDLOCK)         
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,TitleCommitment =@TitleCommitment        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,Comments = @Comments,           
     SubmitedDate = GETDATE() ,[Time]=@time          
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet = '' AND @Comments = '' and @HomeownerInsurance ='' and @TitleCommitment='' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing       WITH (UPDLOCK)       
     SET SchDate = @SchDate         
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked          
     ,SubmitedDate = GETDATE()  ,[Time]=@time         
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments = '' and @HomeownerInsurance ='' and @TitleCommitment='' and @InsuredClosingProtectionLetter=''   and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing      WITH (UPDLOCK)        
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet             
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,SubmitedDate = GETDATE() ,[Time]=@time         
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance ='' and @TitleCommitment='' and @InsuredClosingProtectionLetter=''   and @RateLockConfirmation='' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing     WITH (UPDLOCK)         
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE() ,[Time]=@time          
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter=''   and @RateLockConfirmation='' and @Invoices='' and @FinalLocked=''        
   BEGIN            
    UPDATE tblScheduleClosing   WITH (UPDLOCK)           
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,TitleCommitment =@TitleCommitment        
     ,Comments = @Comments,           
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet = '' AND @Comments = '' and @HomeownerInsurance ='' and @TitleCommitment='' and @InsuredClosingProtectionLetter=''   and @RateLockConfirmation<>'' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing    WITH (UPDLOCK)          
     SET SchDate = @SchDate,         
     RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,SubmitedDate = GETDATE() ,[Time]=@time          
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments = '' and @HomeownerInsurance  = '' and @TitleCommitment = '' and @InsuredClosingProtectionLetter = ''   and @RateLockConfirmation = '' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing    WITH (UPDLOCK)          
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,SubmitedDate = GETDATE()  ,[Time]=@time         
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance  = '' and @TitleCommitment = '' and @InsuredClosingProtectionLetter = ''   and @RateLockConfirmation = '' and @Invoices = '' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing     WITH (UPDLOCK)         
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,FinalLocked =@FinalLocked           
     ,Comments = @Comments,           
     SubmitedDate = GETDATE()  ,[Time]=@time         
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance <>'' and @TitleCommitment = '' and @InsuredClosingProtectionLetter = ''   and @RateLockConfirmation = '' and @Invoices = '' and @FinalLocked = ''        
   BEGIN            
    UPDATE tblScheduleClosing     WITH (UPDLOCK)         
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,Comments = @Comments,           
     SubmitedDate = GETDATE() ,[Time]=@time          
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END           else IF @FeeSheet  =  '' AND @Comments  =  '' and @HomeownerInsurance  = '' and @TitleCommitment = '' and @InsuredClosingProtectionLetter = ''   and @RateLockConfirmation = '' and @Invoices<>'' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing     WITH (UPDLOCK)         
     SET SchDate = @SchDate,         
     Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,SubmitedDate = GETDATE()   ,[Time]=@time        
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments  =  '' and @HomeownerInsurance  = '' and @TitleCommitment = '' and @InsuredClosingProtectionLetter = ''   and @RateLockConfirmation = '' and @Invoices = '' and @FinalLocked<>''        
   BEGIN            
    UPDATE tblScheduleClosing     WITH (UPDLOCK)         
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,FinalLocked =@FinalLocked           
     ,SubmitedDate = GETDATE() ,[Time]=@time          
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' AND @Comments <> '' and @HomeownerInsurance  = '' and @TitleCommitment = '' and @InsuredClosingProtectionLetter = ''   and @RateLockConfirmation = '' and @Invoices = '' and @FinalLocked = ''        
   BEGIN            
    UPDATE tblScheduleClosing      WITH (UPDLOCK)        
     SET SchDate = @SchDate,         
     FeeSheet = @FeeSheet        
     ,Comments = @Comments,           
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END        
   else IF @FeeSheet <> '' and @HomeownerInsurance <>'' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter<>''   and @RateLockConfirmation<>'' and @Invoices<>'' and @Comments = '' and @FinalLocked<>''         
   BEGIN            
    UPDATE tblScheduleClosing     WITH (UPDLOCK)            
     SET SchDate = @SchDate,           
     FeeSheet = @FeeSheet        
     ,HomeownerInsurance =@HomeownerInsurance        
     ,TitleCommitment =@TitleCommitment        
     ,InsuredClosingProtectionLetter =@InsuredClosingProtectionLetter        
     ,RateLockConfirmation =@RateLockConfirmation        
     ,Invoices =@Invoices        
     ,FinalLocked =@FinalLocked           
     ,SubmitedDate = GETDATE(),[Time]=@time          
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo           
   END        
   else IF @FeeSheet ='' and @HomeownerInsurance ='' and @TitleCommitment='' and @InsuredClosingProtectionLetter=''  and @RateLockConfirmation='' and @Invoices='' and @Comments =  '' and @FinalLocked<>''         
   BEGIN          
    UPDATE tblScheduleClosing     WITH (UPDLOCK)           
     SET SchDate = @SchDate,        
     FinalLocked = @FinalLocked,        
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END         
   else IF @FeeSheet ='' and @HomeownerInsurance ='' and @TitleCommitment='' and @InsuredClosingProtectionLetter=''  and @RateLockConfirmation='' and @Invoices<>'' and @Comments = ''  and @FinalLocked=''        
   BEGIN          
    UPDATE tblScheduleClosing    WITH (UPDLOCK)            
     SET SchDate = @SchDate,        
     Invoices = @Invoices,        
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END         
   else IF @FeeSheet ='' and @HomeownerInsurance ='' and @TitleCommitment='' and @InsuredClosingProtectionLetter=''  and @RateLockConfirmation<>'' and @Invoices='' and @Comments = ''  and @FinalLocked=''        
   BEGIN          
    UPDATE tblScheduleClosing     WITH (UPDLOCK)           
     SET SchDate = @SchDate,        
     RateLockConfirmation = @RateLockConfirmation,        
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END         
   else IF @FeeSheet ='' and @HomeownerInsurance ='' and @TitleCommitment='' and @InsuredClosingProtectionLetter<>''  and @RateLockConfirmation='' and @Invoices='' and @Comments = ''  and @FinalLocked=''        
   BEGIN          
    UPDATE tblScheduleClosing    WITH (UPDLOCK)            
     SET SchDate = @SchDate,        
     InsuredClosingProtectionLetter = @InsuredClosingProtectionLetter,        
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END         
   else IF @FeeSheet ='' and @HomeownerInsurance ='' and @TitleCommitment<>'' and @InsuredClosingProtectionLetter=''  and @RateLockConfirmation='' and @Invoices='' and @Comments = '' and @FinalLocked=''         
   BEGIN          
    UPDATE tblScheduleClosing    WITH (UPDLOCK)            
     SET SchDate = @SchDate,        
     TitleCommitment = @TitleCommitment,        
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END         
   else IF @FeeSheet ='' and @HomeownerInsurance <>'' and @TitleCommitment='' and @InsuredClosingProtectionLetter=''  and @RateLockConfirmation='' and @Invoices='' and @Comments = ''  and @FinalLocked=''        
   BEGIN          
    UPDATE tblScheduleClosing    WITH (UPDLOCK)            
     SET SchDate = @SchDate,        
     HomeownerInsurance = @HomeownerInsurance,        
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END         
   else IF @FeeSheet <>'' and @HomeownerInsurance ='' and @TitleCommitment='' and @InsuredClosingProtectionLetter=''  and @RateLockConfirmation='' and @Invoices='' and @Comments = ''  and @FinalLocked=''        
   BEGIN          
    UPDATE tblScheduleClosing    WITH (UPDLOCK)            
     SET SchDate = @SchDate,        
     FeeSheet = @FeeSheet,        
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END         
   else IF @FeeSheet ='' and @HomeownerInsurance ='' and @TitleCommitment='' and @InsuredClosingProtectionLetter=''  and @RateLockConfirmation='' and @Invoices='' and @Comments<>''  and @FinalLocked=''        
   BEGIN          
    UPDATE tblScheduleClosing    WITH (UPDLOCK)            
     SET SchDate = @SchDate,             
     Comments = @Comments,           
     SubmitedDate = GETDATE(),[Time]=@time           
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END           
   else IF @FeeSheet ='' AND @Comments ='' and @HomeownerInsurance ='' and @TitleCommitment='' and @InsuredClosingProtectionLetter=''  and @RateLockConfirmation='' and @Invoices='' and @FinalLocked=''         
   BEGIN            
    UPDATE tblScheduleClosing    WITH (UPDLOCK)             
     SET SchDate = @SchDate,            
     SubmitedDate = GETDATE(),[Time]=@time
     ,AppStatus=@AppStatus,rushStatus=@rushStatus,ReDrawStatus=@ReDrawStatus
     WHERE LoanNo = @LoanNo          
   END          
 END          
 ELSE          
 BEGIN           
  INSERT INTO tblScheduleClosing     
 ([LoanNo]    
 ,[SchDate]    
 ,[FeeSheet]    
 ,[HomeownerInsurance]    
 ,[TitleCommitment]    
 ,[InsuredClosingProtectionLetter]    
 ,[RateLockConfirmation]    
 ,[Invoices]    
 ,[FinalLocked]    
 ,[Userid]    
 ,[IsActive]    
 ,[SubmitedDate]    
 ,[Comments]    
 ,[IsFromLoanSuspendedDoc]    
 ,[Time]
,AppStatus
,RushStatus
,ReDrawStatus
)
  VALUES         
  (        
   @LoanNo        
   ,@SchDate        
   ,@FeeSheet        
   ,@HomeownerInsurance        
   ,@TitleCommitment        
  ,@InsuredClosingProtectionLetter        
   ,@RateLockConfirmation        
   ,@Invoices        
   ,@FinalLocked        
   ,@Userid        
   ,1        
   ,GETDATE()        
   ,@Comments        
   ,1    
	,@time
	,@AppStatus
	,@RushStatus
	,@ReDrawStatus        
)          
 END         
END 

GO

