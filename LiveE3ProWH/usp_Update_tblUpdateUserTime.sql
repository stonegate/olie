USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Update_tblUpdateUserTime]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Update_tblUpdateUserTime]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[usp_Update_tblUpdateUserTime]
(
@userid varchar(Max)
)
as
begin
	update tblusers set lastlogintime = null,maxloginattempt =0 where userid = @userid
end



GO

