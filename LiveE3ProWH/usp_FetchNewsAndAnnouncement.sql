USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FetchNewsAndAnnouncement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FetchNewsAndAnnouncement]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_FetchNewsAndAnnouncement]
(
@ID int

)
as
begin
	select * from tblNewsAndAnnouncement where ID=@ID order by createddate desc
end


GO

