USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_mwlInstitution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_mwlInstitution]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		 Nitin
-- Create date: 10/05/2012
-- Description:	Procedure for clspipelineE3.cs bll 
-- =============================================
CREATE procEDURE  [dbo].[sp_Get_mwlInstitution]

AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

  select Office from mwlInstitution where InstitutionType in('CORRESPOND','Broker') 
		and office is not null and office <> '' group by office
END


GO

