USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_SelectPartnerCompanyDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_SelectPartnerCompanyDetails]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Example for execute stored procedure       
--EXECUTE [udsp_SelectPartnerCompanyDetails] '478452' 
-- =============================================        
-- Author:  Karan Gulati        
-- Create date: 18-04-2013  
-- Name:udsp_SelectPartnerCompanyDetails.sql      
-- Description: Checks for existing Clients Details for PartnerCompanyId        
-- =============================================        
CREATE PROCEDURE [dbo].[udsp_SelectPartnerCompanyDetails] 
(
 --Parameter declaration            
@PCompanyId varchar(40)  
)      
AS        
BEGIN         
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
SET XACT_ABORT ON;   

SELECT     ID FROM         tblManageMandatoryClientsInfo
WHERE     (PartnerCompanyId = @PCompanyId)  
      
END   
GO

