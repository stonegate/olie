USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_InsertQuery]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_InsertQuery]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author:  <Chandrika>    
-- Create date: <4-Oct-2012>    
-- Description: <Insert query>    
-- =============================================    
CREATE PROCEDURE [dbo].[SP_InsertQuery]    
 (    
@TableName varchar(500),    
@Fieldstring nvarchar(max),    
@Valuestring nVarchar(max)   
    
)    
AS    
BEGIN    
--    
--Insert into  '+@TableName+' ('+@Fieldstring+') values ('+@Valuestring+')    
Declare @SQL nvarchar(max)    
set @SQL = ''    
    
     
set @SQL = 'Insert into '+ @TableName +' ( '+@Fieldstring +' ) values ('''+@Valuestring+''')'    
print @SQL    
    
exec sp_executesql @SQL     
END    
GO

