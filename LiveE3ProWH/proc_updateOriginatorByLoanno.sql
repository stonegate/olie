USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[proc_updateOriginatorByLoanno]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[proc_updateOriginatorByLoanno]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[proc_updateOriginatorByLoanno]
(
 @OriginatorID varchar(50),
 @OriginatorName varchar(50),
 @LoanNumber varchar(50),
 @LoanID varchar(50)
)
AS
BEGIN
begin try 
 update mwlloanapp set 
 Originator_ID=@OriginatorID,
 OriginatorName=@OriginatorName
 where ID=@LoanID and LoanNumber=@LoanNumber
 end try 
 begin catch
 

 end catch
 END

GO

