USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLoanDocsSubmitLoans]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLoanDocsSubmitLoans]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  <Author,,Name>      
-- Create date: <Create Date,,>      
-- Description: <Description,,>      
-- =============================================      
CREATE PROCEDURE sp_InsertLoanDocsSubmitLoans      
(      
 @BorrLastName varchar(200),      
 @UserID int,      
 @DocName varchar(200),      
 @LoanNumber varchar(20)      
)      
AS      
BEGIN      
 Insert Into       
  tblloandoc      
    (Comments,userid,DocName,loan_no)       
  Values      
    (@BorrLastName,@UserID,@DocName,@LoanNumber)      
END 
GO

