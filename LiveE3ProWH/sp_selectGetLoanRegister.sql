USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_selectGetLoanRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_selectGetLoanRegister]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Name  
-- Create date:   
-- Description:   
-- =============================================  
CREATE PROCEDURE [dbo].[sp_selectGetLoanRegister]
(  
@urole bigint  
,@userId bigint  
 
)  
AS  
BEGIN 
if(@urole=0)
begin 
select LR.*,LK.Description as TranscationType from tblloanreg LR  
Left outer join tbllookups LK on LR.TransType=LK.Lookupid ORDER BY CreatedDate DESC
end
else
begin
select LR.*,LK.Description as TranscationType from tblloanreg LR 
 Left outer join tbllookups LK on LR.TransType=LK.Lookupid
 where userid=@userId ORDER BY CreatedDate DESC
end

END  

GO

