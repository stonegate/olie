USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tblGetEmailID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tblGetEmailID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[usp_Select_tblGetEmailID]
(
@iUserId varchar(50)
)
as
begin
	select upassword,uemailid,ufirstname +' '+ ISNULL(MI,'') +' '+ ulastname as UserName,Roles  
	from tblusers u left outer join tblroles r on r.Id=u.urole 
	where userid =@iUserId
end


GO

