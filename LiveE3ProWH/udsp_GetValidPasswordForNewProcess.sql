USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetValidPasswordForNewProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetValidPasswordForNewProcess]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		< author name >
-- Create date: 25/10/2012
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[udsp_GetValidPasswordForNewProcess]
@emailID varchar(max),
@userloginid varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select userid from tblusers where Userloginid=@userloginid and uemailid =@emailID and IsActive=1
END


GO

