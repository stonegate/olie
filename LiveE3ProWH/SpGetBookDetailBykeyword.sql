USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetBookDetailBykeyword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetBookDetailBykeyword]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetBookDetailBykeyword] 
	@Keyword varchar(500)
AS
BEGIN
	select *  from BookDetail where description like '%' + @Keyword +'%'  OR IssueName like '%' + @Keyword +'%' 
END

GO

