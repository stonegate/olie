USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert54PendingDateTime]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert54PendingDateTime]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE procedure [dbo].[sp_Insert54PendingDateTime]  
(  
@LoanAppID varchar(32),  
@CUSTOMFieldIDDate varchar(32),  
@CUSTOMFieldIDTime varchar(32),  
@CUSTOMFieldIDUEmail varchar(32),  
@Time varchar(256),  
@UemailID varchar(max)  
)  
AS  
BEGIN  
if not exists(select * From mwlcustomfield where CustFieldDef_ID in (@CUSTOMFieldIDTime)and  LoanApp_id in(@LoanAppID))  
 begin  
  Insert into mwlcustomfield (ID,LoanApp_ID,CustFieldDef_ID,StringValue) values(replace(newid(),'-',''), @LoanAppID, @CUSTOMFieldIDTime,@Time)  
 END    
  
      ---for Date    
         
if not exists(select * From mwlcustomfield where CustFieldDef_ID in (@CUSTOMFieldIDDate) and  LoanApp_id in(@LoanAppID))       
 BEGIN   
  Insert into mwlcustomfield (ID,LoanApp_ID,CustFieldDef_ID,DateValue) values(replace(newid(),'-',''), @LoanAppID, @CUSTOMFieldIDDate,getdate())  
 END  
  
     ---For Uemail  
  
if not exists(select * From mwlcustomfield where CustFieldDef_ID in (@CUSTOMFieldIDUEmail) and  LoanApp_id in(@LoanAppID))       
 BEGIN   
  Insert into mwlcustomfield (ID,LoanApp_ID,CustFieldDef_ID,MemoValue) values(replace(newid(),'-',''), @LoanAppID, @CUSTOMFieldIDUEmail,@UemailID)  
 END  
END  
  
GO

