USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAMBDMEmailForKatalyst]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetAMBDMEmailForKatalyst]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Shwetal,,Shah>  
-- Create date: <03 March 2013>  
-- Description: <This Stored procedure used for Get BDM information from Website DB>  
---Modify By: <Vipul, Thacker>  
-- Modify date: <04 April 2013>  
-- =============================================  
CREATE PROCEDURE [dbo].[GetAMBDMEmailForKatalyst]  
(  
 @UserID int  
)  
AS  
BEGIN  
  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads.   
  
SELECT BDM.uemailid AS BDMEmail, (CASE WHEN ISNULL(tblAM.uemailid,'') = '' THEN DOCFI.uemailid ELSE tblAM.uemailid END) AMEmailID, tblBroker.AEID
FROM tblusers tblBroker WITH(NOLOCK)  
 LEFT JOIN dbo.tblUsers  BDM WITH(NOLOCK) ON tblBroker.AEID = BDM.userid  
 LEFT JOIN dbo.tblUsers  tblAM WITH(NOLOCK) ON tblAM.region= BDM.Region AND tblAM.urole = 8  -- AREA MANAGER
 LEFT JOIN dbo.tblUsers DOCFI WITH(NOLOCK) ON DOCFI.region= BDM.Region AND DOCFI.urole = 25 -- DIRECTOR OF CFI
Where tblBroker.userid = @UserID  
   
END  
GO

