USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetTotalMandatoryLockLoans]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetTotalMandatoryLockLoans]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Example for execute stored procedure       
--EXECUTE udsp_GetTotalMandatoryLockLoans '478452'   
-- =============================================            
-- Author:  Karan Gulati            
-- Create date: 12/04/2013
-- Name: udsp_GetTotalMandatoryLockLoans.sql           
-- Description: Retrives Total Mandatory Lock Loans 
-- =============================================            
            

CREATE PROCEDURE [dbo].[udsp_GetTotalMandatoryLockLoans] 
(
 --Parameter declaration                 
 @PartnerCompanyId varchar(40)   
 )         
 AS            
 BEGIN             
 SET NOCOUNT ON;  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
 SET XACT_ABORT ON;-- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error, 
				   -- the entire transaction is terminated and rolled back.  
             
SELECT     SUM(dbo.mwlLockRecord.AdjustedNoteAmt) AS TotalLockedLoanAmount
FROM         mwlLoanApp INNER JOIN
                      mwlInstitution ON dbo.mwlInstitution.objowner_id = dbo.mwlLoanApp.ID INNER JOIN
                      mwlLockRecord ON dbo.mwlLockRecord.LoanApp_id = dbo.mwlLoanApp.ID
WHERE     (dbo.mwlInstitution.companyemail = @PartnerCompanyId) AND (dbo.mwlLockRecord.deliveryoption = 'Mandatory') AND (dbo.mwlLockRecord.LockType = 'LOCK') AND (CONVERT(VARCHAR, 
                      dbo.mwlLoanApp.CurrentStatus, 2) < '62')               
END 
GO

