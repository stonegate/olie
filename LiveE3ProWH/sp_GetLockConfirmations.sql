USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLockConfirmations]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLockConfirmations]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Stonegate    
-- Create date: 10 Apr 2012            
-- Description: This will get ALL LOCK CONFIRMATION PAGE FIELDS     
-- =============================================     
-- Changes By Mitesh Rajyaguru task id 10375
-- Modified Date date: 02 May 2013     
CREATE procedure [dbo].[sp_GetLockConfirmations]     
    
	@LoanNumber varchar(50)        
AS
      
BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
	SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error 
		
SELECT LoanApp.LoanNumber,LoanApp.OriginatorName,LoanApp.branch,LockRecord.LockDateTime,CONVERT(DECIMAL(10,2),LockRecord.AppraisedValue) as AppraisedValue,LockRecord.Occupancy,LockRecord.PropertyType,LockRecord.Units,LockRecord.RefiPurpose ,LockRecord.comments,      
CONVERT(DECIMAL(10,2),LockRecord.AdjustedNoteAmt) as AdjustedNoteAmt,CONVERT(DECIMAL(10,3),LockRecord.LTV * 100) as LTV,CONVERT(DECIMAL(10,3),LockRecord.CLTV * 100) as CLTV,LockRecord.LoanProgramName,LockRecord.TransactionType,convert(DECIMAL(10,3),LockRecord.Margin) as Margin,convert(DECIMAL(10,3),LockRecord.LockRate * 100) as LockRate, LockRecord.LockDays,LockRecord.LockExpirationDate,borr.LastName,borr.FirstName,borr.MiddleName,LockRecord.LoanCompositeScore as CompositeCreditScore,        
CONVERT(DECIMAL(10,2),LoanData.MIPremiumAmt) as MIPremiumAmt,LockRecord.SalesPrice,LoanApp.MERSMIN,
--LoanData.OtherFinMonthly,
OtherMort.ExistingBalance as OtherFinMonthly,LockRecord.WaiveEscrows,        
LockRecord.SPStreet as Street,LockRecord.SPStreet2 as Street2,LockRecord.SPCity as City,LockRecord.SPState as [State],LockRecord.SPZipcode as Zipcode, 
convert(DECIMAL(10,3),PricingResult.LenderClosingPrice * 100) as LenderClosingPrice , CASE Midata.PremiumSource WHEN 'BORROWER' THEN 'Borrower Paid' WHEN 'LENDER' THEN 'Lender Paid' ELSE '' END AS PremiumSource,LockRecord.DeliveryOption as DeliveryMethod 
from mwlLoanApp LoanApp  
LEFT JOIN mwlBorrower as borr on borr.loanapp_id = LoanApp.id AND borr.SEQUENCENUM=1
LEFT JOIN dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = LoanApp.id  and locktype='LOCK' 
INNER JOIN mwlLoanData as LoanData on  LoanData.ObjOwner_id =LoanApp.id and LoanData.active=1 
INNER JOIN mwlOtherMortgageData as OtherMort on  LoanData.ID =OtherMort.loandata_ID
INNER JOIN mwlSubjectProperty as SubjectProperty on SubjectProperty.LoanApp_ID  =LoanApp.id 
LEFT OUTER JOIN mwlPricingResult as PricingResult ON PricingResult.ObjOwner_ID=LockRecord.ID 
LEFT OUTER JOIN mwlmidata as Midata ON Midata.LoanData_id=LoanData.ID where LoanApp.LoanNumber=@LoanNumber   
order by DateTimeAdded desc   

    
END

--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





GO

