USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetRetailMAOfficeIDs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetRetailMAOfficeIDs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Stonegate>
-- Create date: <10/9/2012>
-- Description:	<This will get GetRetailMAOfficeIDs>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRetailMAOfficeIDs]
(
	@strCurrentStatus varchar(2500)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)

Select 
	distinct(isnull(office,''))office 
From mwlinstitution 
	inner join mwlloanapp on mwlloanapp.ID = mwlinstitution.objOwner_id 
Where InstitutionType = 'Branch' 
	and objownerName='BranchInstitution' 
	and ChannelType='Retail'
	and CurrentStatus IN (select * from dbo.SplitString('+@strCurrentStatus+',','))
	
END

GO

