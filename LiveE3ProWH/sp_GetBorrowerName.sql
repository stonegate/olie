USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBorrowerName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBorrowerName]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  sp_GetBorrowerName
	-- Add the parameters for the stored procedure here

@loan_no varchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

SELECT RTRIM(borr_last) FROM cobo INNER JOIN loandat ON loandat.record_id = cobo.parent_id WHERE loan_no =@loan_no 

END

GO

