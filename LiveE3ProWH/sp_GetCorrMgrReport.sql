USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCorrMgrReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCorrMgrReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Kanva>
-- Create date: <4/10/2012>
-- Description:	<This will get Corr. Manager report>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetCorrMgrReport] 
(
	@strStatusDesc varchar(200),
	@typeofReview varchar(200),
	@strLoanStatus varchar(200),
	@strChannelType varchar(200),
	@dtReportDate varchar(20)
)	
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

Declare @SQL varchar(8000)
set @SQL = ''

set @SQL = 'Select usrDocumentPrepair.CreatedOnDate as DocumentPreparerJoinDate,
usrOrg.CreatedOnDate as OriginatorJoinDate,usrPross.CreatedOnDate as ProcessorJoinDate, 
usrClose.CreatedOnDate as CloserJoinDate,usrUnder.CreatedOnDate as UnderWritterJoinDate,uwcIns.CreatedOnDate as CorrJoinDate,
'+ @strStatusDesc +' as StatusDesc,'+ @typeofReview +' as ReviewType,
loanData.DebtRatio, loanapp.LoanCompositeScore, loanapp.DocumentPreparerName,
Corres.Company as Correspondent,''E3'' as DB,LoanNumber as loan_no, Convert(varchar,CONVERT(DECIMAL(10,3),
loanData.NoteRate * 100)) + ''%'' as interest_rate, appStat.statusdatetime as ApplicationDate,
appStat_ao.statusdatetime as StatusDate, loanData.PropertyType, LookupsPro2.DisplayString as Occupancy, loanData.Occupancy as Occupancy1,
convert(varchar,round((loanData.LTV*100),3)) + ''%'' as LTV, convert(varchar, round((loanData.CLTV*100),3)) + ''%'' as CLTV,
loanData.AdjustedNoteAmt as loan_amt,
borr.FullName,  loanapp.ProcessorName, LookupsPro1.DisplayString as LoanPurpose,loanapp.CloserName,loanapp.OriginatorName, loanapp.currentStatus as LoanStatus,
appStat.StatusDateTime AS SubmittedDate,sp.street,sp.city, sp.State, sp.County,sp.ZipCode, uSummary.Underwritername, LookupsPro.DisplayString as LoanProgram
from mwlLoanApp  loanapp 
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id 
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc=''09 - Application Received''
Left join mwlAppStatus appStat_ao on loanapp.ID=appStat_ao.LoanApp_id and appStat_ao.StatusDesc='+ @strLoanStatus +'
left join dbo.mwlsubjectProperty as sp on sp.LoanApp_ID = loanapp.ID
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''
Left join dbo.mwlInstitution as Corres1 on Corres1.ObjOwner_id = loanapp.id and Corres1.InstitutionType = ''REVIEWAPPR'' and Corres.objownerName=''Contacts''
Left outer join mwlLookups LookupsPro on LoanData.FinancingType=LookupsPro.BOCode  and LookupsPro.ObjectName=''mwlLoanData'' and LookupsPro.fieldname=''FinancingType''
Left outer join mwlLookups LookupsPro1 on Loanapp.LoanPurpose1003=LookupsPro1.BOCode  and LookupsPro1.ObjectName=''mwlLoanApp''
Left outer join mwlLookups LookupsPro2 on LoanData.Occupancy=LookupsPro2.BOCode  and LookupsPro2.ObjectName=''mwlLoanData'' and LookupsPro.fieldname=''Occupancy''
left join mwsCustFieldDef as mCust on  mCust.CustomFieldName like''Correspondent Type%''
left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id and Cust.CustFieldDef_ID =mCust.id
left join mwaMWUser as usrOrg on loanapp.Originator_id=usrOrg.id
left join mwaMWUser as usrPross on loanapp.Processor_ID=usrPross.id
left join mwaMWUser as usrClose on loanapp.Closer_ID=usrClose.id
left join mwaMWUser as usrDocumentPrepair on loanapp.DocumentPreparer_id=usrDocumentPrepair.id
left join  mwlUnderwritingSummary as uwSumm on uwSumm.LoanApp_ID = loanapp.id
left join mwaMWUser as usrUnder on uwSumm.Underwriter_ID= usrUnder.id
Left join mwlInstitution as uwIns on uwIns.ObjOwner_id = loanapp.id and uwIns.InstitutionType = ''CORRESPOND'' and uwIns.objownerName=''Correspondent''
Left Join mwcInstitution as uwcIns on uwcIns.Company = Corres.Company and uwcIns.InstitutionType = ''CORRESPOND''
where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' '

if (@strChannelType = 'RETAIL')
Begin
	set @SQL = @SQL + ' and loanapp.ChannelType = '+ @strChannelType +' '
End
else
Begin
	if (@strChannelType = 'BROKER')
    Begin
       set @SQL = @SQL + ' and (loanapp.ChannelType = '+ @strChannelType +' or StringValue=''Prior Approved'') '
	End
	else if (@strChannelType = 'CORRESPOND')
    Begin
	   set @SQL = @SQL + ' and( StringValue=''Delegated'') '
    End
End
set @SQL = @SQL + ' and Convert(varchar(30),appStat_ao.StatusDateTime,101) = '+ @dtReportDate +' '
set @SQL = @SQL + ' order by appStat.statusdatetime desc '
Print @SQL
exec sp_executesql @SQL	
END


GO

