USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchBranchDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchBranchDetail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_FetchBranchDetail] 
	-- Add the parameters for the stored procedure here
@StatebelongTo varchar(50)=null,
@sOperation	 varchar(20)=NULL

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


	BEGIN
	IF @sOperation='1'
	select * from tblusers where 
	(StatebelongTo like '%' + @StatebelongTo + '%') and urole = 8 and isactive = 1; 
	select * from tblusers where (StatebelongTo like '%' + @StatebelongTo + '%') 
	and urole = 2 and isactive = 1
	
	END
	
	
END

GO

