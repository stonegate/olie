USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_user]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_user]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_user]
AS
     SELECT  * FROM tblusers inner join tblnewroles on tblusers.id=tblnewroles.id where urole in(3)

GO

