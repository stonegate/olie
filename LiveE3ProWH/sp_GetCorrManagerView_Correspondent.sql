USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCorrManagerView_Correspondent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCorrManagerView_Correspondent]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:  Stonegate
-- Create date: 12th July 2012        
-- Description: This will get Correspondant Vendor Role Data Report 
-- =============================================        
CREATE PROCEDURE [dbo].[sp_GetCorrManagerView_Correspondent]        
(        
        
	@whereClause varchar(15),   
	@CurrentStatus varchar(100),  
	@OriginatorId varchar(40),
	@ShowAllData int -- If 1 than display all data and no count
)         
AS        
BEGIN    


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
		SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error 
		
If @ShowAllData = 0 AND @whereClause <> '' 
	Begin
	--Below Query Return Total Count,Total amount and Avg. we had used SplitString function because multiple		  current status passed in this string 
		Select count(loannumber) as Count,ceiling(sum(loanData.AdjustedNoteAmt)) as TotalAmt, 
		ceiling(sum(loanData.AdjustedNoteAmt)/count(loannumber)) as avg from mwlloanapp loanapp
		Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and 
		loanData.Active=1 where DocumentPreparer_ID= @OriginatorId and  LoanNumber like '%'+@whereClause+'%'         and currentstatus IN  (select items from dbo.SplitString(@CurrentStatus,',')) 
		 
	End
	If @ShowAllData = 0 AND @whereClause= '' 
	Begin
	--Below Query Return Total Count,Total amount and Avg. we had used SplitString function because multiple		  current status passed in this string 
		Select count(loannumber) as Count,ceiling(sum(loanData.AdjustedNoteAmt)) as TotalAmt, 
		ceiling(sum(loanData.AdjustedNoteAmt)/count(loannumber)) as avg from mwlloanapp loanapp
		Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and 
		loanData.Active=1 where DocumentPreparer_ID= @OriginatorId and currentstatus IN  (select items from			dbo.SplitString(@CurrentStatus,',')) 
		 
	End
--Below Query Return Loan Data if we add where Clause Cretieria
IF @whereClause <> ''                                             
	BEGIN       
	
		Select  loanapp.LoanNumber as loan_no,'E3' as DB,Corres.Company as coname,loanapp.CurrPipeStatusDate as			CurrentStatusDate ,OriginatorName,'' as Per,'' as LastSubmittedDate, borr.lastname as BorrowerLastName,CASE TransType when NULL			THEN '' WHEN 'P' THEN 'Purchase money first mortgage' WHEN 'R' THEN 'Refinance' WHEN '2' THEN 'Purchase			money second mortgage' WHEN 'S' THEN 'Second mortgage, any other purpose' WHEN 'A' THEN 'Assumption' WHEN		'HOP' THEN 'HELOC - other purpose' WHEN 'HP' THEN 'HELOC - purchase' ELSE '' END AS TransType,loanapp.			CurrentStatus as LoanStatus,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,				LockRecord.LockDatetime as LockDatetime,LockRecord.LockExpirationDate,'' as CorrespodentFSpecialist ,			Loandata.LoanProgDesc as ProgramDesc ,BOffices.Office as OFFICE_NAME1 from mwlLoanApp loanapp Inner join		mwlBorrower as borr on borr.loanapp_id = loanapp.id and borr.Sequencenum=1 Inner join dbo.mwlloandata as		loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active=1 left join dbo.mwlLockRecord as				LockRecord on LockRecord.LoanApp_ID =loanapp.id and LockRecord.LockType='LOCK' and LockRecord.Status<>'CANCELED' Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.InstitutionType ='Branch' and BOffices.objownerName='BranchInstitution'
		Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and 
		Corres.InstitutionType = 'CORRESPOND' and Corres.objownerName='Correspondent' where CurrentStatus  IN			(select items from dbo.SplitString(@CurrentStatus,','))and ChannelType in('CORRESPOND') and borr.				sequencenum=1 and LoanNumber is not null and LoanNumber <> '' 
		and (DocumentPreparer_ID =@OriginatorId) and (LoanNumber like '%'+@whereClause+'%'  OR borr.lastname like		'%'+@whereClause+'%' OR loanapp.CurrentStatus like '%'+@whereClause+'%'  OR LockRecord.[Status] like  
		'%'+@whereClause+'%')
		
	END
ELSE	
	BEGIN 
	--Below Query Return Loan Data if without where Clause Cretieria
        Select  loanapp.LoanNumber as loan_no,'E3' as DB,Corres.Company as coname,loanapp.CurrPipeStatusDate as			CurrentStatusDate ,OriginatorName,'' as Per,'' as LastSubmittedDate, borr.lastname as BorrowerLastName,CASE TransType when NULL	THEN '' WHEN 'P' THEN 'Purchase money first mortgage' WHEN 'R' THEN 'Refinance' WHEN '2' THEN 'Purchase			money second mortgage' WHEN 'S' THEN 'Second mortgage, any other purpose' WHEN 'A' THEN 'Assumption' WHEN		'HOP' THEN 'HELOC - other purpose' WHEN 'HP' THEN 'HELOC - purchase' ELSE '' END AS TransType,loanapp.			CurrentStatus as LoanStatus,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,				LockRecord.LockDatetime as LockDatetime,LockRecord.LockExpirationDate,'' as CorrespodentFSpecialist ,			Loandata.LoanProgDesc as ProgramDesc ,BOffices.Office as OFFICE_NAME1 from mwlLoanApp loanapp Inner join		mwlBorrower as borr on borr.loanapp_id = loanapp.id and borr.Sequencenum=1 Inner join dbo.mwlloandata as		loanData on loanData.ObjOwner_id = loanapp.id  and loanData.Active=1 left join dbo.mwlLockRecord as				LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType='LOCK' and LockRecord.Status<>			'CANCELED' Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.			InstitutionType ='Branch' and BOffices.objownerName='BranchInstitution'
		Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and 
		Corres.InstitutionType = 'CORRESPOND' and Corres.objownerName='Correspondent' where CurrentStatus  IN			(select items from dbo.SplitString(@CurrentStatus,','))and ChannelType in('CORRESPOND') and borr.				sequencenum=1 and LoanNumber is not null and LoanNumber <> '' 
		and (DocumentPreparer_ID =@OriginatorId)   
	END        

END 


GO

