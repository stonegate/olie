USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchTMP]') AND type in (N'U'))
DROP TABLE [dbo].[SearchTMP]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SearchTMP](	  [LoanDocId] BIGINT NOT NULL IDENTITY(1,1)	, [DocName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [userid] INT NULL	, [createddate] DATETIME NULL	, [loan_no] VARCHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL	, [Comments] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO

