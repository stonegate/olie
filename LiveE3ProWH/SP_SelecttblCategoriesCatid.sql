USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SelecttblCategoriesCatid]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SelecttblCategoriesCatid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

Create PROCEDURE [dbo].[SP_SelecttblCategoriesCatid]
(
@CateName varchar(50)
)

AS
BEGIN
	   select count(catid) tot from tblCategories where CateName=@CateName
             
END



GO

