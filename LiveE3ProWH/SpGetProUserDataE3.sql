USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetProUserDataE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetProUserDataE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetProUserDataE3] 
	@RecId varchar(500)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)

	select ID as Record_id, * from mwaMWUser where username =@RecId
END

GO

