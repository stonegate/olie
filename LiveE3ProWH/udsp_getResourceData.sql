USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_getResourceData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_getResourceData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[udsp_getResourceData]
(
@Type Varchar(Max)
)
as
begin
	if(@Type='Transaction')
	begin
		select 'P' as id,'Purchase' as Trantype 
		union  
		select 'R' as id,'Refinance' as Trantype 

	end
	else if (@Type='Channel')
	begin
		select 'RE' as id,'Retail' as channelName 
		union  
		select 'WH' as id,'WholeSale' as channelName
	end
end
GO

