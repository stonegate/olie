USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetReportdashboardSuspendedFilesection]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetReportdashboardSuspendedFilesection]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

        
CREATE PROCEDURE [dbo].[sp_GetReportdashboardSuspendedFilesection] --'3755'--'2852'        
  
(      
  
@Userid varchar(max)      
  
)             
  
as              
  
begin        
  
  
  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)      
  
  
 
  declare @strid varchar(max)  

  set @strid=(select e3userid from tblusers where userid = @Userid)      
  
SELECT TOP 4 * from     --> For filteration  
 (  
   select    
  
     mwlBorrower.LastName AS BorrowerLastName,            
  
    mwlLoanApp.LoanNumber,            
  
    mwlLoanApp.DecisionStatus,            
  
    CASE mwlLoanApp.TransType            
  
     WHEN 'P' THEN 'Purchase'            
  
     WHEN 'R' THEN 'Refinance'            
  
    END AS TransType,            
  
    mwlLoanApp.CurrentStatus AS LoanStatus,            
  
    mwlLoanApp.OriginatorName AS BDM,            
  
    CASE ISNULL(RTRIM(Offices.Company),'')            
  
     WHEN '' THEN RTRIM(Corres.Company)            
  
     ELSE RTRIM(Offices.Company)            
  
    END AS TPOPartner,            
  
   -- (Convert (datetime,(Convert(varchar(30),Cust.DateValue,111)+' '+CustTime.stringvalue))) as SuspendedDate, 
	CONVERT(datetime,(CONVERT(varchar(30), Cust.DateValue, 111) + ' ' + CASE WHEN ISDATE(CustTime.stringvalue) = 1 THEN CONVERT(VARCHAR(8),CustTime.stringvalue,108) ELSE '00:00:00.000' END)) AS SuspendedDate,        
  
    mwlLoanApp.LoanCompositeScore AS FICO,   
  
	CONVERT(VARCHAR(50),  CONVERT(datetime,(CONVERT(varchar(30), Cust.DateValue, 111) + ' ' + CASE WHEN ISDATE(CustTime.stringvalue) = 1 THEN CONVERT(VARCHAR(8),CustTime.stringvalue,108) ELSE '00:00:00.000' END))) AS SuspendedConditionDate,        

	CustTime.StringValue as SuspendedConditionTime      
  
     ,Convert(varchar(30), LastCustDate2.DateValue, 101) as SuspendedConditionReviewedDate, LastCustDate2.StringValue as SuspendedConditionReviewedTime      
  
     FROM mwlLoanApp        
  
     INNER JOIN mwlBorrower ON LoanApp_ID = mwlLoanApp.ID and RLABorrCoborr=1 and Sequencenum=1        
  
     INNER JOIN mwlLoanData ON mwlLoanData.ObjOwner_ID = mwlLoanApp.ID        
  
     LEFT JOIN mwlInstitution ON mwlLoanApp.ID = mwlInstitution.ObjOwner_ID and mwlInstitution.InstitutionType='BROKER'        
  
     LEFT JOIN mwlLockRecord ON mwlLockRecord.LoanApp_ID = mwlLoanApp.ID and mwlLockRecord.LockType='LOCK' and  mwlLockRecord.Status<>'CANCELED'        
  
     LEFT JOIN mwlUnderwritingSummary ON mwlUnderwritingSummary.LoanApp_ID = mwlLoanApp.ID        
  
     LEFT JOIN mwaMWUser ON mwaMWUser.ID = mwlLoanApp.Originator_ID        
  
     LEFT JOIN mwlAppStatus AS SubUW ON SubUW.LoanApp_ID = mwlLoanApp.ID AND SubUW.StatusDesc='25 - Submitted to Underwriting'        
  
     LEFT JOIN mwlAppStatus AS InUW ON InUW.LoanApp_ID = mwlLoanApp.ID AND InUW.StatusDesc='29 - In Underwriting' and mwlLoanApp.DecisionStatus='Not Decisioned'        
  
     LEFT JOIN mwlAppStatus AS InSus ON InSus.LoanApp_ID = mwlLoanApp.ID AND InSus.StatusDesc='29 - In Underwriting' and mwlLoanApp.DecisionStatus='Suspended'        
  
     LEFT JOIN mwlAppStatus AS InAppr ON InAppr.LoanApp_ID = mwlLoanApp.ID AND InAppr.StatusDesc='29 - In Underwriting' and mwlLoanApp.DecisionStatus='Conditionally Approved'        
  
     LEFT JOIN mwlAppStatus AS AppDt ON AppDt.LoanApp_ID = mwlLoanApp.ID AND AppDt.StatusDesc='01 - Registered'        
  
     LEFT JOIN mwlAppStatus AS InUWDate ON InUWDate.LoanApp_ID = mwlLoanApp.ID AND InUWDate.StatusDesc='29 - In Underwriting'        
  
     Left join dbo.mwlInstitution as Offices on Offices.ObjOwner_id = mwlLoanApp.id and Offices.InstitutionType = 'Broker' and Offices.objownerName='Broker'        
  
     Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = mwlLoanApp.id and COffices.InstitutionType = 'CORRESPOND' and COffices.objownerName='Contacts'        
  
     Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = mwlLoanApp.id and BOffices.InstitutionType = 'Branch' and BOffices.objownerName='BranchInstitution'        
  
     Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = mwlLoanApp.id and Corres.InstitutionType = 'CORRESPOND' and Corres.objownerName='Correspondent'       
  
     left join mwlcustomfield as Cust on Cust.loanapp_id = mwlLoanApp.id  and Cust.CustFieldDef_ID ='DCEA042571A74C23953BB691DC5516C8' --  Suspended Condition Date  
  
     left join mwlcustomfield as CustTime on CustTime.loanapp_id = mwlLoanApp.id and CustTime.CustFieldDef_ID ='101F2C44100B497DB02EB66867073DDF'    -- Suspended Condition Time  
  
     Left JOIN mwlcustomfield as LastCustDate2 on LastCustDate2.loanapp_id = mwlLoanApp.id  and LastCustDate2.CustFieldDef_ID = '4E2E2CFFA84D4E71AEA440F981EDA790'   -- Suspended Condition Reviewed Date   
  
     where  CurrentStatus IN ('29 - In Underwriting')     
  
   and (Convert (datetime,(Convert(varchar(30),Cust.DateValue,111)+' '+CustTime.stringvalue)))  is not null  
  
     and mwlUnderwritingSummary.underwriter_id in (select * from dbo.SplitString(@strid,''))  
     
 and   
  
  ( ( LastCustDate2.DateValue is null )  
  
  or (   

         ( CONVERT(DATETIME,(CONVERT(VARCHAR(30),LastCustDate2.DateValue,111) + ' ' + CASE WHEN ISDATE(LastCustDate2.StringValue) = 1 THEN CONVERT(VARCHAR(8),LastCustDate2.StringValue,108) ELSE '00:00:00.000' END )) )  
         <  
         ( CONVERT(DATETIME,(CONVERT(VARCHAR(30),Cust.DateValue,111) + ' ' + CASE WHEN ISDATE(CustTime.StringValue) = 1 THEN  CONVERT(VARCHAR(8),CustTime.StringValue,108) ELSE '00:00:00.000' END )) )  
     )  
  )  
  
 ) Result where DecisionStatus = 'Suspended' --and SuspendedDate is not null --> For filteration  
  
 ORDER BY SuspendedDate asc    
  
End



GO

