USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_UpdateCorrSpecialistByCorrManager]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_UpdateCorrSpecialistByCorrManager]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




--Example for execute stored procedure       
--EXECUTE [udsp_UpdateCorrSpecialistByCorrManager] '0000367141',  'SHARON ALLEN' , '9526','9525','9524',0 
-- =============================================      
-- Author:  Amit Saini      
-- Create date: 06/11/2013   
-- Name:udsp_UpdateCorrSpecialistByCorrManager.sql
-- Description: Update customfields for Name, Date and Time. 
-- Custom Fields:
-- For Closing Package Recieved Tab View
   -- Corr Spec CL : 9526
   -- Corr Spec CL Date Assigned: 9525
   -- Corr Spec CL Time Assigned: 9524
-- Closing Package Reviewed Tab View   
   -- Corr Vend CL : 9523
   -- Corr Vend CL Date Assigned: 9522
   -- Corr Vend CL Time Assigned: 9521  
-- Purchase Submission On Hold Tab View   
   -- Corr Spec POH : 9520
   -- Corr Spec POH Date Assigned: 9519
   -- Corr Spec POH Time Assigned: 9518   
-- =============================================  
CREATE PROCEDURE [dbo].[udsp_UpdateCorrSpecialistByCorrManager] 
( 
 --Parameter declaration   
@loannumber varchar(15),    
@CustName varchar(100),      
@CustAssignedName varchar(10),      
@CustAssignedDate varchar(10),      
@CustAssignedTime varchar(10),   
@Result int output    
)
AS      
BEGIN   
SET NOCOUNT ON;          
 
 DECLARE @LoanApp_id varchar(50), 
         @CustomName varchar(100),      
         @CustomAssignedName varchar(50),      
         @CustomAssignedDate varchar(50),      
         @CustomAssignedTime varchar(50),
         @Date varchar(20),
         @Time varchar(20)         
        
  SET @CustomName = @CustName    
  SELECT @CustomAssignedName = ID from mwscustfielddef where MWFieldNum = @CustAssignedName -- Custom Field Name "Corr Spec CL", Custom Field Number "9526" (For Closing Package Recieved Tab View) 
                                                                                            -- Custom Field Name "Corr Vend CL", Custom Field Number "9523" (For Closing Package Reviewed Tab View)
                                                                                            -- Custom Field Name "Corr Spec POH", Custom Field Number "9520" (For Purchase Submission On Hold Tab View)
  SELECT @CustomAssignedDate = ID from mwscustfielddef where MWFieldNum = @CustAssignedDate -- Custom Field Name "Corr Spec CL Date Assigned:", Custom Field Number "9525" (For Closing Package Recieved Tab View) 
                                                                                            -- Custom Field Name "Corr Vend CL Date Assigned", Custom Field Number "9522" (For Closing Package Reviewed Tab View)
                                                                                            -- Custom Field Name "Corr Spec POH Date Assigned", Custom Field Number "9519" (For Purchase Submission On Hold Tab View)   
  SELECT @CustomAssignedTime = ID from mwscustfielddef where MWFieldNum = @CustAssignedTime -- Custom Field Name "Corr Spec CL Time Assigned", Custom Field Number "9524" (For Closing Package Recieved Tab View) 
                                                                                            -- Custom Field Name "Corr Vend CL Time Assigned", Custom Field Number "9521" (For Closing Package Reviewed Tab View)
                                                                                            -- Custom Field Name "Corr Spec POH Time Assigned", Custom Field Number "9518" (For Purchase Submission On Hold Tab View)  
  SELECT @LoanApp_id=ID from mwlLoanApp where loannumber = @loannumber
  SET @Date = Convert(varchar(12),getdate(),101)
  SET @Time = Convert(varchar,getdate(),8)
  SET @Result=0
  
   ----------Assign Name Start--------------------------------------------------------------------   
   if not exists(select ID From mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID = @CustomAssignedName and  LoanApp_id =@LoanApp_id )   
   begin
   insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue)   values  (replace(newid(),'-',''), @LoanApp_id,@CustomAssignedName,@CustomName) 
   end   
   else     
   begin
   UPDATE mwlcustomfield with(updlock) set StringValue=@CustomName where CustFieldDef_ID = @CustomAssignedName and  LoanApp_id = @LoanApp_id     
   end   
   --------Assign Name End---------------------------------------------------------------------- 
    
   --------Assign Date Start-------------------------------------------------------------------- 
   if not exists(select ID From mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID =@CustomAssignedDate  and  LoanApp_id = @LoanApp_id )   
   begin 
   insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,DateValue)   values  (replace(newid(),'-',''), @LoanApp_id,@CustomAssignedDate,@Date)  
   end
   else     
   begin 
   UPDATE mwlcustomfield with(updlock) set DateValue= @Date where CustFieldDef_ID = @CustomAssignedDate  and  LoanApp_id = @LoanApp_id 
   end  
   --------Assign Date End---------------------------------------------------------------------- 
   
   --------Assign Time Start-------------------------------------------------------------------- 
   if not exists(select ID From mwlcustomfield WITH (NOLOCK) where CustFieldDef_ID = @CustomAssignedTime and  LoanApp_id = @LoanApp_id )     
   begin 
   insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue)   values  (replace(newid(),'-',''), @LoanApp_id,@CustomAssignedTime,@Time)
   end   
   else      
   begin     
   UPDATE mwlcustomfield with(updlock) set StringValue=@Time  where CustFieldDef_ID = @CustomAssignedTime  and  LoanApp_id = @LoanApp_id  
   set @Result=1
   end 
  --------Assign Time End--------------------------------------------------------------------
 END
 
 


GO

