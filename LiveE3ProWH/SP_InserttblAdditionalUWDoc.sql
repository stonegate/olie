USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_InserttblAdditionalUWDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_InserttblAdditionalUWDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

CREATE PROCEDURE [dbo].[SP_InserttblAdditionalUWDoc]
	(
@loan_no char(15)
)
AS
BEGIN
	  select * from tblAdditionalUWDoc
           where iscommited = 0 and loan_no=@loan_no
             
END

GO

