USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RegLoan_GetSubjectPropertyInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RegLoan_GetSubjectPropertyInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Stonegate
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_RegLoan_GetSubjectPropertyInfo]
(
@LoanNo varchar(50)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	Declare @LoanID nvarchar(250)
	select @LoanID = ID from mwlloanapp where loannumber = @LoanNo
	
	If @LoanID <> '' or @LoanID is not null
	Begin
		select *from mwlSubjectProperty  where LoanApp_id = @LoanID
	End
END





GO

