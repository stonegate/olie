USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckforSubmitLoanKatalyst]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckforSubmitLoanKatalyst]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec sp_CheckforSubmitLoanKatalyst 0,'view','','0000397797','01 - Registered,05 - Application Taken, 03 - Appt Set to Review Disclosures, 04 - Disclosures Sent, 07 - Disclosures Received ,09 - Application Received,09 - Application Recieved,13 - File Intake,17 - Submission On Hold,19 - Disclosures,20 - Disclosures On Hold,21 - In Processing,25 - Submitted to Underwriting,29 - In Underwriting,33 - Cleared to Close,37 - Pre-Closing,41 - In Closing,45 - Docs Out,50 - Closing Package Received,54 - Purchase Pending,58 - Cleared for Funding,57 - Sent to Funding','C4D2D8038F614DF8A1B8A6622C99FC3E','FD0C2122181F432FB5C8A73E060D10CD'
-- to remove duplication of loan, LockRecord Join has been removed from SP and respective fields are fatched from mwlLoanAPP.
--- fields are LockRecord.Status as LockStatus,LockRecord.LockExpirationDate,LockRecord.LockDateTime
--left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' and LockRecord.Status<>''CANCELED''
-- =============================================  
-- Author:  <Stonegate>  
-- Create date: <10/4/2012>  
-- Description: <This wil Check for SubmitLoan>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_CheckforSubmitLoanKatalyst] (
	@RoleID INT
	,@strFrom VARCHAR(100)
	,@strID VARCHAR(max)
	,@strChLoanno VARCHAR(100)
	,@CurrentStatus VARCHAR(max)
	,@CorrespondentType VARCHAR(max)
	,@CustomField VARCHAR(max)
	)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;-- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	DECLARE @SQL NVARCHAR(max)
	DECLARE @SQL1 NVARCHAR(max)
	DECLARE @SQLFinal NVARCHAR(max)

	SET @SQL = ''
	SET @SQL1 = ''
	SET @SQLFinal = ''
	SET @SQL = 
		'select ''E3'' as DB,CustEscrowAmt.currencyValue AS EscrowAmt,PROPERTY.STATE AS PropertyState,Loandata.loanprogramname AS CustLoanProgram,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.Channeltype as BusType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber

as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate,  

approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,  

LookupsPro.DisplayString as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,loanapp.LockStatus,

CustFNMA.YNValue,loanapp.LockExpirationDate,loanapp.LockDate as LockDateTime,UnderwriterName as UnderWriter,uEmail.Email as Underwriter_Email,

CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,

Lookups.DisplayString as CashOut,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate,  

CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE''

and a.objOwner_ID IN (SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0 WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a

where(DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'')  and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'' )

and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE (SELECT count(a.ID) as TotalCleared

FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'')and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') and a.objOwner_ID

IN(SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100/(SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'')

and a.objOwner_ID IN (SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER,case cust.stringvalue WHEN ''--Select One--'' then '''' else cust.stringvalue end as UnderwriterType

from mwlLoanApp loanapp Inner join mwlCustomField on loanapp.ID = mwlCustomField.LoanAPP_ID and mwlCustomField.CustFieldDef_ID = ''2056EDE88C524680A8827021BBE8F55E'' Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id Inner join dbo.mwlloandata
 as loanData on loanData.ObjOwner_id = loanapp.id '
	SET @SQL1 = 'left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id 

and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker'' 

Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc=''01 - Registered'' Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved''

Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''

Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' left join dbo.mwamwuser as uEmail on uSummary.UnderWriter_id = uEmail.id left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID=''' + @CorrespondentType + 
		'''

 LEFT JOIN mwlsubjectproperty Property ON PROPERTY.Loanapp_id=loanapp.id

 left join mwlcustomfield as CustEscrowAmt on CustEscrowAmt.loanapp_id = loanapp.id And CustEscrowAmt.CustFieldDef_ID =''8460093CE5984EDB8652D11E8F8AEE5A'' Left outer join mwlLookups LookupsPro on LoanData.FinancingType=LookupsPro.BOCode and LookupsPro.ObjectName=''mwlLoanData'' and LookupsPro.fieldname=''FinancingType'' left join mwlcustomfield as CustFNMA on CustFNMA.loanapp_id = loanapp.id 

and CustFNMA.CustFieldDef_ID=''' + @CustomField + ''' where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' and

CurrentStatus IN (select * from dbo.SplitString(''' + @CurrentStatus + ''','',''))'

	IF (
			@RoleID != 0
			OR @RoleID != 19
			)
	BEGIN
		IF (len(@strID) = 0)
		BEGIN
			IF (@RoleID = 13)
			BEGIN
				SET @SQL1 = @SQL1 + ' and loandat.LT_Broker_Rep in (select * from dbo.SplitString(''' + @strID + ''','','')) '
			END
			ELSE IF (@RoleID = 2)
			BEGIN
				SET @SQL1 = @SQL1 + ' and (Originator_id in (select * from dbo.SplitString(''' + @strID + ''','',''))) '
			END
			ELSE IF (
					@RoleID = 3
					OR @RoleID = 20
					OR @RoleID = 21
					)
			BEGIN
				SET @SQL1 = @SQL1 + ' and (loanapp.ID in (select * from dbo.SplitString(''' + @strID + ''','',''))) '
			END
			ELSE
			BEGIN
				IF (
						@strID <> ''
						AND @strID IS NOT NULL
						)
				BEGIN
					SET @SQL1 = @SQL1 + ' and (Originator_id in (select * from dbo.SplitString(''' + @strID + ''','',''))) '
				END
			END
		END
		ELSE
		BEGIN
			IF (@RoleID = 13)
			BEGIN
				SET @SQL1 = @SQL1 + ' and loandat.LT_Broker_Rep in (select * from dbo.SplitString(''' + @strID + ''','',''))'
			END
			ELSE IF (@RoleID = 2)
			BEGIN
				SET @SQL1 = @SQL1 + ' and (Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')))'
			END
			ELSE IF (
					@RoleID = 3
					OR @RoleID = 20
					OR @RoleID = 21
					)
			BEGIN
				SET @SQL1 = @SQL1 + ' and (loanapp.ID in (select * from dbo.SplitString(''' + @strID + ''','',''))) '
			END
			ELSE IF (
					@RoleID = 12
					OR @RoleID = 1
					OR @RoleID = 34
					OR @RoleID = 32
					OR @RoleID = 33
					)
			BEGIN
				SET @SQL1 = @SQL1 + ' and (Originator_id in (SELECT E3Userid FROM dbo.tblUsers WHERE urole IN(2,8)) )'
			END
			ELSE
			BEGIN
				SET @SQL1 = @SQL1 + ' and (Originator_id in (select * from dbo.SplitString(''' + @strID + ''','',''))) '
			END
		END
	END

	SET @SQL1 = @SQL1 + ' AND (LoanNumber like ''%' + @strChLoanno + '%'' OR borr.lastname like ''%' + @strChLoanno + '%'') '

	IF (@strFrom = 'subpack')
	BEGIN
		SET @SQL1 = @SQL1 + ' and loanapp.ChannelType in(''CORRESPOND'') '
	END

	SET @SQL1 = @SQL1 + ' order by Per desc '
	--Print @SQL
	--Print @SQL1
	SET @SQLFinal = @SQL + @SQL1

	PRINT @SQLFinal

	EXEC sp_executesql @SQLFinal
END


GO

