USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckforPurchaseAdviceLoan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckforPurchaseAdviceLoan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--   0000421860
-- AE  [dbo].[sp_CheckforLockConfirmationLoan] 2,'0000414023',' ',121,'1,2'
-- RM  [dbo].[sp_CheckforLockConfirmationLoan] 8,'0000414023',' ',16664,'1,2'
--Dir  [dbo].[sp_CheckforLockConfirmationLoan] 1,'0000414023',' ',16655,'1,2'   
--       [dbo].[sp_CheckforLockConfirmationLoan] 1,'0000726529',' ',16650,'1,2' 
-- Broker(No) [dbo].[sp_CheckforLockConfirmationLoan] 3,'0000426021','10136',334,'1,2' 
-- corr  [dbo].[sp_CheckforLockConfirmationLoan] 20,'0000420456','268552',630,'1,2'  
--hyb  [dbo].[sp_CheckforLockConfirmationLoan] 21,'0000417313','1549',1577,'1,2' 

-- =============================================  
-- Author:  <Stonegate>  
-- Create date: <10/4/2012>  
-- Description: <This wil Check for Lock ConfirmationLoan>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_CheckforPurchaseAdviceLoan]  
(  
 @RoleID int,  
 @strChLoanno varchar(100) ,
 @strID nvarchar(max)=''
)  
AS  
BEGIN  

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)
 

Declare @SQL nvarchar(max)
Set @SQL =''

if len(@strID) = 0
SET  @strID = 0


Set @SQL = @SQL + 'SELECT ''E3'' AS DB
	,loanapp.ChannelType
	,loanapp.Channeltype AS BusType
	,loanapp.OriginatorName
	,loanapp.Originator_Id
	,loanapp.currentStatus AS LoanStatus
    ,loanapp.ID AS record_id
    ,LoanNumber AS loan_no
	,borr.lastname AS BorrowerLastName
	,loanapp.LockStatus
    ,loanapp.LockExpirationDate
	,loanapp.LockDate AS LockDateTime
	,0 AS YNValue
	,loanapp.DecisionStatus
	,loanData.AdjustedNoteAmt as loan_amt
	,'''' as UnderwriterType
	,appStat.StatusDateTime AS SubmittedDate
    ,CASE ISNULL(RTRIM(Broker.Company), '''')
		WHEN ''''
			THEN RTRIM(Corres.Company)
		ELSE RTRIM(Broker.Company)
		END AS BrokerName
FROM mwlLoanApp loanapp
INNER JOIN mwlBorrower AS borr ON borr.loanapp_id = loanapp.id
INNER JOIN dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
--INNER JOIN 
--(
-- select Distinct LoanApp_ID from [dbo].[mwlLockRecord]  where LockType=''LOCK'' and Status in (''CONFIRMED'' , ''EXPIRED'' , ''CANCELED'')
-- )as locktemp on locktemp.LoanApp_ID = loanapp.ID
LEFT JOIN mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc=''01 - Registered''
LEFT JOIN dbo.mwlInstitution AS Broker ON Broker.ObjOwner_id = loanapp.id	AND Broker.InstitutionType = ''BROKER''
	AND Broker.objownerName = ''Broker''
LEFT JOIN dbo.mwlInstitution AS Corres ON Corres.ObjOwner_id = loanapp.id
	AND Corres.InstitutionType = ''CORRESPOND''
	AND Corres.objownerName = ''Correspondent''
	WHERE borr.sequencenum = 1
	AND LoanNumber IS NOT NULL
	AND LoanNumber <> ''''
	AND loanapp.ChannelType <> ''RETAIL''
	AND loanapp.ChannelType <> ''BROKER''
	AND loanapp.Currentstatus in (''58 - Cleared for Funding'' , ''60 - Closed'',''62 - Funded'',''64 - Out for Correction'' ,''66 - In Shipping'',''68 - Allocated'' ,''70 - Loan Shipped'',''74 - Investor Suspense'' ,''77 - Ineligible For Delivery'' ,''78 - Loan Sold'')
	
	'
 IF  ( @RoleID = 20 Or @RoleID = 21)  --  Hybrid/ Correspondent 
    Begin  
		Set @SQL = @SQL + ' and ( Corres.CompanyEmail IN('''+ @strID +''')) '  
    End  
   
 Set @SQL = @SQL + ' AND (LoanNumber like ''%'+@strChLoanno+'%'' OR borr.lastname like ''%'+@strChLoanno+'%'') '  

Print @SQL
exec sp_executesql @SQL  
END

GO

