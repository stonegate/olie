USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Reports_GetLoanDetails_OlieUserEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Reports_GetLoanDetails_OlieUserEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Stonegate  
-- Create date: April, 18 2012  
-- Description: This will give Loan details for the loan number passed  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_Reports_GetLoanDetails_OlieUserEmail]  
(  
@LoanNumber nvarchar(50)
)  
AS  
BEGIN  

	select uFirstname + ' ' + ulastname as 'Olieuser',Uemailid as 'OlieuserEmail' from tblusers where userid in (select userid from dbo.tblLoanHistory where LoanNumber = @LoanNumber)  
END  
  
  
GO

