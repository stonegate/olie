USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetClientTestimonials]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetClientTestimonials]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblGetClientTestimonials]
(
@sUserID varchar(Max)
)
as
begin
SELECT * FROM tblTestimonials WHERE UserID=@sUserID order by SubmittedDate desc
end


GO

