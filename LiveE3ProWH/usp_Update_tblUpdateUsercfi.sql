USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Update_tblUpdateUsercfi]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Update_tblUpdateUsercfi]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



  
  
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[usp_Update_tblUpdateUsercfi]  
 -- Add the parameters for the stored procedure here  
(  
@ufirstname varchar(Max)=null  
,@ulastname varchar(Max)=null  
,@uemailid varchar(Max)=null  
,@StateBelongTo varchar(Max)=null  
,@urole varchar(Max)=null  
,@isadmin varchar(Max)=null  
,@isactive varchar(Max)=null  
,@uidprolender varchar(Max)=null  
,@uparent varchar(Max)=null  
,@upassword varchar(Max)=null  
,@mobile_ph varchar(Max)=null  
,@phonenum varchar(Max)=null  
,@loginidprolender varchar(Max)=null  
,@carrierid varchar(Max)=null  
,@MI varchar(Max)=null  
,@companyname varchar(Max)=0  
,@AEID varchar(Max)=0  
,@Region varchar(Max)=0  
,@E3Userid varchar(Max)=0  
,@PartnerCompanyid varchar(Max)=null  
--,@BrokerType varchar(Max)  
,@PasswordExpDate varchar(Max)=0  
,@chkRegisterloan varchar(Max)=0  
,@chkQuickPricer varchar(Max)=0  
,@chkpricelockloan varchar(Max)=0  
,@chkSubmitloan varchar(Max)=0  
,@chkSubmitcondition varchar(Max)=0  
,@chkorderappraisal varchar(Max)=0  
,@chkorderfhano varchar(Max)=0  
,@chkscheduleclosingdate varchar(Max)=0  
,@chkupdateloan varchar(Max)=0  
,@chkaccuratedocs varchar(Max)=0  
--,@OrgSystem varchar(Max)  
,@chkSubmitClosingPackage varchar(Max)=0  
--,@CreatedBy varchar(Max)  
--,@IsUserInNewProcess varchar(Max)  
,@chkOrderTaxTranscript varchar(Max)=0  
,@userid varchar(Max)=0  
,@isPublish  varchar(200)=0 
,@bitIsKatalyst bit =0  
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
UPDATE tblUsers  
   SET ufirstname = @ufirstname  
      ,ulastname = @ulastname  
      ,uemailid = @uemailid  
   ,StateBelongTo = @StateBelongTo  
      ,urole = @urole  
      ,isadmin = @isadmin  
      ,isactive = @isactive  
      ,uidprolender = @uidprolender  
      ,uparent = @uparent  
      ,upassword = @upassword  
      ,mobile_ph = @mobile_ph  
      ,phonenum = @phonenum        
   ,loginidprolender = @loginidprolender  
      ,carrierid = @carrierid  
      ,MI = @MI  
      ,companyname = @companyname  
      ,AEID = @AEID  
      ,Region = @Region  
      ,E3Userid = @E3Userid  
      ,PartnerCompanyid = @PartnerCompanyid  
      ,PasswordExpDate = @PasswordExpDate  
      ,chkRegisterloan = @chkRegisterloan  
      ,chkQuickPricer = @chkQuickPricer  
      ,chkpricelockloan = @chkpricelockloan  
      ,chkSubmitloan = @chkSubmitloan  
      ,chkSubmitcondition = @chkSubmitcondition  
      ,chkorderappraisal = @chkorderappraisal  
      ,chkorderfhano = @chkorderfhano  
      ,chkscheduleclosingdate = @chkscheduleclosingdate  
      ,chkupdateloan = @chkupdateloan  
   ,chkaccuratedocs=@chkaccuratedocs  
      ,chkSubmitClosingPackage = @chkSubmitClosingPackage  
      ,chkOrderTaxTranscript = @chkOrderTaxTranscript  
   ,ispublish=@isPublish  
,Iskatalyst=@bitIsKatalyst 
  Where userid =@userid  
END  
  
  
GO

