USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetBookDetailByID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetBookDetailByID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetBookDetailByID] 
	@Id int
AS
BEGIN
	select *  from BookDetail where  issueID=@Id
END

GO

