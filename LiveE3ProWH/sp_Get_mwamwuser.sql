USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_mwamwuser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_mwamwuser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/06/2012
-- Description:	Procedure for clspipelineE3 BLL
-- =============================================
CREATE procEDURE [dbo].[sp_Get_mwamwuser]
(
@loannumber bigint
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

select email as BDMEmail,  *from mwamwuser 
where Id  in(Select Originator_id  from  mwlloanapp 
where loannumber=@loannumber) ;
Select StringValue as ProcessorEmail  from  mwlCustomField
 where  CustFieldDef_ID='D040E2495A8F4D7E8A117A0BDE1889D7'  and 
LoanApp_id in (Select ID as LoanApp_id  from mwlloanapp where Loannumber=@loannumber) ;
SELECT Email as FileIntakeEmail FROM mwamwuser 
where id in (select Processor_ID from mwlloanapp where loannumber=@loannumber);
Select ID as LoanApp_id,ProcessorName,Processor_id,*  from mwlloanapp 
where loannumber=@loannumber 
END


GO

