USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpDeletePermanentUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpDeletePermanentUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpDeletePermanentUser] 
	@UserId int
AS
BEGIN
	Delete From tblusers Where userid =@UserId
END

GO

