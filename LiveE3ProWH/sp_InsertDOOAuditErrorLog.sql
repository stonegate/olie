USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertDOOAuditErrorLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertDOOAuditErrorLog]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Tavant>  
-- Create date: <16th Jan 2014>  
-- Modified By: <Tavant>  
-- Modified Date: <16th Jan 2014>  
-- Description: <This will be used to keep error log for DOO Audit Response>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_InsertDOOAuditErrorLog]
(  
 @LoanNumber varchar(10),  
 @ErrorMessage varchar(max), 
 @XMLMessage varchar(max), 
 @EndUserMsg varchar(50),
 @PartnerCompanyID varchar(100),
 @UserID varchar(200),
 @iDays Int
 )  
AS  
BEGIN  

 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 



Declare @NextSundayDate datetime
Declare @input datetime
set @input = getdate()

Set @NextSundayDate = (Select DATEADD([day], ((DATEDIFF([day], '19000107', @input) / 7) * 7) + 7, '19000107'))

Declare @Expirydate  Datetime

set @Expirydate = (select  @input + @iDays +(Select Count(*) from tblHoliDayList H where H.Status = 'Y' and HolidayType in('s','sf') and H.HolidayDate between CAST(Convert(varchar(10),@input ,121) as datetime) and 
getdate() and Convert(varchar(10),H.HolidayDate,121) <> @NextSundayDate) + (CASE WHEN @NextSundayDate between CAST(Convert(varchar(10),@input ,121) as datetime) and 
CAST(Convert(varchar(10),@input + @iDays,121) as datetime) THEN 1 ELSE 0 END))

INSERT INTO tbl_DooAuditLog 
            (LoanNumber, 
             ErrorMessage, 
             XMLMessage, 
             EndUserMessage, 
             PartnerCompanyID,
			 UserId,
			 ExpiryDate) 
VALUES      (@LoanNumber, 
             @ErrorMessage, 
             @XMLMessage, 
             @EndUserMsg, 
             @PartnerCompanyID,
			 @UserID,
			 @Expirydate
			 ) 
END 

-- Create Procedure Coding ENDS Here





GO

