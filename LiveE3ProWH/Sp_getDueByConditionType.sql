USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_getDueByConditionType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_getDueByConditionType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
      
    -- =============================================              
-- Author      :  <Stonegate>              
-- Create date :  <25/4/2013>              
-- Create by   :  <Shayamal Gajjar>    
-- Description :  <This stored procedure used for get condition type based on conditionid >    
-- Updated By : Shayamal Gajjar  
-----------------------------------------------
  
create PROCEDURE [dbo].[Sp_getDueByConditionType]  
 @condrecid varchar(50)  
  
AS     
    SET TRANSACTION isolation level READ uncommitted;     
    
  BEGIN     
              
      SELECT  DueBy as ConditionType    
      FROM   mwlCondition    
      WHERE  ID=@condrecid  
        
  END     
      
      
GO

