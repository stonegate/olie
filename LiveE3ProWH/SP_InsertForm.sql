USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_InsertForm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_InsertForm]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_InsertForm]
(
@Description varchar(200)= null,
@FileName varchar(200)= null,
@Isnews bit = null,
@CateID int = null,
@Formid int= null,
@Flag varchar(15)
)
AS
BEGIN
 if @Flag = 'Insert'
  Begin
		Insert into tblForms(Description,FileName,dttime,isnews,CateID)
		Values(@Description,@FileName,getdate(),@Isnews,@CateID)
end
else if @Flag = 'Update'
Begin
	  Update tblForms set Description= @Description, FileName = @FileName,
	   isnews = @Isnews, CateID = @CateID 
	  Where formID  = @Formid
End
else if @Flag = 'Delete'
 begin
     Delete from tblForms where FormID = @Formid
 end
select @@Identity
END




GO

