USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_update_UpdateTestimonial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_update_UpdateTestimonial]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--[sp_update_UpdateTestimonial] 'zzzz','zzzz','10/12/2012','50','z','zzzz','10/23/2012'     
     
-- =============================================        
-- Author:  Gourav        
-- Create date:         
-- Description: store procedure for clsuser.cs        
-- =============================================        
CREATE proc [dbo].[sp_update_UpdateTestimonial]        
(        
@uname varchar(Max)=null        
,@Testimonial varchar(Max)=null        
,@SubmittedDate varchar(Max)=null              
,@TestimonialID varchar(Max)=null        
,@City varchar(Max)=null        
,@State varchar(Max)=null        
,@UpdatedDt varchar(Max)=null            
)        
as        
begin        
if(@SubmittedDate!='')        
begin        
  UPDATE tblTestimonials         
  SET         
   UName=@uname        
   ,Testimonial=@Testimonial        
   ,SubmittedDate=convert(Datetime, @SubmittedDate  )      
   ,UpdatedDt=   @UpdatedDt        
   ,City=@City        
   ,State=@State        
   where TestimonialID=@TestimonialID        
 end        
 else        
 begin        
 UPDATE tblTestimonials         
  SET         
   UName=@uname        
   ,Testimonial=@Testimonial        
   ,UpdatedDt=convert(Datetime, @UpdatedDt )      
   ,City=@City        
   ,State=@State        
   where TestimonialID=@TestimonialID        
 end        
end 

GO

