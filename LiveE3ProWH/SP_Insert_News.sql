USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Insert_News]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Insert_News]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROC [dbo].[SP_Insert_News](      
@Title VARCHAR(1000),      
@Body ntext,      
@IsActive bit,      
@IsNewsAnnouncement bit,      
@CreatedBy int,      
@CreatedDate datetime,      
@Role VARCHAR(150),      
@NewsAnnousVisbleAd bit,      
@NewsAnnousVisbleDR bit,       
@NewsAnnousVisbleBR bit,      
@NewsAnnousVisbleLO bit,      
@NewsAnnousVisbleCU bit,      
@NewsAnnousVisbleRE bit,      
@NewsAnnousVisbleRM bit,        
@NewsAnnousVisbleDP bit,     
@NewsAnnousVisbleCP bit,     
@NewsAnnousVisbleHY bit,  
@NewsAnnousVisibleDCFI bit,  
@NewsAnnousVisibleSCFI bit  
  
)      
AS      
BEGIN      
 INSERT INTO tblNewsAndAnnouncement (Title,Body,IsActive,IsNewsAnnouncement,CreatedBy,CreatedDate,Role,NewsAnnousVisbleAd,NewsAnnousVisbleDR,NewsAnnousVisbleBR,NewsAnnousVisbleLO,NewsAnnousVisbleCU,NewsAnnousVisbleRE,NewsAnnousVisbleRM,NewsAnnousVisbleDP,
  
NewsAnnousVisbleCP,NewsAnnousVisbleHY,NewsAnnousVisbleDCFI,NewsAnnousVisibleSCFI)      
 VALUES      
  (@Title,@Body,@IsActive,@IsNewsAnnouncement,@CreatedBy,getdate(),@Role,@NewsAnnousVisbleAd,@NewsAnnousVisbleDR,@NewsAnnousVisbleBR,@NewsAnnousVisbleLO,@NewsAnnousVisbleCU,@NewsAnnousVisbleRE,@NewsAnnousVisbleRM,@NewsAnnousVisbleDP,@NewsAnnousVisbleCP  
,@NewsAnnousVisbleHY,@NewsAnnousVisibleDCFI,@NewsAnnousVisibleSCFI)      
  
    
END  
GO

