USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanNoFronAppID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanNoFronAppID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Kanva>    
-- Create date: <2nd Jan 2013>    
-- Description: <This will get Loan number from mwlloanapp table for given loanappid>    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_GetLoanNoFronAppID]    
(    
 @LoanAppID varchar (200)     
)    
AS    
BEGIN    

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

 select     
  *     
 From     
  mwlloanapp     
 Where     
  ID = @LoanAppID    
END
GO

