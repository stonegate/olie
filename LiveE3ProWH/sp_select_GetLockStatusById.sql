USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_select_GetLockStatusById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_select_GetLockStatusById]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Name  
-- Create date:   
-- Description:   
-- =============================================  
CREATE PROCEDURE [dbo].[sp_select_GetLockStatusById]
(  
@lockId varchar(Max)
)  
AS  
BEGIN 
select id_value ,descriptn from lookups where lookup_id='AAT' and id_value=@lockId
END  

GO

