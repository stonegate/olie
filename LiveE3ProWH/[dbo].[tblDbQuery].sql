USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDbQuery]') AND type in (N'U'))
DROP TABLE [dbo].[tblDbQuery]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDbQuery](	  [Queryid] INT NOT NULL IDENTITY(1,1)	, [MethodName] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [Query] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [DB] VARCHAR(2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, CONSTRAINT [PK_tblDbQuery] PRIMARY KEY ([Queryid] ASC))USE [HomeLendingExperts]
GO

