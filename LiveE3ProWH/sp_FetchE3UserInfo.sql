USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchE3UserInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchE3UserInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_FetchE3UserInfo] 
	-- Add the parameters for the stored procedure here
@Loginid varchar(50)
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
    -- Insert statements for procedure here


	begin
	select * from mwaMWUser where ID= @Loginid
	end
END
GO

