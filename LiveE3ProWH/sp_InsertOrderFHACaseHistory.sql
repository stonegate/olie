USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertOrderFHACaseHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertOrderFHACaseHistory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nitin>
-- Create date: <>
-- Description:	<Procedure for clsOrderFHAE3>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertOrderFHACaseHistory]
(
@TransId varchar(50),
@LoanNumber varchar(32),
@Status varchar(20),
@SubmittedDate datetime
)
AS
BEGIN
	
	SET NOCOUNT ON;
Insert into tblOrderFHACase_History(LoanNumber,TransId,Status,SubmittedDate) 
Values (@LoanNumber,@TransId,@Status,@SubmittedDate)


END


GO

