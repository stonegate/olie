USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanAcqReportForOPFileIntake_E3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanAcqReportForOPFileIntake_E3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Vipul Thakkar    
-- Create date: 03, OCT 2012    
-- Description: This will give Loan Information for Director of operation File intake tab  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_GetLoanAcqReportForOPFileIntake_E3] 
	-- Add the parameters for the stored procedure here
@strCorrespondentCustField varchar(50)=NULL,
@strConditionType varchar(100)=null,
@strApplicationrecStatus varchar(100)=NULL,
@strCurrentStatus varchar(100)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
    -- Insert statements for procedure here


	BEGIN
   select 'E3' as DB,     
                        BOffices.Office as Boffice,loanapp.CurrDecStatusDate as Decisionstatusdate,loanapp.CurrPipeStatusDate as CurrentStatusDate ,loanapp.ChannelType as ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID as record_id,'' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS appDate,Cust.stringvalue as UnderwriterType,    
                        CASE TransType when NULL THEN '' WHEN 'P' THEN 'Purchase money first mortgage' WHEN 'R' THEN 'Refinance' WHEN '2' THEN 'Purchase money second mortgage' WHEN 'S' THEN 'Second mortgage, any other purpose' WHEN 'A' THEN 'Assumption' WHEN 'HOP' THEN 'HELOC - other purpose' WHEN 'HP' THEN 'HELOC - purchase' ELSE '' END AS TransType,    
                        case LoanData.FinancingType WHEN 'F' THEN 'FHA' WHEN 'C' THEN 'CONVENTIONAL' WHEN 'V' THEN  'VA' End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,    
                        LockRecord.LockExpirationDate,UnderwriterName as UnderWriter,Broker.Company as BrokerName,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,Loandata.LoanProgDesc as ProgramDesc ,    
                        CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy='Prior to Closing' and Category !='LENDER' and Category !=@strConditionType and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0    
                         WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER' and Category !=@strConditionType)  and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived')    
                        and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE (SELECT count(a.ID) as TotalCleared     
                        FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER' and Category !=@strConditionType) and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived') and a.objOwner_ID IN     
                        (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy='Prior to Closing' and Category !='LENDER' and Category !=@strConditionType) and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER     
                        ,BOffices.Office as OFFICE_NAME1     
                        from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and borr.Sequencenum=1 Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id      
                        left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id     
                        Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = 'BROKER' and Broker.objownerName='Broker'     
                        left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id  and  LockRecord.LockType='LOCK' and  LockRecord.Status<>'CANCELED'     
                        Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc in (@strApplicationrecStatus)     
                        Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = loanapp.id and COffices.InstitutionType = 'CORRESPOND' and COffices.objownerName='Contacts'     
                        Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.InstitutionType = 'Branch' and BOffices.objownerName='BranchInstitution'     
                        left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID = '" + strCorrespondentCustField + "'     
                        where CurrentStatus  IN (@strCurrentStatus)    
                        and isnull(Cust.stringvalue,'')<>'Delegated'     

                        and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''     
	
	END
	
	
END



GO

