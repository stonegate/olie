USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRegLoanHistory]') AND type in (N'U'))
DROP TABLE [dbo].[tblRegLoanHistory]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRegLoanHistory](	  [id] INT NOT NULL IDENTITY(1,1)	, [userid] INT NULL	, [xmlfilename] XML NULL	, [createddate] DATETIME NULL	, CONSTRAINT [PK_tblRegLoanHistory] PRIMARY KEY ([id] ASC))USE [HomeLendingExperts]
GO

