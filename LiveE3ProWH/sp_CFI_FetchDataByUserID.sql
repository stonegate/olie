USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CFI_FetchDataByUserID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CFI_FetchDataByUserID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ahishek
-- Create date: 10/18/2012
-- Description:	
-- =============================================
CREATE procedure [dbo].[sp_CFI_FetchDataByUserID]
(
@userid varchar(Max)
)
as
begin
select * from tblusers where userid =@userid
end



GO

