USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetClosingMgrAgedReportView]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetClosingMgrAgedReportView]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================
-- Author:		<Stonegate>
-- Create date: <10/8/2012>
-- Description:	<This will get Get Closing Manager Aged ReportView>
-- =================================================================
CREATE PROCEDURE [dbo].[sp_GetClosingMgrAgedReportView]
(
	@strCorrespondentCustField varchar(200),
	@strFrom varchar(50)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

Declare @strOrderby varchar(500)
Set @strOrderby = ''

Declare @SQL nvarchar(max)
set @SQL=''
Set @SQL = 'Select ''E3'' as DB,loanapp.processorname,loanapp.DocumentPreparerName,loanapp.CurrDecStatusDate,CurrPipeStatusDate,
LockRecord.LockDateTime,Offices.Office as office,COffices.Office as coffice,loanapp.CurrDecStatusDate as Decisionstatusdate,
loanapp.CurrPipeStatusDate as CurrentStatusDate ,loanapp.ChannelType as ChannelType,loanapp.DecisionStatus,OriginatorName,
loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,
loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS StatusDate,appReceived.StatusDateTime as AppDate,
CASE TransType when NULL THEN '''' WHEN ''P'' THEN ''Purchase money first mortgage'' WHEN ''R'' THEN ''Refinance'' WHEN ''2'' THEN ''Purchase money second mortgage'' WHEN ''S'' THEN ''Second mortgage, any other purpose'' WHEN ''A'' THEN ''Assumption'' WHEN ''HOP'' THEN ''HELOC - other purpose'' WHEN ''HP'' THEN ''HELOC - purchase'' ELSE '''' END AS TransType,
Case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, 
convert(varchar(35),EstCloseDate,101) AS ScheduleDate, appStat.StatusDesc,
LockRecord.LockExpirationDate,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(COffices.Company) ELSE RTRIM(Broker.Company) END as BrokerName,
CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,Loandata.LoanProgDesc as ProgramDesc ,Cust.stringvalue as UnderwriterType,
CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'' 
and a.objOwner_ID IN (SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0
WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'')
and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0 ELSE (SELECT count(a.ID) as TotalCleared
FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') 
and a.objOwner_ID IN(SELECT ID FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') 
and a.objOwner_ID IN (SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER,
BOffices.Office as OFFICE_NAME1 from mwlLoanApp loanapp Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and Sequencenum=1 
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id
And Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker'' left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id  and  LockRecord.LockType=''LOCK'' 
and LockRecord.Status<>''CANCELED''
Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = loanapp.id and COffices.InstitutionType = ''CORRESPOND'' 
and COffices.objownerName=''Contacts''
Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.InstitutionType = ''BRANCH'' and BOffices.objownerName=''BranchInstitution''
Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id
Left join dbo.mwlInstitution as Offices on Offices.ObjOwner_id = loanapp.id and Offices.InstitutionType = ''Broker'' and Offices.objownerName=''Broker''
Left join mwlAppStatus appReceived on loanapp.ID=appReceived.LoanApp_id and appReceived.StatusDesc IN (''09 - Application Received'')
left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID = '''+ @strCorrespondentCustField +''' '

if (@strFrom = 'clonhold')
Begin
	set @SQL = @SQL + ' where CurrentStatus  IN (''43 - Closing on Hold'') '
End
else if (@strFrom = 'aged_ClToClose')
Begin
	set @SQL = @SQL + ' Where loanapp.CurrentStatus IN (''33 - Cleared to Close'') '
    set @SQL = @SQL + ' and (datediff(day,loanapp.CurrPipeStatusDate,getdate())>=15) '
    Set @strOrderby = ' Order by loanapp.CurrPipeStatusDate Asc'
End
else if (@strFrom = 'aged_InClosing')
Begin
	set @SQL = @SQL + ' Where loanapp.CurrentStatus IN (''In Closing'') '
    set @SQL = @SQL + ' and (datediff(day,loanapp.CurrPipeStatusDate,getdate())>=15) '
    set @strOrderby = ' Order by loanapp.CurrPipeStatusDate Asc'
End
else if (@strFrom = 'aged_ClosingOnHold')
Begin
	set @SQL = @SQL + ' Where loanapp.CurrentStatus IN (''43 - Closing on Hold'') '
    set @SQL = @SQL + ' and (datediff(day,loanapp.CurrPipeStatusDate,getdate())>=5) '
	set @strOrderby = ' Order by loanapp.CurrPipeStatusDate Asc'
End
else if (@strFrom = 'aged_DocsOut')
Begin
	set @SQL = @SQL + ' Where loanapp.CurrentStatus IN (''45 - Docs Out'') '
    set @SQL = @SQL + ' and (datediff(day,loanapp.CurrPipeStatusDate,getdate())>=10) '
    set @strOrderby = ' Order by loanapp.CurrPipeStatusDate Asc'
End
set @SQL = @SQL + ' and loanapp.ChannelType in(''BROKER'',''Retail'') '
set @SQL = @SQL + ' and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' '

if (@strOrderby = '')
Begin
	set @SQL = @SQL + ' ORDER BY PER desc '
End
else
Begin
	Set @SQL = @SQL + ' '+@strOrderby+' '
End

Print @SQL
exec sp_executesql @SQL 

END

GO

