USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InserttblUserPasswordHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InserttblUserPasswordHistory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE procEDURE [dbo].[sp_InserttblUserPasswordHistory]
(
@UserID int,
@Password varchar(200)
)
AS
BEGIN
	
	SET NOCOUNT ON;

INSERT INTO tblUserPasswordHistory(UserID,Password) 
Values(@UserID,@Password)
END



GO

