USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAccActivationStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAccActivationStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Stonegate
-- Create date: 10/05/2012
-- Description:	Get Activation Status
-- =============================================
create proc [dbo].[sp_GetAccActivationStatus]

as
begin
select top 1 ustatus from tblAccActivation order by id desc
end

GO

