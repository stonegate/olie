USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_FNMLookUpData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Get_FNMLookUpData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Tavant  
-- Create date: 01/30/2014  
-- Description: Procedure to get all data from tblFNMLookup    
-- =============================================  

create PROCEDURE [dbo].[Get_FNMLookUpData]
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SELECT * FROM [dbo].[tblFNMLookup] WHERE IsActive=1
END


GO

