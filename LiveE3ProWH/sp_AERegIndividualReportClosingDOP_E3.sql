USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AERegIndividualReportClosingDOP_E3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_AERegIndividualReportClosingDOP_E3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vipul>
-- Create date: <10/4/2012>
-- Description:	<This wil return Loan data reports for DO production>
-- =============================================
CREATE PROCEDURE [dbo].[sp_AERegIndividualReportClosingDOP_E3]
(
	@strRoleID VARCHAR(10),
	@strOriginatorID varchar(2000)=NULL,
	@strChannel varchar(100)=NULL,
	@strCurrentStatus VARCHAR(4000)=NULL,
	@strMultiplePuid varchar(2000)=NULL
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

Declare @SQL varchar(8000)
Set @SQL =''
Set @SQL = 'select ''E3'' as DB,loanapp.DecisionStatus,loanapp.Channeltype as BusType,
 loanapp.ID as record_id,LoanNumber as loan_no,borr.lastname as BorrowerLastName,loanapp.DecisionStatus,loanapp.CurrentStatus as LoanStatus,DateAppSigned as SubmittedDate,LoanProgramName as LoanProgram,  
                      case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase''
                       when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,
                       borr.CompositeCreditScore as CreditScoreUsed,loanData.AdjustedNoteAmt as LoanAmount,  
                      convert(varchar(35),EstCloseDate,101) AS ScheduleDate,LockStatus,LockExpirationDate,
                      UnderwriterName as UnderWriter,CloserName as Closer,CASE ISNULL(RTRIM(Institute.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Institute.Company) END as BrokerName, Institute.CustomDataOne as TitleCompany,  
                      AppStatus.StatusDateTime as DocsSentDate,FundedStatus.StatusDateTime as FundedDate,'''' as LU_FINAL_HUD_STATUS   
                      from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id   
                       left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  Left join dbo.mwlInstitution as Institute on Institute.ObjOwner_id = loanapp.id and   
                      InstitutionType = ''BROKER'' and objownerName=''BROKER'' Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and AppStatus.StatusDesc =''45 - Docs OUT''
                      Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''  
                      Left join mwlappstatus as FundedStatus on FundedStatus.LoanApp_id=loanapp.id  and FundedStatus.StatusDesc = ''62 - Funded''   
                      where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <>''''  and   
                     CurrentStatus IN (select * from dbo.SplitString('+@strCurrentStatus+','',''))'
                     
                     IF(@strChannel!='retail')
                     BEGIN
                     	Set @SQL = @SQL + ' and Originator_id in ('+ @strMultiplePuid +') '
                     	
                     END
                     IF(@strChannel ='retail')
						BEGIN
						
                     	Set @SQL = @SQL + ' and loanapp.ChannelType in (''RETAIL'')'
                    
                     	IF(@strRoleID = '12')
                    
                     		Set @SQL = @SQL + ' and Originator_id in ('+ @strOriginatorID +')'
                    
                    end 
                    
                    Set @SQL = @SQL + ' order by CurrentStatus '	 
                     	
                    
END



GO

