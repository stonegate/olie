USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetGridDatatblCFIforms]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetGridDatatblCFIforms]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SP_GetGridDatatblCFIforms  
	-- Add the parameters for the stored procedure here
	(
	@iURole int
	)
AS
BEGIN
	if(@iURole = 0)
     Begin
       
       select  dbo.GetFormRole(fo.formid)as Roleid,* from tblCFIforms FO left outer join tblCategories CAT on FO.Cateid=CAT.catid order by dttime desc       
     end
    else if (@iURole = 100)
     begin


select dbo.GetFormRole(FO.formid)as Roleid, formID,Description,FileName,dttime,isnews,CateName from tblCFIforms FO left outer join tblCategories CAT on FO.Cateid=CAT.catid where formid in (select formid from tblformroles where uroleid =@iURole ) ORDER BY description asc       
		
        
     end
    else if (@iURole = 10 or @iURole = 11)
     begin


select dbo.GetFormRole(FO.formid)as Roleid, formID,Description,FileName,dttime,isnews,CateName from tblCFIforms FO left outer join tblCategories CAT on FO.Cateid=CAT.catid where formid in (select formid from tblformroles where uroleid = @iURole OR uroleid = 101 )  ORDER BY description asc       


     end
    else
      begin
       

select dbo.GetFormRole(FO.formid)as Roleid, formID,Description,FileName,dttime,isnews,CateName from tblCFIforms FO left outer join tblCategories CAT on FO.Cateid=CAT.catid where formid in (select formid from tblformroles where (uroleid = @iURole OR uroleid = 101)) ORDER BY description asc       
       
        
       
      end
END

GO

