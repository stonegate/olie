USE [HomeLendingExperts]
								GO
								SET ANSI_NULLS ON
								GO
								SET QUOTED_IDENTIFIER ON
								GO-- =============================================
-- Author:		Vipul Thakkar
-- Create date: 04 OCT 10
-- Description:	To get the Broker name
-- =============================================
CREATE FUNCTION [dbo].[GetBrokerName] 
(
	@LoanNo varchar(50)
)
RETURNS varchar(50)
AS
BEGIN
DECLARE @BrokerInfo VARCHAR(1000) 

--SELECT @BrokerName = COALESCE(@BrokerName,'') + ufirstname + ', '  
--FROM 
--(
set @BrokerInfo = (select top 1 ufirstname + '|' + uemailid
from tblLoanReg 
inner join tblusers on tblLoanReg.userid = tblusers.userid
where loannumber = @LoanNo)
--) as temp
--SELECT @BrokerName AS BrokerNames 
return @BrokerInfo
END

--select loannumber, *from tblLoanReg
--
--alter table tblLoanReg
--add loannumber varchar(50) null
GO

