USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_Select_tblManageManadatoryClientsInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_Select_tblManageManadatoryClientsInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Example for execute stored procedure      
--EXECUTE [udsp_Select_tblManageManadatoryClientsInfo] 
-- =============================================      
-- Author:  Karan Gulati      
-- Create date: 16-04-2013  
-- Name : udsp_Select_tblManageManadatoryClientsInfo    
-- Description: Fetch details of ManageClients for Mandatory Delivery Task      
-- =============================================      
CREATE PROCEDURE [dbo].[udsp_Select_tblManageManadatoryClientsInfo]      
 -- Add the parameters for the stored procedure here       
AS      
BEGIN      
     
 SET NOCOUNT ON;    
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
 SET XACT_ABORT ON;     
     
  -- Select statements for procedure here        
SELECT     MandatoryClient.ID, MandatoryClient.PartnerCompanyId, MandatoryClient.ClientCompanyName, MandatoryClient.NetWorth, MandatoryClient.MandatoryPercentage, 
                      MandatoryClient.TotalMandatoryLimit, MandatoryClient.TotalMandatoryLimit - ISNULL
                          ((SELECT     SUM(dbo.mwlLockRecord.AdjustedNoteAmt) AS 'AdjustedNoteAmt'
                              FROM         mwlLoanApp INNER JOIN
                                                    mwlInstitution ON dbo.mwlInstitution.objowner_id = dbo.mwlLoanApp.ID INNER JOIN
                                                    mwlLockRecord ON dbo.mwlLockRecord.LoanApp_id = dbo.mwlLoanApp.ID
                              WHERE     (dbo.mwlInstitution.companyemail = MandatoryClient.PartnerCompanyId) AND (dbo.mwlLockRecord.deliveryoption = 'Mandatory') AND (dbo.mwlLockRecord.LockType = 'LOCK') 
                                                    AND (CONVERT(VARCHAR, dbo.mwlLoanApp.CurrentStatus, 2) < '62')), 0) AS 'MandatoryLimitBal', MandatoryClient.IsActive
FROM         tblManageMandatoryClientsInfo AS MandatoryClient     
   
END 
GO

