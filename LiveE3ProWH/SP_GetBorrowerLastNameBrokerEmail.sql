USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetBorrowerLastNameBrokerEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetBorrowerLastNameBrokerEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

Create PROCEDURE [dbo].[SP_GetBorrowerLastNameBrokerEmail]
	(
@loan_no char(15)
)
AS
BEGIN
select cb.borr_last,user_email from loandat LD
 inner join cobo Cb on ld.record_id = cb.parent_id
 left outer join users on ld.lt_usr_underwriter = users.record_id 
 where ld.loan_no =@loan_no
             
END

GO

