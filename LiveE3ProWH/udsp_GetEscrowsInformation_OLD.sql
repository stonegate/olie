USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetEscrowsInformation_OLD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetEscrowsInformation_OLD]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================     
-- Author:<Gourav Goel>         
-- Create date: <09/11/2012>     
-- Description:  < This will return the value for "Docs On OLIE Step-5 Escrows Information" on the basis of LoanNumber>     
-- =============================================     
CREATE PROCEDURE [dbo].[udsp_GetEscrowsInformation_OLD] --'0000313224'      
  -- Add the parameters for the stored procedure here     
  @LoanNumber VARCHAR(15)   
AS   
  BEGIN   
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
      -- SET NOCOUNT ON added to prevent extra result sets from     
      -- interfering with SELECT statements.     
      SET nocount ON;   
  
    SELECT mwlinstitution.company,   
             mwlinstitution.street,   
             mwlinstitution.street2,   
             mwlinstitution.city,   
             mwlinstitution.state,   
             mwlinstitution.zipcode,   
             mwlinstitution.country,   
             mwlinstitution.companyemail,   
             Hzdphonenumber.phonenumber,   
             HzdFaxnumber.phonenumber              AS FaxNumber,   
             mwlhazard.policynumber,   
             mwlhazard.hazardinscoverage,   
             prepaid.annualpayduedate,   
             CityTaxes.includeinpayment   AS CityIncludeInPayment,   
             FloodInsuranceinPayment.includeinpayment   AS FloodIncludeInPayment,   
             CountryTaxes.includeinpayment AS CountryIncludePayment,   
             hzdinsurace.includeinpayment          AS HazardIncludeInPayment,   
             AssementAnnualAmt.includeinpayment          AS AssessmentIncludeInPayment   
             ,   
             Mortgage.includeinpayment             AS MortgageIncludeinPayment,   
             custfield.CurrencyValue                                   AS currencyvalue,   
             --Convert(varchar(50),mwlcustomfield.currencyvalue) as CurrencyValue,     
             --  mwlaggregateescrow.citytaxesmonthlyamt,     
             -- mwlaggregateescrow.countytaxesmonthlyamt,    
             --,mwlAggregateEscrow.HazardInsMonthlyAmt     
             CityTaxes.annualamt                   AS CityAnnualAmount,   
             FloodInsurance.annualamt             AS FloodAnnualAmount,   
             CityTaxes.pmtnumber                   AS CityPmtNumber,   
             CityTaxes.pmtnumber                   AS FloodPmtNumber,   
             CountryTaxes.annualamt                AS CoutryAnnualAmount,   
             CountryTaxes.pmtnumber                AS CountryPmtNumber,   
             hzdinsuraceTaxes.annualamt            AS HazardAnnualAmount,   
             hzdinsuraceTaxes.pmtnumber            AS HazardPmtNumber,   
            AssementAnnualAmt. annualamt           AS AssessmentAnnualAMount,  
   MortgageTaxes.annualamt                AS MortgageAnnualAMount,  
   MortgageTaxes.pmtnumber                As MortgagepmtNumber,   
             mwlppe.citytaxesallocevery,   
             mwlppe.citytaxesstartingin,   
             mwlppe.FloodinsAllocEvery            AS FloodTaxesAllocEvery,   
             mwlppe.FloodinsStartingIn            AS FloodTaxesStartingin,   
             mwlppe.countytaxallocevery,   
             mwlppe.countytaxstartingin,   
             mwlppe.assessallocevery,   
             mwlppe.assessstartingin,   
             mwlppe.hazardinsallocevery,   
             mwlppe.hazardinsstartingin   
             --last check boxes   
             --,mwlLoanData.waiveescrows   
             --,mwlLoanData.waivefundingfee   
             ,0 as waiveescrows   
             ,0 as waivefundingfee   
             ,mwlppe.escrowinclallins,   
             mwlppe.escrowinclalltaxes   
      FROM   mwlloanapp   
             INNER JOIN dbo.mwlloandata loandata   
                     ON dbo.mwlloanapp.id = loandata.objowner_id   
             
             LEFT JOIN mwlsubjectproperty   
                    ON mwlsubjectproperty.loanapp_id = mwlloanapp.id   
             ----o Hazard Policy No,Premium Amount     
             LEFT JOIN mwlhazard   
                    ON mwlhazard.owner_id = mwlsubjectproperty.id   
             --end     
               --Hazard information like compayname,street,street2 etc--     
             LEFT JOIN mwlinstitution   
                    ON mwlinstitution.objowner_id = mwlhazard.id   
                       AND objownername = 'HazardInstitution'   
             --end     
             ---o Hazard Policy Effective Date     
             LEFT JOIN mwlprepaid prepaid   
                    ON prepaid.loandata_id = loandata.id   
                       AND prepaid.hudlinenum = '903'   
             ---o Hazard Company Phone     
             LEFT JOIN mwlphonenumber Hzdphonenumber   
                    ON mwlinstitution.id = Hzdphonenumber.objowner_id   
                       AND Hzdphonenumber.phonetype = 'Business'   
             ---     
             ---o Hazard Company Fax:     
             LEFT JOIN mwlphonenumber HzdFaxnumber   
                    ON mwlinstitution.id = HzdFaxnumber.objowner_id   
                       AND HzdFaxnumber.phonetype = 'Fax'   
            ----o Hazard Insurance Deductible     
             LEFT JOIN mwlcustomfield custfield   
                    ON custfield.loanapp_id = dbo.mwlloanapp.id   
                       AND custfield.custfielddef_id = '9C4868FA2B50437AAE04662279835028'  
                       --Live  
                           --9C4868FA2B50437AAE04662279835028  
                           --Test  
                           --B263B7B2224B4BBC84591360F16A9ACB  
             --AnnualAmt,Escrow information     
              LEFT JOIN mwlprepaid propertytax   
                    ON propertytax.loandata_id = loandata.id   
                       AND propertytax.hudlinenum = '1004' AND propertytax.hudDesc='Property taxes'  
                         --City taxes and county taxes Due every mons     
             LEFT JOIN mwlhudlinecomponent CityTaxes   
                    ON CityTaxes.objowner_id = propertytax.id   
                       AND CityTaxes.description = 'IQ - City taxes'   
                LEFT JOIN mwlhudlinecomponent CountryTaxes   
                    ON CountryTaxes.objowner_id = propertytax.id   
                       AND CountryTaxes.description = 'IQ - County taxes'   
             LEFT JOIN mwlprepaid hzdinsuraceTaxes   
                    ON hzdinsuraceTaxes.loandata_id = loandata.id   
                       AND hzdinsuraceTaxes.huddesc =   
                           'Hazard insurance reserves'   
   LEFT JOIN mwlprepaid MortgageTaxes   
                    ON MortgageTaxes.loandata_id = loandata.id   
                       AND MortgageTaxes.huddesc =   
                           'Mortgage Insurance Premium'   
             --City taxes and county taxes Due every mons     
             LEFT JOIN mwlppe   
                    ON dbo.mwlppe.loandata_id = loandata.id   
             LEFT JOIN mwlprepaid CountyTaxesinPayment   
                    ON CountyTaxesinPayment.loandata_id = loandata.id   
                       AND CountyTaxesinPayment.hudlinenum = '1004'   
             LEFT JOIN mwlprepaid cityTaxesinPayment   
                    ON cityTaxesinPayment.loandata_id = loandata.id   
                       AND cityTaxesinPayment.hudlinenum = '1004'   
             LEFT JOIN mwlprepaid hzdinsurace   
                    ON hzdinsurace.loandata_id = loandata.id   
                       AND hzdinsurace.hudlinenum = '1002'   
             --mortgage insurance   
             LEFT JOIN mwlprepaid Mortgage   
                    ON Mortgage.loandata_id = loandata.id   
                       AND Mortgage.hudlinenum = '902'   
                         
                         LEFT JOIN mwlprepaid AssementAnnualAmt   
                    ON AssementAnnualAmt.loandata_id = loandata.id   
                       AND AssementAnnualAmt.hudlinenum = '1005'   
                         
                         
                       LEFT JOIN mwlprepaid FloodInsurance   
                    ON FloodInsurance.loandata_id = loandata.id   
                                        
    and FloodInsurance.HUDDesc = 'Flood insurance premium'   
                    AND FloodInsurance.hudlinenum='904'  
                     LEFT JOIN mwlprepaid FloodInsuranceinPayment   
                    ON FloodInsuranceinPayment.loandata_id = loandata.id   
                       AND FloodInsuranceinPayment.hudlinenum = '1006'   
        
      WHERE loannumber=@LoanNumber  
  END 
GO

