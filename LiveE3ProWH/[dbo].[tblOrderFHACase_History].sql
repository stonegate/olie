USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblOrderFHACase_History]') AND type in (N'U'))
DROP TABLE [dbo].[tblOrderFHACase_History]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblOrderFHACase_History](	  [Id] INT NOT NULL IDENTITY(1,1)	, [LoanNumber] VARCHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [TransId] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Status] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SubmittedDate] DATETIME NULL	, CONSTRAINT [PK_tblOrderFHACase_History] PRIMARY KEY ([Id] ASC))USE [HomeLendingExperts]
GO

