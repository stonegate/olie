USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanHistory]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanHistory]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanHistory](	  [Id] INT NOT NULL IDENTITY(1,1)	, [LoanNumber] VARCHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LoanId] VARCHAR(32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [UserId] INT NULL	, CONSTRAINT [PK_tblLoanHistory] PRIMARY KEY ([Id] ASC))USE [HomeLendingExperts]
GO

