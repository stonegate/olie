USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetTitleComp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetTitleComp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Nitin  
-- Create date: 10/20/2012  
-- Description: Procedure for Get Title Comp  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_GetTitleComp]  
(  
@LoanNumber Varchar(15)  
)  
  
AS  
BEGIN  

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
   
 SET NOCOUNT ON;  
 
 DECLARE @count AS INT

 Select @count = COUNT(*) From mwlsubjectproperty Where loanapp_id = (Select id From mwlloanapp Where loannumber = @LoanNumber) 
														AND state in ('AK','AZ','CA','ID','NV','NM','OR','UT','WA')

if(@count > 0)
	BEGIN
	select *from mwlInstitution where InstitutionType ='SETTLEMT' AND ObjOwnerName='SetAgent' and ObjOwner_id in(Select id from MwlLoanapp where LoanNumber=@LoanNumber)  
	END
ELSE
	BEGIN
	select *from mwlInstitution where InstitutionType ='Title' AND ObjOwnerName='Contacts' and ObjOwner_id in(Select id from MwlLoanapp where LoanNumber=@LoanNumber)  
	END

	select @count AS DryStateCount
END



GO

