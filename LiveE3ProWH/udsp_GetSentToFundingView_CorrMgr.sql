USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetSentToFundingView_CorrMgr]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetSentToFundingView_CorrMgr]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




--Example for execute stored procedure       
--EXECUTE [udsp_GetSentToFundingView_CorrMgr]  '9830','9739','9738','9664','9662','01 - Registered','05 - Application Taken', '03 - Appt Set to Review Disclosures', '04 - Disclosures Sent', '07 - Disclosures Received' ,'09 - Application Received','09 - Application Recieved','13 - File Intake','17 - Submission On Hold','19 - Disclosures','25 - Submitted to Underwriting','29 - In Underwriting','33 - Cleared to Close','37 - Pre-Closing','41 - In Closing','43 - Closing on Hold','45 - Docs Out','50 - Closing Package Received','50 - Closing Package Recieved','54 - Purchase Pending','57 - Sent to Funding','20 - Disclosures On Hold','21 - In Processing','52 - Closing Package Reviewed' ,'53 - Closed Package Reviewed' ,'56 - Purch Conditions Received','57 - Sent to Funding','54 - Purchase Pending'
-- =============================================      
-- Author:  Amit Saini      
-- Create date: 06/03/2013   
-- Name:udsp_GetSentToFundingView_CorrMgr.sql
-- Description: Get SentToFundingView for Correspondent Manager. 
-- Custom Fields:
 -- Correspondent Type : 9830
 -- 100% PTP Conditions Rcvd Date: 9739  
 -- 100% PTP Conditions Rcvd Time : 9738
 -- Sent to Funding Date: 9664
 -- Funding Specialist: 9662     
-- =============================================      
CREATE procedure [dbo].[udsp_GetSentToFundingView_CorrMgr]
(
 --Parameter declaration 
@CustomField varchar(10),
@SubmittedDateID varchar(10),
@SubmittedDateTimeID varchar(10),
@CustSendToFundingDate varchar(10),
@CustSendToFundingName varchar(10),
@str1 varchar(max)
)
AS  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
SET XACT_ABORT ON;  
BEGIN

 DECLARE @CustomFieldID varchar(50),         
         @SubmittedDate varchar(50),
         @SubmittedDateTime varchar(50),
         @CustSendToFundingDateID varchar(50),
         @CustSendToFundingNameID varchar(50)       
        
     
  SELECT @CustomFieldID = ID from mwscustfielddef where MWFieldNum = @CustomField -- Custom Field Name "Correspondent Type", Custom Field Number "9830"
  SELECT @SubmittedDate = ID from mwscustfielddef where MWFieldNum = @SubmittedDateID -- Custom Field Name "100% PTP Conditions Rcvd Date", Custom Field Number "9739"
  SELECT @SubmittedDateTime = ID from mwscustfielddef where MWFieldNum = @SubmittedDateTimeID -- Custom Field Name "100% PTP Conditions Rcvd Time", Custom Field Number "9738"  
  SELECT @CustSendToFundingDateID = ID from mwscustfielddef where MWFieldNum = @CustSendToFundingDate -- Custom Field Name "Sent to Funding Date", Custom Field Number "9664"   
  SELECT @CustSendToFundingNameID = ID from mwscustfielddef where MWFieldNum = @CustSendToFundingName -- Custom Field Name "Funding Specialist", Custom Field Number "9662"
  
 SELECT 'E3' as DB,CustDate.DateValue as LastSubmittedDate,CustTime.StringValue as LastSubmittedTime ,Corres.Office as office, Corres.Company as coname,BOffices.Office as Boffice,loanapp.CurrDecStatusDate as Decisionstatusdate,loanapp.CurrPipeStatusDate as CurrentStatusDate ,loanapp.ChannelType as ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID as record_id,'' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer
 ,borr.lastname as BorrowerLastName
 ,loanapp.CurrentStatus as LoanStatus,'' as LU_FINAL_HUD_STATUS,
 CASE TransType when NULL THEN '' WHEN 'P' THEN 'Purchase money first mortgage' WHEN 'R' THEN 'Refinance' WHEN '2' THEN 'Purchase money second mortgage' WHEN 'S' THEN 'Second mortgage, any other purpose' WHEN 'A' THEN 'Assumption' WHEN 'HOP' THEN 'HELOC - other purpose' WHEN 'HP' THEN 'HELOC - purchase' ELSE '' END AS TransType,
 case LoanData.FinancingType WHEN 'F' THEN 'FHA' WHEN 'C' THEN 'CONVENTIONAL' WHEN 'V' THEN 'VA' End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockDatetime as LockDatetime,LockRecord.LockExpirationDate,
 UnderwriterName as UnderWriter,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,Loandata.LoanProgDesc as ProgramDesc 
 ,CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy='Prior to Purchase' and Category !='LENDER' and Category !='NOTICE' and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0
 WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy='Prior to Purchase' and Category !='LENDER' and Category !='NOTICE') and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived')
 and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0 ELSE (SELECT count(a.ID) as TotalCleared 
 FROM dbo.mwlCondition a where (DueBy='Prior to Purchase' and Category !='LENDER' and Category !='NOTICE') and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived') and a.objOwner_ID IN 
 (SELECT ID FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy='Prior to Purchase' and Category !='LENDER' and Category !='NOTICE') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER 
 ,BOffices.Office as OFFICE_NAME1,isnull(Cust.StringValue,'') as CorrespondentType, SendtoFundingDate.DateValue as SentToFundDate ,
 CASE when (SendtoFundingName.StringValue IS null or SendtoFundingName.StringValue='Other') THEN '' else SendtoFundingName.StringValue End as CorrespodentFSpecialist, LockRecord.deliveryoption
 from mwlLoanApp loanapp 
 Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and borr.Sequencenum=1 
 Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active = 1
 left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id 
 left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType='LOCK' and LockRecord.Status<>'CANCELED'
 Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = loanapp.id and COffices.InstitutionType = 'CORRESPOND' and COffices.objownerName='Contacts' 
 Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.InstitutionType = 'Branch' and BOffices.objownerName='BranchInstitution' 
 Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = 'CORRESPOND' and Corres.objownerName='Correspondent' 
 left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id and Cust.CustFieldDef_ID = @CustomFieldID 
 left join mwlcustomfield as CustDate on CustDate.loanapp_id = loanapp.id and CustDate.CustFieldDef_ID =@SubmittedDate 
 left join mwlcustomfield as CustTime on CustTime.loanapp_id = loanapp.id and CustTime.CustFieldDef_ID = @SubmittedDateTime
 inner join mwlcustomfield as SendtoFundingDate on SendtoFundingDate.loanapp_id = loanapp.id and SendtoFundingDate.CustFieldDef_ID =@CustSendToFundingDateID  and SendtoFundingDate.DateValue is not null 
 left join mwlcustomfield as SendtoFundingName on SendtoFundingName.loanapp_id = loanapp.id and SendtoFundingName.CustFieldDef_ID = @CustSendToFundingNameID 
 where CurrentStatus  IN (select * from dbo.SplitString(@str1,',')) and  ChannelType in('CORRESPOND')  
 and LoanNumber is not null and LoanNumber <> '' 
 and (LockRecord.DeliveryOption = 'Best Effort' OR LockRecord.DeliveryOption IS NULL OR LockRecord.DeliveryOption ='')
 order by Per desc ,CustDate.DateValue asc ,CustTime.StringValue asc 
 END


GO

