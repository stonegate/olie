USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetCountryData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetCountryData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetCountryData]
	@StateCode varchar(500)
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	select distinct ((convert(varchar(500),upper(substring(CountyName,1,1))+lower(substring(CountyName,2,499)))))as County From mwsCounty where StateCode=@StateCode and  CountyName is not null  and CountyName <> '' order by County
END

GO

