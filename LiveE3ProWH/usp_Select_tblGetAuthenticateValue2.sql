USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tblGetAuthenticateValue2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tblGetAuthenticateValue2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[usp_Select_tblGetAuthenticateValue2]
(
@strPsCellNumber varchar(Max)
)
as
begin
SELECT userid,urole,isUserInNewProcess FROM tblUsers 
WHERE  (UserLoginid = @strPsCellNumber)
end



GO

