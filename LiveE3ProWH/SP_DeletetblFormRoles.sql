USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DeletetblFormRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_DeletetblFormRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

CREATE PROCEDURE [dbo].[SP_DeletetblFormRoles]
	(
@formID int
)
AS
BEGIN
	 
      Delete from tblformRoles where formid = @formID
END

GO

