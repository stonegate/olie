USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DocsOnOLIE_ClosingDetailStep7]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DocsOnOLIE_ClosingDetailStep7]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

-- =============================================        
-- Author:  Vipul Thakkar        
-- Create date: Sep, 18 2012        
-- Description: This will give Closing details for the loan number passed        
-- =============================================        
CREATE PROCEDURE [dbo].[sp_DocsOnOLIE_ClosingDetailStep7]
    (
      @LoanNumber VARCHAR(10)
    )
AS 
    
    BEGIN  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
		SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,         
                   -- the entire transaction is terminated and rolled back.

	DECLARE @intHudLineNum AS INT 
	SET @intHudLineNum = 901 -- Hud line number available in mwlprepaid  table and usefull for fetching interset From Date and TO Date which are showing at STEP7 closing information
	     
 
        SELECT  loanapp.TransType AS transactiontype ,
                lookups.DisplayString AS transtype ,
                Institution.Company AS CompayName ,  
 
                Institution.companyemail AS Companyemail ,    
    

                Companyphone.phonenumber AS CompayPhone ,
                CompanyFax.phonenumber AS CompayFax ,
                loanapp.LockExpirationDate ,
                Institution.STATE ,
                Instreset.FromDate ,
                Instreset.ToDate ,
                loandata.NoteRate * 100 AS NoteRate ,
                loanapp.DateofDocuments ,
                loanapp.RescissionDate ,
                loandata.AdjustedNoteAmt ,
                loandata.LoanProgDesc ,
                loandata.AmortTerm ,
                Institution.street ,
                Institution.City ,
                Institution.zipcode,
				loandata.Occupancy   -- BRD 218 (For Step 3)
        FROM    mwlloanapp loanapp
                INNER JOIN mwlloandata loandata ON loanapp.ID = loandata.ObjOwner_ID and  loandata.Active = 1
                LEFT JOIN dbo.mwlInstitution AS Institution ON Institution.ObjOwner_id = loanapp.id   AND Institution.InstitutionType = loanapp.ChannelType     
                                            
 
                 LEFT JOIN mwlphonenumber Companyphone ON Institution.id = Companyphone.objowner_id
                                                         AND Companyphone.phonetype = 'Business'     
                LEFT JOIN mwlphonenumber CompanyFax ON Institution.id = CompanyFax.objowner_id
                                                       AND CompanyFax.phonetype = 'Fax'
                LEFT JOIN mwllookups lookups ON lookups.objectname = 'MwlLoanapp'
                                                AND FieldName = 'Transtype'
                                                AND Lookups.BOCode = loanapp.Transtype    
                 LEFT JOIN mwlprepaid Instreset ON Instreset.loandata_id = loandata.id
                                                  AND Instreset.hudlinenum = @intHudLineNum
        WHERE   loanapp.LoanNumber = @LoanNumber    
    END  



GO

