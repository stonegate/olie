USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetTitleInformation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetTitleInformation]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: <Kanva Patel>
-- Create date: <14th Sep 2012>
-- Description: <This will get Title Information for Docs On OLIE project step-4
-- Customfield 9625 is used for Title Policy Amount
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetTitleInformation]
    (
      @LoanNumber VARCHAR(10)
    )
AS 
   
    BEGIN

		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
		SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,         
                   -- the entire transaction is terminated and rolled back.
    DECLARE @CompName VARCHAR(500)
		DECLARE @intMWFieldNum	AS INT 
			SET @intMWFieldNum = 9625 -- Customfield id 9625 is used for Title Policy Amount which show up at STEP4
				

--Get Title information data
        SELECT  institle.InstitutionType ,
                institle.BusinessType ,             
				institle.Company ,
                institle.street ,
                institle.street2 ,
                institle.city ,
                institle.state ,
                institle.zipcode ,
                institle.companyemail ,
                institle.country ,
                loanapp.TitleOrderNumber AS CommitmentNo ,
                CustServicer.Company AS ProposedInsured ,
                Custinsuraceamt.CurrencyValue AS InsuranceAmount ,
                loanapp.TitleReportDate AS EffectiveDate --,
        FROM    dbo.mwlLoanApp loanapp
                LEFT JOIN dbo.mwlInstitution institle ON institle.ObjOwner_ID = loanapp.ID
                                                         AND InstitutionType = 'TITLE'
                                                         AND ObjOwnerName = 'Contacts'
                LEFT JOIN ( SELECT  ID ,
                                    LoanApp_ID ,
                                    CurrencyValue
                            FROM    mwlCustomfield
                            WHERE   CustFieldDef_ID = ( SELECT
                                                              ID
                                                        FROM  mwsCustFieldDef
                                                        WHERE MWFieldNum = @intMWFieldNum
                                                      )
                          ) Custinsuraceamt ON Custinsuraceamt.LoanApp_ID = loanapp.ID
                LEFT JOIN dbo.mwlInstitution CustServicer ON CustServicer.ObjOwner_ID = loanapp.ID
                                                             AND CustServicer.ObjOwnerName = 'Servicer'
        WHERE   LoanNumber = @LoanNumber
--End Get Title information data

--Get Company Name
        SELECT  @CompName = institle.Company
        FROM    dbo.mwlLoanApp loanapp
                LEFT JOIN dbo.mwlInstitution institle ON institle.ObjOwner_ID = loanapp.ID
                                                         AND InstitutionType = 'TITLE'
                                                         AND ObjOwnerName = 'Contacts'
           LEFT JOIN ( SELECT  ID ,
                                    LoanApp_ID ,
                                    StringValue
                            FROM    mwlCustomfield
                            WHERE   CustFieldDef_ID = ( SELECT
                                                              ID
                                                        FROM  mwsCustFieldDef
                                                        WHERE MWFieldNum = @intMWFieldNum
                                                      )
                          ) mwCustFld ON mwCustFld.LoanApp_ID = loanapp.ID
                LEFT JOIN dbo.mwlInstitution CustServicer ON CustServicer.ObjOwner_ID = loanapp.ID
                                                             AND CustServicer.ObjOwnerName = 'Servicer'
     
        WHERE   LoanNumber = @LoanNumber
--End Get company name

--Get Company Phonenumber
        SELECT  mwlphonenumber.phonenumber ,
                ( SELECT    phonenumber
                  FROM      mwlPhoneNumber
                  WHERE     mwlLoanApp.ID = mwlInstitution.ObjOwner_ID
                            AND mwlInstitution.ID = mwlPhoneNumber.ObjOwner_ID
                            AND mwlloanapp.loannumber = @LoanNumber
                            AND mwlinstitution.company = @CompName
                            AND mwlphonenumber.phonetype = 'Fax'
                ) AS FaxNo
        FROM    mwlLoanApp ,
                mwlInstitution ,
                mwlPhoneNumber
        WHERE   mwlLoanApp.ID = mwlInstitution.ObjOwner_ID
                AND mwlInstitution.ID = mwlPhoneNumber.ObjOwner_ID
                AND mwlloanapp.loannumber = @LoanNumber
                AND mwlinstitution.company = @CompName
                AND mwlphonenumber.phonetype = 'Business'
--End Get company phonenumber

--Get Country for Dropdownlist
        SELECT  cntdesc
        FROM    tblCountry
--End Get Country for Dropdownlist

--Start Get Settlement agent information
				SELECT  institle.InstitutionType ,
						institle.BusinessType ,
						institle.Company ,
						institle.street ,
						institle.street2 ,
						institle.city ,
						institle.state ,
						institle.zipcode ,
						institle.companyemail ,
						institle.country 						                       
				FROM    dbo.mwlLoanApp loanapp
								LEFT JOIN dbo.mwlInstitution institle ON institle.ObjOwner_ID = loanapp.ID
																													AND InstitutionType = 'SETTLEMT'
																													AND ObjOwnerName = 'SetAgent'								
				WHERE   LoanNumber = @LoanNumber
--End Get Settlement agent information

--Start Get Settlement agent Company Name
				SELECT  @CompName = institle.Company
				FROM    dbo.mwlLoanApp loanapp
								LEFT JOIN dbo.mwlInstitution institle ON institle.ObjOwner_ID = loanapp.ID
																														AND InstitutionType = 'SETTLEMT'
																															AND ObjOwnerName = 'SetAgent'
				WHERE   LoanNumber = @LoanNumber
--Start Get Settlement agent Company Name

--Get Company Phonenumber
        SELECT  mwlphonenumber.phonenumber ,
                ( SELECT    phonenumber
                  FROM      mwlPhoneNumber
                  WHERE     mwlLoanApp.ID = mwlInstitution.ObjOwner_ID
                            AND mwlInstitution.ID = mwlPhoneNumber.ObjOwner_ID
                            AND mwlloanapp.loannumber = @LoanNumber
                            AND mwlinstitution.company = @CompName
                            AND mwlphonenumber.phonetype = 'Fax'
                ) AS FaxNo
        FROM    mwlLoanApp ,
                mwlInstitution ,
                mwlPhoneNumber
        WHERE   mwlLoanApp.ID = mwlInstitution.ObjOwner_ID
                AND mwlInstitution.ID = mwlPhoneNumber.ObjOwner_ID
                AND mwlloanapp.loannumber = @LoanNumber
                AND mwlinstitution.company = @CompName
                AND mwlphonenumber.phonetype = 'Business'
--End Get company phonenumber

--Start Get Property State which are in dry State
				Select state From mwlsubjectproperty Where loanapp_id = (Select id From mwlloanapp Where loannumber = @LoanNumber) 
																										AND state in ('AK','AZ','CA','ID','NV','NM','OR','UT','WA')
--End Get Property State
				
    

--Start Get Loan Amount
		Select adjustednoteamt As LoanAmount from mwlloandata where ObjOwner_ID = (Select id from mwlloanapp where loannumber = @LoanNumber)
--End Get Loan Amount
    SET ANSI_NULLS ON
    SET QUOTED_IDENTIFIER ON
END




GO

