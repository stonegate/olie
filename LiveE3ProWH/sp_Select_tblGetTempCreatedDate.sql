USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetTempCreatedDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetTempCreatedDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblGetTempCreatedDate]
(
@iRefUserID varchar(Max)
)
as
begin
 SELECT top 1 isnull(temp.createdDate,getdate()) as createdDate  FROM tblTempUser temp
 left join tblusers users on 
 temp.refuserid= users.userid WHERE (refuserid =@iRefUserID) order by tempid desc 
end


GO

