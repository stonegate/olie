USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Update_tblUpdateAdminProfile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Update_tblUpdateAdminProfile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Gourav  
-- Create date: 10/03/2012  
-- Description: Procedure for clsUser.cs bll    
-- =============================================  
CREATE proc [dbo].[usp_Update_tblUpdateAdminProfile]  
(  
@ufirstname varchar(Max)=null  
,@ulastname varchar(Max)=null  
,@MI varchar(Max)=null  
,@mobile_ph varchar(Max)=null  
,@isactive varchar(Max)=null  
,@upassword varchar(Max)=null  
,@phonenum varchar(Max)=null  
,@Userid varchar(Max)=null  
)  
as  
begin  
update tblusers Set  
ufirstname=@ufirstname,  
ulastname=@ulastname,  
MI=@MI,  
mobile_ph=@mobile_ph,  
phonenum=@phonenum,  
isactive=@isactive,  
upassword=@upassword  
 Where userid = @userid  
end  
  
GO

