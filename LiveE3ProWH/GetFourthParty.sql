USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetFourthParty]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetFourthParty]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  
    
CREATE PROCEDURE [dbo].[GetFourthParty]    
(      
@CompanyID VARCHAR(50)      
)                 
AS    
    
 /* --------------------------------------------------------------------------------------    
     
 Name:     
  GetFourthParty.sql    
    
 Description:    
  Used for the "Select From Setup" Process. Script used to get the list of 4th party Originators    
  for the Correspondant Company    
?    
 DDL information:    
  Database    - InterlinqE3    
  Table       - mwcInstitution    
?    
 Steps:    
  Step 0.0 - Test Data    
  Step 1.0 - Get 4th Party Company Names    
     
 Change History:     
  2013/05/03   - Bultemeier, Shelby    
   Create Script    
     
 ---------------------------------------------------------------------------------------*/    
    
BEGIN    
 --DECLARE @CompanyID VARCHAR(50)    
    
 SET NOCOUNT ON;    
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Per E3)    
 SET XACT_ABORT ON;         -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,     
              -- the entire transaction is terminated and rolled back.    
    
-- Step 0.0 - Test Data    
 --DECLARE @CompanyID VARCHAR(50)    
 --SET @CompanyID = '194708'    
    
-- Step 1.0 - Get 4th Party Company Names    
 SELECT [Company] +' ('+[City]+', '+[State]+')' AS [CompanyName], [ID] AS [CompanyGUID]    
 FROM [dbo].[mwcInstitution]    
 WHERE CompanyEmail = @CompanyID    
  AND Company like '%4th Party%'    
  AND Status = 'Active'    
    
END



GO

