USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_GetStateData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_GetStateData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Krunal>    
-- Create date: <11/20/2012>    
-- Description: <For clsUserE3>    
-- =============================================    
-- exec [Sp_GetStateData] 'northeast'    
-- exec [Sp_GetStateData] ''    
CREATE PROCEDURE [dbo].[Sp_GetStateData]     
@RegionValue varchar(50)   
AS    
BEGIN    
 if(@RegionValue='')  
  select abbreviature as abbr,abbreviature as abbreviature,Statename as Name from tblStateNylex where Statename!='Massachusetts' and Statename!='New York' order by StateName    
 else  
  select abbreviature as abbr,Statename as Name,abbreviature as abbreviature from tblStateNylex   
   where RegionValue=@RegionValue and Statename!='Massachusetts' and Statename!='New York'
  order by StateName     
END    


GO

