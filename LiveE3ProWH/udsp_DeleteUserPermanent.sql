USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_DeleteUserPermanent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_DeleteUserPermanent]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		< author name >
-- Create date: 25/10/2012
-- Description:	
-- =============================================
Create PROCEDURE [dbo].[udsp_DeleteUserPermanent]
@userID varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Delete From tblusers  Where userid = @userID
END


GO

