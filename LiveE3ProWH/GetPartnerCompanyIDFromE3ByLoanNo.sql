USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPartnerCompanyIDFromE3ByLoanNo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPartnerCompanyIDFromE3ByLoanNo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Shwetal Shah>  
-- Create date: <03-07-13>  
-- Description: <Get PartnercompanyID from E3>  
-- =============================================  
CREATE PROCEDURE GetPartnerCompanyIDFromE3ByLoanNo  
(  
@LoanNumber varchar(10)  
)  
AS  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
SET XACT_ABORT ON;  
  
SELECT CompanyEmail from mwlInstitution  
Inner join mwlloanapp on mwlInstitution.objOwner_id=mwlloanapp.id and mwlInstitution.InstitutionType=mwlloanapp.Channeltype  
 where mwlloanapp.loannumber=@LoanNumber  
END  
GO

