USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_NewsAnn_FatchList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_NewsAnn_FatchList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_NewsAnn_FatchList]--3,0 
--[sp_NewsAnn_FatchList_new]20,0    
 -- Add the parameters for the stored procedure here      
 @urole int,      
 @IsNewsOrAnn int      
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
      
--change for corp Comm Start
declare @value varchar(max)
set @value='SELECT Top 5 * FROM 
(
SELECT * FROM 
(
SELECT ID,Title,Body,IsActive,IsNewsAnnouncement,CreatedBy,CreatedDate,IPAddress,Role,NewsAnnousVisbleAd,
NewsAnnousVisbleDR,NewsAnnousVisbleBR ,NewsAnnousVisbleLO,NewsAnnousVisbleCU,NewsAnnousVisbleRE,
convert(bit,0) AS NewsAnnousVisbleRM
,convert(bit,0) AS NewsAnnousVisbleDP,convert(bit,0) AS NewsAnnousVisbleCP,convert(bit,0) AS NewsAnnousVisbleHY 
,convert(bit,0) AS NewsAnnousVisbleDCFI
,convert(bit,0) AS NewsAnnousVisibleSCFI
,convert(bit,0) AS NewsAnnousVisbleUM,convert(bit,0) AS ModifiedBy,CreatedDate AS ModifiedDate,NewsAnnousVisbleAM
,NewsAnnousVisbleDM ,NewsAnnousVisbleROM
,NewsAnnousVisbleCO,NewsAnnousVisbleCRM,NewsAnnousVisbleLP,NewsAnnousVisbleCL,NewsAnnousVisbleRTL,CC_ID,SiteStatus 
,SUBSTRING(body,0,500)+''...............'' as news,Convert(bit,0) as ''IsManageTemplate'',''N'' as Popup 
FROM Retail_tblNewsAndAnnouncement) as T1 
UNION ALL 
SELECT * FROM 
(
SELECT ID,Title,Body,IsActive,IsNewsAnnouncement,CreatedBy,CreatedDate,'''' AS IPAddress,Role,NewsAnnousVisbleAd
,NewsAnnousVisbleDR, 
NewsAnnousVisbleBR,NewsAnnousVisbleLO,NewsAnnousVisbleCU,NewsAnnousVisbleRE,NewsAnnousVisbleRM,NewsAnnousVisbleDP ,
NewsAnnousVisbleCP,NewsAnnousVisbleHY,NewsAnnousVisbleDCFI,NewsAnnousVisibleSCFI,NewsAnnousVisbleUM,ModifiedBy ,
ModifiedDate,convert(bit,0) AS NewsAnnousVisbleAM,convert(bit,0) AS NewsAnnousVisbleDM,convert(bit,0) AS NewsAnnousVisbleROM
,convert(bit,0) AS NewsAnnousVisbleCO 
,convert(bit,0) AS NewsAnnousVisbleCRM,convert(bit,0) AS NewsAnnousVisbleLP,convert(bit,0) AS NewsAnnousVisbleCL,
convert(bit,0) AS NewsAnnousVisbleRTL,CC_ID,SiteStatus 
,SUBSTRING(body,0,500)+''...............'' as news,Convert(bit,0) as ''IsManageTemplate'',''N'' as Popup 
FROM tblNewsAndAnnouncement
) 
as T2
)
as tblNewsAndAnnouncement '
--change for corp Comm End


      
    -- Insert statements for procedure here      
 if (@urole = 0) --Admin      
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where  IsActive=1 and IsNewsAnnouncement=0 order by createddate desc    
set @value=@value + ' where  IsActive=1 and IsNewsAnnouncement=0 order by createddate desc    '  

 if (@urole = 1) --Director of Re    
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where   NewsAnnousVisbleDR=1 and IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc    
set @value=@value + ' where NewsAnnousVisbleDR=1 and IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc    ' 

 
 if (@urole = 2) --LO    
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where   NewsAnnousVisbleLO=1 and IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc    
set @value=@value + ' where NewsAnnousVisbleLO=1 and IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc    ' 

 if (@urole = 3) --Branch    
set @value=@value + ' where NewsAnnousVisbleBR=1 and IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc    ' 

 if (@urole = 4) --Customer    
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where   NewsAnnousVisbleCU=1 and IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc    
set @value=@value + ' where  NewsAnnousVisbleCU=1 and IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc    '

 if (@urole = 5) --Realtor    
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where   NewsAnnousVisbleRE=1 and IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc    
set @value=@value + ' where NewsAnnousVisbleRE=1 and IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc    ' 

 if (@urole = 6) --Same As Role 3    
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where   NewsAnnousVisbleBR=1 and IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc    
set @value=@value + ' where  NewsAnnousVisbleBR=1 and IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc    '

 if (@urole = 7) --Same As Role 3    
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where   NewsAnnousVisbleBR=1 and IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc    
set @value=@value + ' where  NewsAnnousVisbleBR=1 and IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc    '

 if (@urole = 8) --Regional Manager    
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where   NewsAnnousVisbleRM=1 and IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc    
set @value=@value + ' where  NewsAnnousVisbleRM=1 and IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc    '

 if (@urole = 13) --Broker LO Rep    
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where   NewsAnnousVisbleRM=1 and IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc    
set @value=@value + ' where  NewsAnnousVisbleRM=1 and IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc    '

 if (@urole = 12) --Director of Production    
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where   NewsAnnousVisbleDP=1 and IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc    
set @value=@value + ' where  NewsAnnousVisbleDP=1 and IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc    '

 if (@urole = 19) --Director of correspondent    
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where  IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc    
set @value=@value + ' where IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc    ' 

 if (@urole = 20) --Same As Role 3    
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where   NewsAnnousVisbleCP=1 and IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc    
set @value=@value + ' where  NewsAnnousVisbleCP=1 and IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc    '

 if (@urole = 21) --Same As Role 3    
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where   NewsAnnousVisbleHY=1 and IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc    
set @value=@value + ' where  NewsAnnousVisbleHY=1 and IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc    '

if (@urole = 25) --Director of CFI    
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where   NewsAnnousVisbleDCFI=1 and IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc    
set @value=@value + ' where   NewsAnnousVisbleDCFI=1 and IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc    '

if (@urole = 26) --Sales Directr    
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where   NewsAnnousVisibleSCFI=1 and IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc    
 set @value=@value + ' where   NewsAnnousVisibleSCFI=1 and IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc '

if (@urole = 35) --Corporate communication        
 --select top 5 *  , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where  IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc  
set @value=@value + ' where  IsActive=1 and IsNewsAnnouncement='+Convert(varchar(2),@IsNewsOrAnn)+' order by createddate desc'

EXEC(@value)
END

GO

