USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Reports_PipelineView_new]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Reports_PipelineView_new]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
  
  
  
  
CREATE PROCEDURE [dbo].[sp_Reports_PipelineView_new]   
 -- Add the parameters for the stored procedure here  
   
--select * from InterlinqE3.dbo.mwlLoanApp  
@UserID int,  
@UserRole int,  
@CustFieldDef varchar(100),  
@OrignatorID varchar(4000),  
@CurrentStatus  varchar(4000)  
  
AS  
BEGIN  
  
  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
--set @sqlString='InterlinqE3.dbo.mwlLoanApp'  
    -- Insert statements for procedure here  
--use InterlinqE3  
--select 'E3' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.Channeltype as BusType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus,loanapp.ID as record_id,'' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate, approvalStat.StatusDateTime as DateApproved,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe, LookupsPro.DisplayString as  LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, CustFNMA.YNValue,LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,uEmail.Email as Underwriter_Email,CASE ISNULL(RTRIM(Broker.Company),'') WHEN '' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate ,  CASE WHEN (SELECT count(a.ID) as TotalConditions FROM mwlCondition a where DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE' and a.objOwner_ID IN ( SELECT ID FROM mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0  WHEN (SELECT count(a.ID) as TotalCleared FROM mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE')  and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived' ) and a.objOwner_ID IN (SELECT id FROM mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE (SELECT count(a.ID) as TotalCleared  FROM mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE') and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived'  ) and a.objOwner_ID IN  (SELECT ID FROM mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM mwlCondition a where ( DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE') and a.objOwner_ID IN ( SELECT ID FROM mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER,case cust.stringvalue WHEN '--Select One--' then '' else cust.stringvalue end as UnderwriterType    
--from mwlLoanApp as loanapp    
--Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id    
--Inner join mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id     
--left join mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id    
--Left join mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = 'BROKER' and Broker.objownerName='Broker'  
--left join mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType='LOCK' and  LockRecord.Status<>'CANCELED'    
--Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc='01 - Registered'   
--Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved'   
--Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName='mwlLoanData' and fieldname='refipurpose'     
--Left join mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = 'CORRESPOND' and Corres.objownerName='Correspondent'    
--left join mwamwuser as uEmail on uSummary.UnderWriter_id = uEmail.id     
--left join mwsCustFieldDef as mCust on  mCust.CustomFieldName like'Correspondent Type%'    
--left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID =mCust.id     
--Left outer join mwlLookups LookupsPro on LoanData.FinancingType=LookupsPro.BOCode  and LookupsPro.ObjectName='mwlLoanData' and LookupsPro.fieldname='FinancingType'     
--left join mwlcustomfield as CustFNMA on CustFNMA.loanapp_id = loanapp.id  and CustFNMA.CustFieldDef_ID='66C1EDE68E304DD5BEDA3AD79AE9C0EA'     
--where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''   and  CurrentStatus  IN ('01 - Registered','05 - Application Taken', '03 - Appt Set to Review Disclosures', '04 - Disclosures Sent', '07 - Disclosures Received' ,'09 - Application Received','09 - Application Recieved','13 - File Intake','17 - Submission On Hold','19 - Disclosures','20 - Disclosures On Hold','21 - In Processing','21 - In Processing','25 - Submitted to Underwriting','29 - In Underwriting' ,'15 - Appraisal Ordered','22 - Appraisal Received','10 - Application on Hold')   
--order by Per desc  
if (@UserRole = 0)   
begin  
select sc.SchDate,loanapp.Originator_id, 'E3' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.Channeltype as BusType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus,loanapp.ID as record_id,'' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate, approvalStat.StatusDateTime as DateApproved,case when TransType is 
null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe, LookupsPro.DisplayString as  LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, CustFNMA.YNValue,LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,uEmail.Email as Underwriter_Email,  
--CASE ISNULL(RTRIM(Broker.Company),'') WHEN '' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,  
Lookups.DisplayString as CashOut,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate ,  CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE' and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0  WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE')  and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived' ) and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 
THEN 0   ELSE (SELECT count(a.ID) as TotalCleared  FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE') and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived'  ) and a.objOwner_ID IN  (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER,case cust.stringvalue WHEN '--Select One--' then '' else cust.stringvalue end as UnderwriterType ,'' as BrokerName   
from mwlLoanApp  loanapp    
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id    
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id     
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id    
--Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = 'BROKER' and Broker.objownerName='Broker'  
left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType='LOCK' and  LockRecord.Status<>'CANCELED'    
Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc='01 - Registered'   
Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved'   
Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName='mwlLoanData' and fieldname='refipurpose'     
--Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = 'CORRESPOND' and Corres.objownerName='Correspondent'    
left join dbo.mwamwuser as uEmail on uSummary.UnderWriter_id = uEmail.id     
--left join mwsCustFieldDef as mCust on  mCust.CustomFieldName like'Correspondent Type%'    
left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID ='C4D2D8038F614DF8A1B8A6622C99FC3E'    
Left outer join mwlLookups LookupsPro on LoanData.FinancingType=LookupsPro.BOCode  and LookupsPro.ObjectName='mwlLoanData' and LookupsPro.fieldname='FinancingType'     
left join mwlcustomfield as CustFNMA on CustFNMA.loanapp_id = loanapp.id  and CustFNMA.CustFieldDef_ID=@CustFieldDef     
left join tblScheduleClosing sc on sc.loanno = loanapp.LoanNumber   
where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''   
  and  CurrentStatus  IN (select * from dbo.SplitString(@CurrentStatus,','))   
  
--and (Originator_id in (select * from dbo.SplitString(@OrignatorID,',')))   
  
order by Per desc   
end  
---sales director role  
if (@UserRole = 26 or @UserRole = 25)   
begin  
select loanapp.Originator_id, sc.SchDate, 'E3' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.Channeltype as BusType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus,loanapp.ID as record_id,'' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate, approvalStat.StatusDateTime as DateApproved,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe, LookupsPro.DisplayString as  LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, CustFNMA.YNValue,LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,uEmail.Email as Underwriter_Email,  
CASE ISNULL(RTRIM(Broker.Company),'') WHEN '' THEN ISNULL(RTRIM(Corres.Company),'') ELSE ISNULL(RTRIM(Broker.Company),'') END as BrokerName,  
Lookups.DisplayString as CashOut,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate ,  CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE' and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0  WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE')  and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived' ) and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 
THEN 0   ELSE (SELECT count(a.ID) as TotalCleared  FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE') and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived'  ) and a.objOwner_ID IN  (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER,case cust.stringvalue WHEN '--Select One--' then '' else cust.stringvalue end as UnderwriterType    
from mwlLoanApp  loanapp    
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id    
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id     
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id    
Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = 'BROKER' and Broker.objownerName='Broker'  
left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType='LOCK' and  LockRecord.Status<>'CANCELED'    
Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc='01 - Registered'   
Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved'   
Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName='mwlLoanData' and fieldname='refipurpose'     
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = 'CORRESPOND' and Corres.objownerName='Correspondent'    
left join dbo.mwamwuser as uEmail on uSummary.UnderWriter_id = uEmail.id     
--left join mwsCustFieldDef as mCust on  mCust.CustomFieldName like'Correspondent Type%'    
left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID ='C4D2D8038F614DF8A1B8A6622C99FC3E'    
Left outer join mwlLookups LookupsPro on LoanData.FinancingType=LookupsPro.BOCode  and LookupsPro.ObjectName='mwlLoanData' and LookupsPro.fieldname='FinancingType'     
left join mwlcustomfield as CustFNMA on CustFNMA.loanapp_id = loanapp.id  and CustFNMA.CustFieldDef_ID=''     
left join tblScheduleClosing sc on sc.loanno = loanapp.LoanNumber   
where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''   
  and  CurrentStatus  IN (select * from dbo.SplitString(@CurrentStatus,','))   
and (Originator_id in (select * from dbo.SplitString(@OrignatorID,',')))   
--and (Originator_id in (select * from dbo.SplitString(@OrignatorID,',')))   
  
order by Per desc   
end  
else   
begin  
select sc.SchDate,loanapp.Originator_id, 'E3' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.Channeltype as BusType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus,loanapp.ID as record_id,'' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate, approvalStat.StatusDateTime as DateApproved,case when TransType is 
null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe, LookupsPro.DisplayString as  LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, CustFNMA.YNValue,LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,uEmail.Email as Underwriter_Email,  
--CASE ISNULL(RTRIM(Broker.Company),'') WHEN '' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,  
Lookups.DisplayString as CashOut,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate ,  CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE' and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0  WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE')  and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived' ) and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 
THEN 0   ELSE (SELECT count(a.ID) as TotalCleared  FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE') and (CurrentState='CLEARED' OR CurrentState='SUBMITTED' or CurrentState='Waived'  ) and a.objOwner_ID IN  (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy='Prior to Closing' and Category !='LENDER' and Category !='NOTICE') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER,case cust.stringvalue WHEN '--Select One--' then '' else cust.stringvalue end as UnderwriterType    
from mwlLoanApp  loanapp    
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id    
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id     
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id    
--Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = 'BROKER' and Broker.objownerName='Broker'  
left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType='LOCK' and  LockRecord.Status<>'CANCELED'    
Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc='01 - Registered'   
Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved'   
Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName='mwlLoanData' and fieldname='refipurpose'     
--Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = 'CORRESPOND' and Corres.objownerName='Correspondent'    
left join dbo.mwamwuser as uEmail on uSummary.UnderWriter_id = uEmail.id     
--left join mwsCustFieldDef as mCust on  mCust.CustomFieldName like'Correspondent Type%'    
left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID ='C4D2D8038F614DF8A1B8A6622C99FC3E'    
Left outer join mwlLookups LookupsPro on LoanData.FinancingType=LookupsPro.BOCode  and LookupsPro.ObjectName='mwlLoanData' and LookupsPro.fieldname='FinancingType'     
left join mwlcustomfield as CustFNMA on CustFNMA.loanapp_id = loanapp.id  and CustFNMA.CustFieldDef_ID=@CustFieldDef     
left join tblScheduleClosing sc on sc.loanno = loanapp.LoanNumber   
where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''   
  and  CurrentStatus  IN (select * from dbo.SplitString(@CurrentStatus,','))   
and (Originator_id in (select * from dbo.SplitString(@OrignatorID,',')))   
order by Per desc   
  
end  
END  
  
  
  
  
  

GO

