USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetOriginatorGUIDByAEID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetOriginatorGUIDByAEID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  
  
CREATE PROCEDURE [dbo].[GetOriginatorGUIDByAEID]  
   
@UserID VARCHAR(50)  
AS  
    
 /* --------------------------------------------------------------------------------------  
   
 Name:   
  GetOriginatorGUIDByAEID.sql  
  
 Description:  
  Used for the "Select From Setup" Process. Script used to get the Originator GUID based   
  upon the User ID.  
�  
 DDL information:  
  Database    - tpotest  
  Table       - tblusers  
�  
 Steps:  
  Step 0.0 - Test Data  
  Step 1.0 - Get OriginatorGUID   
   
 Change History:   
  2013/05/03   - Saini, Amit  
   Create Script  
   
 ---------------------------------------------------------------------------------------*/  
  
BEGIN  
 --DECLARE @UserID VARCHAR(50)  
  
 SET NOCOUNT ON;  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Per E3)  
 SET XACT_ABORT ON;         -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,   
              -- the entire transaction is terminated and rolled back.  
  
-- Step 0.0 - Test Data  
 --DECLARE @UserID VARCHAR(50)  
 --SET @UserID = '1666'  
  
-- Step 1.0 - Get OriginatorGUID   
 SELECT E3Userid FROM tblusers WHERE userid = @UserID  
   
END  


GO

