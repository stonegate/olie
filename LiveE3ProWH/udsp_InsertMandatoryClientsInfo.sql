USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_InsertMandatoryClientsInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_InsertMandatoryClientsInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Example for execute stored procedure      
--EXECUTE [udsp_InsertMandatoryClientsInfo] '478452','HOME STATE BANK NA',1000000,500,50000,50000,10931 
-- =============================================          
-- Author:  Karan Gulati          
-- Create date: 15/04/2013 
-- Name:udsp_InsertMandatoryClientsInfo.sql         
-- Description: Insert Clients Details for Mandatory-Best Effort Delivery Task          
-- =============================================          
CREATE PROCEDURE [dbo].[udsp_InsertMandatoryClientsInfo] 
(
 --Parameter declaration            
@PartnerCompanyId VARCHAR(40),          
@ClientCompanyName VARCHAR(100),        
@NetWorth MONEY,        
@MandatoryPercentage MONEY,        
@TotalMandatoryLimit MONEY,        
@MandatoryLimitBal MONEY,    
@CreatedBy int     
)  
AS          
BEGIN 

SET NOCOUNT ON;          
SET XACT_ABORT ON;-- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error, 
				  -- the entire transaction is terminated and rolled back.      

INSERT INTO [dbo].[tblManageMandatoryClientsInfo](PartnerCompanyId,ClientCompanyName,NetWorth,MandatoryPercentage,TotalMandatoryLimit,MandatoryLimitBal,IsActive,CreatedBy,CreatedDate) VALUES(@PartnerCompanyId,@ClientCompanyName,@NetWorth,
@MandatoryPercentage,@TotalMandatoryLimit,@MandatoryLimitBal,'True',@CreatedBy,GETDATE())               

END 
GO

