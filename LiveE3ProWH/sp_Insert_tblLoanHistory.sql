USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_tblLoanHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_tblLoanHistory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		 Nitin
-- Create date: 10/05/2012
-- Description:	Procedure for clspipelineE3.cs bll 
-- =============================================
CREATE procEDURE [dbo].[sp_Insert_tblLoanHistory]
(@LoanNumber varchar(15),
@LoanId varchar(32),
@UserId int

) 
	
AS
BEGIN
	
	SET NOCOUNT ON;
 Insert into tblLoanHistory(LoanNumber,LoanId,UserId)
values(@LoanNumber,
@LoanId,
@UserId)
END


GO

