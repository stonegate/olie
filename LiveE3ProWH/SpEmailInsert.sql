USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpEmailInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpEmailInsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<StoneGate>
-- Create date: <10-04-2012>
-- Description:	<Insert Email in tblEmail table>
-- =============================================
CREATE PROCEDURE [dbo].[SpEmailInsert]
	@Subject varchar(1000)='',
	@EmailBodyText nvarchar(max)='',
	@CreatedDate datetime =null,
	@Role varchar(1000),
	@CreatedBy varchar(500),
	@IsSent int =0
AS
BEGIN
	Insert into tblEmail(Subject,EmailBodyText,CreatedDate,Role,CreatedBy,IsSent) 
	Values(@Subject,@EmailBodyText,@CreatedDate,@Role,@CreatedBy,@IsSent)
END

GO

