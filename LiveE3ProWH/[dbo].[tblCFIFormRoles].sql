USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCFIFormRoles]') AND type in (N'U'))
DROP TABLE [dbo].[tblCFIFormRoles]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCFIFormRoles](	  [formRoleID] INT NOT NULL	, [formID] INT NULL	, [uroleid] INT NOT NULL IDENTITY(1,1))USE [HomeLendingExperts]
GO

