USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetAllBrokerApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetAllBrokerApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <StoneGate>  
-- Create date: <10-05-2012>  
-- Description: <Get All Broker Application>  
-- =============================================  
CREATE PROCEDURE [dbo].[SpGetAllBrokerApplication]  

AS  
BEGIN  
 select BrokerAppID,PrimaryContactEmail from tblBrokerApplication where IsActive = 1 
END  
GO

