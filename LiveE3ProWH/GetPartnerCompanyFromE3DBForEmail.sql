USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPartnerCompanyFromE3DBForEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPartnerCompanyFromE3DBForEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Shwetal Shah>  
-- Create date: <19 June, 2013>  
-- Description: <Get Partner Company Name for a Loan from E3 Database to generate Email Notification Subject Line on Loan Submission and Loan Update process>  
-- =============================================  
CREATE PROCEDURE [dbo].[GetPartnerCompanyFromE3DBForEmail]   
(  
@LoanNumber varchar(10)  
)  
AS  
BEGIN  
  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
SET XACT_ABORT ON;  
  
SELECT Company FROM mwlloanapp  
INNER JOIN mwlInstitution on mwlloanapp.id=mwlInstitution.objOwner_id and mwlloanapp.Channeltype=mwlInstitution.InstitutionType  
WHERE loannumber=@LoanNumber  
  
END  
GO

