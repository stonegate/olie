USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPurchaseOnHoldNotificationHistory]') AND type in (N'U'))
DROP TABLE [dbo].[tblPurchaseOnHoldNotificationHistory]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPurchaseOnHoldNotificationHistory](	  [ID] BIGINT NOT NULL IDENTITY(1,1)	, [LoanNum] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MailTo] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MailCC] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [InsertDate] DATETIME NULL	, [IsSent] BIT NULL DEFAULT((1))	, CONSTRAINT [PK_tblPurchaseOnHoldNotificationHistory] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO

