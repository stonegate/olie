USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblManageTemplate]') AND type in (N'U'))
DROP TABLE [dbo].[tblManageTemplate]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblManageTemplate](	  [ID] INT NOT NULL IDENTITY(1,1)	, [Title] NVARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Body] TEXT(16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL	, [IsManageTemplate] BIT NULL	, [CreatedBy] INT NULL	, [CreatedDate] DATETIME NULL	, [Role] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RoleC] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ManageTempVisbleAd] BIT NULL	, [ManageTempVisbleDR] BIT NULL	, [ManageTempVisbleBR] BIT NULL	, [ManageTempVisbleLO] BIT NULL	, [ManageTempVisbleCU] BIT NULL	, [ManageTempVisbleRE] BIT NULL	, [ManageTempVisbleRM] BIT NULL	, [ManageTempVisbleDP] BIT NULL	, [ManageTempVisbleCP] BIT NULL	, [ManageTempVisbleHY] BIT NULL	, [ManageTempVisbleDCFI] BIT NULL	, [ManageTempVisibleSCFI] BIT NULL	, [ManageTempVisbleUM] BIT NULL	, [ModifiedBy] INT NULL	, [ModifiedDate] DATETIME NULL	, [RoleR] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CC_ID] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT((0))	, [SiteStatus] VARCHAR(2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT('TC')	, CONSTRAINT [PK_tblManageTemplate] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO

