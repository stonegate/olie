USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ISEmergingBankers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_ISEmergingBankers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_ISEmergingBankers]
(
@PartnerCompanyId varchar(100)
)

AS
BEGIN

Select ltrim(rtrim(IsEmergingBanker)) IsEmergingBanker from tblEmergingBankers where PartnerCompanyId = @PartnerCompanyId and Status = 'Y' and Flag = 'EB'

END

GO

