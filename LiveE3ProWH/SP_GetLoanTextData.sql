USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetLoanTextData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetLoanTextData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

Create PROCEDURE [dbo].[SP_GetLoanTextData]
(
@loan_no char(15)
)

AS
BEGIN
select loan_no,docs_text,filename,dttime from tblLoanDocs 
 where loan_no=@loan_no
             
END

GO

