USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblCFIUserRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblCFIUserRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Select_tblCFIUserRoles]              
as              
begin
--SELECT DISTINCT '-1' as RoleID,'-1' as ID, 'All User' as Roles  FROM tblRoles          
--union ALL             
SELECT
	*
FROM tblRoles
WHERE Id IN (0, 3, 20, 25, 21, 26)            
end

GO

