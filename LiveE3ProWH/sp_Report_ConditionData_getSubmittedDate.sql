USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Report_ConditionData_getSubmittedDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Report_ConditionData_getSubmittedDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>	

--sp_Report_ConditionData_getSubmittedDate '05A11A2D3153496FB6C49024EE1AD9E7','312321'
-- =============================================
CREATE PROCEDURE [dbo].[sp_Report_ConditionData_getSubmittedDate]     
 -- Add the parameters for the stored procedure here    
 @Condid varchar(50),    
 @LoanNumber varchar(50)    
AS    
BEGIN    
Declare @condition varchar(50)    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
    
    -- Insert statements for procedure here    
 Select @condition = DueBy  from mwlCondition where ID=@Condid    
 if @condition <> ''     
 begin    
  if @condition = 'Prior to Closing'    
  begin    
  SELECT submittedDate from tblCondText_History where Loan_No= @LoanNumber and condrecid= @Condid  
  end    
  else     
  begin    
  SELECT dttime as submittedDate from tblCondText_History where Loan_No=@LoanNumber and condrecid= @Condid     
  end    
 end    
 else    
 begin    
 SELECT submittedDate from tblCondText_History where Loan_No=@LoanNumber and condrecid= @Condid     
 end    
END    

GO

