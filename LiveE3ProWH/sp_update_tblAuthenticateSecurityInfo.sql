USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_update_tblAuthenticateSecurityInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_update_tblAuthenticateSecurityInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_update_tblAuthenticateSecurityInfo]
(
@strUserId varchar(Max)
)
as
begin
update tblUserDetails 
set lastlogintime = Getdate()
,MaxSecurityAttempt = isnull(MaxSecurityAttempt,0) + 1 
where UsersID = @strUserId
end



GO

