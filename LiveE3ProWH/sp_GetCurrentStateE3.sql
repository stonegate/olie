USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCurrentStateE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCurrentStateE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================
-- Author:		<Stonegate>
-- Create date: <10/8/2012>
-- Description:	<this will Get Current Status in E3 DB>
-- =====================================================
CREATE PROCEDURE [dbo].[sp_GetCurrentStateE3]
	@strLoanNumber varchar(25),
	@strCust1 varchar(250),
	@strCust2 varchar(250),
	@strCust3 varchar(250),
	@strCust4 varchar(250)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)

Select top 1 mwlConditionState.updatedondate as CondUpdatedDatetime 
From 
	mwlLoanApp, mwlCondition, mwlConditionState 
Where 
	mwlLoanApp.ID = mwlCondition.ObjOwner_ID 
	AND mwlCondition.ID = mwlConditionState.ObjOwner_ID 
	AND mwlLoanApp.LoanNumber = ''+ @strLoanNumber +'' 
	And mwlConditionState.state = 'REJECTED' 
order by mwlConditionState.updatedondate desc
                 

Select CASE when (Cust4.updatedondate IS null) THEN (CASE when (Cust3.updatedondate IS null ) 
	THEN (case when(Cust2.updatedondate IS null ) then ( case when (Cust1.updatedondate IS null ) then NULL else Cust1.updatedondate end ) else Cust2.updatedondate end ) 
	else Cust3.updatedondate End ) else Cust4.updatedondate End as AssignedDatetime 
From mwlloanapp as la 
	left join mwlcustomfield as Cust1 on la.id=Cust1.Loanapp_id 
	and Cust1.CustFieldDef_ID =''+  @strCust1 +''
	left join mwlcustomfield as Cust2 on la.id=Cust2.Loanapp_id
	and Cust2.CustFieldDef_ID =''+ @strCust2 +''
	left join mwlcustomfield as Cust3 on la.id=Cust3.Loanapp_id 
	and Cust3.CustFieldDef_ID =''+ @strCust3 +'' 
	left join mwlcustomfield as Cust4 on  la.id=Cust4.Loanapp_id 
	and Cust4.CustFieldDef_ID =''+ @strCust4 +'' 
where 
	la.loannumber=''+ @strLoanNumber +'' 
order by Cust4.updatedondate,Cust3.updatedondate,Cust2.updatedondate,Cust1.updatedondate desc

END

GO

