USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLoanRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLoanRegister]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertLoanRegister]
(
@borrLast varchar(max),
@transType varchar(max),
@loanAmt numeric(18,3),
@loanPgm varchar(max),
@Userid bigint,
@FileType varchar(Max),
@FileName varchar(Max)
)
AS
BEGIN
Insert into tblLoanReg(borrLast,transType,loanAmt,loanPgm,userid,filetype,filename) 
Values(
@borrLast,
@transType,
@loanAmt,
@loanPgm,
@Userid,
@FileType,
@FileName
)
END


GO

