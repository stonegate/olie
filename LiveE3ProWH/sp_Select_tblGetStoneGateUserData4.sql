USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetStoneGateUserData4]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetStoneGateUserData4]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Gourav  
-- Create date: 10/03/2012  
-- Description: Procedure for clsUser.cs bll    
-- =============================================  
CREATE proc [dbo].[sp_Select_tblGetStoneGateUserData4]
(
@oParterCompanyid varchar(Max),
@iUserid varchar(Max)
)
as  
begin  
SELECT tblUsers.userid, tblUsers.ufirstname, 
tblUsers.ulastname, tblUsers.uemailid, tblUsers.urole, 
tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, 
tblUsers.uparent, tblUsers.upassword, tblUsers.maxloginattempt,
 tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender, tblUsers.carrierid, 
Carrier.Carrier,tblRoles.Roles,tblUsers.PartnerCompanyid,tblusers.AEID,IsUserInNewProcess,userLoginid,
case isnull(tblUsers.IsUserInNewProcess,0) when 1 then 'Yes' else 'No' end as IsUserInNewProcess1 
FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID LEFT OUTER JOIN tblRoles ON tblUsers.urole = tblRoles.ID 
where (PartnerCompanyid=@oParterCompanyid or CreatedBy = @iUserid) and  
userid not in(@iUserid) order by  tblUsers.userid desc
end  


GO

