USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLoanDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLoanDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Nitin  
-- Create date: 10/08/2012  
-- Description: Procedure for clsLoanApplication BLL  
-- =============================================  
create PROCEDURE [dbo].sp_InsertLoanDoc    
(@LoanNo varchar(20)=null,  
@LoanDocText varchar(500)=null,  
@FileName varchar(500)=null,   
@Userid int)  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
  
    Insert into tblloandoc(Loan_No,Comments,DocName,createddate,userid) Values(
@LoanNo,@LoanDocText,@FileName,getdate(),@Userid)  
END  
  

GO

