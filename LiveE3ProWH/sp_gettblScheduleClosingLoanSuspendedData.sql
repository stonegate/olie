USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_gettblScheduleClosingLoanSuspendedData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_gettblScheduleClosingLoanSuspendedData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/06/2012
-- Description:	Procedure for clspipelineE3 BLL
-- =============================================
CREATE procEDURE [dbo].[sp_gettblScheduleClosingLoanSuspendedData]
(
@LoanNo varchar(50)
)
AS
BEGIN
	
	SET NOCOUNT ON;
select ScheduleID as SchId,LoanNo as Loan_No ,Comments, FeeSheet as filename,convert(varchar(35),SubmitedDate,101) as dttime  from tblScheduleClosing 
                  where IsFromLoanSuspendedDoc =1 and  LoanNo=@LoanNo
END


GO

