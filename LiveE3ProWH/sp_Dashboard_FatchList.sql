USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Dashboard_FatchList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Dashboard_FatchList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Dashboard_FatchList] 
	-- Add the parameters for the stored procedure here
	@urole int,
	@IsNewsOrAnn int,
	@operation varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
if (@Operation = 'News')
begin	
if (@urole = 0)
	begin
	select top 5 * , SUBSTRING(body,0,500) +'...............'  as News from tblNewsAndAnnouncement   where   NewsAnnousVisbleDR=1 and IsActive=1 and IsNewsAnnouncement=@IsNewsOrAnn order by createddate desc 
	end
end
else
begin	
	select * from tblQuickLinks	
end
END

GO

