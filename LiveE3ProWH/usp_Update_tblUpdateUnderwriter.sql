USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Update_tblUpdateUnderwriter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Update_tblUpdateUnderwriter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Gourav  
-- Create date: 10/03/2012  
-- Description: Procedure for clsUser.cs bll    
-- =============================================  
CREATE proc [dbo].[usp_Update_tblUpdateUnderwriter]  
(  
@ufirstname varchar(Max)=null  
,@ulastname varchar(Max)=null  
,@uemailid varchar(Max)=null  
,@urole varchar(Max)=null  
,@isadmin varchar(Max)=null  
,@isactive varchar(Max)=null  
,@uidprolender varchar(Max)=null  
,@uparent varchar(Max)=null  
,@mobile_ph varchar(Max)=null  
,@phonenum varchar(Max)=null  
,@companyname varchar(Max)=null  
,@carrierid varchar(Max)=null  
,@loginidprolender varchar(Max)=null  
,@upassword varchar(Max)=null  
,@MI varchar(Max)=null  
,@E3Userid varchar(Max)=null  
,@PasswordExpDate varchar(Max)=null  
,@userid varchar(Max)=null  
)  
as  
begin  
UPDATE tblUsers  
   SET ufirstname = @ufirstname  
      ,ulastname = @ulastname  
      ,uemailid = @uemailid  
      ,urole = @urole  
      ,isadmin = @isadmin  
      ,isactive = @isactive  
      ,uidprolender = @uidprolender  
      ,uparent = @uparent  
      ,upassword = @upassword  
      ,mobile_ph = @mobile_ph  
      ,phonenum = @phonenum        
   ,loginidprolender = @loginidprolender  
      ,carrierid = @carrierid  
      ,MI = @MI  
      ,companyname = @companyname  
      ,E3Userid = @E3Userid  
      ,PasswordExpDate = @PasswordExpDate  
  Where userid =@userid  
end  
  
  
GO

