USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetFHACaseNumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetFHACaseNumber]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 
-- Description:	GetBusineesType_Role for clsPipeline.cs
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetFHACaseNumber]
( 
	@LoanNumber varchar(Max)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	select Casenum from mwlloanapp where LoanNumber=@LoanNumber
END

GO

