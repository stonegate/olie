USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getBDLReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getBDLReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_getBDLReport] @filter NVARCHAR(max) = NULL
	,@sortfield NVARCHAR(50) = '[UploadDate] DESC'
	,@pageindex TINYINT = 0
	,@pagesize TINYINT = 50
AS
BEGIN
	DECLARE @sql NVARCHAR(max)
	DECLARE @startpage INT
	DECLARE @endpage INT
	DECLARE @sortpagefilter NVARCHAR(max)
	DECLARE @UnRegisteredLoansRetainDays VARCHAR(10)

	SET @startpage = @pageindex * @pagesize
	SET @endpage = (@pageindex * @pagesize) + @pagesize
	SET @UnRegisteredLoansRetainDays = (
			SELECT KeyValue
			FROM tblAppsettings
			WHERE KeyName = 'UnRegisteredLoansRetainDays'
			)
	SET @SQL = '
	UPDATE BSL
	SET FinalStatus = ''EXPIRED''
	FROM tblBulkSplitLoanFiles BSL
	INNER JOIN tblbulkloanfiles BLF ON BSL.bulkloanfileid = BLF.bulkloanfileid
	WHERE LoanFileID IN (
			SELECT LoanFileID
			FROM tblBulkSplitLoanFiles BSL
			WHERE BSL.STATUS = ''SUSPEND''
				AND (
					BSL.FinalStatus IS NULL
					OR BSL.FinalStatus NOT IN (
						''FAIL''
						,''SUCCESS''
						,''EXPIRED''
						)
					)
				AND BSL.CreatedOn NOT BETWEEN dbo.Get_NthWorkingDay(10)
					AND GETDATE() + 1
			)'
	SET @SQL = @SQL + ' SELECT 
		LoanFileID as ''LoanID'',

		BSL.LoanNUmber as LoanNumber,
		 
		BLF.CreatedOn as ''UploadDate'',

		BLF.CompanyName as  ''Company'',

		users.ufirstName +'' '' + users.ulastName as ''UserName'',

		BSL.Status as ''InitialStatus'',

		BSL.finalStatus as ''FinalStatus'',
		
		BSL.DuplicateMatches as duplicateMatches,
		
		BSL.FileName

		INTO #tempRecords

		from tblbulksplitloanfiles BSL

	INNER JOIN tblbulkloanfiles BLF on BSL.BulkLoanFileId = BLF.BulkLoanFileId
	INNER JOIN tblusers users on BLF.CreatedBy = users.userid


	where BSL.status IN (''SUSPEND'',''BLOCKED'')'
END

IF (
		@filter = ''
		OR @filter IS NULL
		)
	SET @filter = ''
ELSE
	SET @filter = 'WHERE ' + @filter

IF (@endpage = 0)
	SET @sortpagefilter = ''
ELSE
	SET @sortpagefilter = ' where finaltemp.RowNum > ' + CONVERT(VARCHAR(10), @startpage) + ' and finaltemp.RowNum <= ' + convert(VARCHAR(10), @endpage)

SET @sql = @sql + ' select  * from (  select   ROW_NUMBER() OVER (ORDER BY ' + CONVERT(VARCHAR(100), @sortfield) + ')  as RowNum,  * from #tempRecords ' + @filter + ') as finaltemp ' + @sortpagefilter
SET @sql = @sql + ' select Count(*) as count from #tempRecords ' + @filter

PRINT @sql

EXECUTE sp_executesql @SQL


GO

