USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCorrSpecialistView_sorting]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCorrSpecialistView_sorting]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================       
-- Author:  <Stonegate>    

-- Create date: <10/8/2012>    

-- Description: <This will Get Corr. Specialist View sorting>    

-- Author:  Ratan Gohil 

-- Modified date: 11/01/2013         

-- Name:sp_GetCorrSpecialistView_sorting.sql      

-- Description: returning three additional fields for purchase pending task.  

-- Modified by : Suvarna on 17/12/2013 - Added clear purchase pending calculation logic  

-- ============================================= 

CREATE PROCEDURE [dbo].[sp_GetCorrSpecialistView_sorting]    

(    

 @CustomFieldD1 varchar(250),    

 @CustomFieldD2 varchar(250),    

 @CustomFieldD3 varchar(250),    

 @CustomFieldD4 varchar(250),    

 @CustomField varchar(250),    

 @CustomFieldN1 varchar(250),    

 @CustomFieldN2 varchar(250),    

 @CustomFieldN3 varchar(250),    

 @CustomFieldN4 varchar(250),    

 @SubmittedDateID varchar(250),    

 @SubmittedDateTimeID varchar(250),    

 @CustSendToFundingDate varchar(250)    

)    

AS    

BEGIN    

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 


DECLARE @custDiligenceLevelID AS VARCHAR(50)   

 

Declare @ClearPurDateDays smallint 

Declare @NewLoansRangeDate DateTime    

Declare @ClearPurDateDaysOld smallint

SET @ClearPurDateDays = (select top 1 KeyValue from tblAppSettings where KeyName='ClearedPurDateDays') 	

SET @ClearPurDateDaysOld = (select  top 1 KeyValue from tblAppSettings where KeyName='ClearedPurDateDaysOld')

SET @NewLoansRangeDate = CAST ((select  top 1 KeyValue from tblAppSettings where KeyName='NewloansRangeDate') AS DATETIME) 

    

SET @custDiligenceLevelID = (SELECT ID FROM mwsCustFieldDef WHERE MWFieldNum = '9492')  -- Custom Field Name "Diligence Level", Custom Field Number "9490" 

   

SELECT        loanapp.LoanNumber,

                             (SELECT        COUNT(mwlConditionState.ObjOwner_ID) AS Rejected

                               FROM            mwlLoanApp INNER JOIN

                                                         mwlCondition ON mwlLoanApp.ID = mwlCondition.ObjOwner_ID INNER JOIN

                                                         mwlConditionState ON mwlCondition.ID = mwlConditionState.ObjOwner_ID

                               WHERE        (mwlLoanApp.ID IN

                                                             (SELECT        ID

                                                               FROM            mwlLoanApp AS loan1

                                                               WHERE        (LoanNumber = loanapp.LoanNumber))) AND (mwlConditionState.State = 'REJECTED') AND (mwlCondition.DueBy = 'Prior to Purchase') 

                                                         AND (mwlCondition.Category <> 'LENDER') AND (mwlCondition.Category <> 'NOTICE')) AS Rejected, COALESCE (UPPER(Diligence.StringValue), '') 

                         AS DilligenceLevel, CASE WHEN (CustN4.StringValue IS NULL OR

                         CustN4.StringValue = 'Other') THEN (CASE WHEN (CustN3.StringValue IS NULL OR

                         CustN3.StringValue = 'Other') THEN (CASE WHEN (CustN2.StringValue IS NULL OR

                         CustN2.StringValue = 'Other') THEN (CASE WHEN (CustN1.StringValue IS NULL OR

                         CustN1.StringValue = 'Other') THEN '' ELSE CustN1.StringValue END) ELSE CustN2.StringValue END) ELSE CustN3.StringValue END) 

                         ELSE CustN4.StringValue END AS CorrespodentSpecialist, 'E3' AS DB, CustDate.DateValue AS LastSubmittedDate, CustTime.StringValue AS LastSubmittedTime, 

                         Offices.Office AS office, Corres.Company AS coname, BOffices.Office AS Boffice, loanapp.CurrDecStatusDate AS Decisionstatusdate, 

                         loanapp.CurrPipeStatusDate AS CurrentStatusDate, loanapp.ChannelType, loanapp.DecisionStatus, loanapp.OriginatorName, loanapp.ID AS record_id, 

                         '' AS LT_USR_ASSGNDRAWER, loanapp.LoanNumber AS loan_no, loanapp.CloserName AS Closer, borr.LastName AS BorrowerLastName, 

                         loanapp.CurrentStatus AS LoanStatus, '' AS LU_FINAL_HUD_STATUS, CASE TransType WHEN NULL 

                         THEN '' WHEN 'P' THEN 'Purchase money first mortgage' WHEN 'R' THEN 'Refinance' WHEN '2' THEN 'Purchase money second mortgage' WHEN 'S' THEN 'Second mortgage, any other purpose'

                          WHEN 'A' THEN 'Assumption

  

' WHEN 'HOP' THEN 'HELOC - other purpose' WHEN 'HP' THEN 'HELOC - purchase' ELSE '' END AS TransType, 

                         CASE LoanData.FinancingType WHEN 'F' THEN 'FHA' WHEN 'C' THEN 'CONVENTIONAL' WHEN 'V' THEN 'VA' END AS LoanProgram, 

                         loanData.AdjustedNoteAmt AS loan_amt, LockRecord.Status AS LockStatus, LockRecord.LockDateTime AS LockDatetime, LockRecord.LockExpirationDate, 

                         LockRecord.DeliveryOption, uSummary.UnderwriterName AS UnderWriter, Broker.Company AS BrokerName, CAST(borr.CompositeCreditScore AS CHAR(5)) 

                         AS CreditScoreUsed, loanData.LoanProgDesc AS ProgramDesc, CASE WHEN

                             (SELECT        COUNT(a.ID) AS TotalConditions

                               FROM            dbo.mwlCondition a

                               WHERE        DueBy = 'Prior to Purchase' AND Category != 'LENDER' AND Category != 'NOTICE' AND a.objOwner_ID IN

                                                             (SELECT        ID

                                                               FROM            dbo.mwlLoanApp AS loan1

                                                               WHERE        loan1.LoanNumber = LoanApp.LoanNumber)) = 0 THEN 0 WHEN

                             (SELECT        COUNT(a.ID) AS TotalCleared

                               FROM            dbo.mwlCondition a

                               WHERE        (DueBy = 'Prior to Purchase' AND Category != 'LENDER' AND Category != 'NOTICE') AND (CurrentState = 'CLEARED' OR

                                                         CurrentState = 'SUBMITTED' OR

                                                         CurrentState = 'Waived') AND a.objOwner_ID IN

                                                             (SELECT        id

                                                               FROM            dbo.mwlLoanApp AS loan2

                                                               WHERE        loan2.LoanNumber = LoanApp.Loannumber)) = 0 THEN 0 ELSE

                             (SELECT        COUNT(a.ID) AS TotalCleared

                               FROM            dbo.mwlCondition a

                               WHERE        (DueBy = 'Prior to Purchase' AND Category != 'LENDER' AND Category != 'NOTICE') AND (CurrentState = 'CLEARED' OR

                                                         CurrentState = 'SUBMITTED' OR

                                                         CurrentState = 'Waived') AND a.objOwner_ID IN

                                                             (SELECT        ID

                                                               FROM            dbo.mwlLoanApp AS loan2

                                                               WHERE        loan2.LoanNumber = LoanApp.LoanNumber)) * 100 /

                             (SELECT        COUNT(a.ID) AS TotalConditions

                               FROM            dbo.mwlCondition a

                               WHERE        (DueBy = 'Prior to Purchase' AND Category != 'LENDER' AND Category != 'NOTICE') AND a.objOwner_ID IN

                                                             (SELECT        ID

                                                               FROM            dbo.mwlLoanApp AS loan1

                                                               WHERE        loan1.LoanNumber = LoanApp.LoanNumber)) END AS PER, BOffices.Office AS OFFICE_NAME1, ISNULL(Cust.StringValue, '') 

AS CorrespondentType, '' AS CorrespodentFSpecialist, (CASE WHEN

                             ((SELECT        TOP 1 CASE WHEN (Cust4.updatedondate IS NULL) THEN (CASE WHEN (Cust3.updatedondate IS NULL) 

                                                          THEN (CASE WHEN (Cust2.updatedondate IS NULL) THEN (CASE WHEN (Cust1.updatedondate IS NULL) THEN NULL ELSE Cust1.updatedondate END) 

                                                          ELSE Cust2.updatedondate END) ELSE Cust3.updatedondate END) ELSE Cust4.updatedondate END AS AssignedDatetime

                                 FROM            mwlloanapp AS la LEFT JOIN

                                                          mwlcustomfield AS Cust1 ON la.id = Cust1.Loanapp_id AND Cust1.CustFieldDef_ID = @CustomFieldD1 LEFT JOIN

                                                          mwlcustomfield AS Cust2 ON la.id = Cust2.Loanapp_id AND Cust2.CustFieldDef_ID = @CustomFieldD2 LEFT JOIN

                                                          mwlcustomfield AS Cust3 ON la.id = Cust3.Loanapp_id AND Cust3.CustFieldDef_ID = @CustomFieldD3 LEFT JOIN

                                                          mwlcustomfield AS Cust4 ON la.id = Cust4.Loanapp_id AND Cust4.CustFieldDef_ID = @CustomFieldD4

                                 WHERE        la.loannumber = loanapp.LoanNumber) <

                             (SELECT        TOP 1 mwlConditionState.updatedondate

                               FROM            mwlLoanApp, mwlCondition, mwlConditionState

                               WHERE        mwlLoanApp.ID = mwlCondition.ObjOwner_ID AND mwlCondition.ID = mwlConditionState.ObjOwner_ID AND 

                                                         mwlLoanApp.LoanNumber = loanapp.LoanNumber AND mwlConditionState.state = 'REJECTED')) THEN 1 ELSE 0 END) AS IsRejected

														 

														 

						,(CASE when (LockRecord.DeliveryOption = 'Mandatory' ) THEN Null else lap2.StatusDateTime End )as StatusDateTime    

						,(CASE when (LockRecord.DeliveryOption = 'Mandatory' ) THEN Null else 
						(case when LockRecord.LockExpirationDate > DATEADD(day,@ClearPurDateDays,lap2.StatusDateTime )  then  LockRecord.LockExpirationDate	 						
						when (lap2.StatusDateTime > @NewLoansRangeDate) then DATEADD(day,@ClearPurDateDays,lap2.StatusDateTime ) else DATEADD(day,@ClearPurDateDaysOld,lap2.StatusDateTime) end )   End ) as ClearedForPurchaseDate    

						,(CASE when (LockRecord.DeliveryOption = 'Mandatory' ) THEN Null else ISNULL(CustCRD4.DateValue,ISNULL(CustCRD3.DateValue,ISNULL(CustCRD2.DateValue,ISNULL(CustCRD.DateValue,CustCRD.DateValue)))) End ) as ConditionReviewDate  



FROM            mwlLoanApp AS loanapp INNER JOIN

                         mwlBorrower AS borr ON borr.LoanAPP_ID = loanapp.ID AND borr.SequenceNum = 1 INNER JOIN

                         mwlloandata AS loanData ON loanData.ObjOwner_ID = loanapp.ID LEFT OUTER JOIN

                         mwlUnderwritingSummary AS uSummary ON uSummary.LoanApp_ID = loanapp.ID LEFT OUTER JOIN

                         mwlInstitution AS Broker ON Broker.ObjOwner_ID = loanapp.ID AND Broker.InstitutionType = 'BROKER' AND Broker.ObjOwnerName = 'Broker'  LEFT OUTER JOIN

						  (select LockExpirationDate,loanapp_id,[Status],DeliveryOption,LockDatetime from mwlLockRecord  where LockType='LOCK' and [Status] = 'CONFIRMED' and [Status] <>'CANCELED') LockRecord
						  on LockRecord.loanapp_id= loanapp.id  LEFT OUTER JOIN                        						 
						 
						-- mwlLockRecord AS LockRecord ON LockRecord.LoanApp_ID = loanapp.ID AND LockRecord.LockType = 'LOCK' AND LockRecord.Status <> 'CANCELED'				 
						 

                         mwlInstitution AS Offices ON Offices.ObjOwner_ID = loanapp.ID AND Offices.InstitutionType = 'Broker' AND Offices.ObjOwnerName = 'Broker' LEFT OUTER JOIN

                         mwlInstitution AS COffices ON COffices.ObjOwner_ID = loanapp.ID AND COffices.InstitutionType = 'CORRESPOND' AND 

                         COffices.ObjOwnerName = 'Contacts' LEFT OUTER JOIN

                         mwlInstitution AS BOffices ON BOffices.ObjOwner_ID = loanapp.ID AND BOffices.InstitutionType = 'Branch' AND 

 BOffices.ObjOwnerName = 'BranchInstitution' LEFT OUTER JOIN

                         mwlInstitution AS Corres ON Corres.ObjOwner_ID = loanapp.ID AND Corres.InstitutionType = 'CORRESPOND' AND 

                         Corres.ObjOwnerName = 'Correspondent' LEFT OUTER JOIN

                         mwlcustomfield AS Cust ON Cust.LoanApp_ID = loanapp.ID AND Cust.CustFieldDef_ID = @CustomField LEFT OUTER JOIN

                         mwlcustomfield AS CustN1 ON CustN1.LoanApp_ID = loanapp.ID AND CustN1.CustFieldDef_ID = @CustomFieldN1 LEFT OUTER JOIN

                         mwlcustomfield AS CustN2 ON CustN2.LoanApp_ID = loanapp.ID AND CustN2.CustFieldDef_ID = @CustomFieldN2 LEFT OUTER JOIN

                         mwlcustomfield AS CustN3 ON CustN3.LoanApp_ID = loanapp.ID AND CustN3.CustFieldDef_ID = @CustomFieldN3 LEFT OUTER JOIN

                         mwlcustomfield AS CustN4 ON CustN4.LoanApp_ID = loanapp.ID AND CustN4.CustFieldDef_ID = @CustomFieldN4 LEFT OUTER JOIN

                         mwlcustomfield AS CustDate ON CustDate.LoanApp_ID = loanapp.ID AND CustDate.CustFieldDef_ID = @SubmittedDateID LEFT OUTER JOIN

                         mwlcustomfield AS CustTime ON CustTime.LoanApp_ID = loanapp.ID AND CustTime.CustFieldDef_ID = @SubmittedDateTimeID LEFT OUTER JOIN

                         mwlcustomfield AS SendtoFundingDate ON SendtoFundingDate.LoanApp_ID = loanapp.ID AND SendtoFundingDate.CustFieldDef_ID = @CustSendToFundingDate AND 

                         (SendtoFundingDate.DateValue IS NULL OR

                         SendtoFundingDate.DateValue = '') LEFT OUTER JOIN

                         mwlcustomfield AS Diligence ON Diligence.LoanApp_ID = loanapp.ID AND Diligence.CustFieldDef_ID = @custDiligenceLevelID

						 

						left join (SELECT MIN(StatusDateTime) as StatusDateTime,Loanapp_id,StatusDesc from mwlAppStatus group by Loanapp_id,StatusDesc) lap2 on  lap2.Loanapp_id=loanapp.id and StatusDesc in ('54 - Purchase Pending')

						left join mwlcustomfield as CustCRD on CustCRD.Loanapp_id = loanapp.id and CustCRD.CustFieldDef_ID = @CustomFieldD1

						left join mwlcustomfield as CustCRD2 on CustCRD2.Loanapp_id = loanapp.id and CustCRD2.CustFieldDef_ID = @CustomFieldD2

						left join mwlcustomfield as CustCRD3 on CustCRD3.Loanapp_id = loanapp.id and CustCRD3.CustFieldDef_ID = @CustomFieldD3

						left join mwlcustomfield as CustCRD4 on CustCRD4.Loanapp_id = loanapp.id and CustCRD4.CustFieldDef_ID = @CustomFieldD4  

						 

WHERE        (loanapp.CurrentStatus IN ('54 - Purchase Pending')) AND (loanapp.ChannelType IN ('CORRESPOND')) AND (borr.SequenceNum = 1) AND 

                         (loanapp.LoanNumber IS NOT NULL) AND (loanapp.LoanNumber <> '') AND (loanapp.ID NOT IN

                             (SELECT        LoanApp_ID

                               FROM            mwlcustomfield AS SendtoFundingDate1

                               WHERE        (LoanApp_ID = loanapp.ID) AND (CustFieldDef_ID = @CustSendToFundingDate) AND (ISNULL(DateValue, 0) <> '1900-01-01 00:00:00.000')))
							  AND (LockRecord.DeliveryOption = 'Best Effort' OR LockRecord.DeliveryOption IS NULL OR LockRecord.DeliveryOption ='')

ORDER BY Per DESC, LastSubmittedDate

    

END


GO

