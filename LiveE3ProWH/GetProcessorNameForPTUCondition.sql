USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetProcessorNameForPTUCondition]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetProcessorNameForPTUCondition]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE GetProcessorNameForPTUCondition  
(  
@LoanNumber varchar(10)  
)  
AS  
BEGIN  
  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
SET XACT_ABORT ON;  
  
SELECT Email FROM mwaMWUser WHERE ID IN (SELECT Processor_ID FROM mwlLoanApp WHERE LoanNumber=@LoanNumber)  
  
END  
GO

