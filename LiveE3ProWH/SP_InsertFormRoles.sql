USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_InsertFormRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_InsertFormRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

Create PROCEDURE [dbo].[SP_InsertFormRoles]
	(
@formID int,
@uRols int = null,
@Flag Varchar(15)
)
AS
BEGIN
	 IF @Flag= 'Delete'
      Begin
      Delete from tblformRoles where formid = @formID
      end
     else if @Flag= 'Insert'
      Begin
       Insert into dbo.tblFormRoles(formID,uroleid) values(@formID,@uRols)
      End
END


-- Create Procedure Coding ENDS Here

GO

