USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetSecurityQueandAnswer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetSecurityQueandAnswer]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

Create PROCEDURE [dbo].[SP_GetSecurityQueandAnswer]
(
@UsersID int
)
AS
BEGIN
select * From tblUserDetails ud  
inner join tblSecurityQuestions sc on ud.SecurityQuestion2=sc.QuestionId 
where UsersID =@UsersID;
select userloginid from tblusers where userid=@UsersID
END	


GO

