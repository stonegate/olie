USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetKatalystFolderAttributes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetKatalystFolderAttributes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Shwetal,,Shah>
-- Create date: <03 March 2013>
-- Description:	<This Stored procedure used for Get folder attributes from E3 DB>
---Modify By: <Vipul, Thacker>
-- Modify date: <08 April 2013>
-- =============================================  
CREATE PROCEDURE [dbo].[GetKatalystFolderAttributes]  
(  
@LoanNumber varchar(50)  
)  
AS  
BEGIN  

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
  
Select Bor.FirstName,Bor.LastName, mwlLoanApp.Originator_ID,mwlLoanApp.OriginatorName,mwlLoanApp.LienPosition  
,(Select top(1) AdjustedNoteAmt from mwlLoanData where ObjOwner_ID = mwlLoanApp.ID order by ID desc) as LoanAmount  
,Prop.Street,Prop.City,Prop.State,Prop.County, Prop.ZipCode ,(select top(1) LoanProgramName from mwlLoanData where ObjOwner_Id = mwlLoanApp.ID order by ID desc) as LoanProgram2
, ISNULL(mwlLoanApp.ProcessorName,'') as ProcessorName
From mwlLoanApp  WITH(NOLOCK)
Inner JOin (Select FirstName,LastName,LoanApp_ID From mwlBorrower WITH(NOLOCK) where sequencenum=1) as Bor  
ON Bor.LoanAPP_ID = mwlLoanApp.ID  
Inner Join (Select LoanApp_ID,(ISNULL(Street,'')+','+ISNULL(Street2,'')) as Street, City, State,ISNULL(County,'') County, ZipCode from mwlSubjectProperty WITH(NOLOCK)) Prop  
On Prop.LoanApp_ID = mwlLoanApp.ID  
Where LoanNumber = @LoanNumber   
  
END  




GO

