USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCorrSpecialistView_CorrMgr]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCorrSpecialistView_CorrMgr]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_GetCorrSpecialistView_CorrMgr]
@strFrom nvarchar(500),
@CustomField nvarchar(500),
@CustomFieldN1 nvarchar(500),
@CustomFieldN2 nvarchar(500),
@CustomFieldN3 nvarchar(500),
@CustomFieldN4 nvarchar(500),
@SubmittedDateID nvarchar(500),
@SubmittedDateTimeID nvarchar(500),
@CustSendToFundingDate nvarchar(500),
@str1 nvarchar(max),
@str2 nvarchar(max)
as
begin

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)

Declare @sql varchar(max)   
Declare @sql2 varchar(max) 
--Declare @sql3 varchar(max)  

	set @sql = ''  
	set @sql2 = '' 
                        
	set @sql =  @sql + 'select '''' as CorrespodentFSpecialist, ''E3'' as DB,CustDate.DateValue as LastSubmittedDate,CustTime.StringValue as LastSubmittedTime ,Offices.Office as office, Corres.Company as coname,BOffices.Office as Boffice,loanapp.CurrDecStatusDate as Decisionstatusdate,loanapp.CurrPipeStatusDate as CurrentStatusDate ,loanapp.ChannelType as ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,
				 CASE TransType when NULL THEN '''' WHEN ''P'' THEN ''Purchase money first mortgage'' WHEN ''R'' THEN ''Refinance'' WHEN ''2'' THEN ''Purchase money second mortgage'' WHEN ''S'' THEN ''Second mortgage, any other purpose'' WHEN ''A'' THEN ''Assumption'' WHEN ''HOP'' THEN ''HELOC - other purpose'' WHEN ''HP'' THEN ''HELOC - purchase'' ELSE '''' END AS TransType,
				 case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN ''VA'' End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockDatetime as LockDatetime,LockRecord.LockExpirationDate,
				 UnderwriterName as UnderWriter,Broker.Company as BrokerName,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,Loandata.LoanProgDesc as ProgramDesc ,
				 CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'' and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0
				 WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'')
				 and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0 ELSE (SELECT count(a.ID) as TotalCleared 
				 FROM dbo.mwlCondition a where (DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') and a.objOwner_ID IN 
				 (SELECT ID FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy=''Prior to Purchase'' and Category !=''LENDER'' and Category !=''NOTICE'') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER 
				 ,BOffices.Office as OFFICE_NAME1,isnull(Cust.StringValue,'''') as CorrespondentType '

	IF (@strFrom = 'sendTofundingMgr' OR @strFrom = 'sendTofunding')
	begin
		set @sql =  @sql + '  , SendtoFundingDate.DateValue as SentToFundDate '
	end

	set @sql2 =  @sql2 + '  from mwlLoanApp loanapp Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and borr.Sequencenum=1 Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
				left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id 
				 and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK'' and LockRecord.Status<>''CANCELED'' 
				Left join dbo.mwlInstitution as Offices on Offices.ObjOwner_id = loanapp.id and Offices.InstitutionType = ''Broker'' and Offices.objownerName=''Broker'' 
				Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = loanapp.id and COffices.InstitutionType = ''CORRESPOND'' and COffices.objownerName=''Contacts'' 
				Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.InstitutionType = ''Branch'' and BOffices.objownerName=''BranchInstitution'' 
				 Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' 
				 left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id and Cust.CustFieldDef_ID =''' + @CustomField + ''' 
				 left join mwlcustomfield as CustN1 on CustN1.loanapp_id = loanapp.id and CustN1.CustFieldDef_ID =''' + @CustomFieldN1 + ''' 
				 left join mwlcustomfield as CustN2 on CustN2.loanapp_id = loanapp.id and CustN2.CustFieldDef_ID =''' + @CustomFieldN2 + ''' 
				 left join mwlcustomfield as CustN3 on CustN3.loanapp_id = loanapp.id and CustN3.CustFieldDef_ID =''' + @CustomFieldN3 + ''' 
				 left join mwlcustomfield as CustN4 on CustN4.loanapp_id = loanapp.id and CustN4.CustFieldDef_ID =''' + @CustomFieldN4 + ''' 
				 left join mwlcustomfield as CustDate on CustDate.loanapp_id = loanapp.id and CustDate.CustFieldDef_ID =''' + @SubmittedDateID + ''' 
				 left join mwlcustomfield as CustTime on CustTime.loanapp_id = loanapp.id and CustTime.CustFieldDef_ID =''' + @SubmittedDateTimeID + ''' '

	if @strFrom ='reg'
	begin
		set @sql2 =  @sql2 + '  where CurrentStatus IN (''01 - Registered'') '
	end
	else if @strFrom ='cleared'
	begin
		set @sql2 =  @sql2 + '  where CurrentStatus IN (''33 - Cleared to Close'') '
	end
	else if @strFrom ='closing'
	begin
		set @sql2 =  @sql2 + '  where CurrentStatus IN (''50 - Closing Package Received'') '
	end
	else if @strFrom ='pur'
	begin
		set @sql2 =  @sql2 + '  where CurrentStatus IN (''54 - Purchase Pending'') '
	end
	else if (@strFrom ='crfunding' OR @strFrom ='cfunding')
	begin
		set @sql2 =  @sql2 + '  where CurrentStatus IN (''58 - Cleared for Funding'') '
	end
	else if @strFrom ='sendTofundingMgr'
	begin
		set @sql2 =  @sql2 + ' inner join mwlcustomfield as SendtoFundingDate on SendtoFundingDate.loanapp_id = loanapp.id and SendtoFundingDate.CustFieldDef_ID =''' + @CustSendToFundingDate + ''' and SendtoFundingDate.DateValue is not null 
					 where CurrentStatus  IN (select * from dbo.SplitString('''+@str1+''','','')) '
	end
	else if @strFrom ='sendTofunding'
	begin
		set @sql2 =  @sql2 + '  inner join mwlcustomfield as SendtoFundingDate on SendtoFundingDate.loanapp_id = loanapp.id and SendtoFundingDate.CustFieldDef_ID =''' + @CustSendToFundingDate + ''' and SendtoFundingDate.DateValue is not null 
					 where CurrentStatus  IN (select * from dbo.SplitString('''+@str2+''','','')) '
	end
	else if @strFrom ='creview'
	begin
		set @sql2 =  @sql2 + '  where CurrentStatus IN (''52 - Closing Package Reviewed'')'
	end
	else if @strFrom ='purcond'
	begin
		set @sql2 =  @sql2 + '  where CurrentStatus IN (''56 - Purch Conditions Received'') '
	end
	else if @strFrom ='pursub'
	begin
		set @sql2 =  @sql2 + '  where CurrentStatus IN (''51 - Purchase Sub On Hold'') '
	end
	else if @strFrom ='aged_LSMCF'
	begin
		set @sql2 =  @sql2 + ' where CurrentStatus IN (''58 - Cleared for Funding'')
					 and (datediff(day,loanapp.CurrPipeStatusDate,getdate())<=5) '
	end
	else if @strFrom ='agedpursub'
	begin
		set @sql2 =  @sql2 + ' where CurrentStatus IN (''51 - Purchase Sub On Hold'')
					 and convert (varchar(10),loanapp.CurrPipeStatusDate + 20,101) >=convert (varchar(10), getdate() ,101) '
	end
	else if @strFrom ='agedcloreceived'
	begin
		set @sql2 =  @sql2 + '  where CurrentStatus IN (''50 - Closing Package Received'') 
					 and convert (varchar(10),loanapp.CurrPipeStatusDate + 20,101) >=convert (varchar(10), getdate() ,101) '
	end
	else if @strFrom ='agedcloreviewed'
	begin
		set @sql2 =  @sql2 + '  where CurrentStatus IN (''52 - Closing Package Reviewed'')
					 and convert (varchar(10),loanapp.CurrPipeStatusDate + 20,101) >=convert (varchar(10), getdate() ,101) '
	end
	else if @strFrom ='agedpurpending'
	begin
		set @sql2 =  @sql2 + '  where CurrentStatus IN (''54 - Purchase Pending'')
					 and convert (varchar(10),loanapp.CurrPipeStatusDate + 20,101) >=convert (varchar(10), getdate() ,101) '
	end

	if (@strFrom ='sendTofundingMgr' OR @strFrom ='sendTofunding')
	begin
		set @sql2 =  @sql2 + ' and  ChannelType in(''CORRESPOND'')'
	end
	else
	begin
		set @sql2 =  @sql2 + ' and ChannelType in(''CORRESPOND'')'
	end

	set @sql2 =  @sql2 + '  and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' '

	if(@strFrom <> 'reg' and @strFrom <> 'cleared')
		BEGIN
			set @sql2 =  @sql2 + ' and (LockRecord.DeliveryOption <> ''Mandatory''  OR LockRecord.DeliveryOption is null OR LockRecord.DeliveryOption = '''' )'
		END

	if @strFrom ='aged_LSMCF'
	begin
		set @sql2 =  @sql2 + ' order by loanapp.CurrPipeStatusDate asc '
	end
	else
	begin
		set @sql2 =  @sql2 + ' order by Per desc ,CustDate.DateValue asc ,CustTime.StringValue asc '
	end

	print (@sql + @sql2)
	exec (@sql + @sql2) 
end



GO

