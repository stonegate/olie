USE [HomeLendingExperts]
								GO
								SET ANSI_NULLS ON
								GO
								SET QUOTED_IDENTIFIER ON
								GOCREATE FUNCTION [dbo].[ParseByComma] (
@String VARCHAR(600) )
RETURNS @TblSubString TABLE
(
VarSubString VARCHAR(50)
)
AS
BEGIN
DECLARE @intPos INT,
@SubStr VARCHAR(50)

-- Remove All Spaces
SET @String = REPLACE(@String, ' ','')
-- Find The First Comma
SET @IntPos = CHARINDEX(',', @String)
-- Loop Until There Is Nothing Left Of @String
WHILE @IntPos > 0
BEGIN
-- Extract The String
SET @SubStr = SUBSTRING(@String, 0, @IntPos)
-- Insert The String Into The Table
INSERT INTO @TblSubString (VarSubString) VALUES (@SubStr)
-- Remove The String & Comma Separator From The Original
SET @String = SUBSTRING(@String, LEN(@SubStr) + 2, LEN(@String) - LEN(@SubStr) + 1)
-- SET @String = REPLACE(@String, @SubStr + ',', '', 1)
-- Get The New Index To The String
SET @IntPos = CHARINDEX(',', @String)
END
-- Return The Last One
INSERT INTO @TblSubString (VarSubString) VALUES (@String)
RETURN
END

GO

