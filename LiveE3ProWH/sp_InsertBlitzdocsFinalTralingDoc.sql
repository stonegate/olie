USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertBlitzdocsFinalTralingDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertBlitzdocsFinalTralingDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertBlitzdocsFinalTralingDoc]
(
@Comments varchar(Max)
,@userid bigint,
@DocName varchar(Max)
,@loan_no varchar(max)
)
AS
BEGIN
Insert into tblFinalTralingDoc(Comments,userid,DocName,loan_no)
Values(@Comments,@userid,@DocName,@loan_no)
END



GO

