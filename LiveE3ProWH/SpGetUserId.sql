USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetUserId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetUserId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetUserId] 
	@EmailId nvarchar(500)
AS
BEGIN
	SELECT userid  FROM tblUsers WHERE  (uemailid =@EmailId)
END

GO

