USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_ConditionReceivedDateinE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_ConditionReceivedDateinE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  Vipul      
-- Create date: 11/06/2012      
-- Description: Procedure for insert conditionreceived date and time upon cleared condition
-- =============================================      
CREATE procEDURE [dbo].[sp_Insert_ConditionReceivedDateinE3]      
(      
@strSubmittedDateID varchar(100),      
@LoanApp_id varchar(400),      
@strSubmittedTimeID varchar(115)  
  
)      
AS      
BEGIN      
     
         
  
 if not exists(select * From mwlcustomfield where CustFieldDef_ID in (@strSubmittedDateID)and  LoanApp_id in(@LoanApp_id))     
 BEGIN  
                       insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,DateValue)values      
                       (replace(newid(),'-',''),     
                      @LoanApp_id,     
                     @strSubmittedDateID,Convert(varchar(12),getdate(),101))  
 END  

   
      ---for Time Update  
       
 if not exists(select * From mwlcustomfield where CustFieldDef_ID in (@strSubmittedTimeID) and  LoanApp_id in(@LoanApp_id))     
  BEGIN  
  insert into mwlcustomfield(ID,LoanApp_id,CustFieldDef_ID,StringValue)values      
   (replace(newid(),'-',''),     
   @LoanApp_id,     
   @strSubmittedTimeID,Convert(varchar,getdate(),8))   
  END  
    
                        
                      
   
 END

GO

