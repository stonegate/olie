USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBranchOffice]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetBranchOffice]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- AUTHOR:  <Amit Saini>            
-- CREATE DATE: <4/20/2013>            
-- DESCRIPTION: <  Used for the "Select From Setup" Process. Script used to get the Originator Name,             
   --Originator GUID, Company GUID, Underwriting GUID>             
CREATE PROCEDURE GetBranchOffice            
 @CompanyID VARCHAR(50),            
 @Channel VARCHAR(50) ,        
 @FourthPartyGUID VARCHAR(50) = null             
            
AS            
            
 /* --------------------------------------------------------------------------------------            
             
 Name:             
  GetContactGUID.sql            
            
 Description:            
  Used for the "Select From Setup" Process. Script used to get the Originator Name,             
   Originator GUID, Company GUID, Underwriting GUID            
�            
 DDL information:            
  Database    - InterlinqE3            
  Table       - mwcContact, mwcInstitution, mwaMWUser            
�            
 Steps:            
  Step 0.0 - Test Data            
  Step 1.0 - Get the Originator Name, Originator GUID, Company GUID, Underwriting GUID            
             
 Change History:             
  2013/05/20   - Bultemeier, Shelby            
   Create Script            
             
 ---------------------------------------------------------------------------------------*/            
            
BEGIN            
            
 SET NOCOUNT ON;            
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)            
 SET XACT_ABORT ON;         -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,             
              -- the entire transaction is terminated and rolled back.            
            
-- Step 0.0 - Test Data            
            
 --DECLARE @CompanyID VARCHAR(50)            
 --DECLARE @Channel VARCHAR(50)            
            
 -- Hybrid - Broker            
  --SET @CompanyID = '2289'            
  --SET @Channel = 'Broker'            
            
 -- Hybrid - Correspond            
  --SET @CompanyID = '268552'            
  --SET @Channel = 'Correspond'            
            
 -- Broker            
  --SET @CompanyID = '263708'            
  --SET @Channel = 'Broker'            
            
 -- Correspond            
  --SET @companyID = '194708'            
  --SET @Channel = 'Correspond'            
            
 -- Correspond - 4th Party            
  --SET @CompanyID = '194708'            
  --SET @Channel = 'Correspond'            
            
-- Step 1.0 - Get the Originator Name, Originator GUID, Company GUID, Underwriting GUID            
            
 --IF @FourthPartyGUID IS NULL            
 --BEGIN           
	SELECT TOP 1 [ID] AS [CompanyGUID],'' AS Company          
	FROM  [dbo].[mwcInstitution]          
	WHERE [CompanyEMail] = @CompanyID          
	AND InstitutionType = @Channel          
	AND Status = 'Active'              
	ORDER BY Company         
             
 --END          
 --ELSE            
 --BEGIN          
 -- SELECT DISTINCT [ID] AS [CompanyGUID], Office AS Company            
 --  FROM  [dbo].[mwcInstitution]            
 --  WHERE [CompanyEMail] = @CompanyID            
 --   AND InstitutionType = @Channel            
 --   AND Status = 'Active'            
 --   AND [mwcInstitution].Company  like '%4th Party%'            
 --   OR [mwcInstitution].Company  like '%4thParty%'            
 --  ORDER BY Company          
 -- END        
             
END  
  
GO

