USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetFeesAndCharges_OLD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetFeesAndCharges_OLD]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   
-- =============================================    
-- Author:  <Kanva Patel>    
-- Create date: <23rd Aug 2012>    
-- Description: <This will get Fees & Charges Details for Docs On OLIE project step-3>    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_GetFeesAndCharges_OLD]    
(    
 @LoanNumber varchar(20)    
)    
AS    
BEGIN    

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

Select     
 LoanApp.LoanNumber,ClosingCost.HUDLinenum,ClosingCost.Payee,ClosingCost.BorrowerAmt,ClosingCost.BorrowerPOC,    
 LoanApp.ChannelType,ClosingCost.HUDDesc,cast(ClosingCost.HUDLinenum as money) as lineorder    
From     
 mwlloanapp LoanApp, mwlloandata LoanData, mwlclosingcost ClosingCost    
Where     
 LoanApp.ID = LoanData.objOwner_ID    
 And LoanData.ID = ClosingCost.loanDAta_ID    
 And LoanApp.LoanNumber = @LoanNumber    
 And cast(ClosingCost.HUDLinenum as money) >= 800    
 And cast(ClosingCost.HUDLinenum as money) <= 899    
Order By lineorder    
END    
GO

