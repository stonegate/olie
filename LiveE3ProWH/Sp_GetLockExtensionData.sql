USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_GetLockExtensionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_GetLockExtensionData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  <Author,,Name>      
-- Create date: <Create Date,,>      
-- Description: <Description,,>      
-- =============================================      
CREATE PROCEDURE [dbo].[Sp_GetLockExtensionData]       
 @LoanNumber varchar(400)      
AS      
BEGIN   

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
   
 select loanapp.ID as LoanappID,LoanNumber as loan_no,borr.lastname  + ' ' + borr.FirstName as BorrowerName,borr.EmailAddress,      
LockRecord.LockExpirationDate as LockExpirationDate,LenderClosingPrice  as TotalPrice,LockRecord.lockdays,LockRecord.Id as lockRecordid     
 from mwlLoanApp  loanapp        
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id AND BORR.SEQUENCENUM=1      
Left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id      
LEFT OUTER  JOIN mwlPricingResult as PricingResult ON PricingResult.ObjOwner_ID=LockRecord.ID    
and LockRecord.LockType='LOCK' and  LockRecord.Status<>'CANCELED'   
where     
LoanNumber=@LoanNumber      
END 
GO

