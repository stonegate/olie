USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_select_CountFileByUserid]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_select_CountFileByUserid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================  
-- Author:  Name  
-- Create date:   
-- Description:   
-- =============================================  
CREATE PROCEDURE [dbo].[sp_select_CountFileByUserid]
( 
@userId bigint, 
@fileName varchar(Max)
)  
AS  
BEGIN 
select count(*) TotalFile from tblLoanReg where userid =@userId and filename=@fileName
end

GO

