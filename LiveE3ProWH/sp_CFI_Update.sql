USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CFI_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CFI_Update]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ahishek
-- Create date: 10/18/2012
-- Description:	
-- =============================================
CREATE proc [dbo].[sp_CFI_Update]
(
@userid varchar(Max)
)
as
begin
update tblusers Set isactive = '1' Where userid = @userid
end



GO

