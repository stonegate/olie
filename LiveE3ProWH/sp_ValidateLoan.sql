USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ValidateLoan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ValidateLoan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--Example for execute stored procedure
--EXECUTE sp_ValidateLoan 9795,31,'0000353100'
--EXECUTE sp_ValidateLoan 7691,8,'0000366469'
--EXECUTE sp_ValidateLoan 1250,20,'0000306601'
-- =============================================

-- Author:  <Stonegate>
-- Create date: <06/4/2013>
-- Description: <This wil Check for Loan number exist in roles>
-- Modify Date : <12/4/2013>
-- Modify By :   Vipul
-- =============================================

CREATE PROCEDURE [dbo].[sp_ValidateLoan] (

--Parameter declaration
@UserID int,
@RoleID int,
@strLoanno varchar(100))

AS

BEGIN

   --Variable Declaration

   DECLARE @LoanStatus varchar(100)
   DECLARE @strRegion varchar(100)
   DECLARE @SQL nvarchar(max)
   DECLARE @SQL1 nvarchar(max)
   DECLARE @SQLFinal nvarchar(max)
   DECLARE @strCompanyEmail varchar(200)
   DECLARE @strID nvarchar(max)
   DECLARE @LoanNo varchar(7)

   --declaration of variable and assign meaningful name to role id and set value of that receieved from parameter

   --Start



   SELECT
      @LoanStatus = CONVERT(varchar(2), currentStatus)
   FROM mwlLoanApp
   WHERE LoanNumber = @strLoanno
   DECLARE @Admin int
   DECLARE @DooAdmin int --BRD-099
   SET @Admin = 0
   SET @DooAdmin = 38--BRD-099
   DECLARE @Broker int
   SET @Broker = 3
   DECLARE @Correspondent int
   SET @Correspondent = 20
   DECLARE @Hybrid int
   SET @Hybrid = 21


   DECLARE @BDM int
   SET @BDM = 2



   DECLARE @RegionalManager int
   SET @RegionalManager = 8

   DECLARE @UnderwriterTeamLead int
   SET @UnderwriterTeamLead = 31



   DECLARE @ClientSupportDesk int
   SET @ClientSupportDesk = 34



   DECLARE @CorporateCommunication int
   SET @CorporateCommunication = 35



   DECLARE @RegionalVPEast int
   SET @RegionalVPEast = 32

   DECLARE @RegionalVPWest int
   SET @RegionalVPWest = 33


   DECLARE @Underwriter int
   SET @Underwriter = 10


   DECLARE @UnderwriterMgr int
   SET @UnderwriterMgr = 11


   DECLARE @DirectorofWholeSale int
   SET @DirectorofWholeSale = 1



   DECLARE @DirectorofProduction int
   SET @DirectorofProduction = 12



   DECLARE @SalesDirector int
   SET @SalesDirector = 26

   DECLARE @DirectorofCFI int

   SET @DirectorofCFI = 25

   DECLARE @CorrespondentVendor int

   SET @CorrespondentVendor = 36

   --end of declaration of variable and assigment



   --Set blank string for string concatation.

   SET @SQL = ''

   SET @SQL1 = ''

   SET @SQLFinal = ''

   --end of assign string concatation



   --old loanno

   SELECT
      @LoanNo = SUBSTRING(CONVERT(varchar(7), @strLoanno), 1, 7)

   /*Print @strLoanno + ' ' + @LoanNo*/

   IF (@LoanNo LIKE '%000010%' OR @LoanNo = '0000011' OR @LoanNo = '0000012' OR @LoanNo = '0000013')

   BEGIN

      SELECT
         1

   END

   ELSE

   BEGIN


      IF (@RoleId = @DooAdmin) --BRD-099
      BEGIN
         SELECT
            1
      END --BRD-099
      --Fetching partner company id from Web site db based on user id receieved from parameter if role is broker, correspondent,Hybrid role



      ELSE
      IF (((@RoleID = @BDM

         OR @RoleID = @SalesDirector

         )

         AND (@LoanStatus = '54') OR (@RoleID = @UnderwriterMgr)





         AND (@LoanStatus = '25'))

         )

      BEGIN

         SELECT
            1

      END

      ELSE

      BEGIN

         IF (@RoleID = @Broker

            OR @RoleID = @Correspondent

            OR @RoleID = @Hybrid

            )

         BEGIN

            SELECT
               @strCompanyEmail = PartnerCompanyid

            FROM dbo.tblUsers

            WHERE userid = @UserID

         END



         IF (@RoleID = @BDM

            OR @RoleID = @SalesDirector

            OR @RoleID = @RegionalManager

            OR @RoleID = @UnderwriterTeamLead

            OR @RoleID = @ClientSupportDesk

            OR @RoleID = @CorporateCommunication

            OR @RoleID = @RegionalVPEast

            OR @RoleID = @RegionalVPWest

            OR @RoleID = @RegionalManager

            OR @RoleID = @Underwriter

            OR @RoleID = @UnderwriterMgr

            OR @RoleID = @CorrespondentVendor

            )

         BEGIN

            SELECT
               @strID = E3Userid

            FROM dbo.tblUsers

            WHERE userid = @UserID

         END

         /*
      
         -- base Query
      
         select count(Loannumber)as LoanCount
      
         from mwlLoanApp loanapp    with (NOLOCK)
      
         Left join dbo.mwlInstitution as insitution on insitution.ObjOwner_id = loanapp.id
      
         and insitution.InstitutionType=loanapp.ChannelType
      
         LEFT JOIN mwlUnderwritingSummary ON mwlUnderwritingSummary.LoanApp_ID = loanapp.ID
      
         where LoanNumber is not null and LoanNumber <>''
      
           */

         IF (@RoleID = @Admin



            )



         BEGIN

            SELECT
               COUNT(Loannumber) AS LoanCount

            FROM mwlLoanApp loanapp WITH (NOLOCK)

            LEFT JOIN dbo.mwlInstitution AS insitution
               ON insitution.ObjOwner_id = loanapp.id

               AND insitution.InstitutionType = loanapp.ChannelType

            LEFT JOIN mwlUnderwritingSummary
               ON mwlUnderwritingSummary.LoanApp_ID = loanapp.ID

            WHERE LoanNumber IS NOT NULL

            AND LoanNumber <> ''



            AND (LoanNumber = @strLoanno)

         END



         IF (@RoleID = @BDM

            OR @RoleID = @SalesDirector

            OR @RoleID = @ClientSupportDesk

            )

         BEGIN

            SELECT
               COUNT(Loannumber) AS LoanCount

            FROM mwlLoanApp loanapp WITH (NOLOCK)

            LEFT JOIN dbo.mwlInstitution AS insitution
               ON insitution.ObjOwner_id = loanapp.id

               AND insitution.InstitutionType = loanapp.ChannelType

            LEFT JOIN mwlUnderwritingSummary
               ON mwlUnderwritingSummary.LoanApp_ID = loanapp.ID

            WHERE LoanNumber IS NOT NULL

            AND LoanNumber <> ''

            AND (Originator_id IN (SELECT
               items

            FROM dbo.SplitString(@strID, ','))
            )

            AND (LoanNumber = @strLoanno)



         END

         IF (@RoleID = @Broker



            OR @RoleID = @Hybrid

            )

         BEGIN

            SELECT
               COUNT(Loannumber) AS LoanCount

            FROM mwlLoanApp loanapp WITH (NOLOCK)

            LEFT JOIN dbo.mwlInstitution AS insitution
               ON insitution.ObjOwner_id = loanapp.id

            --                                  AND insitution.InstitutionType = loanapp.ChannelType

            LEFT JOIN mwlUnderwritingSummary
               ON mwlUnderwritingSummary.LoanApp_ID = loanapp.ID

            WHERE LoanNumber IS NOT NULL

            AND LoanNumber <> ''

            --     AND ( insitution.CompanyEmail = @strCompanyEmail )

            AND (LoanNumber = @strLoanno)

         END

         IF (

            @RoleID = @Correspondent



            )

         BEGIN

            SELECT
               COUNT(Loannumber) AS LoanCount

            FROM mwlLoanApp loanapp WITH (NOLOCK)

            LEFT JOIN dbo.mwlInstitution AS insitution
               ON insitution.ObjOwner_id = loanapp.id

            -- AND insitution.InstitutionType = loanapp.ChannelType

            LEFT JOIN mwlUnderwritingSummary
               ON mwlUnderwritingSummary.LoanApp_ID = loanapp.ID

            WHERE LoanNumber IS NOT NULL

            AND LoanNumber <> ''

            --  AND ( insitution.CompanyEmail = @strCompanyEmail )

            AND (LoanNumber = @strLoanno)

         END



         IF (@RoleID = @RegionalManager)

         BEGIN

            SELECT
               @strRegion = Region

            FROM dbo.tblUsers

            WHERE urole = @RegionalManager

            AND userid = @UserID



            SELECT
               COUNT(Loannumber) AS LoanCount

            FROM mwlLoanApp loanapp WITH (NOLOCK)

            LEFT JOIN dbo.mwlInstitution AS insitution
               ON insitution.ObjOwner_id = loanapp.id

               AND insitution.InstitutionType = loanapp.ChannelType

            LEFT JOIN mwlUnderwritingSummary
               ON mwlUnderwritingSummary.LoanApp_ID = loanapp.ID

            WHERE LoanNumber IS NOT NULL

            AND LoanNumber <> ''

            AND (Originator_id IN (SELECT
               e3userid

            FROM dbo.tblUsers

            WHERE urole = 2

            AND region = @strRegion)

            OR (Originator_id IN (SELECT
               items

            FROM dbo.SplitString(@strID,

            ','))
            )

            )

            AND (LoanNumber = @strLoanno)

         END


         -- Condition added by tavant to enable director of production to see all loans in the view pipeline--Jira 12 Begin


         IF (@RoleID = @DirectorofProduction)

         BEGIN

            SELECT
               COUNT(Loannumber) AS LoanCount

            FROM mwlLoanApp loanapp WITH (NOLOCK)

            LEFT JOIN dbo.mwlInstitution AS insitution
               ON insitution.ObjOwner_id = loanapp.id

               AND insitution.InstitutionType = loanapp.ChannelType

            LEFT JOIN mwlUnderwritingSummary
               ON mwlUnderwritingSummary.LoanApp_ID = loanapp.ID

            WHERE LoanNumber IS NOT NULL

            AND LoanNumber <> ''

            AND (LoanNumber = @strLoanno)

         END
         -----------------------------------------------------Jira 12 Ends------------------------------------------------------------------------


         IF (@RoleID = @DirectorofWholeSale)

         BEGIN

            SELECT
               COUNT(Loannumber) AS LoanCount

            FROM mwlLoanApp loanapp WITH (NOLOCK)

            LEFT JOIN dbo.mwlInstitution AS insitution
               ON insitution.ObjOwner_id = loanapp.id

               AND insitution.InstitutionType = loanapp.ChannelType

            LEFT JOIN mwlUnderwritingSummary
               ON mwlUnderwritingSummary.LoanApp_ID = loanapp.ID

            WHERE LoanNumber IS NOT NULL

            AND LoanNumber <> ''

            AND (Originator_id IN (SELECT
               E3Userid

            FROM dbo.tblUsers

            WHERE urole IN (2, 8,26))
            )

            AND (LoanNumber = @strLoanno)

         END

         IF (@RoleID = @UnderwriterMgr)

         BEGIN

            SELECT
               COUNT(Loannumber) AS LoanCount

            FROM mwlLoanApp loanapp WITH (NOLOCK)

            LEFT JOIN dbo.mwlInstitution AS insitution
               ON insitution.ObjOwner_id = loanapp.id

               AND insitution.InstitutionType = loanapp.ChannelType

            LEFT JOIN mwlUnderwritingSummary
               ON mwlUnderwritingSummary.LoanApp_ID = loanapp.ID

            WHERE LoanNumber IS NOT NULL

            AND LoanNumber <> ''

            AND mwlUnderwritingSummary.underwriter_id IN (SELECT
               E3Userid

            FROM dbo.tblUsers

            WHERE urole IN (10) OR (mwlUnderwritingSummary.underwriter_id IN (SELECT
               items

            FROM dbo.SplitString(@strID,

            ','))
            ))

            AND (LoanNumber = @strLoanno)

         END







         IF (@RoleID = @Underwriter

            OR @RoleID = @UnderwriterTeamLead

            )

         BEGIN

            SELECT
               COUNT(Loannumber) AS LoanCount

            FROM mwlLoanApp loanapp WITH (NOLOCK)

            LEFT JOIN dbo.mwlInstitution AS insitution
               ON insitution.ObjOwner_id = loanapp.id

               AND insitution.InstitutionType = loanapp.ChannelType

            LEFT JOIN mwlUnderwritingSummary
               ON mwlUnderwritingSummary.LoanApp_ID = loanapp.ID

            WHERE LoanNumber IS NOT NULL

            AND LoanNumber <> ''

            AND mwlUnderwritingSummary.underwriter_id IN (SELECT
               items

            FROM dbo.SplitString(@strID, ','))

            AND (LoanNumber = @strLoanno)



         END

         IF (@RoleID = @DirectorofCFI)

         BEGIN

            SELECT
               COUNT(Loannumber) AS LoanCount

            FROM mwlLoanApp loanapp WITH (NOLOCK)

            LEFT JOIN dbo.mwlInstitution AS insitution
               ON insitution.ObjOwner_id = loanapp.id

               AND insitution.InstitutionType = loanapp.ChannelType

            LEFT JOIN mwlUnderwritingSummary
               ON mwlUnderwritingSummary.LoanApp_ID = loanapp.ID

            WHERE LoanNumber IS NOT NULL

            AND LoanNumber <> ''

            AND (Originator_id IN (SELECT

               E3Userid

            FROM dbo.tblUsers

            WHERE urole IN (26))
            )

            AND (LoanNumber = @strLoanno)



         END

         IF (@RoleID = @CorrespondentVendor)

         BEGIN

            SELECT
               COUNT(Loannumber) AS LoanCount

            FROM mwlLoanApp loanapp WITH (NOLOCK)

            LEFT JOIN dbo.mwlInstitution AS insitution
               ON insitution.ObjOwner_id = loanapp.id

               AND insitution.InstitutionType = loanapp.ChannelType

            LEFT JOIN mwlUnderwritingSummary
               ON mwlUnderwritingSummary.LoanApp_ID = loanapp.ID

            WHERE LoanNumber IS NOT NULL

            AND LoanNumber <> ''

            AND (documentPreparer_ID IN (SELECT
               items

            FROM dbo.SplitString(@strID, ','))
            )

            AND (LoanNumber = @strLoanno)



         END

         IF (@RoleID NOT IN (@DirectorofCFI, @Underwriter,

            @UnderwriterTeamLead, @RegionalManager,

            @Broker, @Correspondent, @Hybrid, @BDM,

            @SalesDirector, @ClientSupportDesk,

            @CorporateCommunication, @RegionalVPEast,

            @RegionalVPWest,

            @Admin, @CorrespondentVendor))

         BEGIN

            SELECT
               COUNT(Loannumber) AS LoanCount

            FROM mwlLoanApp loanapp WITH (NOLOCK)

            LEFT JOIN dbo.mwlInstitution AS insitution
               ON insitution.ObjOwner_id = loanapp.id

               AND insitution.InstitutionType = loanapp.ChannelType

            LEFT JOIN mwlUnderwritingSummary
               ON mwlUnderwritingSummary.LoanApp_ID = loanapp.ID

            WHERE LoanNumber IS NOT NULL

            AND LoanNumber <> ''

            AND (Originator_id IN (SELECT
               items

            FROM dbo.SplitString(@strID, ','))
            )

            AND (LoanNumber = @strLoanno)

         END

      END





   END

END

GO

