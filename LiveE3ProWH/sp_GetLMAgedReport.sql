USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLMAgedReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLMAgedReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Stonegate>
-- Create date: <10/9/2012>
-- Description:	<This will GetLMAgedReport>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLMAgedReport]
(
	@sCustCorrespodentType varchar(2000),
	@strFrom varchar(2000),
	@strID nvarchar(max)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

Declare @SQL nvarchar(max)
declare @SQL1 nvarchar(max)
set @SQL1 = ''
set @SQL=''
Set @SQL = 'Select ''E3'' as DB,loanapp.processorname,loanapp.DocumentPreparerName,loanapp.CurrDecStatusDate,LockRecord.LockDateTime,
loanapp.CurrDecStatusDate as Decisionstatusdate,loanapp.CurrPipeStatusDate as CurrentStatusDate,loanapp.ChannelType as ChannelType,
loanapp.DecisionStatus,OriginatorName,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,
CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,
appStat.StatusDateTime AS StatusDate,
 CASE TransType when NULL THEN '''' WHEN ''P'' THEN ''Purchase money first mortgage'' WHEN ''R'' THEN ''Refinance'' WHEN ''2'' THEN ''Purchase money second mortgage'' WHEN ''S'' THEN ''Second mortgage, any other purpose'' WHEN ''A'' THEN ''Assumption'' WHEN ''HOP'' THEN ''HELOC - other purpose'' WHEN ''HP'' THEN ''HELOC - purchase'' ELSE '''' END AS TransType, case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, LockRecord.LockExpirationDate,UnderwriterName as UnderWriter, CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(COffices.Company) ELSE RTRIM(Broker.Company) END as BrokerName,  CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,Loandata.LoanProgDesc as ProgramDesc ,Cust.stringvalue as UnderwriterType, CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'' and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0  WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'')  and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE (SELECT count(a.ID) as TotalCleared  FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') and a.objOwner_ID IN  (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER  ,
BOffices.Office as OFFICE_NAME1
From mwlLoanApp loanapp Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and Sequencenum=1 
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id
and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK'' and LockRecord.Status<>''CANCELED''
Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = loanapp.id and COffices.InstitutionType = ''CORRESPOND'' and COffices.objownerName=''Correspondent''
Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.InstitutionType = ''BRANCH'' and BOffices.objownerName=''BranchInstitution''
Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id
left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID ='''+ @sCustCorrespodentType +''' '

if (@strFrom = 'aged_LMDS')
Begin
	set @SQL = @SQL + ' where loanapp.currentstatus  IN (''04 - Disclosures Sent'') '
    set @SQL = @SQL + ' and loanapp.ChannelType in(''BROKER'') '
    set @SQL = @SQL + ' and (datediff(day,loanapp.CurrPipeStatusDate,getdate())>=10) '
End
else if (@strFrom = 'aged_LMAR')
Begin
	set @SQL = @SQL + ' where loanapp.currentstatus  IN (''09 - Application Received'') '
    set @SQL = @SQL + ' and loanapp.ChannelType in(''CORRESPOND'',''BROKER'') '
    set @SQL = @SQL + ' and (datediff(day,loanapp.CurrPipeStatusDate,getdate())>=5) '
End
else if (@strFrom = 'aged_LMAOH')
Begin
	set @SQL = @SQL + ' where loanapp.currentstatus  IN (''10 - Application on Hold'') '
    set @SQL = @SQL + ' and loanapp.ChannelType in(''CORRESPOND'',''BROKER'') '
    set @SQL = @SQL + ' and (datediff(day,loanapp.CurrPipeStatusDate,getdate())>=5) '
End
else if (@strFrom = 'aged_LMFI')
Begin
	set @SQL = @SQL + ' Where loanapp.currentstatus  IN (''13 - File Intake'') '
    set @SQL = @SQL + ' and loanapp.ChannelType in(''CORRESPOND'',''BROKER'') '
    set @SQL = @SQL + ' and (datediff(day,loanapp.CurrPipeStatusDate,getdate())>=10) '
End
else if (@strFrom = 'aged_LMSOH')
Begin
	set @SQL = @SQL + ' where loanapp.currentstatus IN (''17 - Submission On Hold'') '
    set @SQL = @SQL + ' and loanapp.ChannelType in(''CORRESPOND'',''BROKER'') '
    set @SQL = @SQL + ' and (datediff(day,loanapp.CurrPipeStatusDate,getdate())>=15) '
End
               
if (len(@strID) = 0)
Begin
	set @SQL1 = @SQL1 + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','',''))) '
End
else
Begin
	set @SQL1 = @SQL1 + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','',''))) '
End

set @SQL1 = @SQL1 + ' and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' '
set @SQL1 = @SQL1 + ' ORDER BY loanapp.CurrPipeStatusDate asc '

Print @SQL
--exec sp_executesql @SQL 
exec (@SQL + @SQL1)

END



GO

