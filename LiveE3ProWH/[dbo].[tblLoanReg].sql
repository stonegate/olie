USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanReg]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanReg]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanReg](	  [loanregID] INT NOT NULL IDENTITY(1,1)	, [borrLast] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [transType] INT NULL	, [loanAmt] NUMERIC NULL	, [lockStatus] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [loanPgm] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [estCloseDate] DATETIME NULL	, [userid] INT NULL	, [createdDate] DATETIME NULL DEFAULT(getdate())	, [filetype] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [filename] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Assigned] BIT NULL	, [AssignedBy] BIGINT NULL	, [AssignDate] DATETIME NULL	, [Completed] BIT NULL	, [CompletedBy] BIGINT NULL	, [TimeStamp] DATETIME NULL	, [IsActive] BIT NULL DEFAULT('True')	, [IsDeletedFromDocLog] BIT NULL DEFAULT('True')	, [LoanNumber] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblLoanReg] PRIMARY KEY ([loanregID] ASC))USE [HomeLendingExperts]
GO

