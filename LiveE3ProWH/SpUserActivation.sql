USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpUserActivation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpUserActivation]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpUserActivation]
	@UserId int
AS
BEGIN
update tblusers Set isactive = '1' where userid =@UserId
END

GO

