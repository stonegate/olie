USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetUserRoleBYId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetUserRoleBYId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nirav>
-- Create date: <10/4/2012>
-- Description:	<Fetch UserRole By UserId>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetUserRoleBYId] 
	@UserId int
AS
BEGIN
	Select urole from tblusers where userid in(@UserId)
END

GO

