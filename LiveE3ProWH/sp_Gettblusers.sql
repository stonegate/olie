USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Gettblusers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Gettblusers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/06/2012
-- Description:	Procedure for clspipelineE3 BLL
-- =============================================
CREATE procEDURE [dbo].[sp_Gettblusers]
(
@Region nvarchar(50)
)
AS
BEGIN
	
	SET NOCOUNT ON;

  select * From tblusers where Region=@Region and region is not null
END


GO

