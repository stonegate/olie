USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_BulkInsertOrUpdateLoanFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_BulkInsertOrUpdateLoanFile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE sp_BulkInsertOrUpdateLoanFile
(
	@BulkLoanFileId UNIQUEIDENTIFIER,
	@FileName VARCHAR(200) = NULL,
    @Type VARCHAR(50) = NULL,
	@ChannelType VARCHAR(1) = NULL,
    @Status VARCHAR(50) = NULL,
	@CompanyName VARCHAR(100) = NULL,
    @Comments VARCHAR(1000) = NULL,
    @ProcessStartedOn DATETIME = NULL,
    @CreatedBy VARCHAR(100) = NULL   
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS(SELECT * FROM [dbo].[tblBulkLoanFiles] with(NOLOCK) WHERE [BulkLoanFileId] = @BulkLoanFileId)
	BEGIN
    INSERT INTO [dbo].[tblBulkLoanFiles]
           ([BulkLoanFileId]
           ,[FileName]
           ,[Type]
		   ,[ChannelType]
           ,[Status]		   
		   ,[CompanyName]
           ,[Comments]
           ,[ProcessStartedOn]
           ,[CreatedBy]
		   ,[CreatedoN])
     VALUES
           (@BulkLoanFileId
           ,@FileName
           ,@Type
		   ,@ChannelType
           ,@Status		   
		   ,@CompanyName
           ,@Comments
           ,@ProcessStartedOn
           ,@CreatedBy
           ,getdate())
	END
	ELSE
	BEGIN
	UPDATE [dbo].[tblBulkLoanFiles]
	   SET [FileName] = ISNULL(@FileName,[FileName])
		  ,[Type] = ISNULL(@Type,[Type])
		  ,[ChannelType] = ISNULL(@ChannelType,[ChannelType])
		  ,[Status] = ISNULL(@Status,[Status])
		  ,[CompanyName] = ISNULL(@CompanyName,[CompanyName])
		  ,[Comments] = ISNULL(@Comments,Comments)
		   ,[ProcessStartedOn] = ISNULL(@ProcessStartedOn,ProcessStartedOn)
		  ,[ModifiedBy] = ISNULL(@CreatedBy,CreatedBy)
		  ,[ModifiedOn] = getdate()
	  WHERE [BulkLoanFileId] = @BulkLoanFileId
	END
END

GO

