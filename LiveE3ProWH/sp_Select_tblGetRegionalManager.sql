USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetRegionalManager]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetRegionalManager]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblGetRegionalManager]
(
@strStateId varchar(Max)
)
as
begin
select userid,ufirstname, ulastname, uemailid, urole, phonenum, photoname,mobile_ph 
from tblusers where branchid in (@strStateId) AND urole=8
end


GO

