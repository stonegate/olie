USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_InsertCategory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_InsertCategory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here
Create PROCEDURE [dbo].[SP_InsertCategory]
	(@CateName varchar(50))
AS
BEGIN
	Insert into tblCategories(CateName) Values(@CateName)
END



-- Create Procedure Coding ENDS Here

GO

