USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpUpdateActiveDeActiveNewsAnnouncement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpUpdateActiveDeActiveNewsAnnouncement]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpUpdateActiveDeActiveNewsAnnouncement]
@Id varchar(500),
@bActive int
AS
BEGIN
	UPDATE tblNewsAndAnnouncement SET IsActive=@bActive where ID IN (@Id)

END

GO

