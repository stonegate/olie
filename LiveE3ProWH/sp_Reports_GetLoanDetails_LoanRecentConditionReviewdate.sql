USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Reports_GetLoanDetails_LoanRecentConditionReviewdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Reports_GetLoanDetails_LoanRecentConditionReviewdate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================                  
-- Author:  Ratan Gohil 
-- Create date: October, 28 2013                  
-- Description: This will give Recent LoanConditionReviewdate for the loan number 
-- =============================================                  
         
 CREATE      
 PROCEDURE [dbo].[sp_Reports_GetLoanDetails_LoanRecentConditionReviewdate]                  
(                  
@LoanNumber nvarchar(50),              
@CustomeFieldConditionReviewDate nvarchar(50),   
@CustomeFieldPTPSubsequentReviewDate1 varchar(50),
@CustomeFieldPTPSubsequentReviewDate2 varchar(50),
@CustomeFieldPTPSubsequentReviewDate3 varchar(50)            
)                  
AS                  
BEGIN                  
SET NOCOUNT ON; 
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
SET XACT_ABORT ON;                 
select Top 1 DateValue from mwlcustomfield Inner Join mwlLoanApp on mwlLoanApp.ID = mwlcustomfield.LoanApp_ID 
where 
 (mwlcustomfield.custfielddef_id = @CustomeFieldConditionReviewDate 
	OR mwlcustomfield.custfielddef_id = @CustomeFieldPTPSubsequentReviewDate1 
	OR mwlcustomfield.custfielddef_id = @CustomeFieldPTPSubsequentReviewDate2 
	OR mwlcustomfield.custfielddef_id = @CustomeFieldPTPSubsequentReviewDate3)
and mwlLoanApp.LoanNumber=@LoanNumber
ORDER BY DateValue DESC               
               
END 


GO

