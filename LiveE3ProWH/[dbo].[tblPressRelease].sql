USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPressRelease]') AND type in (N'U'))
DROP TABLE [dbo].[tblPressRelease]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPressRelease](	  [id] INT NOT NULL IDENTITY(1,1)	, [PressReleaseContent] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PressReleaseDate] DATE NULL	, [PressReleasePath] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL)USE [HomeLendingExperts]
GO

