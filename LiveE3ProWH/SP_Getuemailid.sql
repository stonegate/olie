USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Getuemailid]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Getuemailid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Getuemailid]  
 (  
@userid int  
)  
AS  
BEGIN  
select uemailid from tblusers where userid=@userid  
END   
  
GO

