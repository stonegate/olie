USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetProBorrowerByLoanNo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetProBorrowerByLoanNo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetProBorrowerByLoanNo] 
@RecId varchar(500)	
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	select LastName AS borr_last from mwlBorrower where LoanAPP_ID in(Select Id from mwlloanapp where LoanNumber=@RecId)
END

GO

