USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLockActivityReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLockActivityReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Stonegate>
-- Create date: <10/5/2012>
-- Description:	<This will Get Lock Activity Report>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLockActivityReport] 
(
	@strID varchar(2000),
	@oPuid varchar(2000)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)

Declare @SQL nvarchar(max)
Set @SQL = ''
Set @SQL ='SELECT ''E3'' as DB,LD2.PROCESSOR, pif.prog_code , loandat.record_id, loan_no,
RTRIM(cobo.borr_last)+ '', '' + RTRIM(cobo.borr_first)  AS BorrowerLastName, lookupprogram.descriptn as LoanType, 
lookups.descriptn AS Purpose,proginfo.prog_desc, LD2.doc_funded_date AS FundedDate, loandat.int_rate, 
convert(varchar(40),locks.lock_exp_date,101) as LockExpires,convert (varchar(40),locks.LOCK_DATE,101) as LockDate, lt_loan_stats AS LoanStatus, 
convert(varchar(35),loandat.UW_RECVD_DATE,101) AS ReceivedInUW,
case LU_FINAL_HUD_STATUS when '''' then ''Pending'' when NULL then ''Pending'' else ''Approved'' end as LU_FINAL_HUD_STATUS, Loan_amt, brokers.brok_name as BrokerName
FROM loandat INNER JOIN cobo ON loandat.record_id = cobo.parent_id LEFT OUTER JOIN locks ON loandat.record_id = locks.parent_id
LEFT OUTER JOIN brokers ON loandat.lt_broker = brokers.record_id LEFT OUTER JOIN users ON lt_usr_underwriter = users.record_id LEFT OUTER JOIN proginfo ON loandat.lt_program = proginfo.record_id
INNER JOIN lookups ON loandat.lu_purpose = lookups.id_Value and lookups.lookup_id = ''AAE'' INNER JOIN lookups as lookups2 ON loandat.lu_loan_type = lookups2.id_Value and lookups2.lookup_id = ''ADC''
left outer JOIN lookups AS lookups3 ON loandat.lu_loan_type = lookups3.id_value AND lookups3.lookup_id = ''ACY'' INNER JOIN lookups AS lookupprogram ON loandat.lu_loan_type = lookupprogram.id_value 
AND lookupprogram.lookup_id = ''AAC''
INNER JOIN lookups AS lookupsLock ON locks.LU_LOCK_STAT = lookupsLock.id_value AND lookupsLock.lookup_id = ''AAT''
left join loandat2 as LD2 on loandat.record_id=LD2.record_id
left join proginfo as PIF on loandat.record_id = PIF.record_id '
If (len(@strID) = 0)
Begin
	Set @SQL = @SQL + ' where lt_loan_stats  IN (''Locked'','''') '
    Set @SQL = @SQL + ' and (loandat.lt_broker in (select * from dbo.SplitString('''+@strID+''','',''))) or loandat2.lt_usr_lorep in (select * from dbo.SplitString('''+@oPuid+''','',''))) '
End
else
Begin
	Set @SQL = @SQL + ' where lt_loan_stats  IN (''Locked'','''') '
    Set @SQL = @SQL + ' and (loandat.lt_broker in (select * from dbo.SplitString('''+@strID+''','',''))) or loandat2.lt_usr_lorep in (select * from dbo.SplitString('''+@oPuid+''','','')))) '
End
Print @SQL 
exec sp_executesql @SQL

END


GO

