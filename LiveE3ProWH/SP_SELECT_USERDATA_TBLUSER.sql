USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SELECT_USERDATA_TBLUSER]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SELECT_USERDATA_TBLUSER]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================      
-- AUTHOR:  <RAJKUMAR>      
-- CREATE DATE: <4/9/2013>      
-- DESCRIPTION: <SELECT USER DATA>      
-- =============================================      
-- Test Data    
-- Exec SP_SELECT_USERDATA_TBLUSER 121    
CREATE PROCEDURE [dbo].[SP_SELECT_USERDATA_TBLUSER]    
@USERID INT       
AS      
BEGIN      
SET NOCOUNT ON;      
  --THIS OPTION HAS THE SAME EFFECT AS SETTING NOLOCK ON ALL TABLES IN ALL SELECT STATEMENTS IN A TRANSACTION (RECOMMENDED BY HARLAND)      
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;       
  -- WHEN SET XACT_ABORT IS ON, IF A TRANSACT-SQL STATEMENT RAISES A RUN-TIME ERROR,       
  SET XACT_ABORT ON;             
  -- THE ENTIRE TRANSACTION IS TERMINATED AND ROLLED BACK.     
                  
  --QUERY USED FOR GETTING DETAIL OF INDIVIDUAL USER BY USING HIS USERID                      
 SELECT  userid, ufirstname, ulastname, uemailid, urole, isadmin, isactive, uidprolender,     
 uparent, upassword, maxloginattempt, lastlogintime, mobile_ph, loginidprolender,     
 carrierid, experience, bio, areaofexpertise, phone, fax, [address],     
 city, [state], zip, phonenum, photos, facebookid, twiterid, Address2, photoname, logoname, TwitterID,     
 BrokerID, BranchID, companyname, AEID, MI, ClientTestimonial, Region, StateBelongTo, E3Userid,     
 PartnerCompanyid, BrokerType, SignedUpDate, PasswordExpDate,     
 chkRegisterloan, chkQuickPricer, chkpricelockloan, chkSubmitloan, chkSubmitcondition,     
 chkorderappraisal, chkorderfhano, chkscheduleclosingdate, chkupdateloan,     
 chkaccuratedocs, IsUserInNewProcess, userloginID, OrgSystem, chkSubmitClosingPackage,     
 PartnerType, CreatedBy, E3Office, chkOrderTaxTranscript,     
 UWTeamleadid, isPublish, IsKatalyst, IsFouthPartyOrigination    
 FROM         tblUsers    
 WHERE     (USERID = @USERID)     
          
END 



GO

