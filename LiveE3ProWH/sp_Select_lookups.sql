USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_lookups]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_lookups]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		 Nitin
-- Create date: 10/04/2012
-- Description:	Procedure for clsloanRegister.cs bll  
-- =============================================
CREATE Proc [dbo].[sp_Select_lookups]
as
begin

select '0' as id_value, '--Select LockStatus--' as descriptn union  select id_value ,descriptn from lookups where lookup_id='AAT' and descriptn in('REGISTERED','LOCKED')
end

GO

