USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_lookups]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_lookups]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		 Nitin
-- Create date: 10/04/2012
-- Description:	Procedure for clsloanRegister.cs bll  
-- =============================================
Create Proc [dbo].[sp_Get_lookups]
(@id_value char(3))
as
begin

select id_value ,descriptn from lookups where lookup_id='AAT' and id_value=@id_value
end

GO

