USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_select_GetLoanDocRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_select_GetLoanDocRegister]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================  
-- Author:  Name  
-- Create date:   
-- Description:   
-- =============================================  
CREATE PROCEDURE [dbo].[sp_select_GetLoanDocRegister]
(  
@loanregId varchar(Max)
)  
AS  
BEGIN 
select loanregid as loanregid,filename as DocName  from tblloanreg 
where loanregid =@loanregId 
union select uloanregid as loanregid,DocName as DocName 
 from tblloandoc where uloanregid =@loanregId
END  

GO

