USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblEscrowDueDates]') AND type in (N'U'))
DROP TABLE [dbo].[tblEscrowDueDates]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEscrowDueDates](	  [LoanNumber] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [EscrowItem] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [DueDate] DATETIME NULL	, [Order] INT NOT NULL	, [CreatedBy] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedDate] DATETIME NULL	, [ModifiedBy] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ModifiedDate] DATETIME NULL	, CONSTRAINT [PK_tblEscrowDueDates_1] PRIMARY KEY ([LoanNumber] ASC, [EscrowItem] ASC, [Order] ASC))USE [HomeLendingExperts]
GO

