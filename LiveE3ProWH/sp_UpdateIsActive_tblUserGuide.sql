USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateIsActive_tblUserGuide]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateIsActive_tblUserGuide]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
        
CREATE proc [dbo].[sp_UpdateIsActive_tblUserGuide]      
(@ID int      
)      
as      
begin      
      
UPDATE tblUserGuide  WITH (UPDLOCK) SET IsActive=0 where ID<>@ID      
end 

GO

