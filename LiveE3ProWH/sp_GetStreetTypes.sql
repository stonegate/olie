USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStreetTypes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStreetTypes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nitin>
-- Create date: 
-- Description:	<Procedure for clsOrderFHAE3>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetStreetTypes]
AS
BEGIN

	SET NOCOUNT ON;

  SELECT * FROM tblStreetType ORDER BY [Type]
END


GO

