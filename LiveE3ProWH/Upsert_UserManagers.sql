USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Upsert_UserManagers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Upsert_UserManagers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author, Somnath M. R.>
-- Create date: <Create Date, 09th Dec 2013>
-- Description:	<Description, Insert into tblUserManagers table>
-- EXEC Upsert_UserManagers '16521', 16524, 1
-- =============================================
CREATE PROCEDURE [dbo].[Upsert_UserManagers]
	@UserIds NVARCHAR(MAX),
	@ManagerId INT,
	@CreatedBy [NVARCHAR](50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	INSERT INTO [dbo].[tblUserManagers] ([UserId], [ManagerId], [CreatedBy], [CreatedOn])
		SELECT Id.Items, @ManagerId, @CreatedBy, GetDate() FROM [dbo].SplitString(@UserIds, ',') Id
		WHERE 
			Id.Items NOT IN (SELECT UM.UserId FROM [dbo].[tblUserManagers] UM WHERE UM.ManagerId = @ManagerId)

	DELETE FROM [dbo].[tblUserManagers]
	WHERE
		ManagerId = @ManagerId 
		AND UserId NOT IN (SELECT Id.Items FROM [dbo].SplitString(@UserIds, ',') Id)
END

GO

