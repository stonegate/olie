USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDOOStepChangeStatus]') AND type in (N'U'))
DROP TABLE [dbo].[tblDOOStepChangeStatus]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDOOStepChangeStatus](	  [RowId] BIGINT NOT NULL IDENTITY(1,1)	, [LoanNumber] VARCHAR(30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [Step1] INT NULL	, [Step2] INT NULL	, [Step3] INT NULL	, [Step4] INT NULL	, [Step5] INT NULL	, [Step6] INT NULL	, [Step7] INT NULL	, [Status] VARCHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedDate] DATETIME NULL	, [CreatedBy] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ModifiedBy] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ModifiedDate] DATETIME NULL	, CONSTRAINT [PK_tblDOOStepChangeStatus] PRIMARY KEY ([RowId] ASC))USE [HomeLendingExperts]
GO

