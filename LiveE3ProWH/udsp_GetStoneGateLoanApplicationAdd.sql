USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetStoneGateLoanApplicationAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetStoneGateLoanApplicationAdd]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		< author name >
-- Create date: 25/10/2012
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[udsp_GetStoneGateLoanApplicationAdd] 
@brokerID varchar(50)
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT userid, ufirstname, ulastname, uemailid, urole FROM tblUsers where userid = @brokerID
END

GO

