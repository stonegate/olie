USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpFetchBrokerUserData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpFetchBrokerUserData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nirav>
-- Create date: <10/04/2012>
-- Description:	<Fetch Broker User data>
-- =============================================
CREATE PROCEDURE [dbo].[SpFetchBrokerUserData] 
	@CompanyName varchar(500)
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	select top 1 isnull(companyEmail,'')companyEmail   from mwcinstitution where Status='Active' and company like '%' + @CompanyName + '%'
END

GO

