USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetParterType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetParterType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetParterType] 
@strParterCompanyid varchar(500)
AS
BEGIN
	select * from tblusers where PartnerType is not null and PartnerCompanyid =@strParterCompanyid
END

GO

