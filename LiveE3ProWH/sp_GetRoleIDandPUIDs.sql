USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetRoleIDandPUIDs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetRoleIDandPUIDs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================
-- Author:		Kanva
-- Create date: <10/3/2012>
-- Description:	<This will get RoleID and oPUID for given UserID>
-- =================================================================
CREATE PROCEDURE [dbo].[sp_GetRoleIDandPUIDs]
(
@UserID int
)
AS
BEGIN
Declare @RoleID int
	
Select urole,PartnerCompanyid from tblusers where userid = @UserID

Select uidprolender from tblusers where userid = @UserID
	
END

GO

