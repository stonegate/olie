USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Update_tblUpdateProfileOLD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Update_tblUpdateProfileOLD]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[usp_Update_tblUpdateProfileOLD]
(
@ufirstname varchar(Max)
,@ulastname varchar(Max)
,@uemailid varchar(Max)
,@experience varchar(max)
,@StateBelongTo varchar(Max)
,@mobile_ph varchar(Max)
,@phonenum varchar(Max)
,@address varchar(Max)
,@address2 varchar(Max)
,@city varchar(Max)
,@state varchar(Max)
,@zip varchar(Max)
,@fax varchar(Max)
,@photoname varchar(max)
,@logoname varchar(Max)
,@facebookID varchar(Max)
,@TwitterID varchar(Max)
,@BrokerID varchar(Max)
,@bio varchar(Max)
,@AreaofExpertise varchar(Max)
,@BranchID varchar(Max)
,@MI varchar(Max)
,@ClientTestimonial varchar(Max)
,@Userid varchar(Max)
)
as
begin
UPDATE tblUsers
   SET ufirstname=@ufirstname
,ulastname=@ulastname
,uemailid=@uemailid
,experience=@experience
,StateBelongTo=@StateBelongTo
,mobile_ph=@mobile_ph
,phonenum=@phonenum
,address=@address
,address2=@address2
,city=@city
,state=@state
,zip=@zip
,fax=@fax
,photoname=@photoname
,logoname=@logoname
,facebookID=@facebookID
,TwitterID=@TwitterID
,BrokerID=@BrokerID
,bio=@bio
,AreaofExpertise=@AreaofExpertise
,BranchID=@BranchID
,MI=@MI
,ClientTestimonial=@ClientTestimonial
where userid=@Userid
end


GO

