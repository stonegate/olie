USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteUserPermanent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteUserPermanent]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Stonegate
-- Create date: 10/05/2012
-- Description:	Get Activation Status
-- =============================================
CREATE PROCEDURE sp_DeleteUserPermanent
(
@UserId int
)
as
begin

Delete From tblusers Where userid =@UserId

end

GO

