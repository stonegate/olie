USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetProBorrowerByLoanNo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetProBorrowerByLoanNo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sushant>
-- Create date: <10/10/2012>
-- Description:	<For ClsUserE3.cs(Retail) >
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProBorrowerByLoanNo]
	-- Add the parameters for the stored procedure here
        
@LoanNumber varchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

-- Insert statements for procedure here
select LastName AS borr_last from mwlBorrower where LoanAPP_ID in(Select Id from mwlloanapp where LoanNumber=@LoanNumber)

 
	
END


GO

