USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetStoneGateUsersDataForRSSFeed]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetStoneGateUsersDataForRSSFeed]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Gourav  
-- Create date: 10/03/2012  
-- Description: Procedure for clsUser.cs bll    
-- =============================================  
CREATE proc [dbo].[sp_Select_tblGetStoneGateUsersDataForRSSFeed]
as  
begin  
SELECT tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid,
 tblUsers.urole, tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent, tblUsers.upassword,
 tblUsers.maxloginattempt, tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender, 
tblUsers.carrierid, Carrier.Carrier,tblRoles.Roles FROM tblUsers LEFT OUTER JOIN Carrier
 ON tblUsers.carrierid = Carrier.ID LEFT OUTER JOIN tblRoles ON tblUsers.urole = tblRoles.ID 
order by  tblUsers.userid desc for xml path('Users'),elements,root('Userlist')
end  


GO

