USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_HeirarchicalUserReportees]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Get_HeirarchicalUserReportees]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Get_HeirarchicalUserReportees]

(
    @managerid varchar(MAX)
)

AS

BEGIN

	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	;WITH MyCTE

	AS ( 

			SELECT u1.userid,u1.E3userid

			FROM tblusermanagers m1 

			INNER JOIN  tblusers u1 on u1.userid = m1.userid 

				and m1.managerid in (SELECT * FROM dbo.splitstring(@managerid,','))

			INNER JOIN tblroles r1 on u1.urole = r1.id 

			UNION ALL 

			SELECT u2.userid,u2.E3userid

			FROM tblusermanagers m2 

			INNER JOIN  tblusers u2 on u2.userid = m2.userid

			INNER JOIN MyCTE on m2.managerid = MyCTE.userid

			INNER JOIN tblroles r2 on u2.urole = r2.id 

			WHERE  m2.managerid NOT IN (SELECT * FROM dbo.splitstring(@managerid,','))

		)SELECT DISTINCT E3userid FROM MyCTE

END
GO

