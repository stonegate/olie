USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Update_ConditionStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Update_ConditionStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  <Vipul Thacker>    
-- Create date: <21-03-2012>    
-- Description: This stored procedure used for update condition status
-- =============================================   
  
create PROCEDURE [dbo].[SP_Update_ConditionStatus]  
   
@ConditionID VARCHAR(50)  
AS  
   
 
BEGIN  
 
  
 SET NOCOUNT ON;  
 SET XACT_ABORT ON;         -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,   
              -- the entire transaction is terminated and rolled back.  
  
update mwlConditionState  WITH (UPDLOCK)   set STATE='SUBMITTED', StateDateTime = getDate() 
where OBJOWNER_ID =@ConditionID;

UPDATE mwlCondition  WITH (UPDLOCK) SET CURRENTSTATE='SUBMITTED' where ID=@ConditionID
   
END  

GO

