USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Fetch_UserRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Fetch_UserRole]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

CREATE PROCEDURE [dbo].[SP_Fetch_UserRole]
	(
@refuserid int,
@tempuserid varchar(150),
@temppswd varchar(150),
@createddate datetime
)
AS
BEGIN
INSERT INTO tblTempUser(refuserid,tempuserid,temppswd,createddate) 
Values(@refuserid,@tempuserid,@temppswd,@createddate) 
END	


GO

