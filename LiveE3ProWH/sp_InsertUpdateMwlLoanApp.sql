USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertUpdateMwlLoanApp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertUpdateMwlLoanApp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Author,,Name>    
-- Create date: <Create Date,,>    
-- Description: <Description,,>    
-- =============================================    
CREATE PROCEDURE sp_InsertUpdateMwlLoanApp    
(    
 @LoanNumber varchar(20),    
 @Status varchar(2000)     
)    
AS    
BEGIN    
Declare @LoanAppID varchar(200)        
    set @LoanAppID = (select id from mwlloanapp where loannumber=@LoanNumber)    
    
if not exists(select * From mwlappstatus where LoanApp_id in(@LoanAppID) and StatusDesc=@Status)    
  insert into mwlappstatus(ID,LoanApp_id,StatusDesc,StatusDateTime)values    
        (replace(newid(),'-',''),@LoanAppID,@Status,dateadd(n,2, getdate()))    
    update mwlloanapp set CurrentStatus=@Status,CurrPipeStatusDate=dateadd(n,2, getdate()) where loannumber=@LoanNumber    
END
GO

