USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertmwlAppStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertmwlAppStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
             
-- =============================================                    
-- Author:  Nitin                    
-- Create date: 10/07/2012                    
-- Description: Procedure for clspipelineE3 BLL       
--Modify by : Vipul Thacker                   
-- =============================================                    
CREATE PROCEDURE [dbo].[sp_InsertmwlAppStatus]                    
(                    
@ID varchar(200),  --added by SMC 12/6/12    ,        
@LoanApp_ID varchar(200),                    
@StatusDesc varchar(200)                   
)                    
AS                    
BEGIN                    
                     
 SET NOCOUNT ON;                    
      
if not exists(select * From mwlAppStatus where LoanApp_ID =@LoanApp_ID and StatusDesc=@StatusDesc)      
BEGIN      
INSERT INTO mwlAppStatus                     
(ID,LoanApp_ID,StatusDesc,StatusDateTime)                    
 VALUES                    
(replace(newid(),'-',''),@LoanApp_ID,@StatusDesc,getdate())                    
END      
ELSE      
BEGIN      
UPDATE mwlAppStatus SET StatusDesc=@StatusDesc WHERE LoanApp_ID =@LoanApp_ID AND StatusDesc=@StatusDesc      
END      
                  
END 
GO

