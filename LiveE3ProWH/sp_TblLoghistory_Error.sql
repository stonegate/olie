USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TblLoghistory_Error]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_TblLoghistory_Error]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[sp_TblLoghistory_Error](
@IPaddress VARCHAR(50),
@ErrorMsg VARCHAR(1000)


)
AS
BEGIN
 INSERT INTO tblLoghistory (IPaddress,ErrorMsg
  ) VALUES
(@IPaddress,@ErrorMsg)
  
 
END

GO

