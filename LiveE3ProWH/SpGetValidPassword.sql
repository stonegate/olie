USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetValidPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetValidPassword]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetValidPassword] 
	@EmailId varchar(500)

AS
BEGIN
	select userid from tblusers where uemailid =@EmailId
END

GO

