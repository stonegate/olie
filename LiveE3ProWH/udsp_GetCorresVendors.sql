USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetCorresVendors]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetCorresVendors]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Example for execute stored procedure       
--EXECUTE [udsp_GetCorresVendors]  36
-- =============================================      
-- Author:  Amit Saini      
-- Create date: 06/03/2013   
-- Name:udsp_GetCorresVendors.sql
-- Description: Get Correspondent Vendors Name List.     
-- =============================================           
CREATE PROCEDURE [dbo].[udsp_GetCorresVendors]    
(
 --Parameter declaration 
@userRoleID int
)    
AS          
BEGIN   
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)  
SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,         
  
SELECT Distinct UPPER(rtrim(ufirstname) + ' ' + ulastname) as CorrespondentVendor FROM tblUsers 
WHERE urole = @userRoleID AND isactive=1 order by CorrespondentVendor
  
END          
    


GO

