USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_UpdateRecordAfterAddImageUpload]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_UpdateRecordAfterAddImageUpload]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

Create PROCEDURE [dbo].[SP_UpdateRecordAfterAddImageUpload]
	(
@ctid int
)
AS
BEGIN
 update tblAdditionalUWDoc set isCommited = 1 where ctid = @ctid
             
END

GO

