USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_UpdateRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_UpdateRole]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		< author name >
-- Create date: 25/10/2012
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[udsp_UpdateRole] 
@userID varchar(20),
@uRole varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

update tblusers Set urole = @uRole Where userid = @userID	
END

GO

