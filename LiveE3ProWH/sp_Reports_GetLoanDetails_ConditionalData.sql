USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Reports_GetLoanDetails_ConditionalData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Reports_GetLoanDetails_ConditionalData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================          
-- Author:  Stonegate          
-- Create date: April, 18 2012          
-- Description: This will give Loan details for the loan number passed          
--sp_Reports_GetLoanDetails_ConditionalData('0000308185')        
--exec sp_Reports_GetLoanDetails_ConditionalData '0000308185'        
-- =============================================          
CREATE PROCEDURE [dbo].[sp_Reports_GetLoanDetails_ConditionalData]        
(        
      
@LoanNumber nvarchar(50)      
)        
AS        
BEGIN 

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
       
SET FMTONLY OFF      
 Declare @StatusDesc nvarchar(200)        
Declare @CurrentStat nvarchar(200)        
Declare @iDecisionStatus int        
  Declare @lNmuber varchar(50)      
set @lNmuber = @LoanNumber      
Set @iDecisionStatus = 0        
select @StatusDesc =AppStatus.StatusDesc,@CurrentStat=CurrentStatus from mwlloanapp loanapp Left join mwlApprovalStatus AppStatus on loanapp.ID= AppStatus.LoanApp_id where loannumber= @LoanNumber        
Print 'status Desc: ' + @StatusDesc        
If @StatusDesc <> '' Or @StatusDesc is null        
Begin        
 set @iDecisionStatus = 1        
End        
Declare @Query nvarchar(2000)        
IF @iDecisionStatus = 1        
Begin        
 Set @Query = 'SELECT Name as descriptn,DisplayOrder as id_value ,case DisplayOrder when ''4'' then ''1''  when ''1'' then ''2'' when ''3'' then ''4''  when ''2'' then ''3'' when ''5'' then ''5'' END AS ''Value''        
 FROM mwsConditionDueBy '        
    --If @CurrentStat <> '54 - Purchase Pending'        
    --Begin        
  --set @Query = @Query +' where Name <> ''Prior to Purchase'' '        
 --End        
End        
Else        
Begin        
 Set @Query = 'SELECT Name as descriptn,DisplayOrder as id_value ,case DisplayOrder when ''4'' then ''1''  when ''1'' then ''2'' when ''3'' then ''4''  when ''2'' then ''3'' when ''5'' then ''5'' END AS ''Value''        
 FROM mwsConditionDueBy where Name = ''Prior to Underwriting'' '        
End        
        
set @Query = '  select ''E3'' as DB,OrderNum as cond_no,a.ID as record_id,a.ObjOwner_Id as parent_id,a.Description as cond_text,        
     CurrentState as STATUS,isnull(Category,'''') as descriptn ,DueBy,         
     a.CreatedOnDate as cond_cleared_date,'''' as  cond_recvd_date, b.DisplayOrder as id_value ,a.ShortName         
     FROM dbo.mwlCondition a         
     left outer join mwsConditionDueBy b on a.Dueby = b.Name         
     Where '        
        
--IF @iDecisionStatus = 1        
--Begin        
 --If @CurrentStat <> '54 - Purchase Pending'        
    --Begin        
  --set @Query = @Query +'  DueBy <> ''Prior to Purchase'' and  '        
 --End        
--End        
--Else        
--Begin        
 --set @Query = @Query +'   DueBy = ''Prior to Underwriting'' and    '         
--End        
set @Query = @Query + 'a.ObjOwner_Id IN ( SELECT id FROM dbo.mwlLoanApp WHERE loannumber= @LoanNumber ) ORDER BY OrderNum asc '        
        
print @Query        
  --EXECUTE  (@Query)      
EXECUTE sp_executesql @Query, N'@LoanNumber nvarchar(50)', @lNmuber      
END

GO

