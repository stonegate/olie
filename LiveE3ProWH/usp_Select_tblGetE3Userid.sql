USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tblGetE3Userid]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tblGetE3Userid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[usp_Select_tblGetE3Userid]
(
@iUserId varchar(50)
)
as
begin
	select isnull(e3userid,'') as  e3userid  from tblusers where userid =@iUserId
end


GO

