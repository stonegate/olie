USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Reports_GetLoanDetails_TitleCompanyDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Reports_GetLoanDetails_TitleCompanyDetails]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Stonegate  
-- Create date: April, 18 2012  
-- Description: This will give Loan details for the loan number passed  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_Reports_GetLoanDetails_TitleCompanyDetails]  
(  
@LoanNumber nvarchar(50)
)  
AS  
BEGIN  

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

 select *from mwlInstitution where InstitutionType ='Title' and ObjOwner_id  in(Select id from MwlLoanapp where LoanNumber=@LoanNumber)  
END  
  
  
GO

