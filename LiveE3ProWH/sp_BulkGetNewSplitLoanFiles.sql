USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_BulkGetNewSplitLoanFiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_BulkGetNewSplitLoanFiles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE sp_BulkGetNewSplitLoanFiles
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    BEGIN
	
	SELECT [FileName],
			[Type],
			[ChannelType],
            [CreatedBy],
			[CompanyName]
			FROM [dbo].[tblBulkSplitLoanFiles] WHERE [STATUS] = 'NEW'
	
	UPDATE [dbo].[tblBulkSplitLoanFiles]
	   SET [Status] = 'IN PROCESS'		  
		  ,[ModifiedBy] = 'WS'
		  ,[ModifiedOn] = GetDate()
	  WHERE [STATUS] = 'NEW'
	  
	END
END

GO

