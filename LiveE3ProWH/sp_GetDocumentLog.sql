USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDocumentLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDocumentLog]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Stonegate    
-- Create date: 20th April 2012    
-- Description: This will get document log    
-- ActionID = 1 Get Document log data    
-- ActionID = 2 Delete (Update) selected document log records    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_GetDocumentLog]    
(    
@iNoOfDays int,    
@ActionID int,    
@LoanID nvarchar(50),    
@CondID nvarchar(50),    
@SchedID nvarchar(50),    
@LoanAndCompID nvarchar(50)    
)    
AS    
BEGIN    
IF @ActionID = 1    
Begin    
 select loanregid as 'loanregid','' as  'loandocid',''as Condid,''as 'SchID', FileName as 'AllFileName',  
 createdDate as 'createdDate','' as 'Description',lreg.Userid as 'Userid',  
 uemailid as Submittedby  ,uFirstName + ' ' + ulastname as OlieName from tblloanreg lreg    
 left outer join tblusers ureg on ureg.userid=lreg.userid where urole in(3,25,26,20,21,0) and FileName <> ''   
 and  (lreg.isDeletedFromDoclog is NULL or lreg.isDeletedFromDoclog=1) and createdDate   >= (getdate()-@iNoOfDays)   
 and createdDate <=getdate()     
 union     
 select '' as 'loanregid',loandocid as  'loandocid',''as Condid,''as 'SchID', DocName as 'AllFileName',createdDate as 'createdDate',  
 Comments as 'Description',  
 ldoc.Userid as 'Userid',uemailid as Submittedby  ,uFirstName + ' ' + ulastname as OlieName   
 from tblloandoc ldoc left outer join tblusers udoc on ldoc.userid=udoc.userid where urole in(3,25,26,20,21,0)   
 and DocName <> '' and  (ldoc.isDeletedFromDoclog is NULL   
 or ldoc.isDeletedFromDoclog=1) and createdDate   >= (getdate()-@iNoOfDays)   
 and createdDate <=getdate()    
 union     
 select '' as 'loanregid','' as  'loandocid',ctID as Condid,''as 'SchID', FileName as 'AllFileName',  
 dttime as 'createdDate',Cond_Text as 'Description','' as 'Userid',uemailid as Submittedby  ,  
 uFirstName + ' ' + ulastname as OlieName from tblCondText_History conh     
 left outer join tblusers uconh on uconh.userid=conh.userid where  FileName <> ''   
 and urole in(3,25,26,20,21,0) and  (conh.IsActive is NULL or conh.IsActive=1)   
 and dttime   >= (getdate()-@iNoOfDays) and dttime <=getdate()    
 union     
 select  '' as 'loanregid','' as  'loandocid','' as Condid,Scheduleid as 'SchID', FeeSheet as 'AllFileName',   
 SubmitedDate as 'createdDate',Comments as 'Description','' as 'Userid',uemailid as Submittedby  ,  
 uFirstName + ' ' + ulastname as OlieName from tblScheduleClosing sch     
 left outer join tblusers usch on sch.userid= usch.userid where  FeeSheet <> '' and urole in(3,25,26,20,21,0)   
 and   IsFromLoanSuspendedDoc <> 1 and (sch.IsActive is NULL or sch.IsActive=1)   
 and SubmitedDate   >= (getdate()-@iNoOfDays) and SubmitedDate <=getdate() order by  createdDate desc     
End    
IF @ActionID = 2    
Begin    
 update tblLoanReg set IsActive= 0 where loanregid = @LoanID      
 update tblCondText_History set IsActive= 0 where ctid = @CondID      
 update tblScheduleClosing set IsActive= 0 where Scheduleid = @SchedID      
 update tblloandoc set IsActive= 0 where loandocid = @LoanAndCompID    
 Select 1    
End    
END 

GO

