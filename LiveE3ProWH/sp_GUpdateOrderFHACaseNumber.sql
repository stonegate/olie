USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GUpdateOrderFHACaseNumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GUpdateOrderFHACaseNumber]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nitin>
-- Create date: <>
-- Description:	<Procedure for clsOrderFHAE3>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GUpdateOrderFHACaseNumber]
(
@CaseNum varchar(25),
@LoanNumber varchar(32)
)
AS
BEGIN
	
	SET NOCOUNT ON;
UPDATE mwlLoanApp 
SET CaseNum=@CaseNum
 WHERE LoanNumber =@LoanNumber


END


GO

