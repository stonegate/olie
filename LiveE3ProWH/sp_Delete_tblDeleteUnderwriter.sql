USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Delete_tblDeleteUnderwriter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Delete_tblDeleteUnderwriter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Delete_tblDeleteUnderwriter]
(
@iUserid varchar(Max)
)
as
begin
delete from tblOffices Where lt_office_id ='' and lt_usr_underwriter <> '' and userid =@iUserid
end


GO

