USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Usp_GetAllHudInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Usp_GetAllHudInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Usp_GetAllHudInfo]
  (
      @LoanNumber VARCHAR(10)
  )
AS 
    BEGIN  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
	SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,         
                   -- the entire transaction is terminated and rolled back.
--STEP-3
 SELECT  LoanApp.LoanNumber ,
                ClosingCost.HUDLinenum ,
                ClosingCost.Payee ,
                ClosingCost.BorrowerAmt ,
                ClosingCost.BorrowerPOC ,
                LoanApp.ChannelType ,
                ClosingCost.HUDDesc ,
                CAST(ClosingCost.HUDLinenum AS MONEY) AS lineorder,
				LoanApp.TransType AS TransactionType,
				'3' StepNumber
        FROM    mwlloanapp LoanApp ,
                mwlloandata LoanData ,
                mwlclosingcost ClosingCost
        WHERE   LoanApp.ID = LoanData.objOwner_ID
				And LoanData.Active = 1
                AND LoanData.ID = ClosingCost.loanDAta_ID
                AND LoanApp.LoanNumber = @LoanNumber
                AND CAST(ClosingCost.HUDLinenum AS MONEY) >= 800 -- HUD Line series start from 
                AND CAST(ClosingCost.HUDLinenum AS MONEY) <= 822 -- HUD Line series upto
--Earlier was commented as per given new mapping sheet by SMC
--Now uncommented as per meeting Dt: 4/29/2013 with Jeff, SMC, Brian and all 
        UNION
        SELECT  LoanApp.LoanNumber ,
                Prepaid.HUDLinenum ,
                Prepaid.Payee ,
                Prepaid.BorrowerAmt ,
                Prepaid.BorrowerPOC ,
                LoanApp.ChannelType ,
                Prepaid.HUDDesc ,
                CAST(Prepaid.HUDLinenum AS MONEY) AS lineorder,
				LoanApp.TransType AS TransactionType,
				'3' StepNumber
        FROM    mwlloanapp LoanApp ,
                mwlloandata LoanData ,
                mwlprepaid Prepaid
        WHERE   LoanApp.ID = LoanData.objOwner_ID
				And LoanData.Active = 1
                AND LoanData.ID = Prepaid.loanDAta_ID
                AND LoanApp.LoanNumber = @LoanNumber
                AND CAST(Prepaid.HUDLinenum AS MONEY) >= 900 -- HUD Line series start from  
                AND CAST(Prepaid.HUDLinenum AS MONEY) <= 999 -- HUD Line series upto 
       -- ORDER BY lineorder    
		UNION ALL
--Step 4
--Get HUD Line Data
        SELECT  LoanApp.LoanNumber ,
                ClosingCost.HUDLinenum ,
                ClosingCost.Payee ,
                ClosingCost.BorrowerAmt ,
				'' BorrowerPOC,
				LoanApp.ChannelType,
                ClosingCost.HUDDesc ,
                CAST(ClosingCost.HUDLinenum AS MONEY) AS lineorder,
				LoanApp.TransType AS TransactionType,
				'4' StepNumber
        FROM    mwlloanapp LoanApp ,
                mwlloandata LoanData ,
                mwlclosingcost ClosingCost
        WHERE   LoanApp.ID = LoanData.objOwner_ID and LoanData.Active = 1
                AND LoanData.ID = ClosingCost.loanDAta_ID
                AND LoanApp.LoanNumber = @LoanNumber
                AND CAST(ClosingCost.HUDLinenum AS MONEY) >= 1100 -- We need to get all hud line information greater then 1100 showing at Title Information
                AND CAST(ClosingCost.HUDLinenum AS MONEY) <= 1299 --We need to get all hud line information greater then 1100 but less then 1299 and showing at Title Information
       -- ORDER BY lineorder
--End Get HUD Line Data
--Step -6
		UNION ALL
		 SELECT  LoanApp.LoanNumber ,
				 ClosingCost.HUDLinenum ,
				 ClosingCost.Payee ,  
                ClosingCost.borroweramt ,   
                ClosingCost.BorrowerPOC ,
				LoanApp.ChannelType,
                ClosingCost.HUDDesc ,
                CAST(ClosingCost.HUDLinenum AS MONEY) AS lineorder ,
                LoanApp.TransType AS TransactionType,
				'6' StepNumber
        FROM    mwlloanapp LoanApp ,
                mwlloandata LoanData ,
         mwlclosingcost ClosingCost
        WHERE   LoanApp.ID = LoanData.objOwner_ID
				AND LoanData.Active = 1 
                AND LoanData.ID = ClosingCost.loanDAta_ID
                AND LoanApp.LoanNumber = @LoanNumber
                AND CAST(ClosingCost.HUDLinenum AS MONEY) >= 1300 -- HUD Line series start from 
                AND CAST(ClosingCost.HUDLinenum AS MONEY) <= 2000 -- HUD Line series upto
        UNION
        SELECT  LoanApp.LoanNumber ,
				OtheCred.HUDLinenum ,
				OtheCred.Payee ,
                OtheCred.borroweramt ,
                OtheCred.BorrowerPOC ,
				LoanApp.ChannelType,
                OtheCred.HUDDesc ,
                CAST(OtheCred.HUDLinenum AS MONEY) AS lineorder ,
                LoanApp.TransType AS TransactionType,
				'6' StepNumber
        FROM    mwlloanapp LoanApp ,
                mwlloandata LoanData ,
                mwlothercredit OtheCred
        WHERE   LoanApp.ID = LoanData.objOwner_ID
				AND LoanData.Active = 1 
                AND LoanData.ID = OtheCred.loanDAta_ID
                AND LoanApp.LoanNumber = @LoanNumber
                AND CAST(OtheCred.HUDLinenum AS MONEY) >= 201-- HUD Line series start from 
                AND CAST(OtheCred.HUDLinenum AS MONEY) <= 212-- HUD Line series upto
        ORDER BY 8 
END 


GO

