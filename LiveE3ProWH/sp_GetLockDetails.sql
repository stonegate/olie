USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLockDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLockDetails]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  Stonegate      
-- Create date: April, 18 2012      
-- Description: This will give Loan details for the loan number passed      
-- =============================================      
CREATE PROCEDURE [dbo].[sp_GetLockDetails]
(      
@LoanNumber nvarchar(50)  ,
@UserId nvarchar(50) = null
)      
AS
BEGIN  

  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
 SET NOCOUNT ON;   
   
SELECT CASE LockRecord.status WHEN NULL then '' WHEN 'INITIALIZE' then 'Incomplete' WHEN 'SUBMITTED' then 'Submitted' 
WHEN 'ONHOLD' then 'On hold' WHEN 'CONFIRMED' then 'Confirmed' 
WHEN 'REJECTED' then 'Rejected' WHEN 'EXPIRED' then 'Expired' WHEN 'WITHDR
AWN' then 'Withdrawn' WHEN 'CANCEL REQ' then 'Cancel requested' 
WHEN 'CANCELED' then 'Canceled' end as LockActionStatus,  
LockRecord.LockDays,LockRecord.LockDateTime as EffectiveDateTime,LockRecord.LockExpirationDate,  
CASE LockRecord.LockReason WHEN NULL then '' WHEN 'EXTENSION' then 'Extension' 
WHEN 'ORIGINAL' then 'Original lock' WHEN 'PROGCHANGE' then 'Program change' WHEN 'RELOCK' then 'Re-lock' 
WHEN 'MODIFIED' then 'Re-negotiation' else LockReason end as LockReason,  
LockRecord.Comments,CONVERT(DECIMAL(10,3), LenderBaseRate * 100) as BaseRate,
CONVERT(DECIMAL(10,3),LenderClosingRate * 100) as FinalRate,CONVERT(DECIMAL(10,3),LenderBaseMargin * 100) as BaseMargin,
CONVERT(DECIMAL(10,3),LenderClosingMargin * 100) as TotalMargin ,CONVERT(DECIMAL(10,3),LenderBasePrice * 100) as BasePrice,CONVERT(DECIMAL(10,3),LenderClosingPrice * 100) as TotalPrice
, LockRecord.DeliveryOption  
FROM mwlLockRecord as LockRecord  
LEFT OUTER JOIN mwlPricingResult as PricingResult ON PricingResult.ObjOwner_ID=LockRecord.ID  
INNER JOIN mwlLoanApp as LoanApp ON LoanApp.ID=LockRecord.LoanApp_ID  
 WHERE LoanNumber=@LoanNumber and locktype='LOCK' order by LockRecord.CreatedOnDate desc  
  
  select isnull(chkpricelockloan,0) as PriceLockPermission from tblUsers where userloginid=@UserId
END



GO

