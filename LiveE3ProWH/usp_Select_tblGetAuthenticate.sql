USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tblGetAuthenticate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tblGetAuthenticate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[usp_Select_tblGetAuthenticate]
(
@strPsCellNumber varchar(Max)
)
as
begin
	SELECT temp.refuserid,temp.tempuserid,temp.Tempid,temp.temppswd,temp.createdDate,temp.IsActive 
	FROM tblTempUser temp left join tblusers users on  temp.refuserid= users.userid 
	WHERE (tempuserid = @strPsCellNumber)  
	and temp.IsActive=1 and users.IsActive=1 and isnull(IsUserInNewProcess,0)=1
end



GO

