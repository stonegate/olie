USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckLoanNo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckLoanNo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  [dbo].[sp_CheckLoanNo]
	-- Add the parameters for the stored procedure here

@loan_No varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select * from loandat where lt_loan_stats  IN ('Locked','Application Received','Submitted to Underwriting','In Underwriting','Loan Suspended','Approved') and loan_no =@loan_No 
END

GO

