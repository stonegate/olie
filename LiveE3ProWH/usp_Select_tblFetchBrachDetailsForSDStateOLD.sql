USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tblFetchBrachDetailsForSDStateOLD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tblFetchBrachDetailsForSDStateOLD]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- Author: Rajesh Mund      
-- Create date: 7/12/2012      
-- Description: Procedure for Publish for CFI site      
-- =============================================  
  
CREATE proc [dbo].[usp_Select_tblFetchBrachDetailsForSDStateOLD]    
(    
@strAbbr varchar(50)    
)    
as    
begin    
    
select * from tblusers where (StatebelongTo like '%' + @strAbbr + '%')     
and urole = 26 and isactive = 1 and ispublish=1;    
    
  
    
end
GO

