USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpDeleteDocument]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpDeleteDocument]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

     
CREATE PROCEDURE [dbo].[SpDeleteDocument]       
(        
 @ctid int    
)        
AS        
BEGIN        
        
delete from tblCondText_History where ctid =@ctid
        
END 



GO

