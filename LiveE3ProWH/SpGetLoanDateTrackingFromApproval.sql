USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetLoanDateTrackingFromApproval]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetLoanDateTrackingFromApproval]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <Stonegate>        
-- Create date: <10/6/2012>        
-- Description: <Get Loan Data Tracking From Approval>        
-- =============================================        
CREATE PROCEDURE [dbo].[SpGetLoanDateTrackingFromApproval]       
(        
 @LoanNo nvarchar(100)    
)        
AS        
BEGIN     
	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
	   
select *from mwlApprovalStatus where loanapp_id in(Select id from MwlLoanapp where DecisionStatus='Approved' and LoanNumber=@LoanNo)
END 

GO

