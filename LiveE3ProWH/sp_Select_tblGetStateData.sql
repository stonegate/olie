USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetStateData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetStateData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Gourav  
-- Create date: 10/03/2012  
-- Description: Procedure for clsUser.cs bll
-- EXEC sp_Select_tblGetStateData 'west'    
-- =============================================  
CREATE PROCEDURE [dbo].[sp_Select_tblGetStateData]  
@RegionValue varchar(50) 
as  
begin 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

 if(@RegionValue='') 
  select state_id,name,rtrim(ltrim(abbr)) as abbr,RegionValue from tblState  
 else
  select state_id,name,rtrim(ltrim(abbr)) as abbr,RegionValue from tblState  
    where RegionValue IN (SELECT * FROM dbo.SplitString(@RegionValue, ','))	
	
end  



GO

