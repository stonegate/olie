USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ManageNewsAndAnnouncement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ManageNewsAndAnnouncement]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_ManageNewsAndAnnouncement]                
(                
    
    
@ActionID int,                
     
    
@Title nvarchar(1000),                
     
    
@Body text,                
     
    
@IsActive bit=0,                
     
    
@IsNewsAnnouncement bit=0,                 
     
    
@Role nvarchar(Max), 
@RoleC nvarchar(Max),    
@RoleR nvarchar(Max),
            
    
@NewsAnnousVisbleAd bit=0,                
     
    
@NewsAnnousVisbleDR bit=0,                
     
    
@NewsAnnousVisbleBR bit=0,                
     
    
@NewsAnnousVisbleLO bit=0,                
     
    
@NewsAnnousVisbleCU bit=0,                
     
    
@NewsAnnousVisbleRE bit=0,                
     
    
@NewsAnnousVisbleRM bit=0,                
     
    
@NewsAnnousVisbleDP bit=0,                
     
    
@NewsAnnousVisbleCP bit=0,                
     
    
@NewsAnnousVisbleHY bit=0,                
     
    
@NewsAnnousVisbleUM bit=0,                
     
    
@NewsAnnousVisbleDCFI bit=0,                
     
    
@NewsAnnousVisibleSCFI bit=0,                
     
    
@ModifiedBy int,                
     
    
@ModifiedDate datetime,                
     
    
@NewsID int --Used for update and delete purpose    

           
)                
AS                
BEGIN    
SET NOCOUNT ON;                
    
    
If (@ActionID = 1) -- Insert                
Begin    
INSERT INTO tblNewsAndAnnouncement (Title, Body, IsActive, IsNewsAnnouncement, CreatedBy, CreatedDate,    
Role, RoleC, RoleR, NewsAnnousVisbleAd, NewsAnnousVisbleDR, NewsAnnousVisbleBR, NewsAnnousVisbleLO,    
NewsAnnousVisbleCU, NewsAnnousVisbleRE, NewsAnnousVisbleRM, NewsAnnousVisbleDP,    
NewsAnnousVisbleCP, NewsAnnousVisbleHY, NewsAnnousVisbleUM, NewsAnnousVisbleDCFI,    
NewsAnnousVisibleSCFI, ModifiedBy, ModifiedDate)    
 VALUES (@Title, @Body, @IsActive, @IsNewsAnnouncement, @ModifiedBy, @ModifiedDate, @Role, @RoleC, @RoleR, @NewsAnnousVisbleAd,   
 @NewsAnnousVisbleDR, @NewsAnnousVisbleBR, @NewsAnnousVisbleLO, @NewsAnnousVisbleCU, @NewsAnnousVisbleRE,  
  @NewsAnnousVisbleRM, @NewsAnnousVisbleDP, @NewsAnnousVisbleCP, @NewsAnnousVisbleHY, @NewsAnnousVisbleUM,   
  @NewsAnnousVisbleDCFI, @NewsAnnousVisibleSCFI, @ModifiedBy, @ModifiedDate)    
    
    
--declare @lid varchar(50)    
--SET @lid = (SELECT TOP 1    
-- ID    
--FROM tblNewsAndAnnouncement    
--ORDER BY ID DESC)    
    
--INSERT INTO Retail_tblNewsAndAnnouncement (Title, Body, IsActive, IsNewsAnnouncement, CreatedBy, CreatedDate,    
--Role, NewsAnnousVisbleAd, NewsAnnousVisbleDR, NewsAnnousVisbleBR, NewsAnnousVisbleLO,    
--NewsAnnousVisbleCU, NewsAnnousVisbleRE, cc_id)    
-- VALUES (@Title, @Body, @IsActive, @IsNewsAnnouncement, @ModifiedBy, @ModifiedDate, @Role, @NewsAnnousVisbleAd, @NewsAnnousVisbleDR, @NewsAnnousVisbleBR, @NewsAnnousVisbleLO, @NewsAnnousVisbleCU, @NewsAnnousVisbleRE, @lid)    
SELECT   1                
END                
                
    
    
If (@ActionID = 2) -- Update                
Begin    
UPDATE tblNewsAndAnnouncement WITH (UPDLOCK)    
SET Title = @Title,    
 Body = @Body,    
 IsActive = @IsActive,    
 --IsNewsAnnouncement = @IsNewsAnnouncement,                
 Role = @Role,
  RoleC = @RoleC, 
 RoleR = @RoleR ,    
 NewsAnnousVisbleAd = @NewsAnnousVisbleAd,    
 NewsAnnousVisbleDR = @NewsAnnousVisbleDR,    
 NewsAnnousVisbleBR = @NewsAnnousVisbleBR,    
 NewsAnnousVisbleLO = @NewsAnnousVisbleLO,    
 NewsAnnousVisbleCU = @NewsAnnousVisbleCU,    
 NewsAnnousVisbleRE = @NewsAnnousVisbleRE,    
 NewsAnnousVisbleRM = @NewsAnnousVisbleRM,    
 NewsAnnousVisbleDP = @NewsAnnousVisbleDP,    
 NewsAnnousVisbleCP = @NewsAnnousVisbleCP,    
 NewsAnnousVisbleHY = @NewsAnnousVisbleHY,    
 NewsAnnousVisbleUM = @NewsAnnousVisbleUM,    
 NewsAnnousVisbleDCFI = @NewsAnnousVisbleDCFI,    
 NewsAnnousVisibleSCFI = @NewsAnnousVisibleSCFI,    
 ModifiedBy = @ModifiedBy,    
 ModifiedDate = @ModifiedDate
  
WHERE ID = @NewsID           
  
SELECT  1                
END                
                
    
    
If (@ActionID = 3) -- Select                
Begin    
--if exists(Select ID  From tblNewsAndAnnouncement Where ID=@NewsID)             
--   begin            
SELECT    
 *    
FROM tblNewsAndAnnouncement    
WHERE ID = @NewsID              

END    
    
 end

GO

