USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchBranchDetailForBDMandRM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchBranchDetailForBDMandRM]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author: Rajesh Mund              
-- Create date: 7/12/2012              
-- Description: Procedure for Publish for TPO site              
-- =============================================             
          
CREATE PROCEDURE [dbo].[sp_FetchBranchDetailForBDMandRM]              
-- Add the parameters for the stored procedure here              
@StatebelongTo varchar(50)=null,              
@sOperation varchar(20)=NULL              
              
AS              
BEGIN              
-- SET NOCOUNT ON added to prevent extra result sets from              
-- interfering with SELECT statements.              
SET NOCOUNT ON;              
              
-- Insert statements for procedure here              
            
BEGIN              
IF @sOperation='1'              
select * from tblusers where              
(StatebelongTo like '%' + @StatebelongTo + '%') and urole =8 and isactive = 1 and ispublish=1;              
select * from tblusers where (StatebelongTo like '%' + @StatebelongTo + '%')              
and urole =2 and isactive = 1 and ispublish=1              
              
END              
              
              
END 
GO

