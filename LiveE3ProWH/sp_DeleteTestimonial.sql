USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteTestimonial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteTestimonial]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Stonegate
-- Create date: 10/05/2012
-- Description:	Delete Testimonial
-- =============================================
create proc [dbo].[sp_DeleteTestimonial]
(
@ids varchar(500)
)
as
begin
DELETE FROM tblTestimonials where TestimonialID in (@ids)
end

GO

