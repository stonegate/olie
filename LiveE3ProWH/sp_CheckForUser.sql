USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckForUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckForUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_CheckForUser]
(
@iRole int
)
as  
begin  
select * from tblusers where urole=@iRole
end  

GO

