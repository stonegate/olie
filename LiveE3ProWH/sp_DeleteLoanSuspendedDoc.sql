USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteLoanSuspendedDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteLoanSuspendedDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  sp_DeleteLoanSuspendedDoc
	-- Add the parameters for the stored procedure here

@intSchId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
delete from tblScheduleClosing where Scheduleid=@intSchId 
END

GO

