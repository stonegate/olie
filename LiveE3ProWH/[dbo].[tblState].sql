USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblState]') AND type in (N'U'))
DROP TABLE [dbo].[tblState]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblState](	  [state_id] INT NOT NULL	, [name] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [abbr] CHAR(3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RegionValue] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_STATES] PRIMARY KEY ([state_id] ASC))USE [HomeLendingExperts]
GO

