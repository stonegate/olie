USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertDOOErrorLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertDOOErrorLog]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Kanva>  
-- Create date: <8th May 2013>  
-- Modified By: <Miteh>  
-- Modified Date: <14th June 2013>  
-- Description: <This will be used to keep error log for DOO>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_InsertDOOErrorLog]
(  
 @LoanNumber varchar(10),  
 @ErrorDesc nvarchar(max), --requst and response from web service may contain special characters
 @Request nvarchar(max), --requst and response from web service may contain special characters
 @Response nvarchar(max),--requst and response from web service may contain special characters
 @PartnerCompanyID varchar(100)  
)  
AS  
BEGIN  
INSERT INTO tbldooerrorlog 
            (loannumber, 
             errordesc, 
             request, 
             response, 
             partnercompid) 
VALUES      (@LoanNumber, 
             @ErrorDesc, 
             @Request, 
             @Response, 
             @PartnerCompanyID) 
END 

-- Create Procedure Coding ENDS Here

GO

