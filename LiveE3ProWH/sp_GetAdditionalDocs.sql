USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAdditionalDocs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAdditionalDocs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/20/2012
-- Description:	Procedure for Get Additional Docs
-- =============================================
CREATE PROCEDURE sp_GetAdditionalDocs
(
@loan_no char(15)
)

AS
BEGIN
	
	SET NOCOUNT ON;
select * from tblAdditionalUWDoc
 where iscommited = 0 and rtrim(loan_no)=@loan_no
END

GO

