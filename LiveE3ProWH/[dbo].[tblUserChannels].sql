USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUserChannels]') AND type in (N'U'))
DROP TABLE [dbo].[tblUserChannels]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserChannels](	  [UserChannelId] INT NOT NULL IDENTITY(1,1)	, [UserId] INT NOT NULL	, [ChannelId] INT NOT NULL	, [CreatedBy] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [CreatedOn] DATETIME NOT NULL	, [ModifiedBy] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ModifiedOn] DATETIME NULL	, CONSTRAINT [PK_tblUserChannels] PRIMARY KEY ([UserChannelId] ASC))ALTER TABLE [dbo].[tblUserChannels] WITH CHECK ADD CONSTRAINT [FK_tblUserChannels_tblChannels] FOREIGN KEY([ChannelId]) REFERENCES [dbo].[tblChannels] ([ChannelId])ALTER TABLE [dbo].[tblUserChannels] CHECK CONSTRAINT [FK_tblUserChannels_tblChannels]ALTER TABLE [dbo].[tblUserChannels] WITH CHECK ADD CONSTRAINT [FK_tblUserChannels_tblUsers] FOREIGN KEY([UserId]) REFERENCES [dbo].[tblUsers] ([userid])ALTER TABLE [dbo].[tblUserChannels] CHECK CONSTRAINT [FK_tblUserChannels_tblUsers]USE [HomeLendingExperts]
GO

