USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Delete_tblDeleteOffice]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Delete_tblDeleteOffice]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Delete_tblDeleteOffice]
(
@iUserid varchar(Max)
)
as
begin
delete from tblOffices Where  userid =@iUserid
end


GO

