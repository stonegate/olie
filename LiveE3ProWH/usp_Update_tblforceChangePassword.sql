USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Update_tblforceChangePassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Update_tblforceChangePassword]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[usp_Update_tblforceChangePassword]
(
@newPass varchar(Max),
@passExpDate varchar(Max),
@userid varchar(Max)
)
as
begin
update tblusers set upassword =@newPass, PasswordExpDate=@passExpDate where userid = @userid
end




GO

