USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetFormRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetFormRole]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetFormRole] 
(
@strRoles varchar(50)
)
AS
BEGIN
 SELECT STUFF((SELECT ', ' + roles FROM tblroles WHERE id IN (@strRoles) FOR XML PATH('')),1,1,'') AS Roles
END







GO

