USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_UpdateTolerance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_UpdateTolerance]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[Sp_UpdateTolerance]    Script Date: 14-01-2014 15:24:18 ******/

Create PROCEDURE [dbo].[Sp_UpdateTolerance] @loanno VARCHAR(10) 
AS 
  BEGIN        
      SET xact_abort ON; 

      UPDATE tbldootolerancehistory WITH (UPDLOCK)
      SET    ActualValue=UpdatedValue, ModifiedDate=getdate()
      WHERE  LoanNo = @loanno 
  END        

-- Create Procedure Coding ENDS Here


GO

