USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[testproc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[testproc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[testproc]
	
AS
BEGIN
--Use LiveE3Pro;
--exec (sp_GetWholeSaleUserList '','Desc','',1,10,2,'858')

--Use DS_Livetest0112;
select top 5 * from DS_Livetest0112..users
END

GO

