USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getmwlBorrowerLastNameAndEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getmwlBorrowerLastNameAndEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/06/2012
-- Description:	Procedure for clspipelineE3 BLL
-- =============================================
CREATE procEDURE [dbo].[sp_getmwlBorrowerLastNameAndEmail]
(@LoanNumber varchar(15))
AS
BEGIN
	
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

 select borr.lastname as borr_last,Users.Email as user_email,UnderWriter_ID from mwlloanapp loanapp
Inner join mwlBorrower  borr on borr.loanapp_id = loanapp.id 
Left join mwlUnderwritingSummary UW on UW.LoanApp_id= loanapp.id
 Left join mwaMWUser Users on Users.ID = UW.UnderWriter_ID
 where LoanNumber = @LoanNumber;
Select StringValue as ProcessorEmail  from  mwlCustomField 
where  CustFieldDef_ID='D040E2495A8F4D7E8A117A0BDE1889D7' 
 and LoanApp_id in (Select ID as LoanApp_id  from mwlloanapp 
where Loannumber = @LoanNumber)
END


GO

