USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_SaveC3ClearCapitalIntegration_Setting]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_SaveC3ClearCapitalIntegration_Setting]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[USP_SaveC3ClearCapitalIntegration_Setting]

(

@PartnerCompanyId varchar(100) = 177330,

@PartnerCompanyName nvarchar(400) = null,

@RoleId bigint = null,

@C3Integration varchar(2) = null,

@ChannelType varchar(10) = null,

@CreatedBy varchar(2) = null,

@IsEPF varchar(2) = null

)

AS

BEGIN


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

	IF EXISTS (Select 1 from tblEmergingBankers Where PartnerCompanyId = @PartnerCompanyId and Flag = 'C3CCI' and Status = 'Y'  and IsEmergingBanker <> @C3Integration)

	BEGIN	--SET NOCOUNT ON;

	Update tblC3Integration

	Set IsEmergingBanker = @C3Integration,

		ModifiedBy = @CreatedBy,

		ModifiedDate = getdate()

	from tblEmergingBankers tblC3Integration Where 

	PartnerCompanyId = @PartnerCompanyId and Flag = 'C3CCI' and Status = 'Y' 

	if(@C3Integration = 'N')

	BEGIN

	UPDATE tblUsers

	SET IsC3Integration = 0

	WHERE PartnerCompanyId = @PartnerCompanyId 

	END

	else 
	BEGIN
	UPDATE tblUsers

	SET IsC3Integration = 1

	WHERE PartnerCompanyId = @PartnerCompanyId  and urole  in(3,20,21) and isadmin = 1  and isactive = 1
	END

	END

	ELSE IF NOT EXISTS (Select 1 from tblEmergingBankers Where PartnerCompanyId = @PartnerCompanyId and Flag = 'C3CCI')

	BEGIN
	
	 insert into tblEmergingBankers(PartnerCompanyId,CompanyName,RoleId,IsEmergingBanker,Flag,Status,CreatedBy,CreatedDate)

	 VALUES (@PartnerCompanyId,@PartnerCompanyName,@RoleId,@C3Integration,'C3CCI','Y',@CreatedBy,getdate())

	IF(@C3Integration = 'Y')
	 BEGIN
	 UPDATE tblUsers

	 SET IsC3Integration = 1

	 WHERE PartnerCompanyId = @PartnerCompanyId  and urole  in(3,20,21) and isadmin = 1  and isactive = 1

	 END

	END


	-- Start Added as part of BRD63 
	IF EXISTS (Select 1 from tblEmergingBankers Where PartnerCompanyId = @PartnerCompanyId and Flag = 'EPF' and Status = 'Y'  and IsEmergingBanker <> @IsEPF)

	BEGIN	

	Update tblEPF

	Set IsEmergingBanker = @IsEPF,

		ModifiedBy = @CreatedBy,

		ModifiedDate = getdate()

	from tblEmergingBankers tblEPF Where 

	PartnerCompanyId = @PartnerCompanyId and Flag = 'EPF' and Status = 'Y' 
	END


	ELSE IF NOT EXISTS (Select 1 from tblEmergingBankers Where PartnerCompanyId = @PartnerCompanyId and Flag = 'EPF')

	BEGIN
	insert into tblEmergingBankers(PartnerCompanyId,CompanyName,RoleId,IsEmergingBanker,Flag,Status,CreatedBy,CreatedDate)

	 VALUES (@PartnerCompanyId,@PartnerCompanyName,@RoleId,@IsEPF,'EPF','Y',@CreatedBy,getdate())

	END 


	-- End  as part of BRD63 

END



GO

