USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_GetRegionalManager]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_GetRegionalManager]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		< author name >
-- Create date: 25/10/2012
-- Description:	
-- =============================================
Create PROCEDURE [dbo].[udsp_GetRegionalManager]
@stateID varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select userid,ufirstname, ulastname, uemailid, urole, phonenum, photoname,mobile_ph from tblusers where branchid in (Select * from SplitString(@stateID,',')) AND urole=8
END


GO

