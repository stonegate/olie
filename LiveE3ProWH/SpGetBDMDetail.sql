USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetBDMDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetBDMDetail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetBDMDetail]
@RecordId varchar(500)
AS
BEGIN
	select user_email from users where record_id=@RecordId
END

GO

