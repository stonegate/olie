USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpInsertOnHoldReason]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpInsertOnHoldReason]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nirav>
-- Create date: <10/9/2012>
-- Description:	<Insert On Hold Reason>
-- =============================================
CREATE PROCEDURE [dbo].[SpInsertOnHoldReason] 
	@LoanNo varchar(500),
	@OnHoldReason varchar(2000),
	@UserId int
AS
BEGIN
Insert into TblOnHoldReason(LoanNumber,OnHoldReason,Userid) 
Values(@LoanNo,@OnHoldReason,@UserId)
END

GO

