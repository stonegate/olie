USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetProLoginDataE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetProLoginDataE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetProLoginDataE3] 
	@LoginID varchar(500)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	select ID as record_id,  * from mwamwUser where Username =@LoginID
END

GO

