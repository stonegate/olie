USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tbl_DooAuditLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tbl_DooAuditLog]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Tavant  
-- Create date: 01/16/2014  
-- Description: Procedure to return DOO Audit error logs    
-- =============================================  
CREATE proc [dbo].[usp_Select_tbl_DooAuditLog]  
(  
@idays int = 2
)  
as  

begin  
select tdl.Loannumber,tdl.UserId,mwc.Company as CompanyName,tdl.ErrorTime,tdl.ErrorMessage,tdl.XMLMessage,tdl.EndUserMessage
from tbl_DooAuditLog tdl join mwcinstitution mwc on mwc.CompanyEmail = tdl.PartnerCompanyID 
where CAST(Convert(varchar(10),ExpiryDate ,121) as datetime) > CAST(Convert(varchar(10),getdate() ,121) as datetime)
end  
  

GO

