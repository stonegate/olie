USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDOOToleranceHistory_New]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDOOToleranceHistory_New]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
     
-- =============================================              
-- Author:  <Kanva Patel>              
-- Create date: <31st May 2013>              
-- Description: <This will get tolerance history for given params>          
-- =============================================              
CREATE PROCEDURE [dbo].[sp_GetDOOToleranceHistory_New]      
(          
   @LoanNo varchar(10),
   @StepNo int
)      
AS       
BEGIN              
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;       
SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,               
                   -- the entire transaction is terminated and rolled back.      
      
  SELECT hudlineno, 
       '$ ' 
       + CONVERT(VARCHAR(50), CONVERT(DECIMAL(30, 2), actualvalue))  AS 
       ActualValue, 
       '$ ' 
       + CONVERT(VARCHAR(50), CONVERT(DECIMAL(30, 2), updatedvalue)) AS 
       UpdatedValue, 
       updatedvalue - actualvalue                                    AS 
       Differance 
FROM   tbldootolerancehistory 
WHERE  loanno = @LoanNo 
       AND stepno = @StepNo 
       AND ( updatedvalue - actualvalue ) > 100 -- $100 is the tolerance amount
        
                      
END        

-- Create Procedure Coding ENDS Here

GO

