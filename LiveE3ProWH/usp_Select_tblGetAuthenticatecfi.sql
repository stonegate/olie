USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tblGetAuthenticatecfi]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tblGetAuthenticatecfi]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Select_tblGetAuthenticatecfi] 
	-- Add the parameters for the stored procedure here
	(
	@psCellNumber varchar(Max)
    )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT temp.refuserid,temp.tempuserid,temp.Tempid,temp.temppswd,temp.createdDate,temp.IsActive 
	FROM tblTempUser temp left join tblusers users on  temp.refuserid= users.userid 
	WHERE (tempuserid = @psCellNumber)  
	and temp.IsActive=1 and users.IsActive=1 and isnull(IsUserInNewProcess,0)=1

END


GO

