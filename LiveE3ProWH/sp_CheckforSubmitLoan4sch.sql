USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckforSubmitLoan4sch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckforSubmitLoan4sch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ===========================================================
-- Author:		<Stonegate>
-- Create date: <10/5/2012>
-- Description:	<This will Check for SubmitLoan for search>
-- ===========================================================
CREATE PROCEDURE [dbo].[sp_CheckforSubmitLoan4sch] 
(
	@RoleID int,
	@CustomField varchar(500),
	@strCurrentStatus varchar(3000),
	@strID varchar(max),
	@strChLoanno varchar(200)
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)

Declare @SQL nvarchar(max)
Set @SQL = ''

Set @SQL = 'Select ''E3'' as DB,CustFNMA.YNValue,loanapp.ChannelType,loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,
loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate,
approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,
case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,
UnderwriterName as UnderWriter,Broker.Company as BrokerName,Lookups.DisplayString as CashOut,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate,
CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'' and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0
WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'')
and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0 ELSE (SELECT count(a.ID) as TotalCleared
FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') 
and a.objOwner_ID IN(SELECT ID FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') 
and a.objOwner_ID IN(SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER
From mwlLoanApp loanapp Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id
and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker'' Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc like''%Registered%''
Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved'' Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''
left join mwlcustomfield as CustFNMA on CustFNMA.loanapp_id = loanapp.id  and CustFNMA.CustFieldDef_ID='''+ @CustomField +'''
where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' 
and CurrentStatus IN (select * from dbo.SplitString('''+@strCurrentStatus+''','','')) ' 

if (@RoleID <> 0)
Begin
	if (len(@strID) = 0)
    Begin
		if (@RoleID = 13)
        Begin
			Set @SQL = @SQL + ' and loandat.LT_Broker_Rep in (select * from dbo.SplitString('''+@strID +''','','')) '
		End
		else if (@RoleID = 3 Or @RoleID = 20 Or @RoleID = 21)
        Begin
			Set @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString('''+@strID +''','',''))) '
        End
	End
    else
    Begin
		if (@RoleID = 13)
        Begin
			Set @SQL = @SQL + ' and loandat.LT_Broker_Rep in (select * from dbo.SplitString('''+@strID +''','','')) '
        End
		else if (@RoleID = 3 Or @RoleID = 20 Or @RoleID = 21)
        Begin
			Set @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString('''+@strID +''','',''))) '
		End
    End
End
Set @SQL = @SQL + ' AND (LoanNumber like ''%'+ @strChLoanno +'%'' OR borr.lastname like ''%'+ @strChLoanno +'%'') '
Set @SQL = @SQL + ' order by Per desc '

Print @SQL
exec sp_executesql @SQL

END



GO

