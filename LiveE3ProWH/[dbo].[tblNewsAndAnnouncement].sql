USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblNewsAndAnnouncement]') AND type in (N'U'))
DROP TABLE [dbo].[tblNewsAndAnnouncement]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblNewsAndAnnouncement](	  [ID] INT NOT NULL IDENTITY(1,1)	, [Title] NVARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Body] TEXT(16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL	, [IsNewsAnnouncement] BIT NULL	, [CreatedBy] INT NULL	, [CreatedDate] DATETIME NULL	, [Role] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [NewsAnnousVisbleAd] BIT NULL	, [NewsAnnousVisbleDR] BIT NULL	, [NewsAnnousVisbleBR] BIT NULL	, [NewsAnnousVisbleLO] BIT NULL	, [NewsAnnousVisbleCU] BIT NULL	, [NewsAnnousVisbleRE] BIT NULL	, [NewsAnnousVisbleRM] BIT NULL DEFAULT((0))	, [NewsAnnousVisbleDP] BIT NULL DEFAULT((0))	, [NewsAnnousVisbleCP] BIT NULL DEFAULT((0))	, [NewsAnnousVisbleHY] BIT NULL	, [NewsAnnousVisbleDCFI] BIT NULL	, [NewsAnnousVisibleSCFI] BIT NULL	, [NewsAnnousVisbleUM] BIT NULL	, [ModifiedBy] INT NULL	, [ModifiedDate] DATETIME NULL DEFAULT(getdate())	, [CC_ID] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT((0))	, [SiteStatus] VARCHAR(2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT('TC')	, [RoleC] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RoleR] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblNewsAndAnnouncement] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO

