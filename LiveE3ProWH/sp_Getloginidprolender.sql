USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Getloginidprolender]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Getloginidprolender]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/19/2012
-- Description:	Procedure for clspipelineE3 BLL
-- =============================================
Create procEDURE [dbo].[sp_Getloginidprolender]
(
@userid int
)
AS
BEGIN
	
	SET NOCOUNT ON;

 Select loginidprolender from tblusers where userid=@userid
END



GO

