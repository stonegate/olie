USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanDateTracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanDateTracking]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/20/2012
-- Description:	Procedure for Get Loan DateTracking
-- Modified by : Suvarna on 17/12/2013 - Added clear purchase pending calculation logic
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanDateTracking]
(
@LoanNumber Varchar(15)
)

AS
BEGIN


	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
	SET NOCOUNT ON;
    Declare @ClearPurDateDays smallint 
	Declare @NewLoansRangeDate DateTime  
	Declare @FirstPurPendDate  DateTime 
	Declare @ClearPurPendingDate DateTime
	Declare @LastPurPendDate DateTime
	Declare @ClearPurDateDaysOld smallint
	Declare @LockExpirationDate DateTime
	

	SET @ClearPurDateDays = (select top 1 KeyValue from tblAppSettings where KeyName='ClearedPurDateDays') 	
	SET @ClearPurDateDaysOld = (select  top 1 KeyValue from tblAppSettings where KeyName='ClearedPurDateDaysOld')
	SET @NewLoansRangeDate = CAST ((select  top 1 KeyValue from tblAppSettings where KeyName='NewloansRangeDate') AS DATETIME)
   SET  @FirstPurPendDate = (select  MIN(StatusDateTime) from mwlAppStatus  where loanapp_id in(Select id from MwlLoanapp where LoanNumber=@LoanNumber) and StatusDesc in ('54 - Purchase Pending') group by Loanapp_id,StatusDesc)
--	SET @LastPurPendDate = (select  Max(StatusDateTime) from mwlAppStatus  where loanapp_id in(Select id from MwlLoanapp where LoanNumber=@LoanNumber) and StatusDesc in ('54 - Purchase Pending') group by Loanapp_id,StatusDesc)
	SET @LockExpirationDate=(select LockRecord.LockExpirationDate from mwlLockRecord as LockRecord inner join MwlLoanapp as loanapp
	on LockRecord.loanapp_id= loanapp.id where LockRecord.LockType='LOCK' and LockRecord.[Status] = 'CONFIRMED' and loanapp.LoanNumber=@LoanNumber)

	 if(@LockExpirationDate > DATEADD( day , @ClearPurDateDays , @FirstPurPendDate))
		begin
			 SET @ClearPurPendingDate = @LockExpirationDate
		end

    if @ClearPurPendingDate is null
	begin
		 if(@FirstPurPendDate > @NewLoansRangeDate ) 
			BEGIN
			  SET @ClearPurPendingDate = (select DATEADD( day , @ClearPurDateDays , @FirstPurPendDate))
			END
	    else
			begin
			 SET @ClearPurPendingDate = (select DATEADD( day , @ClearPurDateDaysOld , @FirstPurPendDate))
		end	 	
	end		  

     select ID,	LoanApp_ID,	StatusDesc,	StatusMWCode,
	 CASE WHEN left(ltrim(rtrim(StatusDesc)),2) = '54' THEN  @FirstPurPendDate  ELSE 	StatusDateTime END as StatusDateTime,
	 SequenceNum,HMDACode,	LPCode,	UpdatedOnDate,	UpdatedByUser,	CreatedOnDate,	CreatedByUser,	
	 @ClearPurPendingDate as ClearPurPendingDate from mwlAppStatus  
	 where loanapp_id in(Select id from MwlLoanapp where LoanNumber=@LoanNumber)
END

GO

