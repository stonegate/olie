USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckLoanforSubmitCondition]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckLoanforSubmitCondition]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Stonegate>
-- Create date: <10/4/2012>
-- Description:	<This will check loan for submit condition >
-- =============================================
CREATE PROCEDURE [dbo].[sp_CheckLoanforSubmitCondition]
(
	@RoleID int,
	@strID varchar(max),
	@strChLoanNo varchar(25)	
)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)

Declare @SQL nvarchar(max)
set @SQL =''
set @SQL ='select ''E3'' as DB,loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,
LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,
'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate,approvalStat.StatusDateTime as DateApproved,
case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,
case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,
loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockExpirationDate,
UnderwriterName as UnderWriter,Broker.Company as BrokerName,Lookups.DisplayString as CashOut,
CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate,
CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a 
where DueBy=''Prior to Closing'' and Category !=''LENDER'' and a.objOwner_ID IN(SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0  
WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where(DueBy=''Prior to Closing'' and Category !=''LENDER'')  
and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'') 
and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0 ELSE(SELECT count(a.ID) as TotalCleared 
FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'')and(CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'') 
and a.objOwner_ID IN(SELECT ID FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions 
FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'') 
and a.objOwner_ID IN(SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER
from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id 
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id 
Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' 
and Broker.objownerName=''Broker'' left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id 
Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc like''%Registered%'' 
Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved'' 
Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode 
where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  
and CurrentStatus IN(''01 - Registered'',''05 - Application Taken'',''09 - Application Recieved'',''13 - File Intake'',''17 - Submission On Hold'',''21 - In Processing'',''25 - Submitted to Underwriting'',''29 - In Underwriting'') '

if (@RoleID = 0)
Begin
	if (len(@strID) = 0)
    Begin
		if (@RoleID = 13)
        Begin
			Set @SQL = @SQL + ' and loandat.LT_Broker_Rep in (select * from dbo.SplitString('''+@strID +''','','')) '  
        End
        else if (@RoleID = 3 Or @RoleID = 20 Or @RoleID = 21)
        Begin
			Set @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString('''+@strID +''','','')) '  
        End
     End
     else
     Begin
		if (@RoleID = 13)
        Begin
			Set @SQL = @SQL + ' and loandat.LT_Broker_Rep in (select * from dbo.SplitString('''+@strID +''','','')) '  
        End
        else if (@RoleID = 3 Or @RoleID = 20 Or @RoleID = 21)
        Begin
			Set @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString('''+@strID +''','','')) '  
        End
      End
 End
Set @SQL = @SQL + ' AND LoanNumber='''+ @strChLoanNo +''' '
Set @SQL = @SQL + ' order by Per desc '
Print @SQL
exec sp_executesql @SQL	
END
GO

