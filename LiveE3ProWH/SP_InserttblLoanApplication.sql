USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_InserttblLoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_InserttblLoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_InserttblLoanApplication]
(
@BrokarID int,
@Description nvarchar(100),
@Email nvarchar(100),
@FileName nvarchar(1000),
@Comments nvarchar(1000),
@DateEntered datetime
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Insert into tblLoanApplication(BrokarID,Description,Email,FileName,Comments,DateEntered) 
Values(@BrokarID,@Description,@Email,@FileName,@Comments,@DateEntered)
END

GO

