USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetIsUserInNewProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetIsUserInNewProcess]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

CREATE PROCEDURE [dbo].[SP_GetIsUserInNewProcess]
	(
@urole int
)
AS
BEGIN
	 
select isnull(IsUserInNewProcess,0) as IsUserInNewProcess from tblusers where urole=@urole 
END	


GO

