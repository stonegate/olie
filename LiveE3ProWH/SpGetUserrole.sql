USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetUserrole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetUserrole]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetUserrole] 
	@Roles varchar(500)
AS
BEGIN
	select * from tblroles where id in(@Roles)
END

GO

