USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLoanCorrespondentType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetLoanCorrespondentType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Vipul,,Thacker>
-- Create date: <10 March 2013>
-- Description:	<This Stored procedure used for Get correspondent type from E3 DB >
---Modify By: <Vipul, Thacker>
-- Modify date: <04 April 2013>
-- =============================================
CREATE PROCEDURE [dbo].[GetLoanCorrespondentType]
(
	@LoanNumber varchar(50)
)	
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
	
	SELECT StringValue FROM mwlCustomField WITH (nolock) where CustFieldDef_ID = (SELECT ID FROM mwsCustFieldDef WITH (nolock) WHERE MWFieldNum=9830) and LoanApp_ID=(Select ID from mwlLoanApp WITH (nolock) WHERE LoanNumber = @LoanNumber)
	
END


GO

