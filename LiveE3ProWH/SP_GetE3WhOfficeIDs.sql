USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetE3WhOfficeIDs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetE3WhOfficeIDs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

CREATE PROCEDURE [dbo].[SP_GetE3WhOfficeIDs]
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	 select Contact.Firstname,Contact.Lastname,Contact.Lastname + ' ,' + Contact.Firstname as FullName,office ,usert.Id as E3Userid 
               from mwccontact Contact   
               inner join mwcinstitution Ins on Contact.Institution_id=Ins.id inner join mwamwuser usert  
               on usert.Firstname=Contact.Firstname and   usert.Lastname=Contact.Lastname  
               where office like 'TPO%' 
END

-- Create Procedure Coding ENDS Here

GO

