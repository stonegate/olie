USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdatemwlLoanApp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdatemwlLoanApp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Nitin  
-- Create date: 10/07/2012  
-- Description: Procedure for clspipelineE3 BLL  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_UpdatemwlLoanApp]  
(  
@ID varchar(32)  ,
@CurrentStatus varchar(30)
)  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
UPDATE mwlLoanApp   
SET  CurrPipeStatusDate=getdate(),   
CurrentStatus=@CurrentStatus  
 WHERE ID=@ID  
    
END  
  
GO

