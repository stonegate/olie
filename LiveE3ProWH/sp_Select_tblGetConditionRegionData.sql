USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetConditionRegionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetConditionRegionData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 
-- Description:	GetBusineesType_Role for clsPipeline.cs
-- =============================================
CREATE PROCEDURE [dbo].[sp_Select_tblGetConditionRegionData]
( 
	@oRole varchar(Max),
	@UserID varchar(Max)
)
AS
BEGIN
	if(@oRole='12' or @oRole='18' or @oRole='1')
	begin
		select Region, uLastName + ', ' + uFirstName AS FullName, uidprolender,e3userid,urole from tblusers where urole in(2,8,19) and IsActive=1
	end
	else if(@oRole='25')
	begin
		select Region, uLastName + ', ' + uFirstName AS FullName, uidprolender,e3userid,urole from tblusers where urole in(26) and IsActive=1
	end
	ELSE
	BEGIN
		if(@oRole='8')
		begin
			declare @office varchar(Max)
			set @office=(Select region from tblusers where userid = @UserID)
			if(@office!='')
			begin
				select Region, uLastName + ', ' + uFirstName AS FullName, uidprolender,e3userid,urole from tblusers where region=@office and e3userid is not null  and urole in(2)
			end
		end
	END
END


GO

