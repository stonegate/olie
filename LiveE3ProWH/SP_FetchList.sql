USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_FetchList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_FetchList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

CREATE PROCEDURE [dbo].[SP_FetchList]

AS
BEGIN
select * from tblNewsAndAnnouncement order by createddate desc
             
END

GO

