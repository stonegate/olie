USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tblGetAuthenticateValue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tblGetAuthenticateValue]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[usp_Select_tblGetAuthenticateValue]
(
@strPsCellNumber varchar(Max)
)
as
begin
SELECT userid,urole,isUserInNewProcess ,UserLoginid FROM tblUsers 
WHERE  (uemailid = @strPsCellNumber or UserLoginid = @strPsCellNumber)
end




GO

