USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_CancelLoan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_CancelLoan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

Create PROCEDURE [dbo].[SP_CancelLoan]
	(
@Loan_no char(15)
)
AS
BEGIN
update loandat set CANCELED_DATE=getDate(), lt_loan_stats='LOAN CANCELED' where Loan_no=@Loan_no
END




GO

