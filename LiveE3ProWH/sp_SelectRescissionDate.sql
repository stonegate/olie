USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SelectRescissionDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SelectRescissionDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Tavant>  
-- Create date: <08 May 2014>  
-- Modified By: <Tavant>  
-- Description: <This will be used to calculate the Rescission date from Closing Date>   
-- =============================================  
CREATE PROCEDURE [dbo].[sp_SelectRescissionDate]
(  
 @ClosingDate datetime,
 @iDays Int,
 @Rescissiondate  Datetime output
 )  
AS  
BEGIN
Declare @NextSundayDate datetime

Declare @FinalDate datetime
Declare @HolidayCount int
Declare @SundayCount int
Declare @InitalClosingDate datetime

Set @InitalClosingDate = @ClosingDate
Set @NextSundayDate = (Select DATEADD([day], ((DATEDIFF([day], '19000107', @ClosingDate) / 7) * 7) + 7, '19000107'))

Set @HolidayCount = (Select Count(*) from tblHoliDayList H where H.Status = 'Y' and HolidayType in('s','sf') and H.HolidayDate between CAST(Convert(varchar(10),@ClosingDate ,121) as datetime) and 
(CAST(Convert(varchar(10),@ClosingDate ,121) as datetime) + @iDays) and Convert(varchar(10),H.HolidayDate,121) <> Convert(varchar(10),@NextSundayDate,121))


set @Rescissiondate = (select  @ClosingDate + @iDays + @HolidayCount)

   Set @SundayCount = 0
   While @ClosingDate <= @Rescissiondate Begin
      If datepart(dw, @ClosingDate) = 1 Set @SundayCount = @SundayCount + 1
      Set @ClosingDate = DateAdd(d, 1, @ClosingDate)
   End

Set  @FinalDate =@Rescissiondate+@SundayCount 

ifholiday:
if exists(select 1 from tblHolidaylist where Convert(varchar(10),HolidayDate,121) = Convert(varchar(10),@FinalDate,121) and [Status] = 'Y' and HolidayType in('s','sf'))
BEGIN
set @FinalDate = @FinalDate+1
goto ifsunday
END 

ifsunday:
if(datepart(dw, @FinalDate) = 1)
BEGIN
Set @FinalDate   = @FinalDate+1
goto ifholiday
END


set @FinalDate = CASE WHEN datepart(dw, @InitalClosingDate) =1 THEN @FinalDate-1 
					  WHEN @InitalClosingDate = (select top 1 HolidayDate from tblHolidaylist where HolidayDate = @InitalClosingDate and [Status] = 'Y' and HolidayType in('s','sf')) THEN @FinalDate-1  ELSE @FinalDate END 


set @Rescissiondate = @FinalDate


END 



GO

