USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Get_UserRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Get_UserRole]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

CREATE PROCEDURE [dbo].[SP_Get_UserRole]
	(
@id int
)
AS
BEGIN
select Roles from tblroles  where id=@id
END	


GO

