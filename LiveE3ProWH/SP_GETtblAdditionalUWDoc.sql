USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GETtblAdditionalUWDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GETtblAdditionalUWDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

Create PROCEDURE [dbo].[SP_GETtblAdditionalUWDoc]
	(
@ctid int
)
AS
BEGIN
select * from tblAdditionalUWDoc 
 where iscommited = 0 and ctid= @ctid
             
END

GO

