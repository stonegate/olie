USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_UpdateLockExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_UpdateLockExtension]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================          
-- Author:  <Author,,Name>          
-- Create date: <Create Date,,>          
-- Description: <Description,,>          
-- =============================================          
CREATE PROCEDURE [dbo].[Sp_UpdateLockExtension]          
 @LockRecordId varchar(200),          
 @LoanAppId varchar(200),          
 @LockExpirationDate datetime,          
 @LockExtensionFee float=null,          
 @NewLockPrice float=null,          
 @LockReason varchar(200)=null,          
 @LockComments varchar(200)=null,          
 @LockDays int=null,    
@LockBy varchar(500)=null          
AS          
BEGIN          

update mwlLoanApp  set lockexpirationdate=@LockExpirationDate where Id=@LoanAppId  
Update mwlLockRecord set LockExpirationDate=@LockExpirationDate,LockDays=@LockDays,LockReason=@LockReason,comments=@LockComments where locktype='Lock' and LoanApp_id=@LoanAppId        
Update mwlPricingResult set LenderClosingPrice=@NewLockPrice,LenderBaseRate=@LockExtensionFee where ObjOwner_ID=@LockRecordId   
--Insert into mwlLockRecord(Id,LoanApp_id,LockDays,LockExpirationDate,LockReason,comments,locktype,Status,LOCKEDBY,LockRate)values(@LockRecordId1,@LoanAppId,@LockDays,@LockExpirationDate,@LockReason,@LockComments,'Lock','CONFIRMED',@LockBy,@LockExtensionFee)    
--print @LockRecordId1    
--set @PricingId=  replace(newid(),'-','')    
--print @PricingId    
--insert into mwlPricingResult(Id,ObjOwner_ID,LenderClosingPrice,LenderBaseRate,updatedondate,UpdatedByUser)values(@PricingId,@LockRecordId1,@NewLockPrice,@LockExtensionFee,getdate(),@LockBy)    
END     
  
    
    


GO

