USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblHolidayList]') AND type in (N'U'))
DROP TABLE [dbo].[tblHolidayList]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblHolidayList](	  [ID] BIGINT NOT NULL IDENTITY(1,1)	, [HolidayName] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [HolidayDate] DATETIME NULL	, [HolidayType] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Status] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblHolidayList] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO

