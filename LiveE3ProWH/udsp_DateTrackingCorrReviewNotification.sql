USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_DateTrackingCorrReviewNotification]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_DateTrackingCorrReviewNotification]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                            
-- Author:  Sonu Gupta                            
-- Create date: January,21 2013                            
-- Description: This will give Email Address of BDM,Proceesor.                          
--[udsp_DateTrackingCorrReviewNotification]('1F1F9C3219834F0FA5FF12E11D7189DB')                          
--exec [udsp_DateTrackingCorrReviewNotification] '1F1F9C3219834F0FA5FF12E11D7189DB'                          
-- =============================================                            
CREATE PROCEDURE [dbo].[udsp_DateTrackingCorrReviewNotification]                          
(                          
                        
@CustFieldDateTracking nvarchar(MAX)                        
)                          
AS                          
BEGIN  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
                    
WITH CTE AS                  
(                   
select LoanApp.currpipestatusdate,              
Loannumber,isnull(muser.Email,'') as BDMEmail,borr.lastname as BLastName,isnull(Cust.StringValue,'') as ProcessorEmail from  
dbo.mwlloanapp LoanApp Left join dbo.mwamwuser muser on LoanApp.Originator_id=muser.id Left join dbo.mwlBorrower as borr  
on borr.loanapp_id = loanapp.id  and  borr.sequencenum=1 inner join dbo.mwlCustomField Cust on LoanApp.ID=Cust.LoanApp_id and     
Cust.CustFieldDef_ID=@CustFieldDateTracking  Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id    
and (Broker.InstitutionType = 'BROKER' OR Broker.InstitutionType = 'CORRESPOND') and   
(Broker.objownerName='Broker' OR Broker.objownerName='Correspondent')    where CurrentStatus = '54 - Purchase Pending'                     
and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '')                
SELECT DISTINCT * from CTE where DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))=     
(SELECT DATEADD(dd, -4, DATEDIFF(dd, 0, Date14Plus)) as FinalDate from     
(SELECT DATEADD(dd, 14, DATEDIFF(dd, 0, currpipestatusdate))as Date14Plus) as d)              
END 
GO

