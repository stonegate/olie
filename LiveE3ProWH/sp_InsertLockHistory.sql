USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLockHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLockHistory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Stonegate>
-- Create date: <10/9/2012>
-- Description:	<This will Insert Lock History>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertLockHistory]
(
	@strLoanNumber varchar(50) = null,
	@strUserid varchar(100) = null,
	@strEmail varchar(300) = null
)
AS
BEGIN

If not exists(select * From tblLockHistory where LoanNumber in (@strLoanNumber) and SendEmail = 0)
Insert into 
	tblLockHistory
	(
		LoanNumber,Userid,BrokerEmailid,CreatedDate,SendEmail,ModifyDate
	) 
	Values 
	(
       @strLoanNumber,@strUserid,@strEmail,Getdate(),0,Getdate()
	) 
else
    update 
	tblLockHistory 
		Set userid = @strUserid, BrokerEmailid=@strEmail,
		ModifyDate = Getdate(),
		UpdateCounter = UpdateCounter + 1 
	where LoanNumber = @strLoanNumber

END

GO

