USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Insert_tblInsertUnderWriterManagercfi]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Insert_tblInsertUnderWriterManagercfi]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  Gourav  
-- Create date: 10/03/2012  
-- Description: Procedure for clsUser.cs bll 
--  Modify by : Shyamal Gajjar
-- Modify Date :  15/03/2013
---Description: Procedure is changed for reflect newly added field 'IsKatalsyt'    
-- ============================================= 
create PROCEDURE [dbo].[usp_Insert_tblInsertUnderWriterManagercfi]  
 -- Add the parameters for the stored procedure here  
 (    
@ufirstname varchar(Max)=null    
,@ulastname varchar(Max)=null    
,@uemailid varchar(Max)=null    
,@urole varchar(Max)=null    
,@isadmin varchar(Max)=null    
,@isactive varchar(Max)=null    
,@uidprolender varchar(Max)=null    
,@uparent varchar(Max)=null    
,@upassword varchar(Max)=null    
,@mobile_ph varchar(Max)=null    
,@loginidprolender varchar(Max)=null    
,@carrierid varchar(Max)=null    
,@MI varchar(Max)=null    
,@E3Userid varchar(Max)=null    
,@PasswordExpDate varchar(Max)=null    
,@bitIsKatalyst bit =0
)    
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
Insert into tblusers    
(    
ufirstname    
,ulastname    
,uemailid    
,urole    
,isadmin    
,isactive    
,uidprolender    
,uparent    
,upassword    
,mobile_ph    
,loginidprolender    
,carrierid    
,MI    
,E3Userid    
,PasswordExpDate    
,Iskatalyst
)    
Values    
(    
@ufirstname    
,@ulastname    
,@uemailid    
,@urole    
,@isadmin    
,@isactive    
,@uidprolender    
,@uparent    
,@upassword    
,@mobile_ph    
,@loginidprolender    
,@carrierid    
,@MI    
,@E3Userid    
,@PasswordExpDate 
,@bitIsKatalyst     
)    
END  


GO

