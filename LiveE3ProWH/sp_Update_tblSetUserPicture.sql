USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_tblSetUserPicture]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_tblSetUserPicture]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Update_tblSetUserPicture]
(
@iUserID varchar(Max),
@photoname varchar(Max)
)
as
begin
update tblusers Set photoname = @photoname
 Where userid =@iUserID
end


GO

