USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetTrackingData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetTrackingData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblGetTrackingData]
as
begin
select tblTracking.id, ufirstname, ulastname, uemailid, Roles,LoginTime, LogoutTime,IPAddress  from tblTracking
inner join tblUsers ON tblUsers.UserID = tblTracking.UserID
inner join tblRoles ON tblRoles.id = tblUsers.urole
where logintime between dateadd(day, datediff(day, 0 ,getdate())-90, 0) and getdate()
order by LoginTime desc, LogoutTime desc
end


GO

