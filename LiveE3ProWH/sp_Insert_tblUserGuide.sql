USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_tblUserGuide]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_tblUserGuide]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
CREATE procEDURE [dbo].[sp_Insert_tblUserGuide]     
 -- Add the parameters for the stored procedure here    
 (@Filename varchar(100),  
@CreatedBy varchar(500)

)    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    -- Insert statements for procedure here    
    
 INSERT INTO [tblUserGuide] (Filename,IsActive,CreatedBy,CreatedDate)  
VALUES (@Filename,0,@CreatedBy,getdate())  
end  
GO

