USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetProBrokarLORepIdData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetProBrokarLORepIdData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblGetProBrokarLORepIdData]
(
@sBrokarRepLoginID varchar(Max)
)
as
begin
select *  from BROK_LOGINS where login=@sBrokarRepLoginID
end


GO

