USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCurrentLoanStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCurrentLoanStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  <Author,,Name>      
-- Create date: <Create Date,,>      
-- Description: <Description,,>      
-- =============================================      
-- Author:  <Author,,Name>      
-- Create date: <Create Date,,>      
-- Description: <Description,,>      
-- =============================================      
CREATE PROCEDURE [dbo].[sp_GetCurrentLoanStatus]      
(       
 @LoanNumber varchar(20)      
)      
AS      
BEGIN      

  
select top 1 currentstatus 
from mwlloanapp with (nolock)
Where loannumber = @LoanNumber 
	and Active=1 
order by UpdatedOnDate desc, Createdondate desc
      
        
END 

GO

