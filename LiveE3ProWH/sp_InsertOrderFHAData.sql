USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertOrderFHAData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertOrderFHAData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nitin>
-- Create date: <>
-- Description:	<Procedure for clsOrderFHA>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertOrderFHAData]
(
@ID varchar(32),
@LoanApp_id varchar(32),
@CustFieldDef_id varchar(32),
@StringValue varchar(256)

)
AS
BEGIN
	
	SET NOCOUNT ON;

    Insert into mwlcustomfield
(ID,LoanApp_id,CustFieldDef_id,StringValue) 
Values(@ID,@LoanApp_id,@CustFieldDef_id,@StringValue) 
END


GO

