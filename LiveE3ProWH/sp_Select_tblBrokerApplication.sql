USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblBrokerApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblBrokerApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		 Nitin
-- Create date: 10/03/2012
-- Description:	Procedure for clsBrokerApplication.cs bll  
-- =============================================
CREATE PROCEDURE [dbo].[sp_Select_tblBrokerApplication]   
 
AS  
BEGIN  
 
 SET NOCOUNT ON;  
  
  
  
 SELECT [BrokerAppID],[PrimaryContactEmail] FROM [tblBrokerApplication] 
where IsActive=1
end

GO

