USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_GetFundendLoanReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_GetFundendLoanReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[Sp_GetFundendLoanReport]  
@Year varchar(500) ,  
@oRole int,  
@ChannelType varchar(100)='',  
@strTeamOpuids nvarchar(max)='',  
@BrokerName varchar(2000)='',  
@strID nvarchar(max) = null  
AS  
BEGIN  

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

Declare @SQL nvarchar(max)  
Set @SQL = ''  
Set @SQL = 'SELECT case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' WHEN ''M'' THEN  ''USDA'' End as LoanProgram ,
count(TransTYpe)CountRefinanance, case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as descriptn,
month(AppStatus.StatusDateTime) as MonthYear,count(AppStatus.StatusDateTime)Total 
from mwlLoanApp  loanapp   
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id  and loanData.Active=1
Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc=''62 - Funded''
Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' 
where  Year(AppStatus.StatusDateTime) <> ''1900'' and  Year(AppStatus.StatusDateTime)=' + @Year 
if(@ChannelType = 'broker')
begin
Set @SQL = @SQL + ' and loanapp.ChannelType=''BROKER'' '
end
else if (@ChannelType = 'retail')
begin
Set @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL''  '
end
else if (@ChannelType = 'correspond')
begin
Set @SQL = @SQL + ' and loanapp.ChannelType=''CORRESPOND'' '
end
if (@oRole = 1)
begin
Set @SQL = @SQL + ' and loanapp.ChannelType<>''RETAIL'' '
end
if(@strTeamOpuids != '')
begin
Set @SQL = @SQL + '  and Originator_id in (select * from dbo.SplitString('''+@strTeamOpuids+''','',''))'
	if (@oRole = 20)
	begin
		Set @SQL = @SQL + '(select * from dbo.SplitString('''+@strTeamOpuids+''','',''))'
	end
end
if(@BrokerName !='')
begin
	 DECLARE @TEMPSTR NVARCHAR(1000)  
  SET @TEMPSTR = @BrokerName  
  
  IF(CHARINDEX('--',@TEMPSTR) = 0)  
  BEGIN  
  Set @SQL =@SQL + ' and (Broker.Company=''' + @BrokerName + ''' or Corres.Company=''' + @BrokerName + ''')'  
  END  
end
else
begin
	if(@oRole = 0 and @strTeamOpuids = '')
	begin
		if(len(@strID) = 0 )
		begin
			if(@oRole = 13)
			begin
				Set @SQL =@SQL + ' loandat.LT_Broker_Rep in ((select * from dbo.SplitString('''+@strID+''','',''))) and Year(LD2.DOC_FUNDED_DATE)=''' + @Year + ''' group by month(LD2.DOC_FUNDED_DATE),lt_loan_stats,lookups.descriptn, lookupprogram.descriptn '
			end
			 --for broker
            else if (@oRole = 13 or @oRole = 20 or @oRole = 21)
            begin
                 	Set @SQL =@SQL + '  and (loanapp.ID in (select * from dbo.SplitString('''+@strID+''','','')))'
            end
            else
			begin            
		      	Set @SQL =@SQL + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','','')))'
			end
		end
		else
		begin
			if(@oRole = 13)
			begin
				Set @SQL =@SQL + ' loandat.LT_Broker_Rep in (select * from dbo.SplitString('''+@strID+''','','')) and Year(LD2.DOC_FUNDED_DATE)=''' + @Year + ''' group by month(LD2.DOC_FUNDED_DATE),lt_loan_stats,lookups.descriptn, lookupprogram.descriptn '
			end
			 --for broker
            else if (@oRole = 3 or @oRole = 20 or @oRole = 21)
            begin
                 	Set @SQL =@SQL + ' and (loanapp.ID in (select * from dbo.SplitString('''+@strID+''','','')))'
            end
			else if(@oRole = 12 )
			begin
				Set @SQL =@SQL + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','','')))'
			end
            else
			begin            
		      	Set @SQL =@SQL + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','','')))'
			end
		end
	end
end

if (@oRole = 12)
begin
	if(len(@strID) =0)
	begin
		Set @SQL =@SQL + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','','')))'	
	end
	else 
	begin
		Set @SQL =@SQL + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','','')))'	
	end
end
Set @SQL =@SQL + ' group by month(AppStatus.StatusDateTime),LoanData.FinancingType, TransTYpe ; '
Set @SQL =@SQL + ' SELECT month(AppStatus.StatusDateTime) as ARmonth, year(AppStatus.StatusDateTime) as ARYear  ,  count(loanapp.CurrentStatus) AS Total, sum(isnull(LoanData.AdjustedNoteAmt, 0)) as SumTotalAmount, round(avg(isnull(LoanData.AdjustedNoteAmt, 0)),0) as AvrageTotalAmount, 
 avg(convert(int,isnull(replace(borr.CompositeCreditScore,'','',''''),0)))  as AvrageCreditScore,sum(convert(int,isnull(replace(borr.CompositeCreditScore,'','',''''),0)))  as TotalCreditScore,count(convert(int,replace(borr.CompositeCreditScore,'','','''')))  as CreditScoreLoanCount ,round(avg(convert(int, isnull(loanData.ltv,0)*100)),2) as AvgLTV,convert(int,sum(convert(float, isnull(loanData.ltv,0)*100))) as TotalLTV  
 from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and  borr.sequencenum=1 
 Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id   and loanData.Active=1
 Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc=''62 - Funded'' 
 Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
 Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' 
where  Year(AppStatus.StatusDateTime) <> ''1900'' and Year(AppStatus.StatusDateTime)='+ @Year
if(@ChannelType = 'broker')
begin
Set @SQL = @SQL + ' and loanapp.ChannelType=''BROKER'''
end
else if (@ChannelType = 'retail')
begin
Set @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL'' '
end
else if (@ChannelType = 'correspond')
begin
Set @SQL = @SQL + ' and loanapp.ChannelType=''CORRESPOND'' '
end
if (@oRole = 1)
begin
Set @SQL = @SQL + ' and loanapp.ChannelType<>''RETAIL'''
end
if(@strTeamOpuids != '')
begin
Set @SQL = @SQL + '  and Originator_id in (select * from dbo.SplitString('''+@strTeamOpuids+''','','')) '
	if (@oRole = 12)
	begin
		Set @SQL = @SQL + '(select * from dbo.SplitString('''+@strTeamOpuids+''','',''))'
	end
end
if(@BrokerName !='')
begin
	 DECLARE @TEMPSTR1 NVARCHAR(1000)  
  SET @TEMPSTR1 = @BrokerName  
  
  IF(CHARINDEX('--',@TEMPSTR1) = 0)  
  BEGIN  
  Set @SQL =@SQL + ' and (Broker.Company=''' + @BrokerName + ''' or Corres.Company=''' + @BrokerName + ''') '  
  END  
end
else
begin
	if(@oRole = 0 and @strTeamOpuids = '')
	begin
		if(len(@strID) = 0)
		begin
			if(@oRole = 13)
			begin
				Set @SQL =@SQL + ' loandat.LT_Broker_Rep in (select * from dbo.SplitString('''+@strID+''','','')) and Year(LD2.DOC_FUNDED_DATE)=''' + @Year + ''' group by month(LD2.DOC_FUNDED_DATE) , year(LD2.DOC_FUNDED_DATE) order by ARMonth'
			end
			 --for broker
            else if (@oRole = 3 or @oRole = 20 or @oRole = 21)
            begin
                 	Set @SQL =@SQL + '  and (loanapp.ID in (select * from dbo.SplitString('''+@strID+''','','')))'
            end
            else
			begin            
		      	Set @SQL =@SQL + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','','')))'
			end
		end
		else
		begin
			if(@oRole = 13)
			begin
				Set @SQL =@SQL + ' loandat.LT_Broker_Rep in (select * from dbo.SplitString('''+@strID+''','','')) and Year(LD2.DOC_FUNDED_DATE)=''' + @Year + ''' group by month(LD2.DOC_FUNDED_DATE) , year(LD2.DOC_FUNDED_DATE) order by ARMonth'
			end
			 --for broker
            else if (@oRole = 3 or @oRole = 20 or @oRole = 21)
            begin
                 	Set @SQL = @SQL + '  and (loanapp.ID in (select * from dbo.SplitString('''+@strID+''','','')))'
            end
			else if(@oRole = 12 )
			begin
				Set @SQL =@SQL + 'and (Originator_id in (select * from dbo.SplitString('''+@strID+''','','')) )'
			end
            else
			begin            
		      	Set @SQL =@SQL + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','','')))'
			end
		end
	end
end
if (@oRole = 12)
begin
	if(len(@strID) =0)
	begin
		Set @SQL =@SQL + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','','')))'	
	end
	else 
	begin
		Set @SQL =@SQL + ' and (Originator_id in (select * from dbo.SplitString('''+@strID+''','','')) )'	
	end
end
Set @SQL =@SQL + ' group by month(AppStatus.StatusDateTime) , year(AppStatus.StatusDateTime) order by ARMonth '



Print @SQL   
exec sp_executesql @SQL  
END  
  
GO

