USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Getmwllookups]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Getmwllookups]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/10/2012
-- Description:	Procedure for GetBusineesType_Role
-- =============================================
CREATE PROCEDURE  [dbo].[sp_Getmwllookups]
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

   select id as id_value,case DisplayString when 'Broker' then 'Wholesale' else DisplayString end as descriptn,BoCode as ChannelType from mwllookups 
where objectName='mwlloanapp' and Fieldname='ChannelType' and DisplayString<>'Retail'
END

GO

