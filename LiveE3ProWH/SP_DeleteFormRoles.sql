USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DeleteFormRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_DeleteFormRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

Create PROCEDURE [dbo].[SP_DeleteFormRoles]
	(
@formID int

)
AS
BEGIN

 Delete from tblCFIformRoles where formid =@formID
      end

GO

