USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Updatetblusers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Updatetblusers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Gourav    
-- Create date: 10/03/2012    
-- Description: Procedure for clsUser.cs bll      
-- =============================================    
CREATE proc [dbo].[usp_Updatetblusers]  --'-1,2,25,26','1'  
(    
@urole varchar(Max),    
@IsUserInNewProcess bit    
)    
as    
begin    
 update tblusers set IsUserInNewProcess=@IsUserInNewProcess   
where urole in (SELECT * FROM dbo.SplitString(@urole,','))  
end    
    
    
GO

