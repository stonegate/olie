USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spQACOnHoldEmailNotification]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spQACOnHoldEmailNotification]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spQACOnHoldEmailNotification]
/*
AUTHOR: RAGHU
DATE: DEC 5, 2012
OBJECTIVE: TO FETCH THE DATA FOR EMAIL NOTIFICATION IN 'QAC On Hold' CONDITION
*/
WITH RECOMPILE AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

WITH AUWD AS
(
SELECT DISTINCT AUWD.loan_no AS LoanNumber,AUWD.userid AS UserID--,count(*) as RepeatedTupple
FROM [tblAdditionalUWDoc] AUWD
GROUP BY loan_no,userid
HAVING COUNT(loan_no)>1 OR COUNT(loan_no)<2
)


Select data.*, tblusers.uemailid as BDMemailID from (
SELECT AUWD.LoanNumber,LoanApp.CurrentStatus,Usr.UEmailID AS Recipient,LoanApp_Borrower.LastName as BorrowerLastName,Usr.aeid
FROM AUWD
LEFT JOIN [mwlLoanApp] LoanApp ON LoanApp.LoanNumber=AUWD.LoanNumber
LEFT JOIN [tblUsers] Usr ON Usr.UserID=AUWD.UserID
LEFT JOIN (
SELECT DISTINCT LoanApp.LoanNumber,
Borr.LastName
FROM [mwlLoanApp] LoanApp
JOIN mwlBorrower Borr ON Borr.LoanApp_ID=LoanApp.ID AND Borr.SequenceNum=1
) LoanApp_Borrower ON LoanApp_Borrower.LoanNumber=AUWD.LoanNumber
--Left join dbo.mwamwuser muser on LoanApp.Originator_id=muser.id
WHERE Usr.UEmailID IS NOT NULL AND CurrentStatus LIKE '%51%' ) as data inner join tblusers on tblusers.userid = data.aeid

GO

