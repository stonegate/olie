USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_InsertConditionTextHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_InsertConditionTextHistory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
      
      
     
  
CREATE  PROCEDURE [dbo].[Sp_InsertConditionTextHistory]  
 @LoanNo char(15),  
 @Cond_Text varchar(1000),  
 @FileName varchar(200),  
 @condrecid varchar(50)=null,  
 @ProlendImageDesc varchar(50)=null,  
 @Userid int=null,  
 @Flag int  
  
AS     
    SET TRANSACTION isolation level READ uncommitted;     
    
   DECLARE @SubmittedDate datetime  
     
   if(@Flag=1)  
    Begin  
   set @SubmittedDate=Getdate()  
    End  
   else  
       Begin  
   set @SubmittedDate=''  
    End    
  BEGIN     
  
  Insert into tblCondText_History  
  (  
   Loan_No,  
   Cond_Text,  
   [FileName],  
   dttime,  
   condrecid,  
   ProlendImageDesc,  
   Userid,  
   SubmittedDate  
  )   
  Values  
  (  
   @LoanNo,  
   @Cond_Text,  
   @FileName,  
   Getdate(),  
   @condrecid,  
   @ProlendImageDesc,  
   @Userid,  
   @SubmittedDate  
  )  
        
       
        
  END     
      
      
GO

