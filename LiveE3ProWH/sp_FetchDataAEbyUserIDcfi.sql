USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchDataAEbyUserIDcfi]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchDataAEbyUserIDcfi]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procEDURE sp_FetchDataAEbyUserIDcfi  
-- Add the parameters for the stored procedure here  
--summary Fetch all Account executive belong to that user  
@userID INT  
AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  
  
-- Insert statements for procedure here  
  
  
BEGIN  
  
select * from tblusers where userid in (@userID)  
And (urole= 19 or urole= 8 or urole= 25 or urole= 26)  
  
END  
  
  
END  
  
GO

