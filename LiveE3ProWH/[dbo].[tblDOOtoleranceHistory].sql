USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDOOtoleranceHistory]') AND type in (N'U'))
DROP TABLE [dbo].[tblDOOtoleranceHistory]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDOOtoleranceHistory](	  [ID] INT NOT NULL IDENTITY(1,1)	, [StepNo] INT NULL	, [LoanNo] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [UserID] INT NULL	, [SessionID] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [HUDLineNo] VARCHAR(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ActualValue] MONEY NULL	, [UpdatedValue] MONEY NULL	, [ModifiedDate] DATETIME NULL DEFAULT(getdate())	, [IsActive] BIT NULL DEFAULT((1))	, CONSTRAINT [PK_tblDOOtoleranceHistory] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO

