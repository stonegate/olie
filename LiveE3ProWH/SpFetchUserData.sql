USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpFetchUserData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpFetchUserData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpFetchUserData] 
	@ComapanyName varchar(500)
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	select top 1 isnull(companyEmail,'')companyEmail   from mwcinstitution where Status='Active' and company like  '%' + @ComapanyName + '%'
END

GO

