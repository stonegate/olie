USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_tblManageTemplate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_tblManageTemplate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Update_tblManageTemplate]          
(          
@ID varchar(50),          
@IsActive bit          
)          
as          
begin  
UPDATE tblManageTemplate WITH (UPDLOCK)  
SET IsActive = @IsActive  
WHERE ID = @ID          
end

GO

