USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_InserttblFormRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_InserttblFormRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

Create PROCEDURE [dbo].[SP_InserttblFormRoles]
	(
@formID int,
@uroleid int
)
AS
BEGIN
	   Insert into tblFormRoles(formID,uroleid) values(@formID,@uroleid)
             
END

GO

