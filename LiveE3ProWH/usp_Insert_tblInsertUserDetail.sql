USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Insert_tblInsertUserDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Insert_tblInsertUserDetail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[usp_Insert_tblInsertUserDetail]
(
@_userid varchar(Max)
,@strClientIPAddress varchar(Max)
)
as
begin
	INSERT INTO tblTracking (UserID,LoginTime,IPAddress) VALUES (@_userid,GETDATE(),@strClientIPAddress)
end



GO

