USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_BulkUpdateMwlInstitution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_BulkUpdateMwlInstitution]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BulkUpdateMwlInstitution]
(
	@SetupInstitution_id VARCHAR(50),
	@Office VARCHAR(50) = NULL,
	@MWIRNCode VARCHAR(50) = NULL,
    @Company VARCHAR(100) = NULL,
	@SetupCompany VARCHAR(100) = NULL,
	@AlternatePayeeName VARCHAR(100) = NULL,    
	@LoanGuid VARCHAR(50)
)
AS
BEGIN
 UPDATE mwlInstitution
SET
    SetupInstitution_id = @SetupInstitution_id,
    office = @Office,
    MWIRNCode = @MWIRNCode,
    Company = @Company,
    SetupCompany = @SetupCompany,
    AlternatePayeeName = @AlternatePayeeName 
WHERE
    objOwner_id = @LoanGuid
    AND InstitutionType = 'Branch' AND objOwnerName = 'BranchInstitution'
END
GO

