USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Delete_tblDeleteUserCLS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Delete_tblDeleteUserCLS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[usp_Delete_tblDeleteUserCLS]
(
@userid varchar(Max)
)
as
begin
update tblusers set isactive = 0
where userid = @userId
end


GO

