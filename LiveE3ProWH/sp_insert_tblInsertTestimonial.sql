USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_insert_tblInsertTestimonial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_insert_tblInsertTestimonial]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Gourav  
-- Create date:   
-- Description: store procedure for clsuser.cs  
-- =============================================  
CREATE proc [dbo].[sp_insert_tblInsertTestimonial]  
(  
@uname varchar(Max)=null  
,@Testimonial varchar(Max)=null  
,@SubmittedDate varchar(Max)=null  
,@UserID varchar(Max)=null  
,@City varchar(Max)=null  
,@State varchar(Max)=null  
,@UpdatedDt varchar(Max)=null  
)  
as  
begin  
if(@SubmittedDate!='')  
begin  
 INSERT INTO tblTestimonials   
 (  
 uname  
 ,Testimonial  
 ,SubmittedDate  
 ,UserID  
 ,UpdatedDt  
 ,City  
 ,State  
 )   
 VALUES   
 (  
 @uname  
 ,@Testimonial  
 ,@SubmittedDate  
 ,@UserID  
 ,getdate()  
 ,@City  
 ,@State  
 )  
 end  
 else  
 begin  
 INSERT INTO tblTestimonials   
 (  
 uname  
 ,Testimonial  
 ,UserID  
 ,UpdatedDt  
 ,City  
 ,State  
 )   
 VALUES   
 (  
 @uname  
 ,@Testimonial  
 ,@UserID  
 ,getdate()  
 ,@City  
 ,@State  
 )  
 end  
end  
GO

