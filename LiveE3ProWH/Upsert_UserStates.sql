USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Upsert_UserStates]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Upsert_UserStates]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author, Somnath M. R.>
-- Create date: <Create Date, 09th Dec 2013>
-- Description:	<Description, Insert into tblUserStates table>
-- EXEC Upsert_UserStates '1', 16524, 1
-- =============================================
CREATE PROCEDURE [dbo].[Upsert_UserStates]
	@StateIds NVARCHAR(MAX),
	@UserId INT,
	@CreatedBy [NVARCHAR](50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	INSERT INTO [dbo].[tblUserStates] ([UserId], [StateId], [CreatedBy], [CreatedOn])
		SELECT @UserId, Id.Items, @CreatedBy, GetDate() FROM [dbo].SplitString(@StateIds, ',') Id
		WHERE 
			Id.Items NOT IN (SELECT UC.StateId FROM [dbo].[tblUserStates] UC WHERE UC.UserId = @UserId)

	DELETE FROM [dbo].[tblUserStates] 
	WHERE
		UserId = @UserId 
		AND StateId NOT IN (SELECT Id.Items FROM [dbo].SplitString(@StateIds, ',') Id)
END

GO

