USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetProlenderUserData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetProlenderUserData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetProlenderUserData] 
	@Email varchar(500)
AS
BEGIN
	select record_id from users where login =@Email
END

GO

