USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetCarrierData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetCarrierData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetCarrierData]
		
AS
BEGIN
	select * from Carrier order by orderid
END

GO

