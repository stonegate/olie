USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpInsertUsers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpInsertUsers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SpInsertUsers] 
	@UFirstname varchar(500)=null,
	@ULastName  varchar(500)=null,
	@UEmailId varchar(500)=null,
	@StateBelongTo varchar(500)=null,
	@Urole int=0,
	@IsAdmin int=0,
	@IsActive int=0,
	@UidProlender varchar(200)=null,
	@Uparent int=0,
	@Upassword nvarchar(500)=null,
	@MobilePh nvarchar(500)=null,
	@Phonenum nvarchar(500)=null,
	@LoginIdProlender varchar(500)=null,
	@Carrerid int =null,
	@MI varchar(500)=null,
	@CompanyName varchar(500)=null,
	@AEID int,
	@Region varchar(500)=null
AS
BEGIN
	Insert into tblusers(ufirstname,ulastname,uemailid,StateBelongTo,urole,isadmin,isactive,uidprolender,uparent,upassword,mobile_ph,phonenum,loginidprolender,carrierid,MI,companyname, AEID, Region) 
	Values(@UFirstname,@ULastName,@UEmailId,@StateBelongTo,@Urole,@IsAdmin,@IsActive,@UidProlender,@Uparent,@Upassword,@MobilePh,@Phonenum,@LoginIdProlender,@Carrerid,@MI,@CompanyName,@AEID,@Region)
select @@IDENTITY
END
GO

