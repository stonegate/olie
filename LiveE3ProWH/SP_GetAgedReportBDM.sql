USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetAgedReportBDM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetAgedReportBDM]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create Procedure Coding Starts Here

CREATE PROCEDURE [dbo].[SP_GetAgedReportBDM]
	(
@Registered nvarchar(200),
@oRole nvarchar(250) = null,
@ConditionType nvarchar(250),
@Waived nvarchar(250),
@CustomeFieldCorrespondentType nvarchar(500),
@strID nvarchar(max) = null
)
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

Declare @SQL nvarchar(max)
Set @SQL = ''
Set @SQL = 'select ''E3'' as DB,loanapp.OriginatorName,loanapp.Channeltype as BusType,loanapp.casenum as FHANumber,
                 loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,
                 loanapp.LoanNumber as loan_no,loanapp.CloserName as Closer,
                 borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,
                 '''' as LU_FINAL_HUD_STATUS ,loanapp.CurrPipeStatusDate  AS SubmittedDate,
                 case when loanapp.TransType is null then '''' when loanapp.TransTYpe = ''P''
                 then ''Purchase'' when loanapp.TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,
                 LookupsPro.DisplayString as LoanProgram, loanData.AdjustedNoteAmt as loan_amt,
                 LockRecord.Status as LockStatus,LockRecord.LockExpirationDate,LockRecord.LockDateTime,
                 UnderwriterName as UnderWriter, CASE ISNULL(RTRIM(Broker.Company),'''')
                 WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName
                 ,Lookups.DisplayString as CashOut, CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, 
                 convert(varchar(35),loanapp.EstCloseDate,101) AS ScheduleDate , 
                 CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a 
                 where DueBy=''Prior to Closing'' and Category !=''LENDER'' and
                 Category !='''+@ConditionType + ''' and a.objOwner_ID IN
                ( SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE
                 loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0
                 WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where 
                 (DueBy=''Prior to Closing'' and Category !=''LENDER'' and 
                 Category !=''' +@ConditionType + ''')and (CurrentState=''CLEARED'' OR 
                 CurrentState=''SUBMITTED'' or CurrentState=''' +@Waived + ''')
                 and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp as loan2
                 WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0 ELSE 
                (SELECT count(a.ID) as TotalCleared 
                 FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and 
                 Category !=''LENDER'' and Category !=''' +@ConditionType + ''')
                 and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED''
                 or CurrentState=''' +@ConditionType + ''') 
                 and a.objOwner_ID IN 
                 (SELECT ID FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 /
                 (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where
                 ( DueBy=''Prior to Closing'' and Category !=''LENDER'' and
                  Category !=''' +@ConditionType + ''') 
                  and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1
                  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER 
                 ,isnull(Cust.StringValue,'''') as CorrespondentType 
                 from mwlLoanApp loanapp 
                 Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id 
                 and appStat.StatusDesc='''+ @Registered +'''
                 left join mwlLoanApp loanappcorr on loanappcorr.id = loanapp.id
                 and loanappcorr.Channeltype=''CORRESPOND'' 
                 left join mwlLoanApp loanappBroker on loanappBroker.id = loanapp.id 
                and loanappBroker.Channeltype=''BROKER'' 
                 Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id 
                 Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
                 left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id 
                 Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id 
                 and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker'' 
                 left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id
                 and LockRecord.LockType=''LOCK'' and LockRecord.Status<> ''CANCELED''
                 Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode
                 and ObjectName=''mwlLoanData'' and fieldname=''refipurpose'' 
                 Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id 
                 and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''
                 Left outer join mwlLookups LookupsPro on LoanData.FinancingType=LookupsPro.BOCode
                 and LookupsPro.ObjectName=''mwlLoanData'' and LookupsPro.fieldname=''FinancingType'' 
                 left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id and
                 Cust.CustFieldDef_ID ='''+ @CustomeFieldCorrespondentType +''' 
                 where borr.sequencenum=1 and loanapp.LoanNumber is not null
                 and loanapp.LoanNumber <> '''' and 
                 loanapp.CurrentStatus IN ( select * from dbo.SplitString('+@Registered+','',''))'

                if (@oRole = 0)
                Begin
                    Set @SQL =   @SQL+  ' and (loanapp.Originator_id in
                    (select * from dbo.SplitString('+@strID+','','')))'
                end
                Set @SQL =   @SQL+ ' and datediff(day,loanapp.CurrPipeStatusDate,getdate())>=30  order by loanapp.CurrPipeStatusDate asc '

Print @SQL 
exec sp_executesql @SQL
END

-- Create Procedure Coding ENDS Here


GO

