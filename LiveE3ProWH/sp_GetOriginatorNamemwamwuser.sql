USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetOriginatorNamemwamwuser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetOriginatorNamemwamwuser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Nitin  
-- Create date: 10/06/2012  
-- Description: Procedure for clspipelineE3 BLL  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_GetOriginatorNamemwamwuser]  
(  
@Id varchar(32)  
)  
AS  
BEGIN  
   
 SET NOCOUNT ON;  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
  
 select  FirstName + ' ' + LastName as OriginatorName from mwamwuser where Id=@Id  
  
END  
GO

