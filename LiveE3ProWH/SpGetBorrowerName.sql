USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetBorrowerName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetBorrowerName]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <Stonegate>        
-- Create date: <10/6/2012>        
-- Description: <Borrower Name by Loan number>        
-- =============================================        
CREATE PROCEDURE [dbo].[SpGetBorrowerName]       
(        
 @LoanNo nvarchar(100)    
)        
AS        
BEGIN        
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

SELECT rtrim(LastName) as borr_last FROM mwlBorrower as borr
 INNER JOIN mwlloanapp  loanapp ON loanapp.ID = borr.loanapp_id WHERE LoanNumber =@LoanNo
        
END 

GO

