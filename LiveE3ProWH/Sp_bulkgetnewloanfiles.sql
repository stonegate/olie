USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_bulkgetnewloanfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_bulkgetnewloanfiles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROCEDURE [dbo].[Sp_bulkgetnewloanfiles]
AS
  BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET nocount ON;

      BEGIN
          SELECT [FileName],
                 [Type],
				 [ChannelType],
                 [CreatedBy],
				 [CompanyName]
          FROM   [dbo].[tblbulkloanfiles]
          WHERE  [Status] = 'NEW'

          UPDATE [dbo].[tblbulkloanfiles]
          SET    [status] = 'IN PROCESS',
                 [modifiedby] = 'WS',
                 [modifiedon] = Getdate()
          WHERE  [status] = 'NEW'
      END
  END  

GO

