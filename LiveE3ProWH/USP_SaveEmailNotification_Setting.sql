USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_SaveEmailNotification_Setting]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_SaveEmailNotification_Setting]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Abhishek Karn>
-- Create date: <08-01-2014>
-- Description:	<This is to insert DOO Email Notification  Setting in tblEmergingBankers Table Settings>
-- =============================================
CREATE PROCEDURE [dbo].[USP_SaveEmailNotification_Setting]
(
@PartnerCompanyId varchar(100) = null,
@PartnerCompanyName nvarchar(400) = null,
@RoleId bigint = null,
@EmailNotification varchar(2) = null,
@CreatedBy varchar(2) = null
)
AS
BEGIN

	--SET NOCOUNT ON;

	IF EXISTS (Select 1 from tblEmergingBankers Where PartnerCompanyId = @PartnerCompanyId and Flag = 'DOO' and Status = 'Y'  )
	BEGIN

	Update DOO
	Set IsEmergingBanker = @EmailNotification,
		ModifiedBy = @CreatedBy,
		ModifiedDate = getdate()
	from tblEmergingBankers DOO Where 
	PartnerCompanyId = @PartnerCompanyId and Flag = 'DOO' and Status = 'Y'

	END
	ELSE 
	BEGIN

	 insert into tblEmergingBankers(PartnerCompanyId,CompanyName,RoleId,IsEmergingBanker,Flag,Status,CreatedBy,CreatedDate)
	 VALUES (@PartnerCompanyId,@PartnerCompanyName,@RoleId,@EmailNotification,'DOO','Y',@CreatedBy,getdate())

	END
   
END


GO

