USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblSetUserLogo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblSetUserLogo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblSetUserLogo]
(
@iUserID varchar(Max),
@logoname varchar(Max)
)
as
begin
update tblusers Set logoname =@logoname  Where userid =@iUserID
end


GO

