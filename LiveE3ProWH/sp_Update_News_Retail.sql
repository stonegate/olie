USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_News_Retail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_News_Retail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  
  
CREATE PROCEDURE [dbo].[sp_Update_News_Retail]     
(    
@id int,          
@Title nvarchar(1000),       
@Body text,        
@IsActive bit,        
@ModifiedDate datetime,        
@Role nvarchar(Max),  
@RoleC nvarchar(Max),  
@RoleR nvarchar(Max)  
)    
AS    
BEGIN  
UPDATE Retail_tblNewsAndAnnouncement WITH (UPDLOCK)  
SET Title = @Title,  
 Body = @Body,  
 IsActive = @IsActive,  
 Role = @Role,  
  RoleC = @RoleC, 
   RoleR = @RoleR, 
 CreatedDate = @ModifiedDate  
WHERE id = @id  
select '1'  
END

GO

