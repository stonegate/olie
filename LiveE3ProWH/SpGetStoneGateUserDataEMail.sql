USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetStoneGateUserDataEMail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetStoneGateUserDataEMail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetStoneGateUserDataEMail] 
	@Emailid varchar(500)
AS
BEGIN
	select * from tblusers where uemailid=@Emailid
END

GO

