USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Getmwlinstitution]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Getmwlinstitution]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Nitin  
-- Create date: 10/06/2012  
-- Description: Procedure for clspipelineE3 BLL  
-- =============================================  
CREATE procEDURE [dbo].[sp_Getmwlinstitution]  
(  
@Loannumber varchar(500)
)  
AS  
BEGIN  
   
 SET NOCOUNT ON; 
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
  
select EmployerIDNumber from mwlinstitution loaninst   
inner join  mwlloanapp loanapp  on loanapp.id = loaninst.objowner_id   
where EmployerIDNumber IS NOT NULL and Loannumber =@Loannumber  
END  
  
GO

