USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblFetchScheduleClosing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblFetchScheduleClosing]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  Gourav        
-- Create date: 10/03/2012        
-- Description: Procedure for clspipeline.cs bll          
-- =============================================        
CREATE PROCEDURE [dbo].[sp_Select_tblFetchScheduleClosing]        
--(  
----Define for Future Purpose but Still is never used in this procedure  
--@Userid varchar(max)  
--)  
AS        
BEGIN        

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

select distinct ScheduleID  
-- ,(TSC.LoanNo +' - Closing Schedule Date Request - ****PENDING****') as LoanNo  
 ,TSC.LoanNo as LoanNo 
 ,TSC.SchDate  
 ,TSC.FeeSheet  
 ,TSC.HomeownerInsurance  
 ,TSC.TitleCommitment  
 ,TSC.InsuredClosingProtectionLetter  
 ,TSC.RateLockConfirmation  
 ,TSC.Invoices  
 ,TSC.FinalLocked  
 ,TSC.Userid  
 ,TSC.IsActive  
 ,TSC.SubmitedDate  
 ,TSC.Comments  
 ,TSC.IsFromLoanSuspendedDoc  
 ,TSC.Time,(Convert(varchar(30),TSC.schdate,111)+' '+TSC.Time) as AppointmentDTime   
,MLA.transtype  
 --,MLA.transtype  
-- ,CASE MLA.retailloanapp  
-- WHEN 1 THEN 'RE'  
-- WHEN 0 THEN 'WH'  
-- END as transtype  
 ,MLA.mwbranchcode   
 --,MLA.retailloanapp   
,CASE MLA.retailloanapp  
 WHEN 1 THEN 'RE'  
 WHEN 0 THEN 'WH'  
 END as retailloanapp 

,(mbr.lastname+', '+TSC.LoanNo+', '+(CASE MLA.transtype  
 WHEN 'P' THEN 'Purchase'  
 WHEN 'R' THEN 'Refinance'  
 END)+', '+MLA.CurrentStatus+', '+mla.originatorname+(CASE tsc.appStatus 
 WHEN  0 THEN ', ***PENDING***'  
 WHEN 1 THEN ''  
 END)+ ' ' + (CASE tsc.RushStatus 
 WHEN  1 THEN 'RUSH'  
 WHEN 0 THEN ''  
 END)+ ' ' + (CASE tsc.ReDrawStatus 
 WHEN  1 THEN 'REDRAW'  
 WHEN 0 THEN ''  
 END) ) as Subject
 ,
tsc.appStatus 
,tsc.RushStatus
,tsc.ReDrawStatus
 from dbo.tblScheduleClosing TSC left join mwlLoanApp MLA on TSC.LoanNo=MLA.LoanNumber 
inner join mwlborrower mbr on mbr.loanapp_id=MLA.id 
 where (Convert(varchar(30),TSC.schdate,111)+' '+TSC.Time) is not null  

--Previous Start Query--
-- select ScheduleID  
-- ,(TSC.LoanNo +' - Closing Schedule Date Request - ****PENDING****') as LoanNo  
-- ,TSC.SchDate  
-- ,TSC.FeeSheet  
-- ,TSC.HomeownerInsurance  
-- ,TSC.TitleCommitment  
-- ,TSC.InsuredClosingProtectionLetter  
-- ,TSC.RateLockConfirmation  
-- ,TSC.Invoices  
-- ,TSC.FinalLocked  
-- ,TSC.Userid  
-- ,TSC.IsActive  
-- ,TSC.SubmitedDate  
-- ,TSC.Comments  
-- ,TSC.IsFromLoanSuspendedDoc  
-- ,TSC.Time,(Convert(varchar(30),TSC.schdate,111)+' '+TSC.Time) as AppointmentDTime   
-- ,MLA.transtype  
-- ,MLA.mwbranchcode   
-- ,MLA.retailloanapp   
-- from dbo.tblScheduleClosing TSC left join mwlLoanApp MLA on TSC.LoanNo=MLA.LoanNumber  
-- where (Convert(varchar(30),TSC.schdate,111)+' '+TSC.Time) is not null  
  
--select ScheduleID  
-- ,(TSC.LoanNo +' - Closing Schedule Date Request - ****PENDING****') as LoanNo  
-- ,TSC.SchDate  
-- ,TSC.FeeSheet  
-- ,TSC.HomeownerInsurance  
-- ,TSC.TitleCommitment  
-- ,TSC.InsuredClosingProtectionLetter  
-- ,TSC.RateLockConfirmation  
-- ,TSC.Invoices  
-- ,TSC.FinalLocked  
-- ,TSC.Userid  
-- ,TSC.IsActive  
-- ,TSC.SubmitedDate  
-- ,TSC.Comments  
-- ,TSC.IsFromLoanSuspendedDoc  
-- ,TSC.Time,(Convert(varchar(30),TSC.schdate,111)+' '+TSC.Time) as AppointmentDTime   
--,MLA.transtype  
-- --,MLA.transtype  
---- ,CASE MLA.retailloanapp  
---- WHEN 1 THEN 'RE'  
---- WHEN 0 THEN 'WH'  
---- END as transtype  
-- ,MLA.mwbranchcode   
-- --,MLA.retailloanapp   
--,CASE MLA.retailloanapp  
-- WHEN 1 THEN 'RE'  
-- WHEN 0 THEN 'WH'  
-- END as retailloanapp  
-- from dbo.tblScheduleClosing TSC left join mwlLoanApp MLA on TSC.LoanNo=MLA.LoanNumber  
-- where (Convert(varchar(30),TSC.schdate,111)+' '+TSC.Time) is not null  

--UPDATE TBLSCHEDULECLOSING SET appstatus=1, RUSHSTATUS=0,REDRAWSTATUS=1 WHERE LOANNO='0000316390'
--  select top 1 * from mwlborrower where loanapp_id ='00003342032A4117A8B1D0B507B46B7E'
-- select  currentstatus,decisionstatus,loanstate from mwlloanapp where currentstatus not like '%clear%'order by id desc
--select top 1 * from mwlloanapp order by id desc
--mwlborrower.lastname
--mwlloanapp.loannumber
--mwlloanapp.transtype
--mwlLoanApp.CurrentStatus
--mwlloanapp.originatorname
--IF closing request status = "Pending" then result = "PENDING". If closing request status = "Approved" then result = "" or blank.
--END Query--

--select mwbranchcode from mwlLoanApp
  
--UPDATE TBLSCHEDULECLOSING SET appstatus=1, RUSHSTATUS=0,REDRAWSTATUS=1 WHERE LOANNO='0000316390'
--UPDATE TBLSCHEDULECLOSING SET appstatus=0, RUSHSTATUS=1,REDRAWSTATUS=1 WHERE LOANNO='0000316695'
--UPDATE TBLSCHEDULECLOSING SET appstatus=1, RUSHSTATUS=0,REDRAWSTATUS=1 WHERE LOANNO='0000317962'

end
GO

