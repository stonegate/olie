USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_tblInsertQuickLinks]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_tblInsertQuickLinks]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Insert_tblInsertQuickLinks]
as
begin
INSERT INTO tblQuickLinks(ID,Linkname) VALUES (1,'Price a Loan')
INSERT INTO tblQuickLinks(ID,Linkname) VALUES (2,'Register a Loan')
INSERT INTO tblQuickLinks(ID,Linkname) VALUES (3,'Lock the Loan')
INSERT INTO tblQuickLinks(ID,Linkname) VALUES (4,'Submit the Loan')
INSERT INTO tblQuickLinks(ID,Linkname) VALUES (5,'Order FHA Case Number')
INSERT INTO tblQuickLinks(ID,Linkname) VALUES (6,'Submit Conditions')
INSERT INTO tblQuickLinks(ID,Linkname) VALUES (7,'Schedule Closing Date')
INSERT INTO tblQuickLinks(ID,Linkname) VALUES (8,'Order an Appraisal')
end



GO

