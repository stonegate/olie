USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDOOToleranceHistoryForStep4And6]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDOOToleranceHistoryForStep4And6]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
     
-- =============================================              
-- Author:  <Kanva Patel>              
-- Create date: <14th June 2013>              
-- Description: <This will get tolerance history for given loan number>          
-- =============================================              
CREATE PROCEDURE [dbo].[sp_GetDOOToleranceHistoryForStep4And6]      
(          
   @LoanNo varchar(10),
   @StepNo int
)      
AS       
BEGIN              
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;       
SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,               
                   -- the entire transaction is terminated and rolled back.      
 
--For Step 4     
IF @StepNo = 4
Begin      

Declare @Is1101 varchar(10); -- To check if the 1101 HUD Line is there in History table or not

select  @Is1101 = HUDLineNo From tblDOOToleranceHistory where loanno = @LoanNo And stepno = @StepNo And Hudlineno = '1101'

If @Is1101 = '' or @Is1101 is null -- If 1101 is not available
Begin
		Select       
			HUDLineNo,'$ '+ convert(varchar(50),CONVERT(DECIMAL(30,2),ActualValue)) As ActualValue, '$ '+ convert(varchar(50),CONVERT(DECIMAL(30,2),UpdatedValue)) As UpdatedValue, UpdatedValue - ActualValue As Differance      
		From       
			tblDOOToleranceHistory       
		Where      
			LoanNo = @LoanNo
			And StepNo = @StepNo		
			And (UpdatedValue - ActualValue) > 100
End
	
Else
Begin
		Select       
		HUDLineNo,'$ '+ convert(varchar(50),CONVERT(DECIMAL(30,2),ActualValue)) As ActualValue, 
		'$ '+ convert(varchar(50),CONVERT(DECIMAL(30,2),ActualValue + (Select 
				sum(UpdatedValue - ActualValue) 
				from tblDOOToleranceHistory Where LoanNo = @LoanNo And StepNo = @StepNo 
					And UpdatedValue > ActualValue					
					And HudLineno in ('1102','1104','1105','1106','1109','1110','1111','1112') ) )) As UpdatedValue,  
				--'1102','1104','1105','1106','1109','1110','1111','1112. We need to make sum of all hud line number except 1103 for $100 aggregrate error message.
				--1101 is aggrigate HUD line number for 1100 HUD lines except 1103
				(UpdatedValue - ActualValue)As Differance 
					  
  From       
		tblDOOToleranceHistory       
  Where      
		LoanNo = @LoanNo
		And StepNo = @StepNo
		And HUDLineNo = '1101'		
		And (Select 
				convert(Decimal(30,2),sum(UpdatedValue - ActualValue))
				from tblDOOToleranceHistory Where LoanNo = @LoanNo And StepNo = @StepNo 
					And UpdatedValue > ActualValue					
					And HudLineno in ('1102','1104','1105','1106','1109','1110','1111','1112')) > 100 
						--'1102','1104','1105','1106','1109','1110','1111','1112. We need to make sum of all hud line number except 1103 for $100 aggregrate error message.
						--1101 is aggrigate HUD line number for 1100 HUD lines except 1103
				
		And UpdatedValue > 100

 UNION All
  Select       
		HUDLineNo,'$ '+ convert(varchar(50),CONVERT(DECIMAL(30,2),ActualValue)) As ActualValue, '$ '+ convert(varchar(50),CONVERT(DECIMAL(30,2),UpdatedValue)) As UpdatedValue, UpdatedValue - ActualValue As Differance      
  From       
		tblDOOToleranceHistory       
  Where      
		LoanNo = @LoanNo
		And StepNo = @StepNo
		And (UpdatedValue - ActualValue) > 100
End
  

End --End step 4


--For Step 6
IF @StepNo = 6
Begin    

Declare @Is1301 varchar(10); -- To check if the 1301 HUD Line is there in History table or not

select  @Is1301 = HUDLineNo From tblDOOToleranceHistory where loanno = @LoanNo And stepno = @StepNo And Hudlineno = '1301'

If @Is1301 = '' or @Is1301 is null -- If 1301 is not available
Begin
		Select       
		HUDLineNo,'$ '+ convert(varchar(50),CONVERT(DECIMAL(30,2),ActualValue)) As ActualValue, '$ '+ convert(varchar(50),CONVERT(DECIMAL(30,2),UpdatedValue)) As UpdatedValue, UpdatedValue - ActualValue As Differance      
	    From       
			tblDOOToleranceHistory       
		Where      
			LoanNo = @LoanNo
			And StepNo = @StepNo			
			And (UpdatedValue - ActualValue) > 100
End

Else -- If 1301 is available
Begin
		  
  Select       
		HUDLineNo,'$ '+ convert(varchar(50),CONVERT(DECIMAL(30,2),ActualValue)) As ActualValue, 
		'$ '+ convert(varchar(50),CONVERT(DECIMAL(30,2),ActualValue + (Select 
				sum(UpdatedValue - ActualValue) 
				from tblDOOToleranceHistory Where LoanNo = @LoanNo And StepNo = @StepNo 
					And UpdatedValue > ActualValue
					And HudLineno not in ('1301') --1301 is aggrigate HUD line number for 1302 and 1303 HUD lines					
					And HudLineno in ('1302','1303')) )) As UpdatedValue, 
					--We need to make sum of all hud line numbers '1302','1303' for $100 aggregrate error message.
		  (UpdatedValue - ActualValue)As Differance    
  From       
		tblDOOToleranceHistory       
  Where      
		LoanNo = @LoanNo
		And StepNo = @StepNo
		And HUDLineNo = '1301'
		And (Select 
				sum(UpdatedValue - ActualValue) 
				from tblDOOToleranceHistory Where LoanNo = @LoanNo And StepNo = @StepNo 
					And UpdatedValue > ActualValue
					And HudLineno not in ('1301') --1301 is aggrigate HUD line number for 1302 and 1303 HUD lines
					And HudLineno in ('1302','1303'))  > 100 -- $100 is tolerance amount
					--We need to make sum of all hud line numbers '1302','1303' for $100 aggregrate error message.
 UNION All
  Select       
		HUDLineNo,'$ '+ convert(varchar(50),CONVERT(DECIMAL(30,2),ActualValue)) As ActualValue, '$ '+ convert(varchar(50),CONVERT(DECIMAL(30,2),UpdatedValue)) As UpdatedValue, UpdatedValue - ActualValue As Differance      
  From       
		tblDOOToleranceHistory       
  Where      
		LoanNo = @LoanNo
		And StepNo = @StepNo
		And (UpdatedValue - ActualValue) > 100  --$100 is tolerance amount
End

End

                      
END 

-- Create Procedure Coding ENDS Here

GO

