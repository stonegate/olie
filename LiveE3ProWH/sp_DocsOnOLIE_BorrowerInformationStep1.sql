USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DocsOnOLIE_BorrowerInformationStep1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DocsOnOLIE_BorrowerInformationStep1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================        
-- Author:  Vipul Thakkar        
-- Create date: April, 18 2012        
-- Description: This will give Loan details for the loan number passed  
-- MWFieldNum = 9854 - Borrower 1 AKA 1  
-- MWFieldNum = 9853 - Borrower 1 AKA 2 
-- MWFieldNum = 9850 - Borrower 2 AKA #1
-- MWFieldNum = 9849 - Borrower 2 AKA #2

-- =============================================        
CREATE PROCEDURE [dbo].[sp_DocsOnOLIE_BorrowerInformationStep1]
    (
      @LoanNumber VARCHAR(10)
    )
AS 
    BEGIN  
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
		SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error,         
                   -- the entire transaction is terminated and rolled back.   
		DECLARE @intBorrower1AKA1 AS INT 
		SET 	@intBorrower1AKA1 = 9854 -- -- Borrower 1 AKA #1 Customfield value
		DECLARE @intBorrower1AKA2 AS INT 
		SET 	@intBorrower1AKA2 = 9853 -- -- Borrower 1 AKA #2 Customfield value
		DECLARE @intBorrower1AKA3 AS INT 
		SET 	@intBorrower1AKA3 = 9852 -- -- Borrower 1 AKA #3 Customfield value
		DECLARE @intBorrower1AKA4 AS INT 
		SET 	@intBorrower1AKA4 = 9851 -- -- Borrower 1 AKA #4 Customfield value


		DECLARE @intBorrower2AKA1 AS INT 
		SET 	@intBorrower2AKA1 = 9850 -- -- Borrower 2 AKA #1 Customfield value
		DECLARE @intBorrower2AKA2 AS INT 
		SET 	@intBorrower2AKA2 = 9849 --	-- Borrower 2 AKA #2 Customfield value
		DECLARE @intBorrower2AKA3 AS INT 
		SET 	@intBorrower2AKA3 = 9848 -- -- Borrower 2 AKA #3 Customfield value
		DECLARE @intBorrower2AKA4 AS INT 
		SET 	@intBorrower2AKA4 = 9847 --	-- Borrower 2 AKA #4 Customfield value

		DECLARE @intBorrower3AKA1 AS INT 
		SET 	@intBorrower3AKA1 = 9846 -- -- Borrower 3 AKA #1 Customfield value
		DECLARE @intBorrower3AKA2 AS INT 
		SET 	@intBorrower3AKA2 = 9845 --	-- Borrower 3 AKA #2 Customfield value
		DECLARE @intBorrower3AKA3 AS INT 
		SET 	@intBorrower3AKA3 = 9844 -- -- Borrower 3 AKA #3 Customfield value
		DECLARE @intBorrower3AKA4 AS INT 
		SET 	@intBorrower3AKA4 = 9843 --	-- Borrower 3 AKA #4 Customfield value
				   
				      
        SELECT  LoanNumber ,
                borr.Title AS borrTitle ,
                borr.Suffix AS borrSuffix ,
                borr.FirstName ,
                borr.MiddleName ,
                borr.LastName ,
                borr.TaxIDNum ,
                borr.MaritalStatus ,
                borrAK1.StringValue AS BorrowerAK1 ,
                borrAK2.StringValue AS BorrowerAK2 ,
				borrAK3.StringValue AS BorrowerAK3 ,
                borrAK4.StringValue AS BorrowerAK4 ,
				loanapp.TitleVestingPreview AS Vesting,--borr.Vesting AS Vesting,
				borr.SequenceNum  AS Borrower1,
                coborr.FirstName AS CoFirstname ,
                coborr.MiddleName AS CoMiddlename ,
                coborr.LastName AS CoLastname ,
                coborr.Title AS coborrTitle ,
                coborr.Suffix AS coborrSuffix ,
                coborr.TaxIDNum AS CoTaxIDnum ,
                coborr.MaritalStatus AS CoMaritailStatus ,
                coborrAK1.StringValue AS CoBorrowerAK1 ,
                coborrAK2.StringValue AS CoBorrowerAK2 ,
				coborrAK3.StringValue AS CoBorrowerAK3 ,
                coborrAK4.StringValue AS CoBorrowerAK4 ,
				coborr.SequenceNum AS Borrower2,


				coborr3.FirstName AS Co3Firstname ,
                coborr3.MiddleName AS Co3Middlename ,
                coborr3.LastName AS Co3Lastname ,
                coborr3.Title AS co3borrTitle ,
                coborr3.Suffix AS co3borrSuffix ,
                coborr3.TaxIDNum AS Co3TaxIDnum ,
                coborr3.MaritalStatus AS Co3MaritailStatus ,
                coborr3AK1.StringValue AS CoBorrower3AK1 ,
                coborr3AK2.StringValue AS CoBorrower3AK2 ,
				coborr3AK3.StringValue AS CoBorrower3AK3 ,
                coborr3AK4.StringValue AS CoBorrower3AK4 ,
				coborr3.SequenceNum AS Borrower3,

	            coborr4.FirstName AS Co4Firstname ,
                coborr4.MiddleName AS Co4Middlename ,
                coborr4.LastName AS Co4Lastname ,
                coborr4.Title AS co4borrTitle ,
                coborr4.Suffix AS co4borrSuffix ,
                coborr4.TaxIDNum AS Co4TaxIDnum ,
                coborr4.MaritalStatus AS Co4MaritailStatus ,
				coborr4.SequenceNum  AS Borrower4,

                nonborr.Title AS nonborrtitle ,
                nonborr.Suffix AS nonborrsuffix ,
                nonborr.FirstName AS NonFirstname ,
                nonborr.LastName AS NonLastname ,
                nonborr.MiddleName AS NonMiddlename ,
                nonborr.TitleHolder AS NonTitleHolder ,
                nonborr.BirthDate AS NonBirthDate ,
                borr.FullName AS BorrowerFullname ,
                nonborr.Taxidnum AS nonborrtaxid ,
                nonborr.TaxidType AS nonborrTaxidtype ,
                Case When nonborr.Birthdate IS NULL Then NULL Else DATEDIFF(hour, nonborr.Birthdate, GETDATE()) / 8766 End AS NoborrAge,
                nonborr.Borrower_ID AS AsocciateBorrowerID
                FROM    mwlLoanApp loanapp  
                LEFT JOIN mwlBorrower AS borr ON borr.loanapp_id = loanapp.id AND borr.sequencenum = 1
                LEFT JOIN mwlBorrower AS coborr ON coborr.loanapp_id = loanapp.id AND coborr.sequencenum = 2
				LEFT JOIN mwlBorrower as coborr3 on coborr3.loanapp_id = loanapp.id AND coborr3.sequencenum = 3
				LEFT JOIN mwlBorrower as coborr4 on coborr4.loanapp_id = loanapp.id AND coborr4.sequencenum = 4
                LEFT JOIN mwlNonBorrower AS nonborr ON nonborr.loanapp_id = loanapp.id
                LEFT JOIN mwlCustomField AS borrAK1 ON loanapp.ID = borrAK1.LoanAPP_ID AND borrAK1.CustFieldDef_ID = ( SELECT ID FROM mwsCustFieldDef WHERE MWFieldNum = @intBorrower1AKA1 )
                LEFT JOIN mwlCustomField AS borrAK2 ON loanapp.ID = borrAK2.LoanAPP_ID AND borrAK2.CustFieldDef_ID = ( SELECT ID FROM mwsCustFieldDef WHERE MWFieldNum = @intBorrower1AKA2 )
				LEFT JOIN mwlCustomField AS borrAK3 ON loanapp.ID = borrAK3.LoanAPP_ID AND borrAK3.CustFieldDef_ID = ( SELECT ID FROM mwsCustFieldDef WHERE MWFieldNum = @intBorrower1AKA3 )
                LEFT JOIN mwlCustomField AS borrAK4 ON loanapp.ID = borrAK4.LoanAPP_ID AND borrAK4.CustFieldDef_ID = ( SELECT ID FROM mwsCustFieldDef WHERE MWFieldNum = @intBorrower1AKA4 )
                LEFT JOIN mwlCustomField AS coborrAK1 ON loanapp.ID = coborrAK1.LoanAPP_ID AND coborrAK1.CustFieldDef_ID = ( SELECT ID FROM mwsCustFieldDef WHERE MWFieldNum = @intBorrower2AKA1 )
                LEFT JOIN mwlCustomField AS coborrAK2 ON loanapp.ID = coborrAK2.LoanAPP_ID AND coborrAK2.CustFieldDef_ID = ( SELECT ID FROM mwsCustFieldDef WHERE MWFieldNum = @intBorrower2AKA2 )
				LEFT JOIN mwlCustomField AS coborrAK3 ON loanapp.ID = coborrAK3.LoanAPP_ID AND coborrAK3.CustFieldDef_ID = ( SELECT ID FROM mwsCustFieldDef WHERE MWFieldNum = @intBorrower2AKA3 )
                LEFT JOIN mwlCustomField AS coborrAK4 ON loanapp.ID = coborrAK4.LoanAPP_ID AND coborrAK4.CustFieldDef_ID = ( SELECT ID FROM mwsCustFieldDef WHERE MWFieldNum = @intBorrower2AKA4 )
				LEFT JOIN mwlCustomField AS coborr3AK1 ON loanapp.ID = coborr3AK1.LoanAPP_ID AND coborr3AK1.CustFieldDef_ID = ( SELECT ID FROM mwsCustFieldDef WHERE MWFieldNum = @intBorrower3AKA1 )
                LEFT JOIN mwlCustomField AS coborr3AK2 ON loanapp.ID = coborr3AK2.LoanAPP_ID AND coborr3AK2.CustFieldDef_ID = ( SELECT ID FROM mwsCustFieldDef WHERE MWFieldNum = @intBorrower3AKA2 )
				LEFT JOIN mwlCustomField AS coborr3AK3 ON loanapp.ID = coborr3AK3.LoanAPP_ID AND coborr3AK3.CustFieldDef_ID = ( SELECT ID FROM mwsCustFieldDef WHERE MWFieldNum = @intBorrower3AKA3 )
                LEFT JOIN mwlCustomField AS coborr3AK4 ON loanapp.ID = coborr3AK4.LoanAPP_ID AND coborr3AK4.CustFieldDef_ID = ( SELECT ID FROM mwsCustFieldDef WHERE MWFieldNum = @intBorrower3AKA4 )
        WHERE   loanapp.LoanNumber = @LoanNumber      
    END 



GO

