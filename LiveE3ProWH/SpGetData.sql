USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Stonegate>  
-- Create date: <10/5/2012>  
-- Description: <Fetch AE Region Individual Report Closing>  
-- =============================================  
create PROCEDURE	[dbo].[SpGetData] 
(  
 @LoanId varchar(100),
@Crecid varchar(100)	  
)  
AS  
BEGIN  
  
Declare @SQL nvarchar(max)  
Set @SQL =''  
Set @SQL = 'select ctid,condrecid, loan_no,cond_text,filename,convert(varchar(35),dttime,101) as dttime,ProlendImageDesc from tblCondText_History 
 where rtrim(loan_no)=''' + @LoanId + '''  and condrecid = ''' + @Crecid + ''' and isnull(iscommited,0) = 0'


Print @SQL  
exec sp_executesql @SQL  
END  
GO

