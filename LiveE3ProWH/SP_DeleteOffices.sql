USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DeleteOffices]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_DeleteOffices]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- =============================================    
-- Author:      <Chandrika>    
-- Create date: <4-Sept-2012>    
-- Description: <Delete Offices>    
-- =============================================  
-- =============================================  
CREATE PROCEDURE [dbo].[SP_DeleteOffices]  
(    
@userid int 
)  
AS  
BEGIN  

delete from tblOffices where userid=@userid
  
END  
  
GO

