USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchmwlCondition]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchmwlCondition]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:		Nitin

-- Create date: 10/06/2012

-- Description:	Procedure for clspipelineE3 BLL

-- Modified by suvarna on 03-01-2014 - Added DueBy column to fix JIRA 176 issue

-- =============================================



CREATE PROCEDURE [dbo].[sp_FetchmwlCondition]
(
@LoanNumber varchar(15)
)
AS
BEGIN

SET NOCOUNT ON;

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   --This option has the same effect as setting NOLOCK on all tables in all SELECT statements in a transaction (Recommended by Harland)
select Description as reason,DueBy from mwlCondition 

where objOwner_ID  in(select ID from  mwlLoanApp 

where LoanNumber = @LoanNumber)   

END


GO

