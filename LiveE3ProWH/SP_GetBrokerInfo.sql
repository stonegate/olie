USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetBrokerInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetBrokerInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create Procedure Coding Starts Here

Create PROCEDURE [dbo].[SP_GetBrokerInfo]
	(
@loan_no char(15)
)
AS
BEGIN
select lt_broker from loandat where loan_no =@loan_no
             
END

GO

