USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_mwlLoanApp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_mwlLoanApp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Nitin  
-- Create date: 10/06/2012  
-- Description: Procedure for clspipelineE3 BLL  
-- =============================================  
CREATE procEDURE [dbo].[sp_Update_mwlLoanApp]  
(  
@ChannelType varchar(100),  
@Originator_id varchar(400),  
@originatorName varchar(115),  
@id varchar(32)  
)  
AS  
BEGIN  
  
 update mwlLoanApp set newrespa='True', ChannelType=@ChannelType,  
Originator_id=@Originator_id,originatorName=@originatorName  
WHERE id= @id 
     
END  
  
GO

