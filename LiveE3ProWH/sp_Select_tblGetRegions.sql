USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetRegions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetRegions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--sp_Select_tblGetRegions
-- =============================================  
-- Author:  Gourav  
-- Create date: 10/03/2012  
-- Description: Procedure for clsUser.cs bll    
-- EXEC sp_Select_tblGetRegions @BusinessSource = 'TPO,CFI'
-- =============================================  
CREATE PROCEDURE [dbo].[sp_Select_tblGetRegions]
@BusinessSource nvarchar(50) = null
as  
begin  
select *,'TPO/CFI' as Type from tblregion where IsActive=1
AND (@BusinessSource IS NULL OR BusinessSource IN (SELECT * FROM dbo.SplitString(@BusinessSource, ',')))
end  


GO

