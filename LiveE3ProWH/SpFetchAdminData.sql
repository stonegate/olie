USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpFetchAdminData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpFetchAdminData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nirav>
-- Create date: <10/4/2012>
-- Description:	<Fetch Admin Role>
-- =============================================
CREATE PROCEDURE [dbo].[SpFetchAdminData]
	@UserRole varchar(500),
	@PartnerCompanyId varchar(500)
AS
BEGIN
	SELECT isnull(isadmin,0) as isadmin from tblusers where  urole=' + oRoleID + ' and  (PartnerCompanyid='" + oParterCompanyid.ToString() + "' ) and (IsUserinNewProcess = '1' or IsUserinNewProcess is null) and isnull(isadmin,0)=1
END

GO

