USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SelecttblCondText_History_dttime]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SelecttblCondText_History_dttime]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 10/07/2012
-- Description:	Procedure for clspipelineE3 BLL
-- =============================================
CREATE procEDURE [dbo].[sp_SelecttblCondText_History_dttime]
(
@Loan_No char(15),
@condrecid varchar(50)
)
AS
BEGIN
	
	SET NOCOUNT ON;
SELECT dttime as submittedDate from tblCondText_History
 where Loan_No= @Loan_No 
and condrecid=@condrecid 
END


GO

