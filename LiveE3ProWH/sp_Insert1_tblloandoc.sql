USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert1_tblloandoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert1_tblloandoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_Insert1_tblloandoc]
(@uloanregid int,
@Docname varchar(500),
@Comments varchar(500),
@userid int,
@loan_no char(15),
@createddate datetime
)
as
begin
	Insert into tblloandoc(uloanregid,Docname,Comments,userid,loan_no,createddate)
values(@uloanregid ,
@Docname ,
@Comments ,
@userid ,
@loan_no,
@createddate
)
 
end


GO

