USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblIsUnderwriter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblIsUnderwriter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE proc [dbo].[sp_Select_tblIsUnderwriter]
(
@loginidprolender varchar(Max)
)
as
begin
SELECT tblUsers.userid FROM tblUsers LEFT OUTER JOIN Carrier 
ON tblUsers.carrierid = Carrier.ID LEFT OUTER JOIN tblRoles 
ON tblUsers.urole = tblRoles.ID where loginidprolender = @loginidprolender
end


GO

