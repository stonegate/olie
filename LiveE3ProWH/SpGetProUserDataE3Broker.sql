USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpGetProUserDataE3Broker]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpGetProUserDataE3Broker]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpGetProUserDataE3Broker]
	@RecId varchar(500)
AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

	select Company,CompanyEmail, firstname, lastname,InstitutionType from mwcinstitution mwc Left join mwcContact mwcontact on mwc.Id=mwcontact.Institution_id where Status='Active' and  mwc.ID =@RecId
END

GO

