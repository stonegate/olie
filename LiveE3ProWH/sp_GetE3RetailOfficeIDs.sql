USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetE3RetailOfficeIDs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetE3RetailOfficeIDs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Stonegate>
-- Create date: <10/9/2012>
-- Description:	<This will Get E3Retail Office IDs>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetE3RetailOfficeIDs]

AS
BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

Select 
	Contact.Firstname,Contact.Lastname,Contact.Lastname + ' ,' + Contact.Firstname as FullName,
	office ,usert.Id as E3Userid
From 
	mwccontact Contact
	inner join mwcinstitution Ins on Contact.Institution_id=Ins.id inner join mwamwuser usert
	on usert.Firstname=Contact.Firstname and usert.Lastname=Contact.Lastname
Where 
	office like '%Retail%'
END

GO

