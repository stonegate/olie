USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpUpdateUserInProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpUpdateUserInProcess]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nirav>
-- Create date: <10/4/2012>
-- Description:	<Fetch Admin Role>
-- =============================================
CREATE PROCEDURE [dbo].[SpUpdateUserInProcess]
	@UserRole int,
	@PartnerCompanyId varchar(500)
AS
BEGIN
	update tblusers Set IsUserinNewProcess = 0 Where urole= @UserRole and  (PartnerCompanyid=@PartnerCompanyId ) and (IsUserinNewProcess = '1' or IsUserinNewProcess is null)
END

GO

