USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPipelineReportForSearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPipelineReportForSearch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_GetPipelineReportForSearch]
(
	@RoleID int,
	@CorrespondentType nvarchar(2000),
	@CustomField nvarchar(2000),
	@StrIDs nvarchar(max),
	@ChannelType nvarchar(max) = null,
	@SearchText varchar(100)
)	
AS
BEGIN
--GetAllMyPipelineReport
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 

Declare @SQL varchar(max)
declare @SQL1 varchar(max)
declare @SQL2 varchar(max)

DECLARE @InternalUsers VARCHAR(500)SELECT @InternalUsers = Roles
FROM tblQuickLinks
WHERE ID = 18 --Lock Confirmation

set @SQL1 =''
set @SQL=''
SET @RoleID  =  (CASE WHEN @RoleID = '38' THEN '0' ELSE @RoleID END)
Set @SQL = 'select top 100  ''E3'' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.Channeltype as BusType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,
loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,loanapp.LoanNumber as loan_no,CloserName as Closer,
borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate,
approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,
COALESCE(loanData.LoanProgDesc ,'''') as LoanProgram ,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,
CustFNMA.YNValue,LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,uEmail.Email as Underwriter_Email,
CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,
CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate, 
CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'' and a.objOwner_ID IN 
(SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0
WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'')
and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0 ELSE 
(SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where(DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=''Waived'') and a.objOwner_ID 
IN(SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions 
FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=''NOTICE'') and a.objOwner_ID IN (SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER,
case cust.stringvalue WHEN ''--Select One--'' then '''' else cust.stringvalue end as UnderwriterType '

If (@RoleID = 2 Or @RoleID = 27)
Begin
	set @SQL = @SQL + ',isnull(Cust.StringValue,'''') as  CorrespondentType'
End
SET @SQL = @SQL + ',LockRecord.DeliveryOption,BOffices.Office TPORegion,loanapp.Originator_id AEId '

IF (@RoleID = 0)
	BEGIN
		SET @SQL = @SQL + ',CASE WHEN (COALESCE(LockRecord.Status,'''') =''CANCELED'' OR COALESCE(LockRecord.Status,'''') =''CONFIRMED'' OR COALESCE(LockRecord.Status,'''') =''EXPIRED'' )  THEN 1 ELSE 0 END As  viewdoc'
	END
	ELSE IF (
			@RoleID = 1
			OR @RoleID = 2
			OR @RoleID = 8
			OR @RoleID = 12
			OR @RoleID = 32
			OR @RoleID = 34
			) --Internal users
	BEGIN
		IF (
				@RoleID IN (
					SELECT *
					FROM dbo.splitstring(@InternalUsers, ',')
					)
				)
		BEGIN
			SET @SQL = @SQL + ',CASE WHEN (COALESCE(LockRecord.Status,'''') =''CANCELED'' OR COALESCE(LockRecord.Status,'''') =''CONFIRMED'' OR COALESCE(LockRecord.Status,'''') =''EXPIRED'' )  THEN 1 ELSE 0 END As  viewdoc'
		END
		ELSE
		BEGIN
			SET @SQL = @SQL + ', 0 As  viewdoc'
		END
	END
	ELSE IF (
			@RoleID = 3
			OR @RoleID = 20
			OR @RoleID = 21
			) --External users 
	BEGIN
		SET @SQL = @SQL + ',CASE WHEN (COALESCE(LockRecord.Status,'''') =''CANCELED'' OR COALESCE(LockRecord.Status,'''') =''CONFIRMED'' OR COALESCE(LockRecord.Status,'''') =''EXPIRED'' )  THEN 1 ELSE 0 END As  viewdoc'
	END
	ELSE
		SET @SQL = @SQL + ', 0 As  viewdoc'

set @SQL = @SQL + ' from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id
and (Broker.InstitutionType = loanapp.channeltype or Broker.objownerName=loanapp.channeltype) left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' 
Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc=''01 - Registered''
Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved'' Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and (Corres.InstitutionType = loanapp.channeltype or Corres.objownerName=loanapp.channeltype) 
Left join (Select distinct Office,ObjOwner_id from dbo.mwlInstitution Where InstitutionType = ''Branch'' and objownerName=''BranchInstitution'') as BOffices on BOffices.ObjOwner_id = loanapp.id  
left join dbo.mwamwuser as uEmail on uSummary.UnderWriter_id = uEmail.id
left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID = '''+ @CorrespondentType+'''
left join mwlcustomfield as CustFNMA on CustFNMA.loanapp_id = loanapp.id and CustFNMA.CustFieldDef_ID = '''+ @CustomField+''''

set @SQL = @SQL + ' where borr.sequencenum=1 and loanapp.LoanNumber is not null and loanapp.LoanNumber <> '''' and '

if (@RoleID = 16)
Begin
	set @SQL = @SQL + ' CurrentStatus IN (''01 - Registered'',''05 - Application Taken'', ''03 - Appt Set to Review Disclosures'', ''04 - Disclosures Sent'',''07 - Disclosures Received'',''09 - Application Received'',''13 - File Intake'',''17 - Submission On Hold'',''19 - Disclosures'',''20 - Disclosures On Hold'',''21 - In Processing'',''25 - Submitted to Underwriting'',''29 - In Underwriting'',''29.1 - In UW Processing''),''33 - Cleared to Close'',''37 - Pre-Closing'',''41 - In Closing'',''43 - Closing on Hold'',''45 - Docs Out'')'
End
else
Begin
	set @SQL = @SQL + ' CurrentStatus IN (''01 - Registered'',''05 - Application Taken'',''03 - Appt Set to Review Disclosures'',''04 - Disclosures Sent'', ''07 - Disclosures Received'',''08 - In GFE Review'', ''09 - Application Received'',''13 - File Intake'',''17 - Submission On Hold'',''19 - Disclosures'',''20 - Disclosures On Hold'',''21 - In Processing'',''25 - Submitted to Underwriting'',''29 - In Underwriting'',''29.1 - In UW Processing'',''15 - Appraisal Ordered'',''22 - Appraisal Received'',''10 - Application on Hold'')'
End
if (@RoleID <> 0)
Begin
	if (len(@StrIDs) = 0)
    Begin
		if   ((@RoleID = 1 or @RoleID = 2 or @RoleID = 8  or @RoleID = 32) AND (@ChannelType <> '' AND @ChannelType IS NOT NULL))
		begin
			SET @SQL = @SQL + ' and loanapp.Channeltype IN (''' + @ChannelType + ''')'
		end
		if (@RoleID = 13)
        Begin
       
           set @SQL1 = @SQL1 + ' and loandat.LT_Broker_Rep in (select * from dbo.SplitString('''+@StrIDs+''','',''))'
		End		
		 else if (@RoleID = 3 Or @RoleID = 20 Or @RoleID = 21)
         BEGIN
			IF(@RoleID = 20)
			BEGIN
				set @SQL1 = @SQL1 + ' and loanapp.Channeltype=''CORRESPOND'''
			END
			set @SQL1 = @SQL1 + ' and (Corres.CompanyEmail IN('''+ @StrIDs +''') OR Broker.CompanyEmail IN('''+ @StrIDs +'''))'
         END
        else
        Begin
			set @SQL1 = @SQL1 + ' and (Originator_id in (select * from dbo.SplitString('''+@StrIDs+''','','')))'
        End
     End
     else
     Begin                         
		 if (@RoleID = 13)
         Begin
			set @SQL1 = @SQL1 + ' and loandat.LT_Broker_Rep in (select * from dbo.SplitString('''+@StrIDs+''','',''))'
         End		
		 else if (@RoleID = 3 Or @RoleID = 20 Or @RoleID = 21)
         BEGIN
			IF(@RoleID = 20)
			BEGIN
				set @SQL1 = @SQL1 + ' and loanapp.Channeltype=''CORRESPOND'''
			END
			set @SQL1 = @SQL1 + ' and (Corres.CompanyEmail IN('''+ @StrIDs +''') OR Broker.CompanyEmail IN('''+ @StrIDs +'''))'
         END
         ELSE IF ((@RoleID = 1 OR @RoleID = 2 OR @RoleID = 8 OR @RoleID = 32) AND (@ChannelType <> '' AND @ChannelType IS NOT NULL))
		 BEGIN
			SET @SQL = @SQL + ' and loanapp.Channeltype IN (select * from dbo.SplitString('''+@ChannelType+''','',''))  and (Originator_id in (select * from dbo.SplitString('''+@StrIDs+''','','')))'
		 END
         else
         Begin
			set @SQL1 = @SQL1 + ' and (Originator_id in (select * from dbo.SplitString('''+@StrIDs+''','','')))'
         End
	 End
End
set @SQL1 = @SQL1 + @SQL1 + ' and (loanapp.loannumber like ''%' + @SearchText + '%'' OR borr.lastname like ''%' + @SearchText + '%'' 
 OR Broker.Company like ''%' + @SearchText + '%'' OR Corres.Company like ''%' + @SearchText + '%'' OR loanapp.currentStatus like ''%' + @SearchText + '%''
 OR TransType like ''%' + @SearchText + '%'' OR LockRecord.Status like ''%' + @SearchText + '%'' 
 OR UnderwriterName like ''%' + @SearchText + '%'' OR loanData.LoanProgDesc like ''%' + @SearchText + '%'' OR loanapp.DecisionStatus like ''%' + @SearchText + '%'') order by Per desc, LockRecord.DateTimeAdded desc'  
Print (@SQL +  @SQL1)
--exec sp_executesql (@SQL +  @SQL1)
exec (@SQL +  @SQL1)
END



GO

