USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTabs]') AND type in (N'U'))
DROP TABLE [dbo].[tblTabs]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTabs](	  [TabID] INT NOT NULL IDENTITY(1,1)	, [TabName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ControlName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [TabLink] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblTabs] PRIMARY KEY ([TabID] ASC))USE [HomeLendingExperts]
GO

