USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckforSubmitLoanRetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckforSubmitLoanRetail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Vipul Thacker>  
-- Create date: <10/4/2012>  
-- Description: <This wil Check for Submit Loans for Submit Loan Document or Submit File to processing>
--9828 MWFieldNUm is used to filter Retail Case Opener loan records only
-- =============================================  
CREATE PROCEDURE [dbo].[sp_CheckforSubmitLoanRetail]        
(        
@RoleID int,       
@strID varchar(max),        
@strChLoanno varchar(100),        
@CurrentStatus varchar(3000),      
@oPuid varchar(3000),      
@strStatusDesc varchar(50),
@OfficeName varchar(50)    
)        
AS        
BEGIN        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
SET @strChLoanno = '%' + @strChLoanno + '%'
--ROLE VARIABLE DECLARATIONS-----------------------------------------------------------------------
DECLARE @Admin INT      
SET @Admin=0 

DECLARE @SalesManager INT      
SET @SalesManager=3

DECLARE @BranchManager INT      
SET @BranchManager=10

Declare @CaseOpener INT      
SET @CaseOpener=12

Declare @RetailOperMgr INT      
SET @RetailOperMgr=11

Declare @Closer INT  
SET @Closer=15
        
Declare @RetailTeamLd INT  
SET @RetailTeamLd=16

Declare @AreaManager INT  
SET @AreaManager=19
---------------------------------------------------------------------------------------------------
IF(@RoleID != @Admin and @RoleID != @Closer and @RoleID != @RetailOperMgr and  @RoleID != @RetailTeamLd and  @RoleID != @AreaManager)
	BEGIN
	if(@RoleID = @SalesManager OR @RoleID = @BranchManager)
	BEGIN
		SELECT 'E3' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus
			,loanapp.ID as record_id,LoanNumber as loan_no,borr.firstname as BorrowerfirstName
			,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,appStat.StatusDateTime AS SubmittedDate
			,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
			,loanData.AdjustedNoteAmt as loan_amt
		FROM mwlLoanApp loanapp WITH ( NOLOCK ) 		
		Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
		Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active = 1			
		Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id 
			and Branch.InstitutionType = 'Branch' and Branch.objownerName='BranchInstitution'
		Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.sequencenum=1 AND appStat.StatusDesc=@strStatusDesc
		WHERE borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''
		AND loanapp.ChannelType like '%RETAIL%' and CurrentStatus IN (select items from dbo.SplitString(@CurrentStatus,','))
		--AND (Branch.Office in (@OfficeName)) commnented out to resolve SMCPS451
		AND (LoanNumber like @strChLoanno OR borr.lastname like @strChLoanno)
	END
	ELSE IF(@RoleID = @CaseOpener)
	BEGIN	
		SELECT 'E3' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus
			,loanapp.ID as record_id,LoanNumber as loan_no,borr.firstname as BorrowerfirstName
			,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,appStat.StatusDateTime AS SubmittedDate
			,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
			,loanData.AdjustedNoteAmt as loan_amt
		from mwlLoanApp loanapp WITH ( NOLOCK )
		INNER JOIN mwlCustomField ON mwlCustomField.LoanApp_ID = loanapp.ID And mwlCustomField.CustFieldDef_ID = (Select ID from mwsCustFieldDef WHERE MWFieldNum = '9828')
		Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id 
		Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active = 1
		Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id 
			and Branch.InstitutionType = 'Branch' and Branch.objownerName='BranchInstitution'
		Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.sequencenum=1 and appStat.StatusDesc=@strStatusDesc
		where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''
		AND loanapp.ChannelType like '%RETAIL%' and CurrentStatus IN (select items from dbo.SplitString(@CurrentStatus,','))
		AND (Originator_id in (select items from dbo.SplitString(@strID,',')) OR Originator_id in (select items from dbo.SplitString(@oPuid,',')) OR loanapp.id in (select items from dbo.SplitString(@strID,',')))
		AND (LoanNumber LIKE @strChLoanno OR borr.lastname LIKE @strChLoanno)
	END
	ELSE
	BEGIN
		SELECT 'E3' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus
			,loanapp.ID as record_id,LoanNumber as loan_no,borr.firstname as BorrowerfirstName
			,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,appStat.StatusDateTime AS SubmittedDate
			,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
			,loanData.AdjustedNoteAmt as loan_amt
		from mwlLoanApp loanapp WITH ( NOLOCK )
		Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id 
		Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active = 1
		Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id 
			and Branch.InstitutionType = 'Branch' and Branch.objownerName='BranchInstitution'
		Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.sequencenum=1 and appStat.StatusDesc=@strStatusDesc
		where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''
		AND loanapp.ChannelType like '%RETAIL%' and CurrentStatus IN (select items from dbo.SplitString(@CurrentStatus,','))
		AND (Originator_id in (select items from dbo.SplitString(@strID,',')) OR Originator_id in (select items from dbo.SplitString(@oPuid,',')) OR loanapp.id in (select items from dbo.SplitString(@strID,',')))
		AND (LoanNumber LIKE @strChLoanno OR borr.lastname LIKE @strChLoanno)
	END
END
Else
BEGIN
select 'E3' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus
		,loanapp.ID as record_id,LoanNumber as loan_no,borr.firstname as BorrowerfirstName
		,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,appStat.StatusDateTime AS SubmittedDate
		,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
		,loanData.AdjustedNoteAmt as loan_amt
	from mwlLoanApp loanapp WITH ( NOLOCK ) 
	Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
	Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active = 1	
	Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id 
		and Branch.InstitutionType = 'Branch' and Branch.objownerName='BranchInstitution'
	Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.sequencenum=1 and appStat.StatusDesc=@strStatusDesc
	where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''
	and loanapp.ChannelType like '%RETAIL%' and CurrentStatus IN (select items from dbo.SplitString(@CurrentStatus,','))
	AND (LoanNumber LIKE @strChLoanno OR borr.lastname LIKE @strChLoanno)
END     
END


GO

