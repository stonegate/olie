USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetMAEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetMAEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsUserRegistrationE3.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetMAEmail]
(
@ufirstname varchar(Max),
@ulastname varchar(Max)
)
AS
BEGIN
	 select uemailid,userid from tblusers where ufirstname =@ufirstname and ulastname =@ulastname and urole in (2,3,7,10) and isactive=1
END

GO

