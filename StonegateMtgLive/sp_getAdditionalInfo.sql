USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getAdditionalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getAdditionalInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_getAdditionalInfo]
(
@loanid VARCHAR(MAX),
@TransactionDate VARCHAR(MAX)
)
AS
BEGIN
select TransactionCode,LoanID,TransactionAmt,TransactionDate from history
where transactioncode = 220
and month(TransactionDate) = month(@TransactionDate)
and year(TransactionDate) = year(@TransactionDate)
and loanid = @loanid  order by TransactionDate desc ;

 select top 1 TransactionCode,LoanID,TransactionAmt,TransactionDate from history 
 where transactioncode = 300 
 and month(TransactionDate) = month(@TransactionDate) 
 and year(TransactionDate) = year(@TransactionDate)
 and loanid = loanid order by TransactionDate desc ;

select top 1 TransactionCode,LoanID,TransactionAmt,TransactionDate from history 
where transactioncode = 240
and loanid = @loanid order by TransactionDate desc ;

select top 1 TransactionCode,LoanID,TransactionAmt,TransactionDate from history
where transactioncode = 260 
           
and loanid = @loanid order by TransactionDate desc ;


            
select top 1 TransactionCode,LoanID,TransactionAmt,TransactionDate from history 
where transactioncode = 310
            
and loanid = @loanid order by TransactionDate desc ;

select top 1 TransactionCode,LoanID,TransactionAmt,TransactionDate from history
where transactioncode = 320
           
and loanid = @loanid order by TransactionDate desc 

END


GO

