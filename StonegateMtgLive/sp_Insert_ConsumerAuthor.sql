USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_ConsumerAuthor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_ConsumerAuthor]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_Insert_ConsumerAuthor] 
(
@stringmode varchar(50),
@AppID int,
@IUnderstand bit,
@IfApplicable bit,
@IAcknowledge bit,
@IAuthorise bit,
@ApplicantOnly bit,
@IHereby bit,
@OtherNameUsed varchar(20),  
@SSNNo varchar(20),
@StreetAddress varchar(500),
@DobDay varchar(5),
@DobMonth varchar(12),
@DobYear varchar(50),
@ConCity varchar(50),
@ConState varchar(50),
@ConZip varchar(15),
@LsnNoState varchar(100),
@LsnNoName varchar(50),
@Acceptterm bit,
@ConAppName varchar(50), 
@ConAppDate datetime 
)
AS
BEGIN
	 INSERT INTO tblConsumerAuthor(AppID,IUnderstand,IfApplicable,IAcknowledge,IAuthorise,ApplicantOnly,IHereby,OtherNameUsed,SSNNo,StreetAddress,DobDay,DobMonth,DobYear,ConCity,ConState,ConZip,LsnNoState,LsnNoName,Acceptterm,ConAppName,ConAppDate)
     VALUES (@AppID,@IUnderstand,@IfApplicable,@IAcknowledge,@IAuthorise,@ApplicantOnly,@IHereby,@OtherNameUsed,@SSNNo,@StreetAddress,@DobDay,@DobMonth,@DobYear,@ConCity,@ConState,@ConZip,@LsnNoState,@LsnNoName,@Acceptterm,@ConAppName,@ConAppDate)  
END








GO

