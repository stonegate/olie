USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDataTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDataTable]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDataTable]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

SELECT     tblloanapp.LoanId, tblloanapp.Fname, tblloanapp.Mname, tblloanapp.LName, tblloanapp.Email, tblloanapp.CEmail, tblloanapp.DOBM,  tblloanapp.DOBDT, tblloanapp.DOBYR, tblloanapp.SSN, tblloanapp.Phone, tblloanapp.Suffix, tblloanapp.COFNAME, tblloanapp.COMNAME,tblloanapp.COLNAME, tblloanapp.COEMAIL, tblloanapp.COCEmail, tblloanapp.CODOBM, tblloanapp.CODOBDT, tblloanapp.CODOBYR,    tblloanapp.COSSN, tblloanapp.COPHONE, tblloanapp.STADDR, tblloanapp.CITY, tblloanapp.MState, tblloanapp.ZipCode, tblloanapp.RSTATUS,  tblloanapp.RentAmt, tblloanapp.LMHOLDER, tblloanapp.STADDR2, tblloanapp.CITY2, tblloanapp.STATE2, tblloanapp.COSuffix, tblloanapp.ZipCode2,  tblloanapp.RLengthY2, tblloanapp.RLengthM2, tblloanapp.RSTATUS2, tblloanapp.RentAmt2, tblloanapp.LMHOLDER2, tblloanapp.AEMPL,  tblloanapp.AOCCU, tblloanapp.AEMPPHONE, tblloanapp.AGMI, tblloanapp.ALEMY, tblloanapp.ALEMM, tblloanapp.AEMPL2, tblloanapp.AOCCU2,tblloanapp.AEMPPHONE2, tblloanapp.AGMI2, tblloanapp.ALEMY2, tblloanapp.ALEMM2, tblloanapp.CAEMPL, tblloanapp.CAOCCU,tblloanapp.CAEMPPHONE, tblloanapp.CAGMI, tblloanapp.CALEMY, tblloanapp.CALEMM, tblloanapp.CAEMPL2, tblloanapp.CAOCCU2,tblloanapp.CAEMPPHONE2, tblloanapp.CAGMI2, tblloanapp.CALEMY2, tblloanapp.CALEMM2, tblloanapp.CurrLoanMake, tblloanapp.CurrLoanYear,tblloanapp.CurrLoanBalance, tblloanapp.CurrLoanMiles, tblloanapp.OtherComments, tblloanapp.DigiSig, tblloanapp.SigDate, tblloanapp.IsAckno,tblloanapp.DateEntered, tblloanapp.RLengthM, tblloanapp.RlengthY, tblloanapp.Status, tblloanapp.Network, tblloanapp.PayDate, tblloanapp.Amount, tblloanapp.Paid, tblloanapp.lastupdateby, tblloanapp.NetworkName, tblloanapp.NetworkPhone, tblstate.StateName AS StateName1 ,tblstate_1.StateName as StateName2  FROM tblloanapp INNER JOIN tblstate ON tblloanapp.MState = tblstate.SId INNER JOIN tblstate AS tblstate_1 ON tblloanapp.STATE2 = tblstate_1.SId  ORDER BY tblloanapp.LoanId DESC
END


GO

