USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetEventList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetEventList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetEventList]

AS
BEGIN
	SELECT EventName,UniqueUrl,EventDate FROM dbo.tblRSVPEdits WHERE Status =1 order by EventDate asc
	
END



GO

