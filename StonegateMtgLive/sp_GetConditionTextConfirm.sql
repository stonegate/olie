USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetConditionTextConfirm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetConditionTextConfirm]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetConditionTextConfirm]
(
@record_id VARCHAR(MAX)
)
AS
BEGIN
SELECT parent_id,cond_text FROM dbo.condits 
WHERE record_id in (@record_id)

END

GO

