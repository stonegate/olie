USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetRetailFinalPriceReport_With_Region]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetRetailFinalPriceReport_With_Region]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SP_GetRetailFinalPriceReport_With_Region]           
(        
@strID varchar(max),    
@FromDate  varchar(20),  
@ToDate varchar(20),  
@oPuid varchar(2000)

)            
AS            
BEGIN            
            
Declare @SQL nvarchar(max)            
Set @SQL =''            
Set @SQL ='select ''E3'' as DB,loanapp.currpipestatusdate, loanapp.ID as record_id,loanapp.Channeltype as BusType,
		   LoanNumber as loan_no,borr.firstname as BorrowerFirstName , borr.lastname as BorrowerLastName,
		   CurrentStatus as LoanStatus,convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,
		   /*DateAppSigned as DateApproved,LoanData.LoanProgramName as Prog_desc ,CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) 
		   as	CreditScoreUsed,(Loandata.NoteRate * 100) as Int_rate,UnderwriterName as UnderWriter,CloserName as Closer,CASE ISNULL
			(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,'''' as FundedDate,''''			as ReceivedInUW,LoanData.ShortProgramName as prog_code ,loanapp.Branch Branch1,  
		   */
		   
		   loanapp.Originatorname,  
		   case when loanapp.LoanPurpose1003 is null then '''' when loanapp.LoanPurpose1003 = ''PU'' then ''Purchase'' 
		   when loanapp.LoanPurpose1003 = ''RE'' then ''Refinance'' end as Purpose,loanData.AdjustedNoteAmt as loan_amt,
		   LockRecord.Status as LockStatus,  
		   convert(varchar(40),LockRecord.LockExpirationDate,101) as LockExpires,LockRecord.LockDateTime,  
		   convert(varchar(35),LockRecord.LockDateTime,101) as Lockdate,
		   Broker.Office as Branch,
		   case loanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' WHEN ''M'' 
		   THEN  ''USDA/Rural Housing Service'' End as LoanProgram ,loanData.AdjustedNoteAmt as LoanAmount,
		   LockRecord.LockReason,case Isnull(PricingResult.lenderbaserate,'''') When '''' then '''' Else 
		   convert(varchar,((PricingResult.lenderbaserate)*100)) + '' %'' end interestRate,
		   PricingResult.lenderClosingprice  FinalLockPrice,isnull(tblusers.Region,'''') as Area 
		   from mwlLoanApp  loanapp     
		   Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id    
		   Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id                    
		   
		   Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BRANCH'' 
		   and Broker.objownerName=''BranchInstitution''  
		   left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id  and  LockRecord.Status<>''CANCELED'' 
		   and isnull(LockRecord.LockReason,'''')<>''''  
		   /*left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id    
		   Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.				objownerName=''Correspondent''  */
		   Left join mwlPricingResult  as PricingResult on  PricingResult.ObjOwner_id = LockRecord.id  
		   left join tblusers on tblusers.E3Userid= loanapp.Originator_ID
		   where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''   
		   and loanapp.ChannelType like ''%RETAIL%'''  
  
    Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+  @oPuid  +''','','')) 
						or loanapp.id in (select * from dbo.SplitString('''+  @strID  +''','','')) 
						or Broker.office in  (select * from dbo.SplitString('''+  @oPuid  +''','','')) )'

    if(@FromDate != '' and @ToDate !='')
			Set @SQL=@SQL+ ' and loanapp.currpipestatusdate between '''+@FromDate+'''  
					     	 and  '''+Convert(varchar(20),DATEADD (dd , 1 , @ToDate),101)+''''
	else if(@FromDate != '')
			Set @SQL=@SQL+ ' and loanapp.currpipestatusdate > '''+@FromDate+''''	
	else if(@ToDate != '')
			Set @SQL=@SQL+ ' and loanapp.currpipestatusdate < '''+@ToDate+''''
       
              
     Set @SQL = @SQL + ' and loanapp.CurrentStatus =''62 - Funded'''

Print @SQL            
exec sp_executesql @SQL            
END
GO

