USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_LoanRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_LoanRegister]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_Insert_LoanRegister] 
(
@borrLast varchar(100),
@transType int,      
@loanAmt numeric(14,2),   
@lockStatus varchar(20),    
@loanPgm varchar(20),    
@userid int,
@filetype varchar(50),
@filename varchar(100) 
)
AS
BEGIN
	 INSERT INTO tblLoanReg (borrLast,transType,loanAmt,lockStatus,loanPgm,userid,filetype,filename)      
     VALUES  (@borrLast,@transType,@loanAmt,@lockStatus,@loanPgm,@userid,@filetype,@filename)       
  
END








GO

