USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_GetRetailOpMgrReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_GetRetailOpMgrReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: <Author,,Name>
-- Create date: <Create Date,,>
-- Description: <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_GetRetailOpMgrReport]
@iUserId INT,
@sRolename nVARCHAR(500),
@whereClause VARCHAR(1000),
@sortField VARCHAR(200),
@SortOrder VARCHAR(10),
@PageNo INT,
@Pagesize INT,
@Export INT

AS
BEGIN
Declare @sql NVARCHAR(Max)
DECLARE @sql1 NVARCHAR(Max)
DECLARE @sql2 NVARCHAR(Max)
DECLARE @sday INT

If @sortField = 'BorrowerLastName'
Begin
    Set @sortField = 'borr.lastname'
End
If @sortField = 'BorrowerFirstName'
Begin
    Set @sortField = 'borr.firstname'
End
If @sortField = 'loan_amt'
Begin
    Set @sortField = 'loanData.AdjustedNoteAmt'
End
If @sortField = 'LoanStatus'
Begin
    Set @sortField = 'loanapp.currentStatus'
End 
If @sortField = 'LoanProgram'
Begin
    Set @sortField = 'LoanData.FinancingType'
End    
If @sortField = 'LockExpirationDate'
Begin
    Set @sortField = 'LockRecord.LockExpirationDate'
End
If @sortField = 'SubmittedDate'
Begin
    Set @sortField = 'appStat.StatusDateTime'
END


IF @sRolename = '09 - Application Received'
BEGIN
SET @sday = 15
END
IF @sRolename = '10 - Application on Hold'
BEGIN
SET @sday = 10
END
IF @sRolename = '13 - File Intake'
BEGIN
SET @sday = 5
END
IF @sRolename = '14 - Prop Inspection Waived'
BEGIN
SET @sday = 45
END
IF @sRolename = '15 - Appraisal Ordered'
BEGIN
SET @sday = 45
END
IF @sRolename = '17 - Submission On Hold'
BEGIN
SET @sday = 15
END
IF @sRolename = '18 - New Construction On Hold'
BEGIN
SET @sday = 30
END
IF @sRolename = '21 - In Processing'
BEGIN
SET @sday = 60
END
IF @sRolename = '22 - Appraisal Received'
BEGIN
SET @sday = 30
END

IF @Export = 1
--export
BEGIN


SET @sql2 = ''
SET @sql2 = ''
SET @sql2 = @sql2
+ 'select ''E3'' as DB,loanData.AdjustedNoteAmt AS Totalamt,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue as ProcessorName,
loanapp.currentStatus as LoanStatus, loanapp.CurrPipeStatusDate, CurrDecStatusDate as CurrDecisionstatus,loanapp.DecisionStatus,
loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.firstname as BorrowerFirstName,
borr.lastname as BorrowerLastName,'''' as LU_FINAL_HUD_STATUS, approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe, case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN ''VA'' End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM
(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,
CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate,
'''' AS PER from mwlLoanApp loanapp
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK''
and LockRecord.Status<>''CANCELED'' 
Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved''
Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''Branch'' and Corres.objownerName=''BranchInstitution''
left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID and CustFieldDef_id in(select Id from mwsCustFieldDef
where CustomFieldName=''Retail Processor'') Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id
and Branch.InstitutionType = ''Branch'' and Branch.objownerName=''BranchInstitution'' where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' and
loanapp.ChannelType like ''%RETAIL%'''

IF @sRolename <> ''
BEGIN
SET @sql2 = @sql2 + ' and loanapp.CurrentStatus=''' + @sRolename + ''''
END
SET @sql2 = @sql2
+ ' and (datediff(day,loanapp.CurrPipeStatusDate,getdate()) >= '
+ CONVERT(VARCHAR, @sday) + ')'
IF @whereClause <> ''
BEGIN
SET @sql2 = @sql2 + ' AND ' + @whereClause
END

SET @sql2 = @sql2 + ' order by + ' + @sortField + ' ' + @SortOrder
print @sql2
EXEC sp_executeSql @sql2

END
ELSE
BEGIN
PRINT @sday
SET @sql = ''
SET @sql = 'Select count(*) from ( '
SET @sql = @sql
+ 'select ''E3'' as DB,loanData.AdjustedNoteAmt AS Totalamt,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue as ProcessorName,
loanapp.currentStatus as LoanStatus, loanapp.CurrPipeStatusDate, CurrDecStatusDate as CurrDecisionstatus,loanapp.DecisionStatus,
loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.firstname as BorrowerFirstName,
borr.lastname as BorrowerLastName,'''' as LU_FINAL_HUD_STATUS, approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe, case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN ''VA'' End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM
(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,
CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate,
'''' AS PER from mwlLoanApp loanapp
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK''
and LockRecord.Status<>''CANCELED'' 
Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved''
Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''Branch'' and Corres.objownerName=''BranchInstitution''
left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID and CustFieldDef_id in(select Id from mwsCustFieldDef
where CustomFieldName=''Retail Processor'') Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id
and Branch.InstitutionType = ''Branch'' and Branch.objownerName=''BranchInstitution'' where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' and
loanapp.ChannelType like ''%RETAIL%'''

IF @sRolename <> ''
BEGIN
SET @sql = @sql + ' and loanapp.CurrentStatus=''' + @sRolename + ''''
END
SET @sql = @sql
+ ' and (datediff(day,loanapp.CurrPipeStatusDate,getdate()) >= '
+ CONVERT(VARCHAR, @sday) + ')'
IF @whereClause <> ''
BEGIN
SET @sql = @sql + ' AND ' + @whereClause
END
SET @sql = @sql + ' ) as t2 where 1=1 '

-- print @sql
EXEC sp_executeSql @sql
SET @sql1 = ''
SET @sql1 = 'Select * from ( '
SET @sql1 = @sql1 + 'SELECT
ROW_NUMBER() OVER (ORDER BY ' + @sortField
+ ' ' + @SortOrder + ' )AS Row,''E3'' as DB,loanData.AdjustedNoteAmt AS Totalamt,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue as ProcessorName,
loanapp.currentStatus as LoanStatus, loanapp.CurrPipeStatusDate, CurrDecStatusDate as CurrDecisionstatus,loanapp.DecisionStatus,
loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.firstname as BorrowerFirstName,
borr.lastname as BorrowerLastName,'''' as LU_FINAL_HUD_STATUS, approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe, case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN ''VA'' End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM
(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,
CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate,
'''' AS PER from mwlLoanApp loanapp
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK''
and LockRecord.Status<>''CANCELED'' 
Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved''
Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''Branch'' and Corres.objownerName=''BranchInstitution''
left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID and CustFieldDef_id in(select Id from mwsCustFieldDef
where CustomFieldName=''Retail Processor'') Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id
and Branch.InstitutionType = ''Branch'' and Branch.objownerName=''BranchInstitution'' where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' and
loanapp.ChannelType like ''%RETAIL%'''

IF @sRolename <> ''
BEGIN
SET @sql1 = @sql1 + ' and loanapp.CurrentStatus=''' + @sRolename + ''''
END
SET @sql1 = @sql1
+ ' and (datediff(day,loanapp.CurrPipeStatusDate,getdate()) >= '+ CONVERT(VARCHAR, @sday) + ')'
IF @whereClause <> ''
BEGIN
SET @sql1 = @sql1 + ' AND ' + @whereClause
END
SET @sql1 = @sql1 + ' ) as t3 '
SET @sql1 = @sql1 + ' Where Row between (' + CAST(@PageNo AS NVARCHAR) + ' - 1) * ' + CAST(@Pagesize AS NVARCHAR) + ' + 1 and ' + CAST(@PageNo AS NVARCHAR) + ' * ' + CAST(@Pagesize AS NVARCHAR)
SET @sql1 = @sql1 + ' Order by row '
PRINT @sql1
EXEC sp_executesql @sql1
END


END

GO

