USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSecurityQuestions]') AND type in (N'U'))
DROP TABLE [dbo].[tblSecurityQuestions]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSecurityQuestions](	  [QuestionID] INT NOT NULL IDENTITY(1,1)	, [SecurityQuestion] VARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DisplayAt] INT NULL	, CONSTRAINT [PK_tblSecurityQuestions] PRIMARY KEY ([QuestionID] ASC))USE [HomeLendingExperts]
GO

