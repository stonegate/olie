USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_NthWorkingDayInMonth]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_NthWorkingDayInMonth]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- [dbo].[sp_Get_NthWorkingDayInMonth] 2014,1,6
Create PROCEDURE [dbo].[sp_Get_NthWorkingDayInMonth] 
(
@year int,
@month int,
@nthWorkingDay int
)

AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE	@StartDate DATETIME,
	@EndDate DATETIME

SELECT	@StartDate = convert(datetime, cast(@month as varchar)+'-01-'+cast(@year as varchar), 101),
	@EndDate = DATEADD(mm, 1, @StartDate) 

--print @StartDate;
--print @EndDate;

;WITH Yak(theDate)
AS (
	SELECT	@StartDate
	UNION ALL
	SELECT	DATEADD(DAY, 1, theDate)
	FROM	Yak
	WHERE	theDate < @EndDate
)

SELECT		y.theDate as NthBusinessDate,
		--w.WorkDate,
		w.nthBusinessDay
FROM		Yak AS y
LEFT JOIN	(
			SELECT	theDate AS WorkDate,
			ROW_NUMBER() OVER (PARTITION BY DATEDIFF(MONTH, '19000101', theDate) ORDER BY theDate) AS nthBusinessDay
			FROM	Yak
			WHERE	DATENAME(WEEKDAY, theDate) NOT IN ('Saturday', 'Sunday')
			and thedate not in (select Convert(varchar(10) , HolidayDate , 112) as HolidayDate from [tblHolidayList] where HolidayDate BETWEEN @StartDate AND @EndDate)
		) AS w ON w.WorkDate = y.theDate
where w.nthBusinessDay = @nthWorkingDay
ORDER BY	y.theDate
OPTION		(MAXRECURSION 0)

END


GO

