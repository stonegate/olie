USE [HomeLendingExperts]
								GO
								SET ANSI_NULLS ON
								GO
								SET QUOTED_IDENTIFIER ON
								GO

-- This function is used get the End date of Business Working Days from Current Date to 10 Working Days
-- Created By Paresh Rathod
-- Date: 12/23/2009
-- Purpose: Used in LockExpirationReportQuery
create FUNCTION [dbo].[GetWorkingDays]( ) 
RETURNS DATETIME  
AS  
BEGIN 
    DECLARE @range INT; 
 
    SET @range = 10; 
 
    RETURN  
    ( 
        SELECT		getdate() +
            11 / 7 * 5 + 11 % 7 +  
            ( 
                SELECT COUNT(*)  
            FROM 
                ( 
                    SELECT 1 AS d 
                    UNION ALL SELECT 2  
                    UNION ALL SELECT 3  
                    UNION ALL SELECT 4  
                    UNION ALL SELECT 5  
                    UNION ALL SELECT 6  
                    UNION ALL SELECT 7 
                ) weekdays 
                WHERE d <= 11 % 7  
                AND DATENAME(WEEKDAY, (getdate() + 10) + 1)  
                IN 
                ( 
                    'Saturday', 
                    'Sunday' 
                ) 
            ) 
    ); 
END  

GO

