USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_update_tblAuthenticateSecurityInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_update_tblAuthenticateSecurityInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_update_tblAuthenticateSecurityInfo]
(
@strUserId varchar(Max),
@IsUserid varchar(Max) = NULL
)
as
begin

if(@IsUserid = '' OR @IsUserid IS  NULL)
	update tblUserDetails 
	set lastlogintime = Getdate()
	,MaxSecurityAttempt = isnull(MaxSecurityAttempt,0) + 1 
	where UsersID = @strUserId
else
	update tblUserDetails 
	set UserIdlastlogintime = Getdate()
	,MaxUserIdSecurityAtmp = isnull(MaxUserIdSecurityAtmp,0) + 1 
	where UsersID = @strUserId
end


GO

