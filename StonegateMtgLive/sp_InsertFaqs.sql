USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertFaqs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertFaqs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create  Procedure [dbo].[sp_InsertFaqs]
(
@Question [varchar](1000),
@Answer [text],
@ModuleName [varchar](100),
@Published [bit],
@DisplayOrder int
)
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

Insert into dbo.tblfaqs
(
Question,
Answer,
ModuleName,
Published,
DisplayOrder,
CreatedDate,
ModifiedDate
)
Values
(
@Question,
@Answer,
@ModuleName,
@Published,
@DisplayOrder,
Getdate(),
Getdate()
)

End


GO

