USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_EmailReader]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_EmailReader]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Insert_EmailReader] 
(    
@ToAddress nvarchar(100),   
@ToName nvarchar(500),    
@MailSubject nvarchar,    
@MailBody nvarchar,
@MobileEmailId nvarchar(50),
@MobileEmailBody nvarchar   
)
AS
BEGIN
	 INSERT INTO tblEmailReader(ToAddress,ToName,MailSubject,MailBody,MobileEmailId,MobileEmailBody)      
     VALUES (@ToAddress,@ToName,@MailSubject,@MailBody,@MobileEmailId,@MobileEmailBody)    
    
END








GO

