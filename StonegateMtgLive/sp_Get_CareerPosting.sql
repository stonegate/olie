USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_CareerPosting]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_CareerPosting]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Get_CareerPosting] 
(
@stringmode varchar(50)
)
AS
BEGIN
select CareerId,PositionName,SUBSTRING (Description,0,50) + '...' as Description,IsPublished,PublishedDate,ModifiedDate from tblCareerPosting
END




GO

