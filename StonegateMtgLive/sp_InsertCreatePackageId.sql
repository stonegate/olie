USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertCreatePackageId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertCreatePackageId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nitin>
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertCreatePackageId] 
(
@PackageId int
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into tblPackageTypeId (PackageId) 
	Values(@PackageId)
END

--------------------------------------------------------------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[sp_getLoanType]    Script Date: 04/12/2013 17:45:14 ******/
SET ANSI_NULLS ON

GO

