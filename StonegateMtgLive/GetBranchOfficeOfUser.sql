USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBranchOfficeOfUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetBranchOfficeOfUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Shwetal Shah>
-- Create date: <22-05-13>
-- Description:	<Get branch Office for the User>
-- =============================================
CREATE PROCEDURE [dbo].[GetBranchOfficeOfUser]
(
	@UserID int
)
AS
BEGIN

Select Office From dbo.mwcInstitution WITH(NOLOCK) Where dbo.mwcInstitution.Office LIKE 'Retail%' And ID = (Select BranchInstitution_ID From mwaMWUser WITH(NOLOCK) Where ID = (Select E3UserID from tblUsers WITH(NOLOCK) Where UserID = @UserID))

END


GO

