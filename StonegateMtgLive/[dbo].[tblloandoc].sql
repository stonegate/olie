USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblloandoc]') AND type in (N'U'))
DROP TABLE [dbo].[tblloandoc]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblloandoc](	  [loanDocID] INT NOT NULL IDENTITY(1,1)	, [uloanregID] INT NULL DEFAULT((0))	, [DocName] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Comments] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Assigned] BIT NULL	, [AssignedBy] BIGINT NULL	, [AssignDate] DATETIME NULL	, [Completed] BIT NULL	, [CompletedBy] BIGINT NULL	, [TimeStamp] DATETIME NULL	, [userid] INT NULL	, [createddate] DATETIME NULL	, [loan_no] CHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL DEFAULT('True')	, [IsDeletedFromDocLog] BIT NULL DEFAULT('True')	, CONSTRAINT [PK_tblloandoc] PRIMARY KEY ([loanDocID] ASC))USE [HomeLendingExperts]
GO

