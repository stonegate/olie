USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_CareerPostingByID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_CareerPostingByID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Get_CareerPostingByID] 
(
@stringmode varchar(50),
@CareerId int
)
AS
BEGIN
select * from tblCareerPosting where CareerId = @CareerId
END




GO

