USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckforSubmitLoanKatalyst]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckforSubmitLoanKatalyst]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Shwetal Shah>  
-- Create date: <17/4/2012>  
-- Description: <This wil Check for Loans of Katalyst for View Loan Folder>  
-- MWFieldNum = 9553 Used for fatching Katalyst Folder ID from CustomField table in E3
-- =============================================  

CREATE PROCEDURE [dbo].[sp_CheckforSubmitLoanKatalyst] (
	@RoleID INT
	,@strID VARCHAR(max)
	,@strChLoanno VARCHAR(100)
	,@CurrentStatus VARCHAR(3000)
	,@oPuid VARCHAR(3000)
	,@strStatusDesc VARCHAR(50)
	,@OfficeName VARCHAR(50)
	)
AS
BEGIN
	SET @strChLoanno = '%' + @strChLoanno + '%'

	--ROLE VARIABLE DECLARATIONS-----------------------------------------------------------------------
	DECLARE @Admin INT

	SET @Admin = 0

	DECLARE @SalesManager INT

	SET @SalesManager = 3

	DECLARE @BranchManager INT

	SET @BranchManager = 10

	DECLARE @CaseOpener INT

	SET @CaseOpener = 12

	DECLARE @RetailOperMgr INT

	SET @RetailOperMgr = 11

	DECLARE @Closer INT

	SET @Closer = 15

	DECLARE @RetailTeamLd INT

	SET @RetailTeamLd = 16

	---------------------------------------------------------------------------------------------------
	DECLARE @Channeltype VARCHAR(20)

	SELECT TOP 1 @Channeltype = ChannelType
	FROM mwlLoanApp
	WHERE LoanNumber LIKE @strChLoanno

	IF (
			@RoleID != @Admin
			AND @RoleID != @CaseOpener
			AND @RoleID != @Closer
			AND @RoleID != @RetailOperMgr
			AND @RoleID != @RetailTeamLd
			)
	BEGIN
		IF (
				@RoleID = @SalesManager
				OR @RoleID = @BranchManager
				)
		BEGIN
			SELECT 'E3' AS DB
				,loanapp.casenum AS FHANumber
				,loanapp.ChannelType
				,loanapp.OriginatorName
				,loanapp.currentStatus AS LoanStatus
				,loanapp.DecisionStatus
				,loanapp.ID AS record_id
				,LoanNumber AS loan_no
				,borr.firstname AS BorrowerfirstName
				,borr.lastname AS BorrowerLastName
				,loanapp.CurrentStatus AS LoanStatus
				,appStat.StatusDateTime AS SubmittedDate
				,CASE 
					WHEN TransType IS NULL
						THEN ''
					WHEN TransTYpe = 'P'
						THEN 'Purchase'
					WHEN TransTYpe = 'R'
						THEN 'Refinance'
					END AS TransTYpe
				,loanData.AdjustedNoteAmt AS loan_amt
			FROM mwlLoanApp loanapp WITH (NOLOCK)
			INNER JOIN mwlBorrower AS borr ON borr.loanapp_id = loanapp.id
			INNER JOIN dbo.mwlloandata AS loanData ON loanData.ObjOwner_id = loanapp.id
				AND loanData.Active = 1
			INNER JOIN (
				SELECT ID
					,LoanApp_ID
					,StringValue
				FROM mwlCustomfield WITH (NOLOCK)
				WHERE CustFieldDef_ID = (
						SELECT ID
						FROM mwsCustFieldDef WITH (NOLOCK)
						WHERE MWFieldNum = 9553
						)
				) mwCustFld ON mwCustFld.LoanApp_ID = loanapp.ID
			LEFT JOIN dbo.mwlInstitution AS Branch ON Branch.ObjOwner_id = loanapp.id
				AND Branch.InstitutionType = 'Branch'
				AND Branch.objownerName = 'BranchInstitution'
			LEFT JOIN mwlAppStatus appStat ON loanapp.ID = appStat.LoanApp_id
				AND appStat.sequencenum = 1
				AND appStat.StatusDesc = @strStatusDesc
			WHERE borr.sequencenum = 1
				AND LoanNumber IS NOT NULL
				AND LoanNumber <> ''
				AND loanapp.ChannelType = @Channeltype
				AND CurrentStatus IN (
					SELECT items
					FROM dbo.SplitString(@CurrentStatus, ',')
					)
				AND (Branch.Office IN (@OfficeName))
				AND (
					LoanNumber LIKE @strChLoanno
					OR borr.lastname LIKE @strChLoanno
					)
		END
		ELSE
		BEGIN
			SELECT 'E3' AS DB
				,loanapp.casenum AS FHANumber
				,loanapp.ChannelType
				,loanapp.OriginatorName
				,loanapp.currentStatus AS LoanStatus
				,loanapp.DecisionStatus
				,loanapp.ID AS record_id
				,LoanNumber AS loan_no
				,borr.firstname AS BorrowerfirstName
				,borr.lastname AS BorrowerLastName
				,loanapp.CurrentStatus AS LoanStatus
				,appStat.StatusDateTime AS SubmittedDate
				,CASE 
					WHEN TransType IS NULL
						THEN ''
					WHEN TransTYpe = 'P'
						THEN 'Purchase'
					WHEN TransTYpe = 'R'
						THEN 'Refinance'
					END AS TransTYpe
				,loanData.AdjustedNoteAmt AS loan_amt
			FROM mwlLoanApp loanapp WITH (NOLOCK)
			INNER JOIN mwlBorrower AS borr ON borr.loanapp_id = loanapp.id
			INNER JOIN dbo.mwlloandata AS loanData ON loanData.ObjOwner_id = loanapp.id
				AND loanData.Active = 1
			INNER JOIN (
				SELECT ID
					,LoanApp_ID
					,StringValue
				FROM mwlCustomfield WITH (NOLOCK)
				WHERE CustFieldDef_ID = (
						SELECT ID
						FROM mwsCustFieldDef WITH (NOLOCK)
						WHERE MWFieldNum = 9553
						)
				) mwCustFld ON mwCustFld.LoanApp_ID = loanapp.ID
			LEFT JOIN dbo.mwlInstitution AS Branch ON Branch.ObjOwner_id = loanapp.id
				AND Branch.InstitutionType = 'Branch'
				AND Branch.objownerName = 'BranchInstitution'
			LEFT JOIN mwlAppStatus appStat ON loanapp.ID = appStat.LoanApp_id
				AND appStat.sequencenum = 1
				AND appStat.StatusDesc = @strStatusDesc
			WHERE borr.sequencenum = 1
				AND LoanNumber IS NOT NULL
				AND LoanNumber <> ''
				AND loanapp.ChannelType = @Channeltype
				AND CurrentStatus IN (
					SELECT items
					FROM dbo.SplitString(@CurrentStatus, ',')
					)
				AND (
					Originator_id IN (
						SELECT items
						FROM dbo.SplitString(@strID, ',')
						)
					OR Originator_id IN (
						SELECT items
						FROM dbo.SplitString(@oPuid, ',')
						)
					OR loanapp.id IN (
						SELECT items
						FROM dbo.SplitString(@strID, ',')
						)
					)
				AND (
					LoanNumber LIKE @strChLoanno
					OR borr.lastname LIKE @strChLoanno
					)
		END
	END
	ELSE
	BEGIN
		SELECT 'E3' AS DB
			,loanapp.casenum AS FHANumber
			,loanapp.ChannelType
			,loanapp.OriginatorName
			,loanapp.currentStatus AS LoanStatus
			,loanapp.DecisionStatus
			,loanapp.ID AS record_id
			,LoanNumber AS loan_no
			,borr.firstname AS BorrowerfirstName
			,borr.lastname AS BorrowerLastName
			,loanapp.CurrentStatus AS LoanStatus
			,appStat.StatusDateTime AS SubmittedDate
			,CASE 
				WHEN TransType IS NULL
					THEN ''
				WHEN TransTYpe = 'P'
					THEN 'Purchase'
				WHEN TransTYpe = 'R'
					THEN 'Refinance'
				END AS TransTYpe
			,loanData.AdjustedNoteAmt AS loan_amt
		FROM mwlLoanApp loanapp
		INNER JOIN mwlBorrower AS borr ON borr.loanapp_id = loanapp.id
		INNER JOIN dbo.mwlloandata AS loanData ON loanData.ObjOwner_id = loanapp.id
			AND loanData.Active = 1
		INNER JOIN (
			SELECT ID
				,LoanApp_ID
				,StringValue
			FROM mwlCustomfield
			WHERE CustFieldDef_ID = (
					SELECT ID
					FROM mwsCustFieldDef
					WHERE MWFieldNum = 9553
					)
			) mwCustFld ON mwCustFld.LoanApp_ID = loanapp.ID
		LEFT JOIN dbo.mwlInstitution AS Branch ON Branch.ObjOwner_id = loanapp.id
			AND Branch.InstitutionType = 'Branch'
			AND Branch.objownerName = 'BranchInstitution'
		LEFT JOIN mwlAppStatus appStat ON loanapp.ID = appStat.LoanApp_id
			AND appStat.sequencenum = 1
			AND appStat.StatusDesc = @strStatusDesc
		WHERE borr.sequencenum = 1
			AND LoanNumber IS NOT NULL
			AND LoanNumber <> ''
			AND loanapp.ChannelType = 'RETAIL'
			AND CurrentStatus IN (
				SELECT items
				FROM dbo.SplitString(@CurrentStatus, ',')
				)
			AND (
				LoanNumber LIKE @strChLoanno
				OR borr.lastname LIKE @strChLoanno
				)
	END
END


GO

