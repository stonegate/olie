USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_Users_By_Role]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_Users_By_Role]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Rakesh>
-- Create date: <Create Date, 7tn May 2014>
-- Description:	<Description, Select Records containing all Active users for specific Role from tblUsers table>
-- EXEC sp_Get_Users_By_Role 1
-- =============================================
CREATE PROCEDURE [dbo].[sp_Get_Users_By_Role]
	@RoleId Int = Null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
		U.ufirstname + ' ' + U.ulastname As UserName,
		U.userid
		
	FROM
		[dbo].[tblUsers] U 
	Where  (@RoleId IS NULL OR U.urole = @RoleId)
			AND U.IsActive = 1
END





GO

