USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDailyActivity_With_Region]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDailyActivity_With_Region]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[sp_GetDailyActivity_With_Region]  
(  
@strID varchar(max)  ,
@Puid varchar(max),
@FromDate  varchar(20),  
@ToDate varchar(20)
 
)  
As  
              
Declare @SQL nvarchar(max)   
Set @SQL =''    
  
Set @SQL ='Select ''E3'' as DB,loanapp.ChannelType as BusType, loanapp.Originator_id,loanapp.ID as record_id,LoanNumber as loan_no,
		borr.lastname as BorrowerName,borr.firstname as BorrowerFirstName,borr.lastname as BorrowerLastName,loanapp.DecisionStatus,
		loanapp.CurrentStatus as LoanStatus,  case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' 
		WHEN ''V'' THEN  ''VA'' WHEN ''M'' THEN  ''USDA/Rural Housing Service'' End as LoanProgram ,case when loanapp.LoanPurpose1003 
		is null then '''' when loanapp.LoanPurpose1003 = ''PU'' then ''Purchase'' when loanapp.LoanPurpose1003 = ''RE'' then 
		''Refinance'' end as Purpose,  CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed, 
		LoanData.AdjustedNoteAmt as LoanAmount,  CONVERT(DECIMAL(10,2),(ISNULL(LoanData.LTV,0) * 100)) as LTV,CONVERT(DECIMAL(10,3),
		(LoanData.NoteRate * 100)) as int_rate,  LoanData.ShortProgramName as prog_code, Broker.Company as brok_name ,  
	  loanapp.Originatorname ,  loanapp.Branch Branch1,  
    loanapp.MWBranchCode,  Broker.Office Branch  FROM mwlLoanApp  loanapp  
    
	Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id    
	Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id    
	Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = ''BRANCH''   
	and Broker.objownerName=''BranchInstitution''  
	--Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc=''62 - Funded''  and AppStatus.sequencenum=1     
	--Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id    and AppStatus.sequencenum=1     
  
Where  
 (loanapp.transtype=''R'' Or loanapp.transtype=''P'')'  
  
 
Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+  @Puid  +''','','')) or loanapp.id in (select * from dbo.SplitString('''+  @strID  +''','','')) or Broker.office in (select * from dbo.SplitString('''+  @Puid  +''','','')) )'  


--Set @SQL = @SQL + ' And convert(varchar,AppStatus.StatusDateTime,103)=convert(varchar,getdate(),103) '  

--  Set @SQL = @SQL + ' and  AppStatus.StatusDateTime between convert(datetime,''' + @FromDate + ''',101) and convert(datetime,''' + @ToDate + ''',101)'  

				if(@FromDate != '' and @ToDate !='')
					Set @SQL=@SQL+ ' and loanapp.CreatedonDate between '''+@FromDate+'''  and 
					 '''+Convert(varchar(20),DATEADD (dd , 1 , @ToDate),101)+''''
				else if(@FromDate != '')
					Set @SQL=@SQL+ ' and loanapp.CreatedonDate > '''+@FromDate+''''	
			  	else if(@ToDate != '')
					Set @SQL=@SQL+ ' and loanapp.CreatedonDate < '''+@ToDate+''''   


Set @SQL = @SQL + ' and (loanapp.CurrentStatus=''13 - File Intake'' )  
     And  (loanData.FinancingType=''C'' or loanData.FinancingType=''F'' or loanData.FinancingType=''V'' or loanData.FinancingType=''M'')  
     and loanapp.ChannelType like ''%RETAIL%''  
     and LoanNumber is not null and LoanNumber <> '''''  
  
Print @SQL              
exec sp_executesql @SQL

/*------------*/


GO

