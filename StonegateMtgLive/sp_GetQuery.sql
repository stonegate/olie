USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetQuery]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetQuery]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetQuery]
(
@MethodName VARCHAR(MAX)

)
AS
BEGIN
SELECT Query FROM tblDbQuery WHERE MethodName=MethodName AND DB='P'

END

GO

