USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_update_tblTracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_update_tblTracking]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc usp_update_tblTracking (@UserID int)
AS
BEGIN
 DECLARE @LastID BIGINT
 SET @LastID = (SELECT top 1 ID FROM tblTracking where UserID=@UserID ORDER BY ID DESC)
 UPDATE tblTracking SET LogoutTime = GETDATE() where Id = @LastID
END

GO

