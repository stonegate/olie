USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLoanDocs_loandoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLoanDocs_loandoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertLoanDocs_loandoc]

(
@uloanregid int ,
@Docname varchar(100),
@Comments varchar(500)
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into tblloandoc(uloanregid,Docname,Comments)
Values(@uloanregid,@Docname,@Comments)
END


GO

