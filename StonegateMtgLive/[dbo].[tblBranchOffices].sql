USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBranchOffices]') AND type in (N'U'))
DROP TABLE [dbo].[tblBranchOffices]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBranchOffices](	  [id] INT NULL	, [OfficeName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [OfficeValue] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Street] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [State] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [City] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Zip] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Phone] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Fax] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Email] VARCHAR(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [GroupId] INT NULL	, [lat1] DECIMAL(18,6) NULL	, [lng1] DECIMAL(18,6) NULL	, [region] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Branch] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO

