USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteOffices]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteOffices]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsOffice.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_DeleteOffices]
(
@userid varchar(Max)
)
AS
BEGIN

delete from tblOffices Where userid =@userid
	 
END

GO

