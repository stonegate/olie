USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetConditionText]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetConditionText]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetConditionText]
(
@record_id varchar(max)
)
AS
BEGIN
SELECT parent_id,cond_text FROM dbo.condits WHERE record_id=@record_id
END


GO

