USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCannedReportSummaryDetail1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCannedReportSummaryDetail1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  
CREATE PROCEDURE [dbo].[sp_GetCannedReportSummaryDetail1]      
(      
@RoleID int,     
@strID varchar(2000),       
@From varchar(15),    
@Year varchar(4),     
@Month varchar(4)    
)      
AS      
BEGIN      
      
Declare @SQL nvarchar(max)      
Set @SQL =''      
Set @SQL ='select ''E3'' as DB,loanapp.ID as record_id,CONVERT(DECIMAL(10,3),(loanData.NoteRate * 100)) as int_rate,LoanNumber as loan_no,borr.lastname as BorrowerLastName,
   CONVERT(DECIMAL(10,2),isnull(loanData.LTV * 100,0))LTV,CurrentStatus as LoanStatus,convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram, case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as Purpose,  
    CAST(CreditScore AS CHAR(5)) as CreditScoreUsed,loanData.UseCashInOut as CashOut,isnull(LoanData.AdjustedNoteAmt, LoanData.AppraisedValue) as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockExpirationDate,LockRecord.LockDateTime as LOCK_DATE  
   from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id 
    Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
    Left join dbo.mwlCreditScore as BorrCredit on BorrCredit.borrower_id = borr.id   
    Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc=''16 - Funded'' 
    left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id
	where'
                if (@From = 'locked')     
                     Set @SQL = @SQL + ' CurrentStatus IN (''16 - Funded'',''17 - In Shipping'',''18 - Loan Shipped'',''19 - Investor Suspense'',''21 - Final Docs Completed'',''20 - Loan Sold'') and Year(AppStatus.StatusDateTime)=''' + @Year + ''' and Month(AppStatus.StatusDateTime)=''' + @Month + ''''                    
                else     
                     Set @SQL = @SQL + ' Year(LockRecord.LockDateTime)=''' + @Year + ''' and Month(LockRecord.LockDateTime)=''' + @Month + ''''    
                    
   if (len(@strID) = 0)    
  Begin    
	if(@RoleID !=0)
	begin
    Set @SQL = @SQL + ' and (Originator_id in (''' + @strID + '''))'    
	end
  End    
 else   
   Begin    
   if(@RoleID !=0)
	begin
    Set @SQL = @SQL + ' and (Originator_id in (''' + @strID + '''))'    
	end    
   End    
              
                      
        
Print @SQL      
exec sp_executesql @SQL      
END 


GO

