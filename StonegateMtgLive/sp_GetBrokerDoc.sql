USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBrokerDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBrokerDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetBrokerDoc]

AS
BEGIN
select *from tblLoanDocs LD 
inner join tblusers US on LD.Userid=US.Userid where urole=3
END



GO

