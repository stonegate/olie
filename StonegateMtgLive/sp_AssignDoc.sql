USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AssignDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_AssignDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_AssignDoc]
(
@AssignedBy varchar(max),
@ctID varchar(max)
)
AS
BEGIN
update tblLoanDocs set Assigned = 1,AssignedBy=@AssignedBy,Assigndate=getdate() where ctID = @ctID
END



GO

