USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getSecurityQuestions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[getSecurityQuestions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
Create  Procedure [dbo].[getSecurityQuestions]
 -- Add the parameters for the stored procedure here  
 @QuestionID int  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
    -- Insert statements for procedure here  
 if (@QuestionID = 0)  
 SELECT [QuestionID],[SecurityQuestion],[DisplayAt] FROM [tblSecurityQuestions]  
 else  
 SELECT [QuestionID],[SecurityQuestion],[DisplayAt] FROM [tblSecurityQuestions] where [QuestionID] = @QuestionID  
  
END 


GO

