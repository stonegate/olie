USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertInForm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertInForm]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertInForm]
(
@Description varchar(200),
@FileName varchar(200),
@dttime datetime,
@isnews bit,
@CateID int
)
AS

BEGIN
	 Insert into tblForms(Description,FileName,dttime,isnews,CateID) Values 

(@Description,@FileName,@dttime,@isnews,@CateID)

END


GO

