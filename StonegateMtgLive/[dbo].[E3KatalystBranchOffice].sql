USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[E3KatalystBranchOffice]') AND type in (N'U'))
DROP TABLE [dbo].[E3KatalystBranchOffice]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[E3KatalystBranchOffice](	  [ID] INT NOT NULL	, [E3BranchOfficeID] VARCHAR(32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [E3BranchOffice] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsKatalyst] BIT NULL	, CONSTRAINT [PK_E3KatalystBranchOffice] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO

