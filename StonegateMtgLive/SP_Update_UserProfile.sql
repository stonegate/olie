USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Update_UserProfile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Update_UserProfile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel  
-- Create Date : 10/04/2012  
-- Description: Procedure for Update User Profile
-- =============================================         
CREATE PROC [dbo].[SP_Update_UserProfile](         
@UserID int,            
@UFirstName VARCHAR(50),                  
@ULastName  VARCHAR(50),                    
@UEmailId  VARCHAR(200),    
@mobile_ph nvarchar(50),    
@upassword nvarchar(200),    
@carrierid int,
@photoname nvarchar(100)= null,
@logoname nvarchar(100) = null         
)                  
AS                  
BEGIN  

declare @Flag nvarchar(10)
if @photoname is not null and @logoname is not null
Begin
set @Flag = 'Both'
end
else if @photoname is not null and @logoname is null
Begin
set @Flag = 'Photo'
end
 else if @photoname is  null and @logoname is not null
Begin
set @Flag = 'Logo'
end

if @Flag = 'Photo'   
Begin
  update tblusers   
 set ufirstname=@UFirstName,ulastname=@ULastName,uemailid=@UEmailId,    
	mobile_ph=@mobile_ph,upassword=upassword,carrierid=@carrierid , photoname = @photoname
	where userid=@UserID 
 end
 else if @Flag = 'Logo' 
 Begin
  update tblusers   
 set ufirstname=@UFirstName,ulastname=@ULastName,uemailid=@UEmailId,    
	mobile_ph=@mobile_ph,upassword=upassword,carrierid=@carrierid ,logoname = @logoname
	where userid=@UserID 
 end
else if @Flag = 'Both'
Begin
 update tblusers   
 set ufirstname=@UFirstName,ulastname=@ULastName,uemailid=@UEmailId,    
	mobile_ph=@mobile_ph,upassword=upassword,carrierid=@carrierid , photoname = @photoname,
	logoname = @logoname
	where userid=@UserID 

end
else
Begin
update tblusers        
	set ufirstname=@UFirstName,ulastname=@ULastName,uemailid=@UEmailId,    
	mobile_ph=@mobile_ph,upassword=upassword,carrierid=@carrierid  
	where userid=@UserID 
end
END 


	
	
	



GO

