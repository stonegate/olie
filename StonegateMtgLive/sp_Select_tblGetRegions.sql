USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetRegions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetRegions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Muruga Nagaraju
-- Create date: 31/01/2014
-- Description: Procedure for clsUser.cs bll    
-- =============================================  
CREATE PROCEDURE [dbo].[sp_Select_tblGetRegions]
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SELECT *
		,'Retail' AS Type
	FROM tblregion
	WHERE IsActive = 1
END

--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




GO

