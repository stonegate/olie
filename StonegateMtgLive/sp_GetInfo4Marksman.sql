USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetInfo4Marksman]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetInfo4Marksman]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetInfo4Marksman]
	-- Add the parameters for the stored procedure here
	


@userid int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select ufirstname + ' ' + ulastname as fullname, city, [state], uemailid, phonenum, GroupName  from tblusers left join TblRealtyGroup on TblRealtyGroup.groupId = tblusers.realtorGroupId where urole = 5 and userid=@userid 
END


GO

