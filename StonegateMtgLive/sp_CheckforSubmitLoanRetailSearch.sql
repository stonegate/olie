USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckforSubmitLoanRetailSearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckforSubmitLoanRetailSearch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================
-- Created By:	Chandresh  Patel
-- Create Date : 10/04/2012
-- Description:	Procedure to insert/update/delete Rates
-- =======================================================

  
CREATE PROCEDURE [dbo].[sp_CheckforSubmitLoanRetailSearch]        
(        
@RoleID int,       
@strID varchar(2000),        
@strChLoanno varchar(100),        
@CurrentStatus varchar(3000)       
)        
AS        
BEGIN        
        
Declare @SQL nvarchar(max)        
Set @SQL =''        
Set @SQL ='select ''E3'' as DB,LockDateTime,loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.lastname as      
 BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate,      
 approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when       
 TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,      
 case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,      
 loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,      
 LockRecord.LockExpirationDate,UnderwriterName as UnderWriter,Broker.Company as BrokerName,Lookups.DisplayString as CashOut,      
 CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate ,       
 CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER''      
 and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0      
 WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'')      
 and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'')      
 and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0        
 ELSE (SELECT count(a.ID) as TotalCleared       
 FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'') and (CurrentState=''CLEARED'' OR       
 CurrentState=''SUBMITTED'') and a.objOwner_ID IN       
 (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions      
 FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1      
 WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER       
 from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  Inner join dbo.mwlloandata as loanData      
 on loanData.ObjOwner_id = loanapp.id        
 left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  Left join dbo.mwlInstitution as Broker on       
 Broker.ObjOwner_id = loanapp.id       
 and Broker.InstitutionType = ''Branch'' and Broker.objownerName=''BranchInstitution'' left join dbo.mwlLockRecord as LockRecord       
 on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED''       
 Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc like ''%Registered%''      
 Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved''      
 Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''         
 where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and       
 loanapp.ChannelType like ''%RETAIL%'' and  CurrentStatus    IN (select * from dbo.SplitString('''+@CurrentStatus+''','',''))'      
      
      
if (@RoleID != 0 and @RoleID != 12 and @RoleID != 15 and @RoleID != 11 and  @RoleID != 16 )      
 BEGIN      
  if (len(@strID) = 0)         
   Set @SQL = @SQL + ' and (Originator_id in (' + @strID + ')  or loanapp.id in (''' + @strID + '''))'      
               
  else       
     Set @SQL = @SQL + ' and (Originator_id in (' + @strID + ') or loanapp.id in (''' + @strID + '''))'      
 END      
                      
SET @strChLoanno = '%' + @strChLoanno + '%'                
Set @SQL = @SQL + '  AND  (LoanNumber like   '''+ @strChLoanno +'''   OR borr.lastname like '''+ @strChLoanno +''')'      
Set @SQL = @SQL + '  order by Per desc '      
          
Print @SQL        
exec sp_executesql @SQL        
END 

GO

