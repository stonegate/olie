USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteNewsAnnouncement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteNewsAnnouncement]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_DeleteNewsAnnouncement]    
@id varchar(50)    
AS    
BEGIN
-- IF exists (SELECT * FROM tblNewsAndAnnouncement where ID=@ID AND cc_id='0')  
-- begin  
-- --print 'a'  
--  Delete FROM tpo_tblNewsAndAnnouncement where cc_id=@id  
-- end  
--else  
-- begin  
--  DELETE FROM tpo_tblNewsAndAnnouncement where ID = (SELECT cc_id FROM tblNewsAndAnnouncement where ID=@ID)   
-- --print 'b'  
-- end   
DELETE FROM tpo_tblNewsAndAnnouncement
WHERE id = @id 
END 


GO

