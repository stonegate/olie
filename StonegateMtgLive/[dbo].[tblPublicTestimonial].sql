USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPublicTestimonial]') AND type in (N'U'))
DROP TABLE [dbo].[tblPublicTestimonial]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPublicTestimonial](	  [ID] INT NOT NULL IDENTITY(1,1)	, [IsPublish] BIT NULL	, [Testimionial] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL	, [CreatedDate] DATETIME NULL DEFAULT(getdate())	, CONSTRAINT [PK_tblTestimonial] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO

