USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IsAlertRegistered]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IsAlertRegistered]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_IsAlertRegistered]
(
@userid varchar(Max)
)
AS
BEGIN
select * from tblServiceInfo where userid = @userid
     
END

GO

