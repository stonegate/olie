USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPreApprovedE3Report]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPreApprovedE3Report]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetPreApprovedE3Report] (@RoleID int,
@strID varchar(max),
@Status varchar(max),
@oPuid varchar(max),
@oYear varchar(100),
@oFromDate varchar(20),
@oToDate varchar(20),
@Branch varchar(max))
AS
BEGIN
  SET NOCOUNT ON;
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

  DECLARE @SQL nvarchar(max)
  -- For BRD-125 Reports (adding branch and region column)
  DECLARE @ForRegion bit

  IF (@RoleID = 0
    OR @RoleID = 11
    OR @RoleID = 1
    OR @RoleID = 7
    OR @RoleID = 19
    OR @RoleID = 3
    OR @RoleID = 10
    OR @RoleID = 2)
  BEGIN
    SET @ForRegion = 1
  END
  ELSE
  BEGIN
    SET @ForRegion = 0
  END
  SET @SQL = ''
  SET @SQL = 'select ''E3'' as DB, loanapp.ChannelType as BusType,loanapp.ID as record_id,CONVERT(DECIMAL(10,3),
		(loanData.NoteRate * 100)) as int_rate,LoanNumber as loan_no,borr.firstname as BorrowerFirstName ,
		borr.lastname as BorrowerLastName,loanapp.Originatorname,isnull(isnull(convert(decimal(10,2), 
		loanData.LTV * 100),0),0) LTV,LoanApp.DecisionStatus,LoanApp.CurrentStatus as LoanStatus,
		convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,
		LoanData.ShortProgramName as LoanProgram,
		case when loanapp.LoanPurpose1003 is null then '''' when loanapp.LoanPurpose1003 = ''PU'' then ''Purchase'' 
		when loanapp.LoanPurpose1003 = ''RE'' then 
		''Refinance'' end as Purpose,
        CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed,case loanData.UseCashInOut 
        when ''0'' then ''No'' when ''1'' then ''Yes'' end as CashOut,isnull(loanData.AdjustedNoteAmt,0) as loan_amt,
        convert(varchar(35),EstCloseDate,101) AS ScheduleDate,
        CurrPipeStatusDate,UnderwriterName as UnderWriter,CloserName as Closer,
        CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as  BrokerName, institute.office as ''Branch Office'' '
  IF (@ForRegion = 1) -- For BRD-125 Reports (adding branch and region column)
  BEGIN
    SET @SQL = @SQL + ', region.regionname AS region '
  END

  SET @SQL = @SQL + '           
			from mwlLoanApp  loanapp  
			INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
			INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
			Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
            Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
            left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
            Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and 
            Broker.InstitutionType = ''BRANCH'' and Broker.objownerName=''BranchInstitution''
            left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id 
            and  LockRecord.LockType=''LOCK'' and LockRecord.Status<>''CANCELED''   
            Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = 
			''CORRESPOND'' and Corres.objownerName=''Correspondent'' '
  IF (@ForRegion = 1) -- For BRD-125 Reports (adding branch and region column)
  BEGIN
    SET @SQL = @SQL + '  left join tblbranchoffices AS bo ON bo.branch=institute.Office left join tblregion AS region ON bo.region = region.regionvalue  '
  END
  SET @SQL = @SQL + ' 
            where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''
            and loanapp.ChannelType like ''%RETAIL%''
            and CurrentStatus  IN (select * from dbo.SplitString(''' + @Status + ''','','')) '

  IF (@ForRegion = 1) -- For BRD-125 Reports (adding branch and region column)
  BEGIN
    SET @SQL = @SQL + ' and (Originator_id in 
                        (select * from dbo.SplitString(''' + @strID + ''','','')))'
    IF (@Branch IS NOT NULL
      AND @Branch <> '')
    BEGIN
      SET @SQL = @SQL + ' and (institute.Office in(select * from dbo.SplitString(''' + @Branch + ''','','')))'
    END
  END
  ELSE
  BEGIN
    IF (@RoleID != 0
      AND @RoleID != 1)
    BEGIN
      IF (LEN(@strID) = 0)
      BEGIN
        SET @SQL = @SQL + '  and (Originator_id in 
					   (select * from dbo.SplitString(''' + @strID + ''','','')))'

        IF (@oPuid != '')
        BEGIN
          SET @SQL = @SQL + '  and (Originator_id in 
						 (select * from dbo.SplitString	 (''' + @oPuid + ''','','')))'
        END

      END
      ELSE
      BEGIN


        SET @SQL = @SQL + '  and (Originator_id in 
                        (select * from dbo.SplitString(''' + @strID + ''','','')))'

        IF (@oPuid != '')

          SET @SQL = @SQL + '   and (Originator_id in 
                           (select * from dbo.SplitString(''' + @oPuid + ''','','')))'


      END
    END
  END

  IF (@oYear != '')
    SET @SQL = @SQL + ' and YEAR(CurrPipeStatusDate) = ''' + @oYear + ''''

  IF (@oFromDate != ''
    AND @oToDate != '')
    SET @SQL = @SQL + ' and CurrPipeStatusDate between ''' + @oFromDate + '''  and  ''' + CONVERT(varchar(20), DATEADD(dd, 1, @oToDate), 101) + ''''
  ELSE
  IF (@oFromDate != '')
    SET @SQL = @SQL + ' and CurrPipeStatusDate > ''' + @oFromDate + ''''
  ELSE
  IF (@oToDate != '')
    SET @SQL = @SQL + ' and CurrPipeStatusDate < ''' + @oToDate + ''''

  PRINT @SQL
  EXEC sp_executesql @SQL
END
/*===========================Credit Pulled============================= */




GO

