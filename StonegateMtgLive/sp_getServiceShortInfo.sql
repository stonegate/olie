USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getServiceShortInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getServiceShortInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author: Abhishek Rastogi    
-- Create Date : 10/06/2012    
-- Edited By : --    
-- Edited Date : --    
-- Description: for clsPipeLine.cs BLL    
-- =============================================    
    
CREATE PROCEDURE [dbo].[sp_getServiceShortInfo]    
(    
@loanid VARCHAR(MAX)    
)    
AS    
BEGIN    
SELECT lastpmtRecvdDate, PrincipalBal, DueDate, isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt FROM   
Loan  
 where loanid = @loanid    
    
select top 1 Escrowamt as EscrowPmt,transactionamt as TotalPmt,(transactionamt - Escrowamt) as PIPmt,* from history where loanid = @loanid and transactioncode in( '210' , '211' ) order by transactionDate Desc  
    
select top 1 PIPayment,EscrowPmt,(PIPayment + EscrowPmt) as TotalPmt,* from paymentchange where loanid = @loanid order by duedate Desc    
END      
GO

