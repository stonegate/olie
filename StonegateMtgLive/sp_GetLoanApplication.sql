USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanApplication]
	-- Add the parameters for the stored procedure here

@LoanApplicationID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	SELECT     LoanApplicationID, LoanReasonID, HometypeID, Producttypeid, StateID, CreditScoreID, AnnulIncomeBorrower,LoanAmount, AssetBorrower,  
                RetirementAssetsBorrower, PropertyLocated, HomeOwned, PurchasePrise, DownPaymentAmt, PartOfTotalAssets, DownPaymentSourceID,   
                HaveRealtor, RealtorContactName, RealtorPhone, RealtorEmail, AppFName, AppLName,vAddressLine1,vAddressLine2, AppStateID, AppZip, AppPrimaryPhone, AppSecondaryPhone,   
                AppEmail, CurrentPropertyValue, PropertyPurchaseMonth, YearID, ExistingPuchasePrice, CurrentMortageBal, MonthlyPayment, SecondMortage,   
                SecondMortageBal, DateEntered, ModifiedBy, ModifiedDate, UserAppearUrl, PropertyTypeID, PropertyTypeName, ContactTime, ContactTimeID,
                Hometype, CreditScoreValue, StateName, LoanReason, ProductTypeName, AppStateName, Year, SourceOfdownPayment ,MAName,IsWorkingWithRealtor, PropertyRealtorCity,IsCashback,RealtorName,RealtorPhoneNo 
                FROM         (SELECT     tblLoanApplication.LoanApplicationID, tblLoanApplication.LoanReasonID, tblLoanApplication.HometypeID, tblLoanApplication.Producttypeid,   
                tblLoanApplication.StateID, tblLoanApplication.CreditScoreID, tblLoanApplication.AnnulIncomeBorrower, tblLoanApplication.LoanAmount, tblLoanApplication.AssetBorrower,   
                tblLoanApplication.RetirementAssetsBorrower, tblLoanApplication.PropertyLocated, tblLoanApplication.HomeOwned,   
                tblLoanApplication.PurchasePrise, tblLoanApplication.DownPaymentAmt, tblLoanApplication.PartOfTotalAssets,
                tblLoanApplication.DownPaymentSourceID, tblLoanApplication.HaveRealtor, tblLoanApplication.RealtorContactName,   
                tblLoanApplication.RealtorPhone, tblLoanApplication.RealtorEmail, tblLoanApplication.AppFName, tblLoanApplication.AppLName, tblLoanApplication.vAddressLine1, tblLoanApplication.vAddressLine2,  
                tblLoanApplication.AppStateID, tblLoanApplication.AppZip, tblLoanApplication.AppPrimaryPhone, tblLoanApplication.AppSecondaryPhone,   
                tblLoanApplication.AppEmail, tblLoanApplication.CurrentPropertyValue, tblLoanApplication.PropertyPurchaseMonth,   
                tblLoanApplication.YearID, tblLoanApplication.ExistingPuchasePrice, tblLoanApplication.CurrentMortageBal, 
                tblLoanApplication.MonthlyPayment, tblLoanApplication.SecondMortage, tblLoanApplication.SecondMortageBal,
                tblLoanApplication.DateEntered, tblLoanApplication.ModifiedBy, tblLoanApplication.ModifiedDate, tblLoanApplication.UserAppearUrl,   
                tblLoanApplication.PropertyTypeID, tblPropertyType.Name AS PropertyTypeName, tblContactTime.ContactTime,   
                tblLoanApplication.ContactTimeID, tblHomeType.Hometype, tblCreditScore.CreditScoreValue, tblStateNylex.StateName,   
                tblLoanReason.LoanReason, tblProductType.name AS ProductTypeName, tblStateNylex_1.StateName AS AppStateName, tblYear.Year,   
                tblDownPaymentSource.SourceOfdownPayment,tblLoanApplication.MAName,tblLoanApplication.IsWorkingWithRealtor, tblLoanApplication.PropertyRealtorCity,tblLoanApplication.IsCashback,tblLoanApplication.RealtorName,tblLoanApplication.RealtorPhoneNo   
                FROM          tblLoanApplication LEFT JOIN  
                tblPropertyType ON tblLoanApplication.PropertyTypeID = tblPropertyType.PropertyTypeID LEFT JOIN  
                tblContactTime ON tblLoanApplication.ContactTimeID = tblContactTime.ContactTimeID LEFT JOIN  
                tblHomeType ON tblLoanApplication.HometypeID = tblHomeType.HometypeID LEFT JOIN
                tblCreditScore ON tblLoanApplication.CreditScoreID = tblCreditScore.CreditScoreID LEFT JOIN  
                tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID LEFT JOIN  
                tblLoanReason ON tblLoanApplication.LoanReasonID = tblLoanReason.LoanReasonID LEFT JOIN  
                tblProductType ON tblLoanApplication.Producttypeid = tblProductType.producttypeid LEFT JOIN  
                tblStateNylex AS tblStateNylex_1 ON tblLoanApplication.AppStateID = tblStateNylex_1.StateID LEFT JOIN  
                tblYear ON tblLoanApplication.YearID = tblYear.YearID LEFT OUTER JOIN  
                tblDownPaymentSource ON tblLoanApplication.DownPaymentSourceID = tblDownPaymentSource.DownPaymentSourceID) AS App where loanApplicationID = @loanApplicationID
END


GO

