USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteFaqs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteFaqs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create  Procedure [dbo].[sp_DeleteFaqs]
@ID bigint
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

Delete from dbo.tblfaqs where ID=@ID

END


GO

