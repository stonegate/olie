USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUserGuide]') AND type in (N'U'))
DROP TABLE [dbo].[tblUserGuide]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserGuide](	  [ID] INT NOT NULL IDENTITY(1,1)	, [Filename] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL	, [CreatedBy] INT NULL	, [CreatedDate] DATETIME NULL	, CONSTRAINT [PK_tblUserGuide] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO

