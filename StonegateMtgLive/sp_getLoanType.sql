USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getLoanType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getLoanType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [sp_getLoanType]  
(  
@sLoanID varchar(MAX)  
)  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 select LoanType from loan where LoanID =@sLoanID  
 END  
GO

