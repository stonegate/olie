USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_Managers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_Managers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Rakesh Mohanty>
-- Create date: <Create Date,12th May 2014>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Get_Managers] 
	@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT DISTINCT 
		UM.ManagerID, 
		U.ufirstname + ' ' + U.ulastname As UserName, 
		U.E3Userid,
		U.urole 
	FROM [dbo].[tblUserManagers] UM

	INNER JOIN [dbo].[tblUsers] U ON U.UserId = UM.ManagerID

	Where UM.Userid=@UserId
END



GO

