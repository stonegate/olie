USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdatemwlSubjectProperty_BuildingStatusinE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdatemwlSubjectProperty_BuildingStatusinE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Narayan.NR>
-- Create date: <05/11/2013>
-- Description:	<This will Update BuildingStatus InfoinE3>
-- =============================================
Create PROCEDURE [dbo].[sp_UpdatemwlSubjectProperty_BuildingStatusinE3]
(
	@strBuildingStatus varchar(10),
	@strLoanappId varchar(100)
)
AS
Begin Try
Begin Transaction    
declare @buildingStatus varchar(10)
if @strBuildingStatus='yes'
set @buildingStatus = 3
else if  @strBuildingStatus='No'
set @buildingStatus = 2

 Update mwlSubjectProperty set BuildingStatus = @strBuildingStatus where LoanApp_id = @strLoanappId
Commit 
End Try
Begin Catch
    Rollback  
    Declare @Msg nvarchar(max)
    Select @Msg=Error_Message();
    RaisError('Error Occured: %s', 20, 101,@Msg) With Log;
End Catch
GO

