USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAllBrokerDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAllBrokerDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [sp_GetAllBrokerDoc] 30

-- =============================================  
-- Author: Abhishek Rastogi  
-- ALTER Date : 10/06/2012  
-- Edited By : --  
-- Edited Date : --  
-- Description: for clsLoanApplication.cs BLL  
-- =============================================  
  
CREATE PROCEDURE [dbo].[sp_GetAllBrokerDoc]  
(  
@NoOfDays VARCHAR(MAX)  
)  
AS  
BEGIN  
declare @a varchar(max)  
  
set @a='select loanregid as loanregid,'''' as  loandocid,'''' as Condid,'''' as SchID, FileName as AllFileName,
createdDate as createdDate,'''' as Description,lreg.Userid as Userid,uemailid as Submittedby  ,
(uFirstName + ''  '' + ulastname) as OlieName from tblloanreg 
lreg  
left outer join tblusers ureg on ureg.userid=lreg.userid where urole=3 and FileName <> '''' 
and  (lreg.isDeletedFromDoclog is NULL or lreg.isDeletedFromDoclog=1) and 
createdDate   >= (getdate()-' + @NoOfDays + ')
 and createdDate <=getdate()   
union  
select '''' as loanregid,loandocid as  loandocid,'''' as Condid,'''' as SchID, DocName as AllFileName,
createdDate as createdDate,Comments as Description,ldoc.Userid as Userid,uemailid as Submittedby  ,uFirstName
 + ''  '' +  ulastname as OlieName from tblloandoc ldoc 
  
left outer join tblusers udoc on ldoc.userid=udoc.userid where urole=3 and  DocName <> '''' and 
 (ldoc.isDeletedFromDoclog is NULL or ldoc.isDeletedFromDoclog=1) and 
createdDate   >= (getdate()-' + @NoOfDays + ') and createdDate <=getdate()  
union  
select '''' as loanregid,'''' as  loandocid,ctID as Condid,'''' as SchID, FileName as AllFileName,dttime as 
createdDate,Cond_Text as Description,'''' as Userid,uemailid as Submittedby  ,uFirstName + ''  '' +
 ulastname as OlieName from tblCondText_History conh   
 left outer join tblusers uconh on uconh.userid=conh.userid where  FileName <> '''' and urole=3 and 
 (conh.IsActive is NULL or conh.IsActive=1) and dttime   >= (getdate()-' + @NoOfDays + ') 
and dttime <=getdate()  
union  
select  '''' as loanregid,'''' as  loandocid,'''' as Condid,Scheduleid as SchID, FeeSheet as AllFileName,
 SubmitedDate as createdDate,Comments as Description,'''' as Userid,uemailid as Submittedby  ,uFirstName +
 '' '' + ulastname as OlieName from tblScheduleClosing sch   
left outer join tblusers usch on sch.userid= usch.userid where  FeeSheet <> ''''
 and urole=3 and  (sch.IsActive is NULL or sch.IsActive=1) 
and SubmitedDate   >= (getdate()-' + @NoOfDays + ') 
and SubmitedDate <=getdate() order by  createdDate desc'  
--print @a
exec(@a)  
END  
  
  
GO

