USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Select_GetLoanLockStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Select_GetLoanLockStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC  [dbo].[SP_Select_GetLoanLockStatus](             
@LoanNo varchar(15)            
)                      
AS                      
BEGIN         
    
 select top 1 lockRecord.status,lockRecord.lockdatetime as lockdate from mwlloanapp as loanApp     
 left join mwlLockRecord as lockRecord on loanApp.id = lockRecord.loanApp_ID    
 where loannumber =@LoanNo  order by lockRecord.createdOnDate desc      
    
    
    
     
               
END 

GO

