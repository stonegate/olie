USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetGridData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetGridData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetGridData]

AS
BEGIN
select '0' as ctID, loanregid, isnull(tuser.MAID,0)AEId, isnull(loandocid,0)loandocid, case rloan.Assigned when '1' then 'true' else 'false' end as Assigned, case rloan.Completed when '1' then 'true' else 'false' end as Completed, tuser.ufirstName, 
tuser.uLastname,tuser.uemailid,tuser.mobile_ph, docname as LoanSubmissionFile   ,rloan.FileName as LoanRegistrationFile  ,  tuser.urole,rloan.userid,rloan.createddate as dttime, cuser.ufirstname + ' ' + cuser.ulastname as CompletedBy,rloan.TimeStamp,auser.ufirstname + ' ' + auser.ulastname as AssignTo
from tblLoanReg rloan  Left outer join tblloandoc rdoc  on rloan.loanregid = rdoc.uloanregid inner join tblusers tuser on rloan.userid=tuser.userid Left outer join tblusers cuser on rloan.CompletedBy=cuser.userid left outer join tblusers auser on rloan.AssignedBy=auser.userid where tuser.urole=2 and  rloan.FileName IS not NULL and rloan.FileName <> '' and  rloan.completed is null  order by dttime desc
END


GO

