USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateForms]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateForms]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_UpdateForms]
(

	@Description varchar(200) ,
	@FileName varchar(200) ,
	@isnews bit ,
	@CateID int ,
    @formID int
)
AS

BEGIN
	Update tblForms Set Description = @Description,FileName=@FileName,isnews=@isnews,CateID=@CateID
    where formID=@formID    

END


GO

