USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_PipeLine]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_PipeLine]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_Insert_PipeLine]
(
@Loan_No VARCHAR(MAX),
@Cond_Text VARCHAR(MAX),
@FileName VARCHAR(MAX),
@dttime VARCHAR(MAX),
@condrecid VARCHAR(MAX),
@ProlendImageDesc VARCHAR(MAX),
@Userid VARCHAR(MAX),
@SubmittedDate VARCHAR(MAX)
)
AS
BEGIN
Insert into tblCondText_History(Loan_No,Cond_Text,FileName,dttime,condrecid,ProlendImageDesc,Userid,SubmittedDate)
values
(@Loan_No,@Cond_Text,@FileName,@dttime,@condrecid,@ProlendImageDesc,@Userid,@SubmittedDate)

END

GO

