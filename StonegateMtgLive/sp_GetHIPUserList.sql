USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHIPUserList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHIPUserList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  Stonegate    
-- Create date: 8th June 2012    
-- Description: This will get user list to bind in the grid    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_GetHIPUserList]    
(    
@sortField varchar(500),    
@sortOrder varchar(100),    
@whereClause varchar(1000),    
@PageNo int,     
@PageSize int    
)     
AS    
BEGIN    
IF @sortField  = ''                                
  set @sortField = 'tblUsers.userid'    
    
SET @sortField = @sortField + ' ' + @sortOrder    
  
set @whereClause =  REPLACE(@whereClause,'[uurole]','tblroles.roles')                                      
    
Declare @sql nvarchar(4000)    
    
set @sql = ''                                
     set @sql = 'Select count(*) from ( '    
     set @sql =  @sql + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,    
tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid,     
tblUsers.urole, tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,     
tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender,     
tblUsers.carrierid, Carrier.Carrier,tblroles.roles as uurole ,tblUsers.signedupdate     
FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID LEFT OUTER JOIN tblroles on tblUsers.urole=tblroles.id      
where urole = 18 '    
    
IF @whereClause <> ''                                         
    BEGIN                                     
    set @sql = @sql  + ' AND ' + @whereClause                               
    END                        
    
set @sql = @sql + ' group by tblUsers.userid,upassword, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid,     
tblUsers.urole, tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,     
tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender,     
tblUsers.carrierid, Carrier.Carrier,tblroles.roles ,tblUsers.signedupdate'                                 
 set @sql = @sql + ' ) as t2 '     
    
print @sql    
exec sp_executesql @sql      
    
    
set @sql = ''                                
     set @sql = 'Select * from ( '     
set @sql =  @sql + 'SELECT             
        ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid,     
tblUsers.urole, tblUsers.isadmin, Case When tblUsers.isactive = 1 Then ''Active'' Else ''Inactive'' End as isactive, tblUsers.uidprolender, tblUsers.uparent,     
tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender,     
tblUsers.carrierid, Carrier.Carrier,tblroles.roles as uurole ,tblUsers.signedupdate     
FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID LEFT OUTER JOIN tblroles on tblUsers.urole=tblroles.id      
where urole = 18 '    
    
IF @whereClause <> ''                                         
    BEGIN                                     
    set @sql = @sql  + ' AND ' + @whereClause                               
    END                        
    
set @sql = @sql + ' group by tblUsers.userid,upassword, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid,     
tblUsers.urole, tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,     
tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender,     
tblUsers.carrierid, Carrier.Carrier,tblroles.roles ,tblUsers.signedupdate'                                 
set @sql = @sql + ' ) as t2 '    
    
set @sql = @sql + ' Where Row between (' + CAST( @PageNo AS NVARCHAR)  + ' - 1) * ' + Cast(@PageSize as nvarchar) + ' + 1 and ' + cast(@PageNo as nvarchar) + ' * ' + cast(@PageSize as nvarchar)    
    
print @sql                              
exec sp_executesql @sql    
    
END 


GO

