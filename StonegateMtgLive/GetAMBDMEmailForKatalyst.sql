USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAMBDMEmailForKatalyst]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetAMBDMEmailForKatalyst]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Shwetal,,Shah>
-- Create date: <03 March 2013>
-- Description:	<This Stored procedure used for Get BDM information from Website DB>
---Modify By: <Vipul, Thacker>
-- Modify date: <04 April 2013>
-- =============================================
CREATE PROCEDURE [dbo].[GetAMBDMEmailForKatalyst]
(
	@UserID int = 0
)
AS
BEGIN
SELECT BDM.uemailid AS BDMEmail,tblAM.uemailid AS AMEmailID, tblBroker.MAID as  AEID FROM tblusers tblBroker WITH(NOLOCK)
	LEFT JOIN dbo.tblUsers  BDM WITH(NOLOCK) ON tblBroker.MAID = BDM.userid
	LEFT JOIN dbo.tblUsers  tblAM WITH(NOLOCK) ON tblAM.region= BDM.Region AND tblAM.urole = 8
	Where tblBroker.userid = @UserID
	
END


GO

