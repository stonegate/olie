USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RetailOperationPipeline_Processor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RetailOperationPipeline_Processor]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================    
-- Author: Krunal Patel   
-- Create date: 04 July 2013            
-- Description: <Select Processor Name and loanappid>    
-- =============================================    

CREATE PROCEDURE [dbo].[sp_RetailOperationPipeline_Processor]     
AS    
BEGIN  

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error	
	
	--Bind Loan Processor User in Retail Pipeline Report Filter
	Select 
		ufirstname+' '+ ulastname as 'FullName',E3Userid as ID from  tblusers where urole=14 and isactive=1

END



GO

