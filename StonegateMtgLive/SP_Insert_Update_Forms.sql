USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Insert_Update_Forms]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Insert_Update_Forms]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel
-- Create Date : 10/04/2012  
-- Description: Procedure for  
-- =============================================  
     
CREATE PROC  [dbo].[SP_Insert_Update_Forms](       
@FormID int,         
@StringMode varchar(10),  
@Description VARCHAR(200),          
@FileName VARCHAR(200),                
@dttime  datetime,                  
@isnews  bit,                  
@CatID int        
)                
AS                
BEGIN            
 IF(@StringMode ='insert')       
          
	INSERT INTO tblForms(Description,[FileName],dttime,isnews,CateID)        
	VALUES (@Description,@FileName,@dttime,@isnews,@CatID)        
       
 ELSE IF(@StringMode ='update')       
       
	update tblForms      
	set Description=@Description,[FileName]= @FileName,    
	dttime =@dttime ,isnews=@isnews,   CateID=@CatID  
	where FormID=@FormID      
     
         
END 

GO

