USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblOffices]') AND type in (N'U'))
DROP TABLE [dbo].[tblOffices]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblOffices](	  [recordid] INT NOT NULL IDENTITY(1,1)	, [lt_office_id] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [prouid] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [userid] INT NULL	, [eTeamMemberid] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [E3TeamMemberid] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblOffices] PRIMARY KEY ([recordid] ASC))USE [HomeLendingExperts]
GO

