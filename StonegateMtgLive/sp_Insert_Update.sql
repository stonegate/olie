USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_Update]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_Insert_Update]
(
@record_id VARCHAR(MAX)
)
AS
BEGIN

update condits  set cond_recvd_date = Getdate() where record_id = @record_id
 
END


GO

