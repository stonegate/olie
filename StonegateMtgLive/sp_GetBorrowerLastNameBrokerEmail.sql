USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBorrowerLastNameBrokerEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBorrowerLastNameBrokerEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetBorrowerLastNameBrokerEmail]
(
@loan_no VARCHAR(MAX)
)
AS
BEGIN

select cb.borr_last,user_email from loandat LD inner join cobo Cb on ld.record_id = cb.parent_id left outer join users on ld.lt_usr_underwriter = users.record_id  where ld.loan_no = @loan_no
END


GO

