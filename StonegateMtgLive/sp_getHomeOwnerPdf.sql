USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getHomeOwnerPdf]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getHomeOwnerPdf]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_getHomeOwnerPdf]
(
@sLoanID varchar(MAX)
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select top 1 * from tblFormsHomeowner where LoanType= @sLoanID
	order by formid desc
	END
--------------------------------------------------------------------------------------------------------------





GO

