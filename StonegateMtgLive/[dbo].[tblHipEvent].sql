USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblHipEvent]') AND type in (N'U'))
DROP TABLE [dbo].[tblHipEvent]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblHipEvent](	  [id] INT NOT NULL IDENTITY(1,1)	, [HipEventDate] DATETIME NULL	, [HipEventName] VARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FirstName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LastName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [StreetAddr] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [City] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [State] VARCHAR(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ZipCode] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Email] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Company] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [attended] BIT NULL	, [location] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CurrentPartner] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedDate] DATETIME NULL)USE [HomeLendingExperts]
GO

