USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SelectGetPackageId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SelectGetPackageId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_SelectGetPackageId]
	
AS
BEGIN

	SET NOCOUNT ON;

 Select Id,PackageId from tblPackageTypeId
END
GO

