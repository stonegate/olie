USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetFalloutReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetFalloutReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   
CREATE PROCEDURE [dbo].[sp_GetFalloutReport]            
(            
@RoleID int,           
@strID varchar(2000),            
@BrokerId char(2),          
@TeamOPuids varchar(30),           
@Year char(4),            
@Channel varchar(15),          
@Status varchar(1000),          
@Status1 varchar(1000),          
@Status2 varchar(1000)          
)            
AS            
BEGIN            
            
Declare @SQL nvarchar(max)            
Set @SQL =''            
Set @SQL ='SELECT '''+ @Year +''' AS ARYear,          
   month(LockRecord.LockDateTime) AS LockMonth, convert(decimal(12,0),round(sum(isnull(loanData.AdjustedNoteAmt,0)),0))          
   AS TotalLockedVolume, convert(decimal(12,0),round(sum(isnull(Closed.AdjustedNoteAmt, isnull(Closed.AppraisedValue,0))),0))          
   AS ClosedVolume,convert(decimal(12,0),round(sum(isnull(NotClosed.AdjustedNoteAmt, isnull(NotClosed.AdjustedNoteAmt,0))),0))           
   AS NotClosedVolume, convert(decimal(12,0),round(SUM(isnull(ActiveStatusLoandat.AdjustedNoteAmt,           
   isnull(ActiveStatusLoandat.AdjustedNoteAmt,0))),0)) ActiveStatusVolume,           
   case when ISNULL(SUM(isnull(Closed.AdjustedNoteAmt, Closed.AdjustedNoteAmt)),0) + ISNULL(SUM(isnull(NotClosed.AdjustedNoteAmt,          
   NotClosed.AdjustedNoteAmt)),0) > 0 THEN           
   convert(decimal(12,0),ROUND((ISNULL(sum(isnull(Closed.AdjustedNoteAmt, Closed.AdjustedNoteAmt)),0) * 100)/ (ISNULL(sum(isnull(Closed.AdjustedNoteAmt, Closed.AdjustedNoteAmt)),0) + ISNULL(sum(isnull(NotClosed.AdjustedNoteAmt, NotClosed.AdjustedNoteAmt)) ,0)),0))           
   ELSE 0 END AS ''% Closed''  FROM mwlLoanApp  AS MainTable           
   Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = MainTable.id           
   INNER JOIN mwlLockRecord LockRecord ON  LockRecord.LoanApp_ID = MainTable.id  and  LockRecord.LockType=''LOCK''           
   LEFT JOIN  mwlloandata Closed ON Closed.ObjOwner_id = MainTable.ID           
   and  MainTable.CurrentStatus IN (select * from dbo.SplitString('''+ @Status +''','',''))           
   LEFT JOIN  mwlloandata NotClosed ON NotClosed.ObjOwner_id = MainTable.ID              
   AND (MainTable.CurrentStatus IN (select * from dbo.SplitString('''+ @Status1 +''','','')) OR          
   (LockRecord.LockExpirationDate < Getdate() AND MainTable.CurrentStatus IN (select * from dbo.SplitString('''+ @Status2 +''','',''))))           
   LEFT JOIN  mwlloandata ActiveStatusLoandat ON ActiveStatusLoandat.ObjOwner_id = MainTable.ID AND          
   LockRecord.LockExpirationDate > getdate() AND MainTable.CurrentStatus  IN ('''','''+ @Status2 +''')            
   Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = MainTable.id and Broker.InstitutionType = ''BROKER'' and           
   Broker.objownerName=''Broker''           
   Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = MainTable.id and Corres.InstitutionType = ''CORRESPOND'' and           
   Corres.objownerName=''Correspondent''            
   WHERE YEAR(LockRecord.LockDateTime) = '''+ @Year +''''          
          
                if (@Channel = 'broker')          
                           
                    Set @SQL = @SQL + '  and MainTable.ChannelType=''BROKER'''           
                           
                if (@Channel = 'retail')          
                           
                     Set @SQL = @SQL + ' and MainTable.ChannelType=''RETAIL'''          
                           
                if (@Channel = 'correspond')          
                           
                     Set @SQL = @SQL + ' and MainTable.ChannelType=''CORRESPOND'''           
                           
          
                if (@RoleID = 1)          
                           
                     Set @SQL = @SQL + 'and MainTable.ChannelType like ''%RETAIL%'''            
                           
          
                if (@TeamOPuids != '')          
                 BEGIN          
                    Set @SQL = @SQL + '  and Originator_id in (''' + @TeamOPuids + ''')'           
                 END          
                else          
               BEGIN          
                    if (@RoleID != 0 and @RoleID != 1)          
                    BEGIN          
                        if (len(@strID) = 0)            
                        BEGIN          
                                       
                            if (@RoleID  = 2)          
                                       
                                   Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+ @strID +''','','')))'          
                             
                                       
                        END          
                        else          
                        BEGIN          
                       if (@RoleID =  2)          
                                       
                                   Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+ @strID +''','','')))'          
                                       
            
                        END          
                    END          
                END          
          
          
                if (@BrokerId != 0 AND @BrokerId != '')          
     BEGIN                    
                       Set @SQL = @SQL + ' and (Broker.CompanyEmail = '''+ @BrokerId +''' or Corres.CompanyEmail = '''+ @BrokerId +''') '          
     END          
                   Set @SQL = @SQL + ' group by month(LockRecord.LockDateTime)'          
                if (@BrokerId != 0 and @BrokerId != '')          
                 BEGIN          
                   Set @SQL = @SQL + ' ,Broker.CompanyEmail,Corres.CompanyEmail '          
                 END          
                   Set @SQL = @SQL + '  order by month(LockRecord.LockDateTime)'          
 Print @SQL            
exec sp_executesql @SQL            
END   
GO

