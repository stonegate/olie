USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckDuplicateLoanType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckDuplicateLoanType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_CheckDuplicateLoanType]
(
@LoanType varchar(max)
)
AS
BEGIN

	SET NOCOUNT ON;

select LoanType from tblLoanType where LoanType=@LoanType

END

--------------------------------------------------------------------------------------------------------------


/****** Object:  StoredProcedure [dbo].[sp_InsertCreateLoanType]    Script Date: 04/12/2013 17:43:39 ******/
SET ANSI_NULLS ON

GO

