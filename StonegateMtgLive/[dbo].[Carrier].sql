USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Carrier]') AND type in (N'U'))
DROP TABLE [dbo].[Carrier]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Carrier](	  [ID] INT NOT NULL IDENTITY(1,1)	, [Carrier] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [OrderID] INT NULL	, [CarrierEMailSMS] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO

