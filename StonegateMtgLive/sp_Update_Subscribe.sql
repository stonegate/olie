USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_Subscribe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_Subscribe]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Update_Subscribe]
	-- Add the parameters for the stored procedure here
	@Email nvarchar(100)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Subscribe 
set IsActive = 0, UpdatedDate=GetDate()Where EMail = @Email 
               
              
                 
END


GO

