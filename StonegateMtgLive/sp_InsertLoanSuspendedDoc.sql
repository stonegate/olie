USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLoanSuspendedDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLoanSuspendedDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertLoanSuspendedDoc]
(
@LoanNo VARCHAR(MAX),
@FeeSheet VARCHAR(MAX),
@userid VARCHAR(MAX),
@Comments VARCHAR(MAX)
)
AS
BEGIN
insert into tblScheduleClosing (LoanNo, SchDate, FeeSheet,userid,SubmitedDate, Comments,IsFromLoanSuspendedDoc) values 
(@LoanNo, '',@FeeSheet, @userid ,getdate(),@Comments,1)

END

GO

