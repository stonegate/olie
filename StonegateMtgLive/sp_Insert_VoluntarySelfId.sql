USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_VoluntarySelfId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_VoluntarySelfId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Insert_VoluntarySelfId] 
(
@stringmode varchar(50),
@AppId int,
@PosApplyingFor varchar(100),
@HowYouReferred varchar(100),
@Gender varchar(50),
@HispanicOLatino bit,
@White bit,
@BlackAfrAmc bit,
@Hawiian bit,  
@Asian bit,
@AmericanIndian bit,
@TwoOrMoreRaces bit,
@DisabledVeteran bit,
@VietnamVeteran bit,
@OtherVeteran bit,
@NewlyVeteran bit,
@Veteran bit,
@NotApplicable bit,
@OtherStatus bit
)
AS
BEGIN
	 INSERT INTO tblVoluntarySelfId(AppId,PosApplyingFor,HowYouReferred,Gender,HispanicOLatino,White,BlackAfrAmc,Hawiian,Asian,AmericanIndian,TwoOrMoreRaces,DisabledVeteran,VietnamVeteran,OtherVeteran,NewlyVeteran,Veteran,NotApplicable,OtherStatus)
     VALUES (@AppId,@PosApplyingFor,@HowYouReferred,@Gender,@HispanicOLatino,@White,@BlackAfrAmc,@Hawiian,@Asian,@AmericanIndian,@TwoOrMoreRaces,@DisabledVeteran,@VietnamVeteran,@OtherVeteran,@NewlyVeteran,@Veteran,@NotApplicable,@OtherStatus)  
END









GO

