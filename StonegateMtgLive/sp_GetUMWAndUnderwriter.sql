USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUMWAndUnderwriter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUMWAndUnderwriter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetUMWAndUnderwriter]
AS
BEGIN
SELECT E3Userid,uidprolender,rtrim(ufirstname) + ' ' + ulastname as underwriter FROM tblUsers WHERE urole=10  or urole=11 AND isactive=1 order by rtrim(ufirstname) + ' ' + ulastname
END


GO

