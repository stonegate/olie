USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DisclosureAnalysisList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DisclosureAnalysisList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  Procedure [dbo].[sp_DisclosureAnalysisList]
AS
BEGIN
	select ufirstname + space(1) + ulastname as FullName,userid,coalesce(e3userid,'') e3userid from tblUsers where urole =21
END


GO

