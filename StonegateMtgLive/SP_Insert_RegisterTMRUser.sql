USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Insert_RegisterTMRUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Insert_RegisterTMRUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Created By: Chandresh Patel  
-- Create Date : 10/04/2012    
-- Description: Procedure for Insert Register TMR User   
-- =============================================       
CREATE PROC [dbo].[SP_Insert_RegisterTMRUser](         
@userid int,    
@StringMode char(10),    
@UFirstName VARCHAR(50),                  
@ULastName  VARCHAR(50),                    
@UEmailId  VARCHAR(200),     
@siteurl varchar(50)=NULL,                                
@urole bit,             
@IsAdmin bit,               
@IsActive bit,                
@Mobile_Ph nvarchar(50),     
@upassword varchar(200),        
@carrierid int,    
@RcvOnCell bit,    
@RcvOnEmail bit,    
@TMRAccActivation bit,    
@RcvExpire30 bit,    
@TMRRole nvarchar(50),    
@TMRLoanType nvarchar(50),    
@TMRTimeFrame nvarchar(50),    
@PropState varchar(50)=null               
)                  
AS                  
BEGIN              
   IF(@StringMode ='insert')         
  BEGIN             
  insert into tblusers(ufirstname,ulastname,uemailid,urole,isadmin,isactive,mobile_ph,    
  upassword,carrierid,RcvOnCell,RcvOnEmail,TMRAccActivation,RcvExpire30,TMRRole,TMRLoanType,    
  TMRTimeFrame,PropState)      
  VALUES (@UFirstName,@ULastName,@UEmailId,@urole,@isadmin,@isactive,    
  @mobile_ph,@upassword,@carrierid,    
  @RcvOnCell,@RcvOnEmail,@TMRAccActivation,@RcvExpire30,@TMRRole,@TMRLoanType,    
  @TMRTimeFrame,@PropState)          
  END    
 ELSE IF(@StringMode ='update')         
  BEGIN       
   update tblusers  set ufirstname=@UFirstName,ulastname=@ulastname,uemailid=@uemailid,urole=@urole,    
   isadmin=@isadmin,isactive=@isactive,mobile_ph=@mobile_ph,    
   upassword=@upassword,carrierid=@carrierid,RcvOnCell=@RcvOnCell,RcvOnEmail=@RcvOnEmail,    
   TMRAccActivation=@TMRAccActivation,RcvExpire30=@RcvExpire30,TMRRole=@TMRRole,    
   TMRLoanType=@TMRLoanType,    
   TMRTimeFrame=@TMRTimeFrame,PropState=@PropState where userid=@userid    
  END        
     
END   
GO

