USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Authenticate_tblUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Authenticate_tblUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Authenticate_tblUser]
	-- Add the parameters for the stored procedure here
	@LoginName varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT *  FROM tblUser WHERE LoginName=@LoginName 
    -- Insert statements for procedure here
	
END


GO

