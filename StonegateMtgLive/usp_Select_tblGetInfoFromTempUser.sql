USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tblGetInfoFromTempUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tblGetInfoFromTempUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Select_tblGetInfoFromTempUser]
(
@strPsCellNumber varchar(Max)
)
as
begin

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT temp.refuserid,temp.tempuserid,temp.Tempid,temp.temppswd,temp.createdDate,temp.IsActive  
FROM tblTempUser temp left join tblusers users on  temp.refuserid= users.userid
WHERE (tempuserid = @strPsCellNumber)  and temp.IsActive=1 and users.IsActive=1
end


GO

