USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblVoluntarySelfId]') AND type in (N'U'))
DROP TABLE [dbo].[tblVoluntarySelfId]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblVoluntarySelfId](	  [VSIID] INT NOT NULL IDENTITY(1,1)	, [AppId] INT NULL	, [PosApplyingFor] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [HowYouReferred] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Gender] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [HispanicOLatino] BIT NULL	, [White] BIT NULL	, [BlackAfrAmc] BIT NULL	, [Hawiian] BIT NULL	, [Asian] BIT NULL	, [AmericanIndian] BIT NULL	, [TwoOrMoreRaces] BIT NULL	, [DisabledVeteran] BIT NULL	, [VietnamVeteran] BIT NULL	, [OtherVeteran] BIT NULL	, [NewlyVeteran] BIT NULL	, [Veteran] BIT NULL	, [NotApplicable] BIT NULL	, [OtherStatus] BIT NULL)USE [HomeLendingExperts]
GO

