USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SetActiveDeactiveNew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SetActiveDeactiveNew]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_SetActiveDeactiveNew]  
@IsActive bit,  
@id varchar(50)  
AS  
BEGIN
--IF exists (SELECT * FROM tblNewsAndAnnouncement where ID=@ID AND cc_id='0')
-- begin
--	--print 'a'
--	 update tpo_tblNewsAndAnnouncement SET IsActive=@IsActive where cc_id=@id  
-- end
--else
-- begin
-- update tpo_tblNewsAndAnnouncement SET IsActive=@IsActive where ID = (SELECT cc_id FROM tblNewsAndAnnouncement where ID=@ID) 
--	--print 'b'
-- end

UPDATE tpo_tblNewsAndAnnouncement
SET IsActive = @IsActive
WHERE id = @id
   

END  


GO

