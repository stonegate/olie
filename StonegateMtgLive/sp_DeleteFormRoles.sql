USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteFormRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteFormRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_DeleteFormRoles]
(
@formid int
)
AS

BEGIN
	Delete from tblformRoles where formid =@formid
END


GO

