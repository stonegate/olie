USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_NewRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_NewRate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Insert_NewRate] 
(
@stringmode varchar(50),
@NewRateID int,      
@strLoanType varchar(50),
@LoanProgram varchar(250),
@GoalOfLoan varchar(50),   
@IPoints int,    
@DecRate decimal(18,3),    
@DecAPR decimal(18,3),
@DecClosingCost decimal(18,2),
@CreatedDate datetime,   
@ModifiedDate datetime   

)
AS
BEGIN
if(@stringmode = 'insert')
insert into tblnewrates(strLoanType,LoanProgram,GoalOfLoan,IPoints,DecRate,DecAPR,DecClosingCost,CreatedDate,ModifiedDate)
VALUES (@strLoanType,@LoanProgram,@GoalOfLoan,@IPoints,@DecRate,@DecAPR,@DecClosingCost,@CreatedDate,@ModifiedDate)  
else if(@stringmode = 'update')
update tblnewrates
set strloantype = @StrLoanType,LoanProgram=@LoanProgram,GoalOfLoan=@GoalOfLoan,IPoints = @IPoints,DecRate =  @DecRate,DecAPR = @DecAPR,DecClosingCost = @DecClosingCost,ModifiedDate = getdate()
where NewRateID = @NewRateID
else if(@stringmode = 'delete')
delete from tblnewrates where NewRateID = @NewRateID
END











GO

