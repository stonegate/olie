USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_tblTempUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_tblTempUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Gourav  
-- Create date: 10/03/2012  
-- Description: Procedure for clsUser.cs bll    
-- =============================================  
CREATE PROCEDURE [dbo].[sp_Update_tblTempUser]
(  
  
@refuserid varchar(400)  
)  
as  
begin  
UPDATE tblTempUser SET IsActive=0 where refuserid=@refuserid  
end  


GO

