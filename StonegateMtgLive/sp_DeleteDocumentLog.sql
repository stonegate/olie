USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteDocumentLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteDocumentLog]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_DeleteDocumentLog]
(
@loanregid varchar(max),
@ctid varchar(max),
@Scheduleid varchar(max),
@loandocid varchar(max)
)
AS
BEGIN
update tblLoanReg set IsDeletedFromDocLog = 0 where loanregid = @loanregid ; update tblCondText_History set IsActive= 0 where ctid = @ctid ; update tblScheduleClosing set IsActive= 0 where Scheduleid = @Scheduleid ; update tblloandoc set IsDeletedFromDocLog = 0 where loandocid=@loandocid
END



GO

