USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUMWAndUnderwriterE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUMWAndUnderwriterE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetUMWAndUnderwriterE3]
AS
BEGIN
SELECT E3Userid,rtrim(ufirstname) + ' ' + ulastname as underwriter FROM tblUsers WHERE urole=10  or urole=11 AND isactive=1 order by rtrim(ufirstname) + ' ' + ulastname
END


GO

