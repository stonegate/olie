USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PNPaymentPostedReminderold]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PNPaymentPostedReminderold]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Harsh Pandit
-- Create date: 
-- Description:	
-- =============================================
Create PROCEDURE [dbo].[sp_PNPaymentPostedReminderold] 
	--@LoanNo varchar(50)
AS
BEGIN
	SET NOCOUNT ON;
		select record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,  loanno, 
		(
			select  top 1 TransactionAmt  
			from [WINSERVER].[service].dbo.history 
			join [WINSERVER].[service].dbo.TransactionCode on TransactionCode.code = history.TransactionCode 
			where loanid = loanno 
			and (TransactionCode = 211 or TransactionCode = 210) 
			order by loanid,TransactionDate DESC
		) as TransactionAmt,  
		PaymentPostedReminderdate,IsSMSPaymentPostedAlert,IsEmailPaymentPostedAlert,IsPaymentPostedReminderAlert,PaymentPostedReminderDay,  
		convert(varchar,(
			select  top 1 TransactionDate  
			from [WINSERVER].[service].dbo.history 
			join [WINSERVER].[service].dbo.TransactionCode on TransactionCode.code = history.TransactionCode 
			where loanid = loanno 
			and (TransactionCode = 211 or TransactionCode = 210) 
			order by TransactionDate DESC
		) , 1) as TransactionDate,
		mobile_ph, carrierid,
	(select carrieremailsms from carrier where id = carrierid) as smsgateway 
		from tblServiceInfo as serviceInfo  
		join [WINSERVER].[service].dbo.loan on loan.loanid = serviceinfo.loanno  
		join tblUsers as users on users.userid = serviceinfo.userid  
		where IsPaymentPostedReminderAlert = 1   
		and (month(PaymentPostedReminderdate) <> month(getdate()) or PaymentPostedReminderdate is null)
		AND DATEDIFF(DAY,(select  top 1 TransactionDate  
			from [WINSERVER].[service].dbo.history 
			join [WINSERVER].[service].dbo.TransactionCode on TransactionCode.code = history.TransactionCode 
			where loanid = loanno 
			and (TransactionCode = 211 or TransactionCode = 210) 
			order by loanid,TransactionDate DESC),GETDATE()) <= PaymentPostedReminderDay
END


GO

