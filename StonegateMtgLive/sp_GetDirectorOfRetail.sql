USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDirectorOfRetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDirectorOfRetail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetDirectorOfRetail]
(
	@ManagerId int
	
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SELECT t1.userid
		,ufirstname + space(1) + ulastname fullname
	FROM tblusers t1
	INNER JOIN tblUserManagers t2 ON t1.userid = t2.userid
		AND t1.isactive = 1
		AND t2.ManagerId = @ManagerId
		WHERE t1.urole = 1
	
END


GO

