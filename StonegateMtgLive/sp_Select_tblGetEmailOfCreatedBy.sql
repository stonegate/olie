USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetEmailOfCreatedBy]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetEmailOfCreatedBy]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE PROCEDURE [dbo].[sp_Select_tblGetEmailOfCreatedBy]
(
@iCreatedBy varchar(Max)
)
as
begin

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select uemailid from tblusers where userid=@iCreatedBy
end


GO

