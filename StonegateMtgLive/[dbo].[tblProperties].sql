USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblProperties]') AND type in (N'U'))
DROP TABLE [dbo].[tblProperties]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblProperties](	  [PropID] INT NOT NULL IDENTITY(1,1)	, [RealtorID] INT NULL	, [PropStatus] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ListingTYpe] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [BedRooms] INT NULL	, [Baths] DECIMAL(18,1) NULL	, [Price] DECIMAL(18,2) NULL	, [Street] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [State] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [City] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Zipcode] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [YearBuilt] INT NULL	, [IsActive] BIT NULL DEFAULT((1))	, [PropDesc] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [lat] DECIMAL(18,6) NULL	, [lng] DECIMAL(18,6) NULL	, [Area] INT NULL	, [CreatedDate] DATETIME NULL DEFAULT(getdate())	, [Amount] DECIMAL(18,0) NULL	, [ImprovementFile] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DaysListed] INT NULL	, CONSTRAINT [PK_tblProperties] PRIMARY KEY ([PropID] ASC))USE [HomeLendingExperts]
GO

