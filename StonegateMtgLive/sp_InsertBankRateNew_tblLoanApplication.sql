USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertBankRateNew_tblLoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertBankRateNew_tblLoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
-- Author      : Joyal F. Macwan
-- Create date : 10-05-2013 ( 4 : 00 P.M.)
-- Description : insert data for tblLoanApplication table
-- =====================================================

CREATE Procedure [dbo].[sp_InsertBankRateNew_tblLoanApplication]  
(   
 @HometypeID int=null,  
 @LoanAmount money=null,  
 @PropertyTypeID int=null,   
 @AppFName nvarchar(100)=null,  
 @AppLName nvarchar(100)=null,  
 @AppPrimaryPhone nvarchar(100),  
 @AppEmail nvarchar(100)=null,  
 @IsWorkingWithRealtor bit  
)  
  
AS  
  
SET NOCOUNT OFF  
   
  begin  
   insert into tblLoanApplication  
   (  
    HometypeID,LoanAmount,PropertyTypeID,AppFName,AppLName,AppPrimaryPhone,AppEmail,IsWorkingWithRealtor  
   )  
   values  
   (  
    @HometypeID,@LoanAmount,@PropertyTypeID,@AppFName,@AppLName,@AppPrimaryPhone,@AppEmail,@IsWorkingWithRealtor  
   )     
  end  
  
SET NOCOUNT ON
GO

