USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetConditionDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetConditionDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetConditionDate]
(
@loan_no VARCHAR(MAX),
@condrecid VARCHAR(MAX)
)
AS
BEGIN
select MAX(convert(varchar(35),dttime,101)) as dttime from tblCondText_History  
where rtrim(loan_no)=@loan_no and condrecid =@condrecid
END




GO

