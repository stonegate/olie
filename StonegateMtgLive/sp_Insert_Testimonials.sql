USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_Testimonials]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_Testimonials]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Insert_Testimonials] 
(
@stringmode varchar(50),
@ID int,      
@IsPublish bit,   
@Testimionial ntext,    
@IsActive bit    
--@CreatedDate datetime
)
AS
BEGIN
if(@stringmode = 'insert')

 INSERT INTO tblPublicTestimonial (IsPublish,Testimionial,IsActive)      
 VALUES      
  (@IsPublish,@Testimionial,@IsActive)  

else if(@stringmode = 'update')

update tblPublicTestimonial set IsPublish =@IsPublish,Testimionial=@Testimionial where id = @id 
 

else if(@stringmode = 'delete')

delete from tblPublicTestimonial where id = @id


END



GO

