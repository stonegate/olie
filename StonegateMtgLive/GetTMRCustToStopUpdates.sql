USE [HomeLendingExperts]
								GO
								SET ANSI_NULLS ON
								GO
								SET QUOTED_IDENTIFIER ON
								GO-- =============================================
-- Author:		Harsh Pandit
-- Create date: 19 Nov 2010
-- Description:	to stop updates to TMR users who has locked their loans
-- =============================================
CREATE FUNCTION [dbo].[GetTMRCustToStopUpdates] 
(
	 
)
RETURNS @EmployeeList table (userid int)
AS
BEGIN
	
	insert into  @EmployeeList select users.userid
	from [INTERLINQE3].[dbo].mwlloanapp as loanapp
	join [INTERLINQE3].[dbo].mwlborrower as borr on loanapp.id = borr.loanapp_id and borr.SequenceNum = 1 
	join tblusers as users on users.uemailid = emailaddress and urole = 9 and TMRRole = 'Consumer' and TMRAccActivation = 1
	where LockStatus='Locked'

	return

END
GO

