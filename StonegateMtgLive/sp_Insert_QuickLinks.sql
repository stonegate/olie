USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_QuickLinks]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_QuickLinks]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Insert_QuickLinks] 
(
@stringmode varchar(50),
@ID int,      
@Linkname varchar(100),   
@Description varchar(1000),  
@Roles varchar(100)
)
AS
BEGIN
if(@stringmode = 'update')
update tblQuickLinks set Linkname=@Linkname,Description=@Description,Roles=@Roles where ID = @ID
END




GO

