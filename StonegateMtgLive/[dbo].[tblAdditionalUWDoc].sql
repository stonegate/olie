USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAdditionalUWDoc]') AND type in (N'U'))
DROP TABLE [dbo].[tblAdditionalUWDoc]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAdditionalUWDoc](	  [ctID] INT NOT NULL IDENTITY(1,1)	, [loan_no] CHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [cond_text] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [FileName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [dttime] DATETIME NULL	, [condrecid] INT NULL	, [isCommited] BIT NULL	, [ProlendImageDesc] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Userid] INT NULL	, [IsActive] BIT NULL	, CONSTRAINT [PK_tblAdditionalUWDoc] PRIMARY KEY ([ctID] ASC))USE [HomeLendingExperts]
GO

