USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchDataForState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchDataForState]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchDataForState]
	-- Add the parameters for the stored procedure here

(
@state nvarchar(10),
@urole int
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

select * from tblusers where urole=@urole and isActive=1 and [state] in (@state) 


		
END


GO

