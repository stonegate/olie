USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchServiceBannerTest]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchServiceBannerTest]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Created By: <Amit Kumar>
-- CREATED date: <03/13/2013>
--Description: If Status=0 then Message is 'Expired'
--If Status=1 then Message is 'Active'
--If Status=2 then Message is 'Pending'
--If Status=3 then Message is 'Archive'
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchServiceBannerTest]
(
@Condition int,
@WhereCondition int
)
-- Add the parameters for the stored procedure here
AS
BEGIN


-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
--Start(to retrieve the all active and pending message)------------------------
if(@Condition=0)
begin

select ServiceBannerID,Message,PublishDate,ExpireDate, Status= case Status when '1' then 'Active' when '2' then 'Pending' END from tblServiceBanner where Status in(1,2) order by ExpireDate desc
end
--End(to retrieve the all active and pending message)------------------------

--Start(to retrieve the all active message)-------------------------------------
if(@Condition=1)
begin
select *from tblServiceBanner where Status=1
end
--End(to retrieve the all active message)-------------------------------------

--Start(to retrieve the all active and pending message)--------------------
if(@Condition=2)
begin

select *from tblServiceBanner where Status in(1,2)
end
--END(to retrieve the all active and pending message)--------------------------

--Start(to retrieve the all archive message)-----------------------------------
if(@Condition=3)
begin
select *from tblServiceBanner where Status=3 order by ExpireDate desc
end
--End(to retrieve the all archive message)-----------------------------------

--Start(to retrieve the all active and pending message for update time)------

if(@Condition=4)
begin

select *from tblServiceBanner where ServiceBannerID=@WhereCondition and Status in(1,2)
end
--End(to retrieve the all active and pending message for update time)--------

end


GO

