USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_RealECValidating_Loan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_RealECValidating_Loan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_RealECValidating_Loan]
(
@LoanNumber varchar(30) ,
@Role int ,
@UserId bigint  
)
AS
BEGIN

 CREATE TABLE #tempLoan
 (
 DB VARCHAR(100),
 loan_no VARCHAR(100),
 CurrentStatus VARCHAR(100),
 LoanStatus VARCHAR(100),
 BorrowerLastName VARCHAR(100),
 YNValue VARCHAR(100),
 DecisionStatus VARCHAR(100),
 UnderwriterType VARCHAR(100),
 loan_amt VARCHAR(100),
 SubmittedDate VARCHAR(100),
 BorrowerFirstName VARCHAR(100)
 )

Declare @uRetailProcessorName  varchar(100)
Declare @Originator_id  varchar(100)
Declare @custfielddef_id  varchar(100)
Set @custfielddef_id = (select ID from [mwsCustFieldDef] where ltrim(rtrim(CustomFieldName)) = 'Retail Processor')
set @uRetailProcessorName = (select ltrim(rtrim(ufirstname)) + ' '+ ltrim(rtrim(ulastname)) from tblUsers where userid = @UserId)
Set @Originator_id = (Select top 1 E3Userid from tblUsers where  userid = @UserId)


-------------------------Loan Processor-----------------------------------------
IF(@Role = 14)
BegiN
	INSERT INTO #tempLoan
	Select distinct 'E3' DB,
	M.LoanNumber loan_no ,
	M.CurrentStatus,
	left(M.CurrentStatus,2)  LoanStatus,
	M.lastname BorrowerLastName,
	1 YNValue,
	M.DecisionStatus,
	'NA' UnderwriterType,
	loanData.AdjustedNoteAmt,
	appStat.StatusDateTime AS SubmittedDate,
	M.firstname BorrowerFirstName
 	from 
	 (Select M.*,borr.* from mwlloanapp M
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id,lastname,firstname from mwlBorrower group by loanapp_id,lastname,firstname) as borr on borr.loanapp_id = M.id
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id from mwlBorrower group by loanapp_id) as borrS on borr.loanapp_id = borrs.loanapp_id and  borrs.SequenceNum = borr.SequenceNum

	where (ltrim(rtrim(LoanNumber)) Like @LoanNumber + '%' or borr.LastName LIKE @LoanNumber +'%') and ChannelType LIKE 'Ret%'  

	) M
	INNER JOIN 
	(
	 select ltrim(rtrim(StringValue)) RetailProcessor,loanapp_id from mwlcustomfield
	 where custfielddef_id = @custfielddef_id  and (ltrim(rtrim(stringValue))= @uRetailProcessorName)
	) C ON C.loanapp_id = M.Id
	Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = M.id
	Left join mwlAppStatus appStat on M.ID=appStat.LoanApp_id and appStat.StatusDesc='01 - Registered' 
	Left join mwlApprovalStatus approvalStat on M.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved'
END
-------Mortgage Advisor role Validation Loan---------------------
ELSE IF(@Role =2)
BEGIN
	INSERT INTO #tempLoan
	 Select distinct 'E3' DB,
	M.LoanNumber loan_no ,
	M.CurrentStatus,
	left(M.CurrentStatus,2)  LoanStatus,
	M.lastname BorrowerLastName,
	1 YNValue,
	M.DecisionStatus,
	'NA' UnderwriterType,
	loanData.AdjustedNoteAmt,
	appStat.StatusDateTime AS SubmittedDate,
	M.firstname BorrowerFirstName 
	from 
	 (Select M.*,borr.* from mwlloanapp M
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id,lastname,firstname from mwlBorrower group by loanapp_id,lastname,firstname) as borr on borr.loanapp_id = M.id
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id from mwlBorrower group by loanapp_id) as borrS on borr.loanapp_id = borrs.loanapp_id and  borrs.SequenceNum = borr.SequenceNum
	where (ltrim(rtrim(LoanNumber)) Like @LoanNumber + '%' or borr.LastName LIKE @LoanNumber +'%') and ChannelType LIKE 'Ret%'  
	and Originator_id = @Originator_id
	) M
	INNER JOIN dbo.mwlloandata as loanData on loanData.ObjOwner_id = M.id
	LEFT JOIN mwlAppStatus appStat on M.ID=appStat.LoanApp_id and appStat.StatusDesc='01 - Registered' 
	LEFT JOIN mwlApprovalStatus approvalStat on M.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved'
END

-------------------------------Sales Manager and Branch Manager role-----------------------------


ELSE IF(@Role in(3,10))
BEGIN

;WITH ReporteCTE
	AS ( 
			SELECT u3.userid,u3.E3userid FROM tblusers u3 where u3.userid = @UserId and u3.urole in (3,10)
			UNION ALL
			SELECT u1.userid,u1.E3userid
			FROM tblusermanagers m1 
			INNER JOIN  tblusers u1 on u1.userid = m1.userid 
				and m1.managerid = @UserId
				
			INNER JOIN tblroles r1 on u1.urole = r1.id 
			UNION ALL 

			SELECT u2.userid,u2.E3userid
			FROM tblusermanagers m2 
			INNER JOIN  tblusers u2 on u2.userid = m2.userid --and m2.managerid = 45027
			INNER JOIN ReporteCTE on m2.managerid = ReporteCTE.userid
			INNER JOIN tblroles r2 on u2.urole = r2.id 
			WHERE  m2.managerid NOT IN (@UserId)
		)


INSERT INTO #tempLoan
	Select distinct 'E3' DB,
	M.LoanNumber loan_no ,
	M.CurrentStatus,
	left(M.CurrentStatus,2)  LoanStatus,
	M.lastname BorrowerLastName,
	1 YNValue,
	M.DecisionStatus,
	'NA' UnderwriterType,
	loanData.AdjustedNoteAmt,
	appStat.StatusDateTime AS SubmittedDate,
	M.firstname BorrowerFirstName 
	from 
		(Select M.*,borr.* from mwlloanapp M
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id,lastname,firstname from mwlBorrower group by loanapp_id,lastname,firstname) as borr on borr.loanapp_id = M.id
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id from mwlBorrower group by loanapp_id) as borrS on borr.loanapp_id = borrs.loanapp_id and  borrs.SequenceNum = borr.SequenceNum
	where (ltrim(rtrim(LoanNumber)) Like @LoanNumber + '%' or borr.LastName LIKE @LoanNumber +'%') and ChannelType LIKE 'Ret%'  
	and Originator_id in(select E3userid from ReporteCTE)
	) M
	INNER JOIN dbo.mwlloandata as loanData on loanData.ObjOwner_id = M.id
	LEFT JOIN mwlAppStatus appStat on M.ID=appStat.LoanApp_id and appStat.StatusDesc='01 - Registered' 
	LEFT JOIN mwlApprovalStatus approvalStat on M.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved'
END
-------------------------------Retail Ops Manager and Retail Team Lead-----------------------------
ELSE IF(@Role in(16,11))
BEGIN


INSERT INTO #tempLoan
	Select distinct 'E3' DB,
	M.LoanNumber loan_no ,
	M.CurrentStatus,
	left(M.CurrentStatus,2)  LoanStatus,
	M.lastname BorrowerLastName,
	1 YNValue,
	M.DecisionStatus,
	'NA' UnderwriterType,
	loanData.AdjustedNoteAmt,
	appStat.StatusDateTime AS SubmittedDate,
	M.firstname BorrowerFirstName 
	from 
		(Select M.*,borr.* from mwlloanapp M
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id,lastname,firstname from mwlBorrower group by loanapp_id,lastname,firstname) as borr on borr.loanapp_id = M.id
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id from mwlBorrower group by loanapp_id) as borrS on borr.loanapp_id = borrs.loanapp_id and  borrs.SequenceNum = borr.SequenceNum
	where (ltrim(rtrim(LoanNumber)) Like @LoanNumber + '%' or borr.LastName LIKE @LoanNumber +'%') and ChannelType LIKE 'Ret%'  
	) M
	INNER JOIN dbo.mwlloandata as loanData on loanData.ObjOwner_id = M.id
	LEFT JOIN mwlAppStatus appStat on M.ID=appStat.LoanApp_id and appStat.StatusDesc='01 - Registered' 
	LEFT JOIN mwlApprovalStatus approvalStat on M.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved'
END
Select * from #tempLoan  
where (ISNUMERIC(LoanStatus) = 1 and  LoanStatus <=  29) --or LoanStatus LIKE 'RE%'
drop table #tempLoan


END

GO

