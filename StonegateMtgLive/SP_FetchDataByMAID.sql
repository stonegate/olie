USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_FetchDataByMAID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_FetchDataByMAID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel
-- Create Date : 10/04/2012  
-- Description: Procedure for Fetch Data MaiD 
-- =============================================        
CREATE PROC  [dbo].[SP_FetchDataByMAID](         
@UserId int        
)                  
AS                  
BEGIN              
    select ufirstname + ' ' + ulastname as uname, userid  from tblusers where userid in(select userid from tblusers where urole = 2 and MAID=@UserId)  
             
END 

GO

