USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanType]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanType]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanType](	  [ID] INT NOT NULL IDENTITY(1,1)	, [LoanTypeID] INT NULL	, [LoanType] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblLoanType] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO

