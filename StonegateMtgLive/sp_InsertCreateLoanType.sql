USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertCreateLoanType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertCreateLoanType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nitin>
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertCreateLoanType] 
(
@Loantype varchar(MAX)
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into tblLoanType (LoanType) 
	Values(@Loantype)
END

--------------------------------------------------------------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[sp_InsertCreatePackageId]    Script Date: 04/12/2013 17:44:25 ******/
SET ANSI_NULLS ON

GO

