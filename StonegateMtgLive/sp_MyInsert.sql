USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MyInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MyInsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsUserRegistrationE3.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_MyInsert]
(
@ufirstname varchar(Max),
@ulastname varchar(Max),
@uemailid varchar(Max),
@urole varchar(Max),
@isadmin bit,
@isactive bit,
@uidprolender varchar(Max),
@uparent varchar(Max),
@upassword varchar(Max),
@mobile_ph varchar(Max),
@loginidprolender varchar(Max),
@carrierid varchar(Max)

)
AS
BEGIN

	 Insert into tblusers(ufirstname,ulastname,uemailid,urole,isadmin,isactive,uidprolender,uparent,upassword,mobile_ph,loginidprolender,carrierid) Values
(@ufirstname,@ulastname,@uemailid,@urole,@isadmin,@isactive,@uidprolender,@uparent,@upassword,@mobile_ph,@loginidprolender,@carrierid)
                
END

GO

