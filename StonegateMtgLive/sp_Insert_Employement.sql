USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_Employement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_Employement]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Insert_Employement] 
(
@stringmode varchar(50),
@ApplicationID int,
@FirstName varchar(50),
@MiddleName varchar(50),
@LastName varchar(50),
@State varchar(30),
@City varchar(30),
@StreetAddress nvarchar(250),
@Zip varchar(50),
@PhoneNo nvarchar(20),
@PosApplied varchar(20),
@DateAvailable datetime,
@Adult bit,
@ProofLegallyEntitled bit,
@SGEmployed bit,
@EmployedDesc ntext,
@SGRelativeEmployed bit,
@RelativesDesc ntext,
@ConvictedOfCrime bit,
@CrimeDesc ntext,
@DismissedOrResign bit,
@DismissalDesc ntext,
@Imseeking varchar(50),
@Imavailable varchar(50),
@TempDatesAvailable varchar(500),
@HoursPerweek varchar(30),
@HighSchool varchar(30),
@TechSchool varchar(30),
@Collage varchar(30),
@Certification varchar(30),
@FirstProRefName varchar(50),
@FirstProRefPhone nvarchar(20),
@FirstProRefAddr varchar(500),
@SecondProRefName varchar(50),
@SecondProRefPhone nvarchar(20),
@SecondProRefAddr varchar(500),
@ThirdProRefName varchar(50),
@ThirdProRefPhone nvarchar(20),
@ThirdProRefAddr varchar(500),
@FirstPerRefName varchar(50),
@FirstPerRefPhone nvarchar(20),
@FirstPerRefAddr nvarchar(500),
@SecondPerRefName varchar(50),
@SecondPerRefPhone nvarchar(20),
@SecondPerRefAddr varchar(500),
@ThirdPerRefName varchar(50),
@ThirdPerRefPhone nvarchar(20),
@ThirdPerRefAddr varchar(500),
@NMLSLicense bit,
@AligiblityOfWork bit ,
@DeniedLicense bit,
@msofficeAvaiblityLevel varchar(12),
@CurrentEmployer bit,
@ContactEmployer bit,
@spSkills varchar(500),
@FileName varchar(50),
@LicenseNo varchar(100),
@ApplicantName varchar(50),
@AppliedDate datetime,
@CreatedDate datetime,
@jobid varchar(50) output,

@CompanyName varchar(50),
@coAddress varchar(500),
@coPhone varchar(20),
@FromDate datetime,
@Todate datetime,
@RateOfPay varchar(20),
@PositionHeld varchar(50),
@Supervisor varchar(50),
@WorkPerformed varchar(150),
@LeaveReason varchar(50),

@CompanyNamesec varchar(50),
@coAddresssec varchar(500),
@coPhonesec varchar(20),
@FromDatesec datetime,
@Todatesec datetime,
@RateOfPaysec varchar(20),
@PositionHeldsec varchar(50),
@Supervisorsec varchar(50),
@WorkPerformedsec varchar(150),
@LeaveReasonsec varchar(50),

@CompanyNamethd varchar(50),
@coAddressthd varchar(500),
@coPhonethd varchar(20),
@FromDatethd datetime,
@Todatethd datetime,
@RateOfPaythd varchar(20),
@PositionHeldthd varchar(50),
@Supervisorthd varchar(50),
@WorkPerformedthd varchar(150),
@LeaveReasonthd varchar(50)
)
AS
BEGIN
 INSERT INTO tblEmployementApplication(FirstName,MiddleName,LastName,State,City,StreetAddress,Zip,PhoneNo,PosApplied,DateAvailable,Adult,ProofLegallyEntitled,SGEmployed,EmployedDesc,SGRelativeEmployed,RelativesDesc,ConvictedOfCrime,CrimeDesc,DismissedOrResign,DismissalDesc,Imseeking,Imavailable,TempDatesAvailable,HoursPerweek,HighSchool,TechSchool,Collage,Certification,FirstProRefName,FirstProRefPhone,FirstProRefAddr,SecondProRefName,SecondProRefPhone,SecondProRefAddr,ThirdProRefName,ThirdProRefPhone,ThirdProRefAddr,FirstPerRefName,FirstPerRefPhone,FirstPerRefAddr,SecondPerRefName,SecondPerRefPhone,SecondPerRefAddr,ThirdPerRefName,ThirdPerRefPhone,ThirdPerRefAddr,NMLSLicense,AligiblityOfWork,DeniedLicense,msofficeAvaiblityLevel,CurrentEmployer,ContactEmployer,spSkills,FileName,LicenseNo,ApplicantName,AppliedDate,CreatedDate)      
 VALUES (@FirstName,@MiddleName,@LastName,@State,@City,@StreetAddress,@Zip,@PhoneNo,@PosApplied,@DateAvailable,@Adult,@ProofLegallyEntitled,@SGEmployed,@EmployedDesc,@SGRelativeEmployed,@RelativesDesc,@ConvictedOfCrime,@CrimeDesc,@DismissedOrResign,@DismissalDesc,@Imseeking,@Imavailable,@TempDatesAvailable,@HoursPerweek,@HighSchool,@TechSchool,@Collage,@Certification,@FirstProRefName,@FirstProRefPhone,@FirstProRefAddr,@SecondProRefName,@SecondProRefPhone,@SecondProRefAddr,@ThirdProRefName,@ThirdProRefPhone,@ThirdProRefAddr,@FirstPerRefName,@FirstPerRefPhone,@FirstPerRefAddr,@SecondPerRefName,@SecondPerRefPhone,@SecondPerRefAddr,@ThirdPerRefName,@ThirdPerRefPhone,@ThirdPerRefAddr,@NMLSLicense,@AligiblityOfWork,@DeniedLicense,@msofficeAvaiblityLevel,@CurrentEmployer,@ContactEmployer,@spSkills,@FileName,@LicenseNo,@ApplicantName,@AppliedDate,@CreatedDate)  
--
 SELECT @jobid = SCOPE_IDENTITY() 
--
INSERT INTO tblEmpWorkExp(jobid,CompanyName,coAddress,coPhone,FromDate,Todate,RateOfPay,PositionHeld,Supervisor,WorkPerformed,LeaveReason)
VALUES (@jobid,@CompanyName,@coAddress,@coPhone,@FromDate,@Todate,@RateOfPay,@PositionHeld,@Supervisor,@WorkPerformed,@LeaveReason)  
--
INSERT INTO tblEmpWorkExp(jobid,CompanyName,coAddress,coPhone,FromDate,Todate,RateOfPay,PositionHeld,Supervisor,WorkPerformed,LeaveReason)
VALUES (@jobid,@CompanyNamesec,@coAddresssec,@coPhonesec,@FromDatesec,@Todatesec,@RateOfPaysec,@PositionHeldsec,@Supervisorsec,@WorkPerformedsec,@LeaveReasonsec)  
--
INSERT INTO tblEmpWorkExp(jobid,CompanyName,coAddress,coPhone,FromDate,Todate,RateOfPay,PositionHeld,Supervisor,WorkPerformed,LeaveReason)
VALUES (@jobid,@CompanyNamethd,@coAddressthd,@coPhonethd,@FromDatethd,@Todatethd,@RateOfPaythd,@PositionHeldthd,@Supervisorthd,@WorkPerformedthd,@LeaveReasonthd)  
END









GO

