USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertHomeOwner]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertHomeOwner]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_InsertHomeOwner]     
(    
@Description varchar(MAX),    
@FileName varchar(MAX),    
@Loantype varchar(MAX),    
@Packageid int    
)    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
  Declare @lType varchar(max)  
  Select @lType = loantypeid from dbo.tblLoanType where id = @Loantype  
    -- Insert statements for procedure here    
 Insert into tblFormsHomeowner(Description,FileName,dttime,Loantype,Packageid)     
 Values(@Description,@FileName,getdate(),@lType,@Packageid)    
END
GO

