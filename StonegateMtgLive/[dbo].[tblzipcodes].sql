USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblzipcodes]') AND type in (N'U'))
DROP TABLE [dbo].[tblzipcodes]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblzipcodes](	  [ZIP Code] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [City] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [abbr] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO

