USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateRecordAfterAddImageUpload]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateRecordAfterAddImageUpload]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_UpdateRecordAfterAddImageUpload]
(
@ctid VARCHAR(MAX)
)
AS
BEGIN
update tblAdditionalUWDoc set isCommited = 1 where ctid = @ctid
END


GO

