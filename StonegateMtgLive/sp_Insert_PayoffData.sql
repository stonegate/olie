USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_PayoffData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_PayoffData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Insert_PayoffData] 
(
@stringmode varchar(50),
@Requestid int,      
@Loannumber varchar(50),   
@RequestDate datetime,    
@EmailID varchar(50),    
@NeedtoKnow ntext
)
AS
BEGIN
	 INSERT INTO tblRequestPayoff (Loannumber,RequestDate,EmailID,NeedtoKnow)      
 VALUES      
  (@Loannumber,@RequestDate,@EmailID,@NeedtoKnow)  
END




GO

