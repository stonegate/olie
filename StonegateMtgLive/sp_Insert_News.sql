USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_News]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_News]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procEDURE [dbo].[sp_Insert_News] 
(
@stringmode varchar(50),
@id int,      
@Title nvarchar(1000),   
@Body text,    
@IsActive bit,    
@IsNewsAnnouncement bit,
@CreatedBy int,
@IPAddress varchar(50),   
@CreatedDate datetime,    
@Role nvarchar(Max),
@RoleC nvarchar(Max),
@RoleR nvarchar(max),      
@NewsAnnousVisbleAd bit,     
@NewsAnnousVisbleDR bit,      
@NewsAnnousVisbleBR bit,     
@NewsAnnousVisbleLO bit,      
@NewsAnnousVisbleCU bit,      
@NewsAnnousVisbleRE bit,      
@NewsAnnousVisbleAM bit,        
@NewsAnnousVisbleDM bit  
)
AS
BEGIN
INSERT INTO tblNewsAndAnnouncement (Title, Body, IsActive, IsNewsAnnouncement, CreatedBy, CreatedDate,
 IPAddress, Role,RoleC,RoleR, NewsAnnousVisbleAd, NewsAnnousVisbleDR, NewsAnnousVisbleBR, NewsAnnousVisbleLO, 
 NewsAnnousVisbleCU, NewsAnnousVisbleRE, NewsAnnousVisbleAM, NewsAnnousVisbleDM)
	VALUES (@Title, @Body, @IsActive, @IsNewsAnnouncement, @CreatedBy, @CreatedDate, @IPAddress, 
	@Role,@RoleC,@RoleR, @NewsAnnousVisbleAd, @NewsAnnousVisbleDR, @NewsAnnousVisbleBR, @NewsAnnousVisbleLO,
	@NewsAnnousVisbleCU, @NewsAnnousVisbleRE, @NewsAnnousVisbleAM, @NewsAnnousVisbleDM)  

  

declare @lid varchar(50)
SET @lid = (SELECT TOP 1
	ID
FROM tblNewsAndAnnouncement
ORDER BY ID DESC)
 
  

END



GO

