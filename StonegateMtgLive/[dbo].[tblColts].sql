USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblColts]') AND type in (N'U'))
DROP TABLE [dbo].[tblColts]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblColts](	  [id] INT NOT NULL IDENTITY(1,1)	, [name] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [lastname] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [streetaddr] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [city] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [state] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [zipcode] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [phoneno] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [email] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [confirmemail] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [scoutprogram] CHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [colts] CHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [newspaper] CHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [friend] CHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [other] CHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [representative] CHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO

