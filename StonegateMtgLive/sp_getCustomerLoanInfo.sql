USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getCustomerLoanInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getCustomerLoanInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author: Abhishek Rastogi  
-- ALTER Date : 10/06/2012  
-- Edited By : --  
-- Edited Date : --  
-- Description: for clsPipeLine.cs BLL  
-- =============================================  
  
CREATE PROCEDURE [dbo].[sp_getCustomerLoanInfo]  
(  
@loanid VARCHAR(MAX)  
  
)  
AS  
BEGIN  
SELECT top 1 BEA.EmailAddress, GrossInterest, PIPmt,EscrowPmt,lo.lastpmtRecvdDate, lo.PrincipalBal,lo.DueDate, isnull(lo.PIPmt, 0) + isnull(lo.EscrowPmt,0) as TotalPmt,lo.EscrowBal, YTD.ClosingInterest,LastFileMainDate,    
YTD.Principal, YTD.TaxesDisb, YTD.InsuranceDisb, ST.Description AS LoanType, lo.OriginalTerm,lo.InterestRate, lo.NextPmtNum, lo.LoanCloseDate,   
lo.OriginalAmt, lo.MaturityDate, pro.AddressLine1, pro.AddressLine2, pro.City,pro.State,pro.Zip,co.CountryName,
pb.FirstMiddleName + ' ' + pb.LastName as  FirstMiddleName,pb.FirstMiddleName as FNAME,pb.LastName as LNAME, pb.HomePhoneNumber,
pb.WorkPhoneNumber,
AppraisalValue,OriginalLTV,MEA.AddressLine1 as MAddressLine1 , MEA.AddressLine2 as MAddressLine2, MEA.City as MCity,MEA.State as MState,MEA.Zip as MZip   
FROM dbo.Loan lo   
JOIN dbo.YearToDate ytd ON lo.LoanID = ytd.LoanID    
JOIN dbo.Property pro ON ytd.LoanID = pro.LoanID    
JOIN dbo.SystemTypes ST ON LO.LoanType = ST.Code    
JOIN dbo.Country co ON co.CountryCode = pro.Country   
JOIN dbo.Borrower pb ON pb.LoanID = lo.LoanID left outer join BorrowerEmailAddress BEA on lo.loanID=bea.loanid 
left outer join MailingAddress MEA on lo.loanID=MEA.loanid    
WHERE ST.FieldName ='LoanType' and pb.BorrowerID = 1 AND lo.LoanID = @loanid  
         
select top 1 Escrowamt as EscrowPmt,transactionamt as TotalPmt,(transactionamt - Escrowamt) as PIPmt,* from history where loanid = @loanid  and transactioncode in( '210' , '211' ) order by transactionDate Desc
             
       
select top 1 PIPayment,EscrowPmt,(PIPayment + EscrowPmt) as TotalPmt,* from paymentchange where loanid = @loanid order by duedate Desc  
  
  
END  
  
GO

