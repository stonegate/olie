USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PrcGetEmailData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PrcGetEmailData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
  
  
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[PrcGetEmailData]  
 -- Add the parameters for the stored procedure here  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 --select TOP 1 * from tblEmailServiceReader where updStatus=0   
--order by ID   
select TOP 1 * from tblEmailServiceReader where updStatus=0
order by id

select TOP 1 * from tblEmailServiceReader where updStatus=1
order by id
--order by RcvdDate desc  
--and ToAddress='tnierste@stonegatemtg.com'   
END  
  
  
  

GO

