USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertRate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertRate]
(
@strLoanType varchar(Max),
@IPoints varchar(Max),
@DecRate varchar(Max),
@DecAPR varchar(Max),
@DecClosingCost varchar(Max),
@CreatedDate varchar(Max),
@ModifiedDate varchar(Max)
)
AS
BEGIN

insert into tblrates(strLoanType,IPoints,DecRate,DecAPR,DecClosingCost,CreatedDate,ModifiedDate)
values (@strLoanType,@IPoints,@DecRate,@DecAPR,@DecClosingCost,@CreatedDate,@ModifiedDate)

END
GO

