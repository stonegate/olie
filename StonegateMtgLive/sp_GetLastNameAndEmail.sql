USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLastNameAndEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLastNameAndEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetLastNameAndEmail]
(
@LoanNo VARCHAR(MAX)
)
AS
BEGIN
select cb.borr_last,LD.lt_usr_underwriter,users.user_email from loandat LD inner join cobo Cb on ld.record_id = cb.parent_id  inner join users on LD.lt_usr_underwriter=users.record_id where ld.loan_no = @LoanNo


END

GO

