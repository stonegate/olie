USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Email]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Email]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Email]
(
    @Role int ,
	@Subject varchar(1000) ,
	@EmailBodyText text ,
	@CreatedBy int ,
	@CreatedDate datetime ,
	@IsSent bit 
)
AS
BEGIN
	 INSERT INTO tblEmail (Subject,EmailBodyText,CreatedDate,Role,CreatedBy,IsSent)      
 VALUES      
  (@Subject,@EmailBodyText,@CreatedDate,@Role,@CreatedBy,@IsSent)  
END
GO

