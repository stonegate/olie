USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getPasswordHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[getPasswordHistory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[getPasswordHistory]
 -- Add the parameters for the stored procedure here  
 @UserID int  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
    -- Insert statements for procedure here  
 SELECT top 3 [PasswordID]  
      ,[UserID]  
      ,[Password]  
  FROM [tblUserPasswordHistory] where UserID=@UserID  order by PasswordID desc
END  


GO

