USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDataListLoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDataListLoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDataListLoanApplication]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT     LoanApplicationID, LoanReasonID, HometypeID, Producttypeid, StateID, CreditScoreID, AnnulIncomeBorrower,LoanAmount, AssetBorrower, 
                RetirementAssetsBorrower, PropertyLocated, HomeOwned, PurchasePrise, DownPaymentAmt, PartOfTotalAssets, DownPaymentSourceID,  
                HaveRealtor, RealtorContactName, RealtorPhone, RealtorEmail, AppFName, AppLName, AppFName+','+ AppLName as AppFullNAme, AppStateID, AppZip, AppPrimaryPhone, AppSecondaryPhone,  
                AppEmail, CurrentPropertyValue, PropertyPurchaseMonth, YearID, ExistingPuchasePrice, CurrentMortageBal, MonthlyPayment, SecondMortage,  
                SecondMortageBal, DateEntered, ModifiedBy, ModifiedDate, UserAppearUrl, PropertyTypeID, PropertyTypeName, ContactTime, ContactTimeID,  
                Hometype, CreditScoreValue, StateName, LoanReason, ProductTypeName, AppStateName,Year,MAName,IsWorkingWithRealtor, PropertyRealtorCity,IsCashback,RealtorName,RealtorPhoneNo 
                FROM         (SELECT     tblLoanApplication.LoanApplicationID, tblLoanApplication.LoanReasonID, tblLoanApplication.HometypeID, tblLoanApplication.Producttypeid,
                tblLoanApplication.StateID, tblLoanApplication.CreditScoreID, tblLoanApplication.AnnulIncomeBorrower,LoanAmount, tblLoanApplication.AssetBorrower,
                tblLoanApplication.RetirementAssetsBorrower, tblLoanApplication.PropertyLocated, tblLoanApplication.HomeOwned, tblLoanApplication.PurchasePrise,  
                tblLoanApplication.DownPaymentAmt, tblLoanApplication.PartOfTotalAssets, tblLoanApplication.DownPaymentSourceID,  
                tblLoanApplication.HaveRealtor, tblLoanApplication.RealtorContactName, tblLoanApplication.RealtorPhone, tblLoanApplication.RealtorEmail,  
                tblLoanApplication.AppFName, tblLoanApplication.AppLName, tblLoanApplication.AppStateID, tblLoanApplication.AppZip,  
                tblLoanApplication.AppPrimaryPhone, tblLoanApplication.AppSecondaryPhone, tblLoanApplication.AppEmail, tblLoanApplication.CurrentPropertyValue,  
                tblLoanApplication.PropertyPurchaseMonth, tblLoanApplication.YearID, tblLoanApplication.ExistingPuchasePrice,  
                tblLoanApplication.CurrentMortageBal, tblLoanApplication.MonthlyPayment, tblLoanApplication.SecondMortage, tblLoanApplication.SecondMortageBal,  
                tblLoanApplication.DateEntered, tblLoanApplication.ModifiedBy, tblLoanApplication.ModifiedDate, tblLoanApplication.UserAppearUrl,  
                tblLoanApplication.PropertyTypeID, tblPropertyType.Name AS PropertyTypeName, tblContactTime.ContactTime, tblLoanApplication.ContactTimeID,  
                tblHomeType.Hometype, tblCreditScore.CreditScoreValue, tblStateNylex.StateName, tblLoanReason.LoanReason,  
                tblProductType.name AS ProductTypeName, tblStateNylex_1.StateName AS AppStateName, tblYear.Year,tblLoanApplication.MAName,tblLoanApplication.IsWorkingWithRealtor, tblLoanApplication.PropertyRealtorCity,tblLoanApplication.IsCashback,tblLoanApplication.RealtorName,tblLoanApplication.RealtorPhoneNo 
                FROM         tblLoanApplication INNER JOIN
                tblPropertyType ON tblLoanApplication.PropertyTypeID = tblPropertyType.PropertyTypeID INNER JOIN 
                tblContactTime ON tblLoanApplication.ContactTimeID = tblContactTime.ContactTimeID INNER JOIN 
                tblHomeType ON tblLoanApplication.HometypeID = tblHomeType.HometypeID INNER JOIN
                tblCreditScore ON tblLoanApplication.CreditScoreID = tblCreditScore.CreditScoreID INNER JOIN 
                tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID INNER JOIN 
                tblLoanReason ON tblLoanApplication.LoanReasonID = tblLoanReason.LoanReasonID INNER JOIN 
                tblProductType ON tblLoanApplication.Producttypeid = tblProductType.producttypeid INNER JOIN 
                tblStateNylex AS tblStateNylex_1 ON tblLoanApplication.AppStateID = tblStateNylex_1.StateID INNER JOIN 
                tblYear ON tblLoanApplication.YearID = tblYear.YearID) AS App order by LoanApplicationID desc 

END


GO

