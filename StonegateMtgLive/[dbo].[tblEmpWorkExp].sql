USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblEmpWorkExp]') AND type in (N'U'))
DROP TABLE [dbo].[tblEmpWorkExp]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmpWorkExp](	  [workid] INT NOT NULL IDENTITY(1,1)	, [jobid] INT NULL	, [CompanyName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [coAddress] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [coPhone] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FromDate] DATETIME NULL	, [Todate] DATETIME NULL	, [RateOfPay] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PositionHeld] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Supervisor] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [WorkPerformed] VARCHAR(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LeaveReason] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO

