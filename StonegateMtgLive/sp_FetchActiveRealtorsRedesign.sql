USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchActiveRealtorsRedesign]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchActiveRealtorsRedesign]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchActiveRealtorsRedesign]
	-- Add the parameters for the stored procedure here

(
@urole int,
@state nvarchar(10), 
@city nvarchar(100)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


select * from tblusers where state=@state and city=@city and urole=@urole and isActive=1 and ispublish=1	
END


GO

