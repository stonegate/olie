USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPayoffStatement]') AND type in (N'U'))
DROP TABLE [dbo].[tblPayoffStatement]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPayoffStatement](	  [record_id] INT NOT NULL IDENTITY(1,1)	, [userid] INT NULL	, [PayoffDate] DATETIME NULL	, [PayoffReasonId] INT NULL	, [IsReadyToContact] BIT NULL)USE [HomeLendingExperts]
GO

