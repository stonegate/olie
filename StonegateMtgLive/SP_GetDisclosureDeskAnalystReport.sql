USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetDisclosureDeskAnalystReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetDisclosureDeskAnalystReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Narayan.NR
-- Create date: 24 Jan 2014            
-- Description: To get Disclosuredesk analyst pipline data.
--(select Id FROM mwsCustFieldDef where CustomFieldName='Retail Disclosure Desk Analyst')
-- =============================================
CREATE PROCEDURE [dbo].[SP_GetDisclosureDeskAnalystReport]              
(  	
	@UserID int,
	@Originators VARCHAR(MAX) = NULL
)                
AS                
BEGIN 

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

Select top 1 urole,e3userid into #tmpdclUser from tblusers  where userid=@UserID

DECLARE @RoleID int
SET @RoleID=(Select urole from #tmpdclUser)
DECLARE @E3userid varchar(200)
SET @E3userid=(Select e3userid from #tmpdclUser)

--BRD-250
DECLARE @tblOriginators TABLE (ID varchar(50))


if(@RoleID = 1 OR @RoleID = 3 OR @RoleID = 10)
BEGIN
	INSERT INTO @tblOriginators
	SELECT items FROM dbo.SplitString (@Originators,',')
END

--select * from @tblOriginators

DECLARE @disclosureduedays smallint
SET  @disclosureduedays= 3


	if(@RoleID=1)
	Begin	
		With CTE_ROM as
		(
		Select distinct 'E3' AS DB
			,loanapp.LoanNumber AS loan_no	
			,loanapp.OriginatorName
			,borr.firstname AS BorrowerFirstName
			,borr.lastname AS BorrowerLastName
			,loanapp.currentStatus AS LoanStatus
			,discappstat.statusdatetime AS InitialDisclosureDate
			,DATEADD(day ,@disclosureduedays, discappstat.statusdatetime)  as DuebyDisclosure 
			,rediscappstat.statusdatetime AS RedisclosureDate
			,DATEADD(day ,@disclosureduedays, rediscappstat.statusdatetime) as DuebyReDisclosure 
			,loanData.AdjustedNoteAmt AS loan_amt	
			,institute.office as Branch
			,uSummary.UnderwriterName as UnderWriter
			,loanapp.processorname as ProcessorName
			,DisclosureAnalyst.StringValue as DisclosureAnalyst
			,loanapp.ID as	record_id
			,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
			,E3user.ID as E3USERID
			,loanapp.originator_id as Originatorid
			,users.branchinstitution_id
			 FROM  mwlloanapp AS loanapp 
			 --OLIEPS-503
			INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
			INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
			INNER JOIN mwlborrower AS borr ON borr.loanapp_id = loanapp.id
			INNER JOIN dbo.mwlloandata AS loandata ON loandata.objowner_id = loanapp.id
			INNER join mwlAppStatus AS appStat on loanapp.ID=appStat.LoanApp_id	
			LEFT JOIN dbo.mwlcustomfield as DisclosureAnalyst ON DisclosureAnalyst.loanapp_id= loanapp.ID AND DisclosureAnalyst.custFieldDef_ID='E16914219F3944DDB08117BDE784BCA4'
			LEFT JOIN dbo.mwaMWUser AS E3user ON E3user.Fullname=DisclosureAnalyst.StringValue
			LEFT JOIN dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
			LEFT join (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus
			where StatusDesc='05 - Application Taken' GROUP BY loanapp_id ,statusdesc) discappstat on loanapp.Id = discappstat.LoanApp_id
			LEFT join  (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus 
			where StatusDesc = '30.1 - Req for Re-disclosure'  GROUP BY loanapp_id ,statusdesc ) rediscappstat ON loanapp.Id = rediscappstat.LoanApp_id
	
			WHERE borr.sequencenum=1 AND loanapp.LoanNumber is not null AND loanapp.LoanNumber <> '' 
			AND loanapp.ChannelType like '%RETAIL%'   
			AND loanapp.CurrentStatus  IN ('05 - Application Taken','06 - Disclosures Out','30.1 - Req for Re-disclosure','30.2 - Re-disclosure Out')  
		)
		select * from CTE_ROM where Originatorid in (select id from @tblOriginators e3user inner join tblusers olieUser on e3user.id=olieuser.e3userid
		where olieuser.urole in(2,3,10) )
	End

	if(@RoleID=11)
	Begin	
		With CTE_ROM as
		(
		Select distinct 'E3' AS DB
			,loanapp.LoanNumber AS loan_no	
			,loanapp.OriginatorName
			,borr.firstname AS BorrowerFirstName
			,borr.lastname AS BorrowerLastName
			,loanapp.currentStatus AS LoanStatus
			,discappstat.statusdatetime AS InitialDisclosureDate
			,DATEADD(day ,@disclosureduedays, discappstat.statusdatetime)  as DuebyDisclosure 
			,rediscappstat.statusdatetime AS RedisclosureDate
			,DATEADD(day ,@disclosureduedays, rediscappstat.statusdatetime) as DuebyReDisclosure 
			,loanData.AdjustedNoteAmt AS loan_amt	
			,institute.office as Branch
			,uSummary.UnderwriterName as UnderWriter
			,loanapp.processorname as ProcessorName
			,DisclosureAnalyst.StringValue as DisclosureAnalyst
			,loanapp.ID as	record_id
			,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
			,E3user.ID as E3USERID
			,loanapp.originator_id as Originatorid
			,users.branchinstitution_id
			 FROM  mwlloanapp AS loanapp 
			 --OLIEPS-503
			INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
			INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
			INNER JOIN mwlborrower AS borr ON borr.loanapp_id = loanapp.id
			INNER JOIN dbo.mwlloandata AS loandata ON loandata.objowner_id = loanapp.id
			INNER join mwlAppStatus AS appStat on loanapp.ID=appStat.LoanApp_id	
			LEFT JOIN dbo.mwlcustomfield as DisclosureAnalyst ON DisclosureAnalyst.loanapp_id= loanapp.ID AND DisclosureAnalyst.custFieldDef_ID='E16914219F3944DDB08117BDE784BCA4'
			LEFT JOIN dbo.mwaMWUser AS E3user ON E3user.Fullname=DisclosureAnalyst.StringValue
			LEFT JOIN dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
			LEFT join (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus
			where StatusDesc='05 - Application Taken' GROUP BY loanapp_id ,statusdesc) discappstat on loanapp.Id = discappstat.LoanApp_id
			LEFT join  (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus 
			where StatusDesc = '30.1 - Req for Re-disclosure'  GROUP BY loanapp_id ,statusdesc ) rediscappstat ON loanapp.Id = rediscappstat.LoanApp_id
	
			WHERE borr.sequencenum=1 AND loanapp.LoanNumber is not null AND loanapp.LoanNumber <> '' 
			AND loanapp.ChannelType like '%RETAIL%'   
			AND loanapp.CurrentStatus  IN ('05 - Application Taken','06 - Disclosures Out','30.1 - Req for Re-disclosure','30.2 - Re-disclosure Out')  
		)
		select * from CTE_ROM
	End


	ELSE if(@RoleID = 21)
	Begin
	With CTE_RDA as
	(
	Select distinct 'E3' AS DB
    ,loanapp.LoanNumber AS loan_no	
	,loanapp.OriginatorName
	,borr.firstname AS BorrowerFirstName
	,borr.lastname AS BorrowerLastName
	,loanapp.currentStatus AS LoanStatus
	,discappstat.statusdatetime AS InitialDisclosureDate
	,DATEADD(day ,@disclosureduedays, discappstat.statusdatetime)  as DuebyDisclosure 
	,rediscappstat.statusdatetime AS RedisclosureDate
	,DATEADD(day ,@disclosureduedays, rediscappstat.statusdatetime) as DuebyReDisclosure 
	,loanData.AdjustedNoteAmt AS loan_amt	
	,institute.Office as Branch
	,uSummary.UnderwriterName as UnderWriter
	,loanapp.processorname as ProcessorName
	,DisclosureAnalyst.StringValue as DisclosureAnalyst
    ,loanapp.ID as	record_id
	,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
	,E3user.ID as E3USERID
	,loanapp.originator_id as Originatorid
	,users.branchinstitution_id
	 FROM  mwlloanapp AS loanapp 
	--OLIEPS-503
	INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
	INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
	INNER JOIN mwlborrower AS borr ON borr.loanapp_id = loanapp.id
	INNER JOIN dbo.mwlloandata AS loandata ON loandata.objowner_id = loanapp.id
	INNER join mwlAppStatus AS appStat on loanapp.ID=appStat.LoanApp_id
	INNER JOIN dbo.mwlcustomfield as DisclosureAnalyst ON DisclosureAnalyst.loanapp_id= loanapp.ID AND	DisclosureAnalyst.custFieldDef_ID='E16914219F3944DDB08117BDE784BCA4'
	INNER JOIN dbo.mwaMWUser AS E3user ON E3user.Fullname=DisclosureAnalyst.StringValue 
	LEFT JOIN dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
	
    LEFT join (select Min(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus
	where StatusDesc='05 - Application Taken' GROUP BY loanapp_id ,statusdesc) discappstat on loanapp.Id = discappstat.LoanApp_id
	LEFT join  (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus 
	where StatusDesc = '30.1 - Req for Re-disclosure'  GROUP BY loanapp_id ,statusdesc ) rediscappstat ON loanapp.Id = rediscappstat.LoanApp_id
	
	WHERE borr.sequencenum=1 AND loanapp.LoanNumber is not null AND loanapp.LoanNumber <> '' 
	AND loanapp.ChannelType like '%RETAIL%'   
	AND loanapp.CurrentStatus  IN ('05 - Application Taken','06 - Disclosures Out','30.1 - Req for Re-disclosure','30.2 - Re-disclosure Out')  
	)
	select * from CTE_RDA where E3USERID=@E3userid
	End

	ELSE if(@RoleID = 2)
	Begin	
	With CTE_MA as
	(
	Select distinct 'E3' AS DB
    ,loanapp.LoanNumber AS loan_no	
	,loanapp.OriginatorName
	,borr.firstname AS BorrowerFirstName
	,borr.lastname AS BorrowerLastName
	,loanapp.currentStatus AS LoanStatus
	,discappstat.statusdatetime AS InitialDisclosureDate
	,DATEADD(day ,@disclosureduedays, discappstat.statusdatetime)  as DuebyDisclosure 
	,rediscappstat.statusdatetime AS RedisclosureDate
	,DATEADD(day ,@disclosureduedays, rediscappstat.statusdatetime) as DuebyReDisclosure 
	,loanData.AdjustedNoteAmt AS loan_amt	
	,institute.office as Branch
	,uSummary.UnderwriterName as UnderWriter
	,loanapp.processorname as ProcessorName
	,DisclosureAnalyst.StringValue as DisclosureAnalyst
    ,loanapp.ID as	record_id
	,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
	,E3user.ID as E3USERID
	,loanapp.originator_id as Originatorid
	,users.branchinstitution_id
	 FROM  mwlloanapp AS loanapp 
	 --OLIEPS 503
	INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
	INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
	INNER JOIN mwlborrower AS borr ON borr.loanapp_id = loanapp.id
	INNER JOIN dbo.mwlloandata AS loandata ON loandata.objowner_id = loanapp.id
	INNER join mwlAppStatus AS appStat on loanapp.ID=appStat.LoanApp_id
	LEFT JOIN dbo.mwlcustomfield as DisclosureAnalyst ON DisclosureAnalyst.loanapp_id= loanapp.ID AND 	DisclosureAnalyst.custFieldDef_ID='E16914219F3944DDB08117BDE784BCA4'
	LEFT JOIN dbo.mwaMWUser AS E3user ON E3user.ID=loanapp.originator_id	
	LEFT JOIN dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
	LEFT join (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus
	where StatusDesc='05 - Application Taken' GROUP BY loanapp_id ,statusdesc) discappstat on loanapp.Id = discappstat.LoanApp_id
	LEFT join  (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus 
	where StatusDesc = '30.1 - Req for Re-disclosure'  GROUP BY loanapp_id ,statusdesc ) rediscappstat ON loanapp.Id = rediscappstat.LoanApp_id
	
	WHERE borr.sequencenum=1 AND loanapp.LoanNumber is not null AND loanapp.LoanNumber <> '' 
	AND loanapp.ChannelType like '%RETAIL%'   
	AND loanapp.CurrentStatus  IN ('05 - Application Taken','06 - Disclosures Out','30.1 - Req for Re-disclosure','30.2 - Re-disclosure Out')  
	AND loanapp.originator_id=@E3userid
	)
	select * from CTE_MA

	End

	ELSE if(@RoleID = 3 or @RoleID = 10)
	Begin

	With CTE_SB as
	(
	Select distinct 'E3' AS DB
    ,loanapp.LoanNumber AS loan_no	
	,loanapp.OriginatorName
	,borr.firstname AS BorrowerFirstName
	,borr.lastname AS BorrowerLastName
	,loanapp.currentStatus AS LoanStatus
	,discappstat.statusdatetime AS InitialDisclosureDate
	,DATEADD(day ,@disclosureduedays, discappstat.statusdatetime)  as DuebyDisclosure 
	,rediscappstat.statusdatetime AS RedisclosureDate
	,DATEADD(day ,@disclosureduedays, rediscappstat.statusdatetime) as DuebyReDisclosure 
	,loanData.AdjustedNoteAmt AS loan_amt	
	,institute.office as Branch
	,uSummary.UnderwriterName as UnderWriter
	,loanapp.processorname as ProcessorName
	,DisclosureAnalyst.StringValue as DisclosureAnalyst
    ,loanapp.ID as	record_id
	,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
	,E3user.ID as E3USERID
	,loanapp.originator_id as Originatorid
	,users.branchinstitution_id
	 FROM  mwlloanapp AS loanapp 
	 --OLIEPS-503
	INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
	INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
	INNER JOIN mwlborrower AS borr ON borr.loanapp_id = loanapp.id
	INNER JOIN dbo.mwlloandata AS loandata ON loandata.objowner_id = loanapp.id
	INNER join mwlAppStatus AS appStat on loanapp.ID=appStat.LoanApp_id
	LEFT JOIN dbo.mwaMWUser AS E3user ON E3user.id=loanapp.originator_id
	LEFT JOIN dbo.mwlcustomfield as DisclosureAnalyst ON DisclosureAnalyst.loanapp_id = loanapp.ID AND	DisclosureAnalyst.custFieldDef_ID='E16914219F3944DDB08117BDE784BCA4'
	
	LEFT JOIN dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
	LEFT join (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus
	where StatusDesc='05 - Application Taken' GROUP BY loanapp_id ,statusdesc) discappstat on loanapp.Id = discappstat.LoanApp_id
	LEFT join  (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus 
	where StatusDesc = '30.1 - Req for Re-disclosure'  GROUP BY loanapp_id ,statusdesc ) rediscappstat ON loanapp.Id = rediscappstat.LoanApp_id
	
	WHERE borr.sequencenum=1 AND loanapp.LoanNumber is not null AND loanapp.LoanNumber <> '' 
	AND loanapp.ChannelType like '%RETAIL%'   
	AND loanapp.CurrentStatus  IN ('05 - Application Taken','06 - Disclosures Out','30.1 - Req for Re-disclosure','30.2 - Re-disclosure Out')  	
	)
	select * from CTE_SB where Originatorid in (select id from @tblOriginators e3user inner join tblusers olieUser on e3user.id=olieuser.e3userid
	where olieuser.urole in(2,3,10) )

	End

END



GO

