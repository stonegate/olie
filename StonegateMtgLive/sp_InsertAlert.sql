USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertAlert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertAlert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertAlert]
(

@UserId varchar (max),
@LoanNo varchar (max),
@IsSMSPaymentReminderAlert varchar (max),
@IsSMSPaymentPostedAlert varchar (max),
@IsSMSPaymentPastDue varchar (max),
@IsEmailPaymentReminderAlert varchar (max),
@IsEmailPaymentPostedAlert varchar (max),
@IsEmailPaymentPastDue varchar (max),
@IsPaymentReminderAlert varchar (max),
@IsPaymentPostedReminderAlert varchar (max),
@IsPaymentPastDue varchar (max),
@PaymentReminderDay varchar (max),
@PaymentPastDueReminderDay varchar (max)

)
AS
BEGIN
insert into tblServiceInfo (UserId,LoanNo,IsSMSPaymentReminderAlert,IsSMSPaymentPostedAlert,IsSMSPaymentPastDue,
IsEmailPaymentReminderAlert,IsEmailPaymentPostedAlert,IsEmailPaymentPastDue,IsPaymentReminderAlert,IsPaymentPostedReminderAlert,
IsPaymentPastDue,PaymentReminderDay,PaymentPastDueReminderDay) values 
(@UserId,@LoanNo,@IsSMSPaymentReminderAlert,@IsSMSPaymentPostedAlert,@IsSMSPaymentPastDue,@IsEmailPaymentReminderAlert,@IsEmailPaymentPostedAlert,@IsEmailPaymentPastDue,@IsPaymentReminderAlert,@IsPaymentPostedReminderAlert,@IsPaymentPastDue,@PaymentReminderDay,@PaymentPastDueReminderDay)
     
END


GO

