USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetRetailFinalPriceReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetRetailFinalPriceReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SP_GetRetailFinalPriceReport]           
(            
@RoleID int,           
@strID varchar(max),    
@FromDate  varchar(20),  
@ToDate varchar(20),  
@oPuid varchar(max),
@type varchar(10)='DLA' ,
@isFliter bit=0,
@Branch VARCHAR(MAX) 
)            
AS            
BEGIN            
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;           
Declare @SQL nvarchar(max)            
Set @SQL =''            
Set @SQL ='select ''E3'' as DB,loanapp.currpipestatusdate, loanapp.ID as record_id,loanapp.Channeltype as BusType,LoanNumber as loan_no,borr.firstname as BorrowerFirstName ,  
		   borr.lastname as BorrowerLastName,CurrentStatus as LoanStatus, CurrentStatus as LoanStatusDA, 
		   convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,  
		   LoanData.LoanProgramName as Prog_desc ,loanapp.Originatorname,  
		   case when loanapp.LoanPurpose1003 is null then '''' when loanapp.LoanPurpose1003 = ''PU'' then ''Purchase'' 
		   when loanapp.LoanPurpose1003 = ''RE'' then ''Refinance''   
		   end as Purpose,CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed,(Loandata.NoteRate * 100) as Int_rate,  
		   loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,  
		   convert(varchar(40),LockRecord.LockExpirationDate,101) as LockExpires,LockRecord.LockDateTime,  
		   convert(varchar(35),LockRecord.LockDateTime,101) as Lockdate,UnderwriterName as UnderWriter,CloserName as Closer,  
		   CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,
		   '''' as FundedDate,  
		   '''' as ReceivedInUW,LoanData.ShortProgramName as prog_code   ,
		   loanapp.Branch Branch1,
		   institute.office Branch,
		   case loanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' WHEN ''M'' 
		   THEN  ''USDA/Rural Housing Service'' End as LoanProgram ,
		   loanData.AdjustedNoteAmt as LoanAmount,
		   LockRecord.LockREason,
		   case Isnull(PricingResult.lenderbaserate,'''') When '''' then '''' Else 
		   convert(varchar,((PricingResult.lenderbaserate)*100)) + '' %'' end interestRate,
		   PricingResult.lenderClosingprice  FinalLockPrice'
---Start --Added By Tavant Team for BRD-125-----------------------------
IF(@isFliter =1)
	BEGIN
		SET @SQL =@SQL + ', region1.regionname as Region'
	END
SET  @SQL = @SQL + ' from mwlLoanApp  loanapp
					 INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
			         INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
                    Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id                    
					left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id    
					Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and 
					Broker.InstitutionType = ''BRANCH'' and Broker.objownerName=''BranchInstitution''  
					left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id  and 
					LockRecord.Status<>''CANCELED'' and isnull(LockRecord.LockReason,'''')<>''''  
					Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''  
					Left join mwlPricingResult  as PricingResult on  PricingResult.ObjOwner_id = LockRecord.id '
IF(@isFliter =1)
	BEGIN
		SET @SQL = @SQL + '  LEFT JOIN tblbranchoffices AS bo ON  bo.branch=institute.office 	
							 LEFT JOIN tblregion AS region1   ON bo.region = region1.regionvalue  '
	END	
SET  @SQL = @SQL + ' where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' and loanapp.ChannelType like ''%RETAIL%'''  					  
IF (@isFliter = 1)
BEGIN
     SET @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) '
	  if(@Branch is not null and @Branch <>'')
	 begin
		SET @SQL = @SQL + ' and (institute.Office in(select * from dbo.SplitString('''+  @Branch  +''','','')))' 
	 end
END		  
ELSE
BEGIN
   IF (@RoleID != 0
      AND @RoleID != 1
      AND @RoleID != 11)
   BEGIN
      IF (LEN(@strID) = 0)
      BEGIN
         IF (@RoleID = 2)
         BEGIN
            --Set @SQL = @SQL + 'and (Originator_id in ('''+ @strID +'''))'  
            SET @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) '
         END
         ELSE
         IF (@RoleID = 3)
         BEGIN
            --Set @SQL = @SQL + ' and (loanapp.ID in ('''+ @strID +''') or Originator_id in ('''+ @strID +''' ) )'  
            SET @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString(''' + @strID + ''','','')) 
                            or Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')))'
         END
         ELSE
         IF (@RoleID = 10
            OR @RoleID = 7)
         BEGIN
            --Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID +''') or loanapp.id in ('''+ @strID +'''))'  
            SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @oPuid + ''','','')) 
                            or loanapp.id in (select * from dbo.SplitString(''' + @strID + ''','','')))'
         END
      END
      ELSE
      BEGIN
         IF (@RoleID = 2)
         BEGIN
            --Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID +'''))'  
            SET @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) '
         END
         ELSE
         IF (@RoleID = 3)
         BEGIN
            -- Set @SQL = @SQL + ' and (loanapp.ID in ('''+ @strID +''') or Originator_id in (''' + @oPuid +''') )'  
            SET @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString(''' + @strID + ''','','')) 
                                or Originator_id in (select * from dbo.SplitString(''' + @oPuid + ''','','')))'
         END
         ELSE
         IF (@RoleID = 10
            OR @RoleID = 7)
         BEGIN
            --Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID +''') or loanapp.id in ('''+ @strID +'''))'  
            SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @oPuid + ''','','')) 
                                or loanapp.id in (select * from dbo.SplitString(''' + @strID + ''','','')))'
         END
      END
   END
END
--Set @SQL = @SQL + ' and  loanapp.currpipestatusdate between convert(datetime,''' + @FromDate + ''',101) and convert(datetime,''' + @ToDate + ''',101)'  
if(@FromDate != '' and @ToDate !='')
Set @SQL=@SQL+ ' and loanapp.currpipestatusdate between '''+@FromDate+'''  and  '''+Convert(varchar(20),DATEADD (dd , 1 , @ToDate),101)+''''
else if(@FromDate != '')
Set @SQL=@SQL+ ' and loanapp.currpipestatusdate > '''+@FromDate+''''	
else if(@ToDate != '')
Set @SQL=@SQL+ ' and loanapp.currpipestatusdate < '''+@ToDate+''''  
              
Set @SQL = @SQL + ' and loanapp.CurrentStatus =''62 - Funded'''
           
                  

--Print @SQL            
exec sp_executesql @SQL            
END 
/*------------*/


--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




GO

