USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertCategory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertCategory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertCategory]
(
    @CateName varchar(50)
)
AS
BEGIN
	 Insert into tblCategories(CateName) Values 
    ( @CateName )

END


GO

