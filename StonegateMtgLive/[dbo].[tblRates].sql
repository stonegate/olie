USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRates]') AND type in (N'U'))
DROP TABLE [dbo].[tblRates]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRates](	  [RateID] INT NOT NULL IDENTITY(1,1)	, [strLoanType] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IPoints] INT NULL	, [DecRate] DECIMAL(18,3) NULL	, [DecAPR] DECIMAL(18,3) NULL	, [DecClosingCost] DECIMAL(18,2) NULL	, [CreatedDate] DATETIME NULL	, [ModifiedDate] DATETIME NULL DEFAULT(getdate())	, [IsActive] BIT NULL DEFAULT((1))	, [IRowOrder] INT NULL	, CONSTRAINT [PK_tblRates] PRIMARY KEY ([RateID] ASC))USE [HomeLendingExperts]
GO

