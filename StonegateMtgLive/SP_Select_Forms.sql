USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Select_Forms]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Select_Forms]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel
-- Create Date : 10/04/2012  
-- Description: Procedure for Select Form 
-- =============================================        
CREATE PROC  [dbo].[SP_Select_Forms](       
@URoleId int      
)                
AS                
BEGIN            
 IF(@URoleId ='0')       
         
   select  dbo.GetFormRole(fo.formid)as Roleid,* from tblforms FO left outer join tblCategories CAT on FO.Cateid=CAT.catid ORDER BY dttime DESC     
       
 ELSE      
       
	select dbo.GetFormRole(FO.formid)as Roleid, formID,Description,FileName,dttime,isnews,CateName from tblforms FO left outer join tblCategories CAT on FO.Cateid=CAT.catid where formid in (select formid from tblformroles   
	where uroleid =  @URoleId ) ORDER BY dttime DESC  
      
         
END 

GO

