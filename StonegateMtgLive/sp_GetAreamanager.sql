USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAreamanager]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAreamanager]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_GetAreamanager]
(
	@ManagerId varchar(500) ,
	@Region varchar(500)
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SELECT t1.userid
		,ufirstname + space(1) + ulastname fullname
	FROM tblusers t1
	INNER JOIN tblUserManagers t2 ON t1.userid = t2.userid
		AND t1.isactive = 1
		AND t2.ManagerId IN (
			SELECT *
			FROM dbo.splitstring(@ManagerId, ',')
			)
		AND t1.region IN (
			SELECT *
			FROM dbo.splitstring(@Region, ',')
			)
	WHERE t1.urole = 19
	
END


GO

