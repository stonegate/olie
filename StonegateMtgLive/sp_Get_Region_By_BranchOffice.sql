USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_Region_By_BranchOffice]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_Region_By_BranchOffice]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Rakesh Kumar Mohanty>
-- Create date: <Create Date,19th may 2014>
-- Description:	<Description,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Get_Region_By_BranchOffice]
	-- Add the parameters for the stored procedure here
	@E3UserId Nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	Declare @offfice Nvarchar(100)
Set @offfice=(select Office from  mwcInstitution c inner join mwaMWUser b ON  c.ID = b.BranchInstitution_ID
			  where b.id=@E3UserId and c.Office LIKE 'Retail%')

select region from [dbo].[tblBranchOffices] where Branch=@offfice

END



GO

