USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblServiceBanner]') AND type in (N'U'))
DROP TABLE [dbo].[tblServiceBanner]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblServiceBanner](	  [ServiceBannerID] INT NOT NULL IDENTITY(1,1)	, [Message] VARCHAR(300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [PublishDate] DATETIME NOT NULL	, [ExpireDate] DATETIME NOT NULL	, [CreatedDate] DATETIME NULL	, [UpdatedDate] DATETIME NULL	, [Status] CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedBy] INT NULL	, [UpdatedBy] INT NULL)USE [HomeLendingExperts]
GO

