USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAllMyPipelineReportChannelWise]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAllMyPipelineReportChannelWise]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetAllMyPipelineReportChannelWise]
AS
BEGIN
SELECT 'P' as DB,'' as DecisionStatus, loandat.record_id, loan_no, RTRIM(cobo.borr_last)  AS BorrowerLastName,
lt_loan_stats AS LoanStatus,case LU_FINAL_HUD_STATUS when '' then 'Pending' when NULL then 'Pending' else 'Approved' end as LU_FINAL_HUD_STATUS,
app_date AS SubmittedDate,appr_date AS DateApproved, lookups.descriptn AS TransType,lookupprogram.descriptn as LoanProgram,
loan_amt,  lookupsLock.descriptn AS LockStatus, LOANDAT2.CRED_SCORE_USED as CreditScoreUsed ,locks.lock_exp_date AS LockExpirationDate, 
RTRIM(users.first_name) + ' ' + users.last_name AS UnderWriter,rtrim(users2.first_name) + ' ' + Rtrim(users2.last_name) AS Closer, 
rtrim(brokers.brok_name) as BrokerName, case locks.LK_LU_CASHOUT when '0' then 'No' when '1' then 'Yes' end as  CashOut, 
case locks.LK_LU_CASHOUT when '0' then 'No' when '1' then 'Yes' end as  CashOut, convert(varchar(35),loandat2.DOC_EST_DATE,101) AS ScheduleDate,
loandat.lu_bus_type,bustype.descriptn BusType, CASE WHEN (SELECT count(a.record_id) as TotalConditions 
FROM dbo.condits a,lookups b  where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = 'aaw' and descriptn='PROCESSOR') 
and a.parent_id IN ( SELECT record_id FROM dbo.loandat as loan1  WHERE loan1.loan_no=loandat.loan_no)) = 0 then 0 
WHEN (SELECT count(a.record_id) as TotalCleared FROM dbo.condits a,lookups b 
where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = 'aaw' and descriptn='PROCESSOR') and ISNULL(a.cond_recvd_date,a.COND_CLEARED_DATE) IS NOT NULL
and a.parent_id IN ( SELECT record_id FROM dbo.loandat as loan2  WHERE loan2.loan_no=loandat.loan_no)) = 0 THEN 0 ELSE (SELECT count(a.record_id) as TotalCleared 
FROM dbo.condits a,lookups b  where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = 'aaw' and descriptn='PROCESSOR') and ISNULL(a.cond_recvd_date,a.COND_CLEARED_DATE) IS NOT NULL 
and a.parent_id IN ( SELECT record_id FROM dbo.loandat as loan2  WHERE loan2.loan_no=loandat.loan_no)) * 100 / (SELECT count(a.record_id) as TotalConditions 
FROM dbo.condits a,lookups b  where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = 'aaw' and descriptn='PROCESSOR') and a.parent_id IN ( SELECT record_id FROM dbo.loandat as loan1  WHERE loan1.loan_no=loandat.loan_no))END AS PER
FROM loandat INNER JOIN loandat2 ON dbo.loandat.record_id = dbo.loandat2.record_id INNER JOIN  cobo ON loandat.record_id = cobo.parent_id LEFT JOIN 
locks ON loandat.record_id = locks.parent_id LEFT JOIN brokers ON loandat.lt_broker = brokers.record_id LEFT JOIN users ON lt_usr_underwriter = users.record_id LEFT JOIN
proginfo ON loandat.lt_program = proginfo.record_id INNER JOIN lookups ON loandat.lu_purpose = lookups.id_Value and lookups.lookup_id = 'AAE' INNER JOIN 
lookups AS lookupprogram ON loandat.lu_loan_type = lookupprogram.id_value AND lookupprogram.lookup_id = 'AAC' Left JOIN lookups AS lookupsLock ON locks.LU_LOCK_STAT = lookupsLock.id_value AND lookupsLock.lookup_id = 'AAT'
left outer JOIN users as users2 on loandat2.LT_USR_ASSGNDRAWER = users2.record_id left JOIN lookups bustype ON loandat.lu_bus_type = bustype.id_Value and bustype.lookup_id = 'ACD' 
where lt_loan_stats  IN ('Locked','Application Received','Submitted to Underwriting','In Underwriting','Loan Suspended','Approved') AND cobo.primary_rec=1 
and loandat.lu_bus_type in (003,004)
ORDER BY PER desc
END




GO

