USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ServiceBanner]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ServiceBanner]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Created By: Amit Kumar
-- Create Date : 04/09/13
-- Description:
-- =============================================
CREATE procedure [dbo].[sp_ServiceBanner](
@ServiceBannerID int,
@Message varchar(300),
@PublishDate datetime,
@ExpireDate datetime,
@CreatedBy int,
@UpdatedBy int,
@Status char(1),
@Flag VARCHAR(15),
@ReturnValue INT OUTPUT
)
AS
BEGIN
----------------Start(To insert data into tblServiceBanner)----------
if(@Flag='insert')
begin
insert into tblServiceBanner(
Message,
PublishDate,
ExpireDate,CreatedBy,Status)
values(@Message,@PublishDate,@ExpireDate,@CreatedBy,@Status)
end
----------------End(To insert data into tblServiceBanner)----------

----------------Start(To update data into tblServiceBanner)----------
if(@Flag='update')
begin
update tblServiceBanner
set Message=@Message,PublishDate=@PublishDate,
ExpireDate=@ExpireDate,UpdatedBy=@UpdatedBy,Status=@Status

where ServiceBannerID=@ServiceBannerID
END

end
----------------End(To update data into tblServiceBanner)----------

GO

