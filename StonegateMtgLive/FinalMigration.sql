USE [HomeLendingExperts]
								GO
								SET ANSI_NULLS ON
								GO
								SET QUOTED_IDENTIFIER ON
								GO
-- =============================================
-- Author:		Harsh Pandit
-- Create date: 27 June 2011
-- Description:	to stop updates to TMR users who has locked their loans
-- =============================================
CREATE FUNCTION [dbo].[GetTMRCustEmailIDsToStopUpdates] 
(
	 
)
RETURNS @EmployeeList table (userEmail varchar(50))
AS
BEGIN
	
	insert into  @EmployeeList select users.uemailid
	from [INTERLINQE3].[dbo].mwlloanapp as loanapp
	join [INTERLINQE3].[dbo].mwlborrower as borr on loanapp.id = borr.loanapp_id and borr.SequenceNum = 1 
	join tblusers as users on users.uemailid = emailaddress and urole = 9 
	--and TMRRole = 'Consumer' and TMRAccActivation = 1
	where LockStatus='Locked'

	return

END
GO
USE [HomeLendingExperts]
								GO
								SET ANSI_NULLS ON
								GO
								SET QUOTED_IDENTIFIER ON
								GO







-- =============================================
-- Author:		<Vipul Thakkar>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
Create FUNCTION [dbo].[GetNotificationDueDate]
(
--added by vipu thakkar on 27 oct
	-- Add the parameters for the function here
	
)
RETURNS datetime
AS
BEGIN
declare @retstr varchar(10);
declare @retstr1 varchar(10);
declare @retstr2 varchar(10); 
declare @retstr3 varchar(50);  
select  @retstr =  datepart(year, getdate())
select  @retstr1 =  datepart(month, getdate()) 
select  @retstr2 =  15 
select @retstr3= @retstr + '-' + @retstr1 + '-' + @retstr2
--select @retstr as uroleid
--	print @retstr
--print @retstr1
--print @retstr2
--print @retstr3
	RETURN  @retstr3

END

--select datepart(year, getdate()) as ye ,datepart(month, getdate()) as mon , 15 as d






GO
USE [HomeLendingExperts]
								GO
								SET ANSI_NULLS ON
								GO
								SET QUOTED_IDENTIFIER ON
								GO-- =============================================
-- Author: <Author,,Name>
-- Create date: <Create Date,,>
-- Description: <Description,,>
-- =============================================
CREATE FUNCTION [dbo].[SplitString](@String varchar(max), @Delimiter char(1))
returns @temptable TABLE (items varchar(max))
as
begin
declare @idx int
declare @slice varchar(max)

select @idx = 1
if len(@String)<1 or @String is null return

while @idx!= 0
begin
set @idx = charindex(@Delimiter,@String)
if @idx!=0
set @slice = left(@String,@idx - 1)
else
set @slice = @String

if(len(@slice)>0)
insert into @temptable(Items) values(@slice)

set @String = right(@String,len(@String) - @idx)
if len(@String) = 0 break
end
return;
end
  
GO
USE [HomeLendingExperts]
								GO
								SET ANSI_NULLS ON
								GO
								SET QUOTED_IDENTIFIER ON
								GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
create FUNCTION [dbo].[GetFormRole]
(
--added by vipu thakkar on 27 oct
	-- Add the parameters for the function here
	@formid int
)
RETURNS varchar(30)
AS
BEGIN
declare @retstr varchar(8000) 
select  @retstr =  COALESCE(@retstr + ',','') + convert(varchar(10),uroleid) 
from tblFormRoles where formid=@formid
--select @retstr as uroleid
	
	RETURN  @retstr

END
GO
USE [HomeLendingExperts]
								GO
								SET ANSI_NULLS ON
								GO
								SET QUOTED_IDENTIFIER ON
								GO

-- This function is used get the End date of Business Working Days from Current Date to 10 Working Days
-- Created By Paresh Rathod
-- Date: 12/23/2009
-- Purpose: Used in LockExpirationReportQuery
create FUNCTION [dbo].[GetWorkingDays]( ) 
RETURNS DATETIME  
AS  
BEGIN 
    DECLARE @range INT; 
 
    SET @range = 10; 
 
    RETURN  
    ( 
        SELECT		getdate() +
            11 / 7 * 5 + 11 % 7 +  
            ( 
                SELECT COUNT(*)  
            FROM 
                ( 
                    SELECT 1 AS d 
                    UNION ALL SELECT 2  
                    UNION ALL SELECT 3  
                    UNION ALL SELECT 4  
                    UNION ALL SELECT 5  
                    UNION ALL SELECT 6  
                    UNION ALL SELECT 7 
                ) weekdays 
                WHERE d <= 11 % 7  
                AND DATENAME(WEEKDAY, (getdate() + 10) + 1)  
                IN 
                ( 
                    'Saturday', 
                    'Sunday' 
                ) 
            ) 
    ); 
END  

GO
USE [HomeLendingExperts]
								GO
								SET ANSI_NULLS ON
								GO
								SET QUOTED_IDENTIFIER ON
								GO
/*
	Initial form of splittingout csv's . . .may still be used in places
*/

Create function [dbo].[getTableByIDs] (@ids varchar(MAX) )
returns @tempTable table 
(
	val int 
)
as 
 begin

declare @count int
declare @temp varchar(100)
			-- get the list of editors
	SET @Count = CHARINDEX(',', @ids)-- get the position of the first comma (will be found after the first category)

	-- this loop continues until all but one users are in the temporary table
	WHILE @Count > 0
	BEGIN
		SET @Temp = SUBSTRING(@ids, 1, (@Count - 1)) 	-- get the next user id
		INSERT INTO @TempTable VALUES(CAST(@Temp AS int))		-- insert the contract id into a temporary table (each contract id must be unique)
		SET @ids = SUBSTRING(@ids, (@Count + 1), (LEN(@ids) - @Count))	-- remove the contract id from the temporary string
		SET @Count = CHARINDEX(',', @ids)				-- get the position of the first comma (will be found after the first category)
	END

	-- insert the last userid into the temprary table
	if @ids <> ''
	INSERT INTO @TempTable VALUES(CAST(@ids AS int))

	return
 end





GO
USE [HomeLendingExperts]
								GO
								SET ANSI_NULLS ON
								GO
								SET QUOTED_IDENTIFIER ON
								GO-- =============================================
-- Author:		Harsh Pandit
-- Create date: 19 Nov 2010
-- Description:	to stop updates to TMR users who has locked their loans
-- =============================================
CREATE FUNCTION [dbo].[GetTMRCustToStopUpdates] 
(
	 
)
RETURNS @EmployeeList table (userid int)
AS
BEGIN
	
	insert into  @EmployeeList select users.userid
	from [INTERLINQE3].[dbo].mwlloanapp as loanapp
	join [INTERLINQE3].[dbo].mwlborrower as borr on loanapp.id = borr.loanapp_id and borr.SequenceNum = 1 
	join tblusers as users on users.uemailid = emailaddress and urole = 9 and TMRRole = 'Consumer' and TMRAccActivation = 1
	where LockStatus='Locked'

	return

END
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCareerPosting]') AND type in (N'U'))
DROP TABLE [dbo].[tblCareerPosting]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCareerPosting](	  [CareerId] INT NOT NULL IDENTITY(1,1)	, [PositionName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Description] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PublishedDate] DATETIME NULL	, [IsPublished] BIT NULL DEFAULT((0))	, [ModifiedDate] DATETIME NULL DEFAULT(getdate())	, CONSTRAINT [PK_tblCareerPosting] PRIMARY KEY ([CareerId] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblOffices]') AND type in (N'U'))
DROP TABLE [dbo].[tblOffices]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblOffices](	  [recordid] INT NOT NULL IDENTITY(1,1)	, [lt_office_id] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [prouid] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [userid] INT NULL	, [eTeamMemberid] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [E3TeamMemberid] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblOffices] PRIMARY KEY ([recordid] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPublicTestimonial]') AND type in (N'U'))
DROP TABLE [dbo].[tblPublicTestimonial]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPublicTestimonial](	  [ID] INT NOT NULL IDENTITY(1,1)	, [IsPublish] BIT NULL	, [Testimionial] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL	, [CreatedDate] DATETIME NULL DEFAULT(getdate())	, CONSTRAINT [PK_tblTestimonial] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Carrier]') AND type in (N'U'))
DROP TABLE [dbo].[Carrier]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Carrier](	  [ID] INT NOT NULL IDENTITY(1,1)	, [Carrier] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [OrderID] INT NULL	, [CarrierEMailSMS] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblNewsAndAnnouncement]') AND type in (N'U'))
DROP TABLE [dbo].[tblNewsAndAnnouncement]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblNewsAndAnnouncement](	  [ID] INT NOT NULL IDENTITY(1,1)	, [Title] NVARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Body] TEXT(16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL	, [IsNewsAnnouncement] BIT NULL	, [CreatedBy] INT NULL	, [CreatedDate] DATETIME NULL	, [IPAddress] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Role] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [NewsAnnousVisbleAd] BIT NULL	, [NewsAnnousVisbleDR] BIT NULL	, [NewsAnnousVisbleBR] BIT NULL	, [NewsAnnousVisbleLO] BIT NULL	, [NewsAnnousVisbleCU] BIT NULL	, [NewsAnnousVisbleRE] BIT NULL	, [NewsAnnousVisbleAM] BIT NULL	, [NewsAnnousVisbleDM] BIT NULL	, [NewsAnnousVisbleROM] BIT NULL	, [NewsAnnousVisbleCO] BIT NULL	, [NewsAnnousVisbleCRM] BIT NULL	, [NewsAnnousVisbleLP] BIT NULL	, [NewsAnnousVisbleCL] BIT NULL	, [NewsAnnousVisbleRTL] BIT NULL	, [CC_ID] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT((0))	, [SiteStatus] VARCHAR(2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT('RE')	, [RoleC] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RoleR] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblForms]') AND type in (N'U'))
DROP TABLE [dbo].[tblForms]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblForms](	  [formID] INT NOT NULL IDENTITY(1,1)	, [Description] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FileName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [dttime] DATETIME NULL	, [isnews] BIT NULL	, [CateID] INT NULL DEFAULT((0))	, CONSTRAINT [PK_tblForms] PRIMARY KEY ([formID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_tblNewsAndAnnouncement]') AND type in (N'U'))
DROP TABLE [dbo].[_tblNewsAndAnnouncement]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[_tblNewsAndAnnouncement](	  [ID] INT NOT NULL IDENTITY(1,1)	, [Title] NVARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Body] TEXT(16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL	, [IsNewsAnnouncement] BIT NULL	, [CreatedBy] INT NULL	, [CreatedDate] DATETIME NULL	, [IPAddress] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Role] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [NewsAnnousVisbleAd] BIT NULL	, [NewsAnnousVisbleDR] BIT NULL	, [NewsAnnousVisbleBR] BIT NULL	, [NewsAnnousVisbleLO] BIT NULL	, [NewsAnnousVisbleCU] BIT NULL	, [NewsAnnousVisbleRE] BIT NULL	, [NewsAnnousVisbleAM] BIT NULL	, [NewsAnnousVisbleDM] BIT NULL	, [NewsAnnousVisbleROM] BIT NULL	, [NewsAnnousVisbleCO] BIT NULL	, [NewsAnnousVisbleCRM] BIT NULL	, [NewsAnnousVisbleLP] BIT NULL	, [NewsAnnousVisbleCL] BIT NULL	, [NewsAnnousVisbleRTL] BIT NULL	, [CC_ID] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SiteStatus] VARCHAR(2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RoleC] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RoleR] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFaqs]') AND type in (N'U'))
DROP TABLE [dbo].[tblFaqs]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFaqs](	  [ID] BIGINT NOT NULL IDENTITY(1,1)	, [Question] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Answer] TEXT(16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ModuleName] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Published] BIT NOT NULL	, [DisplayOrder] INT NULL	, [CreatedDate] DATETIME NULL	, [ModifiedDate] DATETIME NULL	, CONSTRAINT [PK_tblFaqs] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAdditionalUWDoc]') AND type in (N'U'))
DROP TABLE [dbo].[tblAdditionalUWDoc]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAdditionalUWDoc](	  [ctID] INT NOT NULL IDENTITY(1,1)	, [loan_no] CHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [cond_text] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [FileName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [dttime] DATETIME NULL	, [condrecid] INT NULL	, [isCommited] BIT NULL	, [ProlendImageDesc] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Userid] INT NULL	, [IsActive] BIT NULL	, CONSTRAINT [PK_tblAdditionalUWDoc] PRIMARY KEY ([ctID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFormRoles]') AND type in (N'U'))
DROP TABLE [dbo].[tblFormRoles]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFormRoles](	  [formRoleID] INT NOT NULL IDENTITY(1,1)	, [formID] INT NULL	, [uroleid] INT NULL	, CONSTRAINT [PK_tblFormRoles] PRIMARY KEY ([formRoleID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblScheduleClosing]') AND type in (N'U'))
DROP TABLE [dbo].[tblScheduleClosing]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblScheduleClosing](	  [ScheduleID] INT NOT NULL IDENTITY(1,1)	, [LoanNo] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SchDate] DATETIME NULL	, [FeeSheet] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Userid] INT NULL	, [IsActive] BIT NULL	, [SubmitedDate] DATETIME NULL	, [Comments] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsFromLoanSuspendedDoc] BIT NULL	, CONSTRAINT [PK_tblScheduleClosing] PRIMARY KEY ([ScheduleID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblConsumerAuthor]') AND type in (N'U'))
DROP TABLE [dbo].[tblConsumerAuthor]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblConsumerAuthor](	  [CAID] INT NOT NULL IDENTITY(1,1)	, [AppID] INT NULL	, [IUnderstand] BIT NULL	, [IfApplicable] BIT NULL	, [IAcknowledge] BIT NULL	, [IAuthorise] BIT NULL	, [ApplicantOnly] BIT NULL	, [IHereby] BIT NULL	, [OtherNameUsed] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SSNNo] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [StreetAddress] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DobDay] VARCHAR(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DobMonth] VARCHAR(12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DobYear] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ConCity] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ConState] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ConZip] VARCHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LsnNoState] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LsnNoName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [acceptterm] BIT NULL	, [ConAppName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ConAppDate] DATETIME NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblVoluntarySelfId]') AND type in (N'U'))
DROP TABLE [dbo].[tblVoluntarySelfId]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblVoluntarySelfId](	  [VSIID] INT NOT NULL IDENTITY(1,1)	, [AppId] INT NULL	, [PosApplyingFor] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [HowYouReferred] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Gender] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [HispanicOLatino] BIT NULL	, [White] BIT NULL	, [BlackAfrAmc] BIT NULL	, [Hawiian] BIT NULL	, [Asian] BIT NULL	, [AmericanIndian] BIT NULL	, [TwoOrMoreRaces] BIT NULL	, [DisabledVeteran] BIT NULL	, [VietnamVeteran] BIT NULL	, [OtherVeteran] BIT NULL	, [NewlyVeteran] BIT NULL	, [Veteran] BIT NULL	, [NotApplicable] BIT NULL	, [OtherStatus] BIT NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblEmpWorkExp]') AND type in (N'U'))
DROP TABLE [dbo].[tblEmpWorkExp]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmpWorkExp](	  [workid] INT NOT NULL IDENTITY(1,1)	, [jobid] INT NULL	, [CompanyName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [coAddress] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [coPhone] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FromDate] DATETIME NULL	, [Todate] DATETIME NULL	, [RateOfPay] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PositionHeld] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Supervisor] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [WorkPerformed] VARCHAR(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LeaveReason] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanApplicationOld]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanApplicationOld]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanApplicationOld](	  [ID] INT NOT NULL IDENTITY(1,1)	, [BrokarID] INT NULL	, [Email] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Description] NVARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Comments] NVARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FileName] NVARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DateEntered] DATETIME NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblEmployementApplication]') AND type in (N'U'))
DROP TABLE [dbo].[tblEmployementApplication]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmployementApplication](	  [ApplicationID] INT NOT NULL IDENTITY(1,1)	, [FirstName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MiddleName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LastName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [State] VARCHAR(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [City] VARCHAR(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [StreetAddress] NVARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Zip] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PhoneNo] NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SocialSecNo] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Adult] BIT NULL	, [ProofLegallyEntitled] BIT NULL	, [SGEmployed] BIT NULL	, [EmployedDesc] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SGRelativeEmployed] BIT NULL	, [RelativesDesc] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ConvictedOfCrime] BIT NULL	, [CrimeDesc] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DismissedOrResign] BIT NULL	, [DismissalDesc] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PosApplied] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Imseeking] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Imavailable] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DateAvailable] DATETIME NULL	, [TempDatesAvailable] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [HoursPerweek] VARCHAR(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [HighSchool] VARCHAR(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [TechSchool] VARCHAR(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Collage] VARCHAR(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Certification] VARCHAR(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [NMLSLicense] BIT NULL	, [DeniedLicense] BIT NULL	, [msofficeAvaiblityLevel] VARCHAR(12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AligiblityOfWork] BIT NULL	, [OtherName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LicenseNo] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [NameOnLicense] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CACrimeDesc] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [VSPositionApplied] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RefferalToSG] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FirstProRefName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FirstProRefPhone] NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FirstProRefAddr] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SecondProRefName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SecondProRefPhone] NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SecondProRefAddr] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ThirdProRefName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ThirdProRefPhone] NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ThirdProRefAddr] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FirstPerRefName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FirstPerRefPhone] NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FirstPerRefAddr] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SecondPerRefName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SecondPerRefPhone] NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SecondPerRefAddr] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ThirdPerRefName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ThirdPerRefPhone] NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ThirdPerRefAddr] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ApplicantName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppliedDate] DATETIME NULL	, [CurrentEmployer] BIT NULL	, [ContactEmployer] BIT NULL	, [spSkills] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FileName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedDate] DATETIME NULL	, [CareerId] INT NULL	, CONSTRAINT [PK_tblEmployementApplication] PRIMARY KEY ([ApplicationID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblState]') AND type in (N'U'))
DROP TABLE [dbo].[tblState]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblState](	  [state_id] INT NOT NULL	, [name] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [abbr] CHAR(2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, CONSTRAINT [PK_STATES] PRIMARY KEY ([state_id] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblEmail]') AND type in (N'U'))
DROP TABLE [dbo].[tblEmail]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmail](	  [ID] INT NOT NULL IDENTITY(1,1)	, [Role] INT NULL	, [Subject] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EmailBodyText] TEXT(16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedBy] INT NULL	, [CreatedDate] DATETIME NULL	, [ModifiedBy] INT NULL	, [ModifiedDate] DATETIME NULL	, [IsSent] BIT NULL	, [SentDateTime] DATETIME NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblEmailServiceReader]') AND type in (N'U'))
DROP TABLE [dbo].[tblEmailServiceReader]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmailServiceReader](	  [Id] BIGINT NOT NULL IDENTITY(1,1)	, [ToName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FromAddress] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ToAddress] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CcAddress] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [mailSubject] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [mailBody] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MobileEmailBody] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [mailAttachment] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RcvdDate] DATETIME NULL DEFAULT(getdate())	, [SendBy] INT NULL	, [MobileEmailId] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [updStatus] INT NULL DEFAULT((0))	, CONSTRAINT [PK_tblDSEmailHistory] PRIMARY KEY ([Id] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanDocs]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanDocs]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanDocs](	  [ctID] INT NOT NULL IDENTITY(1,1)	, [loan_no] CHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL DEFAULT(' ')	, [docs_text] TEXT(16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL DEFAULT(' ')	, [FileName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [dttime] DATETIME NULL	, [userid] INT NULL	, [Assigned] BIT NULL	, [AssignedBy] BIGINT NULL	, [AssignDate] DATETIME NULL	, [Completed] BIT NULL	, [CompletedBy] BIGINT NULL	, [TimeStamp] DATETIME NULL	, CONSTRAINT [PK_tblLoanDocs] PRIMARY KEY ([ctID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanApplication]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanApplication]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanApplication](	  [LoanApplicationID] INT NOT NULL IDENTITY(1,1)	, [LoanReasonID] INT NULL	, [HometypeID] INT NULL	, [PropertyTypeID] INT NULL	, [Producttypeid] INT NULL	, [StateID] INT NULL	, [CreditScoreID] INT NULL	, [AnnulIncomeBorrower] MONEY NULL	, [AssetBorrower] MONEY NULL	, [RetirementAssetsBorrower] MONEY NULL	, [PropertyLocated] BIT NULL	, [HomeOwned] BIT NULL	, [PurchasePrise] MONEY NULL	, [DownPaymentAmt] MONEY NULL	, [PartOfTotalAssets] BIT NULL	, [DownPaymentSourceID] INT NULL	, [HaveRealtor] BIT NULL	, [RealtorContactName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RealtorPhone] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RealtorEmail] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppFName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppLName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppStateID] INT NULL	, [AppZip] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppPrimaryPhone] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [AppSecondaryPhone] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppEmail] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ContactTimeID] INT NULL	, [CurrentPropertyValue] MONEY NULL	, [PropertyPurchaseMonth] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [YearID] INT NULL	, [ExistingPuchasePrice] MONEY NULL	, [CurrentMortageBal] MONEY NULL	, [MonthlyPayment] MONEY NULL	, [SecondMortage] BIT NULL	, [SecondMortageBal] MONEY NULL	, [DateEntered] DATETIME NULL	, [ModifiedBy] INT NULL	, [ModifiedDate] DATETIME NULL	, [UserAppearUrl] NVARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [vCity] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [bSignAgg] BIT NULL	, [dSchClosingDate] DATETIME NULL	, [vEstRenovationAmt] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [vDownPayAmount] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [vAddressLine1] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [vAddressLine2] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsFullApp] BIT NULL	, [ApplyType] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsLead360] BIT NULL	, [CompIP] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MAName] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AssignedLO] VARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsWorkingWithRealtor] BIT NULL	, [PropertyRealtorCity] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsCashback] BIT NULL	, [RealtorName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RealtorPhoneNo] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LoanAmount] MONEY NULL	, CONSTRAINT [PK_tblLoanApplication] PRIMARY KEY ([LoanApplicationID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUserManagers]') AND type in (N'U'))
DROP TABLE [dbo].[tblUserManagers]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserManagers](	  [UserManagerId] INT NOT NULL IDENTITY(1,1)	, [UserId] INT NOT NULL	, [ManagerId] INT NOT NULL	, [CreatedBy] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [CreatedOn] DATETIME NOT NULL	, CONSTRAINT [PK_tblUserManagers] PRIMARY KEY ([UserManagerId] ASC))ALTER TABLE [dbo].[tblUserManagers] WITH CHECK ADD CONSTRAINT [FK_tblUserManagers_tblManagers] FOREIGN KEY([ManagerId]) REFERENCES [dbo].[tblUsers] ([userid])ALTER TABLE [dbo].[tblUserManagers] CHECK CONSTRAINT [FK_tblUserManagers_tblManagers]ALTER TABLE [dbo].[tblUserManagers] WITH CHECK ADD CONSTRAINT [FK_tblUserManagers_tblUsers] FOREIGN KEY([UserId]) REFERENCES [dbo].[tblUsers] ([userid])ALTER TABLE [dbo].[tblUserManagers] CHECK CONSTRAINT [FK_tblUserManagers_tblUsers]USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanApply]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanApply]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanApply](	  [LoanApplicationID] INT NOT NULL IDENTITY(1,1)	, [LoanReasonID] INT NULL	, [HometypeID] INT NULL	, [PropertyTypeID] INT NULL	, [Producttypeid] INT NULL	, [StateID] INT NULL	, [CreditScoreID] INT NULL	, [AnnulIncomeBorrower] MONEY NULL	, [AssetBorrower] MONEY NULL	, [RetirementAssetsBorrower] MONEY NULL	, [PropertyLocated] BIT NULL	, [HomeOwned] BIT NULL	, [PurchasePrise] MONEY NULL	, [DownPaymentAmt] MONEY NULL	, [PartOfTotalAssets] BIT NULL	, [DownPaymentSourceID] INT NULL	, [HaveRealtor] BIT NULL	, [RealtorContactName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RealtorPhone] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RealtorEmail] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppFName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppLName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppStateID] INT NULL	, [AppZip] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppPrimaryPhone] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [AppSecondaryPhone] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppEmail] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ContactTimeID] INT NULL	, [CurrentPropertyValue] MONEY NULL	, [PropertyPurchaseMonth] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [YearID] INT NULL	, [ExistingPuchasePrice] MONEY NULL	, [CurrentMortageBal] MONEY NULL	, [MonthlyPayment] MONEY NULL	, [SecondMortage] BIT NULL	, [SecondMortageBal] MONEY NULL	, [DateEntered] DATETIME NULL DEFAULT(getdate())	, [ModifiedBy] INT NULL	, [ModifiedDate] DATETIME NULL	, [UserAppearUrl] NVARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblContactTime]') AND type in (N'U'))
DROP TABLE [dbo].[tblContactTime]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblContactTime](	  [ContactTimeID] INT NOT NULL IDENTITY(1,1)	, [ContactTime] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCreditScore]') AND type in (N'U'))
DROP TABLE [dbo].[tblCreditScore]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCreditScore](	  [ID] INT NOT NULL IDENTITY(1,1)	, [CreditScoreID] INT NULL	, [CreditScoreValue] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreditOrder] INT NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDownPaymentSource]') AND type in (N'U'))
DROP TABLE [dbo].[tblDownPaymentSource]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDownPaymentSource](	  [DownPaymentSourceID] INT NOT NULL IDENTITY(1,1)	, [SourceOfdownPayment] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblHomeType]') AND type in (N'U'))
DROP TABLE [dbo].[tblHomeType]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblHomeType](	  [HometypeID] INT NOT NULL IDENTITY(1,1)	, [Hometype] NVARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblServiceBanner]') AND type in (N'U'))
DROP TABLE [dbo].[tblServiceBanner]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblServiceBanner](	  [ServiceBannerID] INT NOT NULL IDENTITY(1,1)	, [Message] VARCHAR(300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [PublishDate] DATETIME NOT NULL	, [ExpireDate] DATETIME NOT NULL	, [CreatedDate] DATETIME NULL	, [UpdatedDate] DATETIME NULL	, [Status] CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedBy] INT NULL	, [UpdatedBy] INT NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanReason]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanReason]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanReason](	  [LoanReasonID] INT NOT NULL IDENTITY(1,1)	, [LoanReason] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFormsHomeowner]') AND type in (N'U'))
DROP TABLE [dbo].[tblFormsHomeowner]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFormsHomeowner](	  [formid] INT NOT NULL IDENTITY(1,1)	, [Description] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FileName] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Loantype] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Packageid] INT NULL	, [dttime] DATETIME NULL	, CONSTRAINT [PK_tblFormsHomeowner] PRIMARY KEY ([formid] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblProductType]') AND type in (N'U'))
DROP TABLE [dbo].[tblProductType]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblProductType](	  [ID] INT NOT NULL IDENTITY(1,1)	, [producttypeid] INT NULL	, [name] NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [term] INT NULL	, [isarm] INT NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPropertyType]') AND type in (N'U'))
DROP TABLE [dbo].[tblPropertyType]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPropertyType](	  [ID] INT NOT NULL IDENTITY(1,1)	, [PropertyTypeID] INT NOT NULL	, [Name] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanType]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanType]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanType](	  [ID] INT NOT NULL IDENTITY(1,1)	, [LoanTypeID] INT NULL	, [LoanType] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblLoanType] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblStateNylex]') AND type in (N'U'))
DROP TABLE [dbo].[tblStateNylex]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblStateNylex](	  [ID] INT NOT NULL IDENTITY(1,1)	, [StateID] INT NULL	, [Abbreviature] NVARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [StateName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [StateOrder] INT NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblYear]') AND type in (N'U'))
DROP TABLE [dbo].[tblYear]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblYear](	  [YearID] INT NOT NULL IDENTITY(1,1)	, [Year] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPackageTypeId]') AND type in (N'U'))
DROP TABLE [dbo].[tblPackageTypeId]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPackageTypeId](	  [Id] INT NOT NULL IDENTITY(1,1)	, [PackageId] INT NULL	, CONSTRAINT [PK_tblPackageTypeId] PRIMARY KEY ([Id] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCategories]') AND type in (N'U'))
DROP TABLE [dbo].[tblCategories]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCategories](	  [CatID] INT NOT NULL IDENTITY(1,1)	, [CateName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT(NULL))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblHolidayList]') AND type in (N'U'))
DROP TABLE [dbo].[tblHolidayList]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblHolidayList](	  [ID] BIGINT NOT NULL IDENTITY(1,1)	, [HolidayName] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [HolidayDate] DATETIME NULL	, [HolidayType] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Status] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblHolidayList] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblloandoc]') AND type in (N'U'))
DROP TABLE [dbo].[tblloandoc]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblloandoc](	  [loanDocID] INT NOT NULL IDENTITY(1,1)	, [uloanregID] INT NULL DEFAULT((0))	, [DocName] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Comments] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Assigned] BIT NULL	, [AssignedBy] BIGINT NULL	, [AssignDate] DATETIME NULL	, [Completed] BIT NULL	, [CompletedBy] BIGINT NULL	, [TimeStamp] DATETIME NULL	, [userid] INT NULL	, [createddate] DATETIME NULL	, [loan_no] CHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL DEFAULT('True')	, [IsDeletedFromDocLog] BIT NULL DEFAULT('True')	, CONSTRAINT [PK_tblloandoc] PRIMARY KEY ([loanDocID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanReg]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanReg]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanReg](	  [loanregID] INT NOT NULL IDENTITY(1,1)	, [borrLast] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [transType] INT NULL	, [loanAmt] NUMERIC NULL	, [lockStatus] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [loanPgm] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [estCloseDate] DATETIME NULL	, [userid] INT NULL	, [createdDate] DATETIME NULL DEFAULT(getdate())	, [filetype] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [filename] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Assigned] BIT NULL	, [AssignedBy] BIGINT NULL	, [AssignDate] DATETIME NULL	, [Completed] BIT NULL	, [CompletedBy] BIGINT NULL	, [TimeStamp] DATETIME NULL	, [IsActive] BIT NULL	, [IsDeletedFromDocLog] BIT NULL	, [LoanNumber] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblLoanReg] PRIMARY KEY ([loanregID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbllookups]') AND type in (N'U'))
DROP TABLE [dbo].[tbllookups]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbllookups](	  [lookupid] BIGINT NOT NULL IDENTITY(1,1)	, [Lookupname] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Description] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tbllookups] PRIMARY KEY ([lookupid] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTestimonials]') AND type in (N'U'))
DROP TABLE [dbo].[tblTestimonials]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTestimonials](	  [TestimonialID] BIGINT NOT NULL IDENTITY(1,1)	, [UName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [City] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [State] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [TestimonialType] CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL DEFAULT((0))	, [Testimonial] TEXT(16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [SubmittedDate] DATETIME NULL	, [UserID] INT NOT NULL	, [UpdatedDt] DATETIME NULL	, CONSTRAINT [PK_tblTestimonials] PRIMARY KEY ([TestimonialID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBranchOffices]') AND type in (N'U'))
DROP TABLE [dbo].[tblBranchOffices]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBranchOffices](	  [id] INT NULL	, [OfficeName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [OfficeValue] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Street] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [State] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [City] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Zip] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Phone] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Fax] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Email] VARCHAR(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [GroupId] INT NULL	, [lat1] DECIMAL(18,6) NULL	, [lng1] DECIMAL(18,6) NULL	, [region] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Branch] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblNewRates]') AND type in (N'U'))
DROP TABLE [dbo].[tblNewRates]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblNewRates](	  [NewRateID] INT NOT NULL IDENTITY(1,1)	, [strLoanType] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LoanProgram] VARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [GoalOfLoan] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IPoints] INT NULL	, [DecRate] DECIMAL(18,3) NULL	, [DecAPR] DECIMAL(18,3) NULL	, [DecClosingCost] DECIMAL(18,2) NULL	, [CreatedDate] DATETIME NULL	, [ModifiedDate] DATETIME NULL DEFAULT(getdate())	, [IsActive] BIT NULL DEFAULT((1))	, [IRowOrder] INT NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRates]') AND type in (N'U'))
DROP TABLE [dbo].[tblRates]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRates](	  [RateID] INT NOT NULL IDENTITY(1,1)	, [strLoanType] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IPoints] INT NULL	, [DecRate] DECIMAL(18,3) NULL	, [DecAPR] DECIMAL(18,3) NULL	, [DecClosingCost] DECIMAL(18,2) NULL	, [CreatedDate] DATETIME NULL	, [ModifiedDate] DATETIME NULL DEFAULT(getdate())	, [IsActive] BIT NULL DEFAULT((1))	, [IRowOrder] INT NULL	, CONSTRAINT [PK_tblRates] PRIMARY KEY ([RateID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRSVPEdits]') AND type in (N'U'))
DROP TABLE [dbo].[tblRSVPEdits]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRSVPEdits](	  [EventID] INT NOT NULL IDENTITY(1,1)	, [EventName] NVARCHAR(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Header] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Contents] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EventDay] NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EventDate] DATETIME NULL	, [EventTimeFrom] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EventTimeTo] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Timezone] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Location] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EvantAdd1] NVARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EventAdd2] NVARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EventCity] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EventState] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EventZip] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EventWhereLink] NVARCHAR(3900) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EventDetailText] NVARCHAR(3900) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EventDetailLink] NVARCHAR(3900) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [TwitterLink] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FBLink] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LinkedInLink] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EventLogo] NVARCHAR(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [EventPDF] NVARCHAR(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FooterAdd1] NVARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FooterAdd2] NVARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FooterCity] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FooterState] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FooterZip] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SkinColor] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedBy] INT NULL	, [CreatedDate] DATETIME NULL	, [ModifiedBy] INT NULL	, [ModifiedDate] DATETIME NULL	, [Status] INT NULL	, [RsvpHeader] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [UniqueUrl] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblRSVPEdits] PRIMARY KEY ([EventID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblServiceInfo]') AND type in (N'U'))
DROP TABLE [dbo].[tblServiceInfo]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblServiceInfo](	  [record_id] INT NOT NULL IDENTITY(1,1)	, [UserId] INT NOT NULL	, [LoanNo] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [IsActive] BIT NULL	, [IsSMSPaymentReminderAlert] BIT NULL	, [IsSMSPaymentPostedAlert] BIT NULL	, [IsEmailPaymentReminderAlert] BIT NULL	, [IsEmailPaymentPostedAlert] BIT NULL	, [IsPaymentReminderAlert] BIT NULL	, [IsPaymentPostedReminderAlert] BIT NULL	, [PaymentReminderDay] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PaymentPostedReminderDay] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PaymentReminderdate] DATETIME NULL	, [PaymentPostedReminderdate] DATETIME NULL	, [SMSPaymentReminderdate] DATETIME NULL	, [SMSPaymentPostedReminderdate] DATETIME NULL	, [IsSMSPaymentPastDue] BIT NULL DEFAULT((0))	, [IsEmailPaymentPastDue] BIT NULL DEFAULT((0))	, [IsPaymentPastDue] BIT NULL DEFAULT((0))	, [PaymentPastDueReminderDay] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PaymentPastDueDate] DATETIME NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[E3KatalystBranchOffice]') AND type in (N'U'))
DROP TABLE [dbo].[E3KatalystBranchOffice]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[E3KatalystBranchOffice](	  [ID] INT NOT NULL	, [E3BranchOfficeID] VARCHAR(32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [E3BranchOffice] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsKatalyst] BIT NULL	, CONSTRAINT [PK_E3KatalystBranchOffice] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPayoffStatement]') AND type in (N'U'))
DROP TABLE [dbo].[tblPayoffStatement]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPayoffStatement](	  [record_id] INT NOT NULL IDENTITY(1,1)	, [userid] INT NULL	, [PayoffDate] DATETIME NULL	, [PayoffReasonId] INT NULL	, [IsReadyToContact] BIT NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblHipEvent]') AND type in (N'U'))
DROP TABLE [dbo].[tblHipEvent]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblHipEvent](	  [id] INT NOT NULL IDENTITY(1,1)	, [HipEventDate] DATETIME NULL	, [HipEventName] VARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FirstName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LastName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [StreetAddr] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [City] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [State] VARCHAR(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ZipCode] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Email] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Company] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [attended] BIT NULL	, [location] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CurrentPartner] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreatedDate] DATETIME NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblColts]') AND type in (N'U'))
DROP TABLE [dbo].[tblColts]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblColts](	  [id] INT NOT NULL IDENTITY(1,1)	, [name] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [lastname] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [streetaddr] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [city] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [state] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [zipcode] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [phoneno] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [email] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [confirmemail] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [scoutprogram] CHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [colts] CHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [newspaper] CHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [friend] CHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [other] CHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [representative] CHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTracking]') AND type in (N'U'))
DROP TABLE [dbo].[tblTracking]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTracking](	  [Id] BIGINT NOT NULL IDENTITY(1,1)	, [UserID] INT NOT NULL	, [LoginTime] DATETIME NOT NULL	, [LogoutTime] DATETIME NULL	, [IPAddress] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblTracking] PRIMARY KEY ([Id] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblEmailReader]') AND type in (N'U'))
DROP TABLE [dbo].[tblEmailReader]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmailReader](	  [Id] BIGINT NOT NULL IDENTITY(1,1)	, [ToName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FromAddress] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ToAddress] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CcAddress] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [mailSubject] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [mailBody] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MobileEmailBody] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [mailAttachment] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RcvdDate] DATETIME NULL DEFAULT(getdate())	, [SendBy] INT NULL	, [MobileEmailId] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [updStatus] INT NULL DEFAULT((0)))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblProperties]') AND type in (N'U'))
DROP TABLE [dbo].[tblProperties]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblProperties](	  [PropID] INT NOT NULL IDENTITY(1,1)	, [RealtorID] INT NULL	, [PropStatus] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ListingTYpe] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [BedRooms] INT NULL	, [Baths] DECIMAL(18,1) NULL	, [Price] DECIMAL(18,2) NULL	, [Street] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [State] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [City] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Zipcode] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [YearBuilt] INT NULL	, [IsActive] BIT NULL DEFAULT((1))	, [PropDesc] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [lat] DECIMAL(18,6) NULL	, [lng] DECIMAL(18,6) NULL	, [Area] INT NULL	, [CreatedDate] DATETIME NULL DEFAULT(getdate())	, [Amount] DECIMAL(18,0) NULL	, [ImprovementFile] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DaysListed] INT NULL	, CONSTRAINT [PK_tblProperties] PRIMARY KEY ([PropID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRequestPayoff]') AND type in (N'U'))
DROP TABLE [dbo].[tblRequestPayoff]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRequestPayoff](	  [Requestid] INT NOT NULL IDENTITY(1,1)	, [Loannumber] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RequestDate] DATETIME NULL	, [EmailID] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [NeedtoKnow] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPropertyImage]') AND type in (N'U'))
DROP TABLE [dbo].[tblPropertyImage]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPropertyImage](	  [PropImageID] INT NOT NULL IDENTITY(1,1)	, [PropID] INT NULL	, [PropImageName] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PropDesc] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsPropImageDel] BIT NULL DEFAULT((1))	, CONSTRAINT [PK_tblPropertyImage] PRIMARY KEY ([PropImageID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRoles]') AND type in (N'U'))
DROP TABLE [dbo].[tblRoles]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRoles](	  [ID] INT NULL	, [Roles] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL DEFAULT((1)))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTabs]') AND type in (N'U'))
DROP TABLE [dbo].[tblTabs]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTabs](	  [TabID] INT NOT NULL IDENTITY(1,1)	, [TabName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ControlName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [TabLink] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblTabs] PRIMARY KEY ([TabID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRoleSetup]') AND type in (N'U'))
DROP TABLE [dbo].[tblRoleSetup]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRoleSetup](	  [RoleSetupID] INT NOT NULL IDENTITY(1,1)	, [RoleID] INT NULL	, [TabID] INT NULL	, [TabOrder] INT NULL	, [isActive] BIT NULL	, CONSTRAINT [PK_tblRoleSetup] PRIMARY KEY ([RoleSetupID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUsers]') AND type in (N'U'))
DROP TABLE [dbo].[tblUsers]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUsers](	  [userid] INT NOT NULL IDENTITY(1,1)	, [ufirstname] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ulastname] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [uemailid] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [urole] INT NULL	, [isadmin] BIT NULL	, [isactive] BIT NULL	, [uidprolender] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [uparent] INT NULL	, [upassword] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [maxloginattempt] INT NULL	, [Lastlogintime] DATETIME NULL	, [mobile_ph] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [loginidprolender] NVARCHAR(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [carrierid] INT NULL	, [loannum] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [address] NVARCHAR(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [city] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [state] NVARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [zip] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [phonenum] NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [propertyaddr] NVARCHAR(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Address2] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [experience] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [fax] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [photoname] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [logoname] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FaceBookID] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [TwitterID] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LoanOfficer] INT NULL	, [bio] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AreaOfExpertise] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [BranchID] INT NULL	, [MAID] INT NULL DEFAULT((0))	, [lead360] VARCHAR(4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MI] VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ClientTestimonial] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RealtorTestimonial] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [TeamName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [TermsRealtor] BIT NULL DEFAULT((0))	, [lat] DECIMAL(18,6) NULL	, [lng] DECIMAL(18,6) NULL	, [StateBelongTo] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SignedUpDate] DATETIME NULL DEFAULT(getdate())	, [RcvOnCell] BIT NULL	, [RcvOnEmail] BIT NULL	, [RcvExpire30] BIT NULL	, [TMRAccActivation] BIT NULL	, [TMRUserName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [TMRRole] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RealtorGroupID] INT NULL	, [RealtorID] INT NULL	, [TMRLoanType] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [TMRTimeFrame] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PropState] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [zlat] DECIMAL(18,6) NULL	, [zlng] DECIMAL(18,6) NULL	, [E3Userid] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [compName] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [shorturl] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Office] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [BranchOffice] VARCHAR(900) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MyBranches] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [siteurl] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ispublish] BIT NULL	, [Region] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Userloginid] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PasswordExpDate] DATETIME NULL	, [LongFormAppurl] NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MCEOLeadOwnerId] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblUsers] PRIMARY KEY ([userid] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TblRealtyGroup]') AND type in (N'U'))
DROP TABLE [dbo].[TblRealtyGroup]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblRealtyGroup](	  [GroupId] BIGINT NOT NULL IDENTITY(1,1)	, [GroupName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblOrbipayHistory]') AND type in (N'U'))
DROP TABLE [dbo].[tblOrbipayHistory]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblOrbipayHistory](	  [id] INT NOT NULL IDENTITY(1,1)	, [custid] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [message] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [errorcode] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [errdate] DATETIME NULL DEFAULT(getdate()))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblzipcodes]') AND type in (N'U'))
DROP TABLE [dbo].[tblzipcodes]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblzipcodes](	  [ZIP Code] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [City] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [abbr] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRegion]') AND type in (N'U'))
DROP TABLE [dbo].[tblRegion]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRegion](	  [Id] INT NOT NULL IDENTITY(1,1)	, [RegionName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [RegionValue] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [IsActive] BIT NULL	, CONSTRAINT [PK_tblRegion] PRIMARY KEY ([Id] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTempUser]') AND type in (N'U'))
DROP TABLE [dbo].[tblTempUser]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTempUser](	  [tempid] INT NOT NULL IDENTITY(1,1)	, [refuserid] INT NULL	, [tempuserid] VARCHAR(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [temppswd] VARCHAR(150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [createddate] DATETIME NULL	, [IsActive] CHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT((1)))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCondText_History]') AND type in (N'U'))
DROP TABLE [dbo].[tblCondText_History]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCondText_History](	  [ctID] INT NOT NULL IDENTITY(1,1)	, [loan_no] CHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL DEFAULT(' ')	, [cond_text] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL DEFAULT(' ')	, [FileName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [dttime] DATETIME NULL	, [condrecid] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [isCommited] BIT NULL DEFAULT((0))	, [ProlendImageDesc] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Userid] INT NULL	, [IsActive] BIT NULL DEFAULT('True')	, [SubmittedDate] DATETIME NULL DEFAULT(getdate())	, CONSTRAINT [PK_tblCondText_History] PRIMARY KEY ([ctID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUserDetails]') AND type in (N'U'))
DROP TABLE [dbo].[tblUserDetails]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserDetails](	  [UserDetailsID] INT NOT NULL IDENTITY(1,1)	, [UsersID] INT NULL	, [ChangeUsername] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SecurityQuestion1] INT NULL	, [Answer1] VARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SecurityQuestion2] INT NULL	, [Answer2] VARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SecurityQuestion3] INT NULL	, [Answer3] VARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MaxSecurityAttempt] INT NULL	, [lastlogintime] DATETIME NULL	, [MaxUserIdSecurityAtmp] INT NULL	, [UserIdlastlogintime] DATETIME NULL	, CONSTRAINT [PK_tblUserDetails] PRIMARY KEY ([UserDetailsID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSecurityQuestions]') AND type in (N'U'))
DROP TABLE [dbo].[tblSecurityQuestions]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSecurityQuestions](	  [QuestionID] INT NOT NULL IDENTITY(1,1)	, [SecurityQuestion] VARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DisplayAt] INT NULL	, CONSTRAINT [PK_tblSecurityQuestions] PRIMARY KEY ([QuestionID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUserPasswordHistory]') AND type in (N'U'))
DROP TABLE [dbo].[tblUserPasswordHistory]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserPasswordHistory](	  [PasswordID] INT NOT NULL IDENTITY(1,1)	, [UserID] INT NULL	, [Password] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblUserPasswordHistory] PRIMARY KEY ([PasswordID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDbQuery]') AND type in (N'U'))
DROP TABLE [dbo].[tblDbQuery]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDbQuery](	  [Queryid] INT NOT NULL IDENTITY(1,1)	, [MethodName] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [Query] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [DB] VARCHAR(2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblQuickLinks]') AND type in (N'U'))
DROP TABLE [dbo].[tblQuickLinks]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblQuickLinks](	  [ID] INT NOT NULL	, [Linkname] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Description] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Roles] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblQuickLinks] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUserGuide]') AND type in (N'U'))
DROP TABLE [dbo].[tblUserGuide]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserGuide](	  [ID] INT NOT NULL IDENTITY(1,1)	, [Filename] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL	, [CreatedBy] INT NULL	, [CreatedDate] DATETIME NULL	, CONSTRAINT [PK_tblUserGuide] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblzipcodes_old]') AND type in (N'U'))
DROP TABLE [dbo].[tblzipcodes_old]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblzipcodes_old](	  [ZIP Code] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [City] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [abbr] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_spUpdateExistingMail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[_spUpdateExistingMail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[_spUpdateExistingMail]
	-- Add the parameters for the stored procedure here
	(
	@Name nvarchar(100),
	@Email nvarchar(100),

	@Comments nvarchar(1000)	
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Subscribe 
set IsActive = 1,Name=@Name,Comments=@Comments, UpdatedDate=GetDate()Where EMail = @Email 
               
              
                 
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_UserReportees]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Get_UserReportees]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Get_UserReportees]
(
    @managerid varchar(MAX)
  --, @branchid varchar(MAX)
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	;WITH MyCTE
	AS ( 
			SELECT u1.userid,u1.ufirstname + SPACE(1) +  u1.ulastname  UserName , m1.managerid, m1.userid,1 [Level],r1.id [role],u1.isactive,u1.E3userid,u1.office [Branch]
			FROM tblusermanagers m1 
			INNER JOIN  tblusers u1 on u1.userid = m1.userid 
				and m1.managerid in (SELECT * FROM dbo.splitstring(@managerid,','))
				--and u1.office in (SELECT * FROM dbo.splitstring(@branchid,','))
			INNER JOIN tblroles r1 on u1.urole = r1.id 
			UNION ALL 

			SELECT u2.userid,u2.ufirstname + SPACE(1) + u2.ulastname  UserName , m2.managerid, m2.userid , MyCTE.[Level]+1 [Level],r2.id [role],u2.isactive,u2.E3userid,u2.office [Branch]
			FROM tblusermanagers m2 
			INNER JOIN  tblusers u2 on u2.userid = m2.userid
				--and u2.office in (SELECT * FROM dbo.splitstring(@branchid,','))
			INNER JOIN MyCTE on m2.managerid = MyCTE.userid
			INNER JOIN tblroles r2 on u2.urole = r2.id 
			WHERE  m2.managerid NOT IN (SELECT * FROM dbo.splitstring(@managerid,','))
		)SELECT * FROM MyCTE


		;WITH CTEBRANCH
		AS ( 
			SELECT u1.userid,u1.ufirstname + SPACE(1) +  u1.ulastname  UserName , m1.managerid, m1.userid,1 [Level],r1.id [role],u1.isactive,u1.E3userid,u1.office [Branch]
			FROM tblusermanagers m1 
			INNER JOIN  tblusers u1 on u1.userid = m1.userid 
				and m1.managerid in (SELECT * FROM dbo.splitstring(@managerid,','))
				--and u1.office in (SELECT * FROM dbo.splitstring(@branchid,','))
			INNER JOIN tblroles r1 on u1.urole = r1.id where u1.isactive=1
			UNION ALL 

			SELECT u2.userid,u2.ufirstname + SPACE(1) + u2.ulastname  UserName , m2.managerid, m2.userid , CTEBRANCH.[Level]+1 [Level],r2.id [role],u2.isactive,u2.E3userid,u2.office [Branch]
			FROM tblusermanagers m2 
			INNER JOIN  tblusers u2 on u2.userid = m2.userid
				--and u2.office in (SELECT * FROM dbo.splitstring(@branchid,','))
			INNER JOIN CTEBRANCH on m2.managerid = CTEBRANCH.userid
			INNER JOIN tblroles r2 on u2.urole = r2.id 
			WHERE  m2.managerid NOT IN (SELECT * FROM dbo.splitstring(@managerid,',')) and u2.isactive =1
			)SELECT * FROM CTEBRANCH
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAMBDMEmailForKatalyst]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetAMBDMEmailForKatalyst]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Shwetal,,Shah>
-- Create date: <03 March 2013>
-- Description:	<This Stored procedure used for Get BDM information from Website DB>
---Modify By: <Vipul, Thacker>
-- Modify date: <04 April 2013>
-- =============================================
CREATE PROCEDURE [dbo].[GetAMBDMEmailForKatalyst]
(
	@UserID int = 0
)
AS
BEGIN
SELECT BDM.uemailid AS BDMEmail,tblAM.uemailid AS AMEmailID, tblBroker.MAID as  AEID FROM tblusers tblBroker WITH(NOLOCK)
	LEFT JOIN dbo.tblUsers  BDM WITH(NOLOCK) ON tblBroker.MAID = BDM.userid
	LEFT JOIN dbo.tblUsers  tblAM WITH(NOLOCK) ON tblAM.region= BDM.Region AND tblAM.urole = 8
	Where tblBroker.userid = @UserID
	
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBranchesForKatalyst]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetBranchesForKatalyst]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Shwetal Shah>
-- Create date: <10-04-13>
-- Description:	<To Get list of Branches from E3 to on / off Katalyst functionality>
-- =============================================
CREATE PROCEDURE [dbo].[GetBranchesForKatalyst]

AS
BEGIN

	SELECT ID as BranchID,Office,(Select Name from tblState WITH ( NOLOCK ) where abbr = mwcInstitution.State)+' - '+State as States 
	,Case When (Select ISNULL(IsKatalyst,0) From E3KatalystBranchoffice  WITH ( NOLOCK ) Where E3BranchOfficeID = dbo.mwcInstitution.ID) = 1 Then 'Allowed' Else 'Not Allowed' End as IsKatalyst
	FROM dbo.mwcInstitution WITH ( NOLOCK )
	WHERE Office LIKE 'Retail%' Order By dbo.mwcInstitution.Office

END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBranchOfficeOfUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetBranchOfficeOfUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Shwetal Shah>
-- Create date: <22-05-13>
-- Description:	<Get branch Office for the User>
-- =============================================
CREATE PROCEDURE [dbo].[GetBranchOfficeOfUser]
(
	@UserID int
)
AS
BEGIN

Select Office From dbo.mwcInstitution WITH(NOLOCK) Where dbo.mwcInstitution.Office LIKE 'Retail%' And ID = (Select BranchInstitution_ID From mwaMWUser WITH(NOLOCK) Where ID = (Select E3UserID from tblUsers WITH(NOLOCK) Where UserID = @UserID))

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCannedReportSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetCannedReportSummary]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetCannedReportSummary]        
(        
@RoleID int,       
@strID varchar(2000),        
@BrokerId char(2),      
@TeamOPuids char(15),       
@Year char(4),        
@Channel varchar(15),      
@StatusDesc varchar(1000),    
@CurrentStatus varchar(1000) 
)        
AS        
BEGIN        
        
Declare @SQL nvarchar(max)        
Set @SQL =''        
Set @SQL ='SELECT case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram ,
                 count(TransTYpe)CountRefinanance, case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when 
				TransTYpe = ''R'' then ''Refinance'' end as descriptn,     
                 month(AppStatus.StatusDateTime) as MonthYear,count(AppStatus.StatusDateTime)Total 
                 from mwlLoanApp  loanapp   
                 Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
                 Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc=''' + @StatusDesc + '''' 

                if (@BrokerId != '' AND @BrokerId != 0)
					BEGIN
						Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''' 
						Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''' 
					END

                   Set @SQL = @SQL + ' where loanapp.CurrentStatus in ( ''' + @CurrentStatus + ''') ' 

                if (@BrokerId != '' AND @BrokerId != 0)
                 
                   Set @SQL = @SQL + '  (Broker.CompanyEmail =''' + @BrokerId + ''' or Corres.CompanyEmail =''' + @BrokerId + ''') and '
                 

                if (@Channel =  'broker')
                 
                   Set @SQL = @SQL + '   and loanapp.ChannelType=''BROKER''' 
                 
                if (@Channel =  'retail')
                 
                     Set @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL''' 
                 
                if (@Channel =  'correspond')
                 
                     Set @SQL = @SQL + ' and loanapp.ChannelType=''CORRESPOND'''
                 

                if (@RoleID =  1)
                  
                     Set @SQL = @SQL + ' and loanapp.ChannelType like ''%RETAIL%'''  
                 

                if (@TeamOPuids != '')
                 
                     Set @SQL = @SQL + ' and Originator_id in (''' + @TeamOPuids + ''') '
                 
                else
                BEGIN
                    if (@RoleID != 0 AND @RoleID != 1)
                    BEGIN
                        if (len(@strID) = 0)
						BEGIN		
                            if (@RoleID =  2)
                             
                                Set @SQL = @SQL + '  and (Originator_id in ('''+ @strID +'''))'
                         END 
                        else
                          BEGIN
                            if (@RoleID =  2)
                             
                                Set @SQL = @SQL + '  and (Originator_id in ('''+ @strID +'''))'
						    END 	    
                         
                    END
                END
                 Set @SQL = @SQL + ' and  Year(AppStatus.StatusDateTime) <> ''1900'' and  Year(AppStatus.StatusDateTime)=''' + @Year + ''' group by '

                if (@BrokerId != '' AND @BrokerId != 0)
                BEGIN
                     Set @SQL = @SQL + ' Broker.CompanyEmail ,Corres.CompanyEmail , '
				END

                Set @SQL = @SQL + '  month(AppStatus.StatusDateTime),LoanData.FinancingType, TransTYpe '  
				Set @SQL = @SQL + '  SELECT month(AppStatus.StatusDateTime) as ARmonth, year(AppStatus.StatusDateTime) as ARYear  ,  count(loanapp.CurrentStatus) AS Total, sum(isnull(LoanData.AdjustedNoteAmt, 0)) as SumTotalAmount, round(avg(isnull(LoanData.AdjustedNoteAmt, 0)),0) as AvrageTotalAmount, '
				Set @SQL = @SQL + '  avg(convert(int,isnull(replace(borr.CompositeCreditScore,'','',''''),0)))  as AvrageCreditScore ,round(avg(convert(int, isnull(loanData.ltv,0))),2) as AvgLTV  '
				Set @SQL = @SQL + ' from mwlLoanApp  loanapp '
				Set @SQL = @SQL + ' Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and  borr.sequencenum=1 '
				Set @SQL = @SQL + ' Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id   '
				Set @SQL = @SQL + ' Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc='''+ @StatusDesc + ''''

                if (@BrokerId != '' AND @BrokerId != 0)
					BEGIN
                     Set @SQL = @SQL + '  Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker'' '
                      Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' '
					END

                Set @SQL = @SQL + '  where loanapp.CurrentStatus in ( '''+ @CurrentStatus + ''') ' 

                  if (@BrokerId != '' AND @BrokerId != 0)
					BEGIN
                     Set @SQL = @SQL + ' (Broker.CompanyEmail =''' + @BrokerId + ''' or Corres.CompanyEmail =''' + @BrokerId + ''' ) and '
					END

                if (@Channel =  'broker')
                 
                      Set @SQL = @SQL + ' and loanapp.ChannelType=''BROKER'''
                 
                if (@Channel =  'retail')
                 
                     Set @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL''' 
                 
                if (@Channel =  'correspond')
                 
                      Set @SQL = @SQL + 'and loanapp.ChannelType=''CORRESPOND'''  
                if (@RoleID =  1)
                 
                      Set @SQL = @SQL + ' and loanapp.ChannelType like ''%RETAIL%'''  
                if (@TeamOPuids != '')
                 BEGIN
                       Set @SQL = @SQL + '  and Originator_id in (''' +   @TeamOPuids +''') '
                 END
                else
                BEGIN
                    if (@RoleID != 0 AND @RoleID != 1)
                    BEGIN
                       if (len(@strID) = 0)
                        BEGIN
                            if (@RoleID =  2) 
                                   Set @SQL = @SQL + '  and (Originator_id in (''' + @strID + '''))'
                             
                        END
                        else
                        BEGIN
                            if (@RoleID =  2)
                            
                                  Set @SQL = @SQL + '   and (Originator_id in (''' + @strID + '''))' 
                        END
                    END
                END
                  Set @SQL = @SQL + '   and Year(AppStatus.StatusDateTime) <> ''1900'' and Year(AppStatus.StatusDateTime)='''+ @Year + ''' group by ' 

                if (@BrokerId != '' AND @BrokerId != 0)
                BEGIN
                       Set @SQL = @SQL + '  Broker.CompanyEmail ,Corres.CompanyEmail ,' 
                END
                  Set @SQL = @SQL + '   month(AppStatus.StatusDateTime),year(AppStatus.StatusDateTime),LoanData.FinancingType, TransTYpe ' 

  
 Print @SQL        
exec sp_executesql @SQL        
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCityFromState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetCityFromState]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCityFromState]
	-- Add the parameters for the stored procedure here
@abbr varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select distinct(City) from tblzipcodes where abbr = @abbr order by city
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetKatalystFolderAttributes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetKatalystFolderAttributes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <SHWETAL SHAH>  
-- Create date: <05-04-13>  
-- Description: <get the list of values for katalyst folder attributes>  
-- =============================================  
CREATE PROCEDURE [dbo].[GetKatalystFolderAttributes]  
(  
@LoanNumber varchar(50)  
)  
AS  
BEGIN  
  
Select Bor.FirstName,Bor.LastName, mwlLoanApp.Originator_ID,mwlLoanApp.OriginatorName,mwlLoanApp.LienPosition  
,(Select top(1) AdjustedNoteAmt from mwlLoanData WITH ( NOLOCK ) where ObjOwner_ID = mwlLoanApp.ID and active=1 order by ID desc) as LoanAmount  
,Prop.Street,Prop.City,Prop.State,Prop.County, Prop.ZipCode ,(select top(1) LoanProgramName from mwlLoanData WITH ( NOLOCK ) where ObjOwner_Id = mwlLoanApp.ID and active=1 order by ID desc) as LoanProgram2
, Case When ISNULL((Select StringValue from mwlCustomField WITH ( NOLOCK ) where CustFieldDef_ID = (Select ID From mwsCustFieldDef WITH ( NOLOCK ) where MWFieldNum = 9966) and LoanApp_ID=mwlLoanApp.ID),'') = '' 
	Then ISNULL(mwlLoanApp.ProcessorName,'')
	Else ISNULL((Select StringValue from mwlCustomField WITH ( NOLOCK ) where CustFieldDef_ID = (Select ID From mwsCustFieldDef WITH ( NOLOCK ) where MWFieldNum = 9966) and LoanApp_ID=mwlLoanApp.ID),'')
	End as ProcessorName
From mwlLoanApp WITH ( NOLOCK )
Inner JOin (Select FirstName,LastName,LoanApp_ID From mwlBorrower WITH ( NOLOCK ) where sequencenum=1) as Bor  
ON Bor.LoanAPP_ID = mwlLoanApp.ID  
Inner Join (Select LoanApp_ID,(ISNULL(Street,'')+','+ISNULL(Street2,'')) as Street, City, State,ISNULL(County,'') County, ZipCode from mwlSubjectProperty WITH ( NOLOCK )) Prop  
On Prop.LoanApp_ID = mwlLoanApp.ID 
Where LoanNumber = @LoanNumber  
  
END







GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetKatalystLoanFolderID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetKatalystLoanFolderID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Shwetal Shah>
-- Create date: <01-04-13>
-- Description:	<Fatch Katalyst Folder ID from custome field of E3 DB>
-- mwfieldNum = 9553 used to fatch katalyst folder id
-- =============================================
CREATE PROCEDURE [dbo].[GetKatalystLoanFolderID]
(
	@LoanNumber varchar(20)
)
AS
BEGIN
	
	Select StringValue from mwlCustomField WITH ( NOLOCK ) where LoanApp_ID = (Select ID from mwlLoanApp WITH ( NOLOCK ) where LoanNumber = @LoanNumber) 
		and CustFieldDef_ID = (Select ID from mwsCustFieldDef WITH ( NOLOCK ) Where MWFieldNum=9553)
	
	
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLockActivityReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetLockActivityReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetLockActivityReport]        
(        

@strID varchar(2000),        
@oPuid varchar(2000)  
)        
AS        
BEGIN        
        
Declare @SQL nvarchar(max)        
Set @SQL =''        
Set @SQL ='SELECT ''E3'' as DB, LD2.PROCESSOR, pif.prog_code , loandat.record_id, loan_no,RTRIM(cobo.borr_last)+ '','' + RTRIM(cobo.borr_first)  AS BorrowerLastName, 
			lookupprogram.descriptn as LoanType, lookups.descriptn AS Purpose, 
			proginfo.prog_desc, LD2.doc_funded_date AS FundedDate, loandat.int_rate, convert(varchar(40),locks.lock_exp_date,101) as 
			LockExpires,convert (varchar(40),locks.LOCK_DATE,101) as LockDate, lt_loan_stats AS LoanStatus, convert(varchar(35),
			loandat.UW_RECVD_DATE,101) AS ReceivedInUW, 
			case LU_FINAL_HUD_STATUS when '''' then ''Pending'' when NULL then ''Pending'' else ''Approved'' end as LU_FINAL_HUD_STATUS,
			Loan_amt, brokers.brok_name as BrokerName 
			FROM loandat INNER JOIN cobo ON loandat.record_id = cobo.parent_id LEFT OUTER JOIN locks ON loandat.record_id = locks.parent_id 
			LEFT OUTER JOIN brokers ON loandat.lt_broker = brokers.record_id LEFT OUTER JOIN users ON lt_usr_underwriter = users.record_id
			LEFT OUTER JOIN proginfo ON loandat.lt_program = proginfo.record_id  
			INNER JOIN lookups ON loandat.lu_purpose = lookups.id_Value and lookups.lookup_id = ''AAE'' INNER JOIN lookups as lookups2 
			ON loandat.lu_loan_type = lookups2.id_Value and lookups2.lookup_id = ''ADC'' 
			left outer JOIN lookups AS lookups3 ON loandat.lu_loan_type = lookups3.id_value AND lookups3.lookup_id = ''ACY'' 
			INNER JOIN lookups AS lookupprogram ON loandat.lu_loan_type = lookupprogram.id_value AND lookupprogram.lookup_id = ''AAC''                  
			INNER JOIN lookups AS lookupsLock ON locks.LU_LOCK_STAT = lookupsLock.id_value AND lookupsLock.lookup_id = ''AAT''  
			left join loandat2 as LD2 on loandat.record_id=LD2.record_id 
			left join proginfo as PIF on loandat.record_id = PIF.record_id' 
               if (len(@strID) = 0)
                BEGIN
                    Set @SQL = @SQL + '  where lt_loan_stats  IN (''Locked'','')'                      
                     Set @SQL = @SQL + ' and (loandat.lt_broker in ('''+ @strID +''') or loandat2.lt_usr_lorep in ('''+ @oPuid +'''))'


                END
                else
                BEGIN
                     Set @SQL = @SQL + ' where lt_loan_stats  IN (''Locked'','')'
                    
                     Set @SQL = @SQL + ' and (loandat.lt_broker in ('''+ @strID +''') or loandat2.lt_usr_lorep in ('''+ @oPuid +'''))'
				END

 

  
 Print @SQL        
exec sp_executesql @SQL        
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getPasswordHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[getPasswordHistory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[getPasswordHistory]
 -- Add the parameters for the stored procedure here  
 @UserID int  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
    -- Insert statements for procedure here  
 SELECT top 3 [PasswordID]  
      ,[UserID]  
      ,[Password]  
  FROM [tblUserPasswordHistory] where UserID=@UserID  order by PasswordID desc
END  


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetReportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetReportData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

      
CREATE PROCEDURE [dbo].[GetReportData]       
(          
@RoleID int,         
@strID varchar(2000),           
@CurrentStatus varchar(3000), 
@oPuid varchar(100)   
)          
AS          
BEGIN          
          
Declare @SQL nvarchar(max)          
Set @SQL =''          
Set @SQL ='select ''E3'' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue as ProcessorName,
			loanapp.currentStatus as LoanStatus,loanapp.CurrPipeStatusDate,loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,
			LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,borr.firstname as BorrowerFirstName,
			loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate,                
			approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase''
			when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,
			case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN ''VA'' End as LoanProgram,
			loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,
			LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'''')
			WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,
			CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate ,
			CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER''
			and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0
			WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'')
			and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'')                
			and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0 ELSE
			 (SELECT count(a.ID) as TotalCleared  FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'')
			 and (CurrentState=''CLEARED'' OR  CurrentState=''SUBMITTED'') and
			 a.objOwner_ID IN (SELECT ID FROM dbo.mwlLoanApp as loan2 WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / 
			(SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'') 
			and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1 WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER  
			from mwlLoanApp loanapp Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id Inner join dbo.mwlloandata as loanData
			 on loanData.ObjOwner_id = loanapp.id 
			left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id Left join dbo.mwlInstitution as Broker on 
			Broker.ObjOwner_id = loanapp.id 
			and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker'' left join dbo.mwlLockRecord as LockRecord on 
			LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK'' and LockRecord.Status<> ''CANCELED'' 
			Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id 
			Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved''
			 Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode and ObjectName=''mwlLoanData'' and 
			fieldname=''refipurpose''  Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and 
			Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''
			left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID and 
			CustFieldDef_id in(select Id from mwsCustFieldDef where CustomFieldName=''Retail Processor'') where borr.sequencenum=1 
			and LoanNumber is not null and LoanNumber <> '''' and loanapp.ChannelType like ''%RETAIL%'' and 
			loanapp.CurrentStatus IN (''' + @CurrentStatus + ''' )'
                if (@RoleID != 0 AND @RoleID != 1)
                BEGIN
                    if (len(@strID) = 0)
					begin
                        if (@RoleID = 2)
                        begin
                            
                           Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID +'''))'
                        end
                         
                        else if (@RoleID  = 3)
                        begin
                            
                             Set @SQL = @SQL + ' and (loanapp.ID in ('''+ @strID +''') or Originator_id in (''' + @oPuid + '''))'
                        end
                        else if (@RoleID =  10 or @RoleID =  7)
                        begin
                            
                             Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID +''' ) or loanapp.id in ('''+ @strID +'''))'
                        end
                        else
                        begin
                            
                             Set @SQL = @SQL + 'and (Originator_id in (  '''+ @strID +'''))'
                            if (@oPuid   != '')
                            begin 
                                 Set @SQL = @SQL + ' and (Originator_id in (''' + @oPuid + '''))'
                            end
                        end
                    end
                    else
                    begin
                        if (@RoleID =  2)
                        begin
                            
                          Set @SQL = @SQL + '  and (Originator_id in ( '''+ @strID +''' ))'
                        end
                         
                        else if (@RoleID  = 3)
                        begin
                            
                           Set @SQL = @SQL + ' and (loanapp.ID in ( '''+ @strID +''' )or Originator_id in (''' + @oPuid + '''))'
                        end
                        else if (@RoleID  = 10 or @RoleID = 7)
                        begin
                            
                         Set @SQL = @SQL + '   and (Originator_id in ( '''+ @strID +''' ) or loanapp.id in ('''+ @strID  +''' ))'
                        end
                        else
                        begin
                            
                         Set @SQL = @SQL + '  and (Originator_id in ( '''+ @strID +''' ))'
                            if (@oPuid != '')
                            begin 
                                Set @SQL = @SQL + '   and (Originator_id in (  '''+ @oPuid +''' ))'
                            end
                        end
                    end
                end
                if (@CurrentStatus =  '01 - Registered')
                begin
                    
                     Set @SQL = @SQL + ' And (datediff(day,loanapp.CurrPipeStatusDate,getdate()) >= CONVERT(VARCHAR, 30)) order by loanapp.CurrPipeStatusDate Asc '
                end
                else if (@CurrentStatus = 'RET - Registered as Prospect')
                begin
                    
                    Set @SQL = @SQL + ' And (datediff(day,loanapp.CurrPipeStatusDate,getdate()) >= CONVERT(VARCHAR, 60))  order by loanapp.CurrPipeStatusDate Asc '
                end
                else if (@CurrentStatus = 'RET - Credit Pulled')
                begin
                    
                    Set @SQL = @SQL + ' And (datediff(day,loanapp.CurrPipeStatusDate,getdate()) >= CONVERT(VARCHAR, 15))   order by loanapp.CurrPipeStatusDate Asc '
                end
                else if (@CurrentStatus = 'RET - Credit Pulled with App')
                begin
                    
                    Set @SQL = @SQL + ' And (datediff(day,loanapp.CurrPipeStatusDate,getdate()) >= CONVERT(VARCHAR, 15)) order by loanapp.CurrPipeStatusDate Asc '
                end
                else if (@CurrentStatus = 'RET - Pre-App House Hunting')
                begin
                    
                    Set @SQL = @SQL + ' And (datediff(day,loanapp.CurrPipeStatusDate,getdate()) >= CONVERT(VARCHAR, 90)) order by loanapp.CurrPipeStatusDate Asc '
                end
                else if (@CurrentStatus = '03 - Appt Set to Review Discls')
                begin
                    
                    Set @SQL = @SQL + ' And (datediff(day,loanapp.CurrPipeStatusDate,getdate()) >= CONVERT(VARCHAR, 10))   order by loanapp.CurrPipeStatusDate Asc '
                end
                else if (@CurrentStatus = '04 - Disclosures Sent')
                begin
                    
                    Set @SQL = @SQL + ' And (datediff(day,loanapp.CurrPipeStatusDate,getdate()) >= CONVERT(VARCHAR, 5))  order by loanapp.CurrPipeStatusDate Asc '
                end
                else if (@CurrentStatus = '07 - Disclosures Received')
                begin
                    
                    Set @SQL = @SQL + ' And (datediff(day,loanapp.CurrPipeStatusDate,getdate()) >= CONVERT(VARCHAR, 10))   order by loanapp.CurrPipeStatusDate Asc '
                end
                else
                begin
                    
                    Set @SQL = @SQL + 'order by Per Asc' 
                end
                  
Print @SQL
exec sp_executesql @SQL          
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getSecurityQuestions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[getSecurityQuestions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
Create  Procedure [dbo].[getSecurityQuestions]
 -- Add the parameters for the stored procedure here  
 @QuestionID int  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
    -- Insert statements for procedure here  
 if (@QuestionID = 0)  
 SELECT [QuestionID],[SecurityQuestion],[DisplayAt] FROM [tblSecurityQuestions]  
 else  
 SELECT [QuestionID],[SecurityQuestion],[DisplayAt] FROM [tblSecurityQuestions] where [QuestionID] = @QuestionID  
  
END 


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Insert_Admin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Insert_Admin]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insert_Admin] 
(
       
@name varchar(50),   
@lastname varchar(50),    
@email varchar(50) 
)
AS
BEGIN
	insert into tblusers(ufirstname,ulastname,uemailid,urole,isadmin,isactive,uparent,upassword,maxloginattempt,carrierid,MAID,TermsRealtor,signedupdate) values(
@name,@lastname,@email,0,1,1,0,'KxsQK5O1HoOSmvGPWOGJCA~~~~',0,1,0,0,getdate()) 
END 


 


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Insert_Colts_Data]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Insert_Colts_Data]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Insert_Colts_Data] 
(
@stringmode varchar(50),
@id int,      
@name varchar(50),   
@lastname varchar(50),    
@streetaddr varchar(500),    
@city varchar(50),
@state varchar(50),   
@zipcode varchar(50),    
@phoneno varchar(50),      
@email varchar(50),     
@confirmemail varchar(50),      
@scoutprogram char(4),     
@colts char(4),      
@newspaper char(4),      
@friend char(4),      
@other char(4),        
@representative char(4)  
)
AS
BEGIN
	 INSERT INTO tblColts (name,lastname,streetaddr,city,state,zipcode,phoneno,email,confirmemail,scoutprogram,colts,newspaper,friend,other,representative)      
 VALUES      
  (@name,@lastname,@streetaddr,@city,@state,@zipcode,@phoneno,@email,@confirmemail,@scoutprogram,@colts,@newspaper,@friend,@other,@representative)  
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertFinalApprove]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertFinalApprove]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InsertFinalApprove]
	-- Add the parameters for the stored procedure here
	(
@LoanApplicationID int, 
@PlaceEmp int, 
@CurrentAnualIncome money, 
@PhoneNum nvarchar(50), 
@TimePeriod int, 
@PriorEmp nvarchar(50), 
@PriorAnualIncome money, 
@IsSelfEmp bit,  
@IsCredit bit, 
@CreditSSNNo nvarchar(50),
@CreditDoBirth datetime,
@IsAuthorize bit, 
@IsCoapplicant bit, 
@CoAppFName nvarchar(50),
@CoAppLName nvarchar(50), 
@Co_BorrowerAdd1 nvarchar(200), 
@Co_BorrowerAdd2 nvarchar(200), 
@Co_BorrowerCity nvarchar(50), 
@Co_BorrowerState int, 
@Co_BorrowerZip nvarchar(50), 
@Co_BorrowerHomePhone nvarchar(50), 
@Co_BorrowerCellorWorkPhone nvarchar(50), 
@Co_BorrowerEmail nvarchar(50), 
@Co_BorrowerPlaceEmp nvarchar(50), 
@Co_BorrowerPostion nvarchar(50), 
@Co_BorrowerCurrentAnual money, 
@Co_BorrowerPhone nvarchar(50), 
@Co_BorrowerTimePeriod int, 
@Co_BorrowerPriorPlaceEmp money,
@Co_BorrowerPriorAnual money, 
@Co_BorrowerIsSelfEmp bit, 
@Co_BorrowerSelfDoBirth datetime, 
@Co_BorrowerSelfSSN nvarchar(50), 
@IsWorkingWithRealtor bit, 
@RealtorName nvarchar(50), 
@RealtorPhone nvarchar(50), 
@IsBorrowersBankruptcy bit,
@Bankruptcywhen nvarchar(50),
@IsBorrowerResponsiblechild bit,
@childsupportqty nvarchar(50)

	)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblBorrowerInfo
(
LoanApplicationID, 
PlaceEmp, 
CurrentAnualIncome, 
PhoneNum, 
TimePeriod, 
PriorEmp, 
PriorAnualIncome, 
IsSelfEmp,  
IsCredit, 
CreditSSNNo,
CreditDoBirth,
IsAuthorize, 
IsCoapplicant, 
CoAppFName,
CoAppLName, 
[Co-BorrowerAdd1], 
[Co-BorrowerAdd2], 
[Co-BorrowerCity], 
[Co-BorrowerState], 
[Co-BorrowerZip], 
[Co-BorrowerHomePhone], 
[Co-BorrowerCellorWorkPhone], 
[Co-BorrowerEmail], 
[Co-BorrowerPlaceEmp], 
[Co-BorrowerPostion], 
[Co-BorrowerCurrentAnual], 
[Co-BorrowerPhone], 
[Co-BorrowerTimePeriod], 
[Co-BorrowerPriorPlaceEmp],
[Co-BorrowerPriorAnual], 
[Co-BorrowerIsSelfEmp], 
[Co-BorrowerSelfDoBirth], 
[Co-BorrowerSelfSSN], 
IsWorkingWithRealtor, 
RealtorName, 
RealtorPhone, 
IsBorrowersBankruptcy,
Bankruptcywhen,
IsBorrowerResponsiblechild,
childsupportqty
) 
Values
(
@LoanApplicationID, 
@PlaceEmp, 
@CurrentAnualIncome, 
@PhoneNum, 
@TimePeriod, 
@PriorEmp, 
@PriorAnualIncome, 
@IsSelfEmp,  
@IsCredit, 
@CreditSSNNo,
@CreditDoBirth,
@IsAuthorize, 
@IsCoapplicant, 
@CoAppFName,
@CoAppLName, 
@Co_BorrowerAdd1, 
@Co_BorrowerAdd2, 
@Co_BorrowerCity, 
@Co_BorrowerState, 
@Co_BorrowerZip, 
@Co_BorrowerHomePhone, 
@Co_BorrowerCellorWorkPhone, 
@Co_BorrowerEmail, 
@Co_BorrowerPlaceEmp, 
@Co_BorrowerPostion, 
@Co_BorrowerCurrentAnual, 
@Co_BorrowerPhone, 
@Co_BorrowerTimePeriod, 
@Co_BorrowerPriorPlaceEmp,
@Co_BorrowerPriorAnual, 
@Co_BorrowerIsSelfEmp, 
@Co_BorrowerSelfDoBirth, 
@Co_BorrowerSelfSSN, 
@IsWorkingWithRealtor, 
@RealtorName, 
@RealtorPhone, 
@IsBorrowersBankruptcy,
@Bankruptcywhen,
@IsBorrowerResponsiblechild,
@childsupportqty

)      
              
                 
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PrcGetEmailData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PrcGetEmailData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
  
  
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[PrcGetEmailData]  
 -- Add the parameters for the stored procedure here  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 --select TOP 1 * from tblEmailServiceReader where updStatus=0   
--order by ID   
select TOP 1 * from tblEmailServiceReader where updStatus=0
order by id

select TOP 1 * from tblEmailServiceReader where updStatus=1
order by id
--order by RcvdDate desc  
--and ToAddress='tnierste@stonegatemtg.com'   
END  
  
  
  

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_add_jobschedule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_add_jobschedule]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Created By: Amit Kumar
-- Create Date : 04/09/13

-- =============================================
CREATE PROC [dbo].[sp_add_jobschedule]

AS
BEGIN
----------------Start (to move from active to archive message)---------------------------
update tblServiceBanner set Status=3 where expiredate=convert(varchar(10),getdate(),101)
----------------End (to move from active to archive message)---------------------------
declare
@count int,
@ServiceId int,
@Counter int,
@PDate datetime,
@EDate Datetime,

@Status char(1)

CREATE TABLE #TempTable(
TempID int IDENTITY(1,1),
TempServiceId int,
TempStatus char(1),
TempPublishDate datetime,
TempExpireDate Datetime
)

insert into #TempTable(TempServiceId,TempStatus,
TempPublishDate,
TempExpireDate ) select ServiceBannerId,Status,PublishDate,ExpireDate from tblservicebanner where status in(2,3)

set @count=0
select @count=count(TempID) from #TempTable
set @Counter=1

if(@count>0)


begin
while(@count<>0)

begin


select @ServiceId=TempServiceId,@PDate=TempPublishDate,@Status=TempStatus from #TempTable where TempID=@Counter
-----------Start (to move from pending to active message)------------------------------------
if(@Status=2)
begin
if(convert(varchar(10),@PDate,101)=convert(varchar(10),getdate(),101))
begin
update tblservicebanner set Status=1 where ServiceBannerId=@ServiceId
end
end
------------End(to move from pending to active message)------------------------------------
------------Start (to move from archve to expire message)------------------------------------

if(@Status=3)
begin
if(convert(varchar(10),dateadd(month,12,@EDate),101)=convert(varchar(10),getdate(),101))
begin
update tblServiceBanner set Status=0 where ServiceBannerId=@ServiceId
end
end
------------End (to move from archve to expire message)--------------------------------------
set @Counter=@Counter+1
set @count=@count-1
end
end

end


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AddOrderByField]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_AddOrderByField]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_AddOrderByField]
AS
BEGIN
	 select dbo.GetFormRole(fo.formid)as Roleid,* from tblforms FO left outer join tblCategories CAT on FO.Cateid=CAT.catid ORDER BY dttime DESC

END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AddOrderByField2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_AddOrderByField2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_AddOrderByField2]
(
@uroleid int
)
AS
BEGIN
	 select dbo.GetFormRole(FO.formid)as Roleid, formID,Description,FileName,dttime,isnews,CateName from tblforms FO left outer join tblCategories CAT on FO.Cateid=CAT.catid where formid in (select formid from tblformroles where uroleid = @uroleid) ORDER BY dttime DESC

END





GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Assign_Managers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Assign_Managers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Rakesh Kumar Mohanty>
-- Create date: <Create Date,9th May 2014>
-- Description:	<Description,,>
--Exec sp_Assign_Managers 79,'2184,76,11427,20875',79
-- =============================================
CREATE PROCEDURE [dbo].[sp_Assign_Managers]
	-- Add the parameters for the stored procedure here
	@UserID int,
	@ManagerIds Nvarchar(max),
	@CreatedBy Nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    -- Insert statements for procedure here
	DELETE FROM [dbo].[tblUserManagers] WHERE UserId = @UserID 
	AND ManagerId NOT IN (SELECT Id.Items FROM [dbo].SplitString(@ManagerIds, ',') Id)
	
	INSERT INTO [dbo].[tblUserManagers] ([UserId], [ManagerId], [CreatedBy], [CreatedOn])
		SELECT @UserID,Id.Items,@CreatedBy, GetDate() FROM [dbo].SplitString(@ManagerIds, ',') Id
		where Id.Items NOT IN (SELECT UM.ManagerId FROM [dbo].[tblUserManagers] UM WHERE UM.UserId = @UserID)
		

END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AssignDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_AssignDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_AssignDoc]
(
@AssignedBy varchar(max),
@ctID varchar(max)
)
AS
BEGIN
update tblLoanDocs set Assigned = 1,AssignedBy=@AssignedBy,Assigndate=getdate() where ctID = @ctID
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AssignDoc2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_AssignDoc2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_AssignDoc2]
(
@AssignedBy varchar(max),
@loanregid varchar(max)
)
AS
BEGIN
update tblLoanReg set Assigned = 1,AssignedBy=@AssignedBy,Assigndate=getdate() where loanregid = @loanregid


END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Authenticate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Authenticate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Authenticate]
	-- Add the parameters for the stored procedure here
@LoginName varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *  FROM tblUser WHERE LoginName=@LoginName
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Authenticate_tblUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Authenticate_tblUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Authenticate_tblUser]
	-- Add the parameters for the stored procedure here
	@LoginName varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT *  FROM tblUser WHERE LoginName=@LoginName 
    -- Insert statements for procedure here
	
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CancelLoan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CancelLoan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_CancelLoan]
(
@Loan_no VARCHAR(MAX)
)
AS
BEGIN
update loandat set CANCELED_DATE=getDate(), lt_loan_stats='LOAN CANCELED' where Loan_no=@Loan_no


END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckDuplicateCategory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckDuplicateCategory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_CheckDuplicateCategory]
(
@CateName varchar (100)
)
AS
BEGIN
	 select count(catid) tot from tblCategories where CateName=@CateName
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckDuplicateLoanType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckDuplicateLoanType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_CheckDuplicateLoanType]
(
@LoanType varchar(max)
)
AS
BEGIN

	SET NOCOUNT ON;

select LoanType from tblLoanType where LoanType=@LoanType

END

--------------------------------------------------------------------------------------------------------------


/****** Object:  StoredProcedure [dbo].[sp_InsertCreateLoanType]    Script Date: 04/12/2013 17:43:39 ******/
SET ANSI_NULLS ON

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckDuplicatePackageId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckDuplicatePackageId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_CheckDuplicatePackageId]
(
@PackageId varchar(max)
)
AS
BEGIN

	SET NOCOUNT ON;

select PackageId from tblPackageTypeId where PackageId=@PackageId

END
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckforSubmitLoanKatalyst]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckforSubmitLoanKatalyst]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Shwetal Shah>  
-- Create date: <17/4/2012>  
-- Description: <This wil Check for Loans of Katalyst for View Loan Folder>  
-- MWFieldNum = 9553 Used for fatching Katalyst Folder ID from CustomField table in E3
-- =============================================  

CREATE PROCEDURE [dbo].[sp_CheckforSubmitLoanKatalyst] (
	@RoleID INT
	,@strID VARCHAR(max)
	,@strChLoanno VARCHAR(100)
	,@CurrentStatus VARCHAR(3000)
	,@oPuid VARCHAR(3000)
	,@strStatusDesc VARCHAR(50)
	,@OfficeName VARCHAR(50)
	)
AS
BEGIN
	SET @strChLoanno = '%' + @strChLoanno + '%'

	--ROLE VARIABLE DECLARATIONS-----------------------------------------------------------------------
	DECLARE @Admin INT

	SET @Admin = 0

	DECLARE @SalesManager INT

	SET @SalesManager = 3

	DECLARE @BranchManager INT

	SET @BranchManager = 10

	DECLARE @CaseOpener INT

	SET @CaseOpener = 12

	DECLARE @RetailOperMgr INT

	SET @RetailOperMgr = 11

	DECLARE @Closer INT

	SET @Closer = 15

	DECLARE @RetailTeamLd INT

	SET @RetailTeamLd = 16

	---------------------------------------------------------------------------------------------------
	DECLARE @Channeltype VARCHAR(20)

	SELECT TOP 1 @Channeltype = ChannelType
	FROM mwlLoanApp
	WHERE LoanNumber LIKE @strChLoanno

	IF (
			@RoleID != @Admin
			AND @RoleID != @CaseOpener
			AND @RoleID != @Closer
			AND @RoleID != @RetailOperMgr
			AND @RoleID != @RetailTeamLd
			)
	BEGIN
		IF (
				@RoleID = @SalesManager
				OR @RoleID = @BranchManager
				)
		BEGIN
			SELECT 'E3' AS DB
				,loanapp.casenum AS FHANumber
				,loanapp.ChannelType
				,loanapp.OriginatorName
				,loanapp.currentStatus AS LoanStatus
				,loanapp.DecisionStatus
				,loanapp.ID AS record_id
				,LoanNumber AS loan_no
				,borr.firstname AS BorrowerfirstName
				,borr.lastname AS BorrowerLastName
				,loanapp.CurrentStatus AS LoanStatus
				,appStat.StatusDateTime AS SubmittedDate
				,CASE 
					WHEN TransType IS NULL
						THEN ''
					WHEN TransTYpe = 'P'
						THEN 'Purchase'
					WHEN TransTYpe = 'R'
						THEN 'Refinance'
					END AS TransTYpe
				,loanData.AdjustedNoteAmt AS loan_amt
			FROM mwlLoanApp loanapp WITH (NOLOCK)
			INNER JOIN mwlBorrower AS borr ON borr.loanapp_id = loanapp.id
			INNER JOIN dbo.mwlloandata AS loanData ON loanData.ObjOwner_id = loanapp.id
				AND loanData.Active = 1
			INNER JOIN (
				SELECT ID
					,LoanApp_ID
					,StringValue
				FROM mwlCustomfield WITH (NOLOCK)
				WHERE CustFieldDef_ID = (
						SELECT ID
						FROM mwsCustFieldDef WITH (NOLOCK)
						WHERE MWFieldNum = 9553
						)
				) mwCustFld ON mwCustFld.LoanApp_ID = loanapp.ID
			LEFT JOIN dbo.mwlInstitution AS Branch ON Branch.ObjOwner_id = loanapp.id
				AND Branch.InstitutionType = 'Branch'
				AND Branch.objownerName = 'BranchInstitution'
			LEFT JOIN mwlAppStatus appStat ON loanapp.ID = appStat.LoanApp_id
				AND appStat.sequencenum = 1
				AND appStat.StatusDesc = @strStatusDesc
			WHERE borr.sequencenum = 1
				AND LoanNumber IS NOT NULL
				AND LoanNumber <> ''
				AND loanapp.ChannelType = @Channeltype
				AND CurrentStatus IN (
					SELECT items
					FROM dbo.SplitString(@CurrentStatus, ',')
					)
				AND (Branch.Office IN (@OfficeName))
				AND (
					LoanNumber LIKE @strChLoanno
					OR borr.lastname LIKE @strChLoanno
					)
		END
		ELSE
		BEGIN
			SELECT 'E3' AS DB
				,loanapp.casenum AS FHANumber
				,loanapp.ChannelType
				,loanapp.OriginatorName
				,loanapp.currentStatus AS LoanStatus
				,loanapp.DecisionStatus
				,loanapp.ID AS record_id
				,LoanNumber AS loan_no
				,borr.firstname AS BorrowerfirstName
				,borr.lastname AS BorrowerLastName
				,loanapp.CurrentStatus AS LoanStatus
				,appStat.StatusDateTime AS SubmittedDate
				,CASE 
					WHEN TransType IS NULL
						THEN ''
					WHEN TransTYpe = 'P'
						THEN 'Purchase'
					WHEN TransTYpe = 'R'
						THEN 'Refinance'
					END AS TransTYpe
				,loanData.AdjustedNoteAmt AS loan_amt
			FROM mwlLoanApp loanapp WITH (NOLOCK)
			INNER JOIN mwlBorrower AS borr ON borr.loanapp_id = loanapp.id
			INNER JOIN dbo.mwlloandata AS loanData ON loanData.ObjOwner_id = loanapp.id
				AND loanData.Active = 1
			INNER JOIN (
				SELECT ID
					,LoanApp_ID
					,StringValue
				FROM mwlCustomfield WITH (NOLOCK)
				WHERE CustFieldDef_ID = (
						SELECT ID
						FROM mwsCustFieldDef WITH (NOLOCK)
						WHERE MWFieldNum = 9553
						)
				) mwCustFld ON mwCustFld.LoanApp_ID = loanapp.ID
			LEFT JOIN dbo.mwlInstitution AS Branch ON Branch.ObjOwner_id = loanapp.id
				AND Branch.InstitutionType = 'Branch'
				AND Branch.objownerName = 'BranchInstitution'
			LEFT JOIN mwlAppStatus appStat ON loanapp.ID = appStat.LoanApp_id
				AND appStat.sequencenum = 1
				AND appStat.StatusDesc = @strStatusDesc
			WHERE borr.sequencenum = 1
				AND LoanNumber IS NOT NULL
				AND LoanNumber <> ''
				AND loanapp.ChannelType = @Channeltype
				AND CurrentStatus IN (
					SELECT items
					FROM dbo.SplitString(@CurrentStatus, ',')
					)
				AND (
					Originator_id IN (
						SELECT items
						FROM dbo.SplitString(@strID, ',')
						)
					OR Originator_id IN (
						SELECT items
						FROM dbo.SplitString(@oPuid, ',')
						)
					OR loanapp.id IN (
						SELECT items
						FROM dbo.SplitString(@strID, ',')
						)
					)
				AND (
					LoanNumber LIKE @strChLoanno
					OR borr.lastname LIKE @strChLoanno
					)
		END
	END
	ELSE
	BEGIN
		SELECT 'E3' AS DB
			,loanapp.casenum AS FHANumber
			,loanapp.ChannelType
			,loanapp.OriginatorName
			,loanapp.currentStatus AS LoanStatus
			,loanapp.DecisionStatus
			,loanapp.ID AS record_id
			,LoanNumber AS loan_no
			,borr.firstname AS BorrowerfirstName
			,borr.lastname AS BorrowerLastName
			,loanapp.CurrentStatus AS LoanStatus
			,appStat.StatusDateTime AS SubmittedDate
			,CASE 
				WHEN TransType IS NULL
					THEN ''
				WHEN TransTYpe = 'P'
					THEN 'Purchase'
				WHEN TransTYpe = 'R'
					THEN 'Refinance'
				END AS TransTYpe
			,loanData.AdjustedNoteAmt AS loan_amt
		FROM mwlLoanApp loanapp
		INNER JOIN mwlBorrower AS borr ON borr.loanapp_id = loanapp.id
		INNER JOIN dbo.mwlloandata AS loanData ON loanData.ObjOwner_id = loanapp.id
			AND loanData.Active = 1
		INNER JOIN (
			SELECT ID
				,LoanApp_ID
				,StringValue
			FROM mwlCustomfield
			WHERE CustFieldDef_ID = (
					SELECT ID
					FROM mwsCustFieldDef
					WHERE MWFieldNum = 9553
					)
			) mwCustFld ON mwCustFld.LoanApp_ID = loanapp.ID
		LEFT JOIN dbo.mwlInstitution AS Branch ON Branch.ObjOwner_id = loanapp.id
			AND Branch.InstitutionType = 'Branch'
			AND Branch.objownerName = 'BranchInstitution'
		LEFT JOIN mwlAppStatus appStat ON loanapp.ID = appStat.LoanApp_id
			AND appStat.sequencenum = 1
			AND appStat.StatusDesc = @strStatusDesc
		WHERE borr.sequencenum = 1
			AND LoanNumber IS NOT NULL
			AND LoanNumber <> ''
			AND loanapp.ChannelType = 'RETAIL'
			AND CurrentStatus IN (
				SELECT items
				FROM dbo.SplitString(@CurrentStatus, ',')
				)
			AND (
				LoanNumber LIKE @strChLoanno
				OR borr.lastname LIKE @strChLoanno
				)
	END
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckforSubmitLoanRetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckforSubmitLoanRetail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Vipul Thacker>  
-- Create date: <10/4/2012>  
-- Description: <This wil Check for Submit Loans for Submit Loan Document or Submit File to processing>
--9828 MWFieldNUm is used to filter Retail Case Opener loan records only
-- =============================================  
CREATE PROCEDURE [dbo].[sp_CheckforSubmitLoanRetail]        
(        
@RoleID int,       
@strID varchar(max),        
@strChLoanno varchar(100),        
@CurrentStatus varchar(3000),      
@oPuid varchar(3000),      
@strStatusDesc varchar(50),
@OfficeName varchar(50)    
)        
AS        
BEGIN        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  -- Due to the structure of E3 DBS, Harland suggests not to lock tables during reads. 
SET @strChLoanno = '%' + @strChLoanno + '%'
--ROLE VARIABLE DECLARATIONS-----------------------------------------------------------------------
DECLARE @Admin INT      
SET @Admin=0 

DECLARE @SalesManager INT      
SET @SalesManager=3

DECLARE @BranchManager INT      
SET @BranchManager=10

Declare @CaseOpener INT      
SET @CaseOpener=12

Declare @RetailOperMgr INT      
SET @RetailOperMgr=11

Declare @Closer INT  
SET @Closer=15
        
Declare @RetailTeamLd INT  
SET @RetailTeamLd=16

Declare @AreaManager INT  
SET @AreaManager=19
---------------------------------------------------------------------------------------------------
IF(@RoleID != @Admin and @RoleID != @Closer and @RoleID != @RetailOperMgr and  @RoleID != @RetailTeamLd and  @RoleID != @AreaManager)
	BEGIN
	if(@RoleID = @SalesManager OR @RoleID = @BranchManager)
	BEGIN
		SELECT 'E3' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus
			,loanapp.ID as record_id,LoanNumber as loan_no,borr.firstname as BorrowerfirstName
			,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,appStat.StatusDateTime AS SubmittedDate
			,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
			,loanData.AdjustedNoteAmt as loan_amt
		FROM mwlLoanApp loanapp WITH ( NOLOCK ) 		
		Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
		Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active = 1			
		Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id 
			and Branch.InstitutionType = 'Branch' and Branch.objownerName='BranchInstitution'
		Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.sequencenum=1 AND appStat.StatusDesc=@strStatusDesc
		WHERE borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''
		AND loanapp.ChannelType like '%RETAIL%' and CurrentStatus IN (select items from dbo.SplitString(@CurrentStatus,','))
		--AND (Branch.Office in (@OfficeName)) commnented out to resolve SMCPS451
		AND (LoanNumber like @strChLoanno OR borr.lastname like @strChLoanno)
	END
	ELSE IF(@RoleID = @CaseOpener)
	BEGIN	
		SELECT 'E3' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus
			,loanapp.ID as record_id,LoanNumber as loan_no,borr.firstname as BorrowerfirstName
			,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,appStat.StatusDateTime AS SubmittedDate
			,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
			,loanData.AdjustedNoteAmt as loan_amt
		from mwlLoanApp loanapp WITH ( NOLOCK )
		INNER JOIN mwlCustomField ON mwlCustomField.LoanApp_ID = loanapp.ID And mwlCustomField.CustFieldDef_ID = (Select ID from mwsCustFieldDef WHERE MWFieldNum = '9828')
		Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id 
		Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active = 1
		Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id 
			and Branch.InstitutionType = 'Branch' and Branch.objownerName='BranchInstitution'
		Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.sequencenum=1 and appStat.StatusDesc=@strStatusDesc
		where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''
		AND loanapp.ChannelType like '%RETAIL%' and CurrentStatus IN (select items from dbo.SplitString(@CurrentStatus,','))
		AND (Originator_id in (select items from dbo.SplitString(@strID,',')) OR Originator_id in (select items from dbo.SplitString(@oPuid,',')) OR loanapp.id in (select items from dbo.SplitString(@strID,',')))
		AND (LoanNumber LIKE @strChLoanno OR borr.lastname LIKE @strChLoanno)
	END
	ELSE
	BEGIN
		SELECT 'E3' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus
			,loanapp.ID as record_id,LoanNumber as loan_no,borr.firstname as BorrowerfirstName
			,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,appStat.StatusDateTime AS SubmittedDate
			,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
			,loanData.AdjustedNoteAmt as loan_amt
		from mwlLoanApp loanapp WITH ( NOLOCK )
		Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id 
		Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active = 1
		Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id 
			and Branch.InstitutionType = 'Branch' and Branch.objownerName='BranchInstitution'
		Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.sequencenum=1 and appStat.StatusDesc=@strStatusDesc
		where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''
		AND loanapp.ChannelType like '%RETAIL%' and CurrentStatus IN (select items from dbo.SplitString(@CurrentStatus,','))
		AND (Originator_id in (select items from dbo.SplitString(@strID,',')) OR Originator_id in (select items from dbo.SplitString(@oPuid,',')) OR loanapp.id in (select items from dbo.SplitString(@strID,',')))
		AND (LoanNumber LIKE @strChLoanno OR borr.lastname LIKE @strChLoanno)
	END
END
Else
BEGIN
select 'E3' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus
		,loanapp.ID as record_id,LoanNumber as loan_no,borr.firstname as BorrowerfirstName
		,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,appStat.StatusDateTime AS SubmittedDate
		,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
		,loanData.AdjustedNoteAmt as loan_amt
	from mwlLoanApp loanapp WITH ( NOLOCK ) 
	Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
	Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active = 1	
	Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id 
		and Branch.InstitutionType = 'Branch' and Branch.objownerName='BranchInstitution'
	Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.sequencenum=1 and appStat.StatusDesc=@strStatusDesc
	where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''
	and loanapp.ChannelType like '%RETAIL%' and CurrentStatus IN (select items from dbo.SplitString(@CurrentStatus,','))
	AND (LoanNumber LIKE @strChLoanno OR borr.lastname LIKE @strChLoanno)
END     
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckforSubmitLoanRetailSearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckforSubmitLoanRetailSearch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================
-- Created By:	Chandresh  Patel
-- Create Date : 10/04/2012
-- Description:	Procedure to insert/update/delete Rates
-- =======================================================

  
CREATE PROCEDURE [dbo].[sp_CheckforSubmitLoanRetailSearch]        
(        
@RoleID int,       
@strID varchar(2000),        
@strChLoanno varchar(100),        
@CurrentStatus varchar(3000)       
)        
AS        
BEGIN        
        
Declare @SQL nvarchar(max)        
Set @SQL =''        
Set @SQL ='select ''E3'' as DB,LockDateTime,loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.lastname as      
 BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate,      
 approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when       
 TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,      
 case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,      
 loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,      
 LockRecord.LockExpirationDate,UnderwriterName as UnderWriter,Broker.Company as BrokerName,Lookups.DisplayString as CashOut,      
 CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate ,       
 CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER''      
 and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0      
 WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'')      
 and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'')      
 and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0        
 ELSE (SELECT count(a.ID) as TotalCleared       
 FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'') and (CurrentState=''CLEARED'' OR       
 CurrentState=''SUBMITTED'') and a.objOwner_ID IN       
 (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions      
 FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1      
 WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER       
 from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  Inner join dbo.mwlloandata as loanData      
 on loanData.ObjOwner_id = loanapp.id        
 left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  Left join dbo.mwlInstitution as Broker on       
 Broker.ObjOwner_id = loanapp.id       
 and Broker.InstitutionType = ''Branch'' and Broker.objownerName=''BranchInstitution'' left join dbo.mwlLockRecord as LockRecord       
 on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED''       
 Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc like ''%Registered%''      
 Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved''      
 Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''         
 where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and       
 loanapp.ChannelType like ''%RETAIL%'' and  CurrentStatus    IN (select * from dbo.SplitString('''+@CurrentStatus+''','',''))'      
      
      
if (@RoleID != 0 and @RoleID != 12 and @RoleID != 15 and @RoleID != 11 and  @RoleID != 16 )      
 BEGIN      
  if (len(@strID) = 0)         
   Set @SQL = @SQL + ' and (Originator_id in (' + @strID + ')  or loanapp.id in (''' + @strID + '''))'      
               
  else       
     Set @SQL = @SQL + ' and (Originator_id in (' + @strID + ') or loanapp.id in (''' + @strID + '''))'      
 END      
                      
SET @strChLoanno = '%' + @strChLoanno + '%'                
Set @SQL = @SQL + '  AND  (LoanNumber like   '''+ @strChLoanno +'''   OR borr.lastname like '''+ @strChLoanno +''')'      
Set @SQL = @SQL + '  order by Per desc '      
          
Print @SQL        
exec sp_executesql @SQL        
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckLoanNo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckLoanNo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_CheckLoanNo]
(
@loan_no varchar (max)
)
AS
BEGIN
select * from loandat where lt_loan_stats  IN ('Locked','Application Received','Submitted to Underwriting','In Underwriting','Loan Suspended','Approved') and loan_no = @loan_no 



END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckSchedultDateSubmited]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckSchedultDateSubmited]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_CheckSchedultDateSubmited]
(
@LoanNo VARCHAR(MAX)
)
AS
BEGIN
select count(LoanNo) as totalrec from tblScheduleClosing where LoanNo =@LoanNo

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CompleteDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CompleteDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_CompleteDoc]
(
@CompletedBy varchar(max),
@ctID varchar(max)
)
AS
BEGIN
update tblLoanDocs set Completed = 1,CompletedBy=@CompletedBy,TimeStamp=getdate() where ctID =@ctID


END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CompleteDoc2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CompleteDoc2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_CompleteDoc2]
(
@CompletedBy varchar(max),
@loanregid varchar(max)
)
AS
BEGIN
update tblLoanReg set Completed = 1,CompletedBy=@CompletedBy,TimeStamp=getdate() where loanregid = @loanregid


END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CountFileByUserid]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CountFileByUserid]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CountFileByUserid]
(
@userid int,
@filename varchar(100)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select count(*) TotalFile from tblLoanReg where userid = @userid and filename=@filename
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CountHUDApprovedDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CountHUDApprovedDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_CountHUDApprovedDoc]
(
@parent_id varchar(max)
)
AS
BEGIN
select parent_id from prolend_di..docdata
where parent_id=@parent_id and doc_descriptn1='HUD-1 Approved' and group_id=6
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Delete_Form]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Delete_Form]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel
-- Create Date : 10/04/2012  
-- Description: Procedure for  Delete Form
-- =============================================  
   
CREATE PROC [dbo].[SP_Delete_Form](       
@FormID int          
)                
AS                
BEGIN            
   Delete from tblformRoles where formid =@FormID   
 Delete from tblForms where FormID =@FormID  
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Delete_Managers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Delete_Managers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Rakesh Kumar Mohanty>
-- Create date: <Create Date,9th May 2014>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Delete_Managers]
	@Userid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	declare @rows_affected_output int 
    -- Insert statements for procedure here
	DELETE FROM [dbo].[tblUserManagers] WHERE USERID = @Userid
	SELECT @rows_affected_output = @@rowcount
	if(@rows_affected_output is null)
	BEGIN
		SELECT @rows_affected_output=0
	END
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Delete_Users]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Delete_Users]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_Delete_Users](     
@UserID int        
)              
AS              
BEGIN          
   Delete From tblusers Where userid =@UserID        
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteAdditionalDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteAdditionalDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_DeleteAdditionalDoc]
(
@ctid VARCHAR(MAX)

)
AS
BEGIN

delete from tblAdditionalUWDoc where ctid = @ctid
 
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_DeleteDoc]
(
@loanregid varchar(max),
@loandocid varchar(max)
)
AS
BEGIN
update tblLoanReg set filename= NULL where loanregid = @loanregid ; delete from tblloandoc where loandocid=@loandocid

END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteDocument]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteDocument]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_DeleteDocument]
(
@ctid VARCHAR(MAX)
)
AS
BEGIN
delete from tblCondText_History where ctid = @ctid
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteDocumentLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteDocumentLog]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_DeleteDocumentLog]
(
@loanregid varchar(max),
@ctid varchar(max),
@Scheduleid varchar(max),
@loandocid varchar(max)
)
AS
BEGIN
update tblLoanReg set IsDeletedFromDocLog = 0 where loanregid = @loanregid ; update tblCondText_History set IsActive= 0 where ctid = @ctid ; update tblScheduleClosing set IsActive= 0 where Scheduleid = @Scheduleid ; update tblloandoc set IsDeletedFromDocLog = 0 where loandocid=@loandocid
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteDocumentUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteDocumentUpdate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_DeleteDocumentUpdate]
(
@record_id VARCHAR(MAX)
)
AS
BEGIN
update condits  set cond_recvd_date = NULL where record_id = @record_id
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteFaqs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteFaqs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create  Procedure [dbo].[sp_DeleteFaqs]
@ID bigint
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

Delete from dbo.tblfaqs where ID=@ID

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteFormRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteFormRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_DeleteFormRoles]
(
@formid int
)
AS

BEGIN
	Delete from tblformRoles where formid =@formid
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteHomeOwner]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteHomeOwner]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteHomeOwner]
(
@formID int
)
AS
BEGIN

	SET NOCOUNT ON;

Delete from tblFormsHomeowner where formID =@formID
END

--------------------------------------------------------------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[sp_CheckDuplicatePackageId]    Script Date: 04/12/2013 17:42:06 ******/
SET ANSI_NULLS ON

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteLoan_LOANAPP]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteLoan_LOANAPP]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,,Name>  
-- ALTER date: <ALTER Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_DeleteLoan_LOANAPP]  
 -- Add the parameters for the stored procedure here  
 @loanid varchar(20)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 DELETE FROM tblLoanApplication  WHERE LoanApplicationID=@loanid  
END  
  

 

 
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteLoan_LoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteLoan_LoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteLoan_LoanApplication]
	-- Add the parameters for the stored procedure here
	@LoanApplicationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM tblLoanApplication WHERE LoanApplicationID =@LoanApplicationID
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteNewRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteNewRate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_DeleteNewRate]
(
@NewRateID varchar(Max)
)
AS
BEGIN

delete from tblnewrates where NewRateID = @NewRateID

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteNewsAnnouncement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteNewsAnnouncement]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_DeleteNewsAnnouncement]    
@id varchar(50)    
AS    
BEGIN
-- IF exists (SELECT * FROM tblNewsAndAnnouncement where ID=@ID AND cc_id='0')  
-- begin  
-- --print 'a'  
--  Delete FROM tpo_tblNewsAndAnnouncement where cc_id=@id  
-- end  
--else  
-- begin  
--  DELETE FROM tpo_tblNewsAndAnnouncement where ID = (SELECT cc_id FROM tblNewsAndAnnouncement where ID=@ID)   
-- --print 'b'  
-- end   
DELETE FROM tpo_tblNewsAndAnnouncement
WHERE id = @id 
END 


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteOffices]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteOffices]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsOffice.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_DeleteOffices]
(
@userid varchar(Max)
)
AS
BEGIN

delete from tblOffices Where userid =@userid
	 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeletePermanent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeletePermanent]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:Shushant		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sp_DeletePermanent
	-- Add the parameters for the stored procedure here
        
@userid int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

        Delete From tblusers 

                Where userid =@userid
       
 -- Insert statements for procedure here
	
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteProperty]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteProperty]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteProperty]
	-- Add the parameters for the stored procedure here
@PropId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

delete from tblPropertyImage	where propId=@propId
delete from tblProperties where propId=@propId
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeletePropertyImage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeletePropertyImage]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeletePropertyImage]
	-- Add the parameters for the stored procedure here


@PropImageID int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

delete from tblPropertyImage	where PropImageID=@PropImageID 


		
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteRate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_DeleteRate]
(

@rateid varchar(Max)
)
AS
BEGIN

delete from tblrates where rateid =@rateid
 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DisclosureAnalysisList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DisclosureAnalysisList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  Procedure [dbo].[sp_DisclosureAnalysisList]
AS
BEGIN
	select ufirstname + space(1) + ulastname as FullName,userid,coalesce(e3userid,'') e3userid from tblUsers where urole =21
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_E3GetProUserData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_E3GetProUserData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sp_E3GetProUserData
	-- Add the parameters for the stored procedure here
        
@username varchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select * from mwaMWUser where username =@username
 -- Insert statements for procedure here
	
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Email]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Email]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Email]
(
    @Role int ,
	@Subject varchar(1000) ,
	@EmailBodyText text ,
	@CreatedBy int ,
	@CreatedDate datetime ,
	@IsSent bit 
)
AS
BEGIN
	 INSERT INTO tblEmail (Subject,EmailBodyText,CreatedDate,Role,CreatedBy,IsSent)      
 VALUES      
  (@Subject,@EmailBodyText,@CreatedDate,@Role,@CreatedBy,@IsSent)  
END
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchActiveRealtors]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchActiveRealtors]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchActiveRealtors]
	-- Add the parameters for the stored procedure here


@urole int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

select * from tblusers where urole=@urole and isActive=1

		
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchActiveRealtorsbyGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchActiveRealtorsbyGroup]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchActiveRealtorsbyGroup]
	-- Add the parameters for the stored procedure here

(
@urole int,
@state nvarchar(10), 
@city nvarchar(100)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

select * from tblusers where state=@state and city=@city and urole=@urole

		
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchActiveRealtorsRedesign]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchActiveRealtorsRedesign]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchActiveRealtorsRedesign]
	-- Add the parameters for the stored procedure here

(
@urole int,
@state nvarchar(10), 
@city nvarchar(100)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


select * from tblusers where state=@state and city=@city and urole=@urole and isActive=1 and ispublish=1	
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchData_loanapp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchData_loanapp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchData_loanapp]

@loanid varchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from tblloanapp
Where loanid=@loanid
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchData_LoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchData_LoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchData_LoanApplication]
	-- Add the parameters for the stored procedure here
	@LoanApplicationID varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select * From tblLoanApplication where LoanApplicationID=@LoanApplicationID
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_FetchDataByMAID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_FetchDataByMAID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel
-- Create Date : 10/04/2012  
-- Description: Procedure for Fetch Data MaiD 
-- =============================================        
CREATE PROC  [dbo].[SP_FetchDataByMAID](         
@UserId int        
)                  
AS                  
BEGIN              
    select ufirstname + ' ' + ulastname as uname, userid  from tblusers where userid in(select userid from tblusers where urole = 2 and MAID=@UserId)  
             
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchDataForState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchDataForState]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchDataForState]
	-- Add the parameters for the stored procedure here

(
@state nvarchar(10),
@urole int
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

select * from tblusers where urole=@urole and isActive=1 and [state] in (@state) 


		
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchImages]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchImages]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchImages]
	-- Add the parameters for the stored procedure here
@PropId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

select * from dbo.tblPropertyImage where isPropImageDel = 1 and PropID = @PropId
select count('A') from dbo.tblPropertyImage where isPropImageDel = 1 and PropID = @PropId
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchImagesForProp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchImagesForProp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchImagesForProp]

@PropId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select * from tblPropertyImage where PropId =@PropId 
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchListingByPropID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchListingByPropID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchListingByPropID]

@PropId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select * from tblProperties where PropID = @PropId
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchListingImages]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchListingImages]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchListingImages]
(
@PropId int,
@PropImageId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select * from dbo.tblPropertyImage where isPropImageDel = 1 and PropID =   @PropId  and PropImageId not in(@PropImageId)

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchServiceBannerTest]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchServiceBannerTest]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Created By: <Amit Kumar>
-- CREATED date: <03/13/2013>
--Description: If Status=0 then Message is 'Expired'
--If Status=1 then Message is 'Active'
--If Status=2 then Message is 'Pending'
--If Status=3 then Message is 'Archive'
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchServiceBannerTest]
(
@Condition int,
@WhereCondition int
)
-- Add the parameters for the stored procedure here
AS
BEGIN


-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
--Start(to retrieve the all active and pending message)------------------------
if(@Condition=0)
begin

select ServiceBannerID,Message,PublishDate,ExpireDate, Status= case Status when '1' then 'Active' when '2' then 'Pending' END from tblServiceBanner where Status in(1,2) order by ExpireDate desc
end
--End(to retrieve the all active and pending message)------------------------

--Start(to retrieve the all active message)-------------------------------------
if(@Condition=1)
begin
select *from tblServiceBanner where Status=1
end
--End(to retrieve the all active message)-------------------------------------

--Start(to retrieve the all active and pending message)--------------------
if(@Condition=2)
begin

select *from tblServiceBanner where Status in(1,2)
end
--END(to retrieve the all active and pending message)--------------------------

--Start(to retrieve the all archive message)-----------------------------------
if(@Condition=3)
begin
select *from tblServiceBanner where Status=3 order by ExpireDate desc
end
--End(to retrieve the all archive message)-----------------------------------

--Start(to retrieve the all active and pending message for update time)------

if(@Condition=4)
begin

select *from tblServiceBanner where ServiceBannerID=@WhereCondition and Status in(1,2)
end
--End(to retrieve the all active and pending message for update time)--------

end


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_generate_insertsStatement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_generate_insertsStatement]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create  PROC [dbo].[sp_generate_insertsStatement]
(
            @table_name varchar(776),                          -- The table/view for which the INSERT statements will be generated using the existing data
            @target_table varchar(776) = NULL,             -- Use this parameter to specify a different table name into which the data will be inserted
            @include_column_list bit = 1,                        -- Use this parameter to include/ommit column list in the generated INSERT statement
            @from varchar(800) = NULL,                         -- Use this parameter to filter the rows based on a filter condition (using WHERE)
            @include_timestamp bit = 0,                         -- Specify 1 for this parameter, if you want to include the TIMESTAMP/ROWVERSION column's data in the INSERT statement
            @debug_mode bit = 0,                                   -- If @debug_mode is set to 1, the SQL statements constructed by this procedure will be printed for later examination
            @owner varchar(64) = NULL,             -- Use this parameter if you are not the owner of the table
            @ommit_images bit = 0,                                -- Use this parameter to generate INSERT statements by omitting the 'image' columns
            @ommit_identity bit = 0,                   -- Use this parameter to ommit the identity columns
            @top int = NULL,                                 -- Use this parameter to generate INSERT statements only for the TOP n rows
            @cols_to_include varchar(8000) = NULL,      -- List of columns to be included in the INSERT statement
            @cols_to_exclude varchar(8000) = NULL,     -- List of columns to be excluded from the INSERT statement
            @disable_constraints bit = 0,             -- When 1, disables foreign key constraints and enables them after the INSERT statements
            @ommit_computed_cols bit = 0                    -- When 1, computed columns will not be included in the INSERT statement
            
)
AS
BEGIN

/***********************************************************************************************************
Procedure:      sp_generate_inserts  (Build 22) 

                                          
Purpose:          To generate INSERT statements from existing data. 
                        These INSERTS can be executed to regenerate the data at some other location.
                        This procedure is also useful to create a database setup, where in you can 
                        script your data along with your table definitions.

Written by:     Manoj Kumar Soni
                    
Tested on:       SQL Server 7.0 and SQL Server 2000

Date created: Sep 15th 2009 21:52 GMT

Email:                         manojsoni80@gmail.com

NOTE:             This procedure may not work with tables with too many columns.
                        Results can be unpredictable with huge text columns or SQL Server 2000's sql_variant data types
                        Whenever possible, Use @include_column_list parameter to ommit column list in the INSERT statement, for better results
                        IMPORTANT: This procedure is not tested with internation data (Extended characters or Unicode). If needed
                        you might want to convert the datatypes of character variables in this procedure to their respective unicode counterparts
                        like nchar and nvarchar
                        

Example 1:      To generate INSERT statements for table 'titles':
                        
                        EXEC sp_generate_inserts 'titles'

Example 2:      To ommit the column list in the INSERT statement: (Column list is included by default)
                        IMPORTANT: If you have too many columns, you are advised to ommit column list, as shown below,
                        to avoid erroneous results
                        
                        EXEC sp_generate_inserts 'titles', @include_column_list = 0

Example 3:      To generate INSERT statements for 'titlesCopy' table from 'titles' table:

                        EXEC sp_generate_inserts 'titles', 'titlesCopy'

Example 4:      To generate INSERT statements for 'titles' table for only those titles 
                        which contain the word 'Computer' in them:
                        NOTE: Do not complicate the FROM or WHERE clause here. It's assumed that you are good with T-SQL if you are using this parameter

                        EXEC sp_generate_inserts 'titles', @from = "from titles where title like '%Computer%'"

Example 5:      To specify that you want to include TIMESTAMP column's data as well in the INSERT statement:
                        (By default TIMESTAMP column's data is not scripted)

                        EXEC sp_generate_inserts 'titles', @include_timestamp = 1

Example 6:      To print the debug information:
  
                        EXEC sp_generate_inserts 'titles', @debug_mode = 1

Example 7:      If you are not the owner of the table, use @owner parameter to specify the owner name
                        To use this option, you must have SELECT permissions on that table

                        EXEC sp_generate_inserts Nickstable, @owner = 'Nick'

Example 8:      To generate INSERT statements for the rest of the columns excluding images
                        When using this otion, DO NOT set @include_column_list parameter to 0.

                        EXEC sp_generate_inserts imgtable, @ommit_images = 1

Example 9:      To generate INSERT statements excluding (ommiting) IDENTITY columns:
                        (By default IDENTITY columns are included in the INSERT statement)

                        EXEC sp_generate_inserts mytable, @ommit_identity = 1

Example 10:    To generate INSERT statements for the TOP 10 rows in the table:
                        
                        EXEC sp_generate_inserts mytable, @top = 10

Example 11:    To generate INSERT statements with only those columns you want:
                        
                        EXEC sp_generate_inserts titles, @cols_to_include = "'title','title_id','au_id'"

Example 12:    To generate INSERT statements by omitting certain columns:
                        
                        EXEC sp_generate_inserts titles, @cols_to_exclude = "'title','title_id','au_id'"

Example 13:    To avoid checking the foreign key constraints while loading data with INSERT statements:
                        
                        EXEC sp_generate_inserts titles, @disable_constraints = 1

Example 14:    To exclude computed columns from the INSERT statement:
                        EXEC sp_generate_inserts MyTable, @ommit_computed_cols = 1
***********************************************************************************************************/

SET NOCOUNT ON

--Making sure user only uses either @cols_to_include or @cols_to_exclude
IF ((@cols_to_include IS NOT NULL) AND (@cols_to_exclude IS NOT NULL))
            BEGIN
                        RAISERROR('Use either @cols_to_include or @cols_to_exclude. Do not use both the parameters at once',16,1)
                        RETURN -1 --Failure. Reason: Both @cols_to_include and @cols_to_exclude parameters are specified
            END

--Making sure the @cols_to_include and @cols_to_exclude parameters are receiving values in proper format
IF ((@cols_to_include IS NOT NULL) AND (PATINDEX('''%''',@cols_to_include) = 0))
            BEGIN
                        RAISERROR('Invalid use of @cols_to_include property',16,1)
                        PRINT 'Specify column names surrounded by single quotes and separated by commas'
                        PRINT 'Eg: EXEC sp_generate_inserts titles, @cols_to_include = "''title_id'',''title''"'
                        RETURN -1 --Failure. Reason: Invalid use of @cols_to_include property
            END

IF ((@cols_to_exclude IS NOT NULL) AND (PATINDEX('''%''',@cols_to_exclude) = 0))
            BEGIN
                        RAISERROR('Invalid use of @cols_to_exclude property',16,1)
                        PRINT 'Specify column names surrounded by single quotes and separated by commas'
                        PRINT 'Eg: EXEC sp_generate_inserts titles, @cols_to_exclude = "''title_id'',''title''"'
                        RETURN -1 --Failure. Reason: Invalid use of @cols_to_exclude property
            END


--Checking to see if the database name is specified along wih the table name
--Your database context should be local to the table for which you want to generate INSERT statements
--specifying the database name is not allowed
IF (PARSENAME(@table_name,3)) IS NOT NULL
            BEGIN
                        RAISERROR('Do not specify the database name. Be in the required database and just specify the table name.',16,1)
                        RETURN -1 --Failure. Reason: Database name is specified along with the table name, which is not allowed
            END

--Checking for the existence of 'user table' or 'view'
--This procedure is not written to work on system tables
--To script the data in system tables, just create a view on the system tables and script the view instead

IF @owner IS NULL
            BEGIN
                        IF ((OBJECT_ID(@table_name,'U') IS NULL) AND (OBJECT_ID(@table_name,'V') IS NULL)) 
                                    BEGIN
                                                RAISERROR('User table or view not found.',16,1)
                                                PRINT 'You may see this error, if you are not the owner of this table or view. In that case use @owner parameter to specify the owner name.'
                                                PRINT 'Make sure you have SELECT permission on that table or view.'
                                                RETURN -1 --Failure. Reason: There is no user table or view with this name
                                    END
            END
ELSE
            BEGIN
                        IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @table_name AND (TABLE_TYPE = 'BASE TABLE' OR TABLE_TYPE = 'VIEW') AND TABLE_SCHEMA = @owner)
                                    BEGIN
                                                RAISERROR('User table or view not found.',16,1)
                                                PRINT 'You may see this error, if you are not the owner of this table. In that case use @owner parameter to specify the owner name.'
                                                PRINT 'Make sure you have SELECT permission on that table or view.'
                                                RETURN -1 --Failure. Reason: There is no user table or view with this name                        
                                    END
            END

--Variable declarations
DECLARE                     @Column_ID int,                    
                        @Column_List varchar(8000), 
                        @Column_Name varchar(128), 
                        @Start_Insert varchar(786), 
                        @Data_Type varchar(128), 
                        @Actual_Values varchar(8000),         --This is the string that will be finally executed to generate INSERT statements
                        @IDN varchar(128)                 --Will contain the IDENTITY column's name in the table

--Variable Initialization
SET @IDN = ''
SET @Column_ID = 0
SET @Column_Name = ''
SET @Column_List = ''
SET @Actual_Values = ''

IF @owner IS NULL 
            BEGIN
                        SET @Start_Insert = 'INSERT INTO ' + '[' + RTRIM(COALESCE(@target_table,@table_name)) + ']' 
            END
ELSE
            BEGIN
                        SET @Start_Insert = 'INSERT ' + '[' + LTRIM(RTRIM(@owner)) + '].' + '[' + RTRIM(COALESCE(@target_table,@table_name)) + ']'                    
            END


--To get the first column's ID

SELECT            @Column_ID = MIN(ORDINAL_POSITION)    
FROM INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
WHERE           TABLE_NAME = @table_name AND
(@owner IS NULL OR TABLE_SCHEMA = @owner)



--Loop through all the columns of the table, to get the column names and their data types
WHILE @Column_ID IS NOT NULL
            BEGIN
                        SELECT            @Column_Name = QUOTENAME(COLUMN_NAME), 
                        @Data_Type = DATA_TYPE 
                        FROM INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
                        WHERE            ORDINAL_POSITION = @Column_ID AND 
                        TABLE_NAME = @table_name AND
                        (@owner IS NULL OR TABLE_SCHEMA = @owner)



                        IF @cols_to_include IS NOT NULL --Selecting only user specified columns
                        BEGIN
                                    IF CHARINDEX( '''' + SUBSTRING(@Column_Name,2,LEN(@Column_Name)-2) + '''',@cols_to_include) = 0 
                                    BEGIN
                                                GOTO SKIP_LOOP
                                    END
                        END

                        IF @cols_to_exclude IS NOT NULL --Selecting only user specified columns
                        BEGIN
                                    IF CHARINDEX( '''' + SUBSTRING(@Column_Name,2,LEN(@Column_Name)-2) + '''',@cols_to_exclude) <> 0 
                                    BEGIN
                                                GOTO SKIP_LOOP
                                    END
                        END

                        --Making sure to output SET IDENTITY_INSERT ON/OFF in case the table has an IDENTITY column
                        IF (SELECT COLUMNPROPERTY( OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + @table_name),SUBSTRING(@Column_Name,2,LEN(@Column_Name) - 2),'IsIdentity')) = 1 
                        BEGIN
                                    IF @ommit_identity = 0 --Determing whether to include or exclude the IDENTITY column
                                                SET @IDN = @Column_Name
                                    ELSE
                                                GOTO SKIP_LOOP                               
                        END
                        
                        --Making sure whether to output computed columns or not
                        IF @ommit_computed_cols = 1
                        BEGIN
                                    IF (SELECT COLUMNPROPERTY( OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + @table_name),SUBSTRING(@Column_Name,2,LEN(@Column_Name) - 2),'IsComputed')) = 1 
                                    BEGIN
                                                GOTO SKIP_LOOP                                                       
                                    END
                        END
                        
                        --Tables with columns of IMAGE data type are not supported for obvious reasons
                        IF(@Data_Type in ('image'))
                                    BEGIN
                                                IF (@ommit_images = 0)
                                                            BEGIN
                                                                        RAISERROR('Tables with image columns are not supported.',16,1)
                                                                        PRINT 'Use @ommit_images = 1 parameter to generate INSERTs for the rest of the columns.'
                                                                        PRINT 'DO NOT ommit Column List in the INSERT statements. If you ommit column list using @include_column_list=0, the generated INSERTs will fail.'
                                                                        RETURN -1 --Failure. Reason: There is a column with image data type
                                                            END
                                                ELSE
                                                            BEGIN
                                                            GOTO SKIP_LOOP
                                                            END
                                    END

                        --Determining the data type of the column and depending on the data type, the VALUES part of
                        --the INSERT statement is generated. Care is taken to handle columns with NULL values. Also
                        --making sure, not to lose any data from flot, real, money, smallmomey, datetime columns
                        SET @Actual_Values = @Actual_Values  +
                        CASE 
                                    WHEN @Data_Type IN ('char','varchar','nchar','nvarchar') 
                                                THEN 
                                                            'COALESCE('''''''' + REPLACE(RTRIM(' + @Column_Name + '),'''''''','''''''''''')+'''''''',''NULL'')'
                                    WHEN @Data_Type IN ('datetime','smalldatetime') 
                                                THEN 
                                                            'COALESCE('''''''' + RTRIM(CONVERT(char,' + @Column_Name + ',109))+'''''''',''NULL'')'
                                    WHEN @Data_Type IN ('uniqueidentifier') 
                                                THEN  
                                                            'COALESCE('''''''' + REPLACE(CONVERT(char(255),RTRIM(' + @Column_Name + ')),'''''''','''''''''''')+'''''''',''NULL'')'
                                    WHEN @Data_Type IN ('text','ntext') 
                                                THEN  
                                                            'COALESCE('''''''' + REPLACE(CONVERT(char(8000),' + @Column_Name + '),'''''''','''''''''''')+'''''''',''NULL'')'                                                 
                                    WHEN @Data_Type IN ('binary','varbinary') 
                                                THEN  
                                                            'COALESCE(RTRIM(CONVERT(char,' + 'CONVERT(int,' + @Column_Name + '))),''NULL'')'  
                                    WHEN @Data_Type IN ('timestamp','rowversion') 
                                                THEN  
                                                            CASE 
                                                                        WHEN @include_timestamp = 0 
                                                                                    THEN 
                                                                                                '''DEFAULT''' 
                                                                                    ELSE 
                                                                                                'COALESCE(RTRIM(CONVERT(char,' + 'CONVERT(int,' + @Column_Name + '))),''NULL'')'  
                                                            END
                                    WHEN @Data_Type IN ('float','real','money','smallmoney')
                                                THEN
                                                            'COALESCE(LTRIM(RTRIM(' + 'CONVERT(char, ' +  @Column_Name  + ',2)' + ')),''NULL'')' 
                                    ELSE 
                                                'COALESCE(LTRIM(RTRIM(' + 'CONVERT(char, ' +  @Column_Name  + ')' + ')),''NULL'')' 
                        END   + '+' +  ''',''' + ' + '
                        
                        --Generating the column list for the INSERT statement
                        SET @Column_List = @Column_List +  @Column_Name + ','           

                        SKIP_LOOP: --The label used in GOTO

                        SELECT            @Column_ID = MIN(ORDINAL_POSITION) 
                        FROM INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
                        WHERE            TABLE_NAME = @table_name AND 
                        ORDINAL_POSITION > @Column_ID AND
                        (@owner IS NULL OR TABLE_SCHEMA = @owner)


            --Loop ends here!
            END

--To get rid of the extra characters that got concatenated during the last run through the loop
SET @Column_List = LEFT(@Column_List,len(@Column_List) - 1)
SET @Actual_Values = LEFT(@Actual_Values,len(@Actual_Values) - 6)

IF LTRIM(@Column_List) = '' 
            BEGIN
                        RAISERROR('No columns to select. There should at least be one column to generate the output',16,1)
                        RETURN -1 --Failure. Reason: Looks like all the columns are ommitted using the @cols_to_exclude parameter
            END

--Forming the final string that will be executed, to output the INSERT statements
IF (@include_column_list <> 0)
            BEGIN
                        SET @Actual_Values = 
                                    'SELECT ' +  
                                    CASE WHEN @top IS NULL OR @top < 0 THEN '' ELSE ' TOP ' + LTRIM(STR(@top)) + ' ' END + 
                                    '''' + RTRIM(@Start_Insert) + 
                                    ' ''+' + '''(' + RTRIM(@Column_List) +  '''+' + ''')''' + 
                                    ' +''VALUES(''+ ' +  @Actual_Values  + '+'')''' + ' ' + 
                                    COALESCE(@from,' FROM ' + CASE WHEN @owner IS NULL THEN '' ELSE '[' + LTRIM(RTRIM(@owner)) + '].' END + '[' + rtrim(@table_name) + ']' + '(NOLOCK)')
            END
ELSE IF (@include_column_list = 0)
            BEGIN
                        SET @Actual_Values = 
                                    'SELECT ' + 
                                    CASE WHEN @top IS NULL OR @top < 0 THEN '' ELSE ' TOP ' + LTRIM(STR(@top)) + ' ' END + 
                                    '''' + RTRIM(@Start_Insert) + 
                                    ' '' +''VALUES(''+ ' +  @Actual_Values + '+'')''' + ' ' + 
                                    COALESCE(@from,' FROM ' + CASE WHEN @owner IS NULL THEN '' ELSE '[' + LTRIM(RTRIM(@owner)) + '].' END + '[' + rtrim(@table_name) + ']' + '(NOLOCK)')
            END     

--Determining whether to ouput any debug information
IF @debug_mode =1
            BEGIN
                        PRINT '/*****START OF DEBUG INFORMATION*****'
                        PRINT 'Beginning of the INSERT statement:'
                        PRINT @Start_Insert
                        PRINT ''
                        PRINT 'The column list:'
                        PRINT @Column_List
                        PRINT ''
                        PRINT 'The SELECT statement executed to generate the INSERTs'
                        PRINT @Actual_Values
                        PRINT ''
                        PRINT '*****END OF DEBUG INFORMATION*****/'
                        PRINT ''
            END
                        
PRINT '--INSERTs generated by ''sp_generate_insertsStatement'' stored procedure written by Manoj'
PRINT '--Build number: 22'
PRINT '--Problems/Suggestions? Contact Manojsoni80@gmail.com'
PRINT ''
PRINT 'SET NOCOUNT ON'
PRINT ''


--Determining whether to print IDENTITY_INSERT or not
IF (@IDN <> '')
            BEGIN
                        PRINT 'SET IDENTITY_INSERT ' + QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + QUOTENAME(@table_name) + ' ON'
                        PRINT 'GO'
                        PRINT ''
            END


IF @disable_constraints = 1 AND (OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + @table_name, 'U') IS NOT NULL)
            BEGIN
                        IF @owner IS NULL
                                    BEGIN
                                                SELECT            'ALTER TABLE ' + QUOTENAME(COALESCE(@target_table, @table_name)) + ' NOCHECK CONSTRAINT ALL' AS '--Code to disable constraints temporarily'
                                    END
                        ELSE
                                    BEGIN
                                                SELECT            'ALTER TABLE ' + QUOTENAME(@owner) + '.' + QUOTENAME(COALESCE(@target_table, @table_name)) + ' NOCHECK CONSTRAINT ALL' AS '--Code to disable constraints temporarily'
                                    END

                        PRINT 'GO'
            END

PRINT ''
PRINT 'PRINT ''Inserting values into ' + '[' + RTRIM(COALESCE(@target_table,@table_name)) + ']' + ''''


--All the hard work pays off here!!! You'll get your INSERT statements, when the next line executes!
EXEC (@Actual_Values)

PRINT 'PRINT ''Done'''
PRINT ''


IF @disable_constraints = 1 AND (OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + @table_name, 'U') IS NOT NULL)
            BEGIN
                        IF @owner IS NULL
                                    BEGIN
                                                SELECT            'ALTER TABLE ' + QUOTENAME(COALESCE(@target_table, @table_name)) + ' CHECK CONSTRAINT ALL'  AS '--Code to enable the previously disabled constraints'
                                    END
                        ELSE
                                    BEGIN
                                                SELECT            'ALTER TABLE ' + QUOTENAME(@owner) + '.' + QUOTENAME(COALESCE(@target_table, @table_name)) + ' CHECK CONSTRAINT ALL' AS '--Code to enable the previously disabled constraints'
                                    END

                        PRINT 'GO'
            END

PRINT ''
IF (@IDN <> '')
            BEGIN
                        PRINT 'SET IDENTITY_INSERT ' + QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + QUOTENAME(@table_name) + ' OFF'
                        PRINT 'GO'
            END

PRINT 'SET NOCOUNT OFF'


SET NOCOUNT OFF
RETURN 0 --Success. We are done!
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_CareerPosting]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_CareerPosting]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Get_CareerPosting] 
(
@stringmode varchar(50)
)
AS
BEGIN
select CareerId,PositionName,SUBSTRING (Description,0,50) + '...' as Description,IsPublished,PublishedDate,ModifiedDate from tblCareerPosting
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_CareerPostingByID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_CareerPostingByID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Get_CareerPostingByID] 
(
@stringmode varchar(50),
@CareerId int
)
AS
BEGIN
select * from tblCareerPosting where CareerId = @CareerId
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_Managers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_Managers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Rakesh Mohanty>
-- Create date: <Create Date,12th May 2014>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Get_Managers] 
	@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT DISTINCT 
		UM.ManagerID, 
		U.ufirstname + ' ' + U.ulastname As UserName, 
		U.E3Userid,
		U.urole 
	FROM [dbo].[tblUserManagers] UM

	INNER JOIN [dbo].[tblUsers] U ON U.UserId = UM.ManagerID

	Where UM.Userid=@UserId
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_NthWorkingDayInMonth]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_NthWorkingDayInMonth]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- [dbo].[sp_Get_NthWorkingDayInMonth] 2014,1,6
Create PROCEDURE [dbo].[sp_Get_NthWorkingDayInMonth] 
(
@year int,
@month int,
@nthWorkingDay int
)

AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE	@StartDate DATETIME,
	@EndDate DATETIME

SELECT	@StartDate = convert(datetime, cast(@month as varchar)+'-01-'+cast(@year as varchar), 101),
	@EndDate = DATEADD(mm, 1, @StartDate) 

--print @StartDate;
--print @EndDate;

;WITH Yak(theDate)
AS (
	SELECT	@StartDate
	UNION ALL
	SELECT	DATEADD(DAY, 1, theDate)
	FROM	Yak
	WHERE	theDate < @EndDate
)

SELECT		y.theDate as NthBusinessDate,
		--w.WorkDate,
		w.nthBusinessDay
FROM		Yak AS y
LEFT JOIN	(
			SELECT	theDate AS WorkDate,
			ROW_NUMBER() OVER (PARTITION BY DATEDIFF(MONTH, '19000101', theDate) ORDER BY theDate) AS nthBusinessDay
			FROM	Yak
			WHERE	DATENAME(WEEKDAY, theDate) NOT IN ('Saturday', 'Sunday')
			and thedate not in (select Convert(varchar(10) , HolidayDate , 112) as HolidayDate from [tblHolidayList] where HolidayDate BETWEEN @StartDate AND @EndDate)
		) AS w ON w.WorkDate = y.theDate
where w.nthBusinessDay = @nthWorkingDay
ORDER BY	y.theDate
OPTION		(MAXRECURSION 0)

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_PublicTestimonial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_PublicTestimonial]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Get_PublicTestimonial] 
(
@stringmode varchar(50)
)
AS
BEGIN
select ID,IsPublish,Testimionial  from tblPublicTestimonial
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_PublishedPostings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_PublishedPostings]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Get_PublishedPostings] 
(
@stringmode varchar(50)
)
AS
BEGIN
select CareerId,PositionName,Description from tblCareerPosting where IsPublished = 1
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_PublishedTestimonial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_PublishedTestimonial]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Get_PublishedTestimonial] 
(
@stringmode varchar(50)
)
AS
BEGIN
select Testimionial  from tblPublicTestimonial where IsPublish = 1
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_Region_By_BranchOffice]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_Region_By_BranchOffice]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Rakesh Kumar Mohanty>
-- Create date: <Create Date,19th may 2014>
-- Description:	<Description,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Get_Region_By_BranchOffice]
	-- Add the parameters for the stored procedure here
	@E3UserId Nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	Declare @offfice Nvarchar(100)
Set @offfice=(select Office from  mwcInstitution c inner join mwaMWUser b ON  c.ID = b.BranchInstitution_ID
			  where b.id=@E3UserId and c.Office LIKE 'Retail%')

select region from [dbo].[tblBranchOffices] where Branch=@offfice

END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_Users_By_Role]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_Users_By_Role]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,Rakesh>
-- Create date: <Create Date, 7tn May 2014>
-- Description:	<Description, Select Records containing all Active users for specific Role from tblUsers table>
-- EXEC sp_Get_Users_By_Role 1
-- =============================================
CREATE PROCEDURE [dbo].[sp_Get_Users_By_Role]
	@RoleId Int = Null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
		U.ufirstname + ' ' + U.ulastname As UserName,
		U.userid
		
	FROM
		[dbo].[tblUsers] U 
	Where  (@RoleId IS NULL OR U.urole = @RoleId)
			AND U.IsActive = 1
END





GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get1098DataForAllLoansFor2009]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get1098DataForAllLoansFor2009]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_Get1098DataForAllLoansFor2009]
(
@loanid varchar (max)
)
AS
BEGIN
select loanid,reportedTIN as TaxIDNumber,ReportedFirstname as firstmiddlename,ReportedLastName as lastname,
MailLine1 as addressline1,MailLine2 as addressline2,MAilCity as city,MailState as state,MailZip as zip,
ReportableAmount AS totalintcoll,PointsPaid,ARMIntReimbursed,PMIPaid,PrincipalBal,princollected,NegativeAmBal,AssistanceBal, 
BeginEscBal,EscDepAmt,TaxesPaid1 , TaxesPaid,InsurancePaid,EscDisbAmt AS OtherFundsBal,escrowbal,IntOnEscrow,
IOEWithholding from dbo.r_annual_stmt
where reportingyear = '2009' and loanid in (@loanid) order by reportingdate desc 


END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get1098DataForAllLoansFor2010]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get1098DataForAllLoansFor2010]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [sp_Get1098DataForAllLoansFor2010]  0000309374
-- =============================================  
-- Author: Abhishek Rastogi  
-- ALTER Date : 10/05/2012  
-- Edited By : --  
-- Edited Date : --  
-- Description: for clsServiceInfo.cs BLL  
-- =============================================  
  
CREATE PROCEDURE [dbo].[sp_Get1098DataForAllLoansFor2010]  
(  
@loanid varchar (max)  
)  
AS  
BEGIN  
select eyear.loanid,reportedTIN as TaxIDNumber,annualStmt.ReportedFirstname as firstmiddlename,  
annualStmt.ReportedLastName as lastname,  annualStmt.MailLine1 as addressline1,annualStmt.MailLine2 as addressline2,  
annualStmt.MAilCity as city,annualStmt.MailState as state,annualStmt.MailZip as zip,  eyear.TotalIntColl AS totalintcoll,  
eyear.PointsPaid,annualStmt.ARMIntReimbursed,eyear.PMIPaid,annualStmt.PrincipalBal,annualStmt.princollected,  
annualStmt.NegativeAmBal,annualStmt.AssistanceBal,  
BeginEscBal,EscDepAmt,TaxesPaid1 , eyear.TaxesPaid,  
annualStmt.InsurancePaid,annualStmt.EscDisbAmt AS OtherFundsBal,annualStmt.escrowbal,annualStmt.IntOnEscrow,annualStmt.IOEWithholding   
from dbo.r_annual_stmt annualStmt  
Left outer join endofyear eyear  
on annualStmt.loanid=eyear.loanid   
and year(eyear.ReportingDate) = year(annualStmt.ReportingDate)  
where year(eyear.ReportingDate) = '2012' and  
eyear.loanid in (@loanid) order by eyear.reportingdate desc   
  
  
END  
  
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAdditionalDocs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAdditionalDocs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetAdditionalDocs]
(
@loan_no VARCHAR(MAX)
)
AS
BEGIN
select * from tblAdditionalUWDoc
where  rtrim(loan_no)=@loan_no
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAdditionalDocs2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAdditionalDocs2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetAdditionalDocs2]
(
@ctid VARCHAR(MAX)
)
AS
BEGIN

select * from tblAdditionalUWDoc where  ctid=@ctid
 
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getAdditionalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getAdditionalInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_getAdditionalInfo]
(
@loanid VARCHAR(MAX),
@TransactionDate VARCHAR(MAX)
)
AS
BEGIN
select TransactionCode,LoanID,TransactionAmt,TransactionDate from history
where transactioncode = 220
and month(TransactionDate) = month(@TransactionDate)
and year(TransactionDate) = year(@TransactionDate)
and loanid = @loanid  order by TransactionDate desc ;

 select top 1 TransactionCode,LoanID,TransactionAmt,TransactionDate from history 
 where transactioncode = 300 
 and month(TransactionDate) = month(@TransactionDate) 
 and year(TransactionDate) = year(@TransactionDate)
 and loanid = loanid order by TransactionDate desc ;

select top 1 TransactionCode,LoanID,TransactionAmt,TransactionDate from history 
where transactioncode = 240
and loanid = @loanid order by TransactionDate desc ;

select top 1 TransactionCode,LoanID,TransactionAmt,TransactionDate from history
where transactioncode = 260 
           
and loanid = @loanid order by TransactionDate desc ;


            
select top 1 TransactionCode,LoanID,TransactionAmt,TransactionDate from history 
where transactioncode = 310
            
and loanid = @loanid order by TransactionDate desc ;

select top 1 TransactionCode,LoanID,TransactionAmt,TransactionDate from history
where transactioncode = 320
           
and loanid = @loanid order by TransactionDate desc 

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAdditionalUWInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAdditionalUWInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetAdditionalUWInfo]
(
@loan_no VARCHAR(MAX)
)
AS
BEGIN

 select * from tblAdditionalUWDoc where  rtrim(loan_no)=@loan_no
 
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAlertInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAlertInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetAlertInfo]
(
@userid varchar(Max),
@loanno varchar(Max)
)
AS
BEGIN
select * from tblServiceInfo where userid = @userid and loanno = @loanno
     
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAllBrokerDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAllBrokerDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [sp_GetAllBrokerDoc] 30

-- =============================================  
-- Author: Abhishek Rastogi  
-- ALTER Date : 10/06/2012  
-- Edited By : --  
-- Edited Date : --  
-- Description: for clsLoanApplication.cs BLL  
-- =============================================  
  
CREATE PROCEDURE [dbo].[sp_GetAllBrokerDoc]  
(  
@NoOfDays VARCHAR(MAX)  
)  
AS  
BEGIN  
declare @a varchar(max)  
  
set @a='select loanregid as loanregid,'''' as  loandocid,'''' as Condid,'''' as SchID, FileName as AllFileName,
createdDate as createdDate,'''' as Description,lreg.Userid as Userid,uemailid as Submittedby  ,
(uFirstName + ''  '' + ulastname) as OlieName from tblloanreg 
lreg  
left outer join tblusers ureg on ureg.userid=lreg.userid where urole=3 and FileName <> '''' 
and  (lreg.isDeletedFromDoclog is NULL or lreg.isDeletedFromDoclog=1) and 
createdDate   >= (getdate()-' + @NoOfDays + ')
 and createdDate <=getdate()   
union  
select '''' as loanregid,loandocid as  loandocid,'''' as Condid,'''' as SchID, DocName as AllFileName,
createdDate as createdDate,Comments as Description,ldoc.Userid as Userid,uemailid as Submittedby  ,uFirstName
 + ''  '' +  ulastname as OlieName from tblloandoc ldoc 
  
left outer join tblusers udoc on ldoc.userid=udoc.userid where urole=3 and  DocName <> '''' and 
 (ldoc.isDeletedFromDoclog is NULL or ldoc.isDeletedFromDoclog=1) and 
createdDate   >= (getdate()-' + @NoOfDays + ') and createdDate <=getdate()  
union  
select '''' as loanregid,'''' as  loandocid,ctID as Condid,'''' as SchID, FileName as AllFileName,dttime as 
createdDate,Cond_Text as Description,'''' as Userid,uemailid as Submittedby  ,uFirstName + ''  '' +
 ulastname as OlieName from tblCondText_History conh   
 left outer join tblusers uconh on uconh.userid=conh.userid where  FileName <> '''' and urole=3 and 
 (conh.IsActive is NULL or conh.IsActive=1) and dttime   >= (getdate()-' + @NoOfDays + ') 
and dttime <=getdate()  
union  
select  '''' as loanregid,'''' as  loandocid,'''' as Condid,Scheduleid as SchID, FeeSheet as AllFileName,
 SubmitedDate as createdDate,Comments as Description,'''' as Userid,uemailid as Submittedby  ,uFirstName +
 '' '' + ulastname as OlieName from tblScheduleClosing sch   
left outer join tblusers usch on sch.userid= usch.userid where  FeeSheet <> ''''
 and urole=3 and  (sch.IsActive is NULL or sch.IsActive=1) 
and SubmitedDate   >= (getdate()-' + @NoOfDays + ') 
and SubmitedDate <=getdate() order by  createdDate desc'  
--print @a
exec(@a)  
END  
  
  
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAllMyPipelineReportChannelWise]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAllMyPipelineReportChannelWise]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetAllMyPipelineReportChannelWise]
AS
BEGIN
SELECT 'P' as DB,'' as DecisionStatus, loandat.record_id, loan_no, RTRIM(cobo.borr_last)  AS BorrowerLastName,
lt_loan_stats AS LoanStatus,case LU_FINAL_HUD_STATUS when '' then 'Pending' when NULL then 'Pending' else 'Approved' end as LU_FINAL_HUD_STATUS,
app_date AS SubmittedDate,appr_date AS DateApproved, lookups.descriptn AS TransType,lookupprogram.descriptn as LoanProgram,
loan_amt,  lookupsLock.descriptn AS LockStatus, LOANDAT2.CRED_SCORE_USED as CreditScoreUsed ,locks.lock_exp_date AS LockExpirationDate, 
RTRIM(users.first_name) + ' ' + users.last_name AS UnderWriter,rtrim(users2.first_name) + ' ' + Rtrim(users2.last_name) AS Closer, 
rtrim(brokers.brok_name) as BrokerName, case locks.LK_LU_CASHOUT when '0' then 'No' when '1' then 'Yes' end as  CashOut, 
case locks.LK_LU_CASHOUT when '0' then 'No' when '1' then 'Yes' end as  CashOut, convert(varchar(35),loandat2.DOC_EST_DATE,101) AS ScheduleDate,
loandat.lu_bus_type,bustype.descriptn BusType, CASE WHEN (SELECT count(a.record_id) as TotalConditions 
FROM dbo.condits a,lookups b  where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = 'aaw' and descriptn='PROCESSOR') 
and a.parent_id IN ( SELECT record_id FROM dbo.loandat as loan1  WHERE loan1.loan_no=loandat.loan_no)) = 0 then 0 
WHEN (SELECT count(a.record_id) as TotalCleared FROM dbo.condits a,lookups b 
where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = 'aaw' and descriptn='PROCESSOR') and ISNULL(a.cond_recvd_date,a.COND_CLEARED_DATE) IS NOT NULL
and a.parent_id IN ( SELECT record_id FROM dbo.loandat as loan2  WHERE loan2.loan_no=loandat.loan_no)) = 0 THEN 0 ELSE (SELECT count(a.record_id) as TotalCleared 
FROM dbo.condits a,lookups b  where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = 'aaw' and descriptn='PROCESSOR') and ISNULL(a.cond_recvd_date,a.COND_CLEARED_DATE) IS NOT NULL 
and a.parent_id IN ( SELECT record_id FROM dbo.loandat as loan2  WHERE loan2.loan_no=loandat.loan_no)) * 100 / (SELECT count(a.record_id) as TotalConditions 
FROM dbo.condits a,lookups b  where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = 'aaw' and descriptn='PROCESSOR') and a.parent_id IN ( SELECT record_id FROM dbo.loandat as loan1  WHERE loan1.loan_no=loandat.loan_no))END AS PER
FROM loandat INNER JOIN loandat2 ON dbo.loandat.record_id = dbo.loandat2.record_id INNER JOIN  cobo ON loandat.record_id = cobo.parent_id LEFT JOIN 
locks ON loandat.record_id = locks.parent_id LEFT JOIN brokers ON loandat.lt_broker = brokers.record_id LEFT JOIN users ON lt_usr_underwriter = users.record_id LEFT JOIN
proginfo ON loandat.lt_program = proginfo.record_id INNER JOIN lookups ON loandat.lu_purpose = lookups.id_Value and lookups.lookup_id = 'AAE' INNER JOIN 
lookups AS lookupprogram ON loandat.lu_loan_type = lookupprogram.id_value AND lookupprogram.lookup_id = 'AAC' Left JOIN lookups AS lookupsLock ON locks.LU_LOCK_STAT = lookupsLock.id_value AND lookupsLock.lookup_id = 'AAT'
left outer JOIN users as users2 on loandat2.LT_USR_ASSGNDRAWER = users2.record_id left JOIN lookups bustype ON loandat.lu_bus_type = bustype.id_Value and bustype.lookup_id = 'ACD' 
where lt_loan_stats  IN ('Locked','Application Received','Submitted to Underwriting','In Underwriting','Loan Suspended','Approved') AND cobo.primary_rec=1 
and loandat.lu_bus_type in (003,004)
ORDER BY PER desc
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetapplicationData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetapplicationData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--[sp_GetapplicationData] 1,50,'LoanApplicationID','([AppFName] like ''drew'')'  
CREATE PROCEDURE [dbo].[sp_GetapplicationData]   
(  
 @pageNum int  
,@pageSize int  
,@orderby varchar(2000)  
,@FilterExp varchar(2000)  
)AS  
  
SET NOCOUNT ON   
   
declare @lownum nvarchar(10)  
declare @highnum nvarchar(10)  
declare @sql nvarchar(4000)  
declare @sqlcount nvarchar(4000)   
set @lownum = convert(nvarchar(10), (@pagesize * (@pagenum - 1)))  
set @highnum = convert(nvarchar(10), (@pagesize * @pagenum))  

if @orderby = ''
set @orderby = 'DateEntered'


if @FilterExp = ''
Begin  
set @sql = 'select LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp, StateName   from (  
  select row_number() over (order by ' + @orderby  + ') as rownum  
         ,LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp,tblStateNylex.StateName   
  from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID    
  
 ) A WHERE RowNum > ' + @lownum + ' AND rownum <= ' + @highnum  
  print @sql
--WHERE rownum >  @pagesize * (@pageNum - 1) AND rownum <=  (@pagesize * @pageNum)  
end
else
Begin
set @sql = 'select LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp, StateName   from (  
  select row_number() over (order by ' + @orderby  + ') as rownum   
         ,LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp,tblStateNylex.StateName   
  from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID where ' + @FilterExp +' ) A WHERE RowNum > ' + @lownum + ' AND rownum <= ' + @highnum  
End  
print @sql
-- Execute the SQL query  

if @FilterExp <> ''
set @sqlcount = 'select count(loanapplicationID) as totalrec from tblLoanApplication  left join tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID where ' + @FilterExp         
else
set @sqlcount = 'select count(loanapplicationID) as totalrec from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID'  
  

EXEC sp_executesql @sql 
EXEC sp_executesql @sqlcount 
  



set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetapplicationData1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetapplicationData1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--[sp_GetapplicationData1] 2,50,'AppFName',''  
CREATE PROCEDURE [dbo].[sp_GetapplicationData1]   
(  
 @pageNum int  
,@pageSize int  
,@orderby varchar(2000)  
,@FilterExp varchar(2000)
)AS  
  
SET NOCOUNT ON   

declare @DESCorASC varchar(50)     
declare @lownum nvarchar(10)  
declare @highnum nvarchar(10)  
declare @sql nvarchar(4000)  
declare @sqlcount nvarchar(4000)   
set @lownum = convert(nvarchar(10), (@pagesize * (@pagenum - 1)))  
set @highnum = convert(nvarchar(10), (@pagesize * @pagenum))  

if @orderby = ''
begin
	set @orderby = 'DateEntered'
	set @DESCorASC = 'DESC'
end
else
	if @orderby = 'DateEntered'
	begin
		set @DESCorASC = 'DESC'
	end
	else
		set @DESCorASC = 'ASC'
print @DESCorASC
if @FilterExp = ''
Begin  
set @sql = 'select LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp, StateName   from (  
  select row_number() over (order by ' + @orderby + ' ' + @DESCorASC + ') as rownum  
         ,LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp,tblStateNylex.StateName   
  from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID    
  
 ) A WHERE RowNum > ' + @lownum + ' AND rownum <= ' + @highnum  
  --print @sql
--WHERE rownum >  @pagesize * (@pageNum - 1) AND rownum <=  (@pagesize * @pageNum)  
end
else
Begin
set @sql = 'select LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp, StateName   from (  
  select row_number() over (order by ' + @orderby + ' ' + @DESCorASC + ') as rownum   
         ,LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp,tblStateNylex.StateName   
  from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID where ' + @FilterExp +' ) A WHERE RowNum > ' + @lownum + ' AND rownum <= ' + @highnum  
End  
--print @sql
-- Execute the SQL query  

if @FilterExp <> ''
set @sqlcount = 'select count(loanapplicationID) as totalrec from tblLoanApplication  left join tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID where ' + @FilterExp         
else
set @sqlcount = 'select count(loanapplicationID) as totalrec from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID'  
  

EXEC sp_executesql @sql
EXEC sp_executesql @sqlcount 
  



set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAreamanager]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAreamanager]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_GetAreamanager]
(
	@ManagerId varchar(500) ,
	@Region varchar(500)
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SELECT t1.userid
		,ufirstname + space(1) + ulastname fullname
	FROM tblusers t1
	INNER JOIN tblUserManagers t2 ON t1.userid = t2.userid
		AND t1.isactive = 1
		AND t2.ManagerId IN (
			SELECT *
			FROM dbo.splitstring(@ManagerId, ',')
			)
		AND t1.region IN (
			SELECT *
			FROM dbo.splitstring(@Region, ',')
			)
	WHERE t1.urole = 19
	
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBorrowerLastName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBorrowerLastName]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetBorrowerLastName]
(
@loan_no VARCHAR(MAX)
)
AS
BEGIN
select cb.borr_last from loandat LD inner join cobo Cb on ld.record_id = cb.parent_id  where ld.loan_no = @loan_no
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBorrowerLastNameBrokerEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBorrowerLastNameBrokerEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetBorrowerLastNameBrokerEmail]
(
@loan_no VARCHAR(MAX)
)
AS
BEGIN

select cb.borr_last,user_email from loandat LD inner join cobo Cb on ld.record_id = cb.parent_id left outer join users on ld.lt_usr_underwriter = users.record_id  where ld.loan_no = @loan_no
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBorrowerName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBorrowerName]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetBorrowerName]
(
@record_id VARCHAR(MAX)
)
AS
BEGIN
SELECT parent_id,cond_text FROM dbo.condits 
WHERE record_id in (@record_id)

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBranch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBranch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================  

-- Author:  <Muruga Nagaraju>  

-- Create date: <30/01/2014>  

-- Description: <This wil give you all the "Branch" based on region>  

-- =============================================  

CREATE PROCEDURE [dbo].[sp_GetBranch] (@RegionID varchar(max) = NULL)

AS

BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

  SELECT

    BO.Id,

    BO.OfficeName,

    BO.OfficeValue,

    BO.BRANCH

  FROM tblBranchOffices BO

  INNER JOIN tblregion TR ON BO.region = TR.RegionValue

  WHERE (@RegionID IS NULL OR TR.RegionValue IN (SELECT * FROM dbo.SplitString(@RegionID,','))) AND (BO.BRANCH IS NOT NULL OR BO.BRANCH <> '')
  ORDER BY BO.BRANCH

END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBrokerDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBrokerDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetBrokerDoc]

AS
BEGIN
select *from tblLoanDocs LD 
inner join tblusers US on LD.Userid=US.Userid where urole=3
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBrokerInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBrokerInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetBrokerInfo]
(
@loan_no VARCHAR(MAX)
)
AS
BEGIN
select lt_broker from loandat where loan_no = @loan_no
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBrokerInfo2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBrokerInfo2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetBrokerInfo2]
(
@loan_no VARCHAR(MAX)
)
AS
BEGIN
select lt_broker from loandat where loan_no = @loan_no
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBusineesType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBusineesType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetBusineesType]

AS
BEGIN
select *from lookups where lookup_id='ACD'

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_GetCannedReportSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_GetCannedReportSummary]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Sp_GetCannedReportSummary]          
(          
@RoleID int,         
@strID varchar(2000),          
@BrokerId char(2),        
@TeamOPuids varchar(30),         
@Year char(4),          
@Channel varchar(15),        
@StatusDesc varchar(1000),      
@CurrentStatus varchar(1000)   
)          
AS          
BEGIN          
          
Declare @SQL nvarchar(max)          
Set @SQL =''          
Set @SQL ='SELECT case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram ,  
                 count(TransTYpe)CountRefinanance, case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when   
    TransTYpe = ''R'' then ''Refinance'' end as descriptn,       
                 month(AppStatus.StatusDateTime) as MonthYear,count(AppStatus.StatusDateTime)Total   
                 from mwlLoanApp  loanapp     
                 Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id   
                 Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc=''' + @StatusDesc + ''''   
  
                if (@BrokerId != '' AND @BrokerId != 0)  
     BEGIN  
      Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker'''   
      Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'''   
     END  
  
                   Set @SQL = @SQL + ' where loanapp.CurrentStatus in ( ''' + @CurrentStatus + ''') '   
  
                if (@BrokerId != '' AND @BrokerId != 0)  
                   
                   Set @SQL = @SQL + '  (Broker.CompanyEmail =''' + @BrokerId + ''' or Corres.CompanyEmail =''' + @BrokerId + ''') and '  
                   
  
                if (@Channel =  'broker')  
                   
                   Set @SQL = @SQL + '   and loanapp.ChannelType=''BROKER'''   
                   
                if (@Channel =  'retail')  
                   
                     Set @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL'''   
                   
                if (@Channel =  'correspond')  
                   
                     Set @SQL = @SQL + ' and loanapp.ChannelType=''CORRESPOND'''  
                   
  
                if (@RoleID =  1)  
                    
                     Set @SQL = @SQL + ' and loanapp.ChannelType like ''%RETAIL%'''    
                   
  
                if (@TeamOPuids != '')  
                   
                     Set @SQL = @SQL + ' and Originator_id in (''' + @TeamOPuids + ''') '  
                   
                else  
                BEGIN  
                    if (@RoleID != 0 AND @RoleID != 1)  
                    BEGIN  
                        if (len(@strID) = 0)  
      BEGIN    
                            if (@RoleID =  2)  
                               
                                Set @SQL = @SQL + '  and (Originator_id in ('''+ @strID +'''))'  
                         END   
                        else  
                          BEGIN  
                            if (@RoleID =  2)  
                               
                                Set @SQL = @SQL + '  and (Originator_id in ('''+ @strID +'''))'  
          END        
                           
                    END  
                END  
                 Set @SQL = @SQL + ' and  Year(AppStatus.StatusDateTime) <> ''1900'' and  Year(AppStatus.StatusDateTime)=''' + @Year + ''' group by '  
  
                if (@BrokerId != '' AND @BrokerId != 0)  
                BEGIN  
                     Set @SQL = @SQL + ' Broker.CompanyEmail ,Corres.CompanyEmail , '  
    END  
  
                Set @SQL = @SQL + '  month(AppStatus.StatusDateTime),LoanData.FinancingType, TransTYpe '    
    Set @SQL = @SQL + '  SELECT month(AppStatus.StatusDateTime) as ARmonth, year(AppStatus.StatusDateTime) as ARYear  ,  count(loanapp.CurrentStatus) AS Total, sum(isnull(LoanData.AdjustedNoteAmt, 0)) as SumTotalAmount, round(avg(isnull(LoanData.AdjustedN
oteAmt, 0)),0) as AvrageTotalAmount, '  
    Set @SQL = @SQL + '  avg(convert(int,isnull(replace(borr.CompositeCreditScore,'','',''''),0)))  as AvrageCreditScore ,round(avg(convert(int, isnull(loanData.ltv,0))),2) as AvgLTV  '  
    Set @SQL = @SQL + ' from mwlLoanApp  loanapp '  
    Set @SQL = @SQL + ' Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and  borr.sequencenum=1 '  
    Set @SQL = @SQL + ' Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id   '  
    Set @SQL = @SQL + ' Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc='''+ @StatusDesc + ''''  
  
                if (@BrokerId != '' AND @BrokerId != 0)  
     BEGIN  
                     Set @SQL = @SQL + '  Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker'' '  
                      Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' '  
     END  
  
                Set @SQL = @SQL + '  where loanapp.CurrentStatus in ( '''+ @CurrentStatus + ''') '   
  
                  if (@BrokerId != '' AND @BrokerId != 0)  
     BEGIN  
                     Set @SQL = @SQL + ' (Broker.CompanyEmail =''' + @BrokerId + ''' or Corres.CompanyEmail =''' + @BrokerId + ''' ) and '  
     END  
  
                if (@Channel =  'broker')  
                   
                      Set @SQL = @SQL + ' and loanapp.ChannelType=''BROKER'''  
                   
                if (@Channel =  'retail')  
                   
                     Set @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL'''   
                   
                if (@Channel =  'correspond')  
                   
                      Set @SQL = @SQL + 'and loanapp.ChannelType=''CORRESPOND'''    
                if (@RoleID =  1)  
                   
                      Set @SQL = @SQL + ' and loanapp.ChannelType like ''%RETAIL%'''    
                if (@TeamOPuids != '')  
                 BEGIN  
                       Set @SQL = @SQL + '  and Originator_id in (''' +   @TeamOPuids +''') '  
                 END  
                else  
                BEGIN  
                    if (@RoleID != 0 AND @RoleID != 1)  
                    BEGIN  
                       if (len(@strID) = 0)  
                        BEGIN  
                            if (@RoleID =  2)   
                                   Set @SQL = @SQL + '  and (Originator_id in (''' + @strID + '''))'  
                               
                        END  
                        else  
                        BEGIN  
                            if (@RoleID =  2)  
                              
                                  Set @SQL = @SQL + '   and (Originator_id in (''' + @strID + '''))'   
                        END  
                    END  
                END  
                  Set @SQL = @SQL + '   and Year(AppStatus.StatusDateTime) <> ''1900'' and Year(AppStatus.StatusDateTime)='''+ @Year + ''' group by '   
  
                if (@BrokerId != '' AND @BrokerId != 0)  
                BEGIN  
                       Set @SQL = @SQL + '  Broker.CompanyEmail ,Corres.CompanyEmail ,'   
                END  
                  Set @SQL = @SQL + '   month(AppStatus.StatusDateTime),year(AppStatus.StatusDateTime),LoanData.FinancingType, TransTYpe '   
  
    
 Print @SQL          
exec sp_executesql @SQL          
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCannedReportSummaryDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCannedReportSummaryDetail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [sp_GetCannedReportSummaryDetail] 1,'','funded','','','01 - Registered','2011','','4'    
      
CREATE PROCEDURE [dbo].[sp_GetCannedReportSummaryDetail]          
(          
@RoleID int,         
@strID varchar(2000),           
@From varchar(15),        
@BrokerId char(2)= null,        
@TeamOPuids varchar(30),        
@StatusDesc varchar(100),        
@Year varchar(4),         
@oPuid varchar(100),        
@Month varchar(4)        
)          
AS          
BEGIN          
          
Declare @SQL nvarchar(max)          
Set @SQL =''          
Set @SQL ='select ''E3'' as DB,loanapp.ID as record_id,CONVERT(DECIMAL(10,3),(loanData.NoteRate * 100)) as int_rate,LoanNumber as loan_no,        
   borr.lastname as BorrowerLastName,        
   CONVERT(DECIMAL(10,2),isnull(loanData.LTV * 100,0))LTV,CurrentStatus as LoanStatus,convert(varchar(35),        
   loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,case LoanData.FinancingType WHEN ''F'' THEN ''FHA''         
   WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,         
   case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as Purpose,          
   CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,loanData.UseCashInOut as CashOut,loanData.AdjustedNoteAmt as loan_amt,'         
        
                if (@From = 'locked')         
                     Set @SQL = @SQL + ' LockRecord.Status as LockStatus,LockRecord.LockExpirationDate,LockRecord.LockDateTime,LockRecord.LockDateTime as LOCK_DATE '                        
                else         
                     Set @SQL = @SQL + '   '''' as LockStatus,loanapp.LockExpirationDate,loanapp.LockDate as LOCK_DATE'        
                        
    Set @SQL = @SQL + ' from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and  borr.sequencenum=1  '        
    Set @SQL = @SQL + ' Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id  '        
    Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''Branch'' and Broker.objownerName=''BranchInstitution'' '        
    Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' '        
    Set @SQL = @SQL + ' Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc=''' + @StatusDesc +'''  and AppStatus.sequencenum=1 '        
        
               if (@From = 'locked')        
    BEGIN        
                    Set @SQL = @SQL + ' left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.locktype=''LOCK'''         
                END        
        
                Set @SQL = @SQL + ' where borr.sequencenum=1 and  loanapp.ChannelType=''RETAIL'' and '        
           
    
                if (@BrokerId != '' and @BrokerId != 0)        
                BEGIN        
                      Set @SQL = @SQL + '(Broker.CompanyEmail ='''+ @BrokerId +''' or Corres.CompanyEmail ='''+ @BrokerId +''') and '        
                END        
        
                if (@From = 'funded')        
               BEGIN        
                        Set @SQL = @SQL + ' Year(AppStatus.StatusDateTime)='''+ @Year +''' and Month(AppStatus.StatusDateTime)='''+ @Month +''''        
                END        
                else if (@From = 'locked')        
                BEGIN        
                      Set @SQL = @SQL + ' Year(LockRecord.LockDateTime)='''+ @Year +''' and Month(LockRecord.LockDateTime)='''+ @Month +''''        
                END        
        
                if (@TeamOPuids = 'broker')        
                BEGIN        
                      Set @SQL = @SQL + ' and loanapp.ChannelType=''BROKER'''         
                END        
                if (@TeamOPuids = 'retail')        
     BEGIN        
                       Set @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL'''         
                END        
                if (@TeamOPuids = 'correspond')        
                BEGIN        
                       Set @SQL = @SQL + ' and loanapp.ChannelType=''CORRESPOND'''         
                END        
                        
                Set @SQL = @SQL + ' and loanapp.ChannelType like ''%RETAIL%'' '         
                         
                if (@TeamOPuids != '' and @TeamOPuids != 'retail' and @TeamOPuids != 'broker' and @TeamOPuids != 'correspond')        
                BEGIN        
                     Set @SQL = @SQL + '  and (Originator_id in (''' + @TeamOPuids  + ''')or loanapp.id in (''' + @strID + ''')or Broker.office in('''+ @TeamOPuids +'''))'        
                END        
                else        
                  BEGIN        
                    if (@RoleID != 0 and @RoleID != 1 and @RoleID != 11)        
     BEGIN        
                         if (len(@strID) = 0)         
   BEGIN        
                                     
        if (@RoleID = 3)        
                                      
         Set @SQL = @SQL + '  and (loanapp.ID in (''' + @strID + ''') or Originator_id in (''' + @oPuid + ''') )'        
                                    
        
        if (@RoleID = 10 or  @RoleID = 7)         
          
         Set @SQL = @SQL + '  and (Originator_id in (''' + @strID + ''') or loanapp.id in (''' + @strID + '''))'        
                                      
        if (@RoleID = 2)         
         Set @SQL = @SQL + '  and (Originator_id in (''' + @strID + '''))'         
                                    
       END        
                        else        
       BEGIN         
        if (@RoleID = 3)         
           Set @SQL = @SQL + '  and (loanapp.ID in (''' + @strID + ''') or Originator_id in (''' + @oPuid + ''') )'        
                                      
        if (@RoleID =  10 or @RoleID = 7)        
                                   Set @SQL = @SQL + ' and (Originator_id in (''' + @strID + ''') or loanapp.id in (''' + @strID + '''))'        
                                      
        if (@RoleID = 2)         
                                  Set @SQL = @SQL + ' and (Originator_id in (''' + @strID + '''))'        
       END          
                         END        
     END        
                          
            
Print @SQL          
exec sp_executesql @SQL          
END 
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCannedReportSummaryDetail1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCannedReportSummaryDetail1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  
CREATE PROCEDURE [dbo].[sp_GetCannedReportSummaryDetail1]      
(      
@RoleID int,     
@strID varchar(2000),       
@From varchar(15),    
@Year varchar(4),     
@Month varchar(4)    
)      
AS      
BEGIN      
      
Declare @SQL nvarchar(max)      
Set @SQL =''      
Set @SQL ='select ''E3'' as DB,loanapp.ID as record_id,CONVERT(DECIMAL(10,3),(loanData.NoteRate * 100)) as int_rate,LoanNumber as loan_no,borr.lastname as BorrowerLastName,
   CONVERT(DECIMAL(10,2),isnull(loanData.LTV * 100,0))LTV,CurrentStatus as LoanStatus,convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram, case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as Purpose,  
    CAST(CreditScore AS CHAR(5)) as CreditScoreUsed,loanData.UseCashInOut as CashOut,isnull(LoanData.AdjustedNoteAmt, LoanData.AppraisedValue) as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockExpirationDate,LockRecord.LockDateTime as LOCK_DATE  
   from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id 
    Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
    Left join dbo.mwlCreditScore as BorrCredit on BorrCredit.borrower_id = borr.id   
    Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc=''16 - Funded'' 
    left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id
	where'
                if (@From = 'locked')     
                     Set @SQL = @SQL + ' CurrentStatus IN (''16 - Funded'',''17 - In Shipping'',''18 - Loan Shipped'',''19 - Investor Suspense'',''21 - Final Docs Completed'',''20 - Loan Sold'') and Year(AppStatus.StatusDateTime)=''' + @Year + ''' and Month(AppStatus.StatusDateTime)=''' + @Month + ''''                    
                else     
                     Set @SQL = @SQL + ' Year(LockRecord.LockDateTime)=''' + @Year + ''' and Month(LockRecord.LockDateTime)=''' + @Month + ''''    
                    
   if (len(@strID) = 0)    
  Begin    
	if(@RoleID !=0)
	begin
    Set @SQL = @SQL + ' and (Originator_id in (''' + @strID + '''))'    
	end
  End    
 else   
   Begin    
   if(@RoleID !=0)
	begin
    Set @SQL = @SQL + ' and (Originator_id in (''' + @strID + '''))'    
	end    
   End    
              
                      
        
Print @SQL      
exec sp_executesql @SQL      
END 


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCannedReportSummaryDetailForFallout]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCannedReportSummaryDetailForFallout]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[sp_GetCannedReportSummaryDetailForFallout]        
(        
@RoleID int,       
@strID varchar(2000),        
@BrokerId char(2),      
@TeamOPuids varchar(30),          
@Year char(4),       
@oPuid varchar(100),      
@Month char(4)      
)        
AS        
BEGIN        
        
Declare @SQL nvarchar(max)        
Set @SQL =''        
Set @SQL =' select ''E3'' as DB, loanapp.ID as record_id,CONVERT(DECIMAL(10,3),(loanData.NoteRate * 100)) as int_rate,LoanNumber as loan_no,      
   borr.lastname as BorrowerLastName,      
   CONVERT(DECIMAL(10,2),isnull(loanData.LTV * 100,0))LTV,CurrentStatus as LoanStatus,convert(varchar(35),loanapp.DateCreated,101)      
   AS DateCreated,DateAppSigned as DateApproved,case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL''       
   WHEN ''V'' THEN  ''VA'' End as LoanProgram, case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase''       
   when TransTYpe = ''R'' then ''Refinance'' end as Purpose, CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,loanData.UseCashInOut      
   as CashOut,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockExpirationDate,LockRecord.LockDateTime,      
   LockRecord.LockDateTime as LOCK_DATE     from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and       
   borr.sequencenum=1    Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id        
   Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''Branch''       
   and Broker.objownerName=''BranchInstitution'' Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and       
   Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''       
   left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.locktype=''LOCK'' and      
   LockRecord.Status<>''CANCELED''       
            where borr.sequencenum=1 and Year(LockRecord.LockDateTime)='''+ @Year +''' and Month(LockRecord.LockDateTime)='''+ @Month +''''      
      
                if (@BrokerId != '' and @BrokerId != 0)      
                       
                   Set @SQL = @SQL + ' and (Broker.CompanyEmail ='+ @BrokerId +' or Corres.CompanyEmail ='+ @BrokerId +')'       
                       
      
                if (@TeamOPuids = 'broker')      
                       
                     Set @SQL = @SQL + ' and loanapp.ChannelType=''BROKER'' '      
                       
                if (@TeamOPuids = 'retail')      
                       
                     Set @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL'''       
                       
                if (@TeamOPuids = 'correspond')      
                       
                   Set @SQL = @SQL + ' and loanapp.ChannelType=''CORRESPOND'''       
                       
      
                      
                 Set @SQL = @SQL + ' and loanapp.ChannelType like ''%RETAIL%'''        
                       
      
                if (@TeamOPuids != '' and @TeamOPuids != 'retail' and @TeamOPuids != 'broker' and @TeamOPuids != 'correspond')      
                BEGIN      
                    Set @SQL = @SQL + ' and (Originator_id in (' + @TeamOPuids +')or loanapp.id in (select * from dbo.SplitString('''+ @strID +''','',''))or Broker.office in('''+ @TeamOPuids +'''))'      
                END      
                else      
                  BEGIN      
                    if (@RoleID != 0 and @RoleID != 1)      
     BEGIN       
                        if (len(@strID) = 0)      
                        BEGIN      
                            if (@RoleID =  3)      
                                   
                                 Set @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString('''+ @strID +''','','')) or Originator_id in (''' + @oPuid + ''') )'      
                                   
                                 
                            if (@RoleID =  2)    
                                   
                                 Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+ @strID +''','','')))'      
                     
                            if (@RoleID = 10 or @RoleID  = 7)       
                                 Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+ @strID +''','','')) or loanapp.id in (select * from dbo.SplitString('''+ @strID +''','','')))'      
                                     
                        END      
                              
                        else      
                        BEGIN      
                            if (@RoleID = 3)      
                                   
                               Set @SQL = @SQL + '  and (loanapp.ID in (select * from dbo.SplitString('''+ @strID +''','','')) or Originator_id in (''' + @oPuid + ''') )'      
                                   
                                  
                            if (@RoleID = 2)      
                                   
                               Set @SQL = @SQL + '   and (Originator_id in (select * from dbo.SplitString('''+ @strID +''','','')))'      
                                   
                            if (@RoleID  = 10 or @RoleID =  7)       
       
                               Set @SQL = @SQL + '  and (Originator_id in (select * from dbo.SplitString('''+ @strID +''','','')) or loanapp.id in (select * from dbo.SplitString('''+ @strID +''','','')))'      
                                    
                       END      
     END      
     END       
          
Print @SQL        
exec sp_executesql @SQL        
END   


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCannedReportSummaryDetailNEW]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCannedReportSummaryDetailNEW]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [sp_GetCannedReportSummaryDetail] 0,'','','','','','','',''  
    
CREATE PROCEDURE [dbo].[sp_GetCannedReportSummaryDetailNEW]        
(        
@RoleID int,       
@strID varchar(2000),         
@From varchar(15),      
@BrokerId char(2)= null,      
@TeamOPuids varchar(30),      
@StatusDesc varchar(100),      
@Year varchar(4),       
@oPuid varchar(100),      
@Month varchar(4)      
)        
AS        
BEGIN        
        
Declare @SQL nvarchar(max)        
Set @SQL =''        
Set @SQL ='select ''E3'' as DB,loanapp.ID as record_id,CONVERT(DECIMAL(10,3),(loanData.NoteRate * 100)) as int_rate,LoanNumber as loan_no,      
   borr.lastname as BorrowerLastName,      
   CONVERT(DECIMAL(10,2),isnull(loanData.LTV * 100,0))LTV,CurrentStatus as LoanStatus,convert(varchar(35),      
   loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,case LoanData.FinancingType WHEN ''F'' THEN ''FHA''       
   WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,       
   case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as Purpose,        
   CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,loanData.UseCashInOut as CashOut,loanData.AdjustedNoteAmt as loan_amt,'       
      
                if (@From = 'locked')       
                     Set @SQL = @SQL + ' LockRecord.Status as LockStatus,LockRecord.LockExpirationDate,LockRecord.LockDateTime,LockRecord.LockDateTime as LOCK_DATE '                      
                else       
                     Set @SQL = @SQL + '   '''' as LockStatus,loanapp.LockExpirationDate,loanapp.LockDate as LOCK_DATE'      
                      
    Set @SQL = @SQL + ' from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and  borr.sequencenum=1  '      
    Set @SQL = @SQL + ' Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id  '      
    Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''Branch'' and Broker.objownerName=''BranchInstitution'' '      
    Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' '      
    Set @SQL = @SQL + ' Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc=''' + @StatusDesc +'''  and AppStatus.sequencenum=1 '      
      
               if (@From = 'locked')      
    BEGIN      
                    Set @SQL = @SQL + ' left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.locktype=''LOCK'''       
                END      
      
                Set @SQL = @SQL + ' where borr.sequencenum=1 and  loanapp.ChannelType=''RETAIL'' and '      
         
  
                if (@BrokerId != '' and @BrokerId != 0)      
                BEGIN      
                      Set @SQL = @SQL + '(Broker.CompanyEmail ='''+ @BrokerId +''' or Corres.CompanyEmail ='''+ @BrokerId +''') and '      
                END      
      
                if (@From = 'funded')      
               BEGIN      
                        Set @SQL = @SQL + ' Year(AppStatus.StatusDateTime)='''+ @Year +''' and Month(AppStatus.StatusDateTime)='''+ @Month +''''      
                END      
                else if (@From = 'locked')      
                BEGIN      
                      Set @SQL = @SQL + ' Year(LockRecord.LockDateTime)='''+ @Year +''' and Month(LockRecord.LockDateTime)='''+ @Month +''''      
                END      
      
                if (@TeamOPuids = 'broker')      
                BEGIN      
                      Set @SQL = @SQL + ' and loanapp.ChannelType=''BROKER'''       
                END      
                if (@TeamOPuids = 'retail')      
                BEGIN      
                       Set @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL'''       
                END      
                if (@TeamOPuids = 'correspond')      
                BEGIN      
                       Set @SQL = @SQL + ' and loanapp.ChannelType=''CORRESPOND'''       
                END      
                      
                Set @SQL = @SQL + ' and loanapp.ChannelType like ''%RETAIL%'' '       
                       
                if (@TeamOPuids != '' and @TeamOPuids != 'retail' and @TeamOPuids != 'broker' and @TeamOPuids != 'correspond')      
                BEGIN      
                     Set @SQL = @SQL + '  and (Originator_id in (''' + @TeamOPuids  + ''')or loanapp.id in (''' + @strID + ''')or Broker.office in('''+ @TeamOPuids +'''))'      
                END      
                else      
                  BEGIN      
                    if (@RoleID != 0 and @RoleID != 1 and @RoleID != 11)      
     BEGIN      
                         if (len(@strID) = 0)       
   BEGIN      
                                   
        if (@RoleID = 3)      
                                    
         Set @SQL = @SQL + '  and (loanapp.ID in (''' + @strID + ''') or Originator_id in (''' + @oPuid + ''') )'      
                                  
      
        if (@RoleID = 10 or  @RoleID = 7)       
        
         Set @SQL = @SQL + '  and (Originator_id in (''' + @strID + ''') or loanapp.id in (''' + @strID + '''))'      
                                    
        if (@RoleID = 2)       
         Set @SQL = @SQL + '  and (Originator_id in (''' + @strID + '''))'       
                                  
       END      
                        else      
       BEGIN       
        if (@RoleID = 3)       
           Set @SQL = @SQL + '  and (loanapp.ID in (''' + @strID + ''') or Originator_id in (''' + @oPuid + ''') )'      
                                    
        if (@RoleID =  10 or @RoleID = 7)      
                                   Set @SQL = @SQL + ' and (Originator_id in (''' + @strID + ''') or loanapp.id in (''' + @strID + '''))'      
                                    
        if (@RoleID = 2)       
                                  Set @SQL = @SQL + ' and (Originator_id in (''' + @strID + '''))'      
       END        
                         END      
     END      
                        
          
Print @SQL        
exec sp_executesql @SQL        
END   
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCannedReportSummaryDetailNew1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCannedReportSummaryDetailNew1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [sp_GetCannedReportSummaryDetail] 0,'','','','','','','',''  
    
CREATE PROCEDURE [dbo].[sp_GetCannedReportSummaryDetailNew1]        
(        
@RoleID int,       
@strID varchar(2000),         
@From varchar(15),      
@BrokerId char(2)= null,      
@TeamOPuids varchar(30),      
@Year varchar(4),       
@oPuid varchar(100),      
@Month varchar(4)      
)        
AS        
BEGIN        
        
Declare @SQL nvarchar(max)        
Set @SQL =''        
Set @SQL ='select ''E3'' as DB,loanapp.ID as record_id,CONVERT(DECIMAL(10,3),(loanData.NoteRate * 100)) as int_rate,LoanNumber as loan_no,borr.lastname as BorrowerLastName,
    CONVERT(DECIMAL(10,2),isnull(loanData.LTV * 100,0))LTV,CurrentStatus as LoanStatus,convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram, case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as Purpose,  
    CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,loanData.UseCashInOut as CashOut,loanData.AdjustedNoteAmt as loan_amt,'
   
  if (@From = 'locked')       
	begin
          Set @SQL = @SQL + ' LockRecord.Status as LockStatus,LockRecord.LockExpirationDate,LockRecord.LockDateTime,LockRecord.LockDateTime as LOCK_DATE  '                      
	end
   else
	begin       
        Set @SQL = @SQL + '  '''' as LockStatus,loanapp.LockExpirationDate,loanapp.LockDate as LOCK_DATE'      
	end
                      
    Set @SQL = @SQL + ' from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and  borr.sequencenum=1    '      
    Set @SQL = @SQL + ' Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id '      
    Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''Branch'' and Broker.objownerName=''BranchInstitution'' '      
    Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' '      
    Set @SQL = @SQL + ' Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc=''62 - Funded''  and AppStatus.sequencenum=1 '      
      
  if (@From = 'locked')      
    BEGIN      
    Set @SQL = @SQL + ' left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.locktype=''LOCK'''       
    END      
     Set @SQL = @SQL + ' where borr.sequencenum=1 and  loanapp.ChannelType=''RETAIL'' and '      
         
  
                if (@BrokerId != '' and @BrokerId != 0)      
                BEGIN      
                      Set @SQL = @SQL + '(Broker.CompanyEmail ='''+ @BrokerId +''' or Corres.CompanyEmail ='''+ @BrokerId +''') and '      
                END      
      
                if (@From = 'funded')      
               BEGIN      
                        Set @SQL = @SQL + ' Year(AppStatus.StatusDateTime)='''+ @Year +''' and Month(AppStatus.StatusDateTime)='''+ @Month +''''      
                END      
                else if (@From = 'locked')      
                BEGIN      
                      Set @SQL = @SQL + ' Year(LockRecord.LockDateTime)='''+ @Year +''' and Month(LockRecord.LockDateTime)='''+ @Month +''''      
                END      
      
                if (@TeamOPuids = 'broker')      
                BEGIN      
                      Set @SQL = @SQL + ' and loanapp.ChannelType=''BROKER'''       
                END      
                if (@TeamOPuids = 'retail')      
                BEGIN      
                       Set @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL'''       
                END      
                if (@TeamOPuids = 'correspond')      
                BEGIN      
                       Set @SQL = @SQL + ' and loanapp.ChannelType=''CORRESPOND'''       
                END      
                      
                Set @SQL = @SQL + ' and loanapp.ChannelType like ''%RETAIL%'' '       
                       
                if (@TeamOPuids != '' and @TeamOPuids != 'retail' and @TeamOPuids != 'broker' and @TeamOPuids != 'correspond')      
                BEGIN      
                     Set @SQL = @SQL + '  and (Originator_id in (''' + @TeamOPuids  + ''') or loanapp.id in (''' + @strID + ''')or Broker.office in('''+ @TeamOPuids +'''))'      
                END      
                else      
                  BEGIN      
                    if (@RoleID != 0 and @RoleID != 1 and @RoleID != 11)      
						BEGIN      
                         if (len(@strID) = 0)       
						BEGIN      
						   if (@RoleID = 3)      
							begin
							Set @SQL = @SQL + '  and (loanapp.ID in (''' + @strID + ''') or Originator_id in (''' + @oPuid + ''') )'      
							end
							  if (@RoleID = 10 or  @RoleID = 7)       
								begin
								 Set @SQL = @SQL + '  and (Originator_id in (''' + @strID + ''') or loanapp.id in (''' + @strID + '''))'  
								end
							if (@RoleID = 2) 
								begin      
								Set @SQL = @SQL + '  and (Originator_id in (''' + @strID + '''))'       
								 END   
						end
						else
							begin
								 if (@RoleID = 3)    
									begin   
									Set @SQL = @SQL + '  and (loanapp.ID in (''' + @strID + ''') or Originator_id in (''' + @oPuid + ''') )'      
									end
								  if (@RoleID =  10 or @RoleID = 7) 
									begin     
                                   Set @SQL = @SQL + ' and (Originator_id in (''' + @strID + ''') or loanapp.id in (''' + @strID + '''))' 
									end
								  if (@RoleID = 2) 
									begin      
                                  Set @SQL = @SQL + ' and (Originator_id in (''' + @strID + '''))'      
									end
							end
						end
                                  
                    end
         
              
                        
          
Print @SQL        
exec sp_executesql @SQL        
END   

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCarrierData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCarrierData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsUserRegistrationE3.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetCarrierData]

AS
BEGIN
	 select * from Carrier order by orderid
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCategories]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCategories]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_GetCategories]
AS
BEGIN
	 select CatID, CateName from tblCategories
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCityFromState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCityFromState]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetCityFromState]

@abbr VARCHAR(MAX)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select distinct(City) from tblzipcodes where abbr = (@abbr) order by city
END





GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetConditionDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetConditionDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetConditionDate]
(
@loan_no VARCHAR(MAX),
@condrecid VARCHAR(MAX)
)
AS
BEGIN
select MAX(convert(varchar(35),dttime,101)) as dttime from tblCondText_History  
where rtrim(loan_no)=@loan_no and condrecid =@condrecid
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetConditionHistoryData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetConditionHistoryData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetConditionHistoryData]
(
@loan_no VARCHAR(MAX)
)
AS
BEGIN
select loan_no, cond_text, Filename, condrecid from tblCondText_History where loan_no=@loan_no
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetConditionText]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetConditionText]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetConditionText]
(
@record_id varchar(max)
)
AS
BEGIN
SELECT parent_id,cond_text FROM dbo.condits WHERE record_id=@record_id
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetConditionTextConfirm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetConditionTextConfirm]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetConditionTextConfirm]
(
@record_id VARCHAR(MAX)
)
AS
BEGIN
SELECT parent_id,cond_text FROM dbo.condits 
WHERE record_id in (@record_id)

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetConditionTextData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetConditionTextData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetConditionTextData]
(
@loan_no VARCHAR(MAX),
@condrecid VARCHAR(MAX)
)
AS
BEGIN
select loan_no,cond_text,filename,dttime,ctID,condrecid from tblCondText_History
where rtrim(loan_no)=@loan_no and condrecid = @condrecid
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetContactTime]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetContactTime]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetContactTime]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT  ContactTimeID, ContactTime FROM  tblContactTime
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCreditPulledE3Report]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCreditPulledE3Report]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetCreditPulledE3Report] (@RoleID int,
@strID varchar(max),
@Status varchar(max),
@oPuid varchar(max),
@oYear varchar(100),
@oFromDate varchar(20),
@oToDate varchar(20),
@Branch varchar(max))
AS
BEGIN
  SET NOCOUNT ON;
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  DECLARE @SQL nvarchar(max)
  -- For BRD-125 Reports (adding branch and region column)
  DECLARE @ForRegion bit
  IF (@RoleID = 0
    OR @RoleID = 11
    OR @RoleID = 1
    OR @RoleID = 7
    OR @RoleID = 19
    OR @RoleID = 3
    OR @RoleID = 10
    OR @RoleID = 2)
  BEGIN
    SET @ForRegion = 1
  END
  ELSE
  BEGIN
    SET @ForRegion = 0
  END
  SET @SQL = ''
  SET @SQL = 'select ''E3'' as DB, loanapp.ChannelType as BusType,loanapp.ID as record_id,CONVERT(DECIMAL(10,3),
			(loanData.NoteRate * 100)) as int_rate,LoanNumber as loan_no,borr.firstname as BorrowerFirstName ,
			borr.lastname as BorrowerLastName,loanapp.Originatorname,isnull(isnull(convert(decimal(10,2), loanData.LTV * 100),0),0)LTV,
			LoanApp.DecisionStatus,LoanApp.CurrentStatus as LoanStatus,convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,
			DateAppSigned as DateApproved,LoanData.ShortProgramName as LoanProgram,
           case when loanapp.LoanPurpose1003 is null then '''' when loanapp.LoanPurpose1003 = ''PU'' then ''Purchase'' 
           when loanapp.LoanPurpose1003 = ''RE'' then ''Refinance'' end as Purpose,CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) 
           as CreditScoreUsed,case loanData.UseCashInOut when ''0'' then ''No'' when ''1'' then ''Yes'' end as CashOut,
           isnull(loanData.AdjustedNoteAmt,0) as loan_amt,convert(varchar(35),EstCloseDate,101) AS ScheduleDate,
           CurrPipeStatusDate,UnderwriterName as UnderWriter,CloserName as Closer,CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' 
           THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as  BrokerName, institute.office as ''Branch Office'' '
  IF (@ForRegion = 1) -- For BRD-125 Reports (adding branch and region column)
  BEGIN
    SET @SQL = @SQL + ', region.regionname AS region '
  END
  SET @SQL = @SQL + ' 
			from mwlLoanApp  loanapp  
			INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
			INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
			Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
            Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
            left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
            Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BRANCH'' 
            and Broker.objownerName=''BranchInstitution''
            left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' 
            and LockRecord.Status<>''CANCELED''   
            Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' '
  IF (@ForRegion = 1) -- For BRD-125 Reports (adding branch and region column)
  BEGIN
    SET @SQL = @SQL + '  left join tblbranchoffices AS bo ON bo.branch=institute.Office left join tblregion AS region ON bo.region = region.regionvalue  '
  END
  SET @SQL = @SQL + '               
            where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''
            and loanapp.ChannelType like ''%RETAIL%''            
            and CurrentStatus  IN (select * from dbo.SplitString(''' + @Status + ''','','')) '

  IF (@ForRegion = 1)
  BEGIN
    SET @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) '
    IF (@Branch IS NOT NULL AND @Branch <> '')
    BEGIN
      SET @SQL = @SQL + ' and (institute.Office in(select * from dbo.SplitString(''' + @Branch + ''','','')))'
    END
  END
  ELSE
  BEGIN
    IF (@RoleID != 0  AND @RoleID != 1)
    BEGIN
      IF (LEN(@strID) = 0)
      BEGIN
        SET @SQL = @SQL + '  and (Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')))'

        IF (@oPuid != '')
        BEGIN
          SET @SQL = @SQL + '  and (Originator_id in (select * from dbo.SplitString(''' + @oPuid + ''','','')))'
        END

      END
      ELSE
      BEGIN

        SET @SQL = @SQL + '  and (Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')))'

        IF (@oPuid != '')

          SET @SQL = @SQL + '   and (Originator_id in (select * from dbo.SplitString(''' + @oPuid + ''','','')))'

      END
    END
  END
  IF (@oYear != '')
    SET @SQL = @SQL + ' and YEAR(CurrPipeStatusDate) = ''' + @oYear + ''''


  IF (@oFromDate != ''
    AND @oToDate != '')
    SET @SQL = @SQL + ' and CurrPipeStatusDate between ''' + @oFromDate + '''  and ''' + CONVERT(varchar(20), DATEADD(dd, 1, @oToDate), 101) + ''''
  ELSE
  IF (@oFromDate != '')
    SET @SQL = @SQL + ' and CurrPipeStatusDate > ''' + @oFromDate + ''''
  ELSE
  IF (@oToDate != '')
    SET @SQL = @SQL + ' and CurrPipeStatusDate < ''' + @oToDate + ''''


  PRINT @SQL
  EXEC sp_executesql @SQL
END
/*------------*/



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCreditPulledE3ReportWithRegion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCreditPulledE3ReportWithRegion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[sp_GetCreditPulledE3ReportWithRegion]
(                
@strID varchar(max),                 
@Status varchar(max),             
@oPuid varchar(max),
@oYear varchar(max),
@oFromDate VARCHAR(20),
@oToDate VARCHAR(20)
)                
AS                
BEGIN                
                
Declare @SQL nvarchar(max)                
Set @SQL =''                
Set @SQL ='select ''E3'' as DB, loanapp.ChannelType as BusType,loanapp.ID as record_id,CONVERT(DECIMAL(10,3),(loanData.NoteRate * 100)) as int_rate,LoanNumber as loan_no,borr.firstname as BorrowerFirstName ,borr.lastname as BorrowerLastName,loanapp.Originatorname,isnull(isnull(convert(decimal(10,2), loanData.LTV * 100),0),0)LTV,LoanApp.DecisionStatus,LoanApp.CurrentStatus as LoanStatus,convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,LoanData.ShortProgramName as LoanProgram,
           case when loanapp.LoanPurpose1003 is null then '''' when loanapp.LoanPurpose1003 = ''PU'' then ''Purchase'' 
           when loanapp.LoanPurpose1003 = ''RE'' then ''Refinance'' end as Purpose,
           CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed,case loanData.UseCashInOut when ''0'' then ''No'' when ''1'' then ''Yes'' end as CashOut,isnull(loanData.AdjustedNoteAmt,0) as loan_amt,convert(varchar(35),EstCloseDate,101) AS ScheduleDate,
           CurrPipeStatusDate,UnderwriterName as UnderWriter,CloserName as Closer,CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as  BrokerName,
            Broker.Office as ''Branch Office''
			from mwlLoanApp  loanapp  
			Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
            Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
            left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
            Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BRANCH'' and Broker.objownerName=''BranchInstitution''
            left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' and LockRecord.Status<>''CANCELED''   
            Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' 
            where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''
            and loanapp.ChannelType like ''%RETAIL%''
            
            and CurrentStatus  IN (select * from dbo.SplitString('''+ @Status +''','','')) ' 
            
            if (len(@oPuid) = 0)      
            Begin
               Set @SQL = @SQL + '   and (Originator_id in (select * from dbo.SplitString('''+  @oPuid  +''','',''))  
									      or loanapp.id in (select * from dbo.SplitString('''+  @strID  +''','',''))
									      or Broker.office in  (select * from dbo.SplitString('''+  @oPuid  +''','','')))'      
			End
			else 
			begin
			   Set @SQL = @SQL + '   and (Originator_id in (select * from dbo.SplitString('''+  @oPuid  +''','',''))  
									      or loanapp.id in (select * from dbo.SplitString('''+  @strID  +''','',''))
									      or Broker.office in  (select * from dbo.SplitString('''+  @oPuid  +''','','')))'      	
			End
			
			if(@oYear != '')
				Set @SQL=@SQL+ ' and YEAR(CurrPipeStatusDate) = ''' + @oYear +''''
				
			if(@oFromDate != '' and @oToDate !='')
				Set @SQL=@SQL+ ' and CurrPipeStatusDate between '''+@oFromDate+'''  and '''+Convert(varchar(20),DATEADD (dd , 1 , @oToDate),101)+''''
			else if(@oFromDate != '')
				Set @SQL=@SQL+ ' and CurrPipeStatusDate > '''+@oFromDate+''''	
			else if(@oToDate != '')
				Set @SQL=@SQL+ ' and CurrPipeStatusDate < '''+@oToDate+''''    
					
Print @SQL      
exec sp_executesql @SQL                
END
/*===========================Daily Activity ============================= */

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCreditScore]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCreditScore]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetCreditScore]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT ID, CreditScoreID, CreditScoreValue FROM tblCreditScore order by CreditOrder
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getCustomerLoanInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getCustomerLoanInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author: Abhishek Rastogi  
-- ALTER Date : 10/06/2012  
-- Edited By : --  
-- Edited Date : --  
-- Description: for clsPipeLine.cs BLL  
-- =============================================  
  
CREATE PROCEDURE [dbo].[sp_getCustomerLoanInfo]  
(  
@loanid VARCHAR(MAX)  
  
)  
AS  
BEGIN  
SELECT top 1 BEA.EmailAddress, GrossInterest, PIPmt,EscrowPmt,lo.lastpmtRecvdDate, lo.PrincipalBal,lo.DueDate, isnull(lo.PIPmt, 0) + isnull(lo.EscrowPmt,0) as TotalPmt,lo.EscrowBal, YTD.ClosingInterest,LastFileMainDate,    
YTD.Principal, YTD.TaxesDisb, YTD.InsuranceDisb, ST.Description AS LoanType, lo.OriginalTerm,lo.InterestRate, lo.NextPmtNum, lo.LoanCloseDate,   
lo.OriginalAmt, lo.MaturityDate, pro.AddressLine1, pro.AddressLine2, pro.City,pro.State,pro.Zip,co.CountryName,
pb.FirstMiddleName + ' ' + pb.LastName as  FirstMiddleName,pb.FirstMiddleName as FNAME,pb.LastName as LNAME, pb.HomePhoneNumber,
pb.WorkPhoneNumber,
AppraisalValue,OriginalLTV,MEA.AddressLine1 as MAddressLine1 , MEA.AddressLine2 as MAddressLine2, MEA.City as MCity,MEA.State as MState,MEA.Zip as MZip   
FROM dbo.Loan lo   
JOIN dbo.YearToDate ytd ON lo.LoanID = ytd.LoanID    
JOIN dbo.Property pro ON ytd.LoanID = pro.LoanID    
JOIN dbo.SystemTypes ST ON LO.LoanType = ST.Code    
JOIN dbo.Country co ON co.CountryCode = pro.Country   
JOIN dbo.Borrower pb ON pb.LoanID = lo.LoanID left outer join BorrowerEmailAddress BEA on lo.loanID=bea.loanid 
left outer join MailingAddress MEA on lo.loanID=MEA.loanid    
WHERE ST.FieldName ='LoanType' and pb.BorrowerID = 1 AND lo.LoanID = @loanid  
         
select top 1 Escrowamt as EscrowPmt,transactionamt as TotalPmt,(transactionamt - Escrowamt) as PIPmt,* from history where loanid = @loanid  and transactioncode in( '210' , '211' ) order by transactionDate Desc
             
       
select top 1 PIPayment,EscrowPmt,(PIPayment + EscrowPmt) as TotalPmt,* from paymentchange where loanid = @loanid order by duedate Desc  
  
  
END  
  
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDailyActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDailyActivity]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--exec [dbo].[sp_GetDailyActivity] '19', '68913FEFFB654D3CA49C0701EC770902,0060375F1CBF4DA198D0365802AB76DE,09CEBD1DB5D048108BDFECC2D12854FD,CD66C36312874E2287EAC61BA2A1A82D,AA3782AC22F64C77B8F248BD9B781887,40C307EFE89744E5B5D3F40F0F2AAF38,FC5DE0C323604F228A669D91C1456E6A,A05DFED28EE041968D25E80E2D391CE9','BF7C6F5AFB7347A68F052A5C8987A7EA','','',1,'Retail Akron'

CREATE PROCEDURE [dbo].[sp_GetDailyActivity] (@RoleID int,
@strID varchar(max),
@oPuid varchar(max),
@FromDate varchar(20),
@ToDate varchar(20),
@isFliter bit = 0,
@Branch VARCHAR(MAX))
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
DECLARE @SQL nvarchar(max)
SET @SQL = ''
  
SET @SQL ='Select distinct ''E3'' as DB,loanapp.ChannelType as BusType,loanapp.ID as record_id,LoanNumber as loan_no,
			borr.lastname as BorrowerName,borr.firstname as BorrowerFirstName,borr.lastname as BorrowerLastName,loanapp.DecisionStatus,
			loanapp.CurrentStatus as LoanStatus,loanapp.CurrentStatus as LoanStatusDA ,case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' 
			THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' WHEN ''M'' THEN  ''USDA/Rural Housing Service'' End as LoanProgram ,  
			case when loanapp.LoanPurpose1003 is null then '''' when loanapp.LoanPurpose1003 = ''PU'' then ''Purchase'' 
			when loanapp.LoanPurpose1003 = ''RE'' 
			then ''Refinance'' end as Purpose,CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed, 
			LoanData.AdjustedNoteAmt as LoanAmount,CONVERT(DECIMAL(10,2),(ISNULL(LoanData.LTV,0) * 100)) as LTV,
			CONVERT(DECIMAL(10,3),(LoanData.NoteRate * 100)) as int_rate,  
			LoanData.ShortProgramName as prog_code, Broker.Company as brok_name ,  
			loanapp.Originatorname , loanapp.Branch Branch1, loanapp.MWBranchCode,  institute.office Branch,loanapp.transType'
---Start --added by Tavant Team brd-125
IF(@isFliter =1)
	BEGIN
		SET @SQL = @SQL + ',region1.regionname AS Region '
	END
SET @SQL = @SQL +' FROM mwlLoanApp  loanapp
				   INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
			       INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
				   Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  and sequencenum=1  
				   Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id    
				   Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = ''BRANCH''   
				   and Broker.objownerName=''BranchInstitution'''
						  
IF(@isFliter =1)
	BEGIN
		SET @SQL = @SQL + ' LEFT JOIN tblbranchoffices AS bo ON  bo.branch=institute.office 	
							LEFT JOIN tblregion AS region1   ON bo.region = region1.regionvalue '

	END
SET @SQL = @SQL + '  Where (loanapp.transtype=''R'' Or loanapp.transtype=''P'')'

IF (@isFliter = 1)
BEGIN
     SET @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) '
	 if(@Branch is not null and @Branch <>'')
	 begin
		SET @SQL = @SQL + 'and (institute.Office in(select * from dbo.SplitString('''+  @Branch  +''','','')))' 
     end
END
------------------END BRD-125
ELSE
BEGIN
   IF (@RoleID != 0 AND @RoleID != 1 AND @RoleID != 11)
   BEGIN
      IF (LEN(@strID) = 0)
      BEGIN
         IF (@RoleID = 2)
         BEGIN
           SET @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) '
         END
         ELSE
         IF (@RoleID = 3)
         BEGIN
           SET @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString(''' + @strID + ''','','')) or Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')))'
         END
         ELSE
         IF (@RoleID = 10   OR @RoleID = 7)
         BEGIN
            SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @oPuid + ''','','')) or loanapp.id in (select * from dbo.SplitString(''' + @strID + ''','','')))'
         END
      END
      ELSE
      BEGIN
         IF (@RoleID = 2)
         BEGIN
                
            SET @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) '
         END
         ELSE
         IF (@RoleID = 3)
         BEGIN
              
            SET @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString(''' + @strID + ''','','')) or Originator_id in (select * from dbo.SplitString(''' + @oPuid + ''','','')))'
         END
         ELSE
         IF (@RoleID = 10
            OR @RoleID = 7)
         BEGIN
            SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @oPuid + ''','','')) or loanapp.id in (select * from dbo.SplitString(''' + @strID + ''','','')))'
            
         END
      END
   END
END

IF (@FromDate != '' AND @ToDate != '')
	BEGIN
		SET @SQL = @SQL + ' and loanapp.CreatedonDate between ''' + @FromDate + '''  and  ''' + CONVERT(varchar(20), DATEADD(dd, 1, @ToDate), 101) + ''''
	END
ELSE
  BEGIN
	 IF (@FromDate != '')
		SET @SQL = @SQL + ' and loanapp.CreatedonDate > ''' + @FromDate + ''''
	ELSE
	 IF (@ToDate != '')
		SET @SQL = @SQL + ' and loanapp.CreatedonDate < ''' + @ToDate + ''''
	END
---- Start --Added By Tavant BRD-125----------------------
IF(@isFliter =1)
	BEGIN
		SET @SQL = @SQL + ' and (loanapp.CurrentStatus between ''03 - Appt Set to Review Discls''  and ''60 - Closed'' ) '
	END
-------------END -BRD-125----------------------------------------------
	ELSE
	BEGIN
		SET @SQL = @SQL + ' and (loanapp.CurrentStatus=''13 - File Intake'')'
	END
SET @SQL = @SQL + ' And  (loanData.FinancingType=''C'' or loanData.FinancingType=''F'' or loanData.FinancingType=''V'' or loanData.FinancingType=''M'') 
					  and loanapp.ChannelType like ''%RETAIL%''  and LoanNumber is not null and LoanNumber <> '''''

   PRINT @SQL
   EXEC sp_executesql @SQL
	


/*===========================Daily Lock Report & Fallout Report============================= */
END


--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDailyActivity_With_Region]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDailyActivity_With_Region]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[sp_GetDailyActivity_With_Region]  
(  
@strID varchar(max)  ,
@Puid varchar(max),
@FromDate  varchar(20),  
@ToDate varchar(20)
 
)  
As  
              
Declare @SQL nvarchar(max)   
Set @SQL =''    
  
Set @SQL ='Select ''E3'' as DB,loanapp.ChannelType as BusType, loanapp.Originator_id,loanapp.ID as record_id,LoanNumber as loan_no,
		borr.lastname as BorrowerName,borr.firstname as BorrowerFirstName,borr.lastname as BorrowerLastName,loanapp.DecisionStatus,
		loanapp.CurrentStatus as LoanStatus,  case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' 
		WHEN ''V'' THEN  ''VA'' WHEN ''M'' THEN  ''USDA/Rural Housing Service'' End as LoanProgram ,case when loanapp.LoanPurpose1003 
		is null then '''' when loanapp.LoanPurpose1003 = ''PU'' then ''Purchase'' when loanapp.LoanPurpose1003 = ''RE'' then 
		''Refinance'' end as Purpose,  CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed, 
		LoanData.AdjustedNoteAmt as LoanAmount,  CONVERT(DECIMAL(10,2),(ISNULL(LoanData.LTV,0) * 100)) as LTV,CONVERT(DECIMAL(10,3),
		(LoanData.NoteRate * 100)) as int_rate,  LoanData.ShortProgramName as prog_code, Broker.Company as brok_name ,  
	  loanapp.Originatorname ,  loanapp.Branch Branch1,  
    loanapp.MWBranchCode,  Broker.Office Branch  FROM mwlLoanApp  loanapp  
    
	Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id    
	Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id    
	Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = ''BRANCH''   
	and Broker.objownerName=''BranchInstitution''  
	--Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc=''62 - Funded''  and AppStatus.sequencenum=1     
	--Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id    and AppStatus.sequencenum=1     
  
Where  
 (loanapp.transtype=''R'' Or loanapp.transtype=''P'')'  
  
 
Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+  @Puid  +''','','')) or loanapp.id in (select * from dbo.SplitString('''+  @strID  +''','','')) or Broker.office in (select * from dbo.SplitString('''+  @Puid  +''','','')) )'  


--Set @SQL = @SQL + ' And convert(varchar,AppStatus.StatusDateTime,103)=convert(varchar,getdate(),103) '  

--  Set @SQL = @SQL + ' and  AppStatus.StatusDateTime between convert(datetime,''' + @FromDate + ''',101) and convert(datetime,''' + @ToDate + ''',101)'  

				if(@FromDate != '' and @ToDate !='')
					Set @SQL=@SQL+ ' and loanapp.CreatedonDate between '''+@FromDate+'''  and 
					 '''+Convert(varchar(20),DATEADD (dd , 1 , @ToDate),101)+''''
				else if(@FromDate != '')
					Set @SQL=@SQL+ ' and loanapp.CreatedonDate > '''+@FromDate+''''	
			  	else if(@ToDate != '')
					Set @SQL=@SQL+ ' and loanapp.CreatedonDate < '''+@ToDate+''''   


Set @SQL = @SQL + ' and (loanapp.CurrentStatus=''13 - File Intake'' )  
     And  (loanData.FinancingType=''C'' or loanData.FinancingType=''F'' or loanData.FinancingType=''V'' or loanData.FinancingType=''M'')  
     and loanapp.ChannelType like ''%RETAIL%''  
     and LoanNumber is not null and LoanNumber <> '''''  
  
Print @SQL              
exec sp_executesql @SQL

/*------------*/


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetDailyLockActivityReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetDailyLockActivityReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SP_GetDailyLockActivityReport]           
(            
@RoleID int,           
@strID varchar(max),    
@FromDate  varchar(20),  
@ToDate varchar(20),  
@oPuid varchar(max),
@type varchar(10)='DLA', 
@isFliter bit =0, 
@Branch VARCHAR(MAX)
)            
AS            
BEGIN            
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;           
Declare @SQL nvarchar(max)            
Set @SQL =''            
Set @SQL ='select ''E3'' as DB, loanapp.ID as record_id,loanapp.Channeltype as BusType,LoanNumber as loan_no,borr.firstname as BorrowerFirstName ,   borr.lastname as BorrowerLastName,CurrentStatus as LoanStatus,  
			   convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,  
			   LoanData.LoanProgramName as Prog_desc ,loanapp.Originatorname,  
			   case when loanapp.LoanPurpose1003 is null then '''' when loanapp.LoanPurpose1003 = ''PU'' 
			   then ''Purchase'' when loanapp.LoanPurpose1003 = ''RE'' then ''Refinance'' end as Purpose
			   ,CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed,(Loandata.NoteRate * 100) as Int_rate,  
			   loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,  
			   convert(varchar(40),LockRecord.LockExpirationDate,101) as LockExpires,LockRecord.LockDateTime,  
			   convert(varchar(35),LockRecord.LockDateTime,101) as Lockdate,
			   --UnderwriterName as UnderWriter,CloserName as Closer,'''' as FundedDate,'''' as ReceivedInUW,LoanData.ShortProgramName as prog_code,
			   --loanapp.Branch Branch1,  
			   --CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,
			   institute.office as Branch,
			   case loanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' WHEN ''M'' THEN  
			   ''USDA/Rural	Housing Service'' End as LoanProgram ,
			   loanData.AdjustedNoteAmt as LoanAmount,
			   LockRecord.LockReason,
			   case Isnull(PricingResult.lenderbaserate,'''') When '''' then '''' 
			   Else convert(varchar,((PricingResult.lenderbaserate)*100)) + '' %'' end as interestRate,
			   PricingResult.lenderClosingprice  as  FinalLockPrice '

---Start --Added By Tavant Team for BRD-125-----------------------------
IF(@isFliter =1)
	BEGIN
		SET @SQL = @SQL + ',region1.regionname AS Region'
	END
SET  @SQL = @SQL + ' from mwlLoanApp  loanapp 
                     INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
			         INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
				     Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id    
					 Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id                    
					 left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id    
					 Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BRANCH'' and Broker.objownerName=''BranchInstitution''  
					 left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED''   
					 Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''  
					 Left join mwlPricingResult  as PricingResult on  PricingResult.ObjOwner_id = LockRecord.id '  
IF(@isFliter =1)
	BEGIN
		SET @SQL = @SQL + '  LEFT JOIN tblbranchoffices AS bo ON  bo.branch= institute.office 	
							 LEFT JOIN tblregion AS region1   ON bo.region = region1.regionvalue '
	END
SET  @SQL = @SQL + ' where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' and loanapp.ChannelType like ''%RETAIL%'''  

IF (@isFliter = 1)
BEGIN
     SET @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) '
	 if(@Branch is not null and @Branch <>'')
	 begin
		SET @SQL = @SQL + ' and (institute.Office in(select * from dbo.SplitString('''+  @Branch  +''','','')))' 
	 end
END
-------End BRD -125---------------------------------------------------------------------------------------------
 ELSE
  BEGIN
		if (@RoleID != 0 and @RoleID != 1 and @RoleID != 11)  
		BEGIN  
			if (len(@strID) = 0)  
				BEGIN  
				if (@RoleID = 2)  
				BEGIN  
					--Set @SQL = @SQL + 'and (Originator_id in ('''+ @strID +'''))'  
					Set @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString('''+  @strID  +''','','')) '      
				END  
				else if (@RoleID = 3)  
				BEGIN  
					-- Set @SQL = @SQL + ' and (loanapp.ID in ('''+ @strID +''') or Originator_id in ('''+ @strID +''' ) )'  
					Set @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString('''+  @strID  +''','','')) 
					or Originator_id in (select * from dbo.SplitString('''+  @strID  +''','','')))' 
				END  
				else if (@RoleID =  10 or @RoleID  = 7)  
				BEGIN  
					-- Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID +''') or loanapp.id in ('''+ @strID +'''))'  
					Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+  @oPuid  +''','','')) 
					or loanapp.id in (select * from dbo.SplitString('''+  @strID  +''','','')))' 
				END  
			END  
			else  
			BEGIN  
			if (@RoleID  = 2)  
				BEGIN  
					--Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID +'''))'  
					Set @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString('''+  @strID  +''','','')) '   
				END  
				else if (@RoleID = 3)  
				BEGIN  
					--Set @SQL = @SQL + ' and (loanapp.ID in ('''+ @strID +''') or Originator_id in (''' + @oPuid +''') )'  
					Set @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString('''+  @strID  +''','','')) 
					or Originator_id in (select * from dbo.SplitString('''+  @oPuid  +''','','')))'
				END  
				else if (@RoleID = 10 or  @RoleID = 7)  
				BEGIN  
					-- Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID +''') or loanapp.id in ('''+ @strID +'''))'  
					Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+  @oPuid  +''','','')) 
					or loanapp.id in (select * from dbo.SplitString('''+  @oPuid  +''','','')))'
				END  
			END  
		END  
   END            
IF(@FromDate != '' and @ToDate !='')
	BEGIN
		Set @SQL=@SQL+ ' and LockRecord.LockDateTime between '''+@FromDate+''' and  '''+Convert(varchar(20),DATEADD (dd , 1 , @ToDate),101)+''''
	END
	else if(@FromDate != '')
		Set @SQL=@SQL+ ' and LockRecord.LockDateTime > '''+@FromDate+''''	
	else if(@ToDate != '')
		Set @SQL=@SQL+ ' and LockRecord.LockDateTime < '''+@ToDate+''''  
IF(@type='RFA')
	BEGIN
        Set @SQL = @SQL + ' and loanapp.CurrentStatus <>''62 - Funded'''
    END
				
Set @SQL = @SQL + 'and LockRecord.LockType is not null '                 

-- Print @SQL            
exec sp_executesql @SQL            
END 
/*===========================Final Price Report============================= */


--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetDailyLockActivityReport_With_Region]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetDailyLockActivityReport_With_Region]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SP_GetDailyLockActivityReport_With_Region]           
(       
@strID varchar(max),    
@oPuid varchar(max),
@FromDate  varchar(20),  
@ToDate varchar(20),  
@type varchar(10)='DLA'  
)            
AS            
BEGIN            
            
Declare @SQL nvarchar(max)            
Set @SQL =''            
Set @SQL ='select ''E3'' as DB, loanapp.ID as record_id,loanapp.Channeltype as BusType,LoanNumber as loan_no,borr.firstname as BorrowerFirstName ,  
   borr.lastname as BorrowerLastName,CurrentStatus as LoanStatus,  
   convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,  
   LoanData.LoanProgramName as Prog_desc ,loanapp.Originatorname,  
   case when loanapp.LoanPurpose1003 is null then '''' when loanapp.LoanPurpose1003 = ''PU'' then ''Purchase'' 
   when loanapp.LoanPurpose1003 = ''RE'' then ''Refinance''   
   end as Purpose,CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed,(Loandata.NoteRate * 100) as Int_rate,  
   loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,  
   convert(varchar(40),LockRecord.LockExpirationDate,101) as LockExpires,LockRecord.LockDateTime,  
   convert(varchar(35),LockRecord.LockDateTime,101) as Lockdate,UnderwriterName as UnderWriter,CloserName as Closer,  
   CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,'''' as FundedDate,  
   '''' as ReceivedInUW,LoanData.ShortProgramName as prog_code   ,
    loanapp.Branch Branch1,
    Broker.Office Branch,
   case loanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' WHEN ''M'' THEN  ''USDA/Rural Housing Service'' End as LoanProgram ,
   loanData.AdjustedNoteAmt as LoanAmount,
   LockRecord.LockREason,
   case Isnull(PricingResult.lenderbaserate,'''') When '''' then '''' Else convert(varchar,((PricingResult.lenderbaserate)*100)) + '' %'' end interestRate,
   PricingResult.lenderClosingprice  FinalLockPrice
   from mwlLoanApp  loanapp     
   Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id    
   Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id                    
   left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id    
   Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and 
   Broker.InstitutionType = ''BRANCH'' and Broker.objownerName=''BranchInstitution''  
   left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED''   
   Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''  
   Left join mwlPricingResult  as PricingResult on  PricingResult.ObjOwner_id = LockRecord.id  
   where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''   
   and loanapp.ChannelType like ''%RETAIL%'''  
  
                
                     
            Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+ @oPuid   +''','','')) 
			or loanapp.id in (select * from dbo.SplitString('''+  @strID  +''','','')) or 
			Broker.office in  (select * from dbo.SplitString('''+  @oPuid  +''','','')) )'
              



           --Set @SQL = @SQL + ' and  LockRecord.LockDateTime between convert(datetime,''' + @FromDate + ''',101) and convert(datetime,''' + @ToDate + ''',101)'  
				if(@FromDate != '' and @ToDate !='')
					Set @SQL=@SQL+ ' and LockRecord.LockDateTime between '''+@FromDate+'''  and  
					'''+Convert(varchar(20),DATEADD (dd , 1 , @ToDate),101)+''''
				else if(@FromDate != '')
					Set @SQL=@SQL+ ' and LockRecord.LockDateTime > '''+@FromDate+''''	
			  	else if(@ToDate != '')
					Set @SQL=@SQL+ ' and LockRecord.LockDateTime < '''+@ToDate+''''  
       
              IF(@type='RFA')
               BEGIN
               Set @SQL = @SQL + ' and loanapp.CurrentStatus <>''62 - Funded'''
               END
			Set @SQL = @SQL + ' and LockRecord.LockType is not null '                  

 Print @SQL            
exec sp_executesql @SQL            
END 
/*------------*/

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDataList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDataList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDataList]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

SELECT 
tblloanapp.LoanId,tblloanapp.Fname,tblloanapp.Mname,tblloanapp.LName,tblloanapp.Email,tblloanapp.CEmail, tblloanapp.DOBM,tblloanapp.DOBDT, tblloanapp.DOBYR, tblloanapp.SSN, tblloanapp.Phone, tblloanapp.Suffix, tblloanapp.COFNAME, tblloanapp.COMNAME,tblloanapp.COLNAME, tblloanapp.COEMAIL, tblloanapp.COCEmail, tblloanapp.CODOBM, tblloanapp.CODOBDT, tblloanapp.CODOBYR,tblloanapp.COSSN, tblloanapp.COPHONE, tblloanapp.STADDR, tblloanapp.CITY, tblloanapp.MState, tblloanapp.ZipCode, tblloanapp.RSTATUS,tblloanapp.RentAmt, tblloanapp.LMHOLDER, tblloanapp.STADDR2, tblloanapp.CITY2, tblloanapp.STATE2, tblloanapp.COSuffix, tblloanapp.ZipCode2,tblloanapp.RLengthY2, tblloanapp.RLengthM2, tblloanapp.RSTATUS2, tblloanapp.RentAmt2, tblloanapp.LMHOLDER2, tblloanapp.AEMPL,tblloanapp.AOCCU, tblloanapp.AEMPPHONE, tblloanapp.AGMI, tblloanapp.ALEMY, tblloanapp.ALEMM, tblloanapp.AEMPL2, tblloanapp.AOCCU2,tblloanapp.AEMPPHONE2, tblloanapp.AGMI2, tblloanapp.ALEMY2, tblloanapp.ALEMM2, tblloanapp.CAEMPL, tblloanapp.CAOCCU,tblloanapp.CAEMPPHONE, tblloanapp.CAGMI, tblloanapp.CALEMY, tblloanapp.CALEMM, tblloanapp.CAEMPL2, tblloanapp.CAOCCU2,tblloanapp.CAEMPPHONE2, tblloanapp.CAGMI2, tblloanapp.CALEMY2, tblloanapp.CALEMM2, tblloanapp.CurrLoanMake, tblloanapp.CurrLoanYear, tblloanapp.CurrLoanBalance, tblloanapp.CurrLoanMiles, tblloanapp.OtherComments, tblloanapp.DigiSig, tblloanapp.SigDate, tblloanapp.IsAckno,tblloanapp.DateEntered, tblloanapp.RLengthM, tblloanapp.RlengthY, tblloanapp.Status, tblloanapp.Network, tblloanapp.PayDate, tblloanapp.Amount, tblloanapp.Paid, tblloanapp.lastupdateby, tblloanapp.NetworkName, tblloanapp.NetworkPhone, tblstate.StateName FROM tblloanapp 
INNER JOIN tblstate ON tblloanapp.MState = tblstate.SId order by tblloanapp.loanid desc  
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDataList_loanapp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDataList_loanapp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDataList_loanapp]
	-- Add the parameters for the stored procedure here
	@loanid varchar(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM tblloanapp where loanid=@loanid

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDataListLoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDataListLoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDataListLoanApplication]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT     LoanApplicationID, LoanReasonID, HometypeID, Producttypeid, StateID, CreditScoreID, AnnulIncomeBorrower,LoanAmount, AssetBorrower, 
                RetirementAssetsBorrower, PropertyLocated, HomeOwned, PurchasePrise, DownPaymentAmt, PartOfTotalAssets, DownPaymentSourceID,  
                HaveRealtor, RealtorContactName, RealtorPhone, RealtorEmail, AppFName, AppLName, AppFName+','+ AppLName as AppFullNAme, AppStateID, AppZip, AppPrimaryPhone, AppSecondaryPhone,  
                AppEmail, CurrentPropertyValue, PropertyPurchaseMonth, YearID, ExistingPuchasePrice, CurrentMortageBal, MonthlyPayment, SecondMortage,  
                SecondMortageBal, DateEntered, ModifiedBy, ModifiedDate, UserAppearUrl, PropertyTypeID, PropertyTypeName, ContactTime, ContactTimeID,  
                Hometype, CreditScoreValue, StateName, LoanReason, ProductTypeName, AppStateName,Year,MAName,IsWorkingWithRealtor, PropertyRealtorCity,IsCashback,RealtorName,RealtorPhoneNo 
                FROM         (SELECT     tblLoanApplication.LoanApplicationID, tblLoanApplication.LoanReasonID, tblLoanApplication.HometypeID, tblLoanApplication.Producttypeid,
                tblLoanApplication.StateID, tblLoanApplication.CreditScoreID, tblLoanApplication.AnnulIncomeBorrower,LoanAmount, tblLoanApplication.AssetBorrower,
                tblLoanApplication.RetirementAssetsBorrower, tblLoanApplication.PropertyLocated, tblLoanApplication.HomeOwned, tblLoanApplication.PurchasePrise,  
                tblLoanApplication.DownPaymentAmt, tblLoanApplication.PartOfTotalAssets, tblLoanApplication.DownPaymentSourceID,  
                tblLoanApplication.HaveRealtor, tblLoanApplication.RealtorContactName, tblLoanApplication.RealtorPhone, tblLoanApplication.RealtorEmail,  
                tblLoanApplication.AppFName, tblLoanApplication.AppLName, tblLoanApplication.AppStateID, tblLoanApplication.AppZip,  
                tblLoanApplication.AppPrimaryPhone, tblLoanApplication.AppSecondaryPhone, tblLoanApplication.AppEmail, tblLoanApplication.CurrentPropertyValue,  
                tblLoanApplication.PropertyPurchaseMonth, tblLoanApplication.YearID, tblLoanApplication.ExistingPuchasePrice,  
                tblLoanApplication.CurrentMortageBal, tblLoanApplication.MonthlyPayment, tblLoanApplication.SecondMortage, tblLoanApplication.SecondMortageBal,  
                tblLoanApplication.DateEntered, tblLoanApplication.ModifiedBy, tblLoanApplication.ModifiedDate, tblLoanApplication.UserAppearUrl,  
                tblLoanApplication.PropertyTypeID, tblPropertyType.Name AS PropertyTypeName, tblContactTime.ContactTime, tblLoanApplication.ContactTimeID,  
                tblHomeType.Hometype, tblCreditScore.CreditScoreValue, tblStateNylex.StateName, tblLoanReason.LoanReason,  
                tblProductType.name AS ProductTypeName, tblStateNylex_1.StateName AS AppStateName, tblYear.Year,tblLoanApplication.MAName,tblLoanApplication.IsWorkingWithRealtor, tblLoanApplication.PropertyRealtorCity,tblLoanApplication.IsCashback,tblLoanApplication.RealtorName,tblLoanApplication.RealtorPhoneNo 
                FROM         tblLoanApplication INNER JOIN
                tblPropertyType ON tblLoanApplication.PropertyTypeID = tblPropertyType.PropertyTypeID INNER JOIN 
                tblContactTime ON tblLoanApplication.ContactTimeID = tblContactTime.ContactTimeID INNER JOIN 
                tblHomeType ON tblLoanApplication.HometypeID = tblHomeType.HometypeID INNER JOIN
                tblCreditScore ON tblLoanApplication.CreditScoreID = tblCreditScore.CreditScoreID INNER JOIN 
                tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID INNER JOIN 
                tblLoanReason ON tblLoanApplication.LoanReasonID = tblLoanReason.LoanReasonID INNER JOIN 
                tblProductType ON tblLoanApplication.Producttypeid = tblProductType.producttypeid INNER JOIN 
                tblStateNylex AS tblStateNylex_1 ON tblLoanApplication.AppStateID = tblStateNylex_1.StateID INNER JOIN 
                tblYear ON tblLoanApplication.YearID = tblYear.YearID) AS App order by LoanApplicationID desc 

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDataTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDataTable]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDataTable]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

SELECT     tblloanapp.LoanId, tblloanapp.Fname, tblloanapp.Mname, tblloanapp.LName, tblloanapp.Email, tblloanapp.CEmail, tblloanapp.DOBM,  tblloanapp.DOBDT, tblloanapp.DOBYR, tblloanapp.SSN, tblloanapp.Phone, tblloanapp.Suffix, tblloanapp.COFNAME, tblloanapp.COMNAME,tblloanapp.COLNAME, tblloanapp.COEMAIL, tblloanapp.COCEmail, tblloanapp.CODOBM, tblloanapp.CODOBDT, tblloanapp.CODOBYR,    tblloanapp.COSSN, tblloanapp.COPHONE, tblloanapp.STADDR, tblloanapp.CITY, tblloanapp.MState, tblloanapp.ZipCode, tblloanapp.RSTATUS,  tblloanapp.RentAmt, tblloanapp.LMHOLDER, tblloanapp.STADDR2, tblloanapp.CITY2, tblloanapp.STATE2, tblloanapp.COSuffix, tblloanapp.ZipCode2,  tblloanapp.RLengthY2, tblloanapp.RLengthM2, tblloanapp.RSTATUS2, tblloanapp.RentAmt2, tblloanapp.LMHOLDER2, tblloanapp.AEMPL,  tblloanapp.AOCCU, tblloanapp.AEMPPHONE, tblloanapp.AGMI, tblloanapp.ALEMY, tblloanapp.ALEMM, tblloanapp.AEMPL2, tblloanapp.AOCCU2,tblloanapp.AEMPPHONE2, tblloanapp.AGMI2, tblloanapp.ALEMY2, tblloanapp.ALEMM2, tblloanapp.CAEMPL, tblloanapp.CAOCCU,tblloanapp.CAEMPPHONE, tblloanapp.CAGMI, tblloanapp.CALEMY, tblloanapp.CALEMM, tblloanapp.CAEMPL2, tblloanapp.CAOCCU2,tblloanapp.CAEMPPHONE2, tblloanapp.CAGMI2, tblloanapp.CALEMY2, tblloanapp.CALEMM2, tblloanapp.CurrLoanMake, tblloanapp.CurrLoanYear,tblloanapp.CurrLoanBalance, tblloanapp.CurrLoanMiles, tblloanapp.OtherComments, tblloanapp.DigiSig, tblloanapp.SigDate, tblloanapp.IsAckno,tblloanapp.DateEntered, tblloanapp.RLengthM, tblloanapp.RlengthY, tblloanapp.Status, tblloanapp.Network, tblloanapp.PayDate, tblloanapp.Amount, tblloanapp.Paid, tblloanapp.lastupdateby, tblloanapp.NetworkName, tblloanapp.NetworkPhone, tblstate.StateName AS StateName1 ,tblstate_1.StateName as StateName2  FROM tblloanapp INNER JOIN tblstate ON tblloanapp.MState = tblstate.SId INNER JOIN tblstate AS tblstate_1 ON tblloanapp.STATE2 = tblstate_1.SId  ORDER BY tblloanapp.LoanId DESC
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDataTable1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDataTable1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDataTable1]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	
                            SELECT     LoanApplicationID, LoanReasonID, HometypeID, Producttypeid, StateID, CreditScoreID, AnnulIncomeBorrower, AssetBorrower,   
                RetirementAssetsBorrower, PropertyLocated, HomeOwned, PurchasePrise, DownPaymentAmt, PartOfTotalAssets, DownPaymentSourceID,   
                HaveRealtor, RealtorContactName, RealtorPhone, RealtorEmail, AppFName, AppLName, AppStateID, AppZip, AppPrimaryPhone, AppSecondaryPhone,
                AppEmail, CurrentPropertyValue, PropertyPurchaseMonth, YearID, ExistingPuchasePrice, CurrentMortageBal, MonthlyPayment, SecondMortage,
                SecondMortageBal, DateEntered, ModifiedBy, ModifiedDate, UserAppearUrl, PropertyTypeID, PropertyTypeName, ContactTime, ContactTimeID,  
                Hometype, CreditScoreValue, StateName, LoanReason, ProductTypeName, AppStateName, Year, SourceOfdownPayment 
                FROM         (SELECT     tblLoanApplication.LoanApplicationID, tblLoanApplication.LoanReasonID, tblLoanApplication.HometypeID, tblLoanApplication.Producttypeid,   
                tblLoanApplication.StateID, tblLoanApplication.CreditScoreID, tblLoanApplication.AnnulIncomeBorrower, tblLoanApplication.AssetBorrower,   
                tblLoanApplication.RetirementAssetsBorrower, tblLoanApplication.PropertyLocated, tblLoanApplication.HomeOwned,   
                tblLoanApplication.PurchasePrise, tblLoanApplication.DownPaymentAmt, tblLoanApplication.PartOfTotalAssets,
                tblLoanApplication.DownPaymentSourceID, tblLoanApplication.HaveRealtor, tblLoanApplication.RealtorContactName,
                tblLoanApplication.RealtorPhone, tblLoanApplication.RealtorEmail, tblLoanApplication.AppFName, tblLoanApplication.AppLName,
                tblLoanApplication.AppStateID, tblLoanApplication.AppZip, tblLoanApplication.AppPrimaryPhone, tblLoanApplication.AppSecondaryPhone,
                tblLoanApplication.AppEmail, tblLoanApplication.CurrentPropertyValue, tblLoanApplication.PropertyPurchaseMonth, 
                tblLoanApplication.YearID, tblLoanApplication.ExistingPuchasePrice, tblLoanApplication.CurrentMortageBal,  
                tblLoanApplication.MonthlyPayment, tblLoanApplication.SecondMortage, tblLoanApplication.SecondMortageBal,
                tblLoanApplication.DateEntered, tblLoanApplication.ModifiedBy, tblLoanApplication.ModifiedDate, tblLoanApplication.UserAppearUrl,  
                tblLoanApplication.PropertyTypeID, tblPropertyType.Name AS PropertyTypeName, tblContactTime.ContactTime,  
                tblLoanApplication.ContactTimeID, tblHomeType.Hometype, tblCreditScore.CreditScoreValue, tblStateNylex.StateName,
                tblLoanReason.LoanReason, tblProductType.name AS ProductTypeName, tblStateNylex_1.StateName AS AppStateName, tblYear.Year,
                tblDownPaymentSource.SourceOfdownPayment 
                FROM          tblLoanApplication LEFT JOIN 
                tblPropertyType ON tblLoanApplication.PropertyTypeID = tblPropertyType.PropertyTypeID LEFT JOIN  
                tblContactTime ON tblLoanApplication.ContactTimeID = tblContactTime.ContactTimeID LEFT JOIN  
                tblHomeType ON tblLoanApplication.HometypeID = tblHomeType.HometypeID LEFT JOIN
                tblCreditScore ON tblLoanApplication.CreditScoreID = tblCreditScore.CreditScoreID LEFT JOIN  
                tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID LEFT JOIN  
                tblLoanReason ON tblLoanApplication.LoanReasonID = tblLoanReason.LoanReasonID LEFT JOIN  
                tblProductType ON tblLoanApplication.Producttypeid = tblProductType.producttypeid LEFT JOIN  
                tblStateNylex AS tblStateNylex_1 ON tblLoanApplication.AppStateID = tblStateNylex_1.StateID LEFT JOIN  
                tblYear ON tblLoanApplication.YearID = tblYear.YearID LEFT OUTER JOIN  
                tblDownPaymentSource ON tblLoanApplication.DownPaymentSourceID = tblDownPaymentSource.DownPaymentSourceID) AS App order by LoanApplicationID desc  
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDirectorOfRetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDirectorOfRetail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetDirectorOfRetail]
(
	@ManagerId int
	
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SELECT t1.userid
		,ufirstname + space(1) + ulastname fullname
	FROM tblusers t1
	INNER JOIN tblUserManagers t2 ON t1.userid = t2.userid
		AND t1.isactive = 1
		AND t2.ManagerId = @ManagerId
		WHERE t1.urole = 1
	
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetDisclosureDeskAnalystReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetDisclosureDeskAnalystReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Narayan.NR
-- Create date: 24 Jan 2014            
-- Description: To get Disclosuredesk analyst pipline data.
--(select Id FROM mwsCustFieldDef where CustomFieldName='Retail Disclosure Desk Analyst')
-- =============================================
CREATE PROCEDURE [dbo].[SP_GetDisclosureDeskAnalystReport]              
(  	
	@UserID int,
	@Originators VARCHAR(MAX) = NULL
)                
AS                
BEGIN 

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

Select top 1 urole,e3userid into #tmpdclUser from tblusers  where userid=@UserID

DECLARE @RoleID int
SET @RoleID=(Select urole from #tmpdclUser)
DECLARE @E3userid varchar(200)
SET @E3userid=(Select e3userid from #tmpdclUser)

--BRD-250
DECLARE @tblOriginators TABLE (ID varchar(50))


if(@RoleID = 1 OR @RoleID = 3 OR @RoleID = 10)
BEGIN
	INSERT INTO @tblOriginators
	SELECT items FROM dbo.SplitString (@Originators,',')
END

--select * from @tblOriginators

DECLARE @disclosureduedays smallint
SET  @disclosureduedays= 3


	if(@RoleID=1)
	Begin	
		With CTE_ROM as
		(
		Select distinct 'E3' AS DB
			,loanapp.LoanNumber AS loan_no	
			,loanapp.OriginatorName
			,borr.firstname AS BorrowerFirstName
			,borr.lastname AS BorrowerLastName
			,loanapp.currentStatus AS LoanStatus
			,discappstat.statusdatetime AS InitialDisclosureDate
			,DATEADD(day ,@disclosureduedays, discappstat.statusdatetime)  as DuebyDisclosure 
			,rediscappstat.statusdatetime AS RedisclosureDate
			,DATEADD(day ,@disclosureduedays, rediscappstat.statusdatetime) as DuebyReDisclosure 
			,loanData.AdjustedNoteAmt AS loan_amt	
			,institute.office as Branch
			,uSummary.UnderwriterName as UnderWriter
			,loanapp.processorname as ProcessorName
			,DisclosureAnalyst.StringValue as DisclosureAnalyst
			,loanapp.ID as	record_id
			,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
			,E3user.ID as E3USERID
			,loanapp.originator_id as Originatorid
			,users.branchinstitution_id
			 FROM  mwlloanapp AS loanapp 
			 --OLIEPS-503
			INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
			INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
			INNER JOIN mwlborrower AS borr ON borr.loanapp_id = loanapp.id
			INNER JOIN dbo.mwlloandata AS loandata ON loandata.objowner_id = loanapp.id
			INNER join mwlAppStatus AS appStat on loanapp.ID=appStat.LoanApp_id	
			LEFT JOIN dbo.mwlcustomfield as DisclosureAnalyst ON DisclosureAnalyst.loanapp_id= loanapp.ID AND DisclosureAnalyst.custFieldDef_ID='E16914219F3944DDB08117BDE784BCA4'
			LEFT JOIN dbo.mwaMWUser AS E3user ON E3user.Fullname=DisclosureAnalyst.StringValue
			LEFT JOIN dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
			LEFT join (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus
			where StatusDesc='05 - Application Taken' GROUP BY loanapp_id ,statusdesc) discappstat on loanapp.Id = discappstat.LoanApp_id
			LEFT join  (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus 
			where StatusDesc = '30.1 - Req for Re-disclosure'  GROUP BY loanapp_id ,statusdesc ) rediscappstat ON loanapp.Id = rediscappstat.LoanApp_id
	
			WHERE borr.sequencenum=1 AND loanapp.LoanNumber is not null AND loanapp.LoanNumber <> '' 
			AND loanapp.ChannelType like '%RETAIL%'   
			AND loanapp.CurrentStatus  IN ('05 - Application Taken','06 - Disclosures Out','30.1 - Req for Re-disclosure','30.2 - Re-disclosure Out')  
		)
		select * from CTE_ROM where Originatorid in (select id from @tblOriginators e3user inner join tblusers olieUser on e3user.id=olieuser.e3userid
		where olieuser.urole in(2,3,10) )
	End

	if(@RoleID=11)
	Begin	
		With CTE_ROM as
		(
		Select distinct 'E3' AS DB
			,loanapp.LoanNumber AS loan_no	
			,loanapp.OriginatorName
			,borr.firstname AS BorrowerFirstName
			,borr.lastname AS BorrowerLastName
			,loanapp.currentStatus AS LoanStatus
			,discappstat.statusdatetime AS InitialDisclosureDate
			,DATEADD(day ,@disclosureduedays, discappstat.statusdatetime)  as DuebyDisclosure 
			,rediscappstat.statusdatetime AS RedisclosureDate
			,DATEADD(day ,@disclosureduedays, rediscappstat.statusdatetime) as DuebyReDisclosure 
			,loanData.AdjustedNoteAmt AS loan_amt	
			,institute.office as Branch
			,uSummary.UnderwriterName as UnderWriter
			,loanapp.processorname as ProcessorName
			,DisclosureAnalyst.StringValue as DisclosureAnalyst
			,loanapp.ID as	record_id
			,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
			,E3user.ID as E3USERID
			,loanapp.originator_id as Originatorid
			,users.branchinstitution_id
			 FROM  mwlloanapp AS loanapp 
			 --OLIEPS-503
			INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
			INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
			INNER JOIN mwlborrower AS borr ON borr.loanapp_id = loanapp.id
			INNER JOIN dbo.mwlloandata AS loandata ON loandata.objowner_id = loanapp.id
			INNER join mwlAppStatus AS appStat on loanapp.ID=appStat.LoanApp_id	
			LEFT JOIN dbo.mwlcustomfield as DisclosureAnalyst ON DisclosureAnalyst.loanapp_id= loanapp.ID AND DisclosureAnalyst.custFieldDef_ID='E16914219F3944DDB08117BDE784BCA4'
			LEFT JOIN dbo.mwaMWUser AS E3user ON E3user.Fullname=DisclosureAnalyst.StringValue
			LEFT JOIN dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
			LEFT join (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus
			where StatusDesc='05 - Application Taken' GROUP BY loanapp_id ,statusdesc) discappstat on loanapp.Id = discappstat.LoanApp_id
			LEFT join  (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus 
			where StatusDesc = '30.1 - Req for Re-disclosure'  GROUP BY loanapp_id ,statusdesc ) rediscappstat ON loanapp.Id = rediscappstat.LoanApp_id
	
			WHERE borr.sequencenum=1 AND loanapp.LoanNumber is not null AND loanapp.LoanNumber <> '' 
			AND loanapp.ChannelType like '%RETAIL%'   
			AND loanapp.CurrentStatus  IN ('05 - Application Taken','06 - Disclosures Out','30.1 - Req for Re-disclosure','30.2 - Re-disclosure Out')  
		)
		select * from CTE_ROM
	End


	ELSE if(@RoleID = 21)
	Begin
	With CTE_RDA as
	(
	Select distinct 'E3' AS DB
    ,loanapp.LoanNumber AS loan_no	
	,loanapp.OriginatorName
	,borr.firstname AS BorrowerFirstName
	,borr.lastname AS BorrowerLastName
	,loanapp.currentStatus AS LoanStatus
	,discappstat.statusdatetime AS InitialDisclosureDate
	,DATEADD(day ,@disclosureduedays, discappstat.statusdatetime)  as DuebyDisclosure 
	,rediscappstat.statusdatetime AS RedisclosureDate
	,DATEADD(day ,@disclosureduedays, rediscappstat.statusdatetime) as DuebyReDisclosure 
	,loanData.AdjustedNoteAmt AS loan_amt	
	,institute.Office as Branch
	,uSummary.UnderwriterName as UnderWriter
	,loanapp.processorname as ProcessorName
	,DisclosureAnalyst.StringValue as DisclosureAnalyst
    ,loanapp.ID as	record_id
	,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
	,E3user.ID as E3USERID
	,loanapp.originator_id as Originatorid
	,users.branchinstitution_id
	 FROM  mwlloanapp AS loanapp 
	--OLIEPS-503
	INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
	INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
	INNER JOIN mwlborrower AS borr ON borr.loanapp_id = loanapp.id
	INNER JOIN dbo.mwlloandata AS loandata ON loandata.objowner_id = loanapp.id
	INNER join mwlAppStatus AS appStat on loanapp.ID=appStat.LoanApp_id
	INNER JOIN dbo.mwlcustomfield as DisclosureAnalyst ON DisclosureAnalyst.loanapp_id= loanapp.ID AND	DisclosureAnalyst.custFieldDef_ID='E16914219F3944DDB08117BDE784BCA4'
	INNER JOIN dbo.mwaMWUser AS E3user ON E3user.Fullname=DisclosureAnalyst.StringValue 
	LEFT JOIN dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
	
    LEFT join (select Min(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus
	where StatusDesc='05 - Application Taken' GROUP BY loanapp_id ,statusdesc) discappstat on loanapp.Id = discappstat.LoanApp_id
	LEFT join  (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus 
	where StatusDesc = '30.1 - Req for Re-disclosure'  GROUP BY loanapp_id ,statusdesc ) rediscappstat ON loanapp.Id = rediscappstat.LoanApp_id
	
	WHERE borr.sequencenum=1 AND loanapp.LoanNumber is not null AND loanapp.LoanNumber <> '' 
	AND loanapp.ChannelType like '%RETAIL%'   
	AND loanapp.CurrentStatus  IN ('05 - Application Taken','06 - Disclosures Out','30.1 - Req for Re-disclosure','30.2 - Re-disclosure Out')  
	)
	select * from CTE_RDA where E3USERID=@E3userid
	End

	ELSE if(@RoleID = 2)
	Begin	
	With CTE_MA as
	(
	Select distinct 'E3' AS DB
    ,loanapp.LoanNumber AS loan_no	
	,loanapp.OriginatorName
	,borr.firstname AS BorrowerFirstName
	,borr.lastname AS BorrowerLastName
	,loanapp.currentStatus AS LoanStatus
	,discappstat.statusdatetime AS InitialDisclosureDate
	,DATEADD(day ,@disclosureduedays, discappstat.statusdatetime)  as DuebyDisclosure 
	,rediscappstat.statusdatetime AS RedisclosureDate
	,DATEADD(day ,@disclosureduedays, rediscappstat.statusdatetime) as DuebyReDisclosure 
	,loanData.AdjustedNoteAmt AS loan_amt	
	,institute.office as Branch
	,uSummary.UnderwriterName as UnderWriter
	,loanapp.processorname as ProcessorName
	,DisclosureAnalyst.StringValue as DisclosureAnalyst
    ,loanapp.ID as	record_id
	,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
	,E3user.ID as E3USERID
	,loanapp.originator_id as Originatorid
	,users.branchinstitution_id
	 FROM  mwlloanapp AS loanapp 
	 --OLIEPS 503
	INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
	INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
	INNER JOIN mwlborrower AS borr ON borr.loanapp_id = loanapp.id
	INNER JOIN dbo.mwlloandata AS loandata ON loandata.objowner_id = loanapp.id
	INNER join mwlAppStatus AS appStat on loanapp.ID=appStat.LoanApp_id
	LEFT JOIN dbo.mwlcustomfield as DisclosureAnalyst ON DisclosureAnalyst.loanapp_id= loanapp.ID AND 	DisclosureAnalyst.custFieldDef_ID='E16914219F3944DDB08117BDE784BCA4'
	LEFT JOIN dbo.mwaMWUser AS E3user ON E3user.ID=loanapp.originator_id	
	LEFT JOIN dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
	LEFT join (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus
	where StatusDesc='05 - Application Taken' GROUP BY loanapp_id ,statusdesc) discappstat on loanapp.Id = discappstat.LoanApp_id
	LEFT join  (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus 
	where StatusDesc = '30.1 - Req for Re-disclosure'  GROUP BY loanapp_id ,statusdesc ) rediscappstat ON loanapp.Id = rediscappstat.LoanApp_id
	
	WHERE borr.sequencenum=1 AND loanapp.LoanNumber is not null AND loanapp.LoanNumber <> '' 
	AND loanapp.ChannelType like '%RETAIL%'   
	AND loanapp.CurrentStatus  IN ('05 - Application Taken','06 - Disclosures Out','30.1 - Req for Re-disclosure','30.2 - Re-disclosure Out')  
	AND loanapp.originator_id=@E3userid
	)
	select * from CTE_MA

	End

	ELSE if(@RoleID = 3 or @RoleID = 10)
	Begin

	With CTE_SB as
	(
	Select distinct 'E3' AS DB
    ,loanapp.LoanNumber AS loan_no	
	,loanapp.OriginatorName
	,borr.firstname AS BorrowerFirstName
	,borr.lastname AS BorrowerLastName
	,loanapp.currentStatus AS LoanStatus
	,discappstat.statusdatetime AS InitialDisclosureDate
	,DATEADD(day ,@disclosureduedays, discappstat.statusdatetime)  as DuebyDisclosure 
	,rediscappstat.statusdatetime AS RedisclosureDate
	,DATEADD(day ,@disclosureduedays, rediscappstat.statusdatetime) as DuebyReDisclosure 
	,loanData.AdjustedNoteAmt AS loan_amt	
	,institute.office as Branch
	,uSummary.UnderwriterName as UnderWriter
	,loanapp.processorname as ProcessorName
	,DisclosureAnalyst.StringValue as DisclosureAnalyst
    ,loanapp.ID as	record_id
	,case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe
	,E3user.ID as E3USERID
	,loanapp.originator_id as Originatorid
	,users.branchinstitution_id
	 FROM  mwlloanapp AS loanapp 
	 --OLIEPS-503
	INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
	INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
	INNER JOIN mwlborrower AS borr ON borr.loanapp_id = loanapp.id
	INNER JOIN dbo.mwlloandata AS loandata ON loandata.objowner_id = loanapp.id
	INNER join mwlAppStatus AS appStat on loanapp.ID=appStat.LoanApp_id
	LEFT JOIN dbo.mwaMWUser AS E3user ON E3user.id=loanapp.originator_id
	LEFT JOIN dbo.mwlcustomfield as DisclosureAnalyst ON DisclosureAnalyst.loanapp_id = loanapp.ID AND	DisclosureAnalyst.custFieldDef_ID='E16914219F3944DDB08117BDE784BCA4'
	
	LEFT JOIN dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
	LEFT join (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus
	where StatusDesc='05 - Application Taken' GROUP BY loanapp_id ,statusdesc) discappstat on loanapp.Id = discappstat.LoanApp_id
	LEFT join  (select Max(StatusdateTime) AS StatusdateTime, loanapp_id ,statusdesc FROM mwlAppStatus 
	where StatusDesc = '30.1 - Req for Re-disclosure'  GROUP BY loanapp_id ,statusdesc ) rediscappstat ON loanapp.Id = rediscappstat.LoanApp_id
	
	WHERE borr.sequencenum=1 AND loanapp.LoanNumber is not null AND loanapp.LoanNumber <> '' 
	AND loanapp.ChannelType like '%RETAIL%'   
	AND loanapp.CurrentStatus  IN ('05 - Application Taken','06 - Disclosures Out','30.1 - Req for Re-disclosure','30.2 - Re-disclosure Out')  	
	)
	select * from CTE_SB where Originatorid in (select id from @tblOriginators e3user inner join tblusers olieUser on e3user.id=olieuser.e3userid
	where olieuser.urole in(2,3,10) )

	End

END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDownPaymentSource]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDownPaymentSource]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDownPaymentSource]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT DownPaymentSourceID, SourceOfdownPayment FROM tblDownPaymentSource
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetEditModeData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetEditModeData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_GetEditModeData]
(
@formID int
)
AS
BEGIN
	 select Description,filename,uroleid from tblForms a, tblFormroles b where a.formID=b.formID and a.formID=@formID
END





GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetEmailId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetEmailId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sp_GetEmailId
	-- Add the parameters for the stored procedure here
        
@userid int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select upassword,ufirstname +' '+ ulastname as FLName, uemailid,ufirstname +' '+ ISNULL(MI,'') +' '+ ulastname as UserName,Roles,u.urole  from tblusers u left outer join tblroles r on r.Id=u.urole where userid =@userid
 -- Insert statements for procedure here
	
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetEscrowInfo2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetEscrowInfo2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetEscrowInfo2]
(
@loanid varchar(Max),
@TransactionDate varchar(Max)
)
AS
BEGIN

select  TransactionDate, description,nameline1,TransactionAmt,EndEscrowBal from history
left join TransactionCode as transcode on history.TransactionCode = transcode.Code
left join payee on payee.payeeid = history.payeeid
where loanid = @loanid
and TransactionAmt < 0
and history.TransactionDate <= @TransactionDate
and (TransactionDate < getdate() and TransactionDate > dateadd(year,-1,getdate()))
order by history.TransactionDate desc
     
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetEscrowInfo3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetEscrowInfo3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetEscrowInfo3]
(
@loanid varchar(Max),
@TransactionDate varchar(Max)
)
AS
BEGIN

select  TransactionDate, description,nameline1,TransactionAmt,EndEscrowBal from history
left join TransactionCode as transcode on history.TransactionCode = transcode.Code 
left join payee on payee.payeeid = history.payeeid
where loanid =@loanid
and TransactionAmt < 0
and (history.TransactionDate >= @TransactionDate and history.TransactionDate <=@TransactionDate)
order by history.TransactionDate desc
     
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetEventList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetEventList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetEventList]

AS
BEGIN
	SELECT EventName,UniqueUrl,EventDate FROM dbo.tblRSVPEdits WHERE Status =1 order by EventDate asc
	
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetFalloutReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetFalloutReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   
CREATE PROCEDURE [dbo].[sp_GetFalloutReport]            
(            
@RoleID int,           
@strID varchar(2000),            
@BrokerId char(2),          
@TeamOPuids varchar(30),           
@Year char(4),            
@Channel varchar(15),          
@Status varchar(1000),          
@Status1 varchar(1000),          
@Status2 varchar(1000)          
)            
AS            
BEGIN            
            
Declare @SQL nvarchar(max)            
Set @SQL =''            
Set @SQL ='SELECT '''+ @Year +''' AS ARYear,          
   month(LockRecord.LockDateTime) AS LockMonth, convert(decimal(12,0),round(sum(isnull(loanData.AdjustedNoteAmt,0)),0))          
   AS TotalLockedVolume, convert(decimal(12,0),round(sum(isnull(Closed.AdjustedNoteAmt, isnull(Closed.AppraisedValue,0))),0))          
   AS ClosedVolume,convert(decimal(12,0),round(sum(isnull(NotClosed.AdjustedNoteAmt, isnull(NotClosed.AdjustedNoteAmt,0))),0))           
   AS NotClosedVolume, convert(decimal(12,0),round(SUM(isnull(ActiveStatusLoandat.AdjustedNoteAmt,           
   isnull(ActiveStatusLoandat.AdjustedNoteAmt,0))),0)) ActiveStatusVolume,           
   case when ISNULL(SUM(isnull(Closed.AdjustedNoteAmt, Closed.AdjustedNoteAmt)),0) + ISNULL(SUM(isnull(NotClosed.AdjustedNoteAmt,          
   NotClosed.AdjustedNoteAmt)),0) > 0 THEN           
   convert(decimal(12,0),ROUND((ISNULL(sum(isnull(Closed.AdjustedNoteAmt, Closed.AdjustedNoteAmt)),0) * 100)/ (ISNULL(sum(isnull(Closed.AdjustedNoteAmt, Closed.AdjustedNoteAmt)),0) + ISNULL(sum(isnull(NotClosed.AdjustedNoteAmt, NotClosed.AdjustedNoteAmt)) ,0)),0))           
   ELSE 0 END AS ''% Closed''  FROM mwlLoanApp  AS MainTable           
   Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = MainTable.id           
   INNER JOIN mwlLockRecord LockRecord ON  LockRecord.LoanApp_ID = MainTable.id  and  LockRecord.LockType=''LOCK''           
   LEFT JOIN  mwlloandata Closed ON Closed.ObjOwner_id = MainTable.ID           
   and  MainTable.CurrentStatus IN (select * from dbo.SplitString('''+ @Status +''','',''))           
   LEFT JOIN  mwlloandata NotClosed ON NotClosed.ObjOwner_id = MainTable.ID              
   AND (MainTable.CurrentStatus IN (select * from dbo.SplitString('''+ @Status1 +''','','')) OR          
   (LockRecord.LockExpirationDate < Getdate() AND MainTable.CurrentStatus IN (select * from dbo.SplitString('''+ @Status2 +''','',''))))           
   LEFT JOIN  mwlloandata ActiveStatusLoandat ON ActiveStatusLoandat.ObjOwner_id = MainTable.ID AND          
   LockRecord.LockExpirationDate > getdate() AND MainTable.CurrentStatus  IN ('''','''+ @Status2 +''')            
   Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = MainTable.id and Broker.InstitutionType = ''BROKER'' and           
   Broker.objownerName=''Broker''           
   Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = MainTable.id and Corres.InstitutionType = ''CORRESPOND'' and           
   Corres.objownerName=''Correspondent''            
   WHERE YEAR(LockRecord.LockDateTime) = '''+ @Year +''''          
          
                if (@Channel = 'broker')          
                           
                    Set @SQL = @SQL + '  and MainTable.ChannelType=''BROKER'''           
                           
                if (@Channel = 'retail')          
                           
                     Set @SQL = @SQL + ' and MainTable.ChannelType=''RETAIL'''          
                           
                if (@Channel = 'correspond')          
                           
                     Set @SQL = @SQL + ' and MainTable.ChannelType=''CORRESPOND'''           
                           
          
                if (@RoleID = 1)          
                           
                     Set @SQL = @SQL + 'and MainTable.ChannelType like ''%RETAIL%'''            
                           
          
                if (@TeamOPuids != '')          
                 BEGIN          
                    Set @SQL = @SQL + '  and Originator_id in (''' + @TeamOPuids + ''')'           
                 END          
                else          
               BEGIN          
                    if (@RoleID != 0 and @RoleID != 1)          
                    BEGIN          
                        if (len(@strID) = 0)            
                        BEGIN          
                                       
                            if (@RoleID  = 2)          
                                       
                                   Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+ @strID +''','','')))'          
                             
                                       
                        END          
                        else          
                        BEGIN          
                       if (@RoleID =  2)          
                                       
                                   Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+ @strID +''','','')))'          
                                       
            
                        END          
                    END          
                END          
          
          
                if (@BrokerId != 0 AND @BrokerId != '')          
     BEGIN                    
                       Set @SQL = @SQL + ' and (Broker.CompanyEmail = '''+ @BrokerId +''' or Corres.CompanyEmail = '''+ @BrokerId +''') '          
     END          
                   Set @SQL = @SQL + ' group by month(LockRecord.LockDateTime)'          
                if (@BrokerId != 0 and @BrokerId != '')          
                 BEGIN          
                   Set @SQL = @SQL + ' ,Broker.CompanyEmail,Corres.CompanyEmail '          
                 END          
                   Set @SQL = @SQL + '  order by month(LockRecord.LockDateTime)'          
 Print @SQL            
exec sp_executesql @SQL            
END   
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetFaqs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetFaqs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create  Procedure [dbo].[sp_GetFaqs]
@ModuleName varchar(100)
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	select  Row_Number() over(order by Id) SlNo,Id,Question,Answer,ModuleName,Published,DisplayOrder from dbo.tblfaqs where ModuleName=@ModuleName order by Id asc
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetFaqView]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetFaqView]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create  Procedure [dbo].[sp_GetFaqView]
@ModuleName varchar(100)
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	select  Row_Number() over(order by Id) SlNo,Id,Question,Answer,ModuleName,Published,DisplayOrder 
	from dbo.tblfaqs where ModuleName=@ModuleName and Published=1 order by Id asc
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetFormRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetFormRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetFormRoles]
(
    @formID int
)
AS
BEGIN
	 Select uroleid from tblformRoles where formID = @formID

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetGridConditionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetGridConditionData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetGridConditionData]
(
@loan_no VARCHAR(MAX)
)
AS
BEGIN
SELECT 'P' as DB,descriptn, id_value ,case id_value when '004' then '1'  when '001' then '2' when '003' then '3'  when '002' then '4' END AS 'Value'  FROM lookups WHERE (lookup_id = 'aau')
order by Value asc;
SELECT 'P' as DB,'PC' as DueBy,cond_no , a.record_id,a.parent_id,a.cond_text,
CASE WHEN (a.COND_ADDED_DATE IS NOT NULL AND (a.COND_RECVD_DATE IS NULL AND a.COND_CLEARED_DATE IS NULL)) THEN 'Open'
WHEN ((a.COND_ADDED_DATE IS NOT NULL AND a.COND_RECVD_DATE IS NOT NULL ) AND a.COND_CLEARED_DATE IS NULL) THEN 'Received'
WHEN (a.COND_CLEARED_DATE IS NOT NULL) THEN 'Cleared' 
ELSE 'OPEN' END AS STATUS,lu_cond_type as id_value,a.record_id,a.cond_cleared_date,b.descriptn,a.cond_recvd_date FROM dbo.condits a,lookups b
where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = 'aaw' )
and a.parent_id IN ( SELECT record_id FROM dbo.loandat WHERE loan_no=@loan_no)  
ORDER BY id_value,COND_NO ASC

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetGridData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetGridData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetGridData]

AS
BEGIN
select '0' as ctID, loanregid, isnull(tuser.MAID,0)AEId, isnull(loandocid,0)loandocid, case rloan.Assigned when '1' then 'true' else 'false' end as Assigned, case rloan.Completed when '1' then 'true' else 'false' end as Completed, tuser.ufirstName, 
tuser.uLastname,tuser.uemailid,tuser.mobile_ph, docname as LoanSubmissionFile   ,rloan.FileName as LoanRegistrationFile  ,  tuser.urole,rloan.userid,rloan.createddate as dttime, cuser.ufirstname + ' ' + cuser.ulastname as CompletedBy,rloan.TimeStamp,auser.ufirstname + ' ' + auser.ulastname as AssignTo
from tblLoanReg rloan  Left outer join tblloandoc rdoc  on rloan.loanregid = rdoc.uloanregid inner join tblusers tuser on rloan.userid=tuser.userid Left outer join tblusers cuser on rloan.CompletedBy=cuser.userid left outer join tblusers auser on rloan.AssignedBy=auser.userid where tuser.urole=2 and  rloan.FileName IS not NULL and rloan.FileName <> '' and  rloan.completed is null  order by dttime desc
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHIPEventList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHIPEventList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

                
CREATE PROCEDURE [dbo].[sp_GetHIPEventList]               
(                  
@sortField varchar(500),                  
@sortOrder varchar(100),                  
@whereClause varchar(1000),                  
@PageNo int,                   
@PageSize int                  
)                   
AS                  
BEGIN                  
IF @sortField  = ''                                              
  set @sortField = 'tblrsvpedits.EventID'            
                  
SET @sortField = @sortField + ' ' + @sortOrder               
                 
Declare @sql nvarchar(4000)            
                  
set @sql = ''                                              
     set @sql = 'Select count(*) from ( '                  
     set @sql =  @sql + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,                  
EventID, CreatedDate, EventName, EventDate, (EventCity+'', ''+EventState)as Location,uniqueurl,        
case Status When  0 then ''New'' when 1 then ''Publish'' When 2 then ''InActive'' end as Status ,status as status1              
FROM tblRSVPEdits '                  
                  
IF @whereClause <> ''                                                       
    BEGIN                                                   
    set @sql = @sql  + ' Where ' + @whereClause                                             
    END                                      
                  
set @sql = @sql + ' group by EventID, CreatedDate, EventName, EventDate, EventCity, EventState, Status ,uniqueurl'                                               
 set @sql = @sql + ' ) as t2 '                   
                  
print @sql                  
exec sp_executesql @sql                    
                  
                  
set @sql = ''                                              
     set @sql = 'Select * from ( '                   
set @sql =  @sql + 'SELECT                           
        ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,EventID, CreatedDate, EventName, EventDate, (EventCity+'', ''+EventState)as Location,         
case Status When  0 then ''UnPublish'' when 1 then ''Published'' When 2 then ''InActive'' end as Status ,status as status1    ,uniqueurl                  
FROM tblRSVPEdits '                  
                  
IF @whereClause <> ''                                                       
    BEGIN                                                   
    set @sql = @sql  + ' Where ' + @whereClause                                             
    END                                      
                  
set @sql = @sql + '  group by  EventID, CreatedDate, EventName, EventDate, EventCity, EventState, Status ,uniqueurl '                                               
set @sql = @sql + ' ) as t2 '                  
                  
set @sql = @sql + ' Where Row between (' + CAST( @PageNo AS NVARCHAR)  + ' - 1) * ' + Cast(@PageSize as nvarchar) + ' + 1 and ' + cast(@PageNo as nvarchar) + ' * ' + cast(@PageSize as nvarchar)                  
                  
print @sql                                            
exec sp_executesql @sql                  
                  
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHIPUserList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHIPUserList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  Stonegate    
-- Create date: 8th June 2012    
-- Description: This will get user list to bind in the grid    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_GetHIPUserList]    
(    
@sortField varchar(500),    
@sortOrder varchar(100),    
@whereClause varchar(1000),    
@PageNo int,     
@PageSize int    
)     
AS    
BEGIN    
IF @sortField  = ''                                
  set @sortField = 'tblUsers.userid'    
    
SET @sortField = @sortField + ' ' + @sortOrder    
  
set @whereClause =  REPLACE(@whereClause,'[uurole]','tblroles.roles')                                      
    
Declare @sql nvarchar(4000)    
    
set @sql = ''                                
     set @sql = 'Select count(*) from ( '    
     set @sql =  @sql + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,    
tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid,     
tblUsers.urole, tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,     
tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender,     
tblUsers.carrierid, Carrier.Carrier,tblroles.roles as uurole ,tblUsers.signedupdate     
FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID LEFT OUTER JOIN tblroles on tblUsers.urole=tblroles.id      
where urole = 18 '    
    
IF @whereClause <> ''                                         
    BEGIN                                     
    set @sql = @sql  + ' AND ' + @whereClause                               
    END                        
    
set @sql = @sql + ' group by tblUsers.userid,upassword, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid,     
tblUsers.urole, tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,     
tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender,     
tblUsers.carrierid, Carrier.Carrier,tblroles.roles ,tblUsers.signedupdate'                                 
 set @sql = @sql + ' ) as t2 '     
    
print @sql    
exec sp_executesql @sql      
    
    
set @sql = ''                                
     set @sql = 'Select * from ( '     
set @sql =  @sql + 'SELECT             
        ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid,     
tblUsers.urole, tblUsers.isadmin, Case When tblUsers.isactive = 1 Then ''Active'' Else ''Inactive'' End as isactive, tblUsers.uidprolender, tblUsers.uparent,     
tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender,     
tblUsers.carrierid, Carrier.Carrier,tblroles.roles as uurole ,tblUsers.signedupdate     
FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID LEFT OUTER JOIN tblroles on tblUsers.urole=tblroles.id      
where urole = 18 '    
    
IF @whereClause <> ''                                         
    BEGIN                                     
    set @sql = @sql  + ' AND ' + @whereClause                               
    END                        
    
set @sql = @sql + ' group by tblUsers.userid,upassword, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid,     
tblUsers.urole, tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,     
tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender,     
tblUsers.carrierid, Carrier.Carrier,tblroles.roles ,tblUsers.signedupdate'                                 
set @sql = @sql + ' ) as t2 '    
    
set @sql = @sql + ' Where Row between (' + CAST( @PageNo AS NVARCHAR)  + ' - 1) * ' + Cast(@PageSize as nvarchar) + ' + 1 and ' + cast(@PageNo as nvarchar) + ' * ' + cast(@PageSize as nvarchar)    
    
print @sql                              
exec sp_executesql @sql    
    
END 


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHistory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetHistory]
	-- Add the parameters for the stored procedure here
	@loanid varchar(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM History  where loanid=@loanid order by id desc
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHistoryNotes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHistoryNotes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetHistoryNotes]
	-- Add the parameters for the stored procedure here
	@loanid varchar(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT modyfieddate,notes,isnull(lastupdateby,0) lastupdateby FROM History  where loanid=@loanid order by id desc
    -- Insert statements for procedure here
	
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getHomeOwnerPdf]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getHomeOwnerPdf]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_getHomeOwnerPdf]
(
@sLoanID varchar(MAX)
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select top 1 * from tblFormsHomeowner where LoanType= @sLoanID
	order by formid desc
	END
--------------------------------------------------------------------------------------------------------------





GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHomeType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHomeType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetHomeType]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT HometypeID, Hometype FROM tblHomeType WHERE HometypeID IN (1,2,3)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHowlong]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHowlong]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetHowlong]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT  * FROM  tbllookups where Lookupname='TP'
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHUDApprovedDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHUDApprovedDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetHUDApprovedDoc]
(
@parent_id VARCHAR(MAX)
)
AS
BEGIN
select parent_id from prolend_di..docdata
where parent_id=@parent_id and doc_descriptn1='HUD-1 Approved' and group_id=6
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHUDApprovedDocImage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHUDApprovedDocImage]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetHUDApprovedDocImage]
(
@sLocation VARCHAR(MAX),
@record_id varchar(max)
)
AS
BEGIN
declare @a varchar(max)
set @a='select * from '+@sLocation+'..docimages DI inner join 
prolend_di..docdata DD on DI.record_id=DD.Record_id where DI.record_id='+@record_id+''
exec(@a)
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetIDAMs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetIDAMs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetIDAMs]
(
@userid VARCHAR(MAX),
@MAID VARCHAR(MAX)
)
AS
BEGIN
Select uidprolender from tblusers where userid = @userid union select distinct(uidprolender)uidprolender from tblusers where userid in (select userid from tblusers where urole=2 and MAID=@MAID)
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetInfo4Marksman]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetInfo4Marksman]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetInfo4Marksman]
	-- Add the parameters for the stored procedure here
	


@userid int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select ufirstname + ' ' + ulastname as fullname, city, [state], uemailid, phonenum, GroupName  from tblusers left join TblRealtyGroup on TblRealtyGroup.groupId = tblusers.realtorGroupId where urole = 5 and userid=@userid 
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetInfoForPayoffEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetInfoForPayoffEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetInfoForPayoffEmail]
(
@userid varchar (max),
@dbname varchar (max)
)
AS
BEGIN
declare @a varchar(max)
set @a='select ufirstname,ulastname,convert(varchar, payoffdate, 101) as payoffdate,description from tblpayoffstatement as payoff
join tblUsers as users on payoff.userid = users.userid 
left join' + @dbname + 'PayoffReason on payoff.payoffreasonid = payoffreason.code
 where users.userid ='+ @userid+''

exec(@a)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetInfoForWelComeEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetInfoForWelComeEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetInfoForWelComeEmail]
(
@loanno varchar (max)
)
AS
BEGIN
 select * from tblServiceInfo as serviceInfo
join tblUsers on serviceInfo.userid = tblUsers.userid 
where serviceInfo.loanno = @loanno

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLastLoanRequest]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLastLoanRequest]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLastLoanRequest]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
select top 1 Appfname,AppLname,AppPrimaryPhone from tblloanapplication order by dateentered desc
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLastNameAndEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLastNameAndEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetLastNameAndEmail]
(
@LoanNo VARCHAR(MAX)
)
AS
BEGIN
select cb.borr_last,LD.lt_usr_underwriter,users.user_email from loandat LD inner join cobo Cb on ld.record_id = cb.parent_id  inner join users on LD.lt_usr_underwriter=users.record_id where ld.loan_no = @LoanNo


END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetList_Subscribe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetList_Subscribe]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetList_Subscribe] 
	-- Add the parameters for the stored procedure here
	@EMail nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT EMail from Subscribe where EMail=@EMail
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetLoanAcquisitionReportFileIntake]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetLoanAcquisitionReportFileIntake]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
create PROCEDURE [dbo].[SP_GetLoanAcquisitionReportFileIntake]           
(            
@RoleID int,            
@Category varchar(100),
@CurrentState varchar(1000),
@CurrentStatus varchar(3000),
@Status1 varchar(1000) ,  
@CustFieldDef_ID  varchar(100),
@strFrom char(5)
)            
AS            
BEGIN            
            
Declare @SQL nvarchar(max)            
Set @SQL =''            
Set @SQL ='select ''E3'' as DB,loanapp.processorname,loanapp.DocumentPreparerName,loanapp.CurrDecStatusDate,LockRecord.LockDateTime, 
			COffices.Office as coffice, 
			loanapp.CurrDecStatusDate as Decisionstatusdate,loanapp.CurrPipeStatusDate as CurrentStatusDate ,
			loanapp.ChannelType as ChannelType,loanapp.DecisionStatus,OriginatorName,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,
			LoanNumber as loan_no,CloserName as Closer,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS, 
			appReceived.StatusDateTime as AppDate, 
			CASE TransType when NULL THEN '''' WHEN ''P'' THEN ''Purchase money first mortgage'' WHEN ''R'' THEN ''Refinance'' WHEN ''2''
			THEN ''Purchase money second mortgage'' WHEN ''S'' THEN ''Second mortgage, any other purpose'' WHEN ''A'' THEN ''Assumption'' WHEN ''HOP''
			THEN ''HELOC - other purpose'' WHEN ''HP'' THEN ''HELOC - purchase'' ELSE '''' END AS TransType, 
			case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA''
			End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, 
			LockRecord.LockExpirationDate,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'''') WHEN ''''
			THEN RTRIM(COffices.Company) ELSE RTRIM(Broker.Company) END as BrokerName,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,
			Loandata.LoanProgDesc as ProgramDesc ,Cust.stringvalue as UnderwriterType, 
			CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and
			Category !=''LENDER'' and Category !='''+ @Category +''' and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE 
			loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0 
			WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' 
			and Category !='''+ @Category +''')  and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState='''+ @CurrentState +''' ) 
			and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0 
			ELSE (SELECT count(a.ID) as TotalCleared  
			FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category !=
			'''+ @Category +''') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'' or CurrentState=
			'''+ @CurrentState +''') and a.objOwner_ID IN  
			(SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions 
			FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'' and Category != '''+ @Category +''') 
			and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER  
			,BOffices.Office as OFFICE_NAME1  
			from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and Sequencenum=1  
			Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id   
			left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
			Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = ''BROKER'' and 
			Broker.objownerName=''Broker''  
			left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id  and  LockRecord.LockType=''LOCK'' and  
			LockRecord.Status<>''CANCELED'' 
			Left join dbo.mwlInstitution as COffices on COffices.ObjOwner_id = loanapp.id and COffices.InstitutionType = ''CORRESPOND''
			and COffices.objownerName=''Contacts''  
			Left join dbo.mwlInstitution as BOffices on BOffices.ObjOwner_id = loanapp.id and BOffices.InstitutionType = ''BRANCH'' 
			and BOffices.objownerName=''BranchInstitution''    
			Left join mwlAppStatus appReceived on loanapp.ID=appReceived.LoanApp_id and appReceived.StatusDesc IN 
			((select * from dbo.SplitString('+@CurrentStatus+','','')))  
            left join mwlcustomfield as Cust on Cust.loanapp_id = loanapp.id  and Cust.CustFieldDef_ID = '''+ @CustFieldDef_ID +''''  

                  if ((@RoleID = 14 or @RoleID = 16) and @strFrom = 'file')
					BEGIN
                       Set @SQL = @SQL + ' where CurrentStatus  IN ('''+ @Status1 +''') '
                       Set @SQL = @SQL + ' and loanapp.ChannelType in(''CORRESPOND'',''BROKER'') '
                       Set @SQL = @SQL + ' and isnull(Cust.stringvalue,'')<>''Delegated'' '
                     END
                   

                 Set @SQL = @SQL + ' and borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  ORDER BY PER desc '
         
 Print @SQL            
exec sp_executesql @SQL            
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanApplication]
	-- Add the parameters for the stored procedure here

@LoanApplicationID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	SELECT     LoanApplicationID, LoanReasonID, HometypeID, Producttypeid, StateID, CreditScoreID, AnnulIncomeBorrower,LoanAmount, AssetBorrower,  
                RetirementAssetsBorrower, PropertyLocated, HomeOwned, PurchasePrise, DownPaymentAmt, PartOfTotalAssets, DownPaymentSourceID,   
                HaveRealtor, RealtorContactName, RealtorPhone, RealtorEmail, AppFName, AppLName,vAddressLine1,vAddressLine2, AppStateID, AppZip, AppPrimaryPhone, AppSecondaryPhone,   
                AppEmail, CurrentPropertyValue, PropertyPurchaseMonth, YearID, ExistingPuchasePrice, CurrentMortageBal, MonthlyPayment, SecondMortage,   
                SecondMortageBal, DateEntered, ModifiedBy, ModifiedDate, UserAppearUrl, PropertyTypeID, PropertyTypeName, ContactTime, ContactTimeID,
                Hometype, CreditScoreValue, StateName, LoanReason, ProductTypeName, AppStateName, Year, SourceOfdownPayment ,MAName,IsWorkingWithRealtor, PropertyRealtorCity,IsCashback,RealtorName,RealtorPhoneNo 
                FROM         (SELECT     tblLoanApplication.LoanApplicationID, tblLoanApplication.LoanReasonID, tblLoanApplication.HometypeID, tblLoanApplication.Producttypeid,   
                tblLoanApplication.StateID, tblLoanApplication.CreditScoreID, tblLoanApplication.AnnulIncomeBorrower, tblLoanApplication.LoanAmount, tblLoanApplication.AssetBorrower,   
                tblLoanApplication.RetirementAssetsBorrower, tblLoanApplication.PropertyLocated, tblLoanApplication.HomeOwned,   
                tblLoanApplication.PurchasePrise, tblLoanApplication.DownPaymentAmt, tblLoanApplication.PartOfTotalAssets,
                tblLoanApplication.DownPaymentSourceID, tblLoanApplication.HaveRealtor, tblLoanApplication.RealtorContactName,   
                tblLoanApplication.RealtorPhone, tblLoanApplication.RealtorEmail, tblLoanApplication.AppFName, tblLoanApplication.AppLName, tblLoanApplication.vAddressLine1, tblLoanApplication.vAddressLine2,  
                tblLoanApplication.AppStateID, tblLoanApplication.AppZip, tblLoanApplication.AppPrimaryPhone, tblLoanApplication.AppSecondaryPhone,   
                tblLoanApplication.AppEmail, tblLoanApplication.CurrentPropertyValue, tblLoanApplication.PropertyPurchaseMonth,   
                tblLoanApplication.YearID, tblLoanApplication.ExistingPuchasePrice, tblLoanApplication.CurrentMortageBal, 
                tblLoanApplication.MonthlyPayment, tblLoanApplication.SecondMortage, tblLoanApplication.SecondMortageBal,
                tblLoanApplication.DateEntered, tblLoanApplication.ModifiedBy, tblLoanApplication.ModifiedDate, tblLoanApplication.UserAppearUrl,   
                tblLoanApplication.PropertyTypeID, tblPropertyType.Name AS PropertyTypeName, tblContactTime.ContactTime,   
                tblLoanApplication.ContactTimeID, tblHomeType.Hometype, tblCreditScore.CreditScoreValue, tblStateNylex.StateName,   
                tblLoanReason.LoanReason, tblProductType.name AS ProductTypeName, tblStateNylex_1.StateName AS AppStateName, tblYear.Year,   
                tblDownPaymentSource.SourceOfdownPayment,tblLoanApplication.MAName,tblLoanApplication.IsWorkingWithRealtor, tblLoanApplication.PropertyRealtorCity,tblLoanApplication.IsCashback,tblLoanApplication.RealtorName,tblLoanApplication.RealtorPhoneNo   
                FROM          tblLoanApplication LEFT JOIN  
                tblPropertyType ON tblLoanApplication.PropertyTypeID = tblPropertyType.PropertyTypeID LEFT JOIN  
                tblContactTime ON tblLoanApplication.ContactTimeID = tblContactTime.ContactTimeID LEFT JOIN  
                tblHomeType ON tblLoanApplication.HometypeID = tblHomeType.HometypeID LEFT JOIN
                tblCreditScore ON tblLoanApplication.CreditScoreID = tblCreditScore.CreditScoreID LEFT JOIN  
                tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID LEFT JOIN  
                tblLoanReason ON tblLoanApplication.LoanReasonID = tblLoanReason.LoanReasonID LEFT JOIN  
                tblProductType ON tblLoanApplication.Producttypeid = tblProductType.producttypeid LEFT JOIN  
                tblStateNylex AS tblStateNylex_1 ON tblLoanApplication.AppStateID = tblStateNylex_1.StateID LEFT JOIN  
                tblYear ON tblLoanApplication.YearID = tblYear.YearID LEFT OUTER JOIN  
                tblDownPaymentSource ON tblLoanApplication.DownPaymentSourceID = tblDownPaymentSource.DownPaymentSourceID) AS App where loanApplicationID = @loanApplicationID
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanApplicationByAppID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanApplicationByAppID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanApplicationByAppID]
	-- Add the parameters for the stored procedure here

@LoanApplicationID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	
                            select *, tblPropertyType.Name AS PropertyTypeName, tblContactTime.ContactTime,    tblLoanApplication.ContactTimeID, tblHomeType.Hometype, tblCreditScore.CreditScoreValue, tblStateNylex.StateName,    tblLoanReason.LoanReason, tblProductType.name AS ProductTypeName, tblStateNylex_1.StateName AS AppStateName, tblYear.Year,    tblDownPaymentSource.SourceOfdownPayment from tblLoanApplication 
                LEFT JOIN   tblPropertyType ON tblLoanApplication.PropertyTypeID = tblPropertyType.PropertyTypeID 
                LEFT JOIN   tblContactTime ON tblLoanApplication.ContactTimeID = tblContactTime.ContactTimeID   
                LEFT JOIN   tblHomeType ON tblLoanApplication.HometypeID = tblHomeType.HometypeID 
                LEFT JOIN   tblCreditScore ON tblLoanApplication.CreditScoreID = tblCreditScore.CreditScoreID   
                LEFT JOIN   tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID   
                LEFT JOIN   tblLoanReason ON tblLoanApplication.LoanReasonID = tblLoanReason.LoanReasonID 
                LEFT JOIN   tblProductType ON tblLoanApplication.Producttypeid = tblProductType.producttypeid   
                LEFT JOIN   tblStateNylex AS tblStateNylex_1 ON tblLoanApplication.AppStateID = tblStateNylex_1.StateID    
                LEFT JOIN   tblYear ON tblLoanApplication.YearID = tblYear.YearID  
                LEFT OUTER JOIN   tblDownPaymentSource ON tblLoanApplication.DownPaymentSourceID = tblDownPaymentSource.DownPaymentSourceID 
                 where loanApplicationID=@loanApplicationID 

;

                select BI.*, LU.Description as HowLong ,LU1.Description as CoHowLong, ST.StateName + ' - ' + ST.Abbreviature as CoBowwowerState 
                from tblBorrowerinfo BI  
                left join tbllookups LU on BI.TimePeriod = LU.lookupid   
                left join tbllookups LU1 on BI.[Co-BorrowerTimePeriod] = LU1.lookupid 
                left join tblStateNylex ST on BI.[Co-BorrowerState]=ST.StateID
                where LU.Lookupname='TP' and LoanApplicationID = @LoanApplicationID
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanDocRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanDocRegister]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanDocRegister]

@loanregid int,
@uloanregid int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select loanregid as loanregid,filename as DocName  from tblloanreg where loanregid =@loanregid union select uloanregid as loanregid,DocName as DocName  from tblloandoc where uloanregid =@uloanregid
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanNoFronAppID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanNoFronAppID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Kanva>    
-- Create date: <2nd Jan 2013>    
-- Description: <This will get Loan number from mwlloanapp table for given loanappid>    
-- =============================================    
CREATE PROCEDURE sp_GetLoanNoFronAppID    
(    
 @LoanAppID varchar (200)     
)    
AS    
BEGIN    
 select     
  *     
 From     
  mwlloanapp     
 Where     
  ID = @LoanAppID    
END
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanNumbermwlLoanApp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanNumbermwlLoanApp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
    
CREATE procEDURE [dbo].[sp_GetLoanNumbermwlLoanApp]        
(        
@id varchar(100)        
)        
AS        
BEGIN        
         
        
select LoanNumber from mwlLoanApp where id =@id        
END 
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanProgram]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanProgram]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanProgram]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select '0' as record_id, '--Select Program--' as prog_code ,'' as prog_desc union select record_id,prog_code,prog_desc from PROGINFO
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanReason]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanReason]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanReason]
	-- Add the parameters for the stored procedure here
(
	@LoanReasonID int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT LoanReasonID=@LoanReasonID, LoanReason FROM tblLoanReason where LoanReasonID not in(1,2)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanReasonNew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanReasonNew]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanReasonNew]
	-- Add the parameters for the stored procedure here
(
	@LoanReasonID int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT LoanReasonID= @LoanReasonID, LoanReason FROM tblLoanReason where loanReasonid  in(3,4)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanReasonNewforUSDA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanReasonNewforUSDA]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanReasonNewforUSDA]
	-- Add the parameters for the stored procedure here
(
	@LoanReasonID int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT LoanReasonID=@LoanReasonID, LoanReason FROM tblLoanReason where loanReasonid in(3)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanRegister]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanRegister]


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select LR.*,LK.Description as TranscationType from tblloanreg LR  Left outer join tbllookups LK on LR.TransType=LK.Lookupid ORDER BY CreatedDate DESC
END
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanRegister1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanRegister1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanRegister1]

@userid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select LR.*,LK.Description as TranscationType from tblloanreg LR Left outer join tbllookups LK on LR.TransType=LK.Lookupid where userid=@userid ORDER BY CreatedDate DESC 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanSuspendedConditionText]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanSuspendedConditionText]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetLoanSuspendedConditionText]
(
@loan_no VARCHAR(MAX)
)
AS
BEGIN
select *from SUSPENSE where  parent_id in(select record_id from  loandat where loan_no =@loan_no )

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanSuspendedData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanSuspendedData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetLoanSuspendedData]
(
@LoanNo VARCHAR(MAX)
)
AS
BEGIN
select ScheduleID as SchId,LoanNo as Loan_No ,Comments, FeeSheet as filename,convert(varchar(35),SubmitedDate,101) as dttime  from tblScheduleClosing
where IsFromLoanSuspendedDoc =1 and  LoanNo=@LoanNo

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanTextData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanTextData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetLoanTextData]
(
@loan_no VARCHAR(MAX)
)
AS
BEGIN
select loan_no,docs_text,filename,dttime from tblLoanDocs
where rtrim(loan_no)=@loan_no
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getLoanType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getLoanType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [sp_getLoanType]  
(  
@sLoanID varchar(MAX)  
)  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 select LoanType from loan where LoanID =@sLoanID  
 END  
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetLockActivityReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetLockActivityReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GetLockActivityReport]        
(        

@strID varchar(2000),        
@oPuid varchar(2000)  
)        
AS        
BEGIN        
        
Declare @SQL nvarchar(max)        
Set @SQL =''        
Set @SQL ='SELECT ''E3'' as DB, LD2.PROCESSOR, pif.prog_code , loandat.record_id, loan_no,RTRIM(cobo.borr_last)+ '','' + RTRIM(cobo.borr_first)  AS BorrowerLastName, 
			lookupprogram.descriptn as LoanType, lookups.descriptn AS Purpose, 
			proginfo.prog_desc, LD2.doc_funded_date AS FundedDate, loandat.int_rate, convert(varchar(40),locks.lock_exp_date,101) as 
			LockExpires,convert (varchar(40),locks.LOCK_DATE,101) as LockDate, lt_loan_stats AS LoanStatus, convert(varchar(35),
			loandat.UW_RECVD_DATE,101) AS ReceivedInUW, 
			case LU_FINAL_HUD_STATUS when '''' then ''Pending'' when NULL then ''Pending'' else ''Approved'' end as LU_FINAL_HUD_STATUS,
			Loan_amt, brokers.brok_name as BrokerName 
			FROM loandat INNER JOIN cobo ON loandat.record_id = cobo.parent_id LEFT OUTER JOIN locks ON loandat.record_id = locks.parent_id 
			LEFT OUTER JOIN brokers ON loandat.lt_broker = brokers.record_id LEFT OUTER JOIN users ON lt_usr_underwriter = users.record_id
			LEFT OUTER JOIN proginfo ON loandat.lt_program = proginfo.record_id  
			INNER JOIN lookups ON loandat.lu_purpose = lookups.id_Value and lookups.lookup_id = ''AAE'' INNER JOIN lookups as lookups2 
			ON loandat.lu_loan_type = lookups2.id_Value and lookups2.lookup_id = ''ADC'' 
			left outer JOIN lookups AS lookups3 ON loandat.lu_loan_type = lookups3.id_value AND lookups3.lookup_id = ''ACY'' 
			INNER JOIN lookups AS lookupprogram ON loandat.lu_loan_type = lookupprogram.id_value AND lookupprogram.lookup_id = ''AAC''                  
			INNER JOIN lookups AS lookupsLock ON locks.LU_LOCK_STAT = lookupsLock.id_value AND lookupsLock.lookup_id = ''AAT''  
			left join loandat2 as LD2 on loandat.record_id=LD2.record_id 
			left join proginfo as PIF on loandat.record_id = PIF.record_id' 
               if (len(@strID) = 0)
                BEGIN
                    Set @SQL = @SQL + '  where lt_loan_stats  IN (''Locked'','')'                      
                     Set @SQL = @SQL + ' and (loandat.lt_broker in ('''+ @strID +''') or loandat2.lt_usr_lorep in ('''+ @oPuid +'''))'


                END
                else
                BEGIN
                     Set @SQL = @SQL + ' where lt_loan_stats  IN (''Locked'','')'
                    
                     Set @SQL = @SQL + ' and (loandat.lt_broker in ('''+ @strID +''') or loandat2.lt_usr_lorep in ('''+ @oPuid +'''))'
				END

 

  
 Print @SQL        
exec sp_executesql @SQL        
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetLockActivityReportFromDt]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetLockActivityReportFromDt]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetLockActivityReportFromDt]             
(              
@RoleID int,             
@strID varchar(2000),      
@TeamOPuids varchar(30),              
@FromDate  varchar(15),    
@ToDate varchar(15),    
@oPuid varchar(2000)    
)              
AS              
BEGIN              
              
Declare @SQL nvarchar(max)              
Set @SQL =''              
Set @SQL ='select ''E3'' as DB, loanapp.ID as record_id,loanapp.Channeltype as BusType,LoanNumber as loan_no,borr.firstname as BorrowerFirstName ,    
   borr.lastname as BorrowerLastName,CurrentStatus as LoanStatus,    
   convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,    
   LoanData.LoanProgramName as Prog_desc ,loanapp.Originatorname,    
   case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance''     
   end as Purpose,CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed,(Loandata.NoteRate * 100) as Int_rate,    
   loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,    
   convert(varchar(40),LockRecord.LockExpirationDate,101) as LockExpires,LockRecord.LockDateTime,    
   convert(varchar(35),LockRecord.LockDateTime,101) as Lockdate,UnderwriterName as UnderWriter,CloserName as Closer,    
   CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,'''' as FundedDate,    
   '''' as ReceivedInUW,LoanData.ShortProgramName as prog_code     
   from mwlLoanApp  loanapp       
   Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id      
   Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id                      
   left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id      
   Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''    
   left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED''     
   Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''    
   where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''     
   and loanapp.ChannelType like ''%RETAIL%'''    
    
                if (@TeamOPuids != '')    
                BEGIN    
                     Set @SQL = @SQL + ' and Originator_id in (''' + @TeamOPuids + ''')'    
                END    
                else    
                BEGIN    
                    if (@RoleID != 0 and @RoleID != 1 and @RoleID != 11)    
                    BEGIN    
                       if (len(@strID) = 0)    
                        BEGIN    
                            if (@RoleID = 2)    
                            BEGIN    
                                  Set @SQL = @SQL + 'and (Originator_id in ('''+ @strID +'''))'    
                            END    
                            else if (@RoleID = 3)    
       BEGIN    
                                  Set @SQL = @SQL + ' and (loanapp.ID in ('''+ @strID +''') or Originator_id in ('''+ @strID +''' ) )'    
                            END    
                            else if (@RoleID =  10 or @RoleID  = 7)    
                            BEGIN    
                                Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID +''') or loanapp.id in ('''+ @strID +'''))'    
                            END    
                        END    
                        else    
                        BEGIN    
                            if (@RoleID  = 2)    
                            BEGIN    
                                 Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID +'''))'    
                            END    
                            else if (@RoleID = 3)    
                            BEGIN    
                                 Set @SQL = @SQL + ' and (loanapp.ID in ('''+ @strID +''') or Originator_id in (''' + @oPuid +''') )'    
                            END    
                            else if (@RoleID = 10 or  @RoleID = 7)    
                            BEGIN    
                                Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID +''') or loanapp.id in ('''+ @strID +'''))'    
                            END    
                        END    
                    END    
                END    
               Set @SQL = @SQL + ' and  LockRecord.LockDateTime between ''' + @FromDate + ''' and ''' + @ToDate + ''''    
         
 Print @SQL              
exec sp_executesql @SQL              
END   
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetLockActivityReportWithRegion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetLockActivityReportWithRegion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[SP_GetLockActivityReportWithRegion]
(          
        
@strID varchar(2000),  
@FromDate VARCHAR(15),
@ToDate VARCHAR(15),
@oPuid varchar(2000)
)          
AS          
BEGIN          
          
Declare @SQL nvarchar(max)          
Set @SQL =''          
Set @SQL ='select ''E3'' as DB, loanapp.ID as record_id,loanapp.Channeltype as BusType,LoanNumber as loan_no,borr.firstname as BorrowerFirstName ,
			borr.lastname as BorrowerLastName,CurrentStatus as LoanStatus,loanapp.Originatorname, 
			convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,LoanData.LoanProgramName as Prog_desc, 
			case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' 
			end as Purpose,CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed,(Loandata.NoteRate * 100) as Int_rate, 
			loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,
			convert(varchar(40),LockRecord.LockExpirationDate,101) as LockExpires,LockRecord.LockDateTime,
			convert(varchar(35),LockRecord.LockDateTime,101) as Lockdate,UnderwriterName as UnderWriter,CloserName as Closer,
			CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,'''' as
			 FundedDate, '''' as ReceivedInUW,LoanData.ShortProgramName as prog_code  from mwlLoanApp  loanapp   
			Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
			Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id  
			left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
			Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' 
			and Broker.objownerName=''Broker'' 
			left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK'' and 
			LockRecord.Status<>''CANCELED'' 
			Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' 
			and Corres.objownerName=''Correspondent'' 
			where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''''

                if (len(@oPuid) = 0)
                 BEGIN
                      Set @SQL = @SQL + 'and (Originator_id in ('''+ @oPuid +''') or loanapp.id in (''' + @strID  + '''))' 
                 END
                else
					BEGIN
                 Set @SQL = @SQL + ' and (Originator_id in (" + OPuid.ToString() + ") or loanapp.id in (''' + @strID  + '''))' 
					END

				Set @SQL = @SQL + ' and LockRecord.LockDateTime between ''' + @FromDate + ''' and ''' + @ToDate + '''' 
				Set @SQL = @SQL + ' and loanapp.ChannelType like ''%RETAIL%''' 

  
     
 Print @SQL          
exec sp_executesql @SQL          
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLockStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLockStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLockStatus]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select '0' as id_value, '--Select LockStatus--' as descriptn union  select id_value ,descriptn from lookups where lookup_id='AAT' and descriptn in('REGISTERED','LOCKED')
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLockStatusById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLockStatusById]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLockStatusById]

@id_value int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select id_value ,descriptn from lookups where lookup_id='AAT' and id_value=@id_value
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetMAData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetMAData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetMAData]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select '--Select MA--' as uname ,0 as userid union select ufirstname + ' ' + ulastname as uname, userid from tblusers where (urole =2 or urole =3 or urole=10) and isactive=1 order by uname
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetMAEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetMAEmail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsUserRegistrationE3.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetMAEmail]
(
@ufirstname varchar(Max),
@ulastname varchar(Max)
)
AS
BEGIN
	 select uemailid,userid from tblusers where ufirstname =@ufirstname and ulastname =@ulastname and urole in (2,3,7,10) and isactive=1
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetManageListingData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetManageListingData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Harsh Pandit
-- Create date: 19 April 2010
-- Description:	to get the data for manage listing
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetManageListingData]
	-- Add the parameters for the stored procedure here
	@RealtorID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @PropID VARCHAR(50) 

	CREATE TABLE #Temp(
	[ID] [int] IDENTITY(1,1) NOT NULL,  
 [PropID] [int] NULL,  
 [RealtorID] [int] NULL,  
 [PropStatus] [varchar](50) NULL,  
 [ListingTYpe] [varchar](50) NULL,  
 [BedRooms] [int] NULL,  
 [Baths] [decimal](18, 1) NULL,  
 [Price] [decimal](18, 2) NULL,  
 [DaysListed] [int] NULL,  
 [Street] [varchar](50) NULL,  
 [State] [varchar](50) NULL,  
 [City] [varchar](50) NULL,  
 [Zipcode] [varchar](50) NULL,  
 [YearBuilt] [int] NULL,  
 [IsActive] [bit] NULL ,  
 [PropDesc] [ntext] NULL,  
 [lat] [decimal](18, 6) NULL,  
 [lng] [decimal](18, 6) NULL ,  
 [Area] [int] NULL,  
 [logoname] [varchar](50) NULL,  
 [uemailid] [varchar](50) NULL,  
 [PropImageName] [varchar](50) NULL,  
 [FullAddress] [varchar] (250),  
 [Amount] [decimal] (18,0)  
	)
	DECLARE db_cursor CURSOR FOR  
	SELECT PropID from tblProperties 

	OPEN db_cursor   
	FETCH NEXT FROM db_cursor INTO @PropID

	WHILE @@FETCH_STATUS = 0   
	BEGIN   
INSERT INTO #Temp (PropID,RealtorID,PropStatus,ListingType,Bedrooms,baths,price,dayslisted,street,[state],city,zipcode,yearbuilt,lat,lng,Area,logoname,uemailid,PropImageName,FullAddress,Amount,isactive)  
   SELECT  TOP(1) prop.PropID, prop.RealtorID,PropStatus,ListingType,Bedrooms,baths,price,datediff(day,CreatedDate,getdate()) as dayslistedcalc,street,prop.[state],prop.city,zipcode,yearbuilt,prop.lat,prop.lng,prop.Area,
			CASE WHEN users.logoname='' THEN 'nologo.gif' ELSE ISNULL(users.logoname,'nologo.gif')END logoname,users.uemailid,  			
			ISNULL(PropImageName,'noHome.png')PropImageName,street + '+' + prop.[state] + '+' + prop.city + '+' + zipcode as FullAddress, Amount,prop.isactive  
			FROM 	tblProperties prop
			LEFT OUTER JOIN tblPropertyImage propImages ON prop.propid = propImages.propid
			JOIN tblusers users ON users.userid = prop.realtorID
			WHERE prop.PropID=@PropID and prop.realtorid = @RealtorID
			
		   FETCH NEXT FROM db_cursor INTO @PropID   
	END   

	SELECT * FROM #Temp
	DROP TABLE #Temp
	CLOSE db_cursor   
	DEALLOCATE db_cursor  
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetMortgageAdvisorReports]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetMortgageAdvisorReports]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Stonegate    
-- Create date: 31st July 2012    
-- Description: This will get user list to bind in the grid    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_GetMortgageAdvisorReports]    
(    
@sortField varchar(500),    
@sortOrder varchar(100),    
@whereClause varchar(1000),    
@PageNo int,     
@PageSize int,
@CurrentStatus varchar(500),
@RoleID varchar(20),
@OriginatorID varchar(2000),
@oPuid varchar(2000)    
)     
AS    
BEGIN    
IF @sortField  = ''                                
  set @sortField = 'LoanApp.LoanNumber'    
    
SET @sortField = @sortField + ' ' + @sortOrder                                                
Declare @sql nvarchar(max)
set @sql = ''                                
     set @sql = 'Select count(*) from ( '    
     set @sql =  @sql + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,
 ''E3'' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue as ProcessorName,
loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,
CloserName as Closer,borr.lastname as BorrowerLastName,borr.firstname as BorrowerFirstName,
'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate, approvalStat.StatusDateTime as DateApproved,
case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe, 
case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,
loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,
CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,
CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate ,  
CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER'' and a.objOwner_ID IN 
	( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0  
		WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'')  
		and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'') and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  
		WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE (SELECT count(a.ID) as TotalCleared  FROM dbo.mwlCondition a 
		where (DueBy=''Prior to Closing'' and Category !=''LENDER'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'') 
			and a.objOwner_ID IN  (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions 
FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  
	WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER  from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
	Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id  left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id 
	Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
	left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED''  
	Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc=''01 - Registered'' 
	Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved'' 
	Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName=''mwlLoanData'' and fieldname=''refipurpose'' 
	Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' 
	left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID and CustFieldDef_id in(select Id from mwsCustFieldDef where CustomFieldName=''Retail Processor'') 
	where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and  loanapp.ChannelType like ''%RETAIL%'' 
and CurrentStatus IN ('''+@CurrentStatus+''' ) '
-- Temp commented to get test data
--If @RoleID <> '0' Or @RoleID <> '1'
--Begin
--	If @RoleID = '2'
--	Begin
--		set @sql =  @sql + ' and (Originator_id in ('''+@OriginatorID+'''))'
--	End
--	If @RoleID = '3'
--	Begin
--		set @sql =  @sql + ' and (loanapp.ID in ('''+@OriginatorID+''') or Originator_id in ('''+@oPuid+'''))'
--	End
--	If @RoleID = '10' Or @RoleID = '7'
--	Begin
--		set @sql =  @sql + ' and (Originator_id in ('''+@OriginatorID+''') or loanapp.id in ('''+@OriginatorID+'''))'
--	End
--	If @RoleID <> '2' Or @RoleID <> '3' Or @RoleID <> '10' Or @RoleID <> '7'  
--	Begin
--		set @sql =  @sql + ' and (Originator_id in ('''+@OriginatorID+''')) '
--		IF @oPuid <> ''
--		Begin
--			set @sql =  @sql + ' and (Originator_id in ('''+@oPuid+'''))'
--		End
--	End
--End
-- End temp comment 
    
IF @whereClause <> ''                                         
    BEGIN                                     
    set @sql = @sql  + ' AND ' + @whereClause                               
    END                        
    
set @sql = @sql + ' group by loanapp.casenum,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue,
loanapp.currentStatus,loanapp.DecisionStatus,loanapp.ID,LoanNumber,
CloserName,borr.lastname,borr.firstname,loanapp.CurrentStatus,appStat.StatusDateTime,approvalStat.StatusDateTime,
TransType,LoanData.FinancingType,loanData.AdjustedNoteAmt,LockRecord.Status, 
LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName,
Broker.Company, Corres.Company, Lookups.DisplayString,
borr.CompositeCreditScore, EstCloseDate,LoanApp.LoanNumber'                                 
 set @sql = @sql + ' ) as t2 '     
    
print @sql    
exec sp_executesql @sql      
    
    
set @sql = ''                                
     set @sql = 'Select * from ( '     
set @sql =  @sql + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,
''E3'' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue as ProcessorName,
loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,
CloserName as Closer,borr.lastname as BorrowerLastName,borr.firstname as BorrowerFirstName,
'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate, approvalStat.StatusDateTime as DateApproved,
case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe, 
case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,
loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,
CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,
CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate ,  
CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER'' and a.objOwner_ID IN 
	( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0  
		WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'')  
		and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'') and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  
		WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE (SELECT count(a.ID) as TotalCleared  FROM dbo.mwlCondition a 
		where (DueBy=''Prior to Closing'' and Category !=''LENDER'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'') 
			and a.objOwner_ID IN  (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions 
FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  
	WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER  from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
	Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id  left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id 
	Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
	left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED''  
	Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc=''01 - Registered'' 
	Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved'' 
	Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName=''mwlLoanData'' and fieldname=''refipurpose'' 
	Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' 
	left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID and CustFieldDef_id in(select Id from mwsCustFieldDef where CustomFieldName=''Retail Processor'') 
	where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and  loanapp.ChannelType like ''%RETAIL%'' and  
CurrentStatus  IN ('''+@CurrentStatus+''' ) '    
-- Temp commented to get test data    
--If @RoleID <> '0' Or @RoleID <> '1'
--Begin
--	If @RoleID = '2'
--	Begin
--		set @sql =  @sql + ' and (Originator_id in ('''+@OriginatorID+'''))'
--	End
--	If @RoleID = '3'
--	Begin
--		set @sql =  @sql + ' and (loanapp.ID in ('''+@OriginatorID+''') or Originator_id in ('''+@oPuid+'''))'
--	End
--	If @RoleID = '10' Or @RoleID = '7'
--	Begin
--		set @sql =  @sql + ' and (Originator_id in ('''+@OriginatorID+''') or loanapp.id in ('''+@OriginatorID+'''))'
--	End
--	If @RoleID <> '2' Or @RoleID <> '3' Or @RoleID <> '10' Or @RoleID <> '7'  
--	Begin
--		set @sql =  @sql + ' and (Originator_id in ('''+@OriginatorID+''')) '
--		IF @oPuid <> ''
--		Begin
--			set @sql =  @sql + ' and (Originator_id in ('''+@oPuid+'''))'
--		End
--	End
--End  
-- End test comment    
IF @whereClause <> ''                                         
    BEGIN                                     
    set @sql = @sql  + ' AND ' + @whereClause                               
    END                        
    
set @sql = @sql + ' group by loanapp.casenum,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue,
loanapp.currentStatus,loanapp.DecisionStatus,loanapp.ID,LoanNumber,
CloserName,borr.lastname,borr.firstname,loanapp.CurrentStatus,appStat.StatusDateTime,approvalStat.StatusDateTime,
TransType,LoanData.FinancingType,loanData.AdjustedNoteAmt,LockRecord.Status, 
LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName,
Broker.Company, Corres.Company, Lookups.DisplayString,
borr.CompositeCreditScore, EstCloseDate,LoanApp.LoanNumber'                                 
 set @sql = @sql + ' ) as t2 '    
    
set @sql = @sql + ' Where Row between (' + CAST( @PageNo AS NVARCHAR)  + ' - 1) * ' + Cast(@PageSize as nvarchar) + ' + 1 and ' + cast(@PageNo as nvarchar) + ' * ' + cast(@PageSize as nvarchar)    
    
print @sql                              
exec sp_executesql @sql    
    
END 


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetMyReportClosing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetMyReportClosing]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

      
  
        
CREATE PROCEDURE DBO.SP_GetMyReportClosing          
(            
          
@strID varchar(2000),             
@CurrentStatus varchar(3000)   
)            
AS            
BEGIN            
            
Declare @SQL nvarchar(max)            
Set @SQL =''            
Set @SQL ='select ''E3'' as DB,loanapp.DecisionStatus, loanapp.ID as record_id,LoanNumber as loan_no,  
   borr.lastname as BorrowerLastName,loanapp.DecisionStatus,  
   loanapp.CurrentStatus as LoanStatus,DateAppSigned as SubmittedDate,LoanProgramName as LoanProgram,  
   case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when   
   TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,borr.CompositeCreditScore as CreditScoreUsed,  
   loanData.AdjustedNoteAmt as LoanAmount,  
   convert(varchar(35),EstCloseDate,101) AS ScheduleDate,LockStatus,LockExpirationDate,  
   UnderwriterName as UnderWriter,CloserName as Closer,Institute.Company as BrokerName,   
   Institute.CustomDataOne as TitleCompany,  
   AppStatus.StatusDateTime as DocsSentDate,FundedStatus.StatusDateTime as FundedDate,  
   '''' as LU_FINAL_HUD_STATUS   
   from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
   Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id   
   left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id   
   Left join dbo.mwlInstitution as Institute on Institute.ObjOwner_id = loanapp.id and   
   InstitutionType = ''BROKER'' and objownerName=''BROKER'' Left join mwlappstatus  
   as AppStatus on AppStatus.LoanApp_id=loanapp.id  and AppStatus.StatusDesc like ''%Docs Out%''   
  
   Left join mwlappstatus as FundedStatus on FundedStatus.LoanApp_id=loanapp.id   
   and FundedStatus.StatusDesc like ''%Funded''   
   where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and   
                   
                 CurrentStatus  IN ('''+ @CurrentStatus +''') '  
  
                if (len(@strID) =  0)  
                begin  
                     Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID+ '''))'  
                end  
                else  
                begin  
                     Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID+ '''))'  
                end  
  
    
    
  Print @SQL  
exec sp_executesql @SQL            
END 
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetMyReportClosingWithRegion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetMyReportClosingWithRegion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.sp_GetMyReportClosingWithRegion          
(              
         
@strID varchar(2000),               
@CurrentStatus varchar(3000),     
@oPuid varchar(100)       
)              
AS              
BEGIN              
              
Declare @SQL nvarchar(max)              
Set @SQL =''              
Set @SQL ='select ''E3'' as DB,loanapp.ChannelType,loanapp.OriginatorName,loanapp.DecisionStatus, loanapp.ID as record_id,LoanNumber as loan_no,    
   borr.lastname as BorrowerLastName,borr.firstname as BorrowerFirstName,loanapp.DecisionStatus,loanapp.CurrentStatus as LoanStatus,    
   DateAppSigned as SubmittedDate,LoanProgramName as LoanProgram,    
   case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance''     
   end as TransTYpe,borr.CompositeCreditScore as CreditScoreUsed,loanData.AdjustedNoteAmt as LoanAmount,    
   convert(varchar(35),EstCloseDate,101) AS ScheduleDate,LockStatus,LockExpirationDate,UnderwriterName as UnderWriter,    
   CloserName as Closer,Institute1.Company as BrokerName, Institute.CustomDataOne as TitleCompany,    
   AppStatus.StatusDateTime as DocsSentDate,FundedStatus.StatusDateTime as FundedDate,'''' as LU_FINAL_HUD_STATUS     
   from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  Inner join dbo.mwlloandata as loanData    
   on loanData.ObjOwner_id = loanapp.id     
   left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id       
   Left join dbo.mwlInstitution as Institute on Institute.ObjOwner_id = loanapp.id and  Institute.InstitutionType = ''Branch''    
   and Institute.objownerName=''BranchInstitution''    
   Left join dbo.mwlInstitution as Institute1 on Institute1.ObjOwner_id = loanapp.id and Institute1.InstitutionType = ''Title''    
   and Institute1.objownerName=''Contacts''      
   Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and AppStatus.StatusDesc like ''%Docs Out%''      
   Left join mwlappstatus as FundedStatus on FundedStatus.LoanApp_id=loanapp.id  and FundedStatus.StatusDesc like ''%Funded''     
   where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and      
            CurrentStatus  IN (select * from dbo.SplitString('''+ @CurrentStatus +''','',''))'    
    
                if (len(@oPuid) =  0)    
     begin    
                         
         Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+ @oPuid +''','',''))  or Institute.office in(select * from dbo.SplitString('''+ @oPuid +''','',''))) '    
    
     end    
                else    
     begin    
                         
       Set @SQL = @SQL + '  and (Originator_id in (select * from dbo.SplitString('''+ @oPuid +''','',''))  or Institute.office in(select * from dbo.SplitString('''+ @oPuid +''','','')))'    
     end    
                Set @SQL = @SQL + '  and loanapp.ChannelType=''RETAIL'''     
                      
Print @SQL    
exec sp_executesql @SQL              
END     
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetNewRateByRateID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetNewRateByRateID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetNewRateByRateID]
(
@NewRateID varchar (max)
)
AS
BEGIN

select * from tblnewrates where NewRateID =@NewRateID
 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetNewRates]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetNewRates]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetNewRates]

AS
BEGIN

select * from tblNewrates 
 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPAEBDMIDs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPAEBDMIDs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/08/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetPAEBDMIDs]
(
@lt_usr_lorep varchar(max)
)
AS
BEGIN
select isnull(record_id,0) as record_id from brokers where lt_usr_lorep in (@lt_usr_lorep)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPAEIDs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPAEIDs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetPAEIDs]
(
@lt_usr_lorep VARCHAR(MAX)
)
AS
BEGIN
select isnull(record_id,0) as record_id from brokers where lt_usr_lorep in(@lt_usr_lorep) 

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPaymentInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPaymentInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetPaymentInfo]
(
@loanid varchar(Max),
@TransactionDate varchar(Max)
)
AS
BEGIN

select  TransactionDate, TransactionAmt,description,EndPrincipalBal,InterestRate,
EscrowAmt,LateChargeAmt,OtherFundsAmt from history join TransactionCode as transcode on history.TransactionCode = transcode.Code
where loanid = @loanid and TransactionAmt > 0 
and (TransactionDate < @TransactionDate and TransactionDate > dateadd(year,-1,@TransactionDate))
order by history.TransactionDate desc
     
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPaymentInfo2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPaymentInfo2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetPaymentInfo2]
(
@loanid varchar(Max),
@TransactionDate varchar(Max)
)
AS
BEGIN

select TransactionDate, TransactionAmt,description,EndPrincipalBal,InterestRate,
EscrowAmt,LateChargeAmt,OtherFundsAmt from history join TransactionCode as transcode on history.TransactionCode = transcode.Code 
where loanid = @loanid and TransactionAmt > 0 
and history.TransactionDate <= @TransactionDate 
and (TransactionDate < getdate() and TransactionDate > dateadd(year,-1,getdate()))
order by history.TransactionDate desc
     
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPaymentInfo3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPaymentInfo3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetPaymentInfo3]
(
@loanid varchar(Max),
@TransactionDate varchar(Max)
)
AS
BEGIN

select TransactionDate, TransactionAmt,description,EndPrincipalBal,InterestRate,
EscrowAmt,LateChargeAmt,OtherFundsAmt from history join TransactionCode as transcode on history.TransactionCode = transcode.Code 
where loanid = @loanid and TransactionAmt > 0 
and (history.TransactionDate >= @TransactionDate and history.TransactionDate <=@TransactionDate)
order by history.TransactionDate desc 
     
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPayoffReasons]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPayoffReasons]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetPayoffReasons]

AS
BEGIN
select * from payoffreason
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPipelineViewData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPipelineViewData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

      
CREATE PROCEDURE DBO.sp_GetPipelineViewData      
(          
     
@sLoanID  varchar(20)
)          
AS          
BEGIN          
          
Declare @SQL nvarchar(max)          
Set @SQL =''          
Set @SQL ='select top 1 ml.DisplayString as BUSTYPE,loanapp.casenum as FHANumber,''E3'' as DB,loanapp.ID as record_id,loanapp.DecisionStatus,
			LoanNumber as LOAN_NO, PropertyAddress.Street as Prop_Addr , PropertyAddress.City + '',''  +  PropertyAddress.State + '','' + 
			PropertyAddress.Zipcode as Prop_Addr1,loanData.AdjustedNoteAmt as LOAN_AMT,
			CONVERT(DECIMAL(10,3),loanData.NoteRate * 100) as INT_RATE,CONVERT(DECIMAL(10,3),loanData.LTV *100)LTV ,
			CONVERT(DECIMAL(10,3),loanData.CLTV *100) CLTV,loanapp.CurrentStatus as LT_LOAN_STATS, 
			RTRIM(LoRep.LastName) +  '''' + RTRIM(LoRep.FirstName) AS INTERV_NAME ,
			LoRep.Email AS INTERV_EMAIL,PhoneNo.PhoneNumber as INTERV_MOBILE,PhoneNo.PhoneNumber as INTERV_PH, 
			uSummary.ApprvlExpirDate as APPR_EXP_DATE,Appraiser.FirstName + '' '' + Appraiser.LastName as LT_ORIG_APPRAIS,
			RTRIM(borr.lastname) + '''',''  '' + RTRIM(borr.FirstName) as BORR_AKA1,
			AppDocument.DateOrder AS DOC_REQ_DATE,AppDocument.ExhibitDate AS DOC_SENT_DATE,AppDocument.DateReceived AS DOC_BACK_DATE,
			CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as  BROK_NAME, 
			'''' as App_Date,DateAppSigned as App_Recvd_Date,ReorderDate as DOC_REVIEWED_DATE,LoanData.LoanProgramName as LoanProgram,
			Loandata.LoanProgDesc as PROG_DESC,  
			Offices.Office as OFFICE_NAME1,case TransType when NULL then '''' when ''P'' then ''Purchase money first mortgage'' 
			when ''R'' then ''Refinance'' when ''2'' then ''Purchase money second mortgage'' when ''S'' then ''Second mortgage, any other purpose'' when ''A'' 
			then ''Assumption'' when ''HOP'' then ''HELOC - other purpose'' when ''HP'' then ''HELOC - purchase'' else '''' end as PURPOSE,
			CreditScore as CreditScoreUsed,
			convert(varchar(35),EstCloseDate,101) AS ScheduleDate,LockStatus,LoanApp.LockExpirationDate,UnderwriterName as UnderWriter,
			uEmail.Email as Underwriter_Email, 
			DocumentPreparerName as DOCDRAWER,Institute.Title as LT_ESCROW_CO,
			Institute.Company as  Comp_Name,   
			case PropertyAddress.PropertyType when NULL then '''' when ''SINGLE FAM'' then ''Single family dwelling''
			when ''2-4 FAMILY'' then ''2-4 family dwelling'' when ''MULTI-FAM'' then ''Multi-family'' when ''CONDO'' then ''Condominium'' when 
			''TOWNHOUSE'' then ''Townhouse'' when ''PUD'' then ''PUD'' when ''OTHER'' then ''Other'' when ''CO-OP APT'' then 
			''Co-operative apartment'' when ''COMM RESID'' then ''Home & business combined'' when ''COMM NON'' then ''Commercial - non-residential'' 
			when ''FARM'' then ''Farm'' when ''LAND'' then ''Land'' when ''MIXED-RES'' then ''Mixed use - residential'' when ''MOBILE'' 
			then ''Manufactured/Mobile home'' else '''' end  as PROPTYPE,
			lAmortType.DisplayString as AMORTTYPE,Lookups.DisplayString as LOANTYPE,loanapp.ChannelType as BUSTYPE1,
			loanData.DocumentationType as DOCTYPE,
			'''' as DocsSentDate,'''' as FundedDate,'''' as UW_SUBMIT_DATE ,'''' as UW_RECVD_DATE,'''' as SUSP_DATE, '''' as DENIED_DATE ,
			'''' as APPR_DATE,'''' as DOC_FUNDED_DATE ,LoanData.ShortProgramName as PROG_CODE,MIData.IMPOUNDS as IMPOUNDS,
			uSummary.FHAApprovalType as ApprovalType,'''' as INTERESTONLY,'''' as CONFORMING,loanData.PrepayPenalty as PREPAYPEN, HelocData.LienPosition as Lien , 
			LockRecord.Buydown,OccupencyLookup.DisplayString as OCCUPANCY 
			from mwlLoanApp  loanapp
			inner join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id  
			Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and borr.sequencenum=1   
			Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
			Left join dbo.mwlCreditScore as BorrCredit on BorrCredit.borrower_id = borr.id 
			left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
			Left join dbo.mwlInstitution as Institute on Institute.ObjOwner_id = loanapp.id and Institute.InstitutionType = ''Title''
			Left join dbo.mwlRealEstate as RealEstate on RealEstate.LoanApp_id = loanapp.id  
			left join dbo.mwaMWUser as LoRep on LoRep.ID = loanapp.Originator_ID  
			Left join dbo.mwlInstitution as Appraiser on Appraiser.ObjOwner_id = loanapp.id and Appraiser.InstitutionType = ''APPRAISER''
			and Appraiser.objownerName=''Contacts'' 
			Left join dbo.mwlAppDocument as AppDocument on AppDocument.loanapp_id = loanapp.id and AppDocument.InstitutionType = ''BROKER'' 
			Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' 
			and Broker.objownerName=''Broker'' 
			Left join dbo.mwlInstitution as UnderWriter on UnderWriter.ObjOwner_id = loanapp.id and UnderWriter.InstitutionType = ''UNDERWR''
			and UnderWriter.objownerName=''Contacts'' 
			Left join dbo.mwlInstitution as Offices on Offices.ObjOwner_id = loanapp.id and Offices.InstitutionType = ''BRANCH''
			and Offices.objownerName=''BranchInstitution''  
			left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK'' and 
			LockRecord.Status<>''CANCELED'' 
			Left outer join mwlLookups Lookups on LoanData.financingtype=Lookups.BOCode and Lookups.objectName=''mwlLoanData'' 
			and Lookups.FieldName=''FinancingType''
			Left outer join mwlLookups OccupencyLookup on LoanData.Occupancy=OccupencyLookup.BOCode and OccupencyLookup.objectName=''mwlLoanData''
			and OccupencyLookup.FieldName=''Occupancy''
			left join dbo.mwlMIData as MIData on MIData.LoanData_ID = loanData.id 
			left join dbo.mwlHELOCData as HelocData on HelocData.LoanApp_id = loanapp.id 
			left join dbo.mwaPhoneNumber as PhoneNo on PhoneNo.objOwner_id = LoRep.id 
			left join dbo.mwlSubjectProperty as PropertyAddress on PropertyAddress.LoanApp_id = loanapp.id   
			left join mwllookups lAmortType on loanData.AmortType=lAmortType.BoCode and lAmortType.objectName=''mwlloandata''
			and lAmortType.fieldname=''AmortType''
			left outer join mwlLookups as ml on ml.bocode=ChannelType and ml.objectName=''mwlLoanApp'' 
			left join dbo.mwamwuser as uEmail on uSummary.UnderWriter_id = uEmail.id  
			Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' 
			and Corres.objownerName=''Correspondent''  WHERE LoanNumber =''' + @sLoanID + ''''
                
                  
Print @SQL
exec sp_executesql @SQL          
END 


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPreApprovedE3Report]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPreApprovedE3Report]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetPreApprovedE3Report] (@RoleID int,
@strID varchar(max),
@Status varchar(max),
@oPuid varchar(max),
@oYear varchar(100),
@oFromDate varchar(20),
@oToDate varchar(20),
@Branch varchar(max))
AS
BEGIN
  SET NOCOUNT ON;
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

  DECLARE @SQL nvarchar(max)
  -- For BRD-125 Reports (adding branch and region column)
  DECLARE @ForRegion bit

  IF (@RoleID = 0
    OR @RoleID = 11
    OR @RoleID = 1
    OR @RoleID = 7
    OR @RoleID = 19
    OR @RoleID = 3
    OR @RoleID = 10
    OR @RoleID = 2)
  BEGIN
    SET @ForRegion = 1
  END
  ELSE
  BEGIN
    SET @ForRegion = 0
  END
  SET @SQL = ''
  SET @SQL = 'select ''E3'' as DB, loanapp.ChannelType as BusType,loanapp.ID as record_id,CONVERT(DECIMAL(10,3),
		(loanData.NoteRate * 100)) as int_rate,LoanNumber as loan_no,borr.firstname as BorrowerFirstName ,
		borr.lastname as BorrowerLastName,loanapp.Originatorname,isnull(isnull(convert(decimal(10,2), 
		loanData.LTV * 100),0),0) LTV,LoanApp.DecisionStatus,LoanApp.CurrentStatus as LoanStatus,
		convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,
		LoanData.ShortProgramName as LoanProgram,
		case when loanapp.LoanPurpose1003 is null then '''' when loanapp.LoanPurpose1003 = ''PU'' then ''Purchase'' 
		when loanapp.LoanPurpose1003 = ''RE'' then 
		''Refinance'' end as Purpose,
        CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed,case loanData.UseCashInOut 
        when ''0'' then ''No'' when ''1'' then ''Yes'' end as CashOut,isnull(loanData.AdjustedNoteAmt,0) as loan_amt,
        convert(varchar(35),EstCloseDate,101) AS ScheduleDate,
        CurrPipeStatusDate,UnderwriterName as UnderWriter,CloserName as Closer,
        CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as  BrokerName, institute.office as ''Branch Office'' '
  IF (@ForRegion = 1) -- For BRD-125 Reports (adding branch and region column)
  BEGIN
    SET @SQL = @SQL + ', region.regionname AS region '
  END

  SET @SQL = @SQL + '           
			from mwlLoanApp  loanapp  
			INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
			INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
			Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
            Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
            left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
            Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and 
            Broker.InstitutionType = ''BRANCH'' and Broker.objownerName=''BranchInstitution''
            left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id 
            and  LockRecord.LockType=''LOCK'' and LockRecord.Status<>''CANCELED''   
            Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = 
			''CORRESPOND'' and Corres.objownerName=''Correspondent'' '
  IF (@ForRegion = 1) -- For BRD-125 Reports (adding branch and region column)
  BEGIN
    SET @SQL = @SQL + '  left join tblbranchoffices AS bo ON bo.branch=institute.Office left join tblregion AS region ON bo.region = region.regionvalue  '
  END
  SET @SQL = @SQL + ' 
            where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''
            and loanapp.ChannelType like ''%RETAIL%''
            and CurrentStatus  IN (select * from dbo.SplitString(''' + @Status + ''','','')) '

  IF (@ForRegion = 1) -- For BRD-125 Reports (adding branch and region column)
  BEGIN
    SET @SQL = @SQL + ' and (Originator_id in 
                        (select * from dbo.SplitString(''' + @strID + ''','','')))'
    IF (@Branch IS NOT NULL
      AND @Branch <> '')
    BEGIN
      SET @SQL = @SQL + ' and (institute.Office in(select * from dbo.SplitString(''' + @Branch + ''','','')))'
    END
  END
  ELSE
  BEGIN
    IF (@RoleID != 0
      AND @RoleID != 1)
    BEGIN
      IF (LEN(@strID) = 0)
      BEGIN
        SET @SQL = @SQL + '  and (Originator_id in 
					   (select * from dbo.SplitString(''' + @strID + ''','','')))'

        IF (@oPuid != '')
        BEGIN
          SET @SQL = @SQL + '  and (Originator_id in 
						 (select * from dbo.SplitString	 (''' + @oPuid + ''','','')))'
        END

      END
      ELSE
      BEGIN


        SET @SQL = @SQL + '  and (Originator_id in 
                        (select * from dbo.SplitString(''' + @strID + ''','','')))'

        IF (@oPuid != '')

          SET @SQL = @SQL + '   and (Originator_id in 
                           (select * from dbo.SplitString(''' + @oPuid + ''','','')))'


      END
    END
  END

  IF (@oYear != '')
    SET @SQL = @SQL + ' and YEAR(CurrPipeStatusDate) = ''' + @oYear + ''''

  IF (@oFromDate != ''
    AND @oToDate != '')
    SET @SQL = @SQL + ' and CurrPipeStatusDate between ''' + @oFromDate + '''  and  ''' + CONVERT(varchar(20), DATEADD(dd, 1, @oToDate), 101) + ''''
  ELSE
  IF (@oFromDate != '')
    SET @SQL = @SQL + ' and CurrPipeStatusDate > ''' + @oFromDate + ''''
  ELSE
  IF (@oToDate != '')
    SET @SQL = @SQL + ' and CurrPipeStatusDate < ''' + @oToDate + ''''

  PRINT @SQL
  EXEC sp_executesql @SQL
END
/*===========================Credit Pulled============================= */




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPreApprovedE3ReportWithRegion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPreApprovedE3ReportWithRegion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[sp_GetPreApprovedE3ReportWithRegion]
(                
@strID varchar(max),                 
@Status varchar(max),             
@oPuid varchar(max),
@oYear varchar(max),
@oFromDate VARCHAR(20),
@oToDate VARCHAR(20)
)                
AS                
BEGIN                


Declare @SQL nvarchar(max)                
Set @SQL =''                
Set @SQL ='select ''E3'' as DB, loanapp.ChannelType as BusType,loanapp.ID as record_id,CONVERT(DECIMAL(10,3),
		(loanData.NoteRate * 100)) as int_rate,LoanNumber as loan_no,borr.firstname as BorrowerFirstName ,
		borr.lastname as BorrowerLastName,loanapp.Originatorname,isnull(isnull(convert(decimal(10,2), loanData.LTV * 100),0),0)LTV,
		LoanApp.DecisionStatus,LoanApp.CurrentStatus as LoanStatus,convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,
		DateAppSigned as DateApproved,LoanData.ShortProgramName as LoanProgram,
        case when loanapp.LoanPurpose1003 is null then '''' when loanapp.LoanPurpose1003 = ''PU'' then ''Purchase'' when 
        loanapp.LoanPurpose1003 = ''RE'' then ''Refinance'' 
        end as Purpose,CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed,case loanData.UseCashInOut when ''0'' 
        then ''No'' when ''1'' then ''Yes'' end as CashOut,isnull(loanData.AdjustedNoteAmt,0) as loan_amt,
        convert(varchar(35),EstCloseDate,101) AS ScheduleDate,CurrPipeStatusDate,UnderwriterName as UnderWriter,CloserName as Closer,
        CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as  BrokerName,
        Broker.Office as ''Branch Office'' 
		from mwlLoanApp  loanapp  
		Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
        Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
        left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
        Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BRANCH'' 
        and Broker.objownerName=''BranchInstitution''
        left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' and 
        LockRecord.Status<>''CANCELED'' Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and 
        Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' 
        where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''
        and loanapp.ChannelType like ''%RETAIL%''
            
        and CurrentStatus  IN (select * from dbo.SplitString('''+ @Status +''','','')) ' 
            
        if (len(@oPuid) = 0)      
        Begin
           Set @SQL = @SQL + '   and (Originator_id in (select * from dbo.SplitString('''+  @oPuid  +''','',''))  
								     or loanapp.id in (select * from dbo.SplitString('''+  @strID  +''','',''))
								     or Broker.office in  (select * from dbo.SplitString('''+  @oPuid  +''','','')))'      
		End
		else 
		begin
		   Set @SQL = @SQL + '   and (Originator_id in (select * from dbo.SplitString('''+  @oPuid  +''','',''))  
								     or loanapp.id in (select * from dbo.SplitString('''+  @strID  +''','',''))
								     or Broker.office in  (select * from dbo.SplitString('''+  @oPuid  +''','','')))'      	
		End
		
		if(@oYear != '')
			Set @SQL=@SQL+ ' and YEAR(CurrPipeStatusDate) = ''' + @oYear +''''
			
		if(@oFromDate != '' and @oToDate !='')
			Set @SQL=@SQL+ ' and CurrPipeStatusDate between '''+@oFromDate+'''  and '''+Convert(varchar(20),DATEADD (dd , 1 , @oToDate),101)+''''
		else if(@oFromDate != '')
			Set @SQL=@SQL+ ' and CurrPipeStatusDate > '''+@oFromDate+''''	
		else if(@oToDate != '')
			Set @SQL=@SQL+ ' and CurrPipeStatusDate < '''+@oToDate+''''    
				
Print @SQL      
exec sp_executesql @SQL                
END

/*------------*/

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetProductType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetProductType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductType]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT ID, producttypeid, name, term, isarm FROM  tblProductType WHERE id NOT IN (4)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetProductType_clsloan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetProductType_clsloan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetProductType_clsloan]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ID, producttypeid, name, term, isarm FROM  tblProductType where id not in(3)

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetProLoginData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetProLoginData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsUserRegistrationE3.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetProLoginData]
(
@Login varchar (100)
)
AS
BEGIN
	 select * from offices where Login=@Login
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPropertyType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPropertyType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetPropertyType]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT ID,PropertyTypeID,Name FROM tblPropertyType where id not in(4,5)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetProUserData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetProUserData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsUserRegistrationE3.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetProUserData]
(
@login varchar (100)
)
AS
BEGIN
	 select * from users where login=@login
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetQuery]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetQuery]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetQuery]
(
@MethodName VARCHAR(MAX)

)
AS
BEGIN
SELECT Query FROM tblDbQuery WHERE MethodName=MethodName AND DB='P'

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetRateByRateID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetRateByRateID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetRateByRateID]
(
@rateid varchar (max)
)
AS
BEGIN

select * from tblrates where rateid = @rateid
 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetRateDateTime]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetRateDateTime]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetRateDateTime]
AS
BEGIN

select top 1 modifiedDate from tblrates order by modifiedDate desc
 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetRateParam]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetRateParam]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetRateParam]
(
@Loanprogram VARCHAR (MAX)
)
AS
BEGIN

select * from tblNewrates where Loanprogram=@Loanprogram
 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetRates]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetRates]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetRates]
AS
BEGIN

select * from tblrates
 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetRatesForDefaultPage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetRatesForDefaultPage]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetRatesForDefaultPage]

AS
BEGIN

select top 4 * from tblrates
 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetRegionValue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetRegionValue]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GetRegionValue]   
(  
@UserID int  
   
)  
AS  
BEGIN  
   
Declare @SQL varchar(1000)  
Declare @SQL1 varchar(1000)  
Set @SQL = ''  
   
                
                
set @SQL=  (select  branchoffice from tblusers where urole=19 and userid=@UserID)  

select [office]=items from dbo.SplitString(@SQL,',')   

            
  
--Print @SQL1   
--exec sp_executesql @SQL1  
END  
  
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetReport]              
(                
@RoleID int,               
@strID varchar(2000),                 
@Status varchar(max),             
@StatusDesc varchar(100),         
@oPuid varchar(100)         
)                
AS                
BEGIN                
                
Declare @SQL nvarchar(max)                
Set @SQL =''                
Set @SQL ='select ''E3'' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue as ProcessorName,      
   loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,      
   CloserName as Closer,borr.lastname as BorrowerLastName,borr.firstname as BorrowerFirstName,loanapp.CurrentStatus as LoanStatus,      
   '''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate,      
   approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase''      
   when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,      
   case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA''      
   End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,      
   LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'''')      
   WHEN ''''      
   THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,      
   CAST(borr.CompositeCreditScore AS CHAR(5))      
   as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate ,       
   CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and       
   Category !=''LENDER''       
   and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0      
   WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'')        
   and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'')      
   and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE       
   (SELECT count(a.ID) as TotalCleared       
   FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'') and (CurrentState=''CLEARED'' OR       
   CurrentState=''SUBMITTED'') and a.objOwner_ID IN       
   (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions FROM      
   dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1        
   WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER       
   from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  Inner join dbo.mwlloandata as loanData      
   on loanData.ObjOwner_id = loanapp.id        
   left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  Left join dbo.mwlInstitution as Broker      
   on Broker.ObjOwner_id = loanapp.id       
   and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker'' left join dbo.mwlLockRecord as LockRecord on       
   LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED''       
   Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc=''' + @StatusDesc  + '''      
   Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved''      
   Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''        
   Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and       
   Corres.objownerName=''Correspondent''       
   left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID and      
   CustFieldDef_id in(select Id from mwsCustFieldDef where CustomFieldName=''Retail Processor'')      
   where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and   loanapp.ChannelType like ''%RETAIL%'' and CurrentStatus  IN (select * from dbo.SplitString('''+ @Status +''','',''))'            
         
      
      
      
 if (@RoleID != 0 and @RoleID != 1)      
            BEGIN      
                if (len(@strID)= 0)      
                begin      
                    if (@RoleID = 2)      
                           
                        Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+  @strID  +''','','')))'      
                           
                        
                    else if (@RoleID = 3)      
                           
                        Set @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString('''+  @strID  +''','','')) or Originator_id in (select * from dbo.SplitString('''+  @oPuid  +''','','')))'      
                           
                    else if (@RoleID =  10 or @RoleID = 7)      
                           
                        Set @SQL = @SQL + '  and (Originator_id in (select * from dbo.SplitString('''+  @strID  +''','','')) or loanapp.id in (select * from dbo.SplitString('''+  @strID  +''','','')))'      
                           
                    else      
      begin      
       Set @SQL = @SQL + '  and (Originator_id in (select * from dbo.SplitString('''+  @strID  +''','','')))'      
      
       if (@oPuid  != '')      
        begin      
         Set @SQL = @SQL + '  and (Originator_id in (select * from dbo.SplitString('''+  @oPuid  +''','','')))'       
        end      
      end      
                end      
                else      
                 begin      
                    if (@RoleID = 2)      
                           
                         Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+  @strID  +''','','')))'      
                           
       
                    else if (@RoleID = 3)      
                           
                        Set @SQL = @SQL + '  and (loanapp.ID in (select * from dbo.SplitString('''+  @strID  +''','',''))or Originator_id in (select * from dbo.SplitString('''+  @oPuid  +''','','')))'      
                           
                    else if (@RoleID = 10 or @RoleID = 7)      
                            
                        Set @SQL = @SQL + '  and (Originator_id in (select * from dbo.SplitString('''+  @strID  +''','','')) or loanapp.id in (select * from dbo.SplitString('''+  @strID  +''','','')))'      
                           
                    else      
                    begin      
                        Set @SQL = @SQL + '  and (Originator_id in (select * from dbo.SplitString('''+  @strID  +''','','')))'      
      
                        if (@oPuid != '')      
                               
                           Set @SQL = @SQL + '   and (Originator_id in (select * from dbo.SplitString('''+  @oPuid  +''','','')))'      
      
                      end         
                   end      
end      
              Set @SQL = @SQL + '  order by Per desc '      
        
  Print @SQL      
exec sp_executesql @SQL                
END 
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetReport4RetailType_office]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetReport4RetailType_office]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
        
CREATE PROCEDURE [dbo].[SP_GetReport4RetailType_office]         
(            
@RoleID int,           
@office varchar(2000),            
@strID varchar(2000),   
@CurrentStatus varchar(3000),         
@StatusDesc varchar(100),     
@oPuid varchar(100)     
)            
AS            
BEGIN            
            
Declare @SQL nvarchar(max)            
Set @SQL =''            
Set @SQL ='select  ''E3'' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue as ProcessorName,  
   loanapp.currentStatus as LoanStatus, loanapp.CurrPipeStatusDate, CurrDecStatusDate as CurrDecisionstatus,loanapp.DecisionStatus ,  
   loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.firstname as BorrowerFirstName,  
   borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate,  
   approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase''  
   when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,  
   case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as   
   LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,  
   LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'''')  
   WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,  
   CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate ,   
   CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER''  
   and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0  
   WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'')   
   and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'')  
   and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   
   ELSE (SELECT count(a.ID) as TotalCleared   
   FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'') and (CurrentState=''CLEARED'' OR   
   CurrentState=''SUBMITTED'') and a.objOwner_ID IN   
   (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 /   
   (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'') and   
   a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER   
   from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  Inner join   
   dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id    
   left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  Left join dbo.mwlInstitution as Broker  
   on Broker.ObjOwner_id = loanapp.id   
   and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker'' left join dbo.mwlLockRecord as LockRecord on   
   LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED''   
   Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc='''  + @StatusDesc +'''  
   Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved''  
   Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''    
   Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''Branch'' and   
   Corres.objownerName=''BranchInstitution''   
   left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID and CustFieldDef_id  
   in(select Id from mwsCustFieldDef where CustomFieldName=''Retail Processor'')  
   Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id and Branch.InstitutionType = ''Branch''  
   and Branch.objownerName=''BranchInstitution'' where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and '  
  
                if (@RoleID = 16)  
                begin  
                    if (@office != '')  
                       
                          Set @SQL = @SQL + '  Corres.office in ('''+ @office +''') and'   
                       
                end  
                if (@RoleID = 11)  
                begin  
                    if (@oPuid != '')  
                       
                            
                          Set @SQL = @SQL + '  (Originator_id in (select * from dbo.SplitString('''+ @oPuid +''','','')) or loanapp.id in (select * from dbo.SplitString('''+ @strID +''','',''))  or  Branch.office in(select * from dbo.SplitString('''+ @oPuid +''','',''))) and '  
                       
                end  
                   
  
                 Set @SQL = @SQL + ' loanapp.ChannelType like ''%RETAIL%'' and CurrentStatus  IN (select * from dbo.SplitString('''+@CurrentStatus+''','',''))  order by Per desc'  
  
   
Print @SQL  
exec sp_executesql @SQL            
END   


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetReportClosing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetReportClosing]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE DBO.sp_GetReportClosing         
(            
@RoleID int,           
@strID varchar(2000),             
@CurrentStatus varchar(3000),   
@oPuid varchar(100)     
)            
AS            
BEGIN            
            
Declare @SQL nvarchar(max)            
Set @SQL =''            
Set @SQL =' select ''E3'' as DB,loanapp.ChannelType,loanapp.OriginatorName,loanapp.DecisionStatus, loanapp.ID as record_id,LoanNumber as loan_no,  
   borr.lastname as BorrowerLastName,borr.firstname as BorrowerFirstName,loanapp.DecisionStatus,loanapp.CurrentStatus as LoanStatus,  
   DateAppSigned as SubmittedDate,LoanProgramName as LoanProgram,  
   case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,  
   borr.CompositeCreditScore as CreditScoreUsed,loanData.AdjustedNoteAmt as LoanAmount,  
   convert(varchar(35),EstCloseDate,101) AS ScheduleDate,LockStatus,LockExpirationDate,UnderwriterName as UnderWriter,  
   CloserName as Closer,Institute1.Company as BrokerName, Institute.CustomDataOne as TitleCompany,  
   AppStatus.StatusDateTime as DocsSentDate,FundedStatus.StatusDateTime as FundedDate,'''' as LU_FINAL_HUD_STATUS   
   from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id   
   Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id   
   left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id     
   Left join dbo.mwlInstitution as Institute on Institute.ObjOwner_id = loanapp.id and Institute.InstitutionType = ''Branch'' and   
   Institute.objownerName=''BranchInstitution''   
   Left join dbo.mwlInstitution as Institute1 on Institute1.ObjOwner_id = loanapp.id and Institute1.InstitutionType = ''Title''   
   and Institute1.objownerName=''Contacts''   
   Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and AppStatus.StatusDesc like ''%Docs Out%''    
   Left join mwlappstatus as FundedStatus on FundedStatus.LoanApp_id=loanapp.id  and FundedStatus.StatusDesc like ''%Funded''   
   where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and   
   CurrentStatus  IN (select * from dbo.SplitString('''+ @CurrentStatus +''','',''))  and loanapp.ChannelType like ''%RETAIL%'''    
               
                if (@RoleID != 0 and @RoleID != 1)  
                begin  
                    if (len(@strID) = 0)  
                    begin  
                        
                        if (@RoleID = 2)  
                        begin  
                            Set @SQL = @SQL + ' and (Originator_id in (''' + @strID +'''))'  
                        end  
                        else if (@RoleID = 3)  
                        begin  
                              
                            Set @SQL = @SQL + ' and (loanapp.ID in (''' + @strID +''') or Originator_id in (''' + @oPuid + '''))'  
                        end  
                        else if (@RoleID = 10)  
                        begin  
  
  
                            Set @SQL = @SQL + ' and (Originator_id in (''' + @strID +''') or loanapp.id in (''' + @strID +'''))'  
                        end  
                          
                        else  
                        begin  
                            Set @SQL = @SQL + ' and (Originator_id in (''' + @strID +'''))'  
  
  
                        end  
                    end  
                    else  
                    begin  
                          
                        if (@RoleID = 2 )  
                        begin  
                            Set @SQL = @SQL + ' and (Originator_id in (''' + @strID +'''))'  
                        end  
                        
                        else if (@RoleID = 3)  
                        begin  
                            Set @SQL = @SQL + ' and (loanapp.ID in (''' + @strID +''') or Originator_id in (''' + @oPuid + '''))'  
                        end  
                        else if (@RoleID = 10)  
          begin  
  
  
                            Set @SQL = @SQL + ' and (Originator_id in (''' + @strID +''') or loanapp.id in (''' + @strID +'''))'  
                        end  
                          
                        else  
                        begin  
                            Set @SQL = @SQL + ' and (Originator_id in (''' + @strID +'''))'  
  
                        end  
                    end  
                end  
                      
                       
                  
                    
Print @SQL  
exec sp_executesql @SQL            
END   
  
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetReportClosingWithRegion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetReportClosingWithRegion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
        
CREATE PROCEDURE DBO.sp_GetReportClosingWithRegion         
(            
       
@strID varchar(2000),             
@CurrentStatus varchar(3000),   
@oPuid varchar(100)     
)            
AS            
BEGIN            
            
Declare @SQL nvarchar(max)            
Set @SQL =''            
Set @SQL ='select ''E3'' as DB,loanapp.ChannelType,loanapp.OriginatorName,loanapp.DecisionStatus, loanapp.ID as record_id,LoanNumber as loan_no,  
   borr.lastname as BorrowerLastName,borr.firstname as BorrowerFirstName,loanapp.DecisionStatus,loanapp.CurrentStatus as LoanStatus,  
   DateAppSigned as SubmittedDate,LoanProgramName as LoanProgram,  
   case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as   
   TransTYpe,borr.CompositeCreditScore as CreditScoreUsed,loanData.AdjustedNoteAmt as LoanAmount,  
   convert(varchar(35),EstCloseDate,101) AS ScheduleDate,LockStatus,LockExpirationDate,UnderwriterName as UnderWriter,  
   CloserName as Closer,Institute1.Company as BrokerName, Institute.CustomDataOne as TitleCompany,  
   AppStatus.StatusDateTime as DocsSentDate,FundedStatus.StatusDateTime as FundedDate,'''' as LU_FINAL_HUD_STATUS   
   from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id    
   Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id   
   left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id    
   Left join dbo.mwlInstitution as Institute on Institute.ObjOwner_id = loanapp.id and  Institute.InstitutionType = ''Branch''  
   and Institute.objownerName=''BranchInstitution''   
   Left join dbo.mwlInstitution as Institute1 on Institute1.ObjOwner_id = loanapp.id and Institute1.InstitutionType = ''Title''  
   and Institute1.objownerName=''Contacts''   
   Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and AppStatus.StatusDesc like ''%Docs Out%''   
   Left join mwlappstatus as FundedStatus on FundedStatus.LoanApp_id=loanapp.id  and FundedStatus.StatusDesc like ''%Funded''   
   where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and   
   CurrentStatus  IN (''' + @CurrentStatus +''')'  
  
                if (len(@oPuid) =  0)  
                begin  
                      
                     Set @SQL = @SQL + ' and (Originator_id in (''' + @oPuid + ''') or loanapp.id in (''' + @strID +''') or Institute.office in(''' + @oPuid + ''')) '  
  
                end  
  
                else  
                begin  
                      
                    Set @SQL = @SQL + '  and (Originator_id in (''' + @oPuid + ''') or loanapp.id in (''' + @strID +''') or Institute.office in(''' + @oPuid + '''))'  
                end  
                 Set @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL'''                         
                  
                    
Print @SQL  
exec sp_executesql @SQL            
END   
  
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetReportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetReportData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

      
CREATE PROCEDURE [dbo].[sp_GetReportData]       
(          
@RoleID int,         
@strID varchar(2000),           
@CurrentStatus varchar(3000),       
@StatusDesc varchar(100),   
@oPuid varchar(100)   
)          
AS          
BEGIN          
          
Declare @SQL nvarchar(max)          
Set @SQL =''          
Set @SQL ='select ''E3'' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue as ProcessorName,
			loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus,loanapp.ID as record_id,'' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as
			Closer,borr.lastname as BorrowerLastName,borr.firstname as BorrowerFirstName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,
			appStat.StatusDateTime AS SubmittedDate,
			approvalStat.StatusDateTime as DateApproved,case when TransType is null then '' when TransTYpe = ''P'' then ''Purchase''
			when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,
			case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,
			loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,
			LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'''')
			WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,
			CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate , 
			CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER''
			and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0
			WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'')
			and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'')
			and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0  
			ELSE (SELECT count(a.ID) as TotalCleared 
			FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'') and (CurrentState=''CLEARED''
			OR CurrentState=''SUBMITTED'') and a.objOwner_ID IN 
			(SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions
			FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1
			WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER 
			from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  Inner join dbo.mwlloandata as loanData on
			loanData.ObjOwner_id = loanapp.id  
			left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  Left join dbo.mwlInstitution as Broker on
			Broker.ObjOwner_id = loanapp.id 
			and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker'' left join dbo.mwlLockRecord as LockRecord on
			LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED'' 
			Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc='''+ @StatusDesc +'''
			Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved'' 
			Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''  
			Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND''
			and Corres.objownerName=''Correspondent'' 
			left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID and
			CustFieldDef_id in(select Id from mwsCustFieldDef where CustomFieldName=''Retail Processor'')
			where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and 
			loanapp.ChannelType like ''%RETAIL%'' and  CurrentStatus  IN (''' + @CurrentStatus +''')' 
                if (@RoleID != 0 AND @RoleID != 1)
                BEGIN
                    if (len(@strID) = 0)
                     begin
                        if (@RoleID =  2)
                        begin
                            Set @SQL = @SQL + 'and (Originator_id in ('''' + @strID + ''''))'
                        end
                         
                        else if (@RoleID = 3)
                        begin
                            Set @SQL = @SQL + ' and (loanapp.ID in ('''' + @strID + '''') or Originator_id in ('''' + @oPuid +''''))'
                        end
                        else if (@RoleID = 10 or @RoleID = 7)
                        begin
                            Set @SQL = @SQL + ' and (Originator_id in ('''' + @strID + '''') or loanapp.id in ('''' + @strID + ''''))'
                        end
                        else
                        begin
                            Set @SQL = @SQL + ' and (Originator_id in ('''' + @strID + ''''))'

                            if (@oPuid != '')
                            begin
                              Set @SQL = @SQL + '   and (Originator_id in ('''' + @oPuid +''''))'
                            end
                        end
                    end
                    else
                    begin
                        if (@RoleID = 2)
                        begin
                            Set @SQL = @SQL + ' and (Originator_id in (''' + @strID + '''))'
                        end
                         
                        else if (@RoleID = 3)
							begin
							   Set @SQL = @SQL + '  and (loanapp.ID in (''' + @strID + ''')or Originator_id in ('''' + @oPuid +''''))'
							end
                        else if (@RoleID = 10 or @RoleID = 7)
							begin
								Set @SQL = @SQL + ' and (Originator_id in (''' + @strID + ''') or loanapp.id in (''' + @strID + '''))'
							end
                        else
                        begin
                           Set @SQL = @SQL + ' and (Originator_id in (''' + @strID + '''))'

                            if (@oPuid != '')
                             
                                Set @SQL = @SQL + 'and (Originator_id in (''' + @oPuid +'''))'
                             
                        end
                    end
                end
               Set @SQL = @SQL + '  order by Per desc' 
Print @SQL
exec sp_executesql @SQL          
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetRetailFinalPriceReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetRetailFinalPriceReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SP_GetRetailFinalPriceReport]           
(            
@RoleID int,           
@strID varchar(max),    
@FromDate  varchar(20),  
@ToDate varchar(20),  
@oPuid varchar(max),
@type varchar(10)='DLA' ,
@isFliter bit=0,
@Branch VARCHAR(MAX) 
)            
AS            
BEGIN            
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;           
Declare @SQL nvarchar(max)            
Set @SQL =''            
Set @SQL ='select ''E3'' as DB,loanapp.currpipestatusdate, loanapp.ID as record_id,loanapp.Channeltype as BusType,LoanNumber as loan_no,borr.firstname as BorrowerFirstName ,  
		   borr.lastname as BorrowerLastName,CurrentStatus as LoanStatus, CurrentStatus as LoanStatusDA, 
		   convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,  
		   LoanData.LoanProgramName as Prog_desc ,loanapp.Originatorname,  
		   case when loanapp.LoanPurpose1003 is null then '''' when loanapp.LoanPurpose1003 = ''PU'' then ''Purchase'' 
		   when loanapp.LoanPurpose1003 = ''RE'' then ''Refinance''   
		   end as Purpose,CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed,(Loandata.NoteRate * 100) as Int_rate,  
		   loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,  
		   convert(varchar(40),LockRecord.LockExpirationDate,101) as LockExpires,LockRecord.LockDateTime,  
		   convert(varchar(35),LockRecord.LockDateTime,101) as Lockdate,UnderwriterName as UnderWriter,CloserName as Closer,  
		   CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,
		   '''' as FundedDate,  
		   '''' as ReceivedInUW,LoanData.ShortProgramName as prog_code   ,
		   loanapp.Branch Branch1,
		   institute.office Branch,
		   case loanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' WHEN ''M'' 
		   THEN  ''USDA/Rural Housing Service'' End as LoanProgram ,
		   loanData.AdjustedNoteAmt as LoanAmount,
		   LockRecord.LockREason,
		   case Isnull(PricingResult.lenderbaserate,'''') When '''' then '''' Else 
		   convert(varchar,((PricingResult.lenderbaserate)*100)) + '' %'' end interestRate,
		   PricingResult.lenderClosingprice  FinalLockPrice'
---Start --Added By Tavant Team for BRD-125-----------------------------
IF(@isFliter =1)
	BEGIN
		SET @SQL =@SQL + ', region1.regionname as Region'
	END
SET  @SQL = @SQL + ' from mwlLoanApp  loanapp
					 INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
			         INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
                    Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id                    
					left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id    
					Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and 
					Broker.InstitutionType = ''BRANCH'' and Broker.objownerName=''BranchInstitution''  
					left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id  and 
					LockRecord.Status<>''CANCELED'' and isnull(LockRecord.LockReason,'''')<>''''  
					Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''  
					Left join mwlPricingResult  as PricingResult on  PricingResult.ObjOwner_id = LockRecord.id '
IF(@isFliter =1)
	BEGIN
		SET @SQL = @SQL + '  LEFT JOIN tblbranchoffices AS bo ON  bo.branch=institute.office 	
							 LEFT JOIN tblregion AS region1   ON bo.region = region1.regionvalue  '
	END	
SET  @SQL = @SQL + ' where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' and loanapp.ChannelType like ''%RETAIL%'''  					  
IF (@isFliter = 1)
BEGIN
     SET @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) '
	  if(@Branch is not null and @Branch <>'')
	 begin
		SET @SQL = @SQL + ' and (institute.Office in(select * from dbo.SplitString('''+  @Branch  +''','','')))' 
	 end
END		  
ELSE
BEGIN
   IF (@RoleID != 0
      AND @RoleID != 1
      AND @RoleID != 11)
   BEGIN
      IF (LEN(@strID) = 0)
      BEGIN
         IF (@RoleID = 2)
         BEGIN
            --Set @SQL = @SQL + 'and (Originator_id in ('''+ @strID +'''))'  
            SET @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) '
         END
         ELSE
         IF (@RoleID = 3)
         BEGIN
            --Set @SQL = @SQL + ' and (loanapp.ID in ('''+ @strID +''') or Originator_id in ('''+ @strID +''' ) )'  
            SET @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString(''' + @strID + ''','','')) 
                            or Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')))'
         END
         ELSE
         IF (@RoleID = 10
            OR @RoleID = 7)
         BEGIN
            --Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID +''') or loanapp.id in ('''+ @strID +'''))'  
            SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @oPuid + ''','','')) 
                            or loanapp.id in (select * from dbo.SplitString(''' + @strID + ''','','')))'
         END
      END
      ELSE
      BEGIN
         IF (@RoleID = 2)
         BEGIN
            --Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID +'''))'  
            SET @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) '
         END
         ELSE
         IF (@RoleID = 3)
         BEGIN
            -- Set @SQL = @SQL + ' and (loanapp.ID in ('''+ @strID +''') or Originator_id in (''' + @oPuid +''') )'  
            SET @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString(''' + @strID + ''','','')) 
                                or Originator_id in (select * from dbo.SplitString(''' + @oPuid + ''','','')))'
         END
         ELSE
         IF (@RoleID = 10
            OR @RoleID = 7)
         BEGIN
            --Set @SQL = @SQL + ' and (Originator_id in ('''+ @strID +''') or loanapp.id in ('''+ @strID +'''))'  
            SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @oPuid + ''','','')) 
                                or loanapp.id in (select * from dbo.SplitString(''' + @strID + ''','','')))'
         END
      END
   END
END
--Set @SQL = @SQL + ' and  loanapp.currpipestatusdate between convert(datetime,''' + @FromDate + ''',101) and convert(datetime,''' + @ToDate + ''',101)'  
if(@FromDate != '' and @ToDate !='')
Set @SQL=@SQL+ ' and loanapp.currpipestatusdate between '''+@FromDate+'''  and  '''+Convert(varchar(20),DATEADD (dd , 1 , @ToDate),101)+''''
else if(@FromDate != '')
Set @SQL=@SQL+ ' and loanapp.currpipestatusdate > '''+@FromDate+''''	
else if(@ToDate != '')
Set @SQL=@SQL+ ' and loanapp.currpipestatusdate < '''+@ToDate+''''  
              
Set @SQL = @SQL + ' and loanapp.CurrentStatus =''62 - Funded'''
           
                  

--Print @SQL            
exec sp_executesql @SQL            
END 
/*------------*/


--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetRetailFinalPriceReport_With_Region]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetRetailFinalPriceReport_With_Region]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SP_GetRetailFinalPriceReport_With_Region]           
(        
@strID varchar(max),    
@FromDate  varchar(20),  
@ToDate varchar(20),  
@oPuid varchar(2000)

)            
AS            
BEGIN            
            
Declare @SQL nvarchar(max)            
Set @SQL =''            
Set @SQL ='select ''E3'' as DB,loanapp.currpipestatusdate, loanapp.ID as record_id,loanapp.Channeltype as BusType,
		   LoanNumber as loan_no,borr.firstname as BorrowerFirstName , borr.lastname as BorrowerLastName,
		   CurrentStatus as LoanStatus,convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,
		   /*DateAppSigned as DateApproved,LoanData.LoanProgramName as Prog_desc ,CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) 
		   as	CreditScoreUsed,(Loandata.NoteRate * 100) as Int_rate,UnderwriterName as UnderWriter,CloserName as Closer,CASE ISNULL
			(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,'''' as FundedDate,''''			as ReceivedInUW,LoanData.ShortProgramName as prog_code ,loanapp.Branch Branch1,  
		   */
		   
		   loanapp.Originatorname,  
		   case when loanapp.LoanPurpose1003 is null then '''' when loanapp.LoanPurpose1003 = ''PU'' then ''Purchase'' 
		   when loanapp.LoanPurpose1003 = ''RE'' then ''Refinance'' end as Purpose,loanData.AdjustedNoteAmt as loan_amt,
		   LockRecord.Status as LockStatus,  
		   convert(varchar(40),LockRecord.LockExpirationDate,101) as LockExpires,LockRecord.LockDateTime,  
		   convert(varchar(35),LockRecord.LockDateTime,101) as Lockdate,
		   Broker.Office as Branch,
		   case loanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' WHEN ''M'' 
		   THEN  ''USDA/Rural Housing Service'' End as LoanProgram ,loanData.AdjustedNoteAmt as LoanAmount,
		   LockRecord.LockReason,case Isnull(PricingResult.lenderbaserate,'''') When '''' then '''' Else 
		   convert(varchar,((PricingResult.lenderbaserate)*100)) + '' %'' end interestRate,
		   PricingResult.lenderClosingprice  FinalLockPrice,isnull(tblusers.Region,'''') as Area 
		   from mwlLoanApp  loanapp     
		   Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id    
		   Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id                    
		   
		   Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BRANCH'' 
		   and Broker.objownerName=''BranchInstitution''  
		   left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id  and  LockRecord.Status<>''CANCELED'' 
		   and isnull(LockRecord.LockReason,'''')<>''''  
		   /*left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id    
		   Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.				objownerName=''Correspondent''  */
		   Left join mwlPricingResult  as PricingResult on  PricingResult.ObjOwner_id = LockRecord.id  
		   left join tblusers on tblusers.E3Userid= loanapp.Originator_ID
		   where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''   
		   and loanapp.ChannelType like ''%RETAIL%'''  
  
    Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+  @oPuid  +''','','')) 
						or loanapp.id in (select * from dbo.SplitString('''+  @strID  +''','','')) 
						or Broker.office in  (select * from dbo.SplitString('''+  @oPuid  +''','','')) )'

    if(@FromDate != '' and @ToDate !='')
			Set @SQL=@SQL+ ' and loanapp.currpipestatusdate between '''+@FromDate+'''  
					     	 and  '''+Convert(varchar(20),DATEADD (dd , 1 , @ToDate),101)+''''
	else if(@FromDate != '')
			Set @SQL=@SQL+ ' and loanapp.currpipestatusdate > '''+@FromDate+''''	
	else if(@ToDate != '')
			Set @SQL=@SQL+ ' and loanapp.currpipestatusdate < '''+@ToDate+''''
       
              
     Set @SQL = @SQL + ' and loanapp.CurrentStatus =''62 - Funded'''

Print @SQL            
exec sp_executesql @SQL            
END
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetRetailOperationReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetRetailOperationReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  Krunal Patel
-- Create date: 04 July 2013            
-- Description: Retail Operation Pipeline Management Task Grid Bind SP
-- =============================================

CREATE PROCEDURE [dbo].[SP_GetRetailOperationReport]              
(             
	@CurrentStatus varchar(1000),         
	@StatusDescr varchar(2000),
	@OriginatorID varchar(100)
)                
AS                
BEGIN                

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error	
	  
   DECLARE @CustomFieldID varchar(40)     
   SELECT @CustomFieldID = ID from mwscustfielddef where MWFieldNum = 9973 -- Custom Field Name "Approval Expiration Date", Custom Field Number "9973"
	  
	if(@OriginatorID  <> '')
	Begin
	
	Select 'E3' as DB,loanapp.EstCloseDate,
	(select top 1 DateValue  from mwlCustomField where custFieldDef_ID=@CustomFieldID
	and loanapp.ID=mwlCustomField.LoanApp_ID 
	order by datevalue desc) as ApprovalExpDate,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,
	loanapp.processorname as ProcessorName,loanapp.currentStatus as LoanStatus, loanapp.CurrPipeStatusDate, loanapp.DecisionStatus,
	loanapp.ID as	record_id,'' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.firstname as BorrowerFirstName,
	borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate,
	approvalStat.StatusDateTime as DateApproved,
	case when TransType is null then '' when TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe,
	LoanData.LoanProgDesc as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,
	LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'') WHEN '' THEN RTRIM	(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,CAST(borr.CompositeCreditScore AS CHAR(5)) as	CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate , 
	CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy='Prior to Closing' and Category !='LENDER' 
	and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0
	WHEN 
		(SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER')  
		and (CurrentState='CLEARED' OR CurrentState='SUBMITTED') and a.objOwner_ID IN 
			(SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   
				ELSE 
				(SELECT count(a.ID) as TotalCleared 
					FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER') and (CurrentState='CLEARED' 
						OR CurrentState='SUBMITTED') and a.objOwner_ID IN 
				(SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / 
				(SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy='Prior to Closing' and Category !='LENDER') 
					and a.objOwner_ID IN 
				(SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER 

	from mwlLoanApp  loanapp  
	Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
	Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active=1
	left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
	Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = 'BROKER' 
	and Broker.objownerName='Broker' 
	left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType='LOCK' 
	and  LockRecord.Status<>'CANCELED' 
	Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc=@StatusDescr
	Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved' 
	Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName='mwlLoanData' and fieldname='refipurpose'  
	Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = 'CORRESPOND' 
	and Corres.objownerName='Correspondent' 
	where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''   

	AND loanapp.ChannelType like '%RETAIL%'   AND CurrentStatus  IN (select items from dbo.SplitString(@CurrentStatus,','))  
	--AND Processor.stringvalue like '%'+@OriginatorID+'%' and Processor.stringvalue <> '' 
	AND  loanapp.processor_id in (select items from dbo.SplitString(@OriginatorID,','))        
	ORDER BY Per desc
	END
	
ELSE

	Select 'E3' as DB,loanapp.EstCloseDate,
	(select top 1 DateValue  from mwlCustomField where custFieldDef_ID= @CustomFieldID
	and loanapp.ID=mwlCustomField.LoanApp_ID  order by datevalue desc) as ApprovalExpDate,loanapp.casenum as FHANumber,loanapp.ChannelType,
	loanapp	.OriginatorName,loanapp.processorname as ProcessorName,loanapp.currentStatus as LoanStatus, loanapp.CurrPipeStatusDate, 
	loanapp.DecisionStatus,loanapp.ID as record_id,'' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,
	borr.firstname as BorrowerFirstName,borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'' as LU_FINAL_HUD_STATUS,
	appStat.StatusDateTime AS SubmittedDate,approvalStat.StatusDateTime as DateApproved,case when TransType is null then '' when 
	TransTYpe = 'P' then 'Purchase' when TransTYpe = 'R' then 'Refinance' end as TransTYpe,LoanData.LoanProgDesc as LoanProgram,
	loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockExpirationDate,LockRecord.LockDateTime,
	UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'') WHEN '' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) 
	END as BrokerName,Lookups.DisplayString as CashOut,CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, 
	convert(varchar(35),EstCloseDate,101) AS ScheduleDate , CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a 
	where DueBy='Prior to Closing' and Category !='LENDER' and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  
	WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0 WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a 
	where (DueBy='Prior to Closing' and Category !='LENDER')  and (CurrentState='CLEARED' 
	OR CurrentState='SUBMITTED')
	and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   
	ELSE (SELECT count(a.ID) as TotalCleared 
	FROM dbo.mwlCondition a where (DueBy='Prior to Closing' and Category !='LENDER') and (CurrentState='CLEARED' OR CurrentState='SUBMITTED') 
	and a.objOwner_ID IN (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / 
	(SELECT count(a.ID) as	TotalConditions FROM dbo.mwlCondition a where ( DueBy='Prior to Closing' and Category !='LENDER') 
	and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER 
	from mwlLoanApp  loanapp  
	Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
	Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id and loanData.Active=1
	left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
	Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = 'BROKER' 
	and Broker.objownerName='Broker' 
	left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType='LOCK' 
	and  LockRecord.Status<>'CANCELED' 
	Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc=@StatusDescr
	Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved' 
	Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName='mwlLoanData' and fieldname='refipurpose'  
	Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = 'CORRESPOND' 
	and Corres.objownerName='Correspondent' 
	left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID 
	and CustFieldDef_id in (select Id from mwsCustFieldDef where CustomFieldName='Retail Processor')
	where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''   
	AND loanapp.ChannelType like '%RETAIL%'   AND CurrentStatus  IN (select items from dbo.SplitString(@CurrentStatus,',')) 
	ORDER BY Per desc
	END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_GetRetailOpMgrReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_GetRetailOpMgrReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: <Author,,Name>
-- Create date: <Create Date,,>
-- Description: <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_GetRetailOpMgrReport]
@iUserId INT,
@sRolename nVARCHAR(500),
@whereClause VARCHAR(1000),
@sortField VARCHAR(200),
@SortOrder VARCHAR(10),
@PageNo INT,
@Pagesize INT,
@Export INT

AS
BEGIN
Declare @sql NVARCHAR(Max)
DECLARE @sql1 NVARCHAR(Max)
DECLARE @sql2 NVARCHAR(Max)
DECLARE @sday INT

If @sortField = 'BorrowerLastName'
Begin
    Set @sortField = 'borr.lastname'
End
If @sortField = 'BorrowerFirstName'
Begin
    Set @sortField = 'borr.firstname'
End
If @sortField = 'loan_amt'
Begin
    Set @sortField = 'loanData.AdjustedNoteAmt'
End
If @sortField = 'LoanStatus'
Begin
    Set @sortField = 'loanapp.currentStatus'
End 
If @sortField = 'LoanProgram'
Begin
    Set @sortField = 'LoanData.FinancingType'
End    
If @sortField = 'LockExpirationDate'
Begin
    Set @sortField = 'LockRecord.LockExpirationDate'
End
If @sortField = 'SubmittedDate'
Begin
    Set @sortField = 'appStat.StatusDateTime'
END


IF @sRolename = '09 - Application Received'
BEGIN
SET @sday = 15
END
IF @sRolename = '10 - Application on Hold'
BEGIN
SET @sday = 10
END
IF @sRolename = '13 - File Intake'
BEGIN
SET @sday = 5
END
IF @sRolename = '14 - Prop Inspection Waived'
BEGIN
SET @sday = 45
END
IF @sRolename = '15 - Appraisal Ordered'
BEGIN
SET @sday = 45
END
IF @sRolename = '17 - Submission On Hold'
BEGIN
SET @sday = 15
END
IF @sRolename = '18 - New Construction On Hold'
BEGIN
SET @sday = 30
END
IF @sRolename = '21 - In Processing'
BEGIN
SET @sday = 60
END
IF @sRolename = '22 - Appraisal Received'
BEGIN
SET @sday = 30
END

IF @Export = 1
--export
BEGIN


SET @sql2 = ''
SET @sql2 = ''
SET @sql2 = @sql2
+ 'select ''E3'' as DB,loanData.AdjustedNoteAmt AS Totalamt,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue as ProcessorName,
loanapp.currentStatus as LoanStatus, loanapp.CurrPipeStatusDate, CurrDecStatusDate as CurrDecisionstatus,loanapp.DecisionStatus,
loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.firstname as BorrowerFirstName,
borr.lastname as BorrowerLastName,'''' as LU_FINAL_HUD_STATUS, approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe, case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN ''VA'' End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM
(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,
CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate,
'''' AS PER from mwlLoanApp loanapp
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK''
and LockRecord.Status<>''CANCELED'' 
Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved''
Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''Branch'' and Corres.objownerName=''BranchInstitution''
left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID and CustFieldDef_id in(select Id from mwsCustFieldDef
where CustomFieldName=''Retail Processor'') Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id
and Branch.InstitutionType = ''Branch'' and Branch.objownerName=''BranchInstitution'' where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' and
loanapp.ChannelType like ''%RETAIL%'''

IF @sRolename <> ''
BEGIN
SET @sql2 = @sql2 + ' and loanapp.CurrentStatus=''' + @sRolename + ''''
END
SET @sql2 = @sql2
+ ' and (datediff(day,loanapp.CurrPipeStatusDate,getdate()) >= '
+ CONVERT(VARCHAR, @sday) + ')'
IF @whereClause <> ''
BEGIN
SET @sql2 = @sql2 + ' AND ' + @whereClause
END

SET @sql2 = @sql2 + ' order by + ' + @sortField + ' ' + @SortOrder
print @sql2
EXEC sp_executeSql @sql2

END
ELSE
BEGIN
PRINT @sday
SET @sql = ''
SET @sql = 'Select count(*) from ( '
SET @sql = @sql
+ 'select ''E3'' as DB,loanData.AdjustedNoteAmt AS Totalamt,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue as ProcessorName,
loanapp.currentStatus as LoanStatus, loanapp.CurrPipeStatusDate, CurrDecStatusDate as CurrDecisionstatus,loanapp.DecisionStatus,
loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.firstname as BorrowerFirstName,
borr.lastname as BorrowerLastName,'''' as LU_FINAL_HUD_STATUS, approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe, case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN ''VA'' End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM
(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,
CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate,
'''' AS PER from mwlLoanApp loanapp
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK''
and LockRecord.Status<>''CANCELED'' 
Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved''
Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''Branch'' and Corres.objownerName=''BranchInstitution''
left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID and CustFieldDef_id in(select Id from mwsCustFieldDef
where CustomFieldName=''Retail Processor'') Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id
and Branch.InstitutionType = ''Branch'' and Branch.objownerName=''BranchInstitution'' where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' and
loanapp.ChannelType like ''%RETAIL%'''

IF @sRolename <> ''
BEGIN
SET @sql = @sql + ' and loanapp.CurrentStatus=''' + @sRolename + ''''
END
SET @sql = @sql
+ ' and (datediff(day,loanapp.CurrPipeStatusDate,getdate()) >= '
+ CONVERT(VARCHAR, @sday) + ')'
IF @whereClause <> ''
BEGIN
SET @sql = @sql + ' AND ' + @whereClause
END
SET @sql = @sql + ' ) as t2 where 1=1 '

-- print @sql
EXEC sp_executeSql @sql
SET @sql1 = ''
SET @sql1 = 'Select * from ( '
SET @sql1 = @sql1 + 'SELECT
ROW_NUMBER() OVER (ORDER BY ' + @sortField
+ ' ' + @SortOrder + ' )AS Row,''E3'' as DB,loanData.AdjustedNoteAmt AS Totalamt,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue as ProcessorName,
loanapp.currentStatus as LoanStatus, loanapp.CurrPipeStatusDate, CurrDecStatusDate as CurrDecisionstatus,loanapp.DecisionStatus,
loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.firstname as BorrowerFirstName,
borr.lastname as BorrowerLastName,'''' as LU_FINAL_HUD_STATUS, approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe, case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN ''VA'' End as LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM
(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,
CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate,
'''' AS PER from mwlLoanApp loanapp
Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id
left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id
Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK''
and LockRecord.Status<>''CANCELED'' 
Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved''
Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''
Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''Branch'' and Corres.objownerName=''BranchInstitution''
left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID and CustFieldDef_id in(select Id from mwsCustFieldDef
where CustomFieldName=''Retail Processor'') Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id
and Branch.InstitutionType = ''Branch'' and Branch.objownerName=''BranchInstitution'' where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''' and
loanapp.ChannelType like ''%RETAIL%'''

IF @sRolename <> ''
BEGIN
SET @sql1 = @sql1 + ' and loanapp.CurrentStatus=''' + @sRolename + ''''
END
SET @sql1 = @sql1
+ ' and (datediff(day,loanapp.CurrPipeStatusDate,getdate()) >= '+ CONVERT(VARCHAR, @sday) + ')'
IF @whereClause <> ''
BEGIN
SET @sql1 = @sql1 + ' AND ' + @whereClause
END
SET @sql1 = @sql1 + ' ) as t3 '
SET @sql1 = @sql1 + ' Where Row between (' + CAST(@PageNo AS NVARCHAR) + ' - 1) * ' + CAST(@Pagesize AS NVARCHAR) + ' + 1 and ' + CAST(@PageNo AS NVARCHAR) + ' * ' + CAST(@Pagesize AS NVARCHAR)
SET @sql1 = @sql1 + ' Order by row '
PRINT @sql1
EXEC sp_executesql @sql1
END


END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getSceondryBorrowerInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getSceondryBorrowerInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_getSceondryBorrowerInfo]
(
@LoanID VARCHAR(MAX)
)
AS
BEGIN
SELECT FirstMiddleName +' '+ LastName as FirstMiddleName FROM dbo.Borrower WHERE BorrowerID = '2' AND LoanID = @LoanID
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getServiceInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getServiceInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_getServiceInfo]
(
@loanid VARCHAR(MAX)
)
AS
BEGIN
SELECT a.LoanCloseDate, a.MaturityDate, a.OriginalAmt, a.OriginalTerm, a.InterestRate, a.PrincipalBal, a.UnappliedBal, a.LCBal, a.OthFeeBal, a.DueDate, a.NextPmtNum, a.PIPmt, 
a.EscrowPmt,isnull(a.PIPmt, 0) + isnull(a.EscrowPmt,0) as TotalPmt, b.principal,b.grossinterest,b.insurancedisb, 
isnull(c.addressline1,'') as addressline1,isnull(c.addressline2,'') as addressline2,c.city,c.county,c.state,c.zip 
FROM Loan a,yeartodate b,property c where c.loanid = a.loanid and a.loanid = b.loanid and a.loanid = @loanid
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getServiceShortInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getServiceShortInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
-- =============================================    
-- Author: Abhishek Rastogi    
-- Create Date : 10/06/2012    
-- Edited By : --    
-- Edited Date : --    
-- Description: for clsPipeLine.cs BLL    
-- =============================================    
    
CREATE PROCEDURE [dbo].[sp_getServiceShortInfo]    
(    
@loanid VARCHAR(MAX)    
)    
AS    
BEGIN    
SELECT lastpmtRecvdDate, PrincipalBal, DueDate, isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt FROM   
Loan  
 where loanid = @loanid    
    
select top 1 Escrowamt as EscrowPmt,transactionamt as TotalPmt,(transactionamt - Escrowamt) as PIPmt,* from history where loanid = @loanid and transactioncode in( '210' , '211' ) order by transactionDate Desc  
    
select top 1 PIPayment,EscrowPmt,(PIPayment + EscrowPmt) as TotalPmt,* from paymentchange where loanid = @loanid order by duedate Desc    
END      
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetShortDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetShortDetails]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetShortDetails]
	-- Add the parameters for the stored procedure here

	
@LoanApplicationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM tblLoanApplication where LoanApplicationID=@LoanApplicationID

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetState]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  [dbo].[sp_GetState]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *  from tblState
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetState_clsloan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetState_clsloan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetState_clsloan]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT   ID, StateID, Abbreviature, StateName + ' - ' + Abbreviature as StatenameWithAbbr FROM tblStateNylex ORDER BY StatenameWithAbbr

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetState1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetState1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetState1]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
    -- Insert statements for procedure here
SELECT   ID, StateID, Abbreviature, StateName + ' - ' + Abbreviature as StatenameWithAbbr FROM tblStateNylex ORDER BY StateOrder
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStateID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStateID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetStateID]
	-- Add the parameters for the stored procedure here

	@Abbreviature nvarchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT StateID FROM tblStateNylex where Abbreviature=@Abbreviature

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStateIDNew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStateIDNew]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetStateIDNew]
	-- Add the parameters for the stored procedure here

	@Abbreviature nvarchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select stateid from tblStateNylex where Abbreviature = @Abbreviature

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStateName_tblState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStateName_tblState]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[sp_GetStateName_tblState]
	-- Add the parameters for the stored procedure here

	@sid varchar(15)
	

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Name from tblState where state_id=@sid
END
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetStatus]
	-- Add the parameters for the stored procedure here
	@id varchar(2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Status where id=@id
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStoneGateLoanApplicationAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStoneGateLoanApplicationAdd]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetStoneGateLoanApplicationAdd]
(
@userid VARCHAR(MAX)
)
AS
BEGIN
SELECT userid, ufirstname, ulastname, uemailid, urole FROM tblUsers where userid =@userid
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStoneGateLoanApplicationData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStoneGateLoanApplicationData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetStoneGateLoanApplicationData]

AS
BEGIN
select * from tblLoanApplication order by LoanApplicationID desc

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStoneGateOfficeData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStoneGateOfficeData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsUserRegistrationE3.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetStoneGateOfficeData]
(
@userid varchar (100)
)
AS
BEGIN
	 select * from tblOffices where userid =@userid order by recordid desc
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStoneGateUserData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStoneGateUserData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsUserRegistrationE3.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetStoneGateUserData]
(
@uidprolender varchar(Max)
)
AS
BEGIN
	 select * from tblusers where uidprolender =@uidprolender
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetTranscationtype]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetTranscationtype]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetTranscationtype]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select 0 as lookupid, '--Select Type--' as Description union select lookupid,Description  from tbllookups where lookupname ='TT'


END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUMWAndUnderwriter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUMWAndUnderwriter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetUMWAndUnderwriter]
AS
BEGIN
SELECT E3Userid,uidprolender,rtrim(ufirstname) + ' ' + ulastname as underwriter FROM tblUsers WHERE urole=10  or urole=11 AND isactive=1 order by rtrim(ufirstname) + ' ' + ulastname
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUMWAndUnderwriterE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUMWAndUnderwriterE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetUMWAndUnderwriterE3]
AS
BEGIN
SELECT E3Userid,rtrim(ufirstname) + ' ' + ulastname as underwriter FROM tblUsers WHERE urole=10  or urole=11 AND isactive=1 order by rtrim(ufirstname) + ' ' + ulastname
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUSDAProductType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUSDAProductType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUSDAProductType]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT top 1 ID, producttypeid, name, term, isarm FROM  tblProductType where id not in(3)

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUserList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUserList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  Stonegate      
-- Create date: 9th Apr 2012      
-- Description: This will get user list to bind in the grid      
-- =============================================     
CREATE PROCEDURE [dbo].[sp_GetUserList]
(      
@sortField varchar(500),      
@sortOrder varchar(100),      
@whereClause varchar(1000),      
@PageNo int,       
@PageSize int      
)       
AS      
BEGIN  

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    
IF @sortField  = ''                                  
  set @sortField = 'tblUsers.userid'      
      
SET @sortField = @sortField + ' ' + @sortOrder      
    
set @whereClause =  REPLACE(@whereClause,'[uurole]','tblroles.roles')                                        
      
Declare @sql nvarchar(4000)      
      
set @sql = ''                                  
     set @sql = 'Select count(*) from ( '      
     set @sql =  @sql + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,      
tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid,       
tblUsers.urole, tblUsers.isadmin, tblUsers.isactive as Status, tblUsers.uidprolender, tblUsers.uparent,       
tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender,       
tblUsers.carrierid, Carrier.Carrier,tblroles.roles as uurole ,tblUsers.signedupdate,userLoginid       
FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID LEFT OUTER JOIN tblroles on tblUsers.urole=tblroles.id        
where urole<>6 and urole<>9 and urole<>17'      
      
IF @whereClause <> ''                                           
    BEGIN                                       
    set @sql = @sql  + ' AND ' + @whereClause                                 
    END                          
      
set @sql = @sql + ' group by tblUsers.userid,upassword, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid,       
tblUsers.urole, tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,       
tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender,       
tblUsers.carrierid, Carrier.Carrier,tblroles.roles ,tblUsers.signedupdate,userLoginid'                                   
 set @sql = @sql + ' ) as t2 '       
      
print @sql      
exec sp_executesql @sql        
      
      
set @sql = ''                                  
     set @sql = 'Select * from ( '       
set @sql =  @sql + 'SELECT               
        ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,tblUsers.userid, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid,       
tblUsers.urole, tblUsers.isadmin, Case When tblUsers.isactive = 1 Then ''Active'' Else ''Inactive'' End  as Status, tblUsers.uidprolender, tblUsers.uparent,       
tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender,       
tblUsers.carrierid, Carrier.Carrier,tblroles.roles as uurole ,tblUsers.signedupdate,userLoginid       
FROM tblUsers LEFT OUTER JOIN Carrier ON tblUsers.carrierid = Carrier.ID LEFT OUTER JOIN tblroles on tblUsers.urole=tblroles.id        
where urole<>6 and urole<>9 and urole<>17 '      
      
IF @whereClause <> ''                                           
    BEGIN                                       
    set @sql = @sql  + ' AND ' + @whereClause                                 
    END                          
      
set @sql = @sql + ' group by tblUsers.userid,upassword, tblUsers.ufirstname, tblUsers.ulastname, tblUsers.uemailid,       
tblUsers.urole, tblUsers.isadmin, tblUsers.isactive, tblUsers.uidprolender, tblUsers.uparent,       
tblUsers.upassword, tblUsers.maxloginattempt, tblUsers.Lastlogintime, tblUsers.mobile_ph, tblUsers.loginidprolender,       
tblUsers.carrierid, Carrier.Carrier,tblroles.roles ,tblUsers.signedupdate,userLoginid'                                   
set @sql = @sql + ' ) as t2 '      
      
set @sql = @sql + ' Where Row between (' + CAST( @PageNo AS NVARCHAR)  + ' - 1) * ' + Cast(@PageSize as nvarchar) + ' + 1 and ' + cast(@PageNo as nvarchar) + ' * ' + cast(@PageSize as nvarchar)      
      
print @sql                                
exec sp_executesql @sql      
      
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUserName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUserName]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUserName]
	-- Add the parameters for the stored procedure here
(
	@uid varchar(5) ,
	@username varchar(20)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select UserName=@username from tbluser where uid=@uid
    -- Insert statements for procedure here
	
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUserName_tbluser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUserName_tbluser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUserName_tbluser]
	-- Add the parameters for the stored procedure here
	
@uid int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select UserName from tbluser where uid=@uid
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetValidPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetValidPassword]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Shushant	<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sp_GetValidPassword
	-- Add the parameters for the stored procedure here
        
@uemailid varchar(200)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select userid from tblusers where uemailid =@uemailid        
 -- Insert statements for procedure here
	
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetYear]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetYear]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetYear]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	
                             DECLARE @max int 
                             set @max = DATEPART(year, getdate()) 
                             CREATE TABLE #temp (Year int) 
                             while @max >= 1909 
                             BEGIN 
                             insert #temp(Year) values(@max) 
                              set @max = @max - 1 
                              END 
                              SELECT Year from #temp 
                              drop table #temp 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert]
	-- Add the parameters for the stored procedure here
(
	@LoanReasonID int, 
@HometypeID int ,
@PropertyTypeID int, 
@Producttypeid int, 
@StateID int, 
@CreditScoreID int, 
@AnnulIncomeBorrower money,
@LoanAmount money, 
@AssetBorrower money, 
@RetirementAssetsBorrower money,
@MAName varchar(100),
@PropertyRealtorCity varchar(100),
@IsWorkingWithRealtor bit,
@IsCashback bit,
@RealtorName nvarchar(100),
@RealtorPhoneNo nvarchar(100) , 
@PropertyLocated bit, 
@HomeOwned bit, 
@PurchasePrise money, 
@DownPaymentAmt int, 
@PartOfTotalAssets bit,
 @DownPaymentSourceID int,
 @HaveRealtor bit,
 @RealtorContactName nvarchar(100),
 @RealtorPhone nvarchar(100) , 
@RealtorEmail nvarchar(100), 
@AppFName nvarchar(100), 
@AppLName nvarchar(100), 
@AppStateID int, 
@AppZip nvarchar(100), 
@AppPrimaryPhone nvarchar(100), 
@AppSecondaryPhone nvarchar(100), 
@AppEmail nvarchar(100), 
@ContactTimeID int, 
@CurrentPropertyValue money, 
@PropertyPurchaseMonth nvarchar(100), 
@YearID int,
 @ExistingPuchasePrice money,
 @CurrentMortageBal money, 
@MonthlyPayment money, 
@SecondMortage bit, 
@SecondMortageBal money,
@UserAppearUrl nvarchar(1000), 
@vCity varchar(100), 
@bSignAgg bit, 
@dSchClosingDate datetime, 
@vEstRenovationAmt nvarchar(100),
 @vDownPayAmount nvarchar(100), 
@vAddressLine1 nvarchar(500),
 @vAddressLine2 nvarchar(500), 
@ApplyType varchar(50) , 
@IsLead360 bit, 
@IsFullApp bit, 
@CompIP varchar(50)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
INSERT INTO tblLoanApplication

(
LoanReasonID , 
HometypeID  ,
PropertyTypeID , 
Producttypeid , 
StateID , 
CreditScoreID , 
AnnulIncomeBorrower,
LoanAmount , 
AssetBorrower , 
RetirementAssetsBorrower ,
MAName ,
PropertyRealtorCity ,
IsWorkingWithRealtor ,
IsCashback ,
RealtorName ,
RealtorPhoneNo , 
PropertyLocated , 
HomeOwned , 
PurchasePrise , 
DownPaymentAmt , 
PartOfTotalAssets,
 DownPaymentSourceID,
 HaveRealtor ,
 RealtorContactName ,
 RealtorPhone  , 
RealtorEmail , 
AppFName , 
AppLName , 
AppStateID , 
AppZip , 
AppPrimaryPhone , 
AppSecondaryPhone , 
AppEmail , 
ContactTimeID , 
CurrentPropertyValue , 
PropertyPurchaseMonth , 
YearID ,
 ExistingPuchasePrice ,
 CurrentMortageBal , 
MonthlyPayment , 
SecondMortage , 
SecondMortageBal ,
UserAppearUrl , 
vCity , 
bSignAgg , 
dSchClosingDate , 
vEstRenovationAmt ,
 vDownPayAmount , 
vAddressLine1 ,
 vAddressLine2 , 
ApplyType  , 
IsLead360 , 
IsFullApp , 
CompIP 
)
values
(
@LoanReasonID , 
@HometypeID  ,
@PropertyTypeID , 
@Producttypeid , 
@StateID , 
@CreditScoreID , 
@AnnulIncomeBorrower,
@LoanAmount , 
@AssetBorrower , 
@RetirementAssetsBorrower ,
@MAName ,
@PropertyRealtorCity ,
@IsWorkingWithRealtor ,
@IsCashback ,
@RealtorName ,
@RealtorPhoneNo , 
@PropertyLocated, 
@HomeOwned , 
@PurchasePrise , 
@DownPaymentAmt , 
@PartOfTotalAssets,
 @DownPaymentSourceID,
 @HaveRealtor ,
 @RealtorContactName ,
 @RealtorPhone  , 
@RealtorEmail , 
@AppFName , 
@AppLName , 
@AppStateID , 
@AppZip , 
@AppPrimaryPhone , 
@AppSecondaryPhone , 
@AppEmail , 
@ContactTimeID , 
@CurrentPropertyValue , 
@PropertyPurchaseMonth , 
@YearID ,
 @ExistingPuchasePrice ,
 @CurrentMortageBal , 
@MonthlyPayment , 
@SecondMortage , 
@SecondMortageBal ,
@UserAppearUrl , 
@vCity , 
@bSignAgg , 
@dSchClosingDate , 
@vEstRenovationAmt ,
 @vDownPayAmount , 
@vAddressLine1 ,
 @vAddressLine2 , 
@ApplyType  , 
@IsLead360 , 
@IsFullApp , 
@CompIP 
)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_CareerPosting]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_CareerPosting]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Created By:	Malav Shah
-- Create Date : 10/04/2012
-- Description:	Procedure for insert/update/delete careeer posting
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert_CareerPosting] 
(
@stringmode varchar(50),
@CareerId int,      
@PositionName varchar(50),   
@Description ntext,  
@PublishedDate datetime = null,
@IsPublished bit
)
AS
BEGIN
if(@stringmode = 'insert')

 INSERT INTO tblCareerPosting (PositionName,Description,ModifiedDate,PublishedDate)      
 VALUES (@PositionName,@Description,getdate(),null)    
   

else if(@stringmode = 'update')

update tblCareerPosting 
set PositionName=@PositionName,Description=@Description,ModifiedDate=getdate()
where CareerId = @CareerId 
 

else if(@stringmode = 'delete')

delete from tblCareerPosting where CareerId = @CareerId


END





GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_ConsumerAuthor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_ConsumerAuthor]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_Insert_ConsumerAuthor] 
(
@stringmode varchar(50),
@AppID int,
@IUnderstand bit,
@IfApplicable bit,
@IAcknowledge bit,
@IAuthorise bit,
@ApplicantOnly bit,
@IHereby bit,
@OtherNameUsed varchar(20),  
@SSNNo varchar(20),
@StreetAddress varchar(500),
@DobDay varchar(5),
@DobMonth varchar(12),
@DobYear varchar(50),
@ConCity varchar(50),
@ConState varchar(50),
@ConZip varchar(15),
@LsnNoState varchar(100),
@LsnNoName varchar(50),
@Acceptterm bit,
@ConAppName varchar(50), 
@ConAppDate datetime 
)
AS
BEGIN
	 INSERT INTO tblConsumerAuthor(AppID,IUnderstand,IfApplicable,IAcknowledge,IAuthorise,ApplicantOnly,IHereby,OtherNameUsed,SSNNo,StreetAddress,DobDay,DobMonth,DobYear,ConCity,ConState,ConZip,LsnNoState,LsnNoName,Acceptterm,ConAppName,ConAppDate)
     VALUES (@AppID,@IUnderstand,@IfApplicable,@IAcknowledge,@IAuthorise,@ApplicantOnly,@IHereby,@OtherNameUsed,@SSNNo,@StreetAddress,@DobDay,@DobMonth,@DobYear,@ConCity,@ConState,@ConZip,@LsnNoState,@LsnNoName,@Acceptterm,@ConAppName,@ConAppDate)  
END








GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_EmailReader]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_EmailReader]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Insert_EmailReader] 
(    
@ToAddress nvarchar(100),   
@ToName nvarchar(500),    
@MailSubject nvarchar,    
@MailBody nvarchar,
@MobileEmailId nvarchar(50),
@MobileEmailBody nvarchar   
)
AS
BEGIN
	 INSERT INTO tblEmailReader(ToAddress,ToName,MailSubject,MailBody,MobileEmailId,MobileEmailBody)      
     VALUES (@ToAddress,@ToName,@MailSubject,@MailBody,@MobileEmailId,@MobileEmailBody)    
    
END








GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_Employement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_Employement]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Insert_Employement] 
(
@stringmode varchar(50),
@ApplicationID int,
@FirstName varchar(50),
@MiddleName varchar(50),
@LastName varchar(50),
@State varchar(30),
@City varchar(30),
@StreetAddress nvarchar(250),
@Zip varchar(50),
@PhoneNo nvarchar(20),
@PosApplied varchar(20),
@DateAvailable datetime,
@Adult bit,
@ProofLegallyEntitled bit,
@SGEmployed bit,
@EmployedDesc ntext,
@SGRelativeEmployed bit,
@RelativesDesc ntext,
@ConvictedOfCrime bit,
@CrimeDesc ntext,
@DismissedOrResign bit,
@DismissalDesc ntext,
@Imseeking varchar(50),
@Imavailable varchar(50),
@TempDatesAvailable varchar(500),
@HoursPerweek varchar(30),
@HighSchool varchar(30),
@TechSchool varchar(30),
@Collage varchar(30),
@Certification varchar(30),
@FirstProRefName varchar(50),
@FirstProRefPhone nvarchar(20),
@FirstProRefAddr varchar(500),
@SecondProRefName varchar(50),
@SecondProRefPhone nvarchar(20),
@SecondProRefAddr varchar(500),
@ThirdProRefName varchar(50),
@ThirdProRefPhone nvarchar(20),
@ThirdProRefAddr varchar(500),
@FirstPerRefName varchar(50),
@FirstPerRefPhone nvarchar(20),
@FirstPerRefAddr nvarchar(500),
@SecondPerRefName varchar(50),
@SecondPerRefPhone nvarchar(20),
@SecondPerRefAddr varchar(500),
@ThirdPerRefName varchar(50),
@ThirdPerRefPhone nvarchar(20),
@ThirdPerRefAddr varchar(500),
@NMLSLicense bit,
@AligiblityOfWork bit ,
@DeniedLicense bit,
@msofficeAvaiblityLevel varchar(12),
@CurrentEmployer bit,
@ContactEmployer bit,
@spSkills varchar(500),
@FileName varchar(50),
@LicenseNo varchar(100),
@ApplicantName varchar(50),
@AppliedDate datetime,
@CreatedDate datetime,
@jobid varchar(50) output,

@CompanyName varchar(50),
@coAddress varchar(500),
@coPhone varchar(20),
@FromDate datetime,
@Todate datetime,
@RateOfPay varchar(20),
@PositionHeld varchar(50),
@Supervisor varchar(50),
@WorkPerformed varchar(150),
@LeaveReason varchar(50),

@CompanyNamesec varchar(50),
@coAddresssec varchar(500),
@coPhonesec varchar(20),
@FromDatesec datetime,
@Todatesec datetime,
@RateOfPaysec varchar(20),
@PositionHeldsec varchar(50),
@Supervisorsec varchar(50),
@WorkPerformedsec varchar(150),
@LeaveReasonsec varchar(50),

@CompanyNamethd varchar(50),
@coAddressthd varchar(500),
@coPhonethd varchar(20),
@FromDatethd datetime,
@Todatethd datetime,
@RateOfPaythd varchar(20),
@PositionHeldthd varchar(50),
@Supervisorthd varchar(50),
@WorkPerformedthd varchar(150),
@LeaveReasonthd varchar(50)
)
AS
BEGIN
 INSERT INTO tblEmployementApplication(FirstName,MiddleName,LastName,State,City,StreetAddress,Zip,PhoneNo,PosApplied,DateAvailable,Adult,ProofLegallyEntitled,SGEmployed,EmployedDesc,SGRelativeEmployed,RelativesDesc,ConvictedOfCrime,CrimeDesc,DismissedOrResign,DismissalDesc,Imseeking,Imavailable,TempDatesAvailable,HoursPerweek,HighSchool,TechSchool,Collage,Certification,FirstProRefName,FirstProRefPhone,FirstProRefAddr,SecondProRefName,SecondProRefPhone,SecondProRefAddr,ThirdProRefName,ThirdProRefPhone,ThirdProRefAddr,FirstPerRefName,FirstPerRefPhone,FirstPerRefAddr,SecondPerRefName,SecondPerRefPhone,SecondPerRefAddr,ThirdPerRefName,ThirdPerRefPhone,ThirdPerRefAddr,NMLSLicense,AligiblityOfWork,DeniedLicense,msofficeAvaiblityLevel,CurrentEmployer,ContactEmployer,spSkills,FileName,LicenseNo,ApplicantName,AppliedDate,CreatedDate)      
 VALUES (@FirstName,@MiddleName,@LastName,@State,@City,@StreetAddress,@Zip,@PhoneNo,@PosApplied,@DateAvailable,@Adult,@ProofLegallyEntitled,@SGEmployed,@EmployedDesc,@SGRelativeEmployed,@RelativesDesc,@ConvictedOfCrime,@CrimeDesc,@DismissedOrResign,@DismissalDesc,@Imseeking,@Imavailable,@TempDatesAvailable,@HoursPerweek,@HighSchool,@TechSchool,@Collage,@Certification,@FirstProRefName,@FirstProRefPhone,@FirstProRefAddr,@SecondProRefName,@SecondProRefPhone,@SecondProRefAddr,@ThirdProRefName,@ThirdProRefPhone,@ThirdProRefAddr,@FirstPerRefName,@FirstPerRefPhone,@FirstPerRefAddr,@SecondPerRefName,@SecondPerRefPhone,@SecondPerRefAddr,@ThirdPerRefName,@ThirdPerRefPhone,@ThirdPerRefAddr,@NMLSLicense,@AligiblityOfWork,@DeniedLicense,@msofficeAvaiblityLevel,@CurrentEmployer,@ContactEmployer,@spSkills,@FileName,@LicenseNo,@ApplicantName,@AppliedDate,@CreatedDate)  
--
 SELECT @jobid = SCOPE_IDENTITY() 
--
INSERT INTO tblEmpWorkExp(jobid,CompanyName,coAddress,coPhone,FromDate,Todate,RateOfPay,PositionHeld,Supervisor,WorkPerformed,LeaveReason)
VALUES (@jobid,@CompanyName,@coAddress,@coPhone,@FromDate,@Todate,@RateOfPay,@PositionHeld,@Supervisor,@WorkPerformed,@LeaveReason)  
--
INSERT INTO tblEmpWorkExp(jobid,CompanyName,coAddress,coPhone,FromDate,Todate,RateOfPay,PositionHeld,Supervisor,WorkPerformed,LeaveReason)
VALUES (@jobid,@CompanyNamesec,@coAddresssec,@coPhonesec,@FromDatesec,@Todatesec,@RateOfPaysec,@PositionHeldsec,@Supervisorsec,@WorkPerformedsec,@LeaveReasonsec)  
--
INSERT INTO tblEmpWorkExp(jobid,CompanyName,coAddress,coPhone,FromDate,Todate,RateOfPay,PositionHeld,Supervisor,WorkPerformed,LeaveReason)
VALUES (@jobid,@CompanyNamethd,@coAddressthd,@coPhonethd,@FromDatethd,@Todatethd,@RateOfPaythd,@PositionHeldthd,@Supervisorthd,@WorkPerformedthd,@LeaveReasonthd)  
END









GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_FinalTralingDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_FinalTralingDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_Insert_FinalTralingDoc] 
(
@Comments varchar(500),
@userid int,      
@DocName varchar(200),
@loan_no varchar(15)
)
AS
BEGIN
	 INSERT INTO tblFinalTralingDoc (Comments,userid,DocName,loan_no)      
     VALUES  (@Comments,@userid,@DocName,@loan_no)       
  
END








GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_loanapp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_loanapp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert_loanapp]
	-- Add the parameters for the stored procedure here

(
@Fname varchar(45),
@Mname varchar(45),
@LName varchar(45),
@Email varchar(200),
@CEmail varchar(200),
@DOBM varchar(10),
@DOBDT int,
@DOBYR int,
@SSN varchar(45),
@Phone varchar(45),
@Suffix varchar(5),
@COFNAME varchar(45),
@COMNAME varchar(45),
@COLNAME varchar(45),
@COEMAIL varchar(200),
@COCEmail varchar(200),
@CODOBM varchar(10),
@CODOBDT int,
@CODOBYR int,
@COSSN varchar(45),
@COPHONE varchar(45),
@STADDR varchar(45),
@CITY varchar(45),
@MState int,
@ZipCode varchar(10),
@RSTATUS varchar(25),
@RentAmt varchar(100),
@LMHOLDER varchar(45),
@STADDR2 varchar(45),
@CITY2 varchar(45),
@STATE2 int,
@COSuffix varchar(45),
@ZipCode2 varchar(10),
@RLengthY2 int,
@RLengthM2 int,
@RSTATUS2 varchar(25),
@RentAmt2 varchar(100),
@LMHOLDER2 varchar(45),
@AEMPL varchar(100),
@AOCCU varchar(100),
@AEMPPHONE varchar(45),
@AGMI varchar(100),
@ALEMY int,
@ALEMM int,
@AEMPL2 varchar(100),
@AOCCU2 varchar(100),
@AEMPPHONE2 varchar(45),
@AGMI2 int,
@ALEMY2 int,
@ALEMM2 int,
@CAEMPL varchar(100),
@CAOCCU varchar(100),
@CAEMPPHONE varchar(45),
@CAGMI varchar(100),
@CALEMY int,
@CALEMM int,
@CAEMPL2 varchar(100),
@CAOCCU2 varchar(100),
@CAEMPPHONE2 varchar(45),
@CAGMI2 varchar(100),
@CALEMY2 int,
@CALEMM2 int,
@OtherComments varchar(1000),
@DigiSig varchar(100),
@SigDate varchar(15),
@IsAckno bit ,
@DateEntered datetime,
@RLengthM int,
@RlengthY int
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into tblloanapp(Fname,Mname,LName,Email,CEmail,DOBM,DOBDT,DOBYR,SSN,Phone,Suffix,COFNAME,COMNAME,COLNAME,COEMAIL,COCEmail,CODOBM,CODOBDT,CODOBYR,COSSN,COPHONE,STADDR,CITY,MState,ZipCode,RSTATUS,RentAmt,LMHOLDER,STADDR2,CITY2,STATE2,COSuffix,ZipCode2,RLengthY2,RLengthM2,RSTATUS2,RentAmt2,LMHOLDER2,AEMPL,AOCCU,AEMPPHONE,AGMI,ALEMY,ALEMM,AEMPL2,AOCCU2,AEMPPHONE2,AGMI2,ALEMY2,ALEMM2,CAEMPL,CAOCCU,CAEMPPHONE,CAGMI,CALEMY,CALEMM,CAEMPL2,CAOCCU2,CAEMPPHONE2,CAGMI2,CALEMY2,CALEMM2,OtherComments,DigiSig,SigDate,IsAckno,DateEntered,RLengthM,RlengthY)
 values(@Fname,@Mname,@LName,@Email,@CEmail,@DOBM,@DOBDT,@DOBYR,@SSN,@Phone,@Suffix,@COFNAME,@COMNAME,@COLNAME,@COEMAIL,@COCEmail,@CODOBM,@CODOBDT,@CODOBYR,@COSSN,@COPHONE,@STADDR,@CITY,@MState,@ZipCode,@RSTATUS,@RentAmt,@LMHOLDER,@STADDR2,@CITY2,@STATE2,@COSuffix,@ZipCode2,@RLengthY2,@RLengthM2,@RSTATUS2,@RentAmt2,@LMHOLDER2,@AEMPL,@AOCCU,@AEMPPHONE,@AGMI,@ALEMY,@ALEMM,@AEMPL2,@AOCCU2,@AEMPPHONE2,@AGMI2,@ALEMY2,@ALEMM2,@CAEMPL,@CAOCCU,@CAEMPPHONE,@CAGMI,@CALEMY,@CALEMM,@CAEMPL2,@CAOCCU2,@CAEMPPHONE2,@CAGMI2,@CALEMY2,@CALEMM2,@OtherComments,@DigiSig,@SigDate,@IsAckno,@DateEntered,@RLengthM,@RlengthY)
 

end


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_LoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_LoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert_LoanApplication]
	-- Add the parameters for the stored procedure here
(
	@LoanReasonID int, 
@HometypeID int ,
@PropertyTypeID int, 
@Producttypeid int, 
@StateID int, 
@CreditScoreID int, 
@AnnulIncomeBorrower money,
@LoanAmount money, 
@AssetBorrower money, 
@RetirementAssetsBorrower money,
@MAName varchar(100),
@PropertyRealtorCity varchar(100),
@IsWorkingWithRealtor bit,
@IsCashback bit,
@RealtorName nvarchar(100),
@RealtorPhoneNo nvarchar(100) , 
@PropertyLocated bit, 
@HomeOwned bit, 
@PurchasePrise money, 
@DownPaymentAmt int, 
@PartOfTotalAssets bit,
 @DownPaymentSourceID int,
 @HaveRealtor bit,
 @RealtorContactName nvarchar(100),
 @RealtorPhone nvarchar(100) , 
@RealtorEmail nvarchar(100), 
@AppFName nvarchar(100), 
@AppLName nvarchar(100), 
@AppStateID int, 
@AppZip nvarchar(100), 
@AppPrimaryPhone nvarchar(100), 
@AppSecondaryPhone nvarchar(100), 
@AppEmail nvarchar(100), 
@ContactTimeID int, 
@CurrentPropertyValue money, 
@PropertyPurchaseMonth nvarchar(100), 
@YearID int,
 @ExistingPuchasePrice money,
 @CurrentMortageBal money, 
@MonthlyPayment money, 
@SecondMortage bit, 
@SecondMortageBal money,
@UserAppearUrl nvarchar(1000), 
@vCity varchar(100), 
@bSignAgg bit, 
@dSchClosingDate datetime, 
@vEstRenovationAmt nvarchar(100),
 @vDownPayAmount nvarchar(100), 
@vAddressLine1 nvarchar(500),
 @vAddressLine2 nvarchar(500), 
@ApplyType varchar(50) , 
@IsLead360 bit, 

@CompIP varchar(50)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblLoanApplication

(
LoanReasonID , 
HometypeID  ,
PropertyTypeID , 
Producttypeid , 
StateID , 
CreditScoreID , 
AnnulIncomeBorrower,
LoanAmount , 
AssetBorrower , 
RetirementAssetsBorrower ,
MAName ,
PropertyRealtorCity ,
IsWorkingWithRealtor ,
IsCashback ,
RealtorName ,
RealtorPhoneNo , 
PropertyLocated , 
HomeOwned , 
PurchasePrise , 
DownPaymentAmt , 
PartOfTotalAssets,
 DownPaymentSourceID,
 HaveRealtor ,
 RealtorContactName ,
 RealtorPhone  , 
RealtorEmail , 
AppFName , 
AppLName , 
AppStateID , 
AppZip , 
AppPrimaryPhone , 
AppSecondaryPhone , 
AppEmail , 
ContactTimeID , 
CurrentPropertyValue , 
PropertyPurchaseMonth , 
YearID ,
 ExistingPuchasePrice ,
 CurrentMortageBal , 
MonthlyPayment , 
SecondMortage , 
SecondMortageBal ,
UserAppearUrl , 
vCity , 
bSignAgg , 
dSchClosingDate , 
vEstRenovationAmt ,
 vDownPayAmount , 
vAddressLine1 ,
 vAddressLine2 , 
ApplyType  , 
IsLead360 , 

CompIP 
)
values
(
@LoanReasonID , 
@HometypeID  ,
@PropertyTypeID , 
@Producttypeid , 
@StateID , 
@CreditScoreID , 
@AnnulIncomeBorrower,
@LoanAmount , 
@AssetBorrower , 
@RetirementAssetsBorrower ,
@MAName ,
@PropertyRealtorCity ,
@IsWorkingWithRealtor ,
@IsCashback ,
@RealtorName ,
@RealtorPhoneNo , 
@PropertyLocated, 
@HomeOwned , 
@PurchasePrise , 
@DownPaymentAmt , 
@PartOfTotalAssets,
 @DownPaymentSourceID,
 @HaveRealtor ,
 @RealtorContactName ,
 @RealtorPhone  , 
@RealtorEmail , 
@AppFName , 
@AppLName , 
@AppStateID , 
@AppZip , 
@AppPrimaryPhone , 
@AppSecondaryPhone , 
@AppEmail , 
@ContactTimeID , 
@CurrentPropertyValue , 
@PropertyPurchaseMonth , 
@YearID ,
 @ExistingPuchasePrice ,
 @CurrentMortageBal , 
@MonthlyPayment , 
@SecondMortage , 
@SecondMortageBal ,
@UserAppearUrl , 
@vCity , 
@bSignAgg , 
@dSchClosingDate , 
@vEstRenovationAmt ,
 @vDownPayAmount , 
@vAddressLine1 ,
 @vAddressLine2 , 
@ApplyType  , 
@IsLead360 , 
 
@CompIP 
)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_LoanDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_LoanDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Insert_LoanDoc] 
(
@Loan_No char(15),
@docs_text text,      
@FileName varchar(200)
)
AS
BEGIN
	 INSERT INTO tblloandocs (Loan_No,docs_text,FileName,dttime)      
     VALUES  (@Loan_No,@docs_text,@FileName,getdate())       
  
END









GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_LoanDocs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_LoanDocs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_Insert_LoanDocs] 
(
@uloanregid int,
@Docname varchar(100),      
@Comments varchar(500)
)
AS
BEGIN
	 INSERT INTO tblloandoc (uloanregid,Docname,Comments)      
     VALUES  (@uloanregid,@Docname,@Comments)       
  
END








GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_LoanRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_LoanRegister]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_Insert_LoanRegister] 
(
@borrLast varchar(100),
@transType int,      
@loanAmt numeric(14,2),   
@lockStatus varchar(20),    
@loanPgm varchar(20),    
@userid int,
@filetype varchar(50),
@filename varchar(100) 
)
AS
BEGIN
	 INSERT INTO tblLoanReg (borrLast,transType,loanAmt,lockStatus,loanPgm,userid,filetype,filename)      
     VALUES  (@borrLast,@transType,@loanAmt,@lockStatus,@loanPgm,@userid,@filetype,@filename)       
  
END








GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_NewRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_NewRate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Insert_NewRate] 
(
@stringmode varchar(50),
@NewRateID int,      
@strLoanType varchar(50),
@LoanProgram varchar(250),
@GoalOfLoan varchar(50),   
@IPoints int,    
@DecRate decimal(18,3),    
@DecAPR decimal(18,3),
@DecClosingCost decimal(18,2),
@CreatedDate datetime,   
@ModifiedDate datetime   

)
AS
BEGIN
if(@stringmode = 'insert')
insert into tblnewrates(strLoanType,LoanProgram,GoalOfLoan,IPoints,DecRate,DecAPR,DecClosingCost,CreatedDate,ModifiedDate)
VALUES (@strLoanType,@LoanProgram,@GoalOfLoan,@IPoints,@DecRate,@DecAPR,@DecClosingCost,@CreatedDate,@ModifiedDate)  
else if(@stringmode = 'update')
update tblnewrates
set strloantype = @StrLoanType,LoanProgram=@LoanProgram,GoalOfLoan=@GoalOfLoan,IPoints = @IPoints,DecRate =  @DecRate,DecAPR = @DecAPR,DecClosingCost = @DecClosingCost,ModifiedDate = getdate()
where NewRateID = @NewRateID
else if(@stringmode = 'delete')
delete from tblnewrates where NewRateID = @NewRateID
END











GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_News]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_News]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procEDURE [dbo].[sp_Insert_News] 
(
@stringmode varchar(50),
@id int,      
@Title nvarchar(1000),   
@Body text,    
@IsActive bit,    
@IsNewsAnnouncement bit,
@CreatedBy int,
@IPAddress varchar(50),   
@CreatedDate datetime,    
@Role nvarchar(Max),
@RoleC nvarchar(Max),
@RoleR nvarchar(max),      
@NewsAnnousVisbleAd bit,     
@NewsAnnousVisbleDR bit,      
@NewsAnnousVisbleBR bit,     
@NewsAnnousVisbleLO bit,      
@NewsAnnousVisbleCU bit,      
@NewsAnnousVisbleRE bit,      
@NewsAnnousVisbleAM bit,        
@NewsAnnousVisbleDM bit  
)
AS
BEGIN
INSERT INTO tblNewsAndAnnouncement (Title, Body, IsActive, IsNewsAnnouncement, CreatedBy, CreatedDate,
 IPAddress, Role,RoleC,RoleR, NewsAnnousVisbleAd, NewsAnnousVisbleDR, NewsAnnousVisbleBR, NewsAnnousVisbleLO, 
 NewsAnnousVisbleCU, NewsAnnousVisbleRE, NewsAnnousVisbleAM, NewsAnnousVisbleDM)
	VALUES (@Title, @Body, @IsActive, @IsNewsAnnouncement, @CreatedBy, @CreatedDate, @IPAddress, 
	@Role,@RoleC,@RoleR, @NewsAnnousVisbleAd, @NewsAnnousVisbleDR, @NewsAnnousVisbleBR, @NewsAnnousVisbleLO,
	@NewsAnnousVisbleCU, @NewsAnnousVisbleRE, @NewsAnnousVisbleAM, @NewsAnnousVisbleDM)  

  

declare @lid varchar(50)
SET @lid = (SELECT TOP 1
	ID
FROM tblNewsAndAnnouncement
ORDER BY ID DESC)
 
  

END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_PayoffData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_PayoffData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Insert_PayoffData] 
(
@stringmode varchar(50),
@Requestid int,      
@Loannumber varchar(50),   
@RequestDate datetime,    
@EmailID varchar(50),    
@NeedtoKnow ntext
)
AS
BEGIN
	 INSERT INTO tblRequestPayoff (Loannumber,RequestDate,EmailID,NeedtoKnow)      
 VALUES      
  (@Loannumber,@RequestDate,@EmailID,@NeedtoKnow)  
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_PipeLine]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_PipeLine]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_Insert_PipeLine]
(
@Loan_No VARCHAR(MAX),
@Cond_Text VARCHAR(MAX),
@FileName VARCHAR(MAX),
@dttime VARCHAR(MAX),
@condrecid VARCHAR(MAX),
@ProlendImageDesc VARCHAR(MAX),
@Userid VARCHAR(MAX),
@SubmittedDate VARCHAR(MAX)
)
AS
BEGIN
Insert into tblCondText_History(Loan_No,Cond_Text,FileName,dttime,condrecid,ProlendImageDesc,Userid,SubmittedDate)
values
(@Loan_No,@Cond_Text,@FileName,@dttime,@condrecid,@ProlendImageDesc,@Userid,@SubmittedDate)

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_QuickLinks]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_QuickLinks]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Insert_QuickLinks] 
(
@stringmode varchar(50),
@ID int,      
@Linkname varchar(100),   
@Description varchar(1000),  
@Roles varchar(100)
)
AS
BEGIN
if(@stringmode = 'update')
update tblQuickLinks set Linkname=@Linkname,Description=@Description,Roles=@Roles where ID = @ID
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_Rate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_Rate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[sp_Insert_Rate] 
(
@stringmode varchar(50),
@RateID int,      
@strLoanType varchar(50),   
@IPoints int,    
@DecRate decimal(18,3),    
@DecAPR decimal(18,3),
@DecClosingCost decimal(18,2),
@CreatedDate datetime,   
@ModifiedDate datetime   

)
AS
BEGIN
if(@stringmode = 'insert')
 insert into tblrates(strLoanType,IPoints,DecRate,DecAPR,DecClosingCost,CreatedDate,ModifiedDate)
 VALUES      
 (@strLoanType,@IPoints,@DecRate,@DecAPR,@DecClosingCost,@CreatedDate,@ModifiedDate)  
else
update tblrates set strloantype = @StrLoanType,IPoints = @IPoints,DecRate =  @DecRate,DecAPR = @DecAPR,
               DecClosingCost = @DecClosingCost,ModifiedDate = getdate()
                where rateid = @RateID


END







GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Insert_RegisterNewCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Insert_RegisterNewCustomer]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Insert_RegisterNewCustomer]
(                 
                  
@UFirstName VARCHAR(50),                          
@ULastName  VARCHAR(50),                            
@UEmailId  VARCHAR(200) = null,             
@siteurl varchar(50),                           
@StateBelongTo nvarchar(100)=null,                          
@urole int,                     
@IsAdmin bit,                       
@IsActive bit,                        
@uidprolender  VARCHAR(200)=null,                            
@uparent int,              
@Mobile_Ph nvarchar(50),              
@LoginIdProlender  nvarchar(300),                  
@LoanNum  nvarchar(50),                   
@address nvarchar(300),            
@address2  varchar(100),            
@city  nvarchar(100),            
@state  nvarchar(100),            
@zip  nvarchar(100),    
@upassword varchar(150),                
@propertyaddr nvarchar(300),            
@MAID int,            
@MI varchar(10),            
@carrierid int,             
@TermsRealtor bit,            
@lat decimal(18,6),                
@lng decimal(18,6),                
@zlat decimal(18,6),                
@zlng decimal(18,6),                
@compName varchar(100)=null,                 
@E3Userid varchar(50)=null,   
@Userloginid varchar(50),
@PasswordExpDate nvarchar(100)                     
)                          
AS                          
BEGIN                      
                
   Insert into tblusers(ufirstname,siteurl,ulastname,uemailid,StateBelongTo,urole,isadmin,isactive,            
uidprolender,uparent,mobile_ph,loginidprolender,loannum,address,Address2,city,state,zip,upassword,            
propertyaddr,MAID,MI,carrierid, TermsRealtor,[lat],[lng],[zlat],[zlng],compName,E3Userid,Userloginid,PasswordExpDate)                 
    VALUES (@UFirstName,@siteurl,@ULastName,@UEmailId,@StateBelongTo,@urole,@isadmin,@isactive,            
 @uidprolender,@uparent,@mobile_ph,@loginidprolender,@loannum,@address,@address2,@city,@state,@zip           
,@upassword,@propertyaddr,@MAID,@MI,@carrierid,@TermsRealtor,@lat,@lng,@zlat,@zlng,@compName,@E3Userid,@Userloginid,@PasswordExpDate)                  
              
END 


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Insert_RegisterNewUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Insert_RegisterNewUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
-- =============================================            
-- Created By: Chandresh Patel          
-- Create Date : 10/04/2012            
-- Description: Procedure for Insert New User Registration           
-- =============================================                 
CREATE PROC [dbo].[SP_Insert_RegisterNewUser](                 
                  
@UFirstName VARCHAR(50),                          
@ULastName  VARCHAR(50),                            
@UEmailId  VARCHAR(200),             
@siteurl varchar(50),                           
@StateBelongTo nvarchar(100)=null,                          
@urole int,                     
@IsAdmin bit,                       
@IsActive bit,                        
@uidprolender  VARCHAR(200)=null,                            
@uparent int,              
@Mobile_Ph nvarchar(50),              
@LoginIdProlender  nvarchar(300),                  
@LoanNum  nvarchar(50),                   
@address nvarchar(300),            
@address2  varchar(100),            
@city  nvarchar(100),            
@state  nvarchar(100),            
@zip  nvarchar(100),            
@phonenum varchar(15),                   
@upassword varchar(150),                
@propertyaddr nvarchar(300),            
@MAID int,            
@MI varchar(10),            
@carrierid int,             
@TermsRealtor bit,            
@lat decimal(18,6),                
@lng decimal(18,6),                
@zlat decimal(18,6),                
@zlng decimal(18,6),                
@compName varchar(100)=null,                 
@E3Userid varchar(50)=null                        
)                          
AS                          
BEGIN                      
                
   Insert into tblusers(ufirstname,siteurl,ulastname,uemailid,StateBelongTo,urole,isadmin,isactive,            
uidprolender,uparent,mobile_ph,loginidprolender,loannum,address,Address2,city,state,zip,phonenum,upassword,            
propertyaddr,MAID,MI,carrierid, TermsRealtor,[lat],[lng],[zlat],[zlng],compName,E3Userid)                 
    VALUES (@UFirstName,@siteurl,@ULastName,@UEmailId,@StateBelongTo,@urole,@isadmin,@isactive,            
 @uidprolender,@uparent,@mobile_ph,@loginidprolender,@loannum,@address,@address2,@city,@state,@zip,            
@phonenum,@upassword,@propertyaddr,@MAID,@MI,@carrierid,@TermsRealtor,@lat,@lng,@zlat,@zlng,@compName,@E3Userid)                  
              
END 
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Insert_RegisterTMRUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Insert_RegisterTMRUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Created By: Chandresh Patel  
-- Create Date : 10/04/2012    
-- Description: Procedure for Insert Register TMR User   
-- =============================================       
CREATE PROC [dbo].[SP_Insert_RegisterTMRUser](         
@userid int,    
@StringMode char(10),    
@UFirstName VARCHAR(50),                  
@ULastName  VARCHAR(50),                    
@UEmailId  VARCHAR(200),     
@siteurl varchar(50)=NULL,                                
@urole bit,             
@IsAdmin bit,               
@IsActive bit,                
@Mobile_Ph nvarchar(50),     
@upassword varchar(200),        
@carrierid int,    
@RcvOnCell bit,    
@RcvOnEmail bit,    
@TMRAccActivation bit,    
@RcvExpire30 bit,    
@TMRRole nvarchar(50),    
@TMRLoanType nvarchar(50),    
@TMRTimeFrame nvarchar(50),    
@PropState varchar(50)=null               
)                  
AS                  
BEGIN              
   IF(@StringMode ='insert')         
  BEGIN             
  insert into tblusers(ufirstname,ulastname,uemailid,urole,isadmin,isactive,mobile_ph,    
  upassword,carrierid,RcvOnCell,RcvOnEmail,TMRAccActivation,RcvExpire30,TMRRole,TMRLoanType,    
  TMRTimeFrame,PropState)      
  VALUES (@UFirstName,@ULastName,@UEmailId,@urole,@isadmin,@isactive,    
  @mobile_ph,@upassword,@carrierid,    
  @RcvOnCell,@RcvOnEmail,@TMRAccActivation,@RcvExpire30,@TMRRole,@TMRLoanType,    
  @TMRTimeFrame,@PropState)          
  END    
 ELSE IF(@StringMode ='update')         
  BEGIN       
   update tblusers  set ufirstname=@UFirstName,ulastname=@ulastname,uemailid=@uemailid,urole=@urole,    
   isadmin=@isadmin,isactive=@isactive,mobile_ph=@mobile_ph,    
   upassword=@upassword,carrierid=@carrierid,RcvOnCell=@RcvOnCell,RcvOnEmail=@RcvOnEmail,    
   TMRAccActivation=@TMRAccActivation,RcvExpire30=@RcvExpire30,TMRRole=@TMRRole,    
   TMRLoanType=@TMRLoanType,    
   TMRTimeFrame=@TMRTimeFrame,PropState=@PropState where userid=@userid    
  END        
     
END   
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_Subscribe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_Subscribe]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert_Subscribe]
	-- Add the parameters for the stored procedure here
(
@Name nvarchar(100),
@EMail nvarchar(100),
@Comments nvarchar(2000)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Subscribe(Name,EMail,Comments) Values(@Name,@EMail,@Comments)

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Insert_Testimonial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Insert_Testimonial]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel
-- Create Date : 10/04/2012  
-- Description: Procedure for Insert Testimonial 
-- =============================================     
CREATE PROC [dbo].[SP_Insert_Testimonial](    
@UName varchar(50),   
@Testimonial char(1),  
@SubmittedDate datetime,  
@UserID int,  
@UpdatedDt datetime,  
@City varchar(50),    
@State varchar(50),   
@TestimonialType char(1))                
AS                
BEGIN           
  INSERT INTO tblTestimonials (UName,Testimonial,SubmittedDate,UserID,UpdatedDt,  
  City, State, TestimonialType)   
  values(@UName,@Testimonial,@SubmittedDate,@UserID,@UpdatedDt,  
  @City,@State,@TestimonialType)  
     
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_Testimonials]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_Testimonials]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Insert_Testimonials] 
(
@stringmode varchar(50),
@ID int,      
@IsPublish bit,   
@Testimionial ntext,    
@IsActive bit    
--@CreatedDate datetime
)
AS
BEGIN
if(@stringmode = 'insert')

 INSERT INTO tblPublicTestimonial (IsPublish,Testimionial,IsActive)      
 VALUES      
  (@IsPublish,@Testimionial,@IsActive)  

else if(@stringmode = 'update')

update tblPublicTestimonial set IsPublish =@IsPublish,Testimionial=@Testimionial where id = @id 
 

else if(@stringmode = 'delete')

delete from tblPublicTestimonial where id = @id


END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_Update]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_Insert_Update]
(
@record_id VARCHAR(MAX)
)
AS
BEGIN

update condits  set cond_recvd_date = Getdate() where record_id = @record_id
 
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Insert_Update_Forms]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Insert_Update_Forms]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel
-- Create Date : 10/04/2012  
-- Description: Procedure for  
-- =============================================  
     
CREATE PROC  [dbo].[SP_Insert_Update_Forms](       
@FormID int,         
@StringMode varchar(10),  
@Description VARCHAR(200),          
@FileName VARCHAR(200),                
@dttime  datetime,                  
@isnews  bit,                  
@CatID int        
)                
AS                
BEGIN            
 IF(@StringMode ='insert')       
          
	INSERT INTO tblForms(Description,[FileName],dttime,isnews,CateID)        
	VALUES (@Description,@FileName,@dttime,@isnews,@CatID)        
       
 ELSE IF(@StringMode ='update')       
       
	update tblForms      
	set Description=@Description,[FileName]= @FileName,    
	dttime =@dttime ,isnews=@isnews,   CateID=@CatID  
	where FormID=@FormID      
     
         
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_UserGuide]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_UserGuide]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_Insert_UserGuide] 
(
@stringmode varchar(50),
@ID int,      
@Filename varchar(100),   
@IsActive bit,  
@CreatedBy int
)
AS
BEGIN
 begin transaction
 if(@stringmode = 'insert')
Begin
    INSERT INTO tblUserGuide (Filename,IsActive,CreatedBy,CreatedDate)      
    VALUES (@Filename,@IsActive,@CreatedBy,getdate()) 
End
else if(@stringmode = 'show')
Begin
update tblUserGuide SET IsActive=@IsActive WHERE ID=@ID
update tblUserGuide set IsActive=0 WHERE ID <> @ID
End
else if(@stringmode = 'hide')
Begin
UPDATE tblUserGuide SET IsActive=@IsActive WHERE ID=@ID
End
  commit transaction
END





GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Insert_Users]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Insert_Users]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[SP_Insert_Users](     
@UserID int,       
@StringMode VARCHAR(50),        
@UFirstName VARCHAR(50),              
@ULastName  VARCHAR(50),                
@UEmailId  VARCHAR(200),                
@StateBelongTo nvarchar(100)=null,              
@urole int,         
@IsAdmin bit,           
@IsActive bit,                 
@ispublish bit,              
@uidprolender  VARCHAR(200)=null,                
@uparent int,              
@upassword  VARCHAR(200),         
@Mobile_Ph nvarchar(50),        
@LoginIdProlender  nvarchar(300),      
@LoanNum  nvarchar(50),       
@CarrierId int,      
@MAID int,      
@Shorturl varchar(50)=null,        
@Lead360 varchar(4000),      
@MI varchar(10),      
@TermsRealtor bit,      
@RealtorID int,      
@RealtorGroupID int,      
@E3Userid varchar(50)=null,        
@Office  VARCHAR(200)=null,          
@BranchOffice VARCHAR(200)=null        
)              
AS              
BEGIN          
 IF(@StringMode ='insert')     
  BEGIN        
  INSERT INTO tblusers(ufirstname,ulastname,uemailid,StateBelongTo,urole,isadmin,isactive,ispublish,uidprolender,uparent,upassword,mobile_ph,loginidprolender,loannum,carrierid,MAID,shorturl,lead360,MI, TermsRealtor,RealtorID,RealtorGroupID,E3Userid,  
  office,BranchOffice)      
  VALUES (@UFirstName,@ULastName,@UEmailId,@StateBelongTo,@urole,@isadmin,@isactive,@ispublish,@uidprolender,@uparent,      
  @upassword,@mobile_ph,@loginidprolender,@loannum,@carrierid,@MAID,@shorturl,@lead360,      
  @MI,@TermsRealtor,@RealtorID,@RealtorGroupID,@E3Userid,@Office,@BranchOffice)      
  END    
 ELSE IF(@StringMode ='update')     
  BEGIN     
  update tblusers    
  set ufirstname=@UFirstName,ulastname=@ULastName,uemailid=@UEmailId,StateBelongTo=@StateBelongTo,    
  urole=@urole,isadmin=@IsAdmin,isactive=@isactive,ispublish=ispublish,    
  uidprolender=@uidprolender,uparent=@uparent,upassword=@upassword,mobile_ph=@mobile_ph,    
  loginidprolender=@loginidprolender,loannum=@loannum,carrierid=@carrierid,MAID=@MAID,    
  shorturl=@shorturl,lead360=@lead360,MI=@MI, TermsRealtor=@TermsRealtor,RealtorID=@RealtorID,    
  RealtorGroupID=@RealtorGroupID,E3Userid=@E3Userid,office=@office,    
  BranchOffice =@BranchOffice    
  where userid=@UserID    
  END    
else if(@stringmode = 'delete')   
  Delete From tblusers Where userid =@UserID         
END          
      

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Insert_Users_Team]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Insert_Users_Team]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel
-- Create Date : 10/04/2012  
-- Description: Procedure for Insert User For Team  
-- =============================================  
     
CREATE PROC [dbo].[SP_Insert_Users_Team](          
@UFirstName VARCHAR(50),                  
@ULastName  VARCHAR(50),                    
@UEmailId  VARCHAR(200),                              
@urole bit,             
@IsAdmin bit,               
@IsActive bit,                                   
@uidprolender  VARCHAR(200)=null,                    
@uparent int,                  
@upassword  VARCHAR(200),             
@Mobile_Ph nvarchar(50),            
@LoginIdProlender  nvarchar(300),          
@LoanNum  nvarchar(50),           
@CarrierId int,        
@experience  VARCHAR(100),    
  @phonenum  nVARCHAR(20),    
@address nvarchar(300),      
@address2 varchar(100),    
@state nvarchar(10),    
@zip nvarchar(50),    
@fax nvarchar(50),    
@photoname nvarchar(50),    
@MAID int,        
@bio ntext,    
@areaofexpertise ntext,    
@ClientTestimonial ntext,    
@RealtorTestimonial ntext,    
@city nvarchar(100),     
@MI varchar(10)  
--, @userid INT OUTPUT     
)                  
AS                  
BEGIN              
            
  Insert into tblusers(ufirstname,ulastname,uemailid,urole,isadmin,isactive,uidprolender,uparent,upassword,    
mobile_ph,loginidprolender,loannum,carrierid,experience,phonenum,address,address2,state,zip,fax,photoname,    
MAID,bio,areaofexpertise,ClientTestimonial,RealtorTestimonial,city,MI)          
    VALUES (@UFirstName,@ULastName,@UEmailId,@urole,@isadmin,@isactive,@uidprolender,@uparent,          
    @upassword,@mobile_ph,@loginidprolender,@loannum,@carrierid,@experience,@phonenum,@address,     
@address2,@state,@zip,@fax,@photoname,@MAID,@bio,@areaofexpertise,@ClientTestimonial,    
@RealtorTestimonial,@city,@MI)          
         
--set @userid= @@identity    
     
           
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_VoluntarySelfId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_VoluntarySelfId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Insert_VoluntarySelfId] 
(
@stringmode varchar(50),
@AppId int,
@PosApplyingFor varchar(100),
@HowYouReferred varchar(100),
@Gender varchar(50),
@HispanicOLatino bit,
@White bit,
@BlackAfrAmc bit,
@Hawiian bit,  
@Asian bit,
@AmericanIndian bit,
@TwoOrMoreRaces bit,
@DisabledVeteran bit,
@VietnamVeteran bit,
@OtherVeteran bit,
@NewlyVeteran bit,
@Veteran bit,
@NotApplicable bit,
@OtherStatus bit
)
AS
BEGIN
	 INSERT INTO tblVoluntarySelfId(AppId,PosApplyingFor,HowYouReferred,Gender,HispanicOLatino,White,BlackAfrAmc,Hawiian,Asian,AmericanIndian,TwoOrMoreRaces,DisabledVeteran,VietnamVeteran,OtherVeteran,NewlyVeteran,Veteran,NotApplicable,OtherStatus)
     VALUES (@AppId,@PosApplyingFor,@HowYouReferred,@Gender,@HispanicOLatino,@White,@BlackAfrAmc,@Hawiian,@Asian,@AmericanIndian,@TwoOrMoreRaces,@DisabledVeteran,@VietnamVeteran,@OtherVeteran,@NewlyVeteran,@Veteran,@NotApplicable,@OtherStatus)  
END









GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertAddDocs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertAddDocs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertAddDocs]
(
@loan_no VARCHAR(MAX),
@cond_text VARCHAR(MAX),
@FileName VARCHAR(MAX),
@dttime VARCHAR(MAX),
@condrecid VARCHAR(MAX),
@ProlendImageDesc VARCHAR(MAX),
@Userid VARCHAR(MAX)
)
AS
BEGIN

insert into tblAdditionalUWDoc(loan_no,cond_text,FileName,dttime,condrecid,ProlendImageDesc,Userid) Values
(@loan_no,@cond_text,@FileName,@dttime,@condrecid,@ProlendImageDesc,@Userid)
 
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertAlert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertAlert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertAlert]
(

@UserId varchar (max),
@LoanNo varchar (max),
@IsSMSPaymentReminderAlert varchar (max),
@IsSMSPaymentPostedAlert varchar (max),
@IsSMSPaymentPastDue varchar (max),
@IsEmailPaymentReminderAlert varchar (max),
@IsEmailPaymentPostedAlert varchar (max),
@IsEmailPaymentPastDue varchar (max),
@IsPaymentReminderAlert varchar (max),
@IsPaymentPostedReminderAlert varchar (max),
@IsPaymentPastDue varchar (max),
@PaymentReminderDay varchar (max),
@PaymentPastDueReminderDay varchar (max)

)
AS
BEGIN
insert into tblServiceInfo (UserId,LoanNo,IsSMSPaymentReminderAlert,IsSMSPaymentPostedAlert,IsSMSPaymentPastDue,
IsEmailPaymentReminderAlert,IsEmailPaymentPostedAlert,IsEmailPaymentPastDue,IsPaymentReminderAlert,IsPaymentPostedReminderAlert,
IsPaymentPastDue,PaymentReminderDay,PaymentPastDueReminderDay) values 
(@UserId,@LoanNo,@IsSMSPaymentReminderAlert,@IsSMSPaymentPostedAlert,@IsSMSPaymentPastDue,@IsEmailPaymentReminderAlert,@IsEmailPaymentPostedAlert,@IsEmailPaymentPastDue,@IsPaymentReminderAlert,@IsPaymentPostedReminderAlert,@IsPaymentPastDue,@PaymentReminderDay,@PaymentPastDueReminderDay)
     
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertBankRateNew_tblLoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertBankRateNew_tblLoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
-- Author      : Joyal F. Macwan
-- Create date : 10-05-2013 ( 4 : 00 P.M.)
-- Description : insert data for tblLoanApplication table
-- =====================================================

CREATE Procedure [dbo].[sp_InsertBankRateNew_tblLoanApplication]  
(   
 @HometypeID int=null,  
 @LoanAmount money=null,  
 @PropertyTypeID int=null,   
 @AppFName nvarchar(100)=null,  
 @AppLName nvarchar(100)=null,  
 @AppPrimaryPhone nvarchar(100),  
 @AppEmail nvarchar(100)=null,  
 @IsWorkingWithRealtor bit  
)  
  
AS  
  
SET NOCOUNT OFF  
   
  begin  
   insert into tblLoanApplication  
   (  
    HometypeID,LoanAmount,PropertyTypeID,AppFName,AppLName,AppPrimaryPhone,AppEmail,IsWorkingWithRealtor  
   )  
   values  
   (  
    @HometypeID,@LoanAmount,@PropertyTypeID,@AppFName,@AppLName,@AppPrimaryPhone,@AppEmail,@IsWorkingWithRealtor  
   )     
  end  
  
SET NOCOUNT ON
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertBlitzdocsLoanRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertBlitzdocsLoanRegister]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertBlitzdocsLoanRegister]
	-- Add the parameters for the stored procedure here
(
@Comments varchar(500),
@userid int,
@DocName varchar(100),
@loan_no char(15) 
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
Insert into tblloandoc(Comments,userid,DocName,loan_no) Values(@Comments,@userid,@DocName,@loan_no)


END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertCategory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertCategory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertCategory]
(
    @CateName varchar(50)
)
AS
BEGIN
	 Insert into tblCategories(CateName) Values 
    ( @CateName )

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertCreateLoanType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertCreateLoanType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nitin>
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertCreateLoanType] 
(
@Loantype varchar(MAX)
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into tblLoanType (LoanType) 
	Values(@Loantype)
END

--------------------------------------------------------------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[sp_InsertCreatePackageId]    Script Date: 04/12/2013 17:44:25 ******/
SET ANSI_NULLS ON

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertCreatePackageId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertCreatePackageId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nitin>
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertCreatePackageId] 
(
@PackageId int
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into tblPackageTypeId (PackageId) 
	Values(@PackageId)
END

--------------------------------------------------------------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[sp_getLoanType]    Script Date: 04/12/2013 17:45:14 ******/
SET ANSI_NULLS ON

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertFaqs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertFaqs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create  Procedure [dbo].[sp_InsertFaqs]
(
@Question [varchar](1000),
@Answer [text],
@ModuleName [varchar](100),
@Published [bit],
@DisplayOrder int
)
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

Insert into dbo.tblfaqs
(
Question,
Answer,
ModuleName,
Published,
DisplayOrder,
CreatedDate,
ModifiedDate
)
Values
(
@Question,
@Answer,
@ModuleName,
@Published,
@DisplayOrder,
Getdate(),
Getdate()
)

End


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertFormRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertFormRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertFormRoles]
(
@formID int ,@uroleid int
)
AS

BEGIN
	INSERT INTO tblformRoles (formID,uroleid)  values (@formID,@uroleid)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertHomeOwner]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertHomeOwner]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_InsertHomeOwner]     
(    
@Description varchar(MAX),    
@FileName varchar(MAX),    
@Loantype varchar(MAX),    
@Packageid int    
)    
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
  Declare @lType varchar(max)  
  Select @lType = loantypeid from dbo.tblLoanType where id = @Loantype  
    -- Insert statements for procedure here    
 Insert into tblFormsHomeowner(Description,FileName,dttime,Loantype,Packageid)     
 Values(@Description,@FileName,getdate(),@lType,@Packageid)    
END
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertInForm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertInForm]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertInForm]
(
@Description varchar(200),
@FileName varchar(200),
@dttime datetime,
@isnews bit,
@CateID int
)
AS

BEGIN
	 Insert into tblForms(Description,FileName,dttime,isnews,CateID) Values 

(@Description,@FileName,@dttime,@isnews,@CateID)

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLoanDocs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLoanDocs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertLoanDocs]
(
@Loan_No VARCHAR(MAX),
@docs_text VARCHAR(MAX),
@FileName VARCHAR(MAX),
@dttime VARCHAR(MAX)
)
AS
BEGIN
Insert into tblLoanDocs(Loan_No,docs_text,FileName,dttime) Values
(@Loan_No,@docs_text,@FileName,@dttime)
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLoanDocs_loandoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLoanDocs_loandoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertLoanDocs_loandoc]

(
@uloanregid int ,
@Docname varchar(100),
@Comments varchar(500)
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into tblloandoc(uloanregid,Docname,Comments)
Values(@uloanregid,@Docname,@Comments)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLoanDocsSubmitLoans]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLoanDocsSubmitLoans]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  <Author,,Name>      
-- Create date: <Create Date,,>      
-- Description: <Description,,>      
-- =============================================      
CREATE PROCEDURE sp_InsertLoanDocsSubmitLoans      
(      
 @BorrLastName varchar(200),      
 @UserID int,      
 @DocName varchar(200),      
 @LoanNumber varchar(20)      
)      
AS      
BEGIN      
 Insert Into       
  tblloandoc      
    (Comments,userid,DocName,loan_no)       
  Values      
    (@BorrLastName,@UserID,@DocName,@LoanNumber)      
END 

--SELECT chkSubmitClosingPackage, *FROM dbo.tblUsers WHERE userloginID LIKE '%jason123%'

--UPDATE tblUsers SET chkSubmitClosingPackage=1 WHERE userid=858
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLoanIdNew_LoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLoanIdNew_LoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertLoanIdNew_LoanApplication]
	-- Add the parameters for the stored procedure here
(
	@HometypeID int ,
			@PropertyTypeID int, 
			@StateID int, 
			@LoanReasonID int,

			@LoanAmount money, 
			@MAName varchar(100),
			@AppFName nvarchar(100), 
			@AppLName nvarchar(100), 
			@AppStateID int, 
			@AppZip nvarchar(100), 
			@AppPrimaryPhone nvarchar(100), 
			@AppEmail nvarchar(100), 
			@vCity varchar(100), 
 			@vDownPayAmount nvarchar(100), 
			@vAddressLine1 nvarchar(500),
			@AssignedLO varchar(250),
			@CompIP varchar(50)	
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblLoanApplication

(

	
			HometypeID ,
			PropertyTypeID ,
			StateID , 
			LoanReasonID ,

			LoanAmount , 
			MAName ,
			AppFName , 
			AppLName , 
			AppStateID , 
			AppZip , 
			AppPrimaryPhone , 
			AppEmail , 
			vCity , 
 			vDownPayAmount , 
			vAddressLine1 ,
			AssignedLO ,
			CompIP 
)
values
(

	
			@HometypeID ,
			@PropertyTypeID ,
			@StateID , 
			@LoanReasonID ,

			@LoanAmount , 
			@MAName ,
			@AppFName , 
			@AppLName , 
			@AppStateID , 
			@AppZip , 
			@AppPrimaryPhone , 
			@AppEmail , 
			@vCity , 
 			@vDownPayAmount , 
			@vAddressLine1 ,
			@AssignedLO ,
			@CompIP 
)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLoanRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLoanRegister]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertLoanRegister]

(
@borrLast varchar(100),
@transType int,
@loanAmt numeric(14,2),
@lockStatus varchar(20),
@loanPgm varchar(20),
@estCloseDate datetime,
@userid int,
@filetype varchar(50),
@filename varchar(100) 
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into tblLoanReg
(borrLast,transType,loanAmt,lockStatus,loanPgm,estCloseDate,userid,filetype,filename) 

Values(@borrLast,@transType,@loanAmt,@lockStatus,@loanPgm,@estCloseDate,@userid,@filetype,@filename)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLoanSuspendedDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLoanSuspendedDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertLoanSuspendedDoc]
(
@LoanNo VARCHAR(MAX),
@FeeSheet VARCHAR(MAX),
@userid VARCHAR(MAX),
@Comments VARCHAR(MAX)
)
AS
BEGIN
insert into tblScheduleClosing (LoanNo, SchDate, FeeSheet,userid,SubmitedDate, Comments,IsFromLoanSuspendedDoc) values 
(@LoanNo, '',@FeeSheet, @userid ,getdate(),@Comments,1)

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertOffices]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertOffices]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsOffice.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertOffices]
(
@lt_office_id varchar(Max),
@prouid varchar(Max),
@userid varchar(Max)

)
AS
BEGIN

	 Insert into tblOffices(lt_office_id,prouid,userid) Values 
(@lt_office_id,@prouid,@userid)   
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertPayoffInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertPayoffInsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertPayoffInsert]
(
@userid varchar (max),
@payoffdate varchar (max),
@payoffreasonid varchar (max),
@isreadytocontact varchar (max)
)
AS
BEGIN
insert into tblpayoffstatement(userid,payoffdate,payoffreasonid,isreadytocontact)
values (@userid,@payoffdate,@payoffreasonid,@isreadytocontact)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertPayoffUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertPayoffUpdate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertPayoffUpdate]
(
@userid varchar (max),
@payoffdate varchar (max),
@payoffreasonid varchar (max),
@isreadytocontact varchar (max)
)
AS
BEGIN
update tblpayoffstatement set payoffdate=@payoffdate,payoffreasonid=@payoffreasonid,isreadytocontact=@isreadytocontact where
userid=@userid

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertPropDetailImage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertPropDetailImage]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertPropDetailImage]
	-- Add the parameters for the stored procedure here


(@PropId int,@PropImageName nvarchar(50) ,@PropDesc nvarchar(50) )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

insert into tblPropertyImage(PropId,PropImageName,PropDesc)

values(@PropId,@PropImageName,@PropDesc)
		
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertPropDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertPropDetails]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertPropDetails]
	-- Add the parameters for the stored procedure here
(
@RealtorID int,
@PropStatus varchar(50),
@ListingType varchar(50),
@Bedrooms int,
@baths decimal(18,1),
@amount decimal(18,0) ,
@street varchar(50),
@state varchar(50),
@city varchar(50),
@zipcode varchar(50),
@yearbuilt int, 
@PropDesc ntext,
@lat decimal(18,6) ,
@lng decimal(18,6) ,
@Area int,
@isactive bit,
@ImprovementFile varchar(50)
) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	insert into tblProperties(RealtorID,PropStatus,ListingType,Bedrooms,baths,amount,street,state,city,zipcode,yearbuilt, PropDesc,lat,lng,Area,isactive,ImprovementFile)

values(@RealtorID,@PropStatus,@ListingType,@Bedrooms,@baths,@amount,@street,@state,@city,@zipcode,@yearbuilt, @PropDesc,@lat,@lng,@Area,@isactive,@ImprovementFile)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertPropDetails1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertPropDetails1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertPropDetails1]
	-- Add the parameters for the stored procedure here
(
@RealtorID int,
@PropStatus varchar(50),
@ListingType varchar(50),
@Bedrooms int,
@baths decimal(18,1),
@amount decimal(18,0) ,
@street varchar(50),
@state varchar(50),
@city varchar(50),
@zipcode varchar(50),
@yearbuilt int, 
@PropDesc ntext,
@Area int,
@isactive bit
) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

		insert into tblProperties(RealtorID,PropStatus,ListingType,Bedrooms,baths,amount,street,state,city,zipcode,yearbuilt, PropDesc, area, isactive) 
					values(@RealtorID,@PropStatus,@ListingType,@Bedrooms,@baths,@amount,@street,@state,@city,@zipcode,@yearbuilt, @PropDesc,@Area,@isactive)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertRate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertRate]
(
@strLoanType varchar(Max),
@IPoints varchar(Max),
@DecRate varchar(Max),
@DecAPR varchar(Max),
@DecClosingCost varchar(Max),
@CreatedDate varchar(Max),
@ModifiedDate varchar(Max)
)
AS
BEGIN

insert into tblrates(strLoanType,IPoints,DecRate,DecAPR,DecClosingCost,CreatedDate,ModifiedDate)
values (@strLoanType,@IPoints,@DecRate,@DecAPR,@DecClosingCost,@CreatedDate,@ModifiedDate)

END
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InserttblUserPasswordHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InserttblUserPasswordHistory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE PROCEDURE [dbo].[sp_InserttblUserPasswordHistory]
(
@UserID int,
@Password varchar(200)
)
AS
BEGIN
	
	SET NOCOUNT ON;

INSERT INTO tblUserPasswordHistory(UserID,Password) 
Values(@UserID,@Password)
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InserttblUserPasswordHistoryNew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InserttblUserPasswordHistoryNew]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shwetal
-- Create date: 03/12/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE PROCEDURE [dbo].[sp_InserttblUserPasswordHistoryNew]
(
@UserID int
)
AS
BEGIN
SET NOCOUNT ON;

INSERT INTO tblUserPasswordHistory(UserID,Password) 
SELECT @UserID, upassword from tblUsers Where UserID = @UserID

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sp_InsertUpdateHMDACust]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Sp_InsertUpdateHMDACust]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Author,,Name>    
-- Create date: <28/12/2012>    
-- Description: <Insert/Update HMDA customfield into mwlcustomfield  table>    
-- =============================================    
CREATE PROCEDURE [dbo].[Sp_InsertUpdateHMDACust]     
 @Loannumber nvarchar(500),    
 @AUCustFieldDef_ID nvarchar(500)='',    
 @ADCustFieldDef_ID nvarchar(500)=''    
    
AS    
BEGIN    
 Declare  @LoanAppId varchar(500)    
 set @LoanAppId = (select id from mwlloanapp where loannumber=@Loannumber)    
 print @LoanAppId    
 -- HMDA Aging Date     
 if not exists(select * From mwlcustomfield where CustFieldDef_ID in (@ADCustFieldDef_ID) and  LoanApp_ID =@LoanAppId)        
 BEGIN        
 INSERT INTO mwlcustomfield                       
 (ID,LoanApp_ID,CustFieldDef_ID,Stringvalue,datevalue)                      
  VALUES                      
 (replace(newid(),'-',''),@LoanAppId,@ADCustFieldDef_ID,'',getdate())              
    
 END        
 ELSE        
 BEGIN        
  UPDATE mwlcustomfield SET DateValue=getdate() WHERE CustFieldDef_ID in (@ADCustFieldDef_ID) and  LoanApp_ID =@LoanAppId    
 END     
    
 --HMDA Aging user    
 if not exists(select * From mwlcustomfield where CustFieldDef_ID in (@AUCustFieldDef_ID) and  LoanApp_ID =@LoanAppId)        
 BEGIN        
 INSERT INTO mwlcustomfield                       
 (ID,LoanApp_ID,CustFieldDef_ID,Stringvalue)                      
  VALUES                      
 (replace(newid(),'-',''),@LoanAppId,@AUCustFieldDef_ID,'OLIE')              
    
 END        
 ELSE        
 BEGIN        
 UPDATE mwlcustomfield SET Stringvalue='OLIE' WHERE CustFieldDef_ID in (@AUCustFieldDef_ID) and  LoanApp_ID =@LoanAppId        
 END        
    
END
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertUserRegistration]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertUserRegistration]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsUserRegistrationE3.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertUserRegistration]
(
@ufirstname varchar(Max),
@ulastname varchar(Max),
@uemailid varchar(Max),
@urole varchar(Max),
@isadmin bit,
@isactive bit,
@uidprolender varchar(Max),
@uparent varchar(Max),
@upassword varchar(Max),
@mobile_ph varchar(Max),
@loginidprolender varchar(Max),
@carrierid varchar(Max)

)
AS
BEGIN
	 Insert into tblusers(ufirstname,ulastname,uemailid,urole,isadmin,isactive,uidprolender,uparent,upassword,mobile_ph,loginidprolender,carrierid) Values
(@ufirstname,@ulastname,@uemailid,@urole,@isadmin,@isactive,@uidprolender,@uparent,@upassword,@mobile_ph,@loginidprolender,@carrierid)
                
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IsAlertRegistered]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IsAlertRegistered]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_IsAlertRegistered]
(
@userid varchar(Max)
)
AS
BEGIN
select * from tblServiceInfo where userid = @userid
     
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IsAlertRegisteredForLoan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IsAlertRegisteredForLoan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_IsAlertRegisteredForLoan]
(
@userid varchar(Max),
@loanno varchar(Max)
)
AS
BEGIN
select * from tblServiceInfo where userid = @userid and loanno = @loanno  
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IsRecordExists]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IsRecordExists]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_IsRecordExists]
(
@userid varchar (max)
)
AS
BEGIN
 select * from tblpayoffstatement where 
userid=@userid

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_LoanApplication]         
(        
@LoanReasonID int ,        
@HometypeID int,        
@PropertyTypeID int,        
@LoanAmount money ,        
@DownPaymentAmt varchar(100)=null ,        
@StateID int ,        
@AppFName nvarchar(100),        
@AppLName nvarchar(100) ,        
@AppEmail nvarchar(100),        
@AddLine1 varchar(500)=null ,        
@City varchar(100)=null ,        
@AppStateID int,        
@AppZip nvarchar(100),        
@AppPrimaryPhone nvarchar(100),        
@MAName varchar(100),        
@AssignedLO varchar(250)=null,        
@CompIP varchar(50)        
)        
AS        
BEGIN        
       INSERT INTO tblLoanApplication(HometypeID,PropertyTypeID,LoanAmount,vDownPayAmount,LoanReasonID,StateID,AppFName,AppLName,AppEmail,vAddressLine1,vCity,AppStateID,AppZip,AppPrimaryPhone,MAName,AssignedLO, CompIP)              
 VALUES              
  (@HometypeID,@PropertyTypeID,@LoanAmount,@DownPaymentAmt,@LoanReasonID,@StateID,@AppFName,@AppLName,
  @AppEmail,@AddLine1,@City,@AppStateID,@AppZip,@AppPrimaryPhone,@MAName,@AssignedLO,@CompIP)          
END 
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MyInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MyInsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsUserRegistrationE3.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_MyInsert]
(
@ufirstname varchar(Max),
@ulastname varchar(Max),
@uemailid varchar(Max),
@urole varchar(Max),
@isadmin bit,
@isactive bit,
@uidprolender varchar(Max),
@uparent varchar(Max),
@upassword varchar(Max),
@mobile_ph varchar(Max),
@loginidprolender varchar(Max),
@carrierid varchar(Max)

)
AS
BEGIN

	 Insert into tblusers(ufirstname,ulastname,uemailid,urole,isadmin,isactive,uidprolender,uparent,upassword,mobile_ph,loginidprolender,carrierid) Values
(@ufirstname,@ulastname,@uemailid,@urole,@isadmin,@isactive,@uidprolender,@uparent,@upassword,@mobile_ph,@loginidprolender,@carrierid)
                
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PNPaymentPastDue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PNPaymentPastDue]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Harsh Pandit
-- Create date: 25 March 2010
-- Description:	to get data for Payment Past Due Reminder
-- modify : Vipul Thakkar
-- Date : 05 OCT 2011
-- =============================================
CREATE PROCEDURE [dbo].[sp_PNPaymentPastDue] 
AS
BEGIN
	SET NOCOUNT ON;
select convert(varchar(10), dateadd(day,- PaymentPastDueReminderDay * 1 ,dbo.GetNotificationDueDate() ),101)as PaymentPastDueNotificationDate
, PaymentPastDueReminderDay, 
dbo.GetNotificationDueDate() as NotificationDuedateforMonth , record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,   serviceinfo.userid,
	isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt,  loanid,convert(varchar, duedate, 1) as duedate,  
	DATEDIFF(DAY,getdate(),duedate) as pastDueDays,  IsSMSPaymentPastDue,IsEmailPaymentPastDue,IsPaymentPastDue,
	PaymentPastDueReminderDay,PaymentPastDueDate,    
	mobile_ph, carrierid, (select carrieremailsms from carrier where id = carrierid) as smsgateway
from [winserver].[service].dbo.loan     
	join tblServiceInfo as serviceInfo on loan.loanid = serviceInfo.loanno    
	join tblUsers as users on users.userid = serviceinfo.userid    
--where 
where 
-- loanid='0000106071'
	--DATEDIFF(DAY,getdate(),duedate) <= 0    
	 IsPaymentPastDue = 1   and
	 dateadd(day,- PaymentPastDueReminderDay * 1 ,dbo.GetNotificationDueDate()) = convert(varchar(10), getdate(),101)
	--and DATEDIFF(DAY,getdate(),duedate) = (PaymentPastDueReminderDay * -1)
	and DATEDIFF(DAY,lastpmtRecvdDate,duedate) > datediff(day,getdate(),dateadd(month,1,getdate()))
and  month(Duedate)=month(getDate()) and year(DueDate)=year(getdate())
---- select record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,   serviceinfo.userid,
----isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt,  loanid,convert(varchar, lastpmtRecvdDate, 1) as lastpmtRecvdDate,  
----DATEDIFF(DAY,getdate(),lastpmtRecvdDate) as pastDueDays,  IsSMSPaymentPastDue,IsEmailPaymentPastDue,IsPaymentPastDue,
----PaymentPastDueReminderDay,PaymentPastDueDate,    
----mobile_ph, carrierid,
----	(select carrieremailsms from carrier where id = carrierid) as smsgateway
----from [WINSERVER].[service].dbo.loan     
----join tblServiceInfo as serviceInfo on loan.loanid = serviceInfo.loanno    
----join tblUsers as users on users.userid = serviceinfo.userid    
----where (month(PaymentPastDueDate) <> month(getdate()) or PaymentPastDueDate is null)     
----and DATEDIFF(DAY,getdate(),lastpmtRecvdDate) <= 0    
----and IsPaymentPastDue = 1   
----and DATEDIFF(DAY,getdate(),lastpmtRecvdDate) >= (PaymentPastDueReminderDay * -1)

	
--	select  record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,   serviceinfo.userid,  
--	isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt,  loanid,convert(varchar, lastpmtRecvdDate, 1) as lastpmtRecvdDate,    
--	DATEDIFF(DAY,getdate(),lastpmtRecvdDate) as pastDueDays,  IsSMSPaymentPastDue,IsEmailPaymentPastDue,  IsPaymentPastDue,
--	PaymentPastDueReminderDay,PaymentPastDueDate ,    
--	mobile_ph, carrierid,
--	(select carrieremailsms from carrier where id = carrierid) as smsgateway     
--	from [WINSERVER].[service].dbo.loan       
--	join tblServiceInfo as serviceInfo on loan.loanid = serviceInfo.loanno      
--	join tblUsers as users on users.userid = serviceinfo.userid   
--	where
--	DATEADD(DAY,CAST(ISNULL(PaymentPastDueReminderDay,0) AS INT),ISNULL(duedate,GETDATE()-CAST(ISNULL(PaymentPastDueReminderDay,0)  AS INT))) = GETDATE()  
--	AND IsPaymentPastDue = 1  
--	and  month(lastPmtRecvdDate) < month(getdate()) and year(lastPmtRecvdDate) = year(getdate())
--	and  DATEADD(DAY,CAST(ISNULL(PaymentPastDueReminderDay,0) AS INT),ISNULL(duedate,GETDATE()-CAST(ISNULL(PaymentPastDueReminderDay,0)  AS INT))) <> PaymentPastDueDate

END






GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PNPaymentPastDueOld]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PNPaymentPastDueOld]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Harsh Pandit
-- Create date: 25 March 2010
-- Modified date: 29 April 2010
-- Description:	to get data for Payment Past Due Reminder
-- =============================================
CREATE PROCEDURE [dbo].[sp_PNPaymentPastDue] 
AS
BEGIN
	SET NOCOUNT ON;

select record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,   serviceinfo.userid,
	isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt,  loanid,convert(varchar, duedate, 1) as duedate,  
	DATEDIFF(DAY,getdate(),duedate) as pastDueDays,  IsSMSPaymentPastDue,IsEmailPaymentPastDue,IsPaymentPastDue,
	PaymentPastDueReminderDay,PaymentPastDueDate,    
	mobile_ph, carrierid, (select carrieremailsms from carrier where id = carrierid) as smsgateway
from [winserver].[service].dbo.loan     
	join tblServiceInfo as serviceInfo on loan.loanid = serviceInfo.loanno    
	join tblUsers as users on users.userid = serviceinfo.userid    
where 
	DATEDIFF(DAY,getdate(),duedate) <= 0    
	and IsPaymentPastDue = 1   
	and DATEDIFF(DAY,getdate(),duedate) = (PaymentPastDueReminderDay * -1)
	and DATEDIFF(DAY,lastpmtRecvdDate,duedate) > datediff(day,getdate(),dateadd(month,1,getdate()))
	and (month(PaymentPastDueDate) <> month(getdate()) or PaymentPastDueDate is null)


-- select record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,   serviceinfo.userid,
--isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt,  loanid,convert(varchar, lastpmtRecvdDate, 1) as lastpmtRecvdDate,  
--DATEDIFF(DAY,getdate(),lastpmtRecvdDate) as pastDueDays,  IsSMSPaymentPastDue,IsEmailPaymentPastDue,IsPaymentPastDue,
--PaymentPastDueReminderDay,PaymentPastDueDate,    
--mobile_ph, carrierid,
--	(select carrieremailsms from carrier where id = carrierid) as smsgateway
--from [WINSERVER].[service].dbo.loan     
--join tblServiceInfo as serviceInfo on loan.loanid = serviceInfo.loanno    
--join tblUsers as users on users.userid = serviceinfo.userid    
--where (month(PaymentPastDueDate) <> month(getdate()) or PaymentPastDueDate is null)     
--and DATEDIFF(DAY,getdate(),lastpmtRecvdDate) <= 0    
--and IsPaymentPastDue = 1   
--and DATEDIFF(DAY,getdate(),lastpmtRecvdDate) >= (PaymentPastDueReminderDay * -1)
--
	
--	select  record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,   serviceinfo.userid,  
--	isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt,  loanid,convert(varchar, lastpmtRecvdDate, 1) as lastpmtRecvdDate,    
--	DATEDIFF(DAY,getdate(),lastpmtRecvdDate) as pastDueDays,  IsSMSPaymentPastDue,IsEmailPaymentPastDue,  IsPaymentPastDue,
--	PaymentPastDueReminderDay,PaymentPastDueDate ,    
--	mobile_ph, carrierid,
--	(select carrieremailsms from carrier where id = carrierid) as smsgateway     
--	from [WINSERVER].[service].dbo.loan       
--	join tblServiceInfo as serviceInfo on loan.loanid = serviceInfo.loanno      
--	join tblUsers as users on users.userid = serviceinfo.userid   
--	where
--	DATEADD(DAY,CAST(ISNULL(PaymentPastDueReminderDay,0) AS INT),ISNULL(duedate,GETDATE()-CAST(ISNULL(PaymentPastDueReminderDay,0)  AS INT))) = GETDATE()  
--	AND IsPaymentPastDue = 1  
--	and  month(lastPmtRecvdDate) < month(getdate()) and year(lastPmtRecvdDate) = year(getdate())
--	and  DATEADD(DAY,CAST(ISNULL(PaymentPastDueReminderDay,0) AS INT),ISNULL(duedate,GETDATE()-CAST(ISNULL(PaymentPastDueReminderDay,0)  AS INT))) <> PaymentPastDueDate

END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PNPaymentPostedReminder]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PNPaymentPostedReminder]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Harsh Pandit
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_PNPaymentPostedReminder] 
	--@LoanNo varchar(50)
AS
BEGIN
	SET NOCOUNT ON;
		select * from (select record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,loanno,
            (
                  select sum(TransactionAmt) 
                  from [WINSERVER].[service].dbo.history
                  join [WINSERVER].[service].dbo.TransactionCode on TransactionCode.code = history.TransactionCode
                  where loanid = loanno
                  and (TransactionCode = 211 or TransactionCode = 210 or TransactionCode = 220)
                  and TransactionDate = (select  top 1 TransactionDate 
                  from [WINSERVER].[service].dbo.history
                  join [WINSERVER].[service].dbo.TransactionCode on TransactionCode.code = history.TransactionCode
                  where loanid = loanno
                  and (TransactionCode = 211 or TransactionCode = 210 or TransactionCode = 220)         
                  order by TransactionDate DESC )
                  --order by loanid,TransactionDate DESC
            ) as TransactionAmt, 
            PaymentPostedReminderdate,IsSMSPaymentPostedAlert,IsEmailPaymentPostedAlert,IsPaymentPostedReminderAlert,
        PaymentPostedReminderDay, 
            convert(varchar,(
                  select  top 1 TransactionDate 
                  from [WINSERVER].[service].dbo.history
                  join [WINSERVER].[service].dbo.TransactionCode on TransactionCode.code = history.TransactionCode
                  where loanid = loanno
                  and (TransactionCode = 211 or TransactionCode = 210)         
                  order by TransactionDate DESC
            ) , 1) as TransactionDate,
            mobile_ph, carrierid,
      (select carrieremailsms from carrier where id = carrierid) as smsgateway
            from tblServiceInfo as serviceInfo 
            join [WINSERVER].[service].dbo.loan on loan.loanid = serviceinfo.loanno 
            join tblUsers as users on users.userid = serviceinfo.userid 
            where IsPaymentPostedReminderAlert = 1 ) as temp
where temp.transactiondate >= dateadd(dd,-2,getdate())
and temp.TransactionDate < dateadd(dd,1,getdate())
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PNPaymentPostedReminderold]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PNPaymentPostedReminderold]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Harsh Pandit
-- Create date: 
-- Description:	
-- =============================================
Create PROCEDURE [dbo].[sp_PNPaymentPostedReminderold] 
	--@LoanNo varchar(50)
AS
BEGIN
	SET NOCOUNT ON;
		select record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,  loanno, 
		(
			select  top 1 TransactionAmt  
			from [WINSERVER].[service].dbo.history 
			join [WINSERVER].[service].dbo.TransactionCode on TransactionCode.code = history.TransactionCode 
			where loanid = loanno 
			and (TransactionCode = 211 or TransactionCode = 210) 
			order by loanid,TransactionDate DESC
		) as TransactionAmt,  
		PaymentPostedReminderdate,IsSMSPaymentPostedAlert,IsEmailPaymentPostedAlert,IsPaymentPostedReminderAlert,PaymentPostedReminderDay,  
		convert(varchar,(
			select  top 1 TransactionDate  
			from [WINSERVER].[service].dbo.history 
			join [WINSERVER].[service].dbo.TransactionCode on TransactionCode.code = history.TransactionCode 
			where loanid = loanno 
			and (TransactionCode = 211 or TransactionCode = 210) 
			order by TransactionDate DESC
		) , 1) as TransactionDate,
		mobile_ph, carrierid,
	(select carrieremailsms from carrier where id = carrierid) as smsgateway 
		from tblServiceInfo as serviceInfo  
		join [WINSERVER].[service].dbo.loan on loan.loanid = serviceinfo.loanno  
		join tblUsers as users on users.userid = serviceinfo.userid  
		where IsPaymentPostedReminderAlert = 1   
		and (month(PaymentPostedReminderdate) <> month(getdate()) or PaymentPostedReminderdate is null)
		AND DATEDIFF(DAY,(select  top 1 TransactionDate  
			from [WINSERVER].[service].dbo.history 
			join [WINSERVER].[service].dbo.TransactionCode on TransactionCode.code = history.TransactionCode 
			where loanid = loanno 
			and (TransactionCode = 211 or TransactionCode = 210) 
			order by loanid,TransactionDate DESC),GETDATE()) <= PaymentPostedReminderDay
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PNPaymentReminder]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PNPaymentReminder]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Harsh Pandit
-- Create date: 25 March 2010
-- Modified date: 29 April 2010
-- Description:	to get data for Payment Reminder
--Modify by : Vipul Thakkar
--Modified Date: 10 OCT 2011
-- =============================================
CREATE PROCEDURE [dbo].[sp_PNPaymentReminder] 
AS
BEGIN
	SET NOCOUNT ON;
	select convert(varchar(10), dateadd(day,- PaymentReminderDay * 1 ,duedate ),101)as PaymentReminderNotificationDate, DATEDIFF(DAY,getdate(),duedate), PaymentReminderDay, record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,  
		serviceinfo.userid,  isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt,  loanid,	  convert(varchar, duedate, 1) as duedate,
		DATEDIFF(DAY,getdate(),duedate) as remainingDays,    IsSMSPaymentReminderAlert,IsEmailPaymentReminderAlert,
		IsPaymentReminderAlert,PaymentReminderDay,  PaymentReminderdate,    
		mobile_ph, carrierid, (select carrieremailsms from carrier where id = carrierid) as smsgateway
	from [winserver].[service].dbo.loan     
		join tblServiceInfo as serviceInfo on loan.loanid = serviceInfo.loanno    
		join tblUsers as users on users.userid = serviceinfo.userid    
	where 
		--uemailid='meggeman@neo.rr.com'
--start  comment
(month(PaymentReminderdate) <> month(getdate()) or PaymentReminderdate is null)     
and IsPaymentReminderAlert = 1   
--and DATEDIFF(DAY,getdate(),duedate) = (PaymentReminderDay*-1)
	--and DATEDIFF(DAY,getdate(),duedate) = (PaymentReminderDay*-1) --  comment old and include below line  for
--Sending before due  date days
and convert(varchar(10), dateadd(day,- PaymentReminderDay * 1 ,duedate ),101)=convert(varchar(10), getdate(),101)
--end  comment
--select record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,  
--serviceinfo.userid,isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt,  loanid,
--convert(varchar, duedate, 1) as duedate,DATEDIFF(DAY,getdate(),duedate) as remainingDays,  
--IsSMSPaymentReminderAlert,IsEmailPaymentReminderAlert,IsPaymentReminderAlert,PaymentReminderDay,PaymentReminderdate,
--mobile_ph, carrierid,
--	(select carrieremailsms from carrier where id = carrierid) as smsgateway  
--from [WINSERVER].[service].dbo.loan   
--join tblServiceInfo as serviceInfo on loan.loanid = serviceInfo.loanno  
--join tblUsers as users on users.userid = serviceinfo.userid  
--where (month(PaymentReminderdate) <> month(getdate()) or PaymentReminderdate is null)   
--and DATEDIFF(DAY,getdate(),duedate) >= 0  
--and IsPaymentReminderAlert = 1 
--and DATEDIFF(DAY,getdate(),duedate) <= PaymentReminderDay 
--	select DATEDIFF(DAY,getdate(),duedate),  record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,  
--	serviceinfo.userid,  isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt,  loanid,	  convert(varchar, duedate, 1) as duedate,
--	DATEDIFF(DAY,getdate(),duedate) as remainingDays,    IsSMSPaymentReminderAlert,IsEmailPaymentReminderAlert,
--	IsPaymentReminderAlert,PaymentReminderDay,  PaymentReminderdate,    
--	mobile_ph, carrierid,
--	(select carrieremailsms from carrier where id = carrierid) as smsgateway
--	from [WINSERVER].[service].dbo.loan     
--	join tblServiceInfo as serviceInfo on loan.loanid = serviceInfo.loanno    
--	join tblUsers as users on users.userid = serviceinfo.userid    
--	where (month(PaymentReminderdate) <> month(getdate()) or PaymentReminderdate is null)     
--	and IsPaymentReminderAlert = 1   and ( DATEDIFF(DAY,getdate(),duedate) <= PaymentReminderDay and PaymentReminderdate is null)
END




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_realtor_signup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_realtor_signup]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_realtor_signup] 
	-- Add the parameters for the stored procedure here
(	
@ufirstname varchar(100), 
@ulastname varchar(100),
@mobile_ph varchar(50),
@urole int,
@isadmin bit,
@isactive bit,
@carrierid VARCHAR(5),
@upassword VARCHAR(50),

@CompName varchar(50),
@address  varchar(200),
@address2  varchar(200),

@zip varchar(50)


)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
insert into tblusers (ufirstname,ulastname,mobile_ph,urole,isadmin,isactive,carrierid,upassword,CompName,address,address2,zip)
values(@ufirstname,@ulastname,@mobile_ph,@urole,@isadmin,@isactive,@carrierid,@upassword,@CompName,@address,@address2,@zip)
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RetailOperationPipeline_Processor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RetailOperationPipeline_Processor]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================    
-- Author: Krunal Patel   
-- Create date: 04 July 2013            
-- Description: <Select Processor Name and loanappid>    
-- =============================================    

CREATE PROCEDURE [dbo].[sp_RetailOperationPipeline_Processor]     
AS    
BEGIN  

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON; -- When SET XACT_ABORT is ON, if a Transact-SQL statement raises a run-time error	
	
	--Bind Loan Processor User in Retail Pipeline Report Filter
	Select 
		ufirstname+' '+ ulastname as 'FullName',E3Userid as ID from  tblusers where urole=14 and isactive=1

END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Select_Forms]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Select_Forms]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel
-- Create Date : 10/04/2012  
-- Description: Procedure for Select Form 
-- =============================================        
CREATE PROC  [dbo].[SP_Select_Forms](       
@URoleId int      
)                
AS                
BEGIN            
 IF(@URoleId ='0')       
         
   select  dbo.GetFormRole(fo.formid)as Roleid,* from tblforms FO left outer join tblCategories CAT on FO.Cateid=CAT.catid ORDER BY dttime DESC     
       
 ELSE      
       
	select dbo.GetFormRole(FO.formid)as Roleid, formID,Description,FileName,dttime,isnews,CateName from tblforms FO left outer join tblCategories CAT on FO.Cateid=CAT.catid where formid in (select formid from tblformroles   
	where uroleid =  @URoleId ) ORDER BY dttime DESC  
      
         
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Select_GetLoanLockStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Select_GetLoanLockStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC  [dbo].[SP_Select_GetLoanLockStatus](             
@LoanNo varchar(15)            
)                      
AS                      
BEGIN         
    
 select top 1 lockRecord.status,lockRecord.lockdatetime as lockdate from mwlloanapp as loanApp     
 left join mwlLockRecord as lockRecord on loanApp.id = lockRecord.loanApp_ID    
 where loannumber =@LoanNo  order by lockRecord.createdOnDate desc      
    
    
    
     
               
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblCFIUserRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblCFIUserRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[sp_Select_tblCFIUserRoles]              
as              
begin
--SELECT DISTINCT '-1' as RoleID,'-1' as ID, 'All User' as Roles  FROM tblRoles          
--union ALL             
SELECT
	*
FROM tpo_tblRoles
WHERE Id IN (0, 3, 20, 25, 21, 26)            
end 



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetEmailOfCreatedBy]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetEmailOfCreatedBy]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE PROCEDURE [dbo].[sp_Select_tblGetEmailOfCreatedBy]
(
@iCreatedBy varchar(Max)
)
as
begin

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select uemailid from tblusers where userid=@iCreatedBy
end


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetRegions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetRegions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Muruga Nagaraju
-- Create date: 31/01/2014
-- Description: Procedure for clsUser.cs bll    
-- =============================================  
CREATE PROCEDURE [dbo].[sp_Select_tblGetRegions]
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SELECT *
		,'Retail' AS Type
	FROM tblregion
	WHERE IsActive = 1
END

--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetSecurityQueandAnswer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetSecurityQueandAnswer]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Select_tblGetSecurityQueandAnswer]
(
@sUserid varchar(Max)
)
as
begin

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select * From tblUserDetails ud  inner join tblSecurityQuestions sc 
on ud.SecurityQuestion2=sc.QuestionId 
where UsersID =@sUserid

select userloginid from tblusers where userid=@sUserid
end


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetSecurityQueandAnswer1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetSecurityQueandAnswer1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Select_tblGetSecurityQueandAnswer1]
(
@sUserid varchar(Max)
)
as
begin

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select ud.*,isnull(ud.MaxSecurityAttempt,0) as MaxSecurityAttempt1 , isnull(ud.MaxUserIdSecurityAtmp,0) as MaxUserIdSecurityAtmp1 , isnull(ud.UserIdlastlogintime,getdate()) as UserIdlastlogintime1
,isnull(ud.lastlogintime,getdate()) as lastlogintime1,sc2.SecurityQuestion 
as SecurityQuestion_2,sc2.DisplayAt as DisplayAt_2
,sc1.SecurityQuestion as SecurityQuestion_1,sc1.DisplayAt as DisplayAt_1
,sc3.SecurityQuestion as SecurityQuestion_3,sc3.DisplayAt as DisplayAt_3 
From tblUserDetails ud inner join tblSecurityQuestions sc2 on ud.SecurityQuestion2=sc2.QuestionId inner join tblSecurityQuestions sc1 
on ud.SecurityQuestion1=sc1.QuestionId inner join tblSecurityQuestions sc3 on ud.SecurityQuestion3=sc3.QuestionId  
where UsersID =@sUserid

select userloginid from tblusers where userid=@sUserid
end


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetValidPasswordForNewProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetValidPasswordForNewProcess]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Select_tblGetValidPasswordForNewProcess]
(
@sUserloginid varchar(Max)
,@sEmailID varchar(Max)
)
as
begin
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


IF(@sUserloginid=@sEmailID)
BEGIN
	select top 1 userid from tblusers where  uemailid = @sEmailID and IsActive=1 and (userloginid IS NULL OR userloginid='')
END

ELSE IF( @sUserloginid IS NOT NULL and @sUserloginid <> '')
BEGIN
	select top 1 userid from tblusers where Userloginid=@sUserloginid and uemailid = @sEmailID and IsActive=1
END
ELSE
BEGIN
	select top 1 userid from tblusers where  uemailid = @sEmailID and IsActive=1

END
end




GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblRetailUserRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblRetailUserRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[sp_Select_tblRetailUserRoles]                  
as                  
begin
--SELECT DISTINCT '-1' as ID, 'All User' as Roles,'1' as IsActive  FROM tblRoles            
--union ALL               
--select * from Retail_tblRoles where id NOT IN(9,10,6,18)    
SELECT
	*
FROM tblRoles
WHERE id NOT IN (9, 6, 18)                 
end  



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblTPOUserRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblTPOUserRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[sp_Select_tblTPOUserRoles]                
as                
begin
--SELECT DISTINCT '-1' as RoleID,'-1' as ID, 'All User' as Roles  FROM tblRoles            
--union ALL            
SELECT
	*
FROM Tpo_tblRoles
WHERE Id NOT IN (25, 26, 4)             
end  



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblUserInfoForNewProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblUserInfoForNewProcess]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[sp_Select_tblUserInfoForNewProcess]
(
@userid varchar(50)
)
as
begin
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	select top 1 userid,userloginid,Uemailid,upassword,ufirstname,ulastname from tblusers where userid=@userid
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Select_TrackingData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Select_TrackingData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_Select_TrackingData](  
@trackid int  
 )                
AS                
BEGIN           
  select tblTracking.id, ufirstname, ulastname, uemailid, Case When urole=0 then 'Admin' when urole=1 then 'Director of Retail' when urole=2 then 'Mortgage Advisor' when urole=3 then 'Branch Manager' when urole=4 then 'Customer' when urole=5 then 'Realtor
' when urole=6 then 'Team' when urole=7 then 'Area Manager' when urole=8 then 'Doc Management' end as Roles,LoginTime, LogoutTime,IPAddress  from tblTracking   
  inner join tblUsers ON tblUsers.UserID = tblTracking.UserID where logintime between dateadd(day, datediff(day, 0 ,getdate())-90, 0) and getdate()  
  order by LoginTime desc, LogoutTime desc  
     
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SelectGetLoanType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SelectGetLoanType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_SelectGetLoanType]
	
AS
BEGIN

	SET NOCOUNT ON;

 select ID, LoanTypeId,LoanType from tblLoanType
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SelectGetPackageId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SelectGetPackageId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_SelectGetPackageId]
	
AS
BEGIN

	SET NOCOUNT ON;

 Select Id,PackageId from tblPackageTypeId
END
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SelectHIPHomePagePropertiesT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SelectHIPHomePagePropertiesT]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Harsh Pandit, Rajnish Pandey  
-- Create date: 16 April 2010  
-- Description: to get record for Properties  
-- [sp_SelectHIPHomePagePropertiesT]1,0 ,'13'  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_SelectHIPHomePagePropertiesT] 
 @SortID int,  
 @PageNo int,
 @PropIDs varchar(MAX)	   
AS  
BEGIN  
 DECLARE @PropID VARCHAR(50)   
 DECLARE @SortField VARCHAR(50)   
 DECLARE @PageRecord INT   
 DECLARE @SortField1 VARCHAR(50) 
 DECLARE @COUNT INT

 CREATE TABLE #Temp(  
 [ID] [int] IDENTITY(1,1) NOT NULL,  
 [PropID] [int] NULL,  
 [RealtorID] [int] NULL,  
 [PropStatus] [varchar](50) NULL,  
 [ListingTYpe] [varchar](50) NULL,  
 [BedRooms] [int] NULL,  
 [Baths] [decimal](18, 1) NULL,  
 [Price] [decimal](18, 2) NULL,  
 [DaysListed] [int] NULL,  
 [Street] [varchar](50) NULL,  
 [State] [varchar](50) NULL,  
 [City] [varchar](50) NULL,  
 [Zipcode] [varchar](50) NULL,  
 [YearBuilt] [int] NULL,  
 [IsActive] [bit] NULL,  
 [PropDesc] [ntext] NULL,  
 [lat] [decimal](18, 6) NULL,  
 [lng] [decimal](18, 6) NULL ,  
 [Area] [int] NULL,  
 [logoname] [varchar](50) NULL,  
 [uemailid] [varchar](50) NULL,  
 [PropImageName] [varchar](50) NULL,  
 [FullAddress] [varchar] (250),  
 [Amount] [varchar] (250),
 [ImageCount] [int] NULL  
 )  
 DECLARE db_cursor CURSOR FOR    
 SELECT PropID from tblProperties   
  
 OPEN db_cursor     
 FETCH NEXT FROM db_cursor INTO @PropID  
  
 WHILE @@FETCH_STATUS = 0     
 BEGIN   
   SET @COUNT=0
   INSERT INTO #Temp (PropID,RealtorID,PropStatus,ListingType,Bedrooms,baths,price,dayslisted,street,[state],city,zipcode,yearbuilt,lat,lng,Area,logoname,uemailid,PropImageName,FullAddress,Amount)  
   SELECT  TOP(1) prop.PropID, prop.RealtorID,PropStatus,ListingType,Bedrooms,baths,price,datediff(day,CreatedDate,getdate()) as dayslistedcalc,street,prop.[state],prop.city,zipcode,yearbuilt,prop.lat,prop.lng,prop.Area,
   CASE WHEN users.logoname='' THEN 'nologo.gif' ELSE ISNULL(users.logoname,'nologo.gif')END logoname,users.uemailid,  
   ISNULL(PropImageName,'noHome.png')PropImageName,street + '+' + prop.[state] + '+' + prop.city + '+' + zipcode as FullAddress, PARSENAME(Convert(varchar,Convert(money,Amount),1),2)
   FROM  tblProperties prop  
   LEFT OUTER JOIN  tblPropertyImage propImages ON prop.propid = propImages.propid  
   JOIN tblusers users ON users.userid = prop.realtorID  
   WHERE prop.PropID=@PropID  AND prop.isactive <> 0
   SET @COUNT=(SELECT COUNT('A') FROM  tblPropertyImage  WHERE propid=@PropID )
   Update #Temp SET ImageCount=@COUNT WHERE PropID=@PropID
     
     FETCH NEXT FROM db_cursor INTO @PropID     
 END     
  
 SET @PageRecord=@PageNo*4   
  
 IF(@PageRecord=0)  
 BEGIN  
  IF(@SortID = 0)   
   BEGIN   
    SELECT * FROM #Temp ORDER BY DaysListed asc  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 1)   
   BEGIN   
    SELECT * FROM #Temp ORDER BY CONVERT(DECIMAL(18,2),REPLACE(Amount,',','')) DESC  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 2)   
   BEGIN   
    SELECT * FROM #Temp ORDER BY BedRooms DESC  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 3)   
   BEGIN   
    SELECT * FROM #Temp ORDER BY Baths DESC  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 4)   
   BEGIN   
    SELECT * FROM #Temp ORDER BY Area DESC  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 5)   
   BEGIN   
    SELECT * FROM #Temp ORDER BY YearBuilt DESC  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
 END  
 ELSE  
 BEGIN  
  IF(@SortID = 0)   
   BEGIN   
    WITH [#Temp ORDERED BY ROWID] AS (SELECT ROW_NUMBER() OVER (ORDER BY DaysListed DESC) AS ROWID, * FROM #Temp)SELECT * FROM [#Temp ORDERED BY ROWID] WHERE ROWID >@PageRecord AND ROWID <@PageRecord+4  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 1)   
   BEGIN   
    WITH [#Temp ORDERED BY ROWID] AS (SELECT ROW_NUMBER() OVER (ORDER BY  CONVERT(DECIMAL(18,2),REPLACE(Amount,',','')) DESC ) AS ROWID, * FROM #Temp)SELECT * FROM [#Temp ORDERED BY ROWID] WHERE ROWID >@PageRecord AND ROWID <@PageRecord+4  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 2)   
   BEGIN   
    WITH [#Temp ORDERED BY ROWID] AS (SELECT ROW_NUMBER() OVER (ORDER BY BedRooms DESC) AS ROWID, * FROM #Temp)SELECT * FROM [#Temp ORDERED BY ROWID] WHERE ROWID >@PageRecord AND ROWID <@PageRecord+4  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 3)   
   BEGIN   
    WITH [#Temp ORDERED BY ROWID] AS (SELECT ROW_NUMBER() OVER (ORDER BY Baths DESC) AS ROWID, * FROM #Temp)SELECT * FROM [#Temp ORDERED BY ROWID] WHERE ROWID >@PageRecord AND ROWID <@PageRecord+4  
    SELECT COUNT('A')PageRecord FROM  #Temp    
   END  
  ELSE IF(@SortID = 4)   
   BEGIN   
    WITH [#Temp ORDERED BY ROWID] AS (SELECT ROW_NUMBER() OVER (ORDER BY Area DESC) AS ROWID, * FROM #Temp)SELECT * FROM [#Temp ORDERED BY ROWID] WHERE ROWID >@PageRecord AND ROWID <@PageRecord+4  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 5)   
   BEGIN   
    WITH [#Temp ORDERED BY ROWID] AS (SELECT ROW_NUMBER() OVER (ORDER BY YearBuilt DESC) AS ROWID, * FROM #Temp)SELECT * FROM [#Temp ORDERED BY ROWID] WHERE ROWID >@PageRecord AND ROWID <@PageRecord+4  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
 END  
 SELECT * INTO 	#Temp1 FROM #Temp 

IF(@SortID = 0)   
   BEGIN   
     SELECT * FROM 	#Temp1 WHERE PropID IN ((SELECT VAL FROM dbo.getTableByIDs(@PropIDS))) ORDER BY DaysListed asc  
   END  
  ELSE IF(@SortID = 1)   
   BEGIN   
     SELECT * FROM 	#Temp1 WHERE PropID IN ((SELECT VAL FROM dbo.getTableByIDs(@PropIDS))) ORDER BY  CONVERT(DECIMAL(18,2),REPLACE(Amount,',','')) DESC  
   END  
  ELSE IF(@SortID = 2)   
   BEGIN   
     SELECT * FROM 	#Temp1 WHERE PropID IN ((SELECT VAL FROM dbo.getTableByIDs(@PropIDS))) ORDER BY BedRooms DESC  
   END  
  ELSE IF(@SortID = 3)   
   BEGIN   
     SELECT * FROM 	#Temp1 WHERE PropID IN ((SELECT VAL FROM dbo.getTableByIDs(@PropIDS))) ORDER BY Baths DESC  
   END  
  ELSE IF(@SortID = 4)   
   BEGIN   
     SELECT * FROM 	#Temp1 WHERE PropID IN ((SELECT VAL FROM dbo.getTableByIDs(@PropIDS))) ORDER BY Area DESC  
   END  
  ELSE IF(@SortID = 5)   
   BEGIN   
     SELECT * FROM 	#Temp1 WHERE PropID IN ((SELECT VAL FROM dbo.getTableByIDs(@PropIDS))) ORDER BY YearBuilt DESC  
   END  
 


 



   
 DROP TABLE #Temp  
 DROP TABLE #Temp1 
 CLOSE db_cursor     
 DEALLOCATE db_cursor    
 END  

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SelectNewsAndAnnouncementID_By_TPO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SelectNewsAndAnnouncementID_By_TPO]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_SelectNewsAndAnnouncementID_By_TPO]            
(            
 
@NewsID int          
)            
AS            
BEGIN
SELECT
	*
FROM tpo_tblNewsAndAnnouncement
WHERE ID = @NewsID          
END 


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ServiceBanner]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ServiceBanner]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Created By: Amit Kumar
-- Create Date : 04/09/13
-- Description:
-- =============================================
CREATE procedure [dbo].[sp_ServiceBanner](
@ServiceBannerID int,
@Message varchar(300),
@PublishDate datetime,
@ExpireDate datetime,
@CreatedBy int,
@UpdatedBy int,
@Status char(1),
@Flag VARCHAR(15),
@ReturnValue INT OUTPUT
)
AS
BEGIN
----------------Start(To insert data into tblServiceBanner)----------
if(@Flag='insert')
begin
insert into tblServiceBanner(
Message,
PublishDate,
ExpireDate,CreatedBy,Status)
values(@Message,@PublishDate,@ExpireDate,@CreatedBy,@Status)
end
----------------End(To insert data into tblServiceBanner)----------

----------------Start(To update data into tblServiceBanner)----------
if(@Flag='update')
begin
update tblServiceBanner
set Message=@Message,PublishDate=@PublishDate,
ExpireDate=@ExpireDate,UpdatedBy=@UpdatedBy,Status=@Status

where ServiceBannerID=@ServiceBannerID
END

end
----------------End(To update data into tblServiceBanner)----------

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SetActiveDeactiveNew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SetActiveDeactiveNew]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_SetActiveDeactiveNew]  
@IsActive bit,  
@id varchar(50)  
AS  
BEGIN
--IF exists (SELECT * FROM tblNewsAndAnnouncement where ID=@ID AND cc_id='0')
-- begin
--	--print 'a'
--	 update tpo_tblNewsAndAnnouncement SET IsActive=@IsActive where cc_id=@id  
-- end
--else
-- begin
-- update tpo_tblNewsAndAnnouncement SET IsActive=@IsActive where ID = (SELECT cc_id FROM tblNewsAndAnnouncement where ID=@ID) 
--	--print 'b'
-- end

UPDATE tpo_tblNewsAndAnnouncement
SET IsActive = @IsActive
WHERE id = @id
   

END  


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SetLoanStatusSubmitLoans]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SetLoanStatusSubmitLoans]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  <Author,,Name>      
-- Create date: <Create Date,,>      
-- Description: <Description,,>      
-- =============================================      
CREATE PROCEDURE sp_SetLoanStatusSubmitLoans      
(       
 @LoanNumber varchar(20)      
)      
AS      
BEGIN      
      
Declare @LoanAppID varchar(200)      
    set @LoanAppID = (select id from mwlloanapp where loannumber=@LoanNumber)      
      
if not exists(select * From mwlappstatus where LoanApp_id in(@LoanAppID)       
    and StatusDesc='09 - Application Received')      
Insert into mwlappstatus(ID,LoanApp_id,StatusDesc,StatusDateTime)values      
                        (replace(newid(),'-',''),@LoanAppID,'09 - Application Received',getdate())      
                              
Update       
 mwlloanapp       
 Set CurrentStatus='09 - Application Received',CurrPipeStatusDate=getdate()       
    Where loannumber = @LoanNumber      
        
END 
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Update_BUserProfile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Update_BUserProfile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel
-- Create Date : 10/04/2012  
-- Description: Procedure for  
-- =============================================  
CREATE PROC [dbo].[SP_Update_BUserProfile](         
@UserID int,            
@UFirstName VARCHAR(50),                  
@ULastName  VARCHAR(50),                    
@UEmailId  VARCHAR(200),         
@experience VARCHAR(100),     
@phonenum nvarchar(20),    
@mobile_ph nvarchar(50),    
@address nvarchar(300),    
@Address2 varchar(100),    
@city nvarchar(100),    
@state nvarchar(10),    
@zip nvarchar(50),    
@fax nvarchar(50),    
@logoname nvarchar(50),    
@FaceBookID nvarchar(50),    
@TwitterID   nvarchar(50),    
@StateBelongTo nvarchar(100)=null,                  
@MyBranches VARCHAR(50),     
@MAID int,        
@LoanOfficer int,    
@bio ntext,    
@AreaOfExpertise ntext,    
@BranchID int,    
@MI varchar(10),    
@ClientTestimonial ntext,    
@RealtorTestimonial ntext,    
@TeamName nvarchar(100),    
@photoname nvarchar(50),    
@RealtorGroupID int,       
@Shorturl varchar(50)=null,    
@lat decimal(18,6),    
@lng decimal(18,6),    
@zlat decimal(18,6),    
@zlng decimal(18,6),    
@compName varchar(100)           
)                  
AS                  
BEGIN              
	update tblusers        
	set ufirstname=@UFirstName,ulastname=@ULastName,uemailid=@UEmailId,    
	experience=@experience,phonenum=@phonenum,mobile_ph=@mobile_ph,address=@address,Address2=@Address2,city=@city,state=@state,    
	zip=@zip,fax=@fax,logoname=@logoname,FaceBookID=@FaceBookID,TwitterID=@TwitterID,StateBelongTo=@StateBelongTo,    
	MyBranches=@MyBranches,MAID=@MAID,LoanOfficer=@LoanOfficer,bio=@bio,AreaOfExpertise=@AreaOfExpertise,    
	BranchID=@BranchID,MI=@MI,ClientTestimonial=@ClientTestimonial,RealtorTestimonial=@RealtorTestimonial,    
	TeamName=@TeamName,photoname=@photoname,RealtorGroupID=@RealtorGroupID,shorturl=@shorturl,lat=@lat,    
	lng=@lng,zlat=@zlat,zlng=@zlng,compName=@compName    
	where userid=@UserID       
           
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_Loanapp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_Loanapp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Update_Loanapp]
	-- Add the parameters for the stored procedure here
	
(
@Fname varchar(45),
@Mname varchar(45),
@LName varchar(45),
@Email varchar(200),
@CEmail varchar(200),
@DOBM varchar(10),
@DOBDT int,
@DOBYR int,
@SSN varchar(45),
@Phone varchar(45),
@Suffix varchar(5),
@COFNAME varchar(45),
@COMNAME varchar(45),
@COLNAME varchar(45),
@COEMAIL varchar(200),
@COCEmail varchar(200),
@CODOBM varchar(10),
@CODOBDT int,
@CODOBYR int,
@COSSN varchar(45),
@COPHONE varchar(45),
@STADDR varchar(45),
@CITY varchar(45),
@MState int,
@ZipCode varchar(10),
@RSTATUS varchar(25),
@RentAmt varchar(100),
@LMHOLDER varchar(45),
@STADDR2 varchar(45),
@CITY2 varchar(45),
@STATE2 int,
@COSuffix varchar(45),
@ZipCode2 varchar(10),
@RLengthY2 int,
@RLengthM2 int,
@RSTATUS2 varchar(25),
@RentAmt2 varchar(100),
@LMHOLDER2 varchar(45),
@AEMPL varchar(100),
@AOCCU varchar(100),
@AEMPPHONE varchar(45),
@AGMI varchar(100),
@ALEMY int,
@ALEMM int,
@AEMPL2 varchar(100),
@AOCCU2 varchar(100),
@AEMPPHONE2 varchar(45),
@AGMI2 int,
@ALEMY2 int,
@ALEMM2 int,
@CAEMPL varchar(100),
@CAOCCU varchar(100),
@CAEMPPHONE varchar(45),
@CAGMI varchar(100),
@CALEMY int,
@CALEMM int,
@CAEMPL2 varchar(100),
@CAOCCU2 varchar(100),
@CAEMPPHONE2 varchar(45),
@CAGMI2 varchar(100),
@CALEMY2 int,
@CALEMM2 int,
@OtherComments varchar(1000),
@DigiSig varchar(100),
@SigDate varchar(15),
@IsAckno bit ,
@DateEntered datetime,
@RLengthM int,
@RlengthY int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

Update TBLLOANAPP set 
Fname=@Fname,Mname=@Mname,LName=@LName,Email=@Email,CEmail=@CEmail,DOBM=@DOBM,DOBDT=@DOBDT,DOBYR=@DOBYR,SSN=@SSN,Phone=@Phone,Suffix=@Suffix,COFNAME=@COFNAME,COMNAME=@COMNAME,COLNAME=@COLNAME,COEMAIL=@COEMAIL,COCEmail=@COCEmail,CODOBM=@CODOBM,CODOBDT=@CODOBDT,CODOBYR=@CODOBYR,COSSN=@COSSN,COPHONE=@COPHONE,STADDR=@STADDR,CITY=@CITY,MState=@MState,ZipCode=@ZipCode,RSTATUS=@RSTATUS,RentAmt=@RentAmt,LMHOLDER=@LMHOLDER,STADDR2=@STADDR2,CITY2=@CITY2,STATE2=@STATE2,COSuffix=@COSuffix,ZipCode2=@ZipCode2,RLengthY2=@RLengthY2,RLengthM2=@RLengthM2,RSTATUS2=@RSTATUS2,RentAmt2=@RentAmt2,LMHOLDER2=@LMHOLDER2,AEMPL=@AEMPL,AOCCU=@AOCCU,AEMPPHONE=@AEMPPHONE,AGMI=@AGMI,ALEMY=@ALEMY,ALEMM=@ALEMM,AEMPL2=@AEMPL2,AOCCU2=@AOCCU2,AEMPPHONE2=@AEMPPHONE2,AGMI2=@AGMI2,ALEMY2=@ALEMY2,ALEMM2=@ALEMM2,CAEMPL=@CAEMPL,CAOCCU=@CAOCCU,CAEMPPHONE=@CAEMPPHONE,CAGMI=@CAGMI,CALEMY=@CALEMY,CALEMM=@CALEMM,CAEMPL2=@CAEMPL2,CAOCCU2=@CAOCCU2,CAEMPPHONE2=@CAEMPPHONE2,CAGMI2=@CAGMI2,CALEMY2=@CALEMY2,CALEMM2=@CALEMM2,OtherComments=@OtherComments,DigiSig=@DigiSig,SigDate=@SigDate,IsAckno=@IsAckno,DateEntered=@DateEntered,RLengthM=@RLengthM,RlengthY=@RlengthY
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_LoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_LoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Update_LoanApplication]
	-- Add the parameters for the stored procedure here
	(
	@LoanReasonID int, 
@HometypeID int ,
@PropertyTypeID int, 
@Producttypeid int, 
@StateID int, 
@CreditScoreID int, 
@AnnulIncomeBorrower money,
@LoanAmount money, 
@AssetBorrower money, 
@RetirementAssetsBorrower money,
@MAName varchar(100),
@PropertyRealtorCity varchar(100),
@IsWorkingWithRealtor bit,
@IsCashback bit,
@RealtorName nvarchar(100),
@RealtorPhoneNo nvarchar(100) , 
@PropertyLocated bit, 
@HomeOwned bit, 
@PurchasePrise money, 
@DownPaymentAmt int, 
@PartOfTotalAssets bit,
 @DownPaymentSourceID int,
 @HaveRealtor bit,
 @RealtorContactName nvarchar(100),
 @RealtorPhone nvarchar(100) , 
@RealtorEmail nvarchar(100), 
@AppFName nvarchar(100), 
@AppLName nvarchar(100), 
@AppStateID int, 
@AppZip nvarchar(100), 
@AppPrimaryPhone nvarchar(100), 
@AppSecondaryPhone nvarchar(100), 
@AppEmail nvarchar(100), 
@ContactTimeID int, 
@CurrentPropertyValue money, 
@PropertyPurchaseMonth nvarchar(100), 
@YearID int,
 @vExistingPuchasePrice money,
 @CurrentMortageBal money, 
@MonthlyPayment money, 
@SecondMortage bit, 
@SecondMortageBal money,
@UserAppearUrl nvarchar(1000), 
@vCity varchar(100), 
@bSignAgg bit, 
@dSchClosingDate datetime, 
@vEstRenovationAmt nvarchar(100),
 @vDownPayAmount nvarchar(100), 
@vAddressLine1 nvarchar(500),
 @vAddressLine2 nvarchar(500), 
@ApplyType varchar(50) , 
@IsLead360 bit, 
@IsFullApp bit, 
@CompIP varchar(50),
@loanapplicationid int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update tblLoanApplication set 
LoanReasonID=@LoanReasonID , 
HometypeID=@HometypeID  ,
PropertyTypeID=@PropertyTypeID , 
Producttypeid=@Producttypeid , 
StateID=@StateID , 
CreditScoreID=@CreditScoreID , 
AnnulIncomeBorrower=@AnnulIncomeBorrower,
LoanAmount=@LoanAmount , 
AssetBorrower=@AssetBorrower , 
RetirementAssetsBorrower=@RetirementAssetsBorrower ,
MAName=@MAName ,
PropertyRealtorCity=@PropertyRealtorCity ,
IsWorkingWithRealtor=@IsWorkingWithRealtor ,
IsCashback=@IsCashback ,
RealtorName=@RealtorName ,
RealtorPhoneNo=@RealtorPhoneNo , 
PropertyLocated=@PropertyLocated , 
HomeOwned=@HomeOwned , 
PurchasePrise=@PurchasePrise , 
DownPaymentAmt=@DownPaymentAmt , 
PartOfTotalAssets=@PartOfTotalAssets,
 DownPaymentSourceID=@DownPaymentSourceID,
 HaveRealtor=@HaveRealtor ,
 RealtorContactName=@RealtorContactName ,
 RealtorPhone=@RealtorPhone  , 
RealtorEmail=@RealtorEmail , 
AppFName=@AppFName , 
AppLName=@AppLName , 
AppStateID=@AppStateID , 
AppZip=@AppZip , 
AppPrimaryPhone=@AppPrimaryPhone , 
AppSecondaryPhone=@AppSecondaryPhone , 
AppEmail=@AppEmail , 
ContactTimeID=@ContactTimeID , 
CurrentPropertyValue=@CurrentPropertyValue , 
PropertyPurchaseMonth=@PropertyPurchaseMonth , 
YearID=@YearID ,
 ExistingPuchasePrice=ExistingPuchasePrice ,
 CurrentMortageBal=CurrentMortageBal , 
MonthlyPayment=@MonthlyPayment , 
SecondMortage=@SecondMortage , 
SecondMortageBal=@SecondMortageBal ,
UserAppearUrl=@UserAppearUrl , 
vCity=@vCity , 
bSignAgg=@bSignAgg , 
dSchClosingDate=@dSchClosingDate , 
vEstRenovationAmt=@vEstRenovationAmt ,
 vDownPayAmount=@vDownPayAmount , 
vAddressLine1=@vAddressLine1 ,
 vAddressLine2=@vAddressLine2 , 
ApplyType=@ApplyType  , 
IsLead360=@IsLead360 , 
IsFullApp=@IsFullApp , 
CompIP=@CompIP 
where loanapplicationid=@loanapplicationid       
              
                 
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_News_TPO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_News_TPO]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
  
CREATE procEDURE [dbo].[sp_Update_News_TPO]     
(    
@id int,          
@Title nvarchar(1000),       
@Body text,        
@IsActive bit,        
@CreatedDate datetime,        
@Role nvarchar(Max),  
@RoleC nvarchar(Max) ,
@RoleR nvarchar(Max) 
)    
AS    
BEGIN  
UPDATE Tpo_tblNewsAndAnnouncement WITH (UPDLOCK)  
SET Title = @Title,  
 Body = @Body,  
 IsActive = @IsActive,  
 Role = @Role,  
  RoleC = @RoleC, 
   RoleR = @RoleR, 
 ModifiedDate = @CreatedDate  
WHERE id = @id  
END    
    

  

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_Subscribe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_Subscribe]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Update_Subscribe]
	-- Add the parameters for the stored procedure here
	@Email nvarchar(100)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Subscribe 
set IsActive = 0, UpdatedDate=GetDate()Where EMail = @Email 
               
              
                 
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_update_tblAuthenticateSecurityInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_update_tblAuthenticateSecurityInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_update_tblAuthenticateSecurityInfo]
(
@strUserId varchar(Max),
@IsUserid varchar(Max) = NULL
)
as
begin

if(@IsUserid = '' OR @IsUserid IS  NULL)
	update tblUserDetails 
	set lastlogintime = Getdate()
	,MaxSecurityAttempt = isnull(MaxSecurityAttempt,0) + 1 
	where UsersID = @strUserId
else
	update tblUserDetails 
	set UserIdlastlogintime = Getdate()
	,MaxUserIdSecurityAtmp = isnull(MaxUserIdSecurityAtmp,0) + 1 
	where UsersID = @strUserId
end


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_update_tblCheckSecurityInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_update_tblCheckSecurityInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_update_tblCheckSecurityInfo]
(
@sUserid varchar(Max),
@IsUserid varchar(Max) = NULL
)
as
begin
if(@IsUserid = '' OR @IsUserid IS  NULL)
	update tblUserDetails set lastlogintime = null,MaxSecurityAttempt =0 where UsersID = @sUserid
ELSE
	update tblUserDetails set UserIdlastlogintime = null,MaxUserIdSecurityAtmp =0 where UsersID = @sUserid
end


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_tblTempUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_tblTempUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Gourav  
-- Create date: 10/03/2012  
-- Description: Procedure for clsUser.cs bll    
-- =============================================  
CREATE PROCEDURE [dbo].[sp_Update_tblTempUser]
(  
  
@refuserid varchar(400)  
)  
as  
begin  
UPDATE tblTempUser SET IsActive=0 where refuserid=@refuserid  
end  


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_tblusers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_tblusers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE PROCEDURE [dbo].[sp_Update_tblusers]
(
@userloginid varchar(50),
@newPass varchar(Max),
@userid varchar(Max)
)
as
begin
update tblusers set userloginid = @userloginid, upassword = @newPass, PasswordExpDate = GETDATE() where userid = @userid
end



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Update_Testimonial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Update_Testimonial]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Created By: Chandresh Patel  
-- Create Date : 10/04/2012    
-- Description: Procedure for Update SP_Update_Testimonial   
-- =============================================       
CREATE PROC [dbo].[SP_Update_Testimonial](      
@UName varchar(50),     
@Testimonial char(1),    
@SubmittedDate datetime,    
@TestimonialID int,    
@UpdatedDt datetime,    
@City varchar(50),      
@State varchar(50) )                  
AS                  
BEGIN             
	UPDATE tblTestimonials SET UName=@UName,
	Testimonial=@Testimonial,
	SubmittedDate=@SubmittedDate,
	UpdatedDt=GETDATE(), 
	City=@City,
	State=@State
	where TestimonialID=@TestimonialID
       
END 

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Update_UserProfile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Update_UserProfile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel  
-- Create Date : 10/04/2012  
-- Description: Procedure for Update User Profile
-- =============================================         
CREATE PROC [dbo].[SP_Update_UserProfile](         
@UserID int,            
@UFirstName VARCHAR(50),                  
@ULastName  VARCHAR(50),                    
@UEmailId  VARCHAR(200),    
@mobile_ph nvarchar(50),    
@upassword nvarchar(200),    
@carrierid int,
@photoname nvarchar(100)= null,
@logoname nvarchar(100) = null         
)                  
AS                  
BEGIN  

declare @Flag nvarchar(10)
if @photoname is not null and @logoname is not null
Begin
set @Flag = 'Both'
end
else if @photoname is not null and @logoname is null
Begin
set @Flag = 'Photo'
end
 else if @photoname is  null and @logoname is not null
Begin
set @Flag = 'Logo'
end

if @Flag = 'Photo'   
Begin
  update tblusers   
 set ufirstname=@UFirstName,ulastname=@ULastName,uemailid=@UEmailId,    
	mobile_ph=@mobile_ph,upassword=upassword,carrierid=@carrierid , photoname = @photoname
	where userid=@UserID 
 end
 else if @Flag = 'Logo' 
 Begin
  update tblusers   
 set ufirstname=@UFirstName,ulastname=@ULastName,uemailid=@UEmailId,    
	mobile_ph=@mobile_ph,upassword=upassword,carrierid=@carrierid ,logoname = @logoname
	where userid=@UserID 
 end
else if @Flag = 'Both'
Begin
 update tblusers   
 set ufirstname=@UFirstName,ulastname=@ULastName,uemailid=@UEmailId,    
	mobile_ph=@mobile_ph,upassword=upassword,carrierid=@carrierid , photoname = @photoname,
	logoname = @logoname
	where userid=@UserID 

end
else
Begin
update tblusers        
	set ufirstname=@UFirstName,ulastname=@ULastName,uemailid=@UEmailId,    
	mobile_ph=@mobile_ph,upassword=upassword,carrierid=@carrierid  
	where userid=@UserID 
end
END 


	
	
	



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateAlert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateAlert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_UpdateAlert]
(
 @IsSMSPaymentReminderAlert varchar (max),
 @IsSMSPaymentPostedAlert varchar (max),
 @IsSMSPaymentPastDue varchar (max),
 @IsEmailPaymentReminderAlert varchar (max),
 @IsEmailPaymentPostedAlert  varchar (max),
 @IsEmailPaymentPastDue  varchar (max),
 @IsPaymentReminderAlert  varchar (max),
 @IsPaymentPostedReminderAlert  varchar (max),
 @IsPaymentPastDue varchar (max),
 @PaymentReminderDay varchar (max),
 @PaymentPastDueReminderDay varchar (max),
 @record_id  varchar (max)
)
AS
BEGIN
update tblServiceInfo set 

 IsSMSPaymentReminderAlert=@IsSMSPaymentReminderAlert,
 IsSMSPaymentPostedAlert=@IsSMSPaymentPostedAlert,
 IsSMSPaymentPastDue=@IsSMSPaymentPastDue,
 IsEmailPaymentReminderAlert=@IsEmailPaymentReminderAlert,
 IsEmailPaymentPostedAlert =@IsEmailPaymentPostedAlert,
 IsEmailPaymentPastDue =@IsEmailPaymentPastDue,
 IsPaymentReminderAlert =@IsPaymentReminderAlert,
 IsPaymentPostedReminderAlert =@IsPaymentPostedReminderAlert,
 IsPaymentPastDue=@IsPaymentPastDue,
 PaymentReminderDay=@PaymentReminderDay,
 PaymentPastDueReminderDay=@PaymentPastDueReminderDay
 where record_id =@record_id
     
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateFaqs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateFaqs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create  Procedure [dbo].[sp_UpdateFaqs]
@ID bigint,
@Question varchar(1000),
@Answer Text,
@Published bit,
@DisplayOrder int
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

Update dbo.tblfaqs set Question=@Question,Answer=@Answer,Published=@Published,DisplayOrder=@DisplayOrder where ID=@ID
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateForms]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateForms]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_UpdateForms]
(

	@Description varchar(200) ,
	@FileName varchar(200) ,
	@isnews bit ,
	@CateID int ,
    @formID int
)
AS

BEGIN
	Update tblForms Set Description = @Description,FileName=@FileName,isnews=@isnews,CateID=@CateID
    where formID=@formID    

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateLong]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateLong]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateLong]
	-- Add the parameters for the stored procedure here
(
				@loanapplicationid int,
				@HometypeID int,
                @PropertyTypeID int,
                @LoanAmount money,
                @LoanReasonID int,
                @StateID int,
                @CurrentPropertyValue money,
                @YearID int,
                @CurrentMortageBal money,
                @MonthlyPayment money,
                @SecondMortage bit,
                @SecondMortageBal money,
                @ExistingPuchasePrice money,
                @vAddressLine2 varchar(500),
                @PurchasePrise money,
                @AppFName nvarchar(100),
                @AppLName nvarchar(100),
                @AppEmail nvarchar(100),
               
                @vAddressLine1 varchar(500),
                @vCity varchar(100),
                @AppStateID int,
                @AppZip nvarchar(100),
                @AppPrimaryPhone nvarchar(100),
                @AppSecondaryPhone nvarchar(100),
                @ContactTimeID int
				
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
update tblLoanApplication set

                HometypeID=@HometypeID,
                PropertyTypeID=@PropertyTypeID,
                LoanAmount=@LoanAmount,
                LoanReasonID=@LoanReasonID,
                StateID=@StateID,
                CurrentPropertyValue=@CurrentPropertyValue,
                YearID=@YearID,
                CurrentMortageBal=@CurrentMortageBal,
                MonthlyPayment=@MonthlyPayment,
                SecondMortage=@SecondMortage,
                SecondMortageBal=@SecondMortageBal,
                ExistingPuchasePrice=@ExistingPuchasePrice,
                vAddressLine2=@vAddressLine2,
                PurchasePrise=@PurchasePrise,
                AppFName=@AppFName,
                AppLName=@AppLName,
                AppEmail=@AppEmail,
           
                vAddressLine1=@vAddressLine1,
                vCity=@vCity,
                AppStateID=@AppStateID,
                AppZip=@AppZip,
                AppPrimaryPhone=@AppPrimaryPhone,
                AppSecondaryPhone=@AppSecondaryPhone,
                ContactTimeID=@ContactTimeID

		where	loanapplicationid=@loanapplicationid 


END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdatemwlSubjectProperty_BuildingStatusinE3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdatemwlSubjectProperty_BuildingStatusinE3]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Narayan.NR>
-- Create date: <05/11/2013>
-- Description:	<This will Update BuildingStatus InfoinE3>
-- =============================================
Create PROCEDURE [dbo].[sp_UpdatemwlSubjectProperty_BuildingStatusinE3]
(
	@strBuildingStatus varchar(10),
	@strLoanappId varchar(100)
)
AS
Begin Try
Begin Transaction    
declare @buildingStatus varchar(10)
if @strBuildingStatus='yes'
set @buildingStatus = 3
else if  @strBuildingStatus='No'
set @buildingStatus = 2

 Update mwlSubjectProperty set BuildingStatus = @strBuildingStatus where LoanApp_id = @strLoanappId
Commit 
End Try
Begin Catch
    Rollback  
    Declare @Msg nvarchar(max)
    Select @Msg=Error_Message();
    RaisError('Error Occured: %s', 20, 101,@Msg) With Log;
End Catch
GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateNewRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateNewRate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_UpdateNewRate]
(
@strloantype varchar(Max),
@LoanProgram varchar(Max),
@GoalOfLoan varchar(Max),
@IPoints varchar(Max),
@DecRate bit,
@DecAPR bit,
@DecClosingCost varchar(Max),
@ModifiedDate varchar(Max),
@NewRateID varchar(Max)
)
AS
BEGIN

update tblnewrates set strloantype=@strloantype,LoanProgram=@LoanProgram,GoalOfLoan=@GoalOfLoan,
IPoints=@IPoints,DecRate=@DecRate,DecAPR=@DecAPR,DecClosingCost=DecClosingCost,ModifiedDate=@ModifiedDate
 where NewRateID=@NewRateID
                
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdatePropDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdatePropDetails]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdatePropDetails]
	-- Add the parameters for the stored procedure here
(
@RealtorID int,
@PropStatus varchar(50),
@ListingType varchar(50),
@Bedrooms int,
@baths decimal(18,1),
@amount decimal(18,0) ,
@street varchar(50),
@state varchar(50),
@city varchar(50),
@zipcode varchar(50),
@yearbuilt int, 
@PropDesc ntext,
@Area int,
@isactive bit,
@ImprovementFile varchar(50),
@PropID int 
) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

		update tblProperties set 
					
RealtorID=@RealtorID,
PropStatus=@PropStatus,
ListingType=@ListingType,
Bedrooms=@Bedrooms,
baths=@baths,
amount=@amount,
street=@street,
state=@state,
city=@city,
zipcode=@zipcode,
yearbuilt=@yearbuilt, 
PropDesc=@PropDesc,
Area=@Area,
isactive=@isactive,
ImprovementFile=@ImprovementFile

 where PropID=@PropID
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdatePropDetails1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdatePropDetails1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdatePropDetails1]
	-- Add the parameters for the stored procedure here
(

@PropStatus varchar(50),
@ListingType varchar(50),
@Bedrooms int,
@baths decimal(18,1),
@amount decimal(18,0) ,
@street varchar(50),
@state varchar(50),
@city varchar(50),
@zipcode varchar(50),
@yearbuilt int, 
@PropDesc ntext,
@lat decimal(18,6),
@lng decimal(18,6),
@Area int,
@isactive bit,

@PropID int 
)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

		update tblProperties set 

PropStatus=@PropStatus,
ListingType=@ListingType,
Bedrooms=@Bedrooms,
baths=@baths,
amount=@amount,
street=@street,
state=@state,
city=@city,
zipcode=@zipcode,
yearbuilt=@yearbuilt, 
PropDesc=@PropDesc,
Area=@Area,
isactive=@isactive,

lat=@lat ,
lng=@lng 

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateRate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_UpdateRate]
(
@strLoanType varchar(Max),
@IPoints varchar(Max),
@DecRate varchar(Max),
@DecAPR varchar(Max),
@DecClosingCost varchar(Max),
@ModifiedDate varchar(Max),
@rateid varchar(Max)
)
AS
BEGIN

update tblrates set strLoanType=@strLoanType,IPoints=@IPoints,DecRate=@DecRate,DecAPR=@DecAPR,
DecClosingCost=@DecClosingCost,ModifiedDate=@ModifiedDate where rateid=@rateid
 
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateRecordAfterAddImageUpload]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateRecordAfterAddImageUpload]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_UpdateRecordAfterAddImageUpload]
(
@ctid VARCHAR(MAX)
)
AS
BEGIN
update tblAdditionalUWDoc set isCommited = 1 where ctid = @ctid
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateShortLoanApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateShortLoanApplication]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateShortLoanApplication]
	-- Add the parameters for the stored procedure here
	@LoanApplicationID varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update tblLoanApplication set IsLead360=1, IsFullApp=1 WHERE LoanApplicationID =@LoanApplicationID
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateShortLoanApplication1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateShortLoanApplication1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateShortLoanApplication1]
	-- Add the parameters for the stored procedure here
	@LoanApplicationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update tblLoanApplication set IsLead360=1, IsFullApp=1 WHERE LoanApplicationID=@LoanApplicationID
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateUserRegistration]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateUserRegistration]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsUserRegistrationE3.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_UpdateUserRegistration]
(
@ufirstname varchar(Max),
@ulastname varchar(Max),
@uemailid varchar(Max),
@urole varchar(Max),
@isadmin bit,
@isactive bit,
@uidprolender varchar(Max),
@uparent varchar(Max),
@upassword varchar(Max),
@mobile_ph varchar(Max),
@userid varchar(Max)

)
AS
BEGIN

	 update tblusers set ufirstname=@ufirstname,ulastname=@ulastname,uemailid=@uemailid,urole=@urole,isadmin=@isadmin,isactive=@isactive,uidprolender=@uidprolender,uparent=@uparent,upassword=@upassword,mobile_ph=@mobile_ph where userid=@userid
                
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ValidateLoan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ValidateLoan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

      
--Example for execute stored procedure     
--EXECUTE sp_ValidateLoan 1,0,'0000013164'  
--EXECUTE sp_ValidateLoan 15143,11,'0000366349'     
--EXECUTE sp_ValidateLoan 1,0,'0000333815'         
--EXECUTE sp_ValidateLoan 10144,1,'0000333815'          
--EXECUTE sp_ValidateLoan 166,2,'0000357464'          
--EXECUTE sp_ValidateLoan 76,3,'0000353743'         
--EXECUTE sp_ValidateLoan 226,7,'0000353743'         
--EXECUTE sp_ValidateLoan 226,7,'0000353743'         
--EXECUTE sp_ValidateLoan 226,7,'0000353743'         
--EXECUTE sp_ValidateLoan 132,10,'0000360684'    
--EXECUTE sp_ValidateLoan 132,10,'0000358076'     
--EXECUTE sp_ValidateLoan 76,3,'0000365345'    
--EXECUTE sp_ValidateLoan '15143', '11', '0000357992'    
--EXECUTE sp_ValidateLoan '3918', '14', '0000354867'      
--EXECUTE sp_ValidateLoan '3926', '15', '0000322775'        
--EXECUTE sp_ValidateLoan '18475', '16', '0000317476'    
--EXECUTE sp_ValidateLoan 12910,13,'0000366349'    
--EXECUTE sp_ValidateLoan 15143,11,'0000366349'    
-- =============================================              
-- Author:  <Stonegate>              
-- Create date: <06/4/2013>              
-- Description: <This wil Check for Loan number exist in roles>              
-- Modify Date : <12/4/2013>              
-- Modify By :   Chandresh Patel    
-- =============================================              
CREATE PROCEDURE [dbo].[sp_ValidateLoan]    
    (    
      --Parameter declaration              
  @UserID VARCHAR(10),          
  @RoleID CHAR(3),               
  @strLoanno VARCHAR(100)     
    )    
AS     
    BEGIN            
  --Variable Declaration         
        DECLARE @strID VARCHAR(MAX)      
  --declaration of variable and assign meaningful name to role id and set value of that receieved from parameter       
  --Start      
               
  DECLARE @Admin INT        
  SET @Admin=0        
      
  DECLARE @DirRetail INT         
  SET @DirRetail=1     
         
  DECLARE @MA INT         
  SET @MA = 2         
      
  DECLARE @BranchMgr INT        
  SET @BranchMgr=10        
          
  DECLARE @SalesMgr INT         
  SET @SalesMgr=3       
       
  DECLARE @RetailVP INT        
  SET @RetailVP=7        
           
  Declare @RetailOperMgr INT        
  SET @RetailOperMgr=11        
            
        Declare @ClientRelMgr INT        
  SET @ClientRelMgr=13      
      
  Declare @LoanProcessor INT        
  SET @LoanProcessor=14        
          
  Declare @Closer INT    
  SET @Closer=15        
          
  Declare @RetailTeamLd INT    
  SET @RetailTeamLd=16        
            
  Declare @AreaMgr INT        
  SET @AreaMgr=19      
  
  Declare @DisclosureDeskAnlst INT 
  SET @DisclosureDeskAnlst = 21
       
  --end of declaration of variable and assigment    
        
              /* Fetching E3UserID for Mortgate Advisor Role Only */    
                IF ( @RoleID = @MA     
                   )     
                    BEGIN          
                        SELECT  @strID = E3Userid    
                        FROM    dbo.tblUsers    
                        WHERE   userid = @UserID       
                            
                    END      
   /*    
   -- base Query     
   select count(Loannumber)as LoanCount          
   from mwlLoanApp loanapp    with (NOLOCK)       
   Left join dbo.mwlInstitution as insitution on insitution.ObjOwner_id = loanapp.id               
   and insitution.InstitutionType=loanapp.ChannelType      
   where LoanNumber is not null and LoanNumber <>''    
     */    
         
     /* Fetching Loan Number Count for Dir. Of Retail,Retail VP.Loan processor,Closer,    
        Retail Team Lead,Client Relationship Manager and Retail Operation Manager Role */    
    IF ( @RoleID = @DirRetail     
      OR @RoleID = @RetailVP    
      OR @RoleID = @LoanProcessor    
      OR @RoleID = @Closer     
                     OR @RoleID = @RetailTeamLd     
                     OR @RoleID = @Admin      
                     OR @RoleID = @ClientRelMgr    
                     OR @RoleID = @RetailOperMgr   
                     OR @RoleID = @DisclosureDeskAnlst  
                   )     
                    BEGIN    
                 SELECT  COUNT(Loannumber) AS LoanCount    
                        FROM    mwlLoanApp loanapp WITH ( NOLOCK )    
                                LEFT JOIN dbo.mwlInstitution AS insitution WITH ( NOLOCK ) ON insitution.ObjOwner_id = loanapp.id    
                                                              AND insitution.InstitutionType = loanapp.ChannelType     
                        WHERE   LoanNumber IS NOT NULL    
                                AND LoanNumber <> ''     
                                AND ( LoanNumber = @strLoanno )     
                    
                    END      
            /* Fetching Loan Number Count for Mortgage Advisor Role According to their Loan number and E3User ID*/       
                IF ( @RoleID = @MA    
                   )     
                    BEGIN    
                        SELECT  COUNT(Loannumber) AS LoanCount    
                        FROM    mwlLoanApp loanapp WITH ( NOLOCK )    
                                LEFT JOIN dbo.mwlInstitution AS insitution WITH ( NOLOCK ) ON insitution.ObjOwner_id = loanapp.id    
                                                              AND insitution.InstitutionType = loanapp.ChannelType     
                        WHERE   LoanNumber IS NOT NULL    
                                AND LoanNumber <> ''    
                                AND ( Originator_id = @strID )    
                                AND ( LoanNumber = @strLoanno )     
                    
                    END     
           /* Fetching Loan Number Count for Area Manager Role */       
                IF ( @RoleID = @AreaMgr     
                   )     
                    BEGIN    
                        SELECT  COUNT(Loannumber) AS LoanCount    
                        FROM    mwlLoanApp loanapp WITH ( NOLOCK )    
                                LEFT JOIN dbo.mwlInstitution AS insitution WITH ( NOLOCK ) ON insitution.ObjOwner_id = loanapp.id
                                 AND insitution.InstitutionType = loanapp.ChannelType     
                        WHERE   LoanNumber IS NOT NULL    
                                AND LoanNumber <> ''    
                                --AND ( Originator_id IN (     
                                --      SELECT E3Userid FROM tblusers WITH ( NOLOCK ) where     
                                --      office=(SELECT office from tblusers WITH ( NOLOCK )     
                                --      where urole in(@MA,@SalesMgr,@RetailVP,@BranchMgr,@AreaMgr)     
                                --      and userid=@UserID) ) )    
                                AND ( LoanNumber = @strLoanno )             
                    END     
                /* Fetching Loan Number Count for Branch Manager Role */     
                IF ( @RoleID = @BranchMgr    
                   )     
                    BEGIN    
                         SELECT  COUNT(Loannumber) AS LoanCount    
                        FROM    mwlLoanApp loanapp WITH ( NOLOCK )    
                                LEFT JOIN dbo.mwlInstitution AS insitution WITH ( NOLOCK ) ON insitution.ObjOwner_id = loanapp.id  
                                AND insitution.InstitutionType = loanapp.ChannelType     
                        WHERE   LoanNumber IS NOT NULL    
                                AND LoanNumber <> ''     
                                AND ( LoanNumber = @strLoanno )               
                    END    
               /* Fetching Loan Number Count for Sales Manager Role */     
                IF (@RoleID = @SalesMgr     
                   )     
                    BEGIN    
                        SELECT  COUNT(Loannumber) AS LoanCount    
                        FROM    mwlLoanApp loanapp WITH ( NOLOCK )    
                                LEFT JOIN dbo.mwlInstitution AS insitution WITH ( NOLOCK ) ON insitution.ObjOwner_id = loanapp.id  
                                AND insitution.InstitutionType = loanapp.ChannelType    
                        WHERE   LoanNumber IS NOT NULL    
                                AND LoanNumber <> ''    
                                AND ( Originator_id IN (     
                                      SELECT E3Userid FROM tblusers WITH ( NOLOCK ) where     
                                      office=(SELECT office FROM tblusers WITH ( NOLOCK )     
                                      where urole in(@RoleID)     
                                      and userid=@UserID) ) )    
                                AND ( LoanNumber = @strLoanno )              
                    END      
            END   


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_VerifyClosingScheduleDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_VerifyClosingScheduleDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_VerifyClosingScheduleDate]
(
@LoanNo VARCHAR(MAX)
)
AS
BEGIN
select SchDate from tblScheduleClosing where LoanNo =@LoanNo

END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_NewsAndAnnouncement_ManageTemplate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_NewsAndAnnouncement_ManageTemplate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
CREATE procEDURE [dbo].[udsp_NewsAndAnnouncement_ManageTemplate]    --0              
                   
      
      
-- Add the parameters for the stored procedure here                  
                   
      
      
@uRole varchar(2)                  
              
      
      
AS                  
BEGIN      
-- SET NOCOUNT ON added to prevent extra result sets from                  
-- interfering with SELECT statements.                  
SET NOCOUNT ON;      
      
SELECT      
 [ID],      
 [Title],      
 [Body],      
 [IsActive],      
 [IsManageTemplate] AS [IsNewsAnnouncement],      
 [CreatedBy],      
 [CreatedDate],      
 '' AS IPAddress,      
 [Role],      
 [ManageTempVisbleAd] AS [NewsAnnousVisbleAd],      
 [ManageTempVisbleDR] AS [NewsAnnousVisbleDR],      
 [ManageTempVisbleBR] AS [NewsAnnousVisbleBR],      
 [ManageTempVisbleLO] AS [NewsAnnousVisbleLO],      
 [ManageTempVisbleCU] AS [NewsAnnousVisbleCU],      
 [ManageTempVisbleRE] AS [NewsAnnousVisbleRE],      
 [ManageTempVisbleRM] AS [NewsAnnousVisbleRM],      
 [ManageTempVisbleDP] AS [NewsAnnousVisbleDP],      
 [ManageTempVisbleCP] AS [NewsAnnousVisbleCP],      
 [ManageTempVisbleHY] AS [NewsAnnousVisbleHY],      
 [ManageTempVisbleDCFI] AS [NewsAnnousVisbleDCFI],      
 [ManageTempVisibleSCFI] AS [NewsAnnousVisbleSCFI],      
 [ManageTempVisbleUM] AS [NewsAnnousVisbleUM],      
 [ModifiedBy],      
 [ModifiedDate]      
 ,Convert(bit,0) AS NewsAnnousVisbleAM,      
  Convert(bit,0) AS NewsAnnousVisbleDM,      
  Convert(bit,0) AS NewsAnnousVisbleROM,      
  Convert(bit,0) AS NewsAnnousVisbleCO,      
  Convert(bit,0) AS NewsAnnousVisbleCRM,      
  Convert(bit,0) AS NewsAnnousVisbleLP,      
  Convert(bit,0) AS NewsAnnousVisbleCL,      
  Convert(bit,0) AS NewsAnnousVisbleRTL,      
  CC_ID,      
  SiteStatus                  
   ,[Body] as News                  
      ,[IsManageTemplate]              
      ,'M' as Popup       
      
FROM [Tpo_tblManageTemplate]      
WHERE PATINDEX('%,' + @uRole + ',%', ','+ role+',') > 0  and IsActive=1    
--and IsManageTemplate!=0            
ORDER BY id DESC                
END 





GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateKatalystFlagInBranchOffices]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateKatalystFlagInBranchOffices]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Shwetal Shah>
-- Create date: <25-04-2013>
-- Description:	<To update Katalyst Rights applied by Admin to specific branch office>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateKatalystFlagInBranchOffices]
(
	@E3BranchOfficeID varchar(32),
	@E3BranchOffice varchar(200),
	@IsKatalyst bit
)
AS
BEGIN
	IF Exists (Select ID From E3KatalystBranchOffice WITH ( NOLOCK ) Where E3BranchOfficeID = @E3BranchOfficeId)
	Begin
		Update E3KatalystBranchOffice WITH (UPDLOCK) Set IsKatalyst = @IsKatalyst Where E3BranchOfficeID = @E3BranchOfficeID
	End
	Else
	Begin
		Insert Into E3KatalystBranchOffice(ID,E3BranchOfficeID,E3BranchOffice,IsKatalyst)
		Select ISNULL((Select MAX(ID) From E3KatalystBranchOffice),0)+1, @E3BranchOfficeID, @E3BranchOffice, @IsKatalyst
	End
END


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateMailstatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateMailstatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[UpdateMailstatus]  
 -- Add the parameters for the stored procedure here  
@ID int,
@status int
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
update tblEmailServiceReader set updStatus=@status where Id=@ID
 --select TOP 1 * from tblEmailServiceReader where updStatus=0 order by RcvdDate desc  
END  


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_C3ClearCapitalValidating_Loan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_C3ClearCapitalValidating_Loan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



Create PROCEDURE [dbo].[USP_C3ClearCapitalValidating_Loan]
(
@LoanNumber varchar(30) = '0',
@Role bigint = 14,
@UserId bigint = 28046
)
AS
BEGIN

 CREATE TABLE #tempLoan
 (
 DB VARCHAR(100),
 loan_no VARCHAR(100),
 CurrentStatus VARCHAR(100),
 LoanStatus VARCHAR(100),
 BorrowerLastName VARCHAR(100),
 YNValue VARCHAR(100),
 DecisionStatus VARCHAR(100),
 UnderwriterType VARCHAR(100),
 loan_amt VARCHAR(100),
 SubmittedDate VARCHAR(100),
 BorrowerFirstName VARCHAR(100)
 )

Declare @uRetailProcessorName  varchar(100)
Declare @custfielddef_id  varchar(100)
Set @custfielddef_id = (select ID from [mwsCustFieldDef] where ltrim(rtrim(CustomFieldName)) = 'Retail Processor')
set @uRetailProcessorName = (select ltrim(rtrim(ufirstname)) + ' '+ ltrim(rtrim(ulastname)) from tblUsers where userid = @UserId)

INSERT INTO #tempLoan Select distinct 'E3' DB,
M.LoanNumber loan_no ,
M.CurrentStatus,
left(M.CurrentStatus,2)  LoanStatus,
borr.lastname BorrowerLastName,
1 YNValue,
M.DecisionStatus,
'NA' UnderwriterType,
loanData.AdjustedNoteAmt,
appStat.StatusDateTime AS SubmittedDate,
borr.firstname BorrowerFirstName
 
from 
 (
Select * from mwlloanapp where ltrim(rtrim(LoanNumber)) Like @LoanNumber +'%' and ChannelType LIKE 'Ret%'  

--and CASE WHEN @Role = 0 THEN 1 ELSE originator_ID  END  = CASE WHEN @Role = 0 THEN 1 ELSE (select e3userid from tblUsers where Userid =@UserId ) END 
) M
INNER JOIN 
(
 select ltrim(rtrim(StringValue)) RetailProcessor,loanapp_id from mwlcustomfield
 where custfielddef_id = @custfielddef_id  and (@Role = 0 OR ltrim(rtrim(stringValue))= @uRetailProcessorName)
) C ON C.loanapp_id = M.Id
INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id,lastname,firstname from mwlBorrower group by loanapp_id,lastname,firstname) as borr on borr.loanapp_id = M.id
INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id from mwlBorrower group by loanapp_id) as borrS on borr.loanapp_id = borrs.loanapp_id and  borrs.SequenceNum = borr.SequenceNum
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = M.id
Left join mwlAppStatus appStat on M.ID=appStat.LoanApp_id and appStat.StatusDesc='01 - Registered' 
Left join mwlApprovalStatus approvalStat on M.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved'

Select * from #tempLoan  
where (ISNUMERIC(LoanStatus) = 1 and  LoanStatus <  33) or LoanStatus LIKE 'RE%'
drop table #tempLoan

END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Inserttbluserdetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Inserttbluserdetails]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gourav
-- Create date: 10/09/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE PROCEDURE [dbo].[usp_Inserttbluserdetails]
(
@ChangeUsername varchar(50),
@SecurityQuestion1 int,
@Answer1 varchar(250),
@SecurityQuestion2 int,
@Answer2 varchar(250),
@SecurityQuestion3 int,
@Answer3 varchar(250),
@UsersID int
)
as
begin
INSERT INTO tblUserDetails
(UsersID,ChangeUsername,SecurityQuestion1,Answer1,
SecurityQuestion2,Answer2,SecurityQuestion3,Answer3) 
Values(@UsersID,@ChangeUsername,@SecurityQuestion1,@Answer1,
@SecurityQuestion2,@Answer2,@SecurityQuestion3,@Answer3)
end 


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_RealECValidating_Loan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_RealECValidating_Loan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_RealECValidating_Loan]
(
@LoanNumber varchar(30) ,
@Role int ,
@UserId bigint  
)
AS
BEGIN

 CREATE TABLE #tempLoan
 (
 DB VARCHAR(100),
 loan_no VARCHAR(100),
 CurrentStatus VARCHAR(100),
 LoanStatus VARCHAR(100),
 BorrowerLastName VARCHAR(100),
 YNValue VARCHAR(100),
 DecisionStatus VARCHAR(100),
 UnderwriterType VARCHAR(100),
 loan_amt VARCHAR(100),
 SubmittedDate VARCHAR(100),
 BorrowerFirstName VARCHAR(100)
 )

Declare @uRetailProcessorName  varchar(100)
Declare @Originator_id  varchar(100)
Declare @custfielddef_id  varchar(100)
Set @custfielddef_id = (select ID from [mwsCustFieldDef] where ltrim(rtrim(CustomFieldName)) = 'Retail Processor')
set @uRetailProcessorName = (select ltrim(rtrim(ufirstname)) + ' '+ ltrim(rtrim(ulastname)) from tblUsers where userid = @UserId)
Set @Originator_id = (Select top 1 E3Userid from tblUsers where  userid = @UserId)


-------------------------Loan Processor-----------------------------------------
IF(@Role = 14)
BegiN
	INSERT INTO #tempLoan
	Select distinct 'E3' DB,
	M.LoanNumber loan_no ,
	M.CurrentStatus,
	left(M.CurrentStatus,2)  LoanStatus,
	M.lastname BorrowerLastName,
	1 YNValue,
	M.DecisionStatus,
	'NA' UnderwriterType,
	loanData.AdjustedNoteAmt,
	appStat.StatusDateTime AS SubmittedDate,
	M.firstname BorrowerFirstName
 	from 
	 (Select M.*,borr.* from mwlloanapp M
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id,lastname,firstname from mwlBorrower group by loanapp_id,lastname,firstname) as borr on borr.loanapp_id = M.id
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id from mwlBorrower group by loanapp_id) as borrS on borr.loanapp_id = borrs.loanapp_id and  borrs.SequenceNum = borr.SequenceNum

	where (ltrim(rtrim(LoanNumber)) Like @LoanNumber + '%' or borr.LastName LIKE @LoanNumber +'%') and ChannelType LIKE 'Ret%'  

	) M
	INNER JOIN 
	(
	 select ltrim(rtrim(StringValue)) RetailProcessor,loanapp_id from mwlcustomfield
	 where custfielddef_id = @custfielddef_id  and (ltrim(rtrim(stringValue))= @uRetailProcessorName)
	) C ON C.loanapp_id = M.Id
	Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = M.id
	Left join mwlAppStatus appStat on M.ID=appStat.LoanApp_id and appStat.StatusDesc='01 - Registered' 
	Left join mwlApprovalStatus approvalStat on M.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved'
END
-------Mortgage Advisor role Validation Loan---------------------
ELSE IF(@Role =2)
BEGIN
	INSERT INTO #tempLoan
	 Select distinct 'E3' DB,
	M.LoanNumber loan_no ,
	M.CurrentStatus,
	left(M.CurrentStatus,2)  LoanStatus,
	M.lastname BorrowerLastName,
	1 YNValue,
	M.DecisionStatus,
	'NA' UnderwriterType,
	loanData.AdjustedNoteAmt,
	appStat.StatusDateTime AS SubmittedDate,
	M.firstname BorrowerFirstName 
	from 
	 (Select M.*,borr.* from mwlloanapp M
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id,lastname,firstname from mwlBorrower group by loanapp_id,lastname,firstname) as borr on borr.loanapp_id = M.id
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id from mwlBorrower group by loanapp_id) as borrS on borr.loanapp_id = borrs.loanapp_id and  borrs.SequenceNum = borr.SequenceNum
	where (ltrim(rtrim(LoanNumber)) Like @LoanNumber + '%' or borr.LastName LIKE @LoanNumber +'%') and ChannelType LIKE 'Ret%'  
	and Originator_id = @Originator_id
	) M
	INNER JOIN dbo.mwlloandata as loanData on loanData.ObjOwner_id = M.id
	LEFT JOIN mwlAppStatus appStat on M.ID=appStat.LoanApp_id and appStat.StatusDesc='01 - Registered' 
	LEFT JOIN mwlApprovalStatus approvalStat on M.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved'
END

-------------------------------Sales Manager and Branch Manager role-----------------------------


ELSE IF(@Role in(3,10))
BEGIN

;WITH ReporteCTE
	AS ( 
			SELECT u3.userid,u3.E3userid FROM tblusers u3 where u3.userid = @UserId and u3.urole in (3,10)
			UNION ALL
			SELECT u1.userid,u1.E3userid
			FROM tblusermanagers m1 
			INNER JOIN  tblusers u1 on u1.userid = m1.userid 
				and m1.managerid = @UserId
				
			INNER JOIN tblroles r1 on u1.urole = r1.id 
			UNION ALL 

			SELECT u2.userid,u2.E3userid
			FROM tblusermanagers m2 
			INNER JOIN  tblusers u2 on u2.userid = m2.userid --and m2.managerid = 45027
			INNER JOIN ReporteCTE on m2.managerid = ReporteCTE.userid
			INNER JOIN tblroles r2 on u2.urole = r2.id 
			WHERE  m2.managerid NOT IN (@UserId)
		)


INSERT INTO #tempLoan
	Select distinct 'E3' DB,
	M.LoanNumber loan_no ,
	M.CurrentStatus,
	left(M.CurrentStatus,2)  LoanStatus,
	M.lastname BorrowerLastName,
	1 YNValue,
	M.DecisionStatus,
	'NA' UnderwriterType,
	loanData.AdjustedNoteAmt,
	appStat.StatusDateTime AS SubmittedDate,
	M.firstname BorrowerFirstName 
	from 
		(Select M.*,borr.* from mwlloanapp M
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id,lastname,firstname from mwlBorrower group by loanapp_id,lastname,firstname) as borr on borr.loanapp_id = M.id
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id from mwlBorrower group by loanapp_id) as borrS on borr.loanapp_id = borrs.loanapp_id and  borrs.SequenceNum = borr.SequenceNum
	where (ltrim(rtrim(LoanNumber)) Like @LoanNumber + '%' or borr.LastName LIKE @LoanNumber +'%') and ChannelType LIKE 'Ret%'  
	and Originator_id in(select E3userid from ReporteCTE)
	) M
	INNER JOIN dbo.mwlloandata as loanData on loanData.ObjOwner_id = M.id
	LEFT JOIN mwlAppStatus appStat on M.ID=appStat.LoanApp_id and appStat.StatusDesc='01 - Registered' 
	LEFT JOIN mwlApprovalStatus approvalStat on M.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved'
END
-------------------------------Retail Ops Manager and Retail Team Lead-----------------------------
ELSE IF(@Role in(16,11))
BEGIN


INSERT INTO #tempLoan
	Select distinct 'E3' DB,
	M.LoanNumber loan_no ,
	M.CurrentStatus,
	left(M.CurrentStatus,2)  LoanStatus,
	M.lastname BorrowerLastName,
	1 YNValue,
	M.DecisionStatus,
	'NA' UnderwriterType,
	loanData.AdjustedNoteAmt,
	appStat.StatusDateTime AS SubmittedDate,
	M.firstname BorrowerFirstName 
	from 
		(Select M.*,borr.* from mwlloanapp M
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id,lastname,firstname from mwlBorrower group by loanapp_id,lastname,firstname) as borr on borr.loanapp_id = M.id
	INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id from mwlBorrower group by loanapp_id) as borrS on borr.loanapp_id = borrs.loanapp_id and  borrs.SequenceNum = borr.SequenceNum
	where (ltrim(rtrim(LoanNumber)) Like @LoanNumber + '%' or borr.LastName LIKE @LoanNumber +'%') and ChannelType LIKE 'Ret%'  
	) M
	INNER JOIN dbo.mwlloandata as loanData on loanData.ObjOwner_id = M.id
	LEFT JOIN mwlAppStatus appStat on M.ID=appStat.LoanApp_id and appStat.StatusDesc='01 - Registered' 
	LEFT JOIN mwlApprovalStatus approvalStat on M.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved'
END
Select * from #tempLoan  
where (ISNUMERIC(LoanStatus) = 1 and  LoanStatus <=  29) --or LoanStatus LIKE 'RE%'
drop table #tempLoan


END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tblGetAuthenticateValue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tblGetAuthenticateValue]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Select_tblGetAuthenticateValue]
(
@strPsCellNumber VARCHAR(Max)
)
AS
BEGIN

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT
	     userid
		,urole
		,UserLoginid
	FROM tblUsers
	WHERE (
			(	uemailid = @strPsCellNumber
				AND UserLoginid IS NULL
			)
			OR UserLoginid = @strPsCellNumber
			)
		AND userid NOT IN (
			SELECT refuserid
			FROM tblTempUser WHERE ISACTIVE = 1
			)
		AND urole NOT IN (6,9) -- Added to block Text My Rate User from log in
END



GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tblGetInfoFromTempUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tblGetInfoFromTempUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Select_tblGetInfoFromTempUser]
(
@strPsCellNumber varchar(Max)
)
as
begin

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT temp.refuserid,temp.tempuserid,temp.Tempid,temp.temppswd,temp.createdDate,temp.IsActive  
FROM tblTempUser temp left join tblusers users on  temp.refuserid= users.userid
WHERE (tempuserid = @strPsCellNumber)  and temp.IsActive=1 and users.IsActive=1
end


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_select_tblGetUserloginID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_select_tblGetUserloginID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE PROCEDURE [dbo].[usp_select_tblGetUserloginID]
(
@strLoginID varchar(Max)
)
as
begin

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select * from tblusers where userloginid =@strLoginID
end


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_update_tblAuthenticate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_update_tblAuthenticate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_update_tblAuthenticate]
(
@tempid varchar(Max),
@refuserid varchar(Max)
)
as
begin
update tblTempUser set IsActive = 0 
where tempid = @tempid

update tblusers set IsActive = 0 where userid = @refuserid
END 


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Update_tblforceChangePassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Update_tblforceChangePassword]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE PROCEDURE [dbo].[usp_Update_tblforceChangePassword]
(
@newPass varchar(Max),
@passExpDate varchar(Max),
@userid varchar(Max)
)
as
begin
update tblusers set upassword =@newPass, PasswordExpDate=@passExpDate where userid = @userid
end  


GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_update_tblTracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_update_tblTracking]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc usp_update_tblTracking (@UserID int)
AS
BEGIN
 DECLARE @LastID BIGINT
 SET @LastID = (SELECT top 1 ID FROM tblTracking where UserID=@UserID ORDER BY ID DESC)
 UPDATE tblTracking SET LogoutTime = GETDATE() where Id = @LastID
END

GO
USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidateUserForKatalystBranchOffice]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ValidateUserForKatalystBranchOffice]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Shwetal Shah>
-- Create date: <25-04-2013>
-- Description:	<Check the specific User's branch office has Katalyst enabled or not>
-- =============================================
create PROCEDURE [dbo].[ValidateUserForKatalystBranchOffice]
(
 @UserID int
)
AS
BEGIN

	Select ISNULL(IsKatalyst,0) as IsKatalyst from E3KatalystBranchOffice WITH ( NOLOCK )
	Inner Join (SELECT dbo.mwcInstitution.ID,dbo.mwcInstitution.Office FROM dbo.mwcInstitution WITH ( NOLOCK )
		INNER JOIN dbo.mwcContact contact ON dbo.mwcInstitution.ID = contact.Institution_ID
		INNER JOIN dbo.mwaMWUser mwuser ON contact.EMail = mwuser.EMail
		WHERE dbo.mwcInstitution.Office LIKE 'Retail%' AND mwuser.ID=(Select E3UserId from tblUsers WITH ( NOLOCK ) Where UserID = @UserID)) Institution 
	ON Institution.ID = E3KatalystBranchOffice.E3BranchOfficeID


END

GO
