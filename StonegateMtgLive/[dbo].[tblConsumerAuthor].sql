USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblConsumerAuthor]') AND type in (N'U'))
DROP TABLE [dbo].[tblConsumerAuthor]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblConsumerAuthor](	  [CAID] INT NOT NULL IDENTITY(1,1)	, [AppID] INT NULL	, [IUnderstand] BIT NULL	, [IfApplicable] BIT NULL	, [IAcknowledge] BIT NULL	, [IAuthorise] BIT NULL	, [ApplicantOnly] BIT NULL	, [IHereby] BIT NULL	, [OtherNameUsed] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SSNNo] VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [StreetAddress] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DobDay] VARCHAR(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DobMonth] VARCHAR(12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DobYear] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ConCity] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ConState] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ConZip] VARCHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LsnNoState] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [LsnNoName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [acceptterm] BIT NULL	, [ConAppName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ConAppDate] DATETIME NULL)USE [HomeLendingExperts]
GO

