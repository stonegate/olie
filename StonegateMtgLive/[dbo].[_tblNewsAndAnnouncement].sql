USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_tblNewsAndAnnouncement]') AND type in (N'U'))
DROP TABLE [dbo].[_tblNewsAndAnnouncement]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[_tblNewsAndAnnouncement](	  [ID] INT NOT NULL IDENTITY(1,1)	, [Title] NVARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Body] TEXT(16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsActive] BIT NULL	, [IsNewsAnnouncement] BIT NULL	, [CreatedBy] INT NULL	, [CreatedDate] DATETIME NULL	, [IPAddress] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Role] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [NewsAnnousVisbleAd] BIT NULL	, [NewsAnnousVisbleDR] BIT NULL	, [NewsAnnousVisbleBR] BIT NULL	, [NewsAnnousVisbleLO] BIT NULL	, [NewsAnnousVisbleCU] BIT NULL	, [NewsAnnousVisbleRE] BIT NULL	, [NewsAnnousVisbleAM] BIT NULL	, [NewsAnnousVisbleDM] BIT NULL	, [NewsAnnousVisbleROM] BIT NULL	, [NewsAnnousVisbleCO] BIT NULL	, [NewsAnnousVisbleCRM] BIT NULL	, [NewsAnnousVisbleLP] BIT NULL	, [NewsAnnousVisbleCL] BIT NULL	, [NewsAnnousVisbleRTL] BIT NULL	, [CC_ID] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [SiteStatus] VARCHAR(2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RoleC] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RoleR] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO

