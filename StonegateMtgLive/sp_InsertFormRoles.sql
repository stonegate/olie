USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertFormRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertFormRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertFormRoles]
(
@formID int ,@uroleid int
)
AS

BEGIN
	INSERT INTO tblformRoles (formID,uroleid)  values (@formID,@uroleid)
END


GO

