USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBorrowerLastName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBorrowerLastName]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetBorrowerLastName]
(
@loan_no VARCHAR(MAX)
)
AS
BEGIN
select cb.borr_last from loandat LD inner join cobo Cb on ld.record_id = cb.parent_id  where ld.loan_no = @loan_no
END

GO

