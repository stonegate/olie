USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Assign_Managers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Assign_Managers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Rakesh Kumar Mohanty>
-- Create date: <Create Date,9th May 2014>
-- Description:	<Description,,>
--Exec sp_Assign_Managers 79,'2184,76,11427,20875',79
-- =============================================
CREATE PROCEDURE [dbo].[sp_Assign_Managers]
	-- Add the parameters for the stored procedure here
	@UserID int,
	@ManagerIds Nvarchar(max),
	@CreatedBy Nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    -- Insert statements for procedure here
	DELETE FROM [dbo].[tblUserManagers] WHERE UserId = @UserID 
	AND ManagerId NOT IN (SELECT Id.Items FROM [dbo].SplitString(@ManagerIds, ',') Id)
	
	INSERT INTO [dbo].[tblUserManagers] ([UserId], [ManagerId], [CreatedBy], [CreatedOn])
		SELECT @UserID,Id.Items,@CreatedBy, GetDate() FROM [dbo].SplitString(@ManagerIds, ',') Id
		where Id.Items NOT IN (SELECT UM.ManagerId FROM [dbo].[tblUserManagers] UM WHERE UM.UserId = @UserID)
		

END



GO

