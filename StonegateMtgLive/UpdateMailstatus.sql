USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateMailstatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateMailstatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[UpdateMailstatus]  
 -- Add the parameters for the stored procedure here  
@ID int,
@status int
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
update tblEmailServiceReader set updStatus=@status where Id=@ID
 --select TOP 1 * from tblEmailServiceReader where updStatus=0 order by RcvdDate desc  
END  


GO

