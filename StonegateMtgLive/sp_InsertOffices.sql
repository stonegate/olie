USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertOffices]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertOffices]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsOffice.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertOffices]
(
@lt_office_id varchar(Max),
@prouid varchar(Max),
@userid varchar(Max)

)
AS
BEGIN

	 Insert into tblOffices(lt_office_id,prouid,userid) Values 
(@lt_office_id,@prouid,@userid)   
END

GO

