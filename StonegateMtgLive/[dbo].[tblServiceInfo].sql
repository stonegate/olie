USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblServiceInfo]') AND type in (N'U'))
DROP TABLE [dbo].[tblServiceInfo]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblServiceInfo](	  [record_id] INT NOT NULL IDENTITY(1,1)	, [UserId] INT NOT NULL	, [LoanNo] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [IsActive] BIT NULL	, [IsSMSPaymentReminderAlert] BIT NULL	, [IsSMSPaymentPostedAlert] BIT NULL	, [IsEmailPaymentReminderAlert] BIT NULL	, [IsEmailPaymentPostedAlert] BIT NULL	, [IsPaymentReminderAlert] BIT NULL	, [IsPaymentPostedReminderAlert] BIT NULL	, [PaymentReminderDay] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PaymentPostedReminderDay] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PaymentReminderdate] DATETIME NULL	, [PaymentPostedReminderdate] DATETIME NULL	, [SMSPaymentReminderdate] DATETIME NULL	, [SMSPaymentPostedReminderdate] DATETIME NULL	, [IsSMSPaymentPastDue] BIT NULL DEFAULT((0))	, [IsEmailPaymentPastDue] BIT NULL DEFAULT((0))	, [IsPaymentPastDue] BIT NULL DEFAULT((0))	, [PaymentPastDueReminderDay] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PaymentPastDueDate] DATETIME NULL)USE [HomeLendingExperts]
GO

