USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckDuplicateCategory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckDuplicateCategory]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_CheckDuplicateCategory]
(
@CateName varchar (100)
)
AS
BEGIN
	 select count(catid) tot from tblCategories where CateName=@CateName
END

GO

