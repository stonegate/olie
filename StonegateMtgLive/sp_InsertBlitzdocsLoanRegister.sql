USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertBlitzdocsLoanRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertBlitzdocsLoanRegister]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertBlitzdocsLoanRegister]
	-- Add the parameters for the stored procedure here
(
@Comments varchar(500),
@userid int,
@DocName varchar(100),
@loan_no char(15) 
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
Insert into tblloandoc(Comments,userid,DocName,loan_no) Values(@Comments,@userid,@DocName,@loan_no)


END


GO

