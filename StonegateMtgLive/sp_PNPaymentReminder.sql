USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PNPaymentReminder]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PNPaymentReminder]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Harsh Pandit
-- Create date: 25 March 2010
-- Modified date: 29 April 2010
-- Description:	to get data for Payment Reminder
--Modify by : Vipul Thakkar
--Modified Date: 10 OCT 2011
-- =============================================
CREATE PROCEDURE [dbo].[sp_PNPaymentReminder] 
AS
BEGIN
	SET NOCOUNT ON;
	select convert(varchar(10), dateadd(day,- PaymentReminderDay * 1 ,duedate ),101)as PaymentReminderNotificationDate, DATEDIFF(DAY,getdate(),duedate), PaymentReminderDay, record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,  
		serviceinfo.userid,  isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt,  loanid,	  convert(varchar, duedate, 1) as duedate,
		DATEDIFF(DAY,getdate(),duedate) as remainingDays,    IsSMSPaymentReminderAlert,IsEmailPaymentReminderAlert,
		IsPaymentReminderAlert,PaymentReminderDay,  PaymentReminderdate,    
		mobile_ph, carrierid, (select carrieremailsms from carrier where id = carrierid) as smsgateway
	from [winserver].[service].dbo.loan     
		join tblServiceInfo as serviceInfo on loan.loanid = serviceInfo.loanno    
		join tblUsers as users on users.userid = serviceinfo.userid    
	where 
		--uemailid='meggeman@neo.rr.com'
--start  comment
(month(PaymentReminderdate) <> month(getdate()) or PaymentReminderdate is null)     
and IsPaymentReminderAlert = 1   
--and DATEDIFF(DAY,getdate(),duedate) = (PaymentReminderDay*-1)
	--and DATEDIFF(DAY,getdate(),duedate) = (PaymentReminderDay*-1) --  comment old and include below line  for
--Sending before due  date days
and convert(varchar(10), dateadd(day,- PaymentReminderDay * 1 ,duedate ),101)=convert(varchar(10), getdate(),101)
--end  comment
--select record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,  
--serviceinfo.userid,isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt,  loanid,
--convert(varchar, duedate, 1) as duedate,DATEDIFF(DAY,getdate(),duedate) as remainingDays,  
--IsSMSPaymentReminderAlert,IsEmailPaymentReminderAlert,IsPaymentReminderAlert,PaymentReminderDay,PaymentReminderdate,
--mobile_ph, carrierid,
--	(select carrieremailsms from carrier where id = carrierid) as smsgateway  
--from [WINSERVER].[service].dbo.loan   
--join tblServiceInfo as serviceInfo on loan.loanid = serviceInfo.loanno  
--join tblUsers as users on users.userid = serviceinfo.userid  
--where (month(PaymentReminderdate) <> month(getdate()) or PaymentReminderdate is null)   
--and DATEDIFF(DAY,getdate(),duedate) >= 0  
--and IsPaymentReminderAlert = 1 
--and DATEDIFF(DAY,getdate(),duedate) <= PaymentReminderDay 
--	select DATEDIFF(DAY,getdate(),duedate),  record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,  
--	serviceinfo.userid,  isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt,  loanid,	  convert(varchar, duedate, 1) as duedate,
--	DATEDIFF(DAY,getdate(),duedate) as remainingDays,    IsSMSPaymentReminderAlert,IsEmailPaymentReminderAlert,
--	IsPaymentReminderAlert,PaymentReminderDay,  PaymentReminderdate,    
--	mobile_ph, carrierid,
--	(select carrieremailsms from carrier where id = carrierid) as smsgateway
--	from [WINSERVER].[service].dbo.loan     
--	join tblServiceInfo as serviceInfo on loan.loanid = serviceInfo.loanno    
--	join tblUsers as users on users.userid = serviceinfo.userid    
--	where (month(PaymentReminderdate) <> month(getdate()) or PaymentReminderdate is null)     
--	and IsPaymentReminderAlert = 1   and ( DATEDIFF(DAY,getdate(),duedate) <= PaymentReminderDay and PaymentReminderdate is null)
END




GO

