USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbllookups]') AND type in (N'U'))
DROP TABLE [dbo].[tbllookups]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbllookups](	  [lookupid] BIGINT NOT NULL IDENTITY(1,1)	, [Lookupname] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Description] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tbllookups] PRIMARY KEY ([lookupid] ASC))USE [HomeLendingExperts]
GO

