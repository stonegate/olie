USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStoneGateUserData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStoneGateUserData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsUserRegistrationE3.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetStoneGateUserData]
(
@uidprolender varchar(Max)
)
AS
BEGIN
	 select * from tblusers where uidprolender =@uidprolender
END

GO

