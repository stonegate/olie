USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCategories]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCategories]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_GetCategories]
AS
BEGIN
	 select CatID, CateName from tblCategories
END

GO

