USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_News_TPO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_News_TPO]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
  
CREATE procEDURE [dbo].[sp_Update_News_TPO]     
(    
@id int,          
@Title nvarchar(1000),       
@Body text,        
@IsActive bit,        
@CreatedDate datetime,        
@Role nvarchar(Max),  
@RoleC nvarchar(Max) ,
@RoleR nvarchar(Max) 
)    
AS    
BEGIN  
UPDATE Tpo_tblNewsAndAnnouncement WITH (UPDLOCK)  
SET Title = @Title,  
 Body = @Body,  
 IsActive = @IsActive,  
 Role = @Role,  
  RoleC = @RoleC, 
   RoleR = @RoleR, 
 ModifiedDate = @CreatedDate  
WHERE id = @id  
END    
    

  

GO

