USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanDocRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanDocRegister]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanDocRegister]

@loanregid int,
@uloanregid int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select loanregid as loanregid,filename as DocName  from tblloanreg where loanregid =@loanregid union select uloanregid as loanregid,DocName as DocName  from tblloandoc where uloanregid =@uloanregid
END


GO

