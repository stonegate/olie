USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getSceondryBorrowerInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getSceondryBorrowerInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_getSceondryBorrowerInfo]
(
@LoanID VARCHAR(MAX)
)
AS
BEGIN
SELECT FirstMiddleName +' '+ LastName as FirstMiddleName FROM dbo.Borrower WHERE BorrowerID = '2' AND LoanID = @LoanID
END




GO

