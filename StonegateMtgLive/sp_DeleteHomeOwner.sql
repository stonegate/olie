USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteHomeOwner]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteHomeOwner]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteHomeOwner]
(
@formID int
)
AS
BEGIN

	SET NOCOUNT ON;

Delete from tblFormsHomeowner where formID =@formID
END

--------------------------------------------------------------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[sp_CheckDuplicatePackageId]    Script Date: 04/12/2013 17:42:06 ******/
SET ANSI_NULLS ON

GO

