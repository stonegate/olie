USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetapplicationData1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetapplicationData1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--[sp_GetapplicationData1] 2,50,'AppFName',''  
CREATE PROCEDURE [dbo].[sp_GetapplicationData1]   
(  
 @pageNum int  
,@pageSize int  
,@orderby varchar(2000)  
,@FilterExp varchar(2000)
)AS  
  
SET NOCOUNT ON   

declare @DESCorASC varchar(50)     
declare @lownum nvarchar(10)  
declare @highnum nvarchar(10)  
declare @sql nvarchar(4000)  
declare @sqlcount nvarchar(4000)   
set @lownum = convert(nvarchar(10), (@pagesize * (@pagenum - 1)))  
set @highnum = convert(nvarchar(10), (@pagesize * @pagenum))  

if @orderby = ''
begin
	set @orderby = 'DateEntered'
	set @DESCorASC = 'DESC'
end
else
	if @orderby = 'DateEntered'
	begin
		set @DESCorASC = 'DESC'
	end
	else
		set @DESCorASC = 'ASC'
print @DESCorASC
if @FilterExp = ''
Begin  
set @sql = 'select LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp, StateName   from (  
  select row_number() over (order by ' + @orderby + ' ' + @DESCorASC + ') as rownum  
         ,LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp,tblStateNylex.StateName   
  from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID    
  
 ) A WHERE RowNum > ' + @lownum + ' AND rownum <= ' + @highnum  
  --print @sql
--WHERE rownum >  @pagesize * (@pageNum - 1) AND rownum <=  (@pagesize * @pageNum)  
end
else
Begin
set @sql = 'select LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp, StateName   from (  
  select row_number() over (order by ' + @orderby + ' ' + @DESCorASC + ') as rownum   
         ,LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp,tblStateNylex.StateName   
  from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID where ' + @FilterExp +' ) A WHERE RowNum > ' + @lownum + ' AND rownum <= ' + @highnum  
End  
--print @sql
-- Execute the SQL query  

if @FilterExp <> ''
set @sqlcount = 'select count(loanapplicationID) as totalrec from tblLoanApplication  left join tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID where ' + @FilterExp         
else
set @sqlcount = 'select count(loanapplicationID) as totalrec from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID'  
  

EXEC sp_executesql @sql
EXEC sp_executesql @sqlcount 
  



set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON

GO

