USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFormsHomeowner]') AND type in (N'U'))
DROP TABLE [dbo].[tblFormsHomeowner]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFormsHomeowner](	  [formid] INT NOT NULL IDENTITY(1,1)	, [Description] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FileName] VARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Loantype] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Packageid] INT NULL	, [dttime] DATETIME NULL	, CONSTRAINT [PK_tblFormsHomeowner] PRIMARY KEY ([formid] ASC))USE [HomeLendingExperts]
GO

