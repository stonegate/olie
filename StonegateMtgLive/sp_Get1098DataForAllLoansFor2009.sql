USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get1098DataForAllLoansFor2009]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get1098DataForAllLoansFor2009]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_Get1098DataForAllLoansFor2009]
(
@loanid varchar (max)
)
AS
BEGIN
select loanid,reportedTIN as TaxIDNumber,ReportedFirstname as firstmiddlename,ReportedLastName as lastname,
MailLine1 as addressline1,MailLine2 as addressline2,MAilCity as city,MailState as state,MailZip as zip,
ReportableAmount AS totalintcoll,PointsPaid,ARMIntReimbursed,PMIPaid,PrincipalBal,princollected,NegativeAmBal,AssistanceBal, 
BeginEscBal,EscDepAmt,TaxesPaid1 , TaxesPaid,InsurancePaid,EscDisbAmt AS OtherFundsBal,escrowbal,IntOnEscrow,
IOEWithholding from dbo.r_annual_stmt
where reportingyear = '2009' and loanid in (@loanid) order by reportingdate desc 


END


GO

