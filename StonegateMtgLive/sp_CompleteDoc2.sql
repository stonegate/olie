USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CompleteDoc2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CompleteDoc2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_CompleteDoc2]
(
@CompletedBy varchar(max),
@loanregid varchar(max)
)
AS
BEGIN
update tblLoanReg set Completed = 1,CompletedBy=@CompletedBy,TimeStamp=getdate() where loanregid = @loanregid


END



GO

