USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateKatalystFlagInBranchOffices]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateKatalystFlagInBranchOffices]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Shwetal Shah>
-- Create date: <25-04-2013>
-- Description:	<To update Katalyst Rights applied by Admin to specific branch office>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateKatalystFlagInBranchOffices]
(
	@E3BranchOfficeID varchar(32),
	@E3BranchOffice varchar(200),
	@IsKatalyst bit
)
AS
BEGIN
	IF Exists (Select ID From E3KatalystBranchOffice WITH ( NOLOCK ) Where E3BranchOfficeID = @E3BranchOfficeId)
	Begin
		Update E3KatalystBranchOffice WITH (UPDLOCK) Set IsKatalyst = @IsKatalyst Where E3BranchOfficeID = @E3BranchOfficeID
	End
	Else
	Begin
		Insert Into E3KatalystBranchOffice(ID,E3BranchOfficeID,E3BranchOffice,IsKatalyst)
		Select ISNULL((Select MAX(ID) From E3KatalystBranchOffice),0)+1, @E3BranchOfficeID, @E3BranchOffice, @IsKatalyst
	End
END


GO

