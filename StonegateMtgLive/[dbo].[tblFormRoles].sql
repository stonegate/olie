USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFormRoles]') AND type in (N'U'))
DROP TABLE [dbo].[tblFormRoles]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFormRoles](	  [formRoleID] INT NOT NULL IDENTITY(1,1)	, [formID] INT NULL	, [uroleid] INT NULL	, CONSTRAINT [PK_tblFormRoles] PRIMARY KEY ([formRoleID] ASC))USE [HomeLendingExperts]
GO

