USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBranchesForKatalyst]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetBranchesForKatalyst]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Shwetal Shah>
-- Create date: <10-04-13>
-- Description:	<To Get list of Branches from E3 to on / off Katalyst functionality>
-- =============================================
CREATE PROCEDURE [dbo].[GetBranchesForKatalyst]

AS
BEGIN

	SELECT ID as BranchID,Office,(Select Name from tblState WITH ( NOLOCK ) where abbr = mwcInstitution.State)+' - '+State as States 
	,Case When (Select ISNULL(IsKatalyst,0) From E3KatalystBranchoffice  WITH ( NOLOCK ) Where E3BranchOfficeID = dbo.mwcInstitution.ID) = 1 Then 'Allowed' Else 'Not Allowed' End as IsKatalyst
	FROM dbo.mwcInstitution WITH ( NOLOCK )
	WHERE Office LIKE 'Retail%' Order By dbo.mwcInstitution.Office

END



GO

