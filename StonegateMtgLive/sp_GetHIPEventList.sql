USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHIPEventList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHIPEventList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

                
CREATE PROCEDURE [dbo].[sp_GetHIPEventList]               
(                  
@sortField varchar(500),                  
@sortOrder varchar(100),                  
@whereClause varchar(1000),                  
@PageNo int,                   
@PageSize int                  
)                   
AS                  
BEGIN                  
IF @sortField  = ''                                              
  set @sortField = 'tblrsvpedits.EventID'            
                  
SET @sortField = @sortField + ' ' + @sortOrder               
                 
Declare @sql nvarchar(4000)            
                  
set @sql = ''                                              
     set @sql = 'Select count(*) from ( '                  
     set @sql =  @sql + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,                  
EventID, CreatedDate, EventName, EventDate, (EventCity+'', ''+EventState)as Location,uniqueurl,        
case Status When  0 then ''New'' when 1 then ''Publish'' When 2 then ''InActive'' end as Status ,status as status1              
FROM tblRSVPEdits '                  
                  
IF @whereClause <> ''                                                       
    BEGIN                                                   
    set @sql = @sql  + ' Where ' + @whereClause                                             
    END                                      
                  
set @sql = @sql + ' group by EventID, CreatedDate, EventName, EventDate, EventCity, EventState, Status ,uniqueurl'                                               
 set @sql = @sql + ' ) as t2 '                   
                  
print @sql                  
exec sp_executesql @sql                    
                  
                  
set @sql = ''                                              
     set @sql = 'Select * from ( '                   
set @sql =  @sql + 'SELECT                           
        ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,EventID, CreatedDate, EventName, EventDate, (EventCity+'', ''+EventState)as Location,         
case Status When  0 then ''UnPublish'' when 1 then ''Published'' When 2 then ''InActive'' end as Status ,status as status1    ,uniqueurl                  
FROM tblRSVPEdits '                  
                  
IF @whereClause <> ''                                                       
    BEGIN                                                   
    set @sql = @sql  + ' Where ' + @whereClause                                             
    END                                      
                  
set @sql = @sql + '  group by  EventID, CreatedDate, EventName, EventDate, EventCity, EventState, Status ,uniqueurl '                                               
set @sql = @sql + ' ) as t2 '                  
                  
set @sql = @sql + ' Where Row between (' + CAST( @PageNo AS NVARCHAR)  + ' - 1) * ' + Cast(@PageSize as nvarchar) + ' + 1 and ' + cast(@PageNo as nvarchar) + ' * ' + cast(@PageSize as nvarchar)                  
                  
print @sql                                            
exec sp_executesql @sql                  
                  
END 

GO

