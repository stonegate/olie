USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetapplicationData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetapplicationData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--[sp_GetapplicationData] 1,50,'LoanApplicationID','([AppFName] like ''drew'')'  
CREATE PROCEDURE [dbo].[sp_GetapplicationData]   
(  
 @pageNum int  
,@pageSize int  
,@orderby varchar(2000)  
,@FilterExp varchar(2000)  
)AS  
  
SET NOCOUNT ON   
   
declare @lownum nvarchar(10)  
declare @highnum nvarchar(10)  
declare @sql nvarchar(4000)  
declare @sqlcount nvarchar(4000)   
set @lownum = convert(nvarchar(10), (@pagesize * (@pagenum - 1)))  
set @highnum = convert(nvarchar(10), (@pagesize * @pagenum))  

if @orderby = ''
set @orderby = 'DateEntered'


if @FilterExp = ''
Begin  
set @sql = 'select LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp, StateName   from (  
  select row_number() over (order by ' + @orderby  + ') as rownum  
         ,LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp,tblStateNylex.StateName   
  from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID    
  
 ) A WHERE RowNum > ' + @lownum + ' AND rownum <= ' + @highnum  
  print @sql
--WHERE rownum >  @pagesize * (@pageNum - 1) AND rownum <=  (@pagesize * @pageNum)  
end
else
Begin
set @sql = 'select LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp, StateName   from (  
  select row_number() over (order by ' + @orderby  + ') as rownum   
         ,LoanApplicationID, DateEntered,  AppFName, AppLName, AppEmail,UserAppearUrl,isfullapp,tblStateNylex.StateName   
  from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID where ' + @FilterExp +' ) A WHERE RowNum > ' + @lownum + ' AND rownum <= ' + @highnum  
End  
print @sql
-- Execute the SQL query  

if @FilterExp <> ''
set @sqlcount = 'select count(loanapplicationID) as totalrec from tblLoanApplication  left join tblStateNylex ON tblLoanApplication.StateID = tblStateNylex.StateID where ' + @FilterExp         
else
set @sqlcount = 'select count(loanapplicationID) as totalrec from tblLoanApplication left join tblStateNylex  ON tblLoanApplication.StateID = tblStateNylex.StateID'  
  

EXEC sp_executesql @sql 
EXEC sp_executesql @sqlcount 
  



set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON

GO

