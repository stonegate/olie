USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblUserInfoForNewProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblUserInfoForNewProcess]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[sp_Select_tblUserInfoForNewProcess]
(
@userid varchar(50)
)
as
begin
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	select top 1 userid,userloginid,Uemailid,upassword,ufirstname,ulastname from tblusers where userid=@userid
END


GO

