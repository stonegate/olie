USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUserPasswordHistory]') AND type in (N'U'))
DROP TABLE [dbo].[tblUserPasswordHistory]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserPasswordHistory](	  [PasswordID] INT NOT NULL IDENTITY(1,1)	, [UserID] INT NULL	, [Password] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblUserPasswordHistory] PRIMARY KEY ([PasswordID] ASC))USE [HomeLendingExperts]
GO

