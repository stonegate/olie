USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetKatalystLoanFolderID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetKatalystLoanFolderID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Shwetal Shah>
-- Create date: <01-04-13>
-- Description:	<Fatch Katalyst Folder ID from custome field of E3 DB>
-- mwfieldNum = 9553 used to fatch katalyst folder id
-- =============================================
CREATE PROCEDURE [dbo].[GetKatalystLoanFolderID]
(
	@LoanNumber varchar(20)
)
AS
BEGIN
	
	Select StringValue from mwlCustomField WITH ( NOLOCK ) where LoanApp_ID = (Select ID from mwlLoanApp WITH ( NOLOCK ) where LoanNumber = @LoanNumber) 
		and CustFieldDef_ID = (Select ID from mwsCustFieldDef WITH ( NOLOCK ) Where MWFieldNum=9553)
	
	
END


GO

