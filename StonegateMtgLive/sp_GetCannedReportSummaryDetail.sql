USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCannedReportSummaryDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCannedReportSummaryDetail]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [sp_GetCannedReportSummaryDetail] 1,'','funded','','','01 - Registered','2011','','4'    
      
CREATE PROCEDURE [dbo].[sp_GetCannedReportSummaryDetail]          
(          
@RoleID int,         
@strID varchar(2000),           
@From varchar(15),        
@BrokerId char(2)= null,        
@TeamOPuids varchar(30),        
@StatusDesc varchar(100),        
@Year varchar(4),         
@oPuid varchar(100),        
@Month varchar(4)        
)          
AS          
BEGIN          
          
Declare @SQL nvarchar(max)          
Set @SQL =''          
Set @SQL ='select ''E3'' as DB,loanapp.ID as record_id,CONVERT(DECIMAL(10,3),(loanData.NoteRate * 100)) as int_rate,LoanNumber as loan_no,        
   borr.lastname as BorrowerLastName,        
   CONVERT(DECIMAL(10,2),isnull(loanData.LTV * 100,0))LTV,CurrentStatus as LoanStatus,convert(varchar(35),        
   loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,case LoanData.FinancingType WHEN ''F'' THEN ''FHA''         
   WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,         
   case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as Purpose,          
   CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,loanData.UseCashInOut as CashOut,loanData.AdjustedNoteAmt as loan_amt,'         
        
                if (@From = 'locked')         
                     Set @SQL = @SQL + ' LockRecord.Status as LockStatus,LockRecord.LockExpirationDate,LockRecord.LockDateTime,LockRecord.LockDateTime as LOCK_DATE '                        
                else         
                     Set @SQL = @SQL + '   '''' as LockStatus,loanapp.LockExpirationDate,loanapp.LockDate as LOCK_DATE'        
                        
    Set @SQL = @SQL + ' from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and  borr.sequencenum=1  '        
    Set @SQL = @SQL + ' Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id  '        
    Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''Branch'' and Broker.objownerName=''BranchInstitution'' '        
    Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' '        
    Set @SQL = @SQL + ' Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc=''' + @StatusDesc +'''  and AppStatus.sequencenum=1 '        
        
               if (@From = 'locked')        
    BEGIN        
                    Set @SQL = @SQL + ' left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.locktype=''LOCK'''         
                END        
        
                Set @SQL = @SQL + ' where borr.sequencenum=1 and  loanapp.ChannelType=''RETAIL'' and '        
           
    
                if (@BrokerId != '' and @BrokerId != 0)        
                BEGIN        
                      Set @SQL = @SQL + '(Broker.CompanyEmail ='''+ @BrokerId +''' or Corres.CompanyEmail ='''+ @BrokerId +''') and '        
                END        
        
                if (@From = 'funded')        
               BEGIN        
                        Set @SQL = @SQL + ' Year(AppStatus.StatusDateTime)='''+ @Year +''' and Month(AppStatus.StatusDateTime)='''+ @Month +''''        
                END        
                else if (@From = 'locked')        
                BEGIN        
                      Set @SQL = @SQL + ' Year(LockRecord.LockDateTime)='''+ @Year +''' and Month(LockRecord.LockDateTime)='''+ @Month +''''        
                END        
        
                if (@TeamOPuids = 'broker')        
                BEGIN        
                      Set @SQL = @SQL + ' and loanapp.ChannelType=''BROKER'''         
                END        
                if (@TeamOPuids = 'retail')        
     BEGIN        
                       Set @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL'''         
                END        
                if (@TeamOPuids = 'correspond')        
                BEGIN        
                       Set @SQL = @SQL + ' and loanapp.ChannelType=''CORRESPOND'''         
                END        
                        
                Set @SQL = @SQL + ' and loanapp.ChannelType like ''%RETAIL%'' '         
                         
                if (@TeamOPuids != '' and @TeamOPuids != 'retail' and @TeamOPuids != 'broker' and @TeamOPuids != 'correspond')        
                BEGIN        
                     Set @SQL = @SQL + '  and (Originator_id in (''' + @TeamOPuids  + ''')or loanapp.id in (''' + @strID + ''')or Broker.office in('''+ @TeamOPuids +'''))'        
                END        
                else        
                  BEGIN        
                    if (@RoleID != 0 and @RoleID != 1 and @RoleID != 11)        
     BEGIN        
                         if (len(@strID) = 0)         
   BEGIN        
                                     
        if (@RoleID = 3)        
                                      
         Set @SQL = @SQL + '  and (loanapp.ID in (''' + @strID + ''') or Originator_id in (''' + @oPuid + ''') )'        
                                    
        
        if (@RoleID = 10 or  @RoleID = 7)         
          
         Set @SQL = @SQL + '  and (Originator_id in (''' + @strID + ''') or loanapp.id in (''' + @strID + '''))'        
                                      
        if (@RoleID = 2)         
         Set @SQL = @SQL + '  and (Originator_id in (''' + @strID + '''))'         
                                    
       END        
                        else        
       BEGIN         
        if (@RoleID = 3)         
           Set @SQL = @SQL + '  and (loanapp.ID in (''' + @strID + ''') or Originator_id in (''' + @oPuid + ''') )'        
                                      
        if (@RoleID =  10 or @RoleID = 7)        
                                   Set @SQL = @SQL + ' and (Originator_id in (''' + @strID + ''') or loanapp.id in (''' + @strID + '''))'        
                                      
        if (@RoleID = 2)         
                                  Set @SQL = @SQL + ' and (Originator_id in (''' + @strID + '''))'        
       END          
                         END        
     END        
                          
            
Print @SQL          
exec sp_executesql @SQL          
END 
GO

