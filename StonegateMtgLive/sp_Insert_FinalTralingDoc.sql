USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_FinalTralingDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_FinalTralingDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_Insert_FinalTralingDoc] 
(
@Comments varchar(500),
@userid int,      
@DocName varchar(200),
@loan_no varchar(15)
)
AS
BEGIN
	 INSERT INTO tblFinalTralingDoc (Comments,userid,DocName,loan_no)      
     VALUES  (@Comments,@userid,@DocName,@loan_no)       
  
END








GO

