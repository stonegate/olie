USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetEditModeData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetEditModeData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_GetEditModeData]
(
@formID int
)
AS
BEGIN
	 select Description,filename,uroleid from tblForms a, tblFormroles b where a.formID=b.formID and a.formID=@formID
END





GO

