USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCreditScore]') AND type in (N'U'))
DROP TABLE [dbo].[tblCreditScore]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCreditScore](	  [ID] INT NOT NULL IDENTITY(1,1)	, [CreditScoreID] INT NULL	, [CreditScoreValue] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CreditOrder] INT NULL)USE [HomeLendingExperts]
GO

