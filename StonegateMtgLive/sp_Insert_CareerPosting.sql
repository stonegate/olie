USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_CareerPosting]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_CareerPosting]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Created By:	Malav Shah
-- Create Date : 10/04/2012
-- Description:	Procedure for insert/update/delete careeer posting
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert_CareerPosting] 
(
@stringmode varchar(50),
@CareerId int,      
@PositionName varchar(50),   
@Description ntext,  
@PublishedDate datetime = null,
@IsPublished bit
)
AS
BEGIN
if(@stringmode = 'insert')

 INSERT INTO tblCareerPosting (PositionName,Description,ModifiedDate,PublishedDate)      
 VALUES (@PositionName,@Description,getdate(),null)    
   

else if(@stringmode = 'update')

update tblCareerPosting 
set PositionName=@PositionName,Description=@Description,ModifiedDate=getdate()
where CareerId = @CareerId 
 

else if(@stringmode = 'delete')

delete from tblCareerPosting where CareerId = @CareerId


END





GO

