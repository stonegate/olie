USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_VerifyClosingScheduleDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_VerifyClosingScheduleDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_VerifyClosingScheduleDate]
(
@LoanNo VARCHAR(MAX)
)
AS
BEGIN
select SchDate from tblScheduleClosing where LoanNo =@LoanNo

END


GO

