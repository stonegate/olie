USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCannedReportSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetCannedReportSummary]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetCannedReportSummary]        
(        
@RoleID int,       
@strID varchar(2000),        
@BrokerId char(2),      
@TeamOPuids char(15),       
@Year char(4),        
@Channel varchar(15),      
@StatusDesc varchar(1000),    
@CurrentStatus varchar(1000) 
)        
AS        
BEGIN        
        
Declare @SQL nvarchar(max)        
Set @SQL =''        
Set @SQL ='SELECT case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram ,
                 count(TransTYpe)CountRefinanance, case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when 
				TransTYpe = ''R'' then ''Refinance'' end as descriptn,     
                 month(AppStatus.StatusDateTime) as MonthYear,count(AppStatus.StatusDateTime)Total 
                 from mwlLoanApp  loanapp   
                 Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
                 Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc=''' + @StatusDesc + '''' 

                if (@BrokerId != '' AND @BrokerId != 0)
					BEGIN
						Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''' 
						Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''' 
					END

                   Set @SQL = @SQL + ' where loanapp.CurrentStatus in ( ''' + @CurrentStatus + ''') ' 

                if (@BrokerId != '' AND @BrokerId != 0)
                 
                   Set @SQL = @SQL + '  (Broker.CompanyEmail =''' + @BrokerId + ''' or Corres.CompanyEmail =''' + @BrokerId + ''') and '
                 

                if (@Channel =  'broker')
                 
                   Set @SQL = @SQL + '   and loanapp.ChannelType=''BROKER''' 
                 
                if (@Channel =  'retail')
                 
                     Set @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL''' 
                 
                if (@Channel =  'correspond')
                 
                     Set @SQL = @SQL + ' and loanapp.ChannelType=''CORRESPOND'''
                 

                if (@RoleID =  1)
                  
                     Set @SQL = @SQL + ' and loanapp.ChannelType like ''%RETAIL%'''  
                 

                if (@TeamOPuids != '')
                 
                     Set @SQL = @SQL + ' and Originator_id in (''' + @TeamOPuids + ''') '
                 
                else
                BEGIN
                    if (@RoleID != 0 AND @RoleID != 1)
                    BEGIN
                        if (len(@strID) = 0)
						BEGIN		
                            if (@RoleID =  2)
                             
                                Set @SQL = @SQL + '  and (Originator_id in ('''+ @strID +'''))'
                         END 
                        else
                          BEGIN
                            if (@RoleID =  2)
                             
                                Set @SQL = @SQL + '  and (Originator_id in ('''+ @strID +'''))'
						    END 	    
                         
                    END
                END
                 Set @SQL = @SQL + ' and  Year(AppStatus.StatusDateTime) <> ''1900'' and  Year(AppStatus.StatusDateTime)=''' + @Year + ''' group by '

                if (@BrokerId != '' AND @BrokerId != 0)
                BEGIN
                     Set @SQL = @SQL + ' Broker.CompanyEmail ,Corres.CompanyEmail , '
				END

                Set @SQL = @SQL + '  month(AppStatus.StatusDateTime),LoanData.FinancingType, TransTYpe '  
				Set @SQL = @SQL + '  SELECT month(AppStatus.StatusDateTime) as ARmonth, year(AppStatus.StatusDateTime) as ARYear  ,  count(loanapp.CurrentStatus) AS Total, sum(isnull(LoanData.AdjustedNoteAmt, 0)) as SumTotalAmount, round(avg(isnull(LoanData.AdjustedNoteAmt, 0)),0) as AvrageTotalAmount, '
				Set @SQL = @SQL + '  avg(convert(int,isnull(replace(borr.CompositeCreditScore,'','',''''),0)))  as AvrageCreditScore ,round(avg(convert(int, isnull(loanData.ltv,0))),2) as AvgLTV  '
				Set @SQL = @SQL + ' from mwlLoanApp  loanapp '
				Set @SQL = @SQL + ' Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and  borr.sequencenum=1 '
				Set @SQL = @SQL + ' Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id   '
				Set @SQL = @SQL + ' Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and StatusDesc='''+ @StatusDesc + ''''

                if (@BrokerId != '' AND @BrokerId != 0)
					BEGIN
                     Set @SQL = @SQL + '  Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker'' '
                      Set @SQL = @SQL + ' Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' '
					END

                Set @SQL = @SQL + '  where loanapp.CurrentStatus in ( '''+ @CurrentStatus + ''') ' 

                  if (@BrokerId != '' AND @BrokerId != 0)
					BEGIN
                     Set @SQL = @SQL + ' (Broker.CompanyEmail =''' + @BrokerId + ''' or Corres.CompanyEmail =''' + @BrokerId + ''' ) and '
					END

                if (@Channel =  'broker')
                 
                      Set @SQL = @SQL + ' and loanapp.ChannelType=''BROKER'''
                 
                if (@Channel =  'retail')
                 
                     Set @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL''' 
                 
                if (@Channel =  'correspond')
                 
                      Set @SQL = @SQL + 'and loanapp.ChannelType=''CORRESPOND'''  
                if (@RoleID =  1)
                 
                      Set @SQL = @SQL + ' and loanapp.ChannelType like ''%RETAIL%'''  
                if (@TeamOPuids != '')
                 BEGIN
                       Set @SQL = @SQL + '  and Originator_id in (''' +   @TeamOPuids +''') '
                 END
                else
                BEGIN
                    if (@RoleID != 0 AND @RoleID != 1)
                    BEGIN
                       if (len(@strID) = 0)
                        BEGIN
                            if (@RoleID =  2) 
                                   Set @SQL = @SQL + '  and (Originator_id in (''' + @strID + '''))'
                             
                        END
                        else
                        BEGIN
                            if (@RoleID =  2)
                            
                                  Set @SQL = @SQL + '   and (Originator_id in (''' + @strID + '''))' 
                        END
                    END
                END
                  Set @SQL = @SQL + '   and Year(AppStatus.StatusDateTime) <> ''1900'' and Year(AppStatus.StatusDateTime)='''+ @Year + ''' group by ' 

                if (@BrokerId != '' AND @BrokerId != 0)
                BEGIN
                       Set @SQL = @SQL + '  Broker.CompanyEmail ,Corres.CompanyEmail ,' 
                END
                  Set @SQL = @SQL + '   month(AppStatus.StatusDateTime),year(AppStatus.StatusDateTime),LoanData.FinancingType, TransTYpe ' 

  
 Print @SQL        
exec sp_executesql @SQL        
END 

GO

