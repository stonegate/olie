USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SelectNewsAndAnnouncementID_By_TPO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SelectNewsAndAnnouncementID_By_TPO]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_SelectNewsAndAnnouncementID_By_TPO]            
(            
 
@NewsID int          
)            
AS            
BEGIN
SELECT
	*
FROM tpo_tblNewsAndAnnouncement
WHERE ID = @NewsID          
END 


GO

