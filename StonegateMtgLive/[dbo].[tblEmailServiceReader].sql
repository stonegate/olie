USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblEmailServiceReader]') AND type in (N'U'))
DROP TABLE [dbo].[tblEmailServiceReader]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmailServiceReader](	  [Id] BIGINT NOT NULL IDENTITY(1,1)	, [ToName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FromAddress] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ToAddress] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [CcAddress] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [mailSubject] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [mailBody] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [MobileEmailBody] NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [mailAttachment] NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RcvdDate] DATETIME NULL DEFAULT(getdate())	, [SendBy] INT NULL	, [MobileEmailId] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [updStatus] INT NULL DEFAULT((0))	, CONSTRAINT [PK_tblDSEmailHistory] PRIMARY KEY ([Id] ASC))USE [HomeLendingExperts]
GO

