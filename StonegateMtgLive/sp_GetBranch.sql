USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetBranch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetBranch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================  

-- Author:  <Muruga Nagaraju>  

-- Create date: <30/01/2014>  

-- Description: <This wil give you all the "Branch" based on region>  

-- =============================================  

CREATE PROCEDURE [dbo].[sp_GetBranch] (@RegionID varchar(max) = NULL)

AS

BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

  SELECT

    BO.Id,

    BO.OfficeName,

    BO.OfficeValue,

    BO.BRANCH

  FROM tblBranchOffices BO

  INNER JOIN tblregion TR ON BO.region = TR.RegionValue

  WHERE (@RegionID IS NULL OR TR.RegionValue IN (SELECT * FROM dbo.SplitString(@RegionID,','))) AND (BO.BRANCH IS NOT NULL OR BO.BRANCH <> '')
  ORDER BY BO.BRANCH

END




GO

