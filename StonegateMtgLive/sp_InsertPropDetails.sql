USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertPropDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertPropDetails]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertPropDetails]
	-- Add the parameters for the stored procedure here
(
@RealtorID int,
@PropStatus varchar(50),
@ListingType varchar(50),
@Bedrooms int,
@baths decimal(18,1),
@amount decimal(18,0) ,
@street varchar(50),
@state varchar(50),
@city varchar(50),
@zipcode varchar(50),
@yearbuilt int, 
@PropDesc ntext,
@lat decimal(18,6) ,
@lng decimal(18,6) ,
@Area int,
@isactive bit,
@ImprovementFile varchar(50)
) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	insert into tblProperties(RealtorID,PropStatus,ListingType,Bedrooms,baths,amount,street,state,city,zipcode,yearbuilt, PropDesc,lat,lng,Area,isactive,ImprovementFile)

values(@RealtorID,@PropStatus,@ListingType,@Bedrooms,@baths,@amount,@street,@state,@city,@zipcode,@yearbuilt, @PropDesc,@lat,@lng,@Area,@isactive,@ImprovementFile)
END


GO

