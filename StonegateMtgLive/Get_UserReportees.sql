USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_UserReportees]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Get_UserReportees]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Get_UserReportees]
(
    @managerid varchar(MAX)
  --, @branchid varchar(MAX)
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	;WITH MyCTE
	AS ( 
			SELECT u1.userid,u1.ufirstname + SPACE(1) +  u1.ulastname  UserName , m1.managerid, m1.userid,1 [Level],r1.id [role],u1.isactive,u1.E3userid,u1.office [Branch]
			FROM tblusermanagers m1 
			INNER JOIN  tblusers u1 on u1.userid = m1.userid 
				and m1.managerid in (SELECT * FROM dbo.splitstring(@managerid,','))
				--and u1.office in (SELECT * FROM dbo.splitstring(@branchid,','))
			INNER JOIN tblroles r1 on u1.urole = r1.id 
			UNION ALL 

			SELECT u2.userid,u2.ufirstname + SPACE(1) + u2.ulastname  UserName , m2.managerid, m2.userid , MyCTE.[Level]+1 [Level],r2.id [role],u2.isactive,u2.E3userid,u2.office [Branch]
			FROM tblusermanagers m2 
			INNER JOIN  tblusers u2 on u2.userid = m2.userid
				--and u2.office in (SELECT * FROM dbo.splitstring(@branchid,','))
			INNER JOIN MyCTE on m2.managerid = MyCTE.userid
			INNER JOIN tblroles r2 on u2.urole = r2.id 
			WHERE  m2.managerid NOT IN (SELECT * FROM dbo.splitstring(@managerid,','))
		)SELECT * FROM MyCTE


		;WITH CTEBRANCH
		AS ( 
			SELECT u1.userid,u1.ufirstname + SPACE(1) +  u1.ulastname  UserName , m1.managerid, m1.userid,1 [Level],r1.id [role],u1.isactive,u1.E3userid,u1.office [Branch]
			FROM tblusermanagers m1 
			INNER JOIN  tblusers u1 on u1.userid = m1.userid 
				and m1.managerid in (SELECT * FROM dbo.splitstring(@managerid,','))
				--and u1.office in (SELECT * FROM dbo.splitstring(@branchid,','))
			INNER JOIN tblroles r1 on u1.urole = r1.id where u1.isactive=1
			UNION ALL 

			SELECT u2.userid,u2.ufirstname + SPACE(1) + u2.ulastname  UserName , m2.managerid, m2.userid , CTEBRANCH.[Level]+1 [Level],r2.id [role],u2.isactive,u2.E3userid,u2.office [Branch]
			FROM tblusermanagers m2 
			INNER JOIN  tblusers u2 on u2.userid = m2.userid
				--and u2.office in (SELECT * FROM dbo.splitstring(@branchid,','))
			INNER JOIN CTEBRANCH on m2.managerid = CTEBRANCH.userid
			INNER JOIN tblroles r2 on u2.urole = r2.id 
			WHERE  m2.managerid NOT IN (SELECT * FROM dbo.splitstring(@managerid,',')) and u2.isactive =1
			)SELECT * FROM CTEBRANCH
END




GO

