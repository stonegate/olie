USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Insert_Colts_Data]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Insert_Colts_Data]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Insert_Colts_Data] 
(
@stringmode varchar(50),
@id int,      
@name varchar(50),   
@lastname varchar(50),    
@streetaddr varchar(500),    
@city varchar(50),
@state varchar(50),   
@zipcode varchar(50),    
@phoneno varchar(50),      
@email varchar(50),     
@confirmemail varchar(50),      
@scoutprogram char(4),     
@colts char(4),      
@newspaper char(4),      
@friend char(4),      
@other char(4),        
@representative char(4)  
)
AS
BEGIN
	 INSERT INTO tblColts (name,lastname,streetaddr,city,state,zipcode,phoneno,email,confirmemail,scoutprogram,colts,newspaper,friend,other,representative)      
 VALUES      
  (@name,@lastname,@streetaddr,@city,@state,@zipcode,@phoneno,@email,@confirmemail,@scoutprogram,@colts,@newspaper,@friend,@other,@representative)  
END



GO

