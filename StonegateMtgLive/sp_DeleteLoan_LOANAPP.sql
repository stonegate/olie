USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteLoan_LOANAPP]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteLoan_LOANAPP]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  <Author,,Name>  
-- ALTER date: <ALTER Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_DeleteLoan_LOANAPP]  
 -- Add the parameters for the stored procedure here  
 @loanid varchar(20)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 DELETE FROM tblLoanApplication  WHERE LoanApplicationID=@loanid  
END  
  

 

 
GO

