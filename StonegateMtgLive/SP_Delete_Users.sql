USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Delete_Users]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Delete_Users]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_Delete_Users](     
@UserID int        
)              
AS              
BEGIN          
   Delete From tblusers Where userid =@UserID        
END

GO

