USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUserManagers]') AND type in (N'U'))
DROP TABLE [dbo].[tblUserManagers]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserManagers](	  [UserManagerId] INT NOT NULL IDENTITY(1,1)	, [UserId] INT NOT NULL	, [ManagerId] INT NOT NULL	, [CreatedBy] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [CreatedOn] DATETIME NOT NULL	, CONSTRAINT [PK_tblUserManagers] PRIMARY KEY ([UserManagerId] ASC))ALTER TABLE [dbo].[tblUserManagers] WITH CHECK ADD CONSTRAINT [FK_tblUserManagers_tblManagers] FOREIGN KEY([ManagerId]) REFERENCES [dbo].[tblUsers] ([userid])ALTER TABLE [dbo].[tblUserManagers] CHECK CONSTRAINT [FK_tblUserManagers_tblManagers]ALTER TABLE [dbo].[tblUserManagers] WITH CHECK ADD CONSTRAINT [FK_tblUserManagers_tblUsers] FOREIGN KEY([UserId]) REFERENCES [dbo].[tblUsers] ([userid])ALTER TABLE [dbo].[tblUserManagers] CHECK CONSTRAINT [FK_tblUserManagers_tblUsers]USE [HomeLendingExperts]
GO

