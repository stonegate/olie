USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AddOrderByField2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_AddOrderByField2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_AddOrderByField2]
(
@uroleid int
)
AS
BEGIN
	 select dbo.GetFormRole(FO.formid)as Roleid, formID,Description,FileName,dttime,isnews,CateName from tblforms FO left outer join tblCategories CAT on FO.Cateid=CAT.catid where formid in (select formid from tblformroles where uroleid = @uroleid) ORDER BY dttime DESC

END





GO

