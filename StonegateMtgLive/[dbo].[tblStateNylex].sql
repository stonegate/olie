USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblStateNylex]') AND type in (N'U'))
DROP TABLE [dbo].[tblStateNylex]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblStateNylex](	  [ID] INT NOT NULL IDENTITY(1,1)	, [StateID] INT NULL	, [Abbreviature] NVARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [StateName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [StateOrder] INT NULL)USE [HomeLendingExperts]
GO

