USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCareerPosting]') AND type in (N'U'))
DROP TABLE [dbo].[tblCareerPosting]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCareerPosting](	  [CareerId] INT NOT NULL IDENTITY(1,1)	, [PositionName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Description] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PublishedDate] DATETIME NULL	, [IsPublished] BIT NULL DEFAULT((0))	, [ModifiedDate] DATETIME NULL DEFAULT(getdate())	, CONSTRAINT [PK_tblCareerPosting] PRIMARY KEY ([CareerId] ASC))USE [HomeLendingExperts]
GO

