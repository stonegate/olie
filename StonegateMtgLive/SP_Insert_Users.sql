USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Insert_Users]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Insert_Users]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[SP_Insert_Users](     
@UserID int,       
@StringMode VARCHAR(50),        
@UFirstName VARCHAR(50),              
@ULastName  VARCHAR(50),                
@UEmailId  VARCHAR(200),                
@StateBelongTo nvarchar(100)=null,              
@urole int,         
@IsAdmin bit,           
@IsActive bit,                 
@ispublish bit,              
@uidprolender  VARCHAR(200)=null,                
@uparent int,              
@upassword  VARCHAR(200),         
@Mobile_Ph nvarchar(50),        
@LoginIdProlender  nvarchar(300),      
@LoanNum  nvarchar(50),       
@CarrierId int,      
@MAID int,      
@Shorturl varchar(50)=null,        
@Lead360 varchar(4000),      
@MI varchar(10),      
@TermsRealtor bit,      
@RealtorID int,      
@RealtorGroupID int,      
@E3Userid varchar(50)=null,        
@Office  VARCHAR(200)=null,          
@BranchOffice VARCHAR(200)=null        
)              
AS              
BEGIN          
 IF(@StringMode ='insert')     
  BEGIN        
  INSERT INTO tblusers(ufirstname,ulastname,uemailid,StateBelongTo,urole,isadmin,isactive,ispublish,uidprolender,uparent,upassword,mobile_ph,loginidprolender,loannum,carrierid,MAID,shorturl,lead360,MI, TermsRealtor,RealtorID,RealtorGroupID,E3Userid,  
  office,BranchOffice)      
  VALUES (@UFirstName,@ULastName,@UEmailId,@StateBelongTo,@urole,@isadmin,@isactive,@ispublish,@uidprolender,@uparent,      
  @upassword,@mobile_ph,@loginidprolender,@loannum,@carrierid,@MAID,@shorturl,@lead360,      
  @MI,@TermsRealtor,@RealtorID,@RealtorGroupID,@E3Userid,@Office,@BranchOffice)      
  END    
 ELSE IF(@StringMode ='update')     
  BEGIN     
  update tblusers    
  set ufirstname=@UFirstName,ulastname=@ULastName,uemailid=@UEmailId,StateBelongTo=@StateBelongTo,    
  urole=@urole,isadmin=@IsAdmin,isactive=@isactive,ispublish=ispublish,    
  uidprolender=@uidprolender,uparent=@uparent,upassword=@upassword,mobile_ph=@mobile_ph,    
  loginidprolender=@loginidprolender,loannum=@loannum,carrierid=@carrierid,MAID=@MAID,    
  shorturl=@shorturl,lead360=@lead360,MI=@MI, TermsRealtor=@TermsRealtor,RealtorID=@RealtorID,    
  RealtorGroupID=@RealtorGroupID,E3Userid=@E3Userid,office=@office,    
  BranchOffice =@BranchOffice    
  where userid=@UserID    
  END    
else if(@stringmode = 'delete')   
  Delete From tblusers Where userid =@UserID         
END          
      

GO

