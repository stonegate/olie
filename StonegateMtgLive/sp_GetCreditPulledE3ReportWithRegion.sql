USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCreditPulledE3ReportWithRegion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCreditPulledE3ReportWithRegion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[sp_GetCreditPulledE3ReportWithRegion]
(                
@strID varchar(max),                 
@Status varchar(max),             
@oPuid varchar(max),
@oYear varchar(max),
@oFromDate VARCHAR(20),
@oToDate VARCHAR(20)
)                
AS                
BEGIN                
                
Declare @SQL nvarchar(max)                
Set @SQL =''                
Set @SQL ='select ''E3'' as DB, loanapp.ChannelType as BusType,loanapp.ID as record_id,CONVERT(DECIMAL(10,3),(loanData.NoteRate * 100)) as int_rate,LoanNumber as loan_no,borr.firstname as BorrowerFirstName ,borr.lastname as BorrowerLastName,loanapp.Originatorname,isnull(isnull(convert(decimal(10,2), loanData.LTV * 100),0),0)LTV,LoanApp.DecisionStatus,LoanApp.CurrentStatus as LoanStatus,convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,LoanData.ShortProgramName as LoanProgram,
           case when loanapp.LoanPurpose1003 is null then '''' when loanapp.LoanPurpose1003 = ''PU'' then ''Purchase'' 
           when loanapp.LoanPurpose1003 = ''RE'' then ''Refinance'' end as Purpose,
           CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed,case loanData.UseCashInOut when ''0'' then ''No'' when ''1'' then ''Yes'' end as CashOut,isnull(loanData.AdjustedNoteAmt,0) as loan_amt,convert(varchar(35),EstCloseDate,101) AS ScheduleDate,
           CurrPipeStatusDate,UnderwriterName as UnderWriter,CloserName as Closer,CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as  BrokerName,
            Broker.Office as ''Branch Office''
			from mwlLoanApp  loanapp  
			Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
            Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id 
            left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
            Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BRANCH'' and Broker.objownerName=''BranchInstitution''
            left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' and LockRecord.Status<>''CANCELED''   
            Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' 
            where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''
            and loanapp.ChannelType like ''%RETAIL%''
            
            and CurrentStatus  IN (select * from dbo.SplitString('''+ @Status +''','','')) ' 
            
            if (len(@oPuid) = 0)      
            Begin
               Set @SQL = @SQL + '   and (Originator_id in (select * from dbo.SplitString('''+  @oPuid  +''','',''))  
									      or loanapp.id in (select * from dbo.SplitString('''+  @strID  +''','',''))
									      or Broker.office in  (select * from dbo.SplitString('''+  @oPuid  +''','','')))'      
			End
			else 
			begin
			   Set @SQL = @SQL + '   and (Originator_id in (select * from dbo.SplitString('''+  @oPuid  +''','',''))  
									      or loanapp.id in (select * from dbo.SplitString('''+  @strID  +''','',''))
									      or Broker.office in  (select * from dbo.SplitString('''+  @oPuid  +''','','')))'      	
			End
			
			if(@oYear != '')
				Set @SQL=@SQL+ ' and YEAR(CurrPipeStatusDate) = ''' + @oYear +''''
				
			if(@oFromDate != '' and @oToDate !='')
				Set @SQL=@SQL+ ' and CurrPipeStatusDate between '''+@oFromDate+'''  and '''+Convert(varchar(20),DATEADD (dd , 1 , @oToDate),101)+''''
			else if(@oFromDate != '')
				Set @SQL=@SQL+ ' and CurrPipeStatusDate > '''+@oFromDate+''''	
			else if(@oToDate != '')
				Set @SQL=@SQL+ ' and CurrPipeStatusDate < '''+@oToDate+''''    
					
Print @SQL      
exec sp_executesql @SQL                
END
/*===========================Daily Activity ============================= */

GO

