USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Delete_Form]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Delete_Form]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel
-- Create Date : 10/04/2012  
-- Description: Procedure for  Delete Form
-- =============================================  
   
CREATE PROC [dbo].[SP_Delete_Form](       
@FormID int          
)                
AS                
BEGIN            
   Delete from tblformRoles where formid =@FormID   
 Delete from tblForms where FormID =@FormID  
END

GO

