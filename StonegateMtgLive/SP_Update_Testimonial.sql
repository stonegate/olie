USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Update_Testimonial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Update_Testimonial]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Created By: Chandresh Patel  
-- Create Date : 10/04/2012    
-- Description: Procedure for Update SP_Update_Testimonial   
-- =============================================       
CREATE PROC [dbo].[SP_Update_Testimonial](      
@UName varchar(50),     
@Testimonial char(1),    
@SubmittedDate datetime,    
@TestimonialID int,    
@UpdatedDt datetime,    
@City varchar(50),      
@State varchar(50) )                  
AS                  
BEGIN             
	UPDATE tblTestimonials SET UName=@UName,
	Testimonial=@Testimonial,
	SubmittedDate=@SubmittedDate,
	UpdatedDt=GETDATE(), 
	City=@City,
	State=@State
	where TestimonialID=@TestimonialID
       
END 

GO

