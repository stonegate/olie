USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertAddDocs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertAddDocs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertAddDocs]
(
@loan_no VARCHAR(MAX),
@cond_text VARCHAR(MAX),
@FileName VARCHAR(MAX),
@dttime VARCHAR(MAX),
@condrecid VARCHAR(MAX),
@ProlendImageDesc VARCHAR(MAX),
@Userid VARCHAR(MAX)
)
AS
BEGIN

insert into tblAdditionalUWDoc(loan_no,cond_text,FileName,dttime,condrecid,ProlendImageDesc,Userid) Values
(@loan_no,@cond_text,@FileName,@dttime,@condrecid,@ProlendImageDesc,@Userid)
 
END


GO

