USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CancelLoan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CancelLoan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_CancelLoan]
(
@Loan_no VARCHAR(MAX)
)
AS
BEGIN
update loandat set CANCELED_DATE=getDate(), lt_loan_stats='LOAN CANCELED' where Loan_no=@Loan_no


END

GO

