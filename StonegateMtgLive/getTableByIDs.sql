USE [HomeLendingExperts]
								GO
								SET ANSI_NULLS ON
								GO
								SET QUOTED_IDENTIFIER ON
								GO
/*
	Initial form of splittingout csv's . . .may still be used in places
*/

Create function [dbo].[getTableByIDs] (@ids varchar(MAX) )
returns @tempTable table 
(
	val int 
)
as 
 begin

declare @count int
declare @temp varchar(100)
			-- get the list of editors
	SET @Count = CHARINDEX(',', @ids)-- get the position of the first comma (will be found after the first category)

	-- this loop continues until all but one users are in the temporary table
	WHILE @Count > 0
	BEGIN
		SET @Temp = SUBSTRING(@ids, 1, (@Count - 1)) 	-- get the next user id
		INSERT INTO @TempTable VALUES(CAST(@Temp AS int))		-- insert the contract id into a temporary table (each contract id must be unique)
		SET @ids = SUBSTRING(@ids, (@Count + 1), (LEN(@ids) - @Count))	-- remove the contract id from the temporary string
		SET @Count = CHARINDEX(',', @ids)				-- get the position of the first comma (will be found after the first category)
	END

	-- insert the last userid into the temprary table
	if @ids <> ''
	INSERT INTO @TempTable VALUES(CAST(@ids AS int))

	return
 end





GO

