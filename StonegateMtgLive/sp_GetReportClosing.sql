USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetReportClosing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetReportClosing]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE DBO.sp_GetReportClosing         
(            
@RoleID int,           
@strID varchar(2000),             
@CurrentStatus varchar(3000),   
@oPuid varchar(100)     
)            
AS            
BEGIN            
            
Declare @SQL nvarchar(max)            
Set @SQL =''            
Set @SQL =' select ''E3'' as DB,loanapp.ChannelType,loanapp.OriginatorName,loanapp.DecisionStatus, loanapp.ID as record_id,LoanNumber as loan_no,  
   borr.lastname as BorrowerLastName,borr.firstname as BorrowerFirstName,loanapp.DecisionStatus,loanapp.CurrentStatus as LoanStatus,  
   DateAppSigned as SubmittedDate,LoanProgramName as LoanProgram,  
   case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,  
   borr.CompositeCreditScore as CreditScoreUsed,loanData.AdjustedNoteAmt as LoanAmount,  
   convert(varchar(35),EstCloseDate,101) AS ScheduleDate,LockStatus,LockExpirationDate,UnderwriterName as UnderWriter,  
   CloserName as Closer,Institute1.Company as BrokerName, Institute.CustomDataOne as TitleCompany,  
   AppStatus.StatusDateTime as DocsSentDate,FundedStatus.StatusDateTime as FundedDate,'''' as LU_FINAL_HUD_STATUS   
   from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id   
   Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id   
   left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id     
   Left join dbo.mwlInstitution as Institute on Institute.ObjOwner_id = loanapp.id and Institute.InstitutionType = ''Branch'' and   
   Institute.objownerName=''BranchInstitution''   
   Left join dbo.mwlInstitution as Institute1 on Institute1.ObjOwner_id = loanapp.id and Institute1.InstitutionType = ''Title''   
   and Institute1.objownerName=''Contacts''   
   Left join mwlappstatus as AppStatus on AppStatus.LoanApp_id=loanapp.id  and AppStatus.StatusDesc like ''%Docs Out%''    
   Left join mwlappstatus as FundedStatus on FundedStatus.LoanApp_id=loanapp.id  and FundedStatus.StatusDesc like ''%Funded''   
   where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and   
   CurrentStatus  IN (select * from dbo.SplitString('''+ @CurrentStatus +''','',''))  and loanapp.ChannelType like ''%RETAIL%'''    
               
                if (@RoleID != 0 and @RoleID != 1)  
                begin  
                    if (len(@strID) = 0)  
                    begin  
                        
                        if (@RoleID = 2)  
                        begin  
                            Set @SQL = @SQL + ' and (Originator_id in (''' + @strID +'''))'  
                        end  
                        else if (@RoleID = 3)  
                        begin  
                              
                            Set @SQL = @SQL + ' and (loanapp.ID in (''' + @strID +''') or Originator_id in (''' + @oPuid + '''))'  
                        end  
                        else if (@RoleID = 10)  
                        begin  
  
  
                            Set @SQL = @SQL + ' and (Originator_id in (''' + @strID +''') or loanapp.id in (''' + @strID +'''))'  
                        end  
                          
                        else  
                        begin  
                            Set @SQL = @SQL + ' and (Originator_id in (''' + @strID +'''))'  
  
  
                        end  
                    end  
                    else  
                    begin  
                          
                        if (@RoleID = 2 )  
                        begin  
                            Set @SQL = @SQL + ' and (Originator_id in (''' + @strID +'''))'  
                        end  
                        
                        else if (@RoleID = 3)  
                        begin  
                            Set @SQL = @SQL + ' and (loanapp.ID in (''' + @strID +''') or Originator_id in (''' + @oPuid + '''))'  
                        end  
                        else if (@RoleID = 10)  
          begin  
  
  
                            Set @SQL = @SQL + ' and (Originator_id in (''' + @strID +''') or loanapp.id in (''' + @strID +'''))'  
                        end  
                          
                        else  
                        begin  
                            Set @SQL = @SQL + ' and (Originator_id in (''' + @strID +'''))'  
  
                        end  
                    end  
                end  
                      
                       
                  
                    
Print @SQL  
exec sp_executesql @SQL            
END   
  
GO

