USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_select_tblGetUserloginID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_select_tblGetUserloginID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE PROCEDURE [dbo].[usp_select_tblGetUserloginID]
(
@strLoginID varchar(Max)
)
as
begin

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select * from tblusers where userloginid =@strLoginID
end


GO

