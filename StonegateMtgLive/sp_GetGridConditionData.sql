USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetGridConditionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetGridConditionData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetGridConditionData]
(
@loan_no VARCHAR(MAX)
)
AS
BEGIN
SELECT 'P' as DB,descriptn, id_value ,case id_value when '004' then '1'  when '001' then '2' when '003' then '3'  when '002' then '4' END AS 'Value'  FROM lookups WHERE (lookup_id = 'aau')
order by Value asc;
SELECT 'P' as DB,'PC' as DueBy,cond_no , a.record_id,a.parent_id,a.cond_text,
CASE WHEN (a.COND_ADDED_DATE IS NOT NULL AND (a.COND_RECVD_DATE IS NULL AND a.COND_CLEARED_DATE IS NULL)) THEN 'Open'
WHEN ((a.COND_ADDED_DATE IS NOT NULL AND a.COND_RECVD_DATE IS NOT NULL ) AND a.COND_CLEARED_DATE IS NULL) THEN 'Received'
WHEN (a.COND_CLEARED_DATE IS NOT NULL) THEN 'Cleared' 
ELSE 'OPEN' END AS STATUS,lu_cond_type as id_value,a.record_id,a.cond_cleared_date,b.descriptn,a.cond_recvd_date FROM dbo.condits a,lookups b
where (b.id_value = a.lu_cond_grp_assignd and b.lookup_id = 'aaw' )
and a.parent_id IN ( SELECT record_id FROM dbo.loandat WHERE loan_no=@loan_no)  
ORDER BY id_value,COND_NO ASC

END

GO

