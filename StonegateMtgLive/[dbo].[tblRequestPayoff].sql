USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRequestPayoff]') AND type in (N'U'))
DROP TABLE [dbo].[tblRequestPayoff]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRequestPayoff](	  [Requestid] INT NOT NULL IDENTITY(1,1)	, [Loannumber] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RequestDate] DATETIME NULL	, [EmailID] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [NeedtoKnow] NTEXT(8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO

