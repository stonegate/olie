USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetFormRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetFormRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetFormRoles]
(
    @formID int
)
AS
BEGIN
	 Select uroleid from tblformRoles where formID = @formID

END


GO

