USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_tblusers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_tblusers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gourav
-- Create date: 10/03/2012
-- Description:	Procedure for clsUser.cs bll  
-- =============================================
CREATE PROCEDURE [dbo].[sp_Update_tblusers]
(
@userloginid varchar(50),
@newPass varchar(Max),
@userid varchar(Max)
)
as
begin
update tblusers set userloginid = @userloginid, upassword = @newPass, PasswordExpDate = GETDATE() where userid = @userid
end



GO

