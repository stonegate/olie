USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetFaqView]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetFaqView]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create  Procedure [dbo].[sp_GetFaqView]
@ModuleName varchar(100)
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	select  Row_Number() over(order by Id) SlNo,Id,Question,Answer,ModuleName,Published,DisplayOrder 
	from dbo.tblfaqs where ModuleName=@ModuleName and Published=1 order by Id asc
END


GO

