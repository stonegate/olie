USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetMortgageAdvisorReports]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetMortgageAdvisorReports]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Stonegate    
-- Create date: 31st July 2012    
-- Description: This will get user list to bind in the grid    
-- =============================================    
CREATE PROCEDURE [dbo].[sp_GetMortgageAdvisorReports]    
(    
@sortField varchar(500),    
@sortOrder varchar(100),    
@whereClause varchar(1000),    
@PageNo int,     
@PageSize int,
@CurrentStatus varchar(500),
@RoleID varchar(20),
@OriginatorID varchar(2000),
@oPuid varchar(2000)    
)     
AS    
BEGIN    
IF @sortField  = ''                                
  set @sortField = 'LoanApp.LoanNumber'    
    
SET @sortField = @sortField + ' ' + @sortOrder                                                
Declare @sql nvarchar(max)
set @sql = ''                                
     set @sql = 'Select count(*) from ( '    
     set @sql =  @sql + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,
 ''E3'' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue as ProcessorName,
loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,
CloserName as Closer,borr.lastname as BorrowerLastName,borr.firstname as BorrowerFirstName,
'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate, approvalStat.StatusDateTime as DateApproved,
case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe, 
case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,
loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,
CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,
CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate ,  
CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER'' and a.objOwner_ID IN 
	( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0  
		WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'')  
		and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'') and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  
		WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE (SELECT count(a.ID) as TotalCleared  FROM dbo.mwlCondition a 
		where (DueBy=''Prior to Closing'' and Category !=''LENDER'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'') 
			and a.objOwner_ID IN  (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions 
FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  
	WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER  from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
	Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id  left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id 
	Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
	left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED''  
	Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc=''01 - Registered'' 
	Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved'' 
	Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName=''mwlLoanData'' and fieldname=''refipurpose'' 
	Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' 
	left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID and CustFieldDef_id in(select Id from mwsCustFieldDef where CustomFieldName=''Retail Processor'') 
	where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and  loanapp.ChannelType like ''%RETAIL%'' 
and CurrentStatus IN ('''+@CurrentStatus+''' ) '
-- Temp commented to get test data
--If @RoleID <> '0' Or @RoleID <> '1'
--Begin
--	If @RoleID = '2'
--	Begin
--		set @sql =  @sql + ' and (Originator_id in ('''+@OriginatorID+'''))'
--	End
--	If @RoleID = '3'
--	Begin
--		set @sql =  @sql + ' and (loanapp.ID in ('''+@OriginatorID+''') or Originator_id in ('''+@oPuid+'''))'
--	End
--	If @RoleID = '10' Or @RoleID = '7'
--	Begin
--		set @sql =  @sql + ' and (Originator_id in ('''+@OriginatorID+''') or loanapp.id in ('''+@OriginatorID+'''))'
--	End
--	If @RoleID <> '2' Or @RoleID <> '3' Or @RoleID <> '10' Or @RoleID <> '7'  
--	Begin
--		set @sql =  @sql + ' and (Originator_id in ('''+@OriginatorID+''')) '
--		IF @oPuid <> ''
--		Begin
--			set @sql =  @sql + ' and (Originator_id in ('''+@oPuid+'''))'
--		End
--	End
--End
-- End temp comment 
    
IF @whereClause <> ''                                         
    BEGIN                                     
    set @sql = @sql  + ' AND ' + @whereClause                               
    END                        
    
set @sql = @sql + ' group by loanapp.casenum,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue,
loanapp.currentStatus,loanapp.DecisionStatus,loanapp.ID,LoanNumber,
CloserName,borr.lastname,borr.firstname,loanapp.CurrentStatus,appStat.StatusDateTime,approvalStat.StatusDateTime,
TransType,LoanData.FinancingType,loanData.AdjustedNoteAmt,LockRecord.Status, 
LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName,
Broker.Company, Corres.Company, Lookups.DisplayString,
borr.CompositeCreditScore, EstCloseDate,LoanApp.LoanNumber'                                 
 set @sql = @sql + ' ) as t2 '     
    
print @sql    
exec sp_executesql @sql      
    
    
set @sql = ''                                
     set @sql = 'Select * from ( '     
set @sql =  @sql + 'SELECT ROW_NUMBER() OVER (ORDER BY ' +@sortField + ' )AS Row,
''E3'' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue as ProcessorName,
loanapp.currentStatus as LoanStatus,loanapp.DecisionStatus,loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,
CloserName as Closer,borr.lastname as BorrowerLastName,borr.firstname as BorrowerFirstName,
'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate, approvalStat.StatusDateTime as DateApproved,
case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe, 
case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as LoanProgram,
loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus, LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,
CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,
CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate ,  
CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER'' and a.objOwner_ID IN 
	( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0  
		WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'')  
		and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'') and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  
		WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   ELSE (SELECT count(a.ID) as TotalCleared  FROM dbo.mwlCondition a 
		where (DueBy=''Prior to Closing'' and Category !=''LENDER'') and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'') 
			and a.objOwner_ID IN  (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 / (SELECT count(a.ID) as TotalConditions 
FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'') and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  
	WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER  from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
	Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id  left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id 
	Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker''
	left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED''  
	Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc=''01 - Registered'' 
	Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved'' 
	Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName=''mwlLoanData'' and fieldname=''refipurpose'' 
	Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent'' 
	left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID and CustFieldDef_id in(select Id from mwsCustFieldDef where CustomFieldName=''Retail Processor'') 
	where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and  loanapp.ChannelType like ''%RETAIL%'' and  
CurrentStatus  IN ('''+@CurrentStatus+''' ) '    
-- Temp commented to get test data    
--If @RoleID <> '0' Or @RoleID <> '1'
--Begin
--	If @RoleID = '2'
--	Begin
--		set @sql =  @sql + ' and (Originator_id in ('''+@OriginatorID+'''))'
--	End
--	If @RoleID = '3'
--	Begin
--		set @sql =  @sql + ' and (loanapp.ID in ('''+@OriginatorID+''') or Originator_id in ('''+@oPuid+'''))'
--	End
--	If @RoleID = '10' Or @RoleID = '7'
--	Begin
--		set @sql =  @sql + ' and (Originator_id in ('''+@OriginatorID+''') or loanapp.id in ('''+@OriginatorID+'''))'
--	End
--	If @RoleID <> '2' Or @RoleID <> '3' Or @RoleID <> '10' Or @RoleID <> '7'  
--	Begin
--		set @sql =  @sql + ' and (Originator_id in ('''+@OriginatorID+''')) '
--		IF @oPuid <> ''
--		Begin
--			set @sql =  @sql + ' and (Originator_id in ('''+@oPuid+'''))'
--		End
--	End
--End  
-- End test comment    
IF @whereClause <> ''                                         
    BEGIN                                     
    set @sql = @sql  + ' AND ' + @whereClause                               
    END                        
    
set @sql = @sql + ' group by loanapp.casenum,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue,
loanapp.currentStatus,loanapp.DecisionStatus,loanapp.ID,LoanNumber,
CloserName,borr.lastname,borr.firstname,loanapp.CurrentStatus,appStat.StatusDateTime,approvalStat.StatusDateTime,
TransType,LoanData.FinancingType,loanData.AdjustedNoteAmt,LockRecord.Status, 
LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName,
Broker.Company, Corres.Company, Lookups.DisplayString,
borr.CompositeCreditScore, EstCloseDate,LoanApp.LoanNumber'                                 
 set @sql = @sql + ' ) as t2 '    
    
set @sql = @sql + ' Where Row between (' + CAST( @PageNo AS NVARCHAR)  + ' - 1) * ' + Cast(@PageSize as nvarchar) + ' + 1 and ' + cast(@PageNo as nvarchar) + ' * ' + cast(@PageSize as nvarchar)    
    
print @sql                              
exec sp_executesql @sql    
    
END 


GO

