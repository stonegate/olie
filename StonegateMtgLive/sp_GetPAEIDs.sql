USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetPAEIDs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetPAEIDs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetPAEIDs]
(
@lt_usr_lorep VARCHAR(MAX)
)
AS
BEGIN
select isnull(record_id,0) as record_id from brokers where lt_usr_lorep in(@lt_usr_lorep) 

END

GO

