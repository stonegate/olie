USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Insert_RegisterNewCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Insert_RegisterNewCustomer]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Insert_RegisterNewCustomer]
(                 
                  
@UFirstName VARCHAR(50),                          
@ULastName  VARCHAR(50),                            
@UEmailId  VARCHAR(200) = null,             
@siteurl varchar(50),                           
@StateBelongTo nvarchar(100)=null,                          
@urole int,                     
@IsAdmin bit,                       
@IsActive bit,                        
@uidprolender  VARCHAR(200)=null,                            
@uparent int,              
@Mobile_Ph nvarchar(50),              
@LoginIdProlender  nvarchar(300),                  
@LoanNum  nvarchar(50),                   
@address nvarchar(300),            
@address2  varchar(100),            
@city  nvarchar(100),            
@state  nvarchar(100),            
@zip  nvarchar(100),    
@upassword varchar(150),                
@propertyaddr nvarchar(300),            
@MAID int,            
@MI varchar(10),            
@carrierid int,             
@TermsRealtor bit,            
@lat decimal(18,6),                
@lng decimal(18,6),                
@zlat decimal(18,6),                
@zlng decimal(18,6),                
@compName varchar(100)=null,                 
@E3Userid varchar(50)=null,   
@Userloginid varchar(50),
@PasswordExpDate nvarchar(100)                     
)                          
AS                          
BEGIN                      
                
   Insert into tblusers(ufirstname,siteurl,ulastname,uemailid,StateBelongTo,urole,isadmin,isactive,            
uidprolender,uparent,mobile_ph,loginidprolender,loannum,address,Address2,city,state,zip,upassword,            
propertyaddr,MAID,MI,carrierid, TermsRealtor,[lat],[lng],[zlat],[zlng],compName,E3Userid,Userloginid,PasswordExpDate)                 
    VALUES (@UFirstName,@siteurl,@ULastName,@UEmailId,@StateBelongTo,@urole,@isadmin,@isactive,            
 @uidprolender,@uparent,@mobile_ph,@loginidprolender,@loannum,@address,@address2,@city,@state,@zip           
,@upassword,@propertyaddr,@MAID,@MI,@carrierid,@TermsRealtor,@lat,@lng,@zlat,@zlng,@compName,@E3Userid,@Userloginid,@PasswordExpDate)                  
              
END 


GO

