USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_FetchImages]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_FetchImages]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_FetchImages]
	-- Add the parameters for the stored procedure here
@PropId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

select * from dbo.tblPropertyImage where isPropImageDel = 1 and PropID = @PropId
select count('A') from dbo.tblPropertyImage where isPropImageDel = 1 and PropID = @PropId
END


GO

