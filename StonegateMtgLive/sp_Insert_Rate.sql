USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_Rate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_Rate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[sp_Insert_Rate] 
(
@stringmode varchar(50),
@RateID int,      
@strLoanType varchar(50),   
@IPoints int,    
@DecRate decimal(18,3),    
@DecAPR decimal(18,3),
@DecClosingCost decimal(18,2),
@CreatedDate datetime,   
@ModifiedDate datetime   

)
AS
BEGIN
if(@stringmode = 'insert')
 insert into tblrates(strLoanType,IPoints,DecRate,DecAPR,DecClosingCost,CreatedDate,ModifiedDate)
 VALUES      
 (@strLoanType,@IPoints,@DecRate,@DecAPR,@DecClosingCost,@CreatedDate,@ModifiedDate)  
else
update tblrates set strloantype = @StrLoanType,IPoints = @IPoints,DecRate =  @DecRate,DecAPR = @DecAPR,
               DecClosingCost = @DecClosingCost,ModifiedDate = getdate()
                where rateid = @RateID


END







GO

