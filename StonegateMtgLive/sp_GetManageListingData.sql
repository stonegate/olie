USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetManageListingData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetManageListingData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Harsh Pandit
-- Create date: 19 April 2010
-- Description:	to get the data for manage listing
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetManageListingData]
	-- Add the parameters for the stored procedure here
	@RealtorID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @PropID VARCHAR(50) 

	CREATE TABLE #Temp(
	[ID] [int] IDENTITY(1,1) NOT NULL,  
 [PropID] [int] NULL,  
 [RealtorID] [int] NULL,  
 [PropStatus] [varchar](50) NULL,  
 [ListingTYpe] [varchar](50) NULL,  
 [BedRooms] [int] NULL,  
 [Baths] [decimal](18, 1) NULL,  
 [Price] [decimal](18, 2) NULL,  
 [DaysListed] [int] NULL,  
 [Street] [varchar](50) NULL,  
 [State] [varchar](50) NULL,  
 [City] [varchar](50) NULL,  
 [Zipcode] [varchar](50) NULL,  
 [YearBuilt] [int] NULL,  
 [IsActive] [bit] NULL ,  
 [PropDesc] [ntext] NULL,  
 [lat] [decimal](18, 6) NULL,  
 [lng] [decimal](18, 6) NULL ,  
 [Area] [int] NULL,  
 [logoname] [varchar](50) NULL,  
 [uemailid] [varchar](50) NULL,  
 [PropImageName] [varchar](50) NULL,  
 [FullAddress] [varchar] (250),  
 [Amount] [decimal] (18,0)  
	)
	DECLARE db_cursor CURSOR FOR  
	SELECT PropID from tblProperties 

	OPEN db_cursor   
	FETCH NEXT FROM db_cursor INTO @PropID

	WHILE @@FETCH_STATUS = 0   
	BEGIN   
INSERT INTO #Temp (PropID,RealtorID,PropStatus,ListingType,Bedrooms,baths,price,dayslisted,street,[state],city,zipcode,yearbuilt,lat,lng,Area,logoname,uemailid,PropImageName,FullAddress,Amount,isactive)  
   SELECT  TOP(1) prop.PropID, prop.RealtorID,PropStatus,ListingType,Bedrooms,baths,price,datediff(day,CreatedDate,getdate()) as dayslistedcalc,street,prop.[state],prop.city,zipcode,yearbuilt,prop.lat,prop.lng,prop.Area,
			CASE WHEN users.logoname='' THEN 'nologo.gif' ELSE ISNULL(users.logoname,'nologo.gif')END logoname,users.uemailid,  			
			ISNULL(PropImageName,'noHome.png')PropImageName,street + '+' + prop.[state] + '+' + prop.city + '+' + zipcode as FullAddress, Amount,prop.isactive  
			FROM 	tblProperties prop
			LEFT OUTER JOIN tblPropertyImage propImages ON prop.propid = propImages.propid
			JOIN tblusers users ON users.userid = prop.realtorID
			WHERE prop.PropID=@PropID and prop.realtorid = @RealtorID
			
		   FETCH NEXT FROM db_cursor INTO @PropID   
	END   

	SELECT * FROM #Temp
	DROP TABLE #Temp
	CLOSE db_cursor   
	DEALLOCATE db_cursor  
END

GO

