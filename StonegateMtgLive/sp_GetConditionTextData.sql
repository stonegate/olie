USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetConditionTextData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetConditionTextData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetConditionTextData]
(
@loan_no VARCHAR(MAX),
@condrecid VARCHAR(MAX)
)
AS
BEGIN
select loan_no,cond_text,filename,dttime,ctID,condrecid from tblCondText_History
where rtrim(loan_no)=@loan_no and condrecid = @condrecid
END




GO

