USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckDuplicatePackageId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckDuplicatePackageId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_CheckDuplicatePackageId]
(
@PackageId varchar(max)
)
AS
BEGIN

	SET NOCOUNT ON;

select PackageId from tblPackageTypeId where PackageId=@PackageId

END
GO

