USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateRate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_UpdateRate]
(
@strLoanType varchar(Max),
@IPoints varchar(Max),
@DecRate varchar(Max),
@DecAPR varchar(Max),
@DecClosingCost varchar(Max),
@ModifiedDate varchar(Max),
@rateid varchar(Max)
)
AS
BEGIN

update tblrates set strLoanType=@strLoanType,IPoints=@IPoints,DecRate=@DecRate,DecAPR=@DecAPR,
DecClosingCost=@DecClosingCost,ModifiedDate=@ModifiedDate where rateid=@rateid
 
END

GO

