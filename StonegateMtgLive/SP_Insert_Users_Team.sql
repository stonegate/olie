USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Insert_Users_Team]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Insert_Users_Team]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel
-- Create Date : 10/04/2012  
-- Description: Procedure for Insert User For Team  
-- =============================================  
     
CREATE PROC [dbo].[SP_Insert_Users_Team](          
@UFirstName VARCHAR(50),                  
@ULastName  VARCHAR(50),                    
@UEmailId  VARCHAR(200),                              
@urole bit,             
@IsAdmin bit,               
@IsActive bit,                                   
@uidprolender  VARCHAR(200)=null,                    
@uparent int,                  
@upassword  VARCHAR(200),             
@Mobile_Ph nvarchar(50),            
@LoginIdProlender  nvarchar(300),          
@LoanNum  nvarchar(50),           
@CarrierId int,        
@experience  VARCHAR(100),    
  @phonenum  nVARCHAR(20),    
@address nvarchar(300),      
@address2 varchar(100),    
@state nvarchar(10),    
@zip nvarchar(50),    
@fax nvarchar(50),    
@photoname nvarchar(50),    
@MAID int,        
@bio ntext,    
@areaofexpertise ntext,    
@ClientTestimonial ntext,    
@RealtorTestimonial ntext,    
@city nvarchar(100),     
@MI varchar(10)  
--, @userid INT OUTPUT     
)                  
AS                  
BEGIN              
            
  Insert into tblusers(ufirstname,ulastname,uemailid,urole,isadmin,isactive,uidprolender,uparent,upassword,    
mobile_ph,loginidprolender,loannum,carrierid,experience,phonenum,address,address2,state,zip,fax,photoname,    
MAID,bio,areaofexpertise,ClientTestimonial,RealtorTestimonial,city,MI)          
    VALUES (@UFirstName,@ULastName,@UEmailId,@urole,@isadmin,@isactive,@uidprolender,@uparent,          
    @upassword,@mobile_ph,@loginidprolender,@loannum,@carrierid,@experience,@phonenum,@address,     
@address2,@state,@zip,@fax,@photoname,@MAID,@bio,@areaofexpertise,@ClientTestimonial,    
@RealtorTestimonial,@city,@MI)          
         
--set @userid= @@identity    
     
           
END 

GO

