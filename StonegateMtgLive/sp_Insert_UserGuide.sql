USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_UserGuide]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_UserGuide]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_Insert_UserGuide] 
(
@stringmode varchar(50),
@ID int,      
@Filename varchar(100),   
@IsActive bit,  
@CreatedBy int
)
AS
BEGIN
 begin transaction
 if(@stringmode = 'insert')
Begin
    INSERT INTO tblUserGuide (Filename,IsActive,CreatedBy,CreatedDate)      
    VALUES (@Filename,@IsActive,@CreatedBy,getdate()) 
End
else if(@stringmode = 'show')
Begin
update tblUserGuide SET IsActive=@IsActive WHERE ID=@ID
update tblUserGuide set IsActive=0 WHERE ID <> @ID
End
else if(@stringmode = 'hide')
Begin
UPDATE tblUserGuide SET IsActive=@IsActive WHERE ID=@ID
End
  commit transaction
END





GO

