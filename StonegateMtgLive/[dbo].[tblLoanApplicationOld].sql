USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanApplicationOld]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanApplicationOld]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanApplicationOld](	  [ID] INT NOT NULL IDENTITY(1,1)	, [BrokarID] INT NULL	, [Email] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Description] NVARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Comments] NVARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [FileName] NVARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [DateEntered] DATETIME NULL)USE [HomeLendingExperts]
GO

