USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHUDApprovedDocImage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHUDApprovedDocImage]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetHUDApprovedDocImage]
(
@sLocation VARCHAR(MAX),
@record_id varchar(max)
)
AS
BEGIN
declare @a varchar(max)
set @a='select * from '+@sLocation+'..docimages DI inner join 
prolend_di..docdata DD on DI.record_id=DD.Record_id where DI.record_id='+@record_id+''
exec(@a)
END



GO

