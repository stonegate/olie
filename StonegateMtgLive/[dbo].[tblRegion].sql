USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRegion]') AND type in (N'U'))
DROP TABLE [dbo].[tblRegion]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRegion](	  [Id] INT NOT NULL IDENTITY(1,1)	, [RegionName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [RegionValue] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [IsActive] BIT NULL	, CONSTRAINT [PK_tblRegion] PRIMARY KEY ([Id] ASC))USE [HomeLendingExperts]
GO

