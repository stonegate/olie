USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_C3ClearCapitalValidating_Loan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[USP_C3ClearCapitalValidating_Loan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



Create PROCEDURE [dbo].[USP_C3ClearCapitalValidating_Loan]
(
@LoanNumber varchar(30) = '0',
@Role bigint = 14,
@UserId bigint = 28046
)
AS
BEGIN

 CREATE TABLE #tempLoan
 (
 DB VARCHAR(100),
 loan_no VARCHAR(100),
 CurrentStatus VARCHAR(100),
 LoanStatus VARCHAR(100),
 BorrowerLastName VARCHAR(100),
 YNValue VARCHAR(100),
 DecisionStatus VARCHAR(100),
 UnderwriterType VARCHAR(100),
 loan_amt VARCHAR(100),
 SubmittedDate VARCHAR(100),
 BorrowerFirstName VARCHAR(100)
 )

Declare @uRetailProcessorName  varchar(100)
Declare @custfielddef_id  varchar(100)
Set @custfielddef_id = (select ID from [mwsCustFieldDef] where ltrim(rtrim(CustomFieldName)) = 'Retail Processor')
set @uRetailProcessorName = (select ltrim(rtrim(ufirstname)) + ' '+ ltrim(rtrim(ulastname)) from tblUsers where userid = @UserId)

INSERT INTO #tempLoan Select distinct 'E3' DB,
M.LoanNumber loan_no ,
M.CurrentStatus,
left(M.CurrentStatus,2)  LoanStatus,
borr.lastname BorrowerLastName,
1 YNValue,
M.DecisionStatus,
'NA' UnderwriterType,
loanData.AdjustedNoteAmt,
appStat.StatusDateTime AS SubmittedDate,
borr.firstname BorrowerFirstName
 
from 
 (
Select * from mwlloanapp where ltrim(rtrim(LoanNumber)) Like @LoanNumber +'%' and ChannelType LIKE 'Ret%'  

--and CASE WHEN @Role = 0 THEN 1 ELSE originator_ID  END  = CASE WHEN @Role = 0 THEN 1 ELSE (select e3userid from tblUsers where Userid =@UserId ) END 
) M
INNER JOIN 
(
 select ltrim(rtrim(StringValue)) RetailProcessor,loanapp_id from mwlcustomfield
 where custfielddef_id = @custfielddef_id  and (@Role = 0 OR ltrim(rtrim(stringValue))= @uRetailProcessorName)
) C ON C.loanapp_id = M.Id
INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id,lastname,firstname from mwlBorrower group by loanapp_id,lastname,firstname) as borr on borr.loanapp_id = M.id
INNER JOIN (Select MAX(SequenceNum) SequenceNum,loanapp_id from mwlBorrower group by loanapp_id) as borrS on borr.loanapp_id = borrs.loanapp_id and  borrs.SequenceNum = borr.SequenceNum
Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = M.id
Left join mwlAppStatus appStat on M.ID=appStat.LoanApp_id and appStat.StatusDesc='01 - Registered' 
Left join mwlApprovalStatus approvalStat on M.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc ='Approved'

Select * from #tempLoan  
where (ISNUMERIC(LoanStatus) = 1 and  LoanStatus <  33) or LoanStatus LIKE 'RE%'
drop table #tempLoan

END



GO

