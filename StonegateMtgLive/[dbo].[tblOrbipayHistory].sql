USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblOrbipayHistory]') AND type in (N'U'))
DROP TABLE [dbo].[tblOrbipayHistory]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblOrbipayHistory](	  [id] INT NOT NULL IDENTITY(1,1)	, [custid] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [message] VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [errorcode] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [errdate] DATETIME NULL DEFAULT(getdate()))USE [HomeLendingExperts]
GO

