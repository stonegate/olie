USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateFaqs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateFaqs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create  Procedure [dbo].[sp_UpdateFaqs]
@ID bigint,
@Question varchar(1000),
@Answer Text,
@Published bit,
@DisplayOrder int
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

Update dbo.tblfaqs set Question=@Question,Answer=@Answer,Published=@Published,DisplayOrder=@DisplayOrder where ID=@ID
END


GO

