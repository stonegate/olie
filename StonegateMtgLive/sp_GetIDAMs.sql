USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetIDAMs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetIDAMs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetIDAMs]
(
@userid VARCHAR(MAX),
@MAID VARCHAR(MAX)
)
AS
BEGIN
Select uidprolender from tblusers where userid = @userid union select distinct(uidprolender)uidprolender from tblusers where userid in (select userid from tblusers where urole=2 and MAID=@MAID)
END

GO

