USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidateUserForKatalystBranchOffice]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ValidateUserForKatalystBranchOffice]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Shwetal Shah>
-- Create date: <25-04-2013>
-- Description:	<Check the specific User's branch office has Katalyst enabled or not>
-- =============================================
create PROCEDURE [dbo].[ValidateUserForKatalystBranchOffice]
(
 @UserID int
)
AS
BEGIN

	Select ISNULL(IsKatalyst,0) as IsKatalyst from E3KatalystBranchOffice WITH ( NOLOCK )
	Inner Join (SELECT dbo.mwcInstitution.ID,dbo.mwcInstitution.Office FROM dbo.mwcInstitution WITH ( NOLOCK )
		INNER JOIN dbo.mwcContact contact ON dbo.mwcInstitution.ID = contact.Institution_ID
		INNER JOIN dbo.mwaMWUser mwuser ON contact.EMail = mwuser.EMail
		WHERE dbo.mwcInstitution.Office LIKE 'Retail%' AND mwuser.ID=(Select E3UserId from tblUsers WITH ( NOLOCK ) Where UserID = @UserID)) Institution 
	ON Institution.ID = E3KatalystBranchOffice.E3BranchOfficeID


END

GO

