USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDailyActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDailyActivity]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--exec [dbo].[sp_GetDailyActivity] '19', '68913FEFFB654D3CA49C0701EC770902,0060375F1CBF4DA198D0365802AB76DE,09CEBD1DB5D048108BDFECC2D12854FD,CD66C36312874E2287EAC61BA2A1A82D,AA3782AC22F64C77B8F248BD9B781887,40C307EFE89744E5B5D3F40F0F2AAF38,FC5DE0C323604F228A669D91C1456E6A,A05DFED28EE041968D25E80E2D391CE9','BF7C6F5AFB7347A68F052A5C8987A7EA','','',1,'Retail Akron'

CREATE PROCEDURE [dbo].[sp_GetDailyActivity] (@RoleID int,
@strID varchar(max),
@oPuid varchar(max),
@FromDate varchar(20),
@ToDate varchar(20),
@isFliter bit = 0,
@Branch VARCHAR(MAX))
AS
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
DECLARE @SQL nvarchar(max)
SET @SQL = ''
  
SET @SQL ='Select distinct ''E3'' as DB,loanapp.ChannelType as BusType,loanapp.ID as record_id,LoanNumber as loan_no,
			borr.lastname as BorrowerName,borr.firstname as BorrowerFirstName,borr.lastname as BorrowerLastName,loanapp.DecisionStatus,
			loanapp.CurrentStatus as LoanStatus,loanapp.CurrentStatus as LoanStatusDA ,case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' 
			THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' WHEN ''M'' THEN  ''USDA/Rural Housing Service'' End as LoanProgram ,  
			case when loanapp.LoanPurpose1003 is null then '''' when loanapp.LoanPurpose1003 = ''PU'' then ''Purchase'' 
			when loanapp.LoanPurpose1003 = ''RE'' 
			then ''Refinance'' end as Purpose,CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed, 
			LoanData.AdjustedNoteAmt as LoanAmount,CONVERT(DECIMAL(10,2),(ISNULL(LoanData.LTV,0) * 100)) as LTV,
			CONVERT(DECIMAL(10,3),(LoanData.NoteRate * 100)) as int_rate,  
			LoanData.ShortProgramName as prog_code, Broker.Company as brok_name ,  
			loanapp.Originatorname , loanapp.Branch Branch1, loanapp.MWBranchCode,  institute.office Branch,loanapp.transType'
---Start --added by Tavant Team brd-125
IF(@isFliter =1)
	BEGIN
		SET @SQL = @SQL + ',region1.regionname AS Region '
	END
SET @SQL = @SQL +' FROM mwlLoanApp  loanapp
				   INNER JOIN mwaMWUser users  ON loanapp.Originator_ID = users.ID
			       INNER JOIN mwcInstitution institute  ON users.BranchInstitution_ID = institute.id
				   Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  and sequencenum=1  
				   Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id    
				   Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id  and Broker.InstitutionType = ''BRANCH''   
				   and Broker.objownerName=''BranchInstitution'''
						  
IF(@isFliter =1)
	BEGIN
		SET @SQL = @SQL + ' LEFT JOIN tblbranchoffices AS bo ON  bo.branch=institute.office 	
							LEFT JOIN tblregion AS region1   ON bo.region = region1.regionvalue '

	END
SET @SQL = @SQL + '  Where (loanapp.transtype=''R'' Or loanapp.transtype=''P'')'

IF (@isFliter = 1)
BEGIN
     SET @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) '
	 if(@Branch is not null and @Branch <>'')
	 begin
		SET @SQL = @SQL + 'and (institute.Office in(select * from dbo.SplitString('''+  @Branch  +''','','')))' 
     end
END
------------------END BRD-125
ELSE
BEGIN
   IF (@RoleID != 0 AND @RoleID != 1 AND @RoleID != 11)
   BEGIN
      IF (LEN(@strID) = 0)
      BEGIN
         IF (@RoleID = 2)
         BEGIN
           SET @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) '
         END
         ELSE
         IF (@RoleID = 3)
         BEGIN
           SET @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString(''' + @strID + ''','','')) or Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')))'
         END
         ELSE
         IF (@RoleID = 10   OR @RoleID = 7)
         BEGIN
            SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @oPuid + ''','','')) or loanapp.id in (select * from dbo.SplitString(''' + @strID + ''','','')))'
         END
      END
      ELSE
      BEGIN
         IF (@RoleID = 2)
         BEGIN
                
            SET @SQL = @SQL + ' and  Originator_id in (select * from dbo.SplitString(''' + @strID + ''','','')) '
         END
         ELSE
         IF (@RoleID = 3)
         BEGIN
              
            SET @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString(''' + @strID + ''','','')) or Originator_id in (select * from dbo.SplitString(''' + @oPuid + ''','','')))'
         END
         ELSE
         IF (@RoleID = 10
            OR @RoleID = 7)
         BEGIN
            SET @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString(''' + @oPuid + ''','','')) or loanapp.id in (select * from dbo.SplitString(''' + @strID + ''','','')))'
            
         END
      END
   END
END

IF (@FromDate != '' AND @ToDate != '')
	BEGIN
		SET @SQL = @SQL + ' and loanapp.CreatedonDate between ''' + @FromDate + '''  and  ''' + CONVERT(varchar(20), DATEADD(dd, 1, @ToDate), 101) + ''''
	END
ELSE
  BEGIN
	 IF (@FromDate != '')
		SET @SQL = @SQL + ' and loanapp.CreatedonDate > ''' + @FromDate + ''''
	ELSE
	 IF (@ToDate != '')
		SET @SQL = @SQL + ' and loanapp.CreatedonDate < ''' + @ToDate + ''''
	END
---- Start --Added By Tavant BRD-125----------------------
IF(@isFliter =1)
	BEGIN
		SET @SQL = @SQL + ' and (loanapp.CurrentStatus between ''03 - Appt Set to Review Discls''  and ''60 - Closed'' ) '
	END
-------------END -BRD-125----------------------------------------------
	ELSE
	BEGIN
		SET @SQL = @SQL + ' and (loanapp.CurrentStatus=''13 - File Intake'')'
	END
SET @SQL = @SQL + ' And  (loanData.FinancingType=''C'' or loanData.FinancingType=''F'' or loanData.FinancingType=''V'' or loanData.FinancingType=''M'') 
					  and loanapp.ChannelType like ''%RETAIL%''  and LoanNumber is not null and LoanNumber <> '''''

   PRINT @SQL
   EXEC sp_executesql @SQL
	


/*===========================Daily Lock Report & Fallout Report============================= */
END


--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



GO

