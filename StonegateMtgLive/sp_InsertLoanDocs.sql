USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLoanDocs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLoanDocs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_InsertLoanDocs]
(
@Loan_No VARCHAR(MAX),
@docs_text VARCHAR(MAX),
@FileName VARCHAR(MAX),
@dttime VARCHAR(MAX)
)
AS
BEGIN
Insert into tblLoanDocs(Loan_No,docs_text,FileName,dttime) Values
(@Loan_No,@docs_text,@FileName,@dttime)
END



GO

