USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetSecurityQueandAnswer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetSecurityQueandAnswer]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Select_tblGetSecurityQueandAnswer]
(
@sUserid varchar(Max)
)
as
begin

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select * From tblUserDetails ud  inner join tblSecurityQuestions sc 
on ud.SecurityQuestion2=sc.QuestionId 
where UsersID =@sUserid

select userloginid from tblusers where userid=@sUserid
end


GO

