USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblRetailUserRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblRetailUserRoles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[sp_Select_tblRetailUserRoles]                  
as                  
begin
--SELECT DISTINCT '-1' as ID, 'All User' as Roles,'1' as IsActive  FROM tblRoles            
--union ALL               
--select * from Retail_tblRoles where id NOT IN(9,10,6,18)    
SELECT
	*
FROM tblRoles
WHERE id NOT IN (9, 6, 18)                 
end  



GO

