USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ValidateLoan]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ValidateLoan]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

      
--Example for execute stored procedure     
--EXECUTE sp_ValidateLoan 1,0,'0000013164'  
--EXECUTE sp_ValidateLoan 15143,11,'0000366349'     
--EXECUTE sp_ValidateLoan 1,0,'0000333815'         
--EXECUTE sp_ValidateLoan 10144,1,'0000333815'          
--EXECUTE sp_ValidateLoan 166,2,'0000357464'          
--EXECUTE sp_ValidateLoan 76,3,'0000353743'         
--EXECUTE sp_ValidateLoan 226,7,'0000353743'         
--EXECUTE sp_ValidateLoan 226,7,'0000353743'         
--EXECUTE sp_ValidateLoan 226,7,'0000353743'         
--EXECUTE sp_ValidateLoan 132,10,'0000360684'    
--EXECUTE sp_ValidateLoan 132,10,'0000358076'     
--EXECUTE sp_ValidateLoan 76,3,'0000365345'    
--EXECUTE sp_ValidateLoan '15143', '11', '0000357992'    
--EXECUTE sp_ValidateLoan '3918', '14', '0000354867'      
--EXECUTE sp_ValidateLoan '3926', '15', '0000322775'        
--EXECUTE sp_ValidateLoan '18475', '16', '0000317476'    
--EXECUTE sp_ValidateLoan 12910,13,'0000366349'    
--EXECUTE sp_ValidateLoan 15143,11,'0000366349'    
-- =============================================              
-- Author:  <Stonegate>              
-- Create date: <06/4/2013>              
-- Description: <This wil Check for Loan number exist in roles>              
-- Modify Date : <12/4/2013>              
-- Modify By :   Chandresh Patel    
-- =============================================              
CREATE PROCEDURE [dbo].[sp_ValidateLoan]    
    (    
      --Parameter declaration              
  @UserID VARCHAR(10),          
  @RoleID CHAR(3),               
  @strLoanno VARCHAR(100)     
    )    
AS     
    BEGIN            
  --Variable Declaration         
        DECLARE @strID VARCHAR(MAX)      
  --declaration of variable and assign meaningful name to role id and set value of that receieved from parameter       
  --Start      
               
  DECLARE @Admin INT        
  SET @Admin=0        
      
  DECLARE @DirRetail INT         
  SET @DirRetail=1     
         
  DECLARE @MA INT         
  SET @MA = 2         
      
  DECLARE @BranchMgr INT        
  SET @BranchMgr=10        
          
  DECLARE @SalesMgr INT         
  SET @SalesMgr=3       
       
  DECLARE @RetailVP INT        
  SET @RetailVP=7        
           
  Declare @RetailOperMgr INT        
  SET @RetailOperMgr=11        
            
        Declare @ClientRelMgr INT        
  SET @ClientRelMgr=13      
      
  Declare @LoanProcessor INT        
  SET @LoanProcessor=14        
          
  Declare @Closer INT    
  SET @Closer=15        
          
  Declare @RetailTeamLd INT    
  SET @RetailTeamLd=16        
            
  Declare @AreaMgr INT        
  SET @AreaMgr=19      
  
  Declare @DisclosureDeskAnlst INT 
  SET @DisclosureDeskAnlst = 21
       
  --end of declaration of variable and assigment    
        
              /* Fetching E3UserID for Mortgate Advisor Role Only */    
                IF ( @RoleID = @MA     
                   )     
                    BEGIN          
                        SELECT  @strID = E3Userid    
                        FROM    dbo.tblUsers    
                        WHERE   userid = @UserID       
                            
                    END      
   /*    
   -- base Query     
   select count(Loannumber)as LoanCount          
   from mwlLoanApp loanapp    with (NOLOCK)       
   Left join dbo.mwlInstitution as insitution on insitution.ObjOwner_id = loanapp.id               
   and insitution.InstitutionType=loanapp.ChannelType      
   where LoanNumber is not null and LoanNumber <>''    
     */    
         
     /* Fetching Loan Number Count for Dir. Of Retail,Retail VP.Loan processor,Closer,    
        Retail Team Lead,Client Relationship Manager and Retail Operation Manager Role */    
    IF ( @RoleID = @DirRetail     
      OR @RoleID = @RetailVP    
      OR @RoleID = @LoanProcessor    
      OR @RoleID = @Closer     
                     OR @RoleID = @RetailTeamLd     
                     OR @RoleID = @Admin      
                     OR @RoleID = @ClientRelMgr    
                     OR @RoleID = @RetailOperMgr   
                     OR @RoleID = @DisclosureDeskAnlst  
                   )     
                    BEGIN    
                 SELECT  COUNT(Loannumber) AS LoanCount    
                        FROM    mwlLoanApp loanapp WITH ( NOLOCK )    
                                LEFT JOIN dbo.mwlInstitution AS insitution WITH ( NOLOCK ) ON insitution.ObjOwner_id = loanapp.id    
                                                              AND insitution.InstitutionType = loanapp.ChannelType     
                        WHERE   LoanNumber IS NOT NULL    
                                AND LoanNumber <> ''     
                                AND ( LoanNumber = @strLoanno )     
                    
                    END      
            /* Fetching Loan Number Count for Mortgage Advisor Role According to their Loan number and E3User ID*/       
                IF ( @RoleID = @MA    
                   )     
                    BEGIN    
                        SELECT  COUNT(Loannumber) AS LoanCount    
                        FROM    mwlLoanApp loanapp WITH ( NOLOCK )    
                                LEFT JOIN dbo.mwlInstitution AS insitution WITH ( NOLOCK ) ON insitution.ObjOwner_id = loanapp.id    
                                                              AND insitution.InstitutionType = loanapp.ChannelType     
                        WHERE   LoanNumber IS NOT NULL    
                                AND LoanNumber <> ''    
                                AND ( Originator_id = @strID )    
                                AND ( LoanNumber = @strLoanno )     
                    
                    END     
           /* Fetching Loan Number Count for Area Manager Role */       
                IF ( @RoleID = @AreaMgr     
                   )     
                    BEGIN    
                        SELECT  COUNT(Loannumber) AS LoanCount    
                        FROM    mwlLoanApp loanapp WITH ( NOLOCK )    
                                LEFT JOIN dbo.mwlInstitution AS insitution WITH ( NOLOCK ) ON insitution.ObjOwner_id = loanapp.id
                                 AND insitution.InstitutionType = loanapp.ChannelType     
                        WHERE   LoanNumber IS NOT NULL    
                                AND LoanNumber <> ''    
                                --AND ( Originator_id IN (     
                                --      SELECT E3Userid FROM tblusers WITH ( NOLOCK ) where     
                                --      office=(SELECT office from tblusers WITH ( NOLOCK )     
                                --      where urole in(@MA,@SalesMgr,@RetailVP,@BranchMgr,@AreaMgr)     
                                --      and userid=@UserID) ) )    
                                AND ( LoanNumber = @strLoanno )             
                    END     
                /* Fetching Loan Number Count for Branch Manager Role */     
                IF ( @RoleID = @BranchMgr    
                   )     
                    BEGIN    
                         SELECT  COUNT(Loannumber) AS LoanCount    
                        FROM    mwlLoanApp loanapp WITH ( NOLOCK )    
                                LEFT JOIN dbo.mwlInstitution AS insitution WITH ( NOLOCK ) ON insitution.ObjOwner_id = loanapp.id  
                                AND insitution.InstitutionType = loanapp.ChannelType     
                        WHERE   LoanNumber IS NOT NULL    
                                AND LoanNumber <> ''     
                                AND ( LoanNumber = @strLoanno )               
                    END    
               /* Fetching Loan Number Count for Sales Manager Role */     
                IF (@RoleID = @SalesMgr     
                   )     
                    BEGIN    
                        SELECT  COUNT(Loannumber) AS LoanCount    
                        FROM    mwlLoanApp loanapp WITH ( NOLOCK )    
                                LEFT JOIN dbo.mwlInstitution AS insitution WITH ( NOLOCK ) ON insitution.ObjOwner_id = loanapp.id  
                                AND insitution.InstitutionType = loanapp.ChannelType    
                        WHERE   LoanNumber IS NOT NULL    
                                AND LoanNumber <> ''    
                                AND ( Originator_id IN (     
                                      SELECT E3Userid FROM tblusers WITH ( NOLOCK ) where     
                                      office=(SELECT office FROM tblusers WITH ( NOLOCK )     
                                      where urole in(@RoleID)     
                                      and userid=@UserID) ) )    
                                AND ( LoanNumber = @strLoanno )              
                    END      
            END   


GO

