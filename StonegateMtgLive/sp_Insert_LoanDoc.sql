USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_LoanDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_LoanDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Insert_LoanDoc] 
(
@Loan_No char(15),
@docs_text text,      
@FileName varchar(200)
)
AS
BEGIN
	 INSERT INTO tblloandocs (Loan_No,docs_text,FileName,dttime)      
     VALUES  (@Loan_No,@docs_text,@FileName,getdate())       
  
END









GO

