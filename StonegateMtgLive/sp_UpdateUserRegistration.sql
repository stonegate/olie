USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateUserRegistration]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateUserRegistration]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsUserRegistrationE3.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_UpdateUserRegistration]
(
@ufirstname varchar(Max),
@ulastname varchar(Max),
@uemailid varchar(Max),
@urole varchar(Max),
@isadmin bit,
@isactive bit,
@uidprolender varchar(Max),
@uparent varchar(Max),
@upassword varchar(Max),
@mobile_ph varchar(Max),
@userid varchar(Max)

)
AS
BEGIN

	 update tblusers set ufirstname=@ufirstname,ulastname=@ulastname,uemailid=@uemailid,urole=@urole,isadmin=@isadmin,isactive=@isactive,uidprolender=@uidprolender,uparent=@uparent,upassword=@upassword,mobile_ph=@mobile_ph where userid=@userid
                
END

GO

