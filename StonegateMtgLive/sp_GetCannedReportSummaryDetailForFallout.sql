USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCannedReportSummaryDetailForFallout]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCannedReportSummaryDetailForFallout]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[sp_GetCannedReportSummaryDetailForFallout]        
(        
@RoleID int,       
@strID varchar(2000),        
@BrokerId char(2),      
@TeamOPuids varchar(30),          
@Year char(4),       
@oPuid varchar(100),      
@Month char(4)      
)        
AS        
BEGIN        
        
Declare @SQL nvarchar(max)        
Set @SQL =''        
Set @SQL =' select ''E3'' as DB, loanapp.ID as record_id,CONVERT(DECIMAL(10,3),(loanData.NoteRate * 100)) as int_rate,LoanNumber as loan_no,      
   borr.lastname as BorrowerLastName,      
   CONVERT(DECIMAL(10,2),isnull(loanData.LTV * 100,0))LTV,CurrentStatus as LoanStatus,convert(varchar(35),loanapp.DateCreated,101)      
   AS DateCreated,DateAppSigned as DateApproved,case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL''       
   WHEN ''V'' THEN  ''VA'' End as LoanProgram, case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase''       
   when TransTYpe = ''R'' then ''Refinance'' end as Purpose, CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed,loanData.UseCashInOut      
   as CashOut,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,LockRecord.LockExpirationDate,LockRecord.LockDateTime,      
   LockRecord.LockDateTime as LOCK_DATE     from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id and       
   borr.sequencenum=1    Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id        
   Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''Branch''       
   and Broker.objownerName=''BranchInstitution'' Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and       
   Corres.InstitutionType = ''CORRESPOND'' and Corres.objownerName=''Correspondent''       
   left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.locktype=''LOCK'' and      
   LockRecord.Status<>''CANCELED''       
            where borr.sequencenum=1 and Year(LockRecord.LockDateTime)='''+ @Year +''' and Month(LockRecord.LockDateTime)='''+ @Month +''''      
      
                if (@BrokerId != '' and @BrokerId != 0)      
                       
                   Set @SQL = @SQL + ' and (Broker.CompanyEmail ='+ @BrokerId +' or Corres.CompanyEmail ='+ @BrokerId +')'       
                       
      
                if (@TeamOPuids = 'broker')      
                       
                     Set @SQL = @SQL + ' and loanapp.ChannelType=''BROKER'' '      
                       
                if (@TeamOPuids = 'retail')      
                       
                     Set @SQL = @SQL + ' and loanapp.ChannelType=''RETAIL'''       
                       
                if (@TeamOPuids = 'correspond')      
                       
                   Set @SQL = @SQL + ' and loanapp.ChannelType=''CORRESPOND'''       
                       
      
                      
                 Set @SQL = @SQL + ' and loanapp.ChannelType like ''%RETAIL%'''        
                       
      
                if (@TeamOPuids != '' and @TeamOPuids != 'retail' and @TeamOPuids != 'broker' and @TeamOPuids != 'correspond')      
                BEGIN      
                    Set @SQL = @SQL + ' and (Originator_id in (' + @TeamOPuids +')or loanapp.id in (select * from dbo.SplitString('''+ @strID +''','',''))or Broker.office in('''+ @TeamOPuids +'''))'      
                END      
                else      
                  BEGIN      
                    if (@RoleID != 0 and @RoleID != 1)      
     BEGIN       
                        if (len(@strID) = 0)      
                        BEGIN      
                            if (@RoleID =  3)      
                                   
                                 Set @SQL = @SQL + ' and (loanapp.ID in (select * from dbo.SplitString('''+ @strID +''','','')) or Originator_id in (''' + @oPuid + ''') )'      
                                   
                                 
                            if (@RoleID =  2)    
                                   
                                 Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+ @strID +''','','')))'      
                     
                            if (@RoleID = 10 or @RoleID  = 7)       
                                 Set @SQL = @SQL + ' and (Originator_id in (select * from dbo.SplitString('''+ @strID +''','','')) or loanapp.id in (select * from dbo.SplitString('''+ @strID +''','','')))'      
                                     
                        END      
                              
                        else      
                        BEGIN      
                            if (@RoleID = 3)      
                                   
                               Set @SQL = @SQL + '  and (loanapp.ID in (select * from dbo.SplitString('''+ @strID +''','','')) or Originator_id in (''' + @oPuid + ''') )'      
                                   
                                  
                            if (@RoleID = 2)      
                                   
                               Set @SQL = @SQL + '   and (Originator_id in (select * from dbo.SplitString('''+ @strID +''','','')))'      
                                   
                            if (@RoleID  = 10 or @RoleID =  7)       
       
                               Set @SQL = @SQL + '  and (Originator_id in (select * from dbo.SplitString('''+ @strID +''','','')) or loanapp.id in (select * from dbo.SplitString('''+ @strID +''','','')))'      
                                    
                       END      
     END      
     END       
          
Print @SQL        
exec sp_executesql @SQL        
END   


GO

