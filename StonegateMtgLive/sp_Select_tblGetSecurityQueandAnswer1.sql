USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetSecurityQueandAnswer1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetSecurityQueandAnswer1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Select_tblGetSecurityQueandAnswer1]
(
@sUserid varchar(Max)
)
as
begin

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select ud.*,isnull(ud.MaxSecurityAttempt,0) as MaxSecurityAttempt1 , isnull(ud.MaxUserIdSecurityAtmp,0) as MaxUserIdSecurityAtmp1 , isnull(ud.UserIdlastlogintime,getdate()) as UserIdlastlogintime1
,isnull(ud.lastlogintime,getdate()) as lastlogintime1,sc2.SecurityQuestion 
as SecurityQuestion_2,sc2.DisplayAt as DisplayAt_2
,sc1.SecurityQuestion as SecurityQuestion_1,sc1.DisplayAt as DisplayAt_1
,sc3.SecurityQuestion as SecurityQuestion_3,sc3.DisplayAt as DisplayAt_3 
From tblUserDetails ud inner join tblSecurityQuestions sc2 on ud.SecurityQuestion2=sc2.QuestionId inner join tblSecurityQuestions sc1 
on ud.SecurityQuestion1=sc1.QuestionId inner join tblSecurityQuestions sc3 on ud.SecurityQuestion3=sc3.QuestionId  
where UsersID =@sUserid

select userloginid from tblusers where userid=@sUserid
end


GO

