USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SetLoanStatusSubmitLoans]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SetLoanStatusSubmitLoans]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  <Author,,Name>      
-- Create date: <Create Date,,>      
-- Description: <Description,,>      
-- =============================================      
CREATE PROCEDURE sp_SetLoanStatusSubmitLoans      
(       
 @LoanNumber varchar(20)      
)      
AS      
BEGIN      
      
Declare @LoanAppID varchar(200)      
    set @LoanAppID = (select id from mwlloanapp where loannumber=@LoanNumber)      
      
if not exists(select * From mwlappstatus where LoanApp_id in(@LoanAppID)       
    and StatusDesc='09 - Application Received')      
Insert into mwlappstatus(ID,LoanApp_id,StatusDesc,StatusDateTime)values      
                        (replace(newid(),'-',''),@LoanAppID,'09 - Application Received',getdate())      
                              
Update       
 mwlloanapp       
 Set CurrentStatus='09 - Application Received',CurrPipeStatusDate=getdate()       
    Where loannumber = @LoanNumber      
        
END 
GO

