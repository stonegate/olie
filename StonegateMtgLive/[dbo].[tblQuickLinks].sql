USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblQuickLinks]') AND type in (N'U'))
DROP TABLE [dbo].[tblQuickLinks]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblQuickLinks](	  [ID] INT NOT NULL	, [Linkname] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Description] VARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [Roles] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, CONSTRAINT [PK_tblQuickLinks] PRIMARY KEY ([ID] ASC))USE [HomeLendingExperts]
GO

