USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetValidPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetValidPassword]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Shushant	<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sp_GetValidPassword
	-- Add the parameters for the stored procedure here
        
@uemailid varchar(200)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select userid from tblusers where uemailid =@uemailid        
 -- Insert statements for procedure here
	
END

GO

