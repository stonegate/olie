USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateNewRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateNewRate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_UpdateNewRate]
(
@strloantype varchar(Max),
@LoanProgram varchar(Max),
@GoalOfLoan varchar(Max),
@IPoints varchar(Max),
@DecRate bit,
@DecAPR bit,
@DecClosingCost varchar(Max),
@ModifiedDate varchar(Max),
@NewRateID varchar(Max)
)
AS
BEGIN

update tblnewrates set strloantype=@strloantype,LoanProgram=@LoanProgram,GoalOfLoan=@GoalOfLoan,
IPoints=@IPoints,DecRate=@DecRate,DecAPR=@DecAPR,DecClosingCost=DecClosingCost,ModifiedDate=@ModifiedDate
 where NewRateID=@NewRateID
                
END

GO

