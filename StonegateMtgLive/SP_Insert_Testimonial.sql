USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Insert_Testimonial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Insert_Testimonial]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel
-- Create Date : 10/04/2012  
-- Description: Procedure for Insert Testimonial 
-- =============================================     
CREATE PROC [dbo].[SP_Insert_Testimonial](    
@UName varchar(50),   
@Testimonial char(1),  
@SubmittedDate datetime,  
@UserID int,  
@UpdatedDt datetime,  
@City varchar(50),    
@State varchar(50),   
@TestimonialType char(1))                
AS                
BEGIN           
  INSERT INTO tblTestimonials (UName,Testimonial,SubmittedDate,UserID,UpdatedDt,  
  City, State, TestimonialType)   
  values(@UName,@Testimonial,@SubmittedDate,@UserID,@UpdatedDt,  
  @City,@State,@TestimonialType)  
     
END 

GO

