USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetLockActivityReportWithRegion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetLockActivityReportWithRegion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[SP_GetLockActivityReportWithRegion]
(          
        
@strID varchar(2000),  
@FromDate VARCHAR(15),
@ToDate VARCHAR(15),
@oPuid varchar(2000)
)          
AS          
BEGIN          
          
Declare @SQL nvarchar(max)          
Set @SQL =''          
Set @SQL ='select ''E3'' as DB, loanapp.ID as record_id,loanapp.Channeltype as BusType,LoanNumber as loan_no,borr.firstname as BorrowerFirstName ,
			borr.lastname as BorrowerLastName,CurrentStatus as LoanStatus,loanapp.Originatorname, 
			convert(varchar(35),loanapp.DateCreated,101) AS DateCreated,DateAppSigned as DateApproved,LoanData.LoanProgramName as Prog_desc, 
			case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase'' when TransTYpe = ''R'' then ''Refinance'' 
			end as Purpose,CAST(isnull(borr.CompositeCreditScore,0) AS CHAR(5)) as CreditScoreUsed,(Loandata.NoteRate * 100) as Int_rate, 
			loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,
			convert(varchar(40),LockRecord.LockExpirationDate,101) as LockExpires,LockRecord.LockDateTime,
			convert(varchar(35),LockRecord.LockDateTime,101) as Lockdate,UnderwriterName as UnderWriter,CloserName as Closer,
			CASE ISNULL(RTRIM(Broker.Company),'''') WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,'''' as
			 FundedDate, '''' as ReceivedInUW,LoanData.ShortProgramName as prog_code  from mwlLoanApp  loanapp   
			Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  
			Inner join dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id  
			left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  
			Left join dbo.mwlInstitution as Broker on Broker.ObjOwner_id = loanapp.id and Broker.InstitutionType = ''BROKER'' 
			and Broker.objownerName=''Broker'' 
			left join dbo.mwlLockRecord as LockRecord on LockRecord.LoanApp_ID = loanapp.id and LockRecord.LockType=''LOCK'' and 
			LockRecord.Status<>''CANCELED'' 
			Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''CORRESPOND'' 
			and Corres.objownerName=''Correspondent'' 
			where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> '''''

                if (len(@oPuid) = 0)
                 BEGIN
                      Set @SQL = @SQL + 'and (Originator_id in ('''+ @oPuid +''') or loanapp.id in (''' + @strID  + '''))' 
                 END
                else
					BEGIN
                 Set @SQL = @SQL + ' and (Originator_id in (" + OPuid.ToString() + ") or loanapp.id in (''' + @strID  + '''))' 
					END

				Set @SQL = @SQL + ' and LockRecord.LockDateTime between ''' + @FromDate + ''' and ''' + @ToDate + '''' 
				Set @SQL = @SQL + ' and loanapp.ChannelType like ''%RETAIL%''' 

  
     
 Print @SQL          
exec sp_executesql @SQL          
END 

GO

