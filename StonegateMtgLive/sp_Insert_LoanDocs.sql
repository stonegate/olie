USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Insert_LoanDocs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Insert_LoanDocs]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_Insert_LoanDocs] 
(
@uloanregid int,
@Docname varchar(100),      
@Comments varchar(500)
)
AS
BEGIN
	 INSERT INTO tblloandoc (uloanregid,Docname,Comments)      
     VALUES  (@uloanregid,@Docname,@Comments)       
  
END








GO

