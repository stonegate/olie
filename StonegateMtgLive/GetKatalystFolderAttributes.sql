USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetKatalystFolderAttributes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetKatalystFolderAttributes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <SHWETAL SHAH>  
-- Create date: <05-04-13>  
-- Description: <get the list of values for katalyst folder attributes>  
-- =============================================  
CREATE PROCEDURE [dbo].[GetKatalystFolderAttributes]  
(  
@LoanNumber varchar(50)  
)  
AS  
BEGIN  
  
Select Bor.FirstName,Bor.LastName, mwlLoanApp.Originator_ID,mwlLoanApp.OriginatorName,mwlLoanApp.LienPosition  
,(Select top(1) AdjustedNoteAmt from mwlLoanData WITH ( NOLOCK ) where ObjOwner_ID = mwlLoanApp.ID and active=1 order by ID desc) as LoanAmount  
,Prop.Street,Prop.City,Prop.State,Prop.County, Prop.ZipCode ,(select top(1) LoanProgramName from mwlLoanData WITH ( NOLOCK ) where ObjOwner_Id = mwlLoanApp.ID and active=1 order by ID desc) as LoanProgram2
, Case When ISNULL((Select StringValue from mwlCustomField WITH ( NOLOCK ) where CustFieldDef_ID = (Select ID From mwsCustFieldDef WITH ( NOLOCK ) where MWFieldNum = 9966) and LoanApp_ID=mwlLoanApp.ID),'') = '' 
	Then ISNULL(mwlLoanApp.ProcessorName,'')
	Else ISNULL((Select StringValue from mwlCustomField WITH ( NOLOCK ) where CustFieldDef_ID = (Select ID From mwsCustFieldDef WITH ( NOLOCK ) where MWFieldNum = 9966) and LoanApp_ID=mwlLoanApp.ID),'')
	End as ProcessorName
From mwlLoanApp WITH ( NOLOCK )
Inner JOin (Select FirstName,LastName,LoanApp_ID From mwlBorrower WITH ( NOLOCK ) where sequencenum=1) as Bor  
ON Bor.LoanAPP_ID = mwlLoanApp.ID  
Inner Join (Select LoanApp_ID,(ISNULL(Street,'')+','+ISNULL(Street2,'')) as Street, City, State,ISNULL(County,'') County, ZipCode from mwlSubjectProperty WITH ( NOLOCK )) Prop  
On Prop.LoanApp_ID = mwlLoanApp.ID 
Where LoanNumber = @LoanNumber  
  
END







GO

