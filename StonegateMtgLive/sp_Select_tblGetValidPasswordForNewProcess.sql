USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Select_tblGetValidPasswordForNewProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Select_tblGetValidPasswordForNewProcess]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Select_tblGetValidPasswordForNewProcess]
(
@sUserloginid varchar(Max)
,@sEmailID varchar(Max)
)
as
begin
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


IF(@sUserloginid=@sEmailID)
BEGIN
	select top 1 userid from tblusers where  uemailid = @sEmailID and IsActive=1 and (userloginid IS NULL OR userloginid='')
END

ELSE IF( @sUserloginid IS NOT NULL and @sUserloginid <> '')
BEGIN
	select top 1 userid from tblusers where Userloginid=@sUserloginid and uemailid = @sEmailID and IsActive=1
END
ELSE
BEGIN
	select top 1 userid from tblusers where  uemailid = @sEmailID and IsActive=1

END
end




GO

