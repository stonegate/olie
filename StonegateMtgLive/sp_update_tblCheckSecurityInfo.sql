USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_update_tblCheckSecurityInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_update_tblCheckSecurityInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_update_tblCheckSecurityInfo]
(
@sUserid varchar(Max),
@IsUserid varchar(Max) = NULL
)
as
begin
if(@IsUserid = '' OR @IsUserid IS  NULL)
	update tblUserDetails set lastlogintime = null,MaxSecurityAttempt =0 where UsersID = @sUserid
ELSE
	update tblUserDetails set UserIdlastlogintime = null,MaxUserIdSecurityAtmp =0 where UsersID = @sUserid
end


GO

