USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateShortLoanApplication1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateShortLoanApplication1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateShortLoanApplication1]
	-- Add the parameters for the stored procedure here
	@LoanApplicationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update tblLoanApplication set IsLead360=1, IsFullApp=1 WHERE LoanApplicationID=@LoanApplicationID
END



GO

