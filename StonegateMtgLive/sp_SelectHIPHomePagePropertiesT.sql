USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SelectHIPHomePagePropertiesT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SelectHIPHomePagePropertiesT]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Harsh Pandit, Rajnish Pandey  
-- Create date: 16 April 2010  
-- Description: to get record for Properties  
-- [sp_SelectHIPHomePagePropertiesT]1,0 ,'13'  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_SelectHIPHomePagePropertiesT] 
 @SortID int,  
 @PageNo int,
 @PropIDs varchar(MAX)	   
AS  
BEGIN  
 DECLARE @PropID VARCHAR(50)   
 DECLARE @SortField VARCHAR(50)   
 DECLARE @PageRecord INT   
 DECLARE @SortField1 VARCHAR(50) 
 DECLARE @COUNT INT

 CREATE TABLE #Temp(  
 [ID] [int] IDENTITY(1,1) NOT NULL,  
 [PropID] [int] NULL,  
 [RealtorID] [int] NULL,  
 [PropStatus] [varchar](50) NULL,  
 [ListingTYpe] [varchar](50) NULL,  
 [BedRooms] [int] NULL,  
 [Baths] [decimal](18, 1) NULL,  
 [Price] [decimal](18, 2) NULL,  
 [DaysListed] [int] NULL,  
 [Street] [varchar](50) NULL,  
 [State] [varchar](50) NULL,  
 [City] [varchar](50) NULL,  
 [Zipcode] [varchar](50) NULL,  
 [YearBuilt] [int] NULL,  
 [IsActive] [bit] NULL,  
 [PropDesc] [ntext] NULL,  
 [lat] [decimal](18, 6) NULL,  
 [lng] [decimal](18, 6) NULL ,  
 [Area] [int] NULL,  
 [logoname] [varchar](50) NULL,  
 [uemailid] [varchar](50) NULL,  
 [PropImageName] [varchar](50) NULL,  
 [FullAddress] [varchar] (250),  
 [Amount] [varchar] (250),
 [ImageCount] [int] NULL  
 )  
 DECLARE db_cursor CURSOR FOR    
 SELECT PropID from tblProperties   
  
 OPEN db_cursor     
 FETCH NEXT FROM db_cursor INTO @PropID  
  
 WHILE @@FETCH_STATUS = 0     
 BEGIN   
   SET @COUNT=0
   INSERT INTO #Temp (PropID,RealtorID,PropStatus,ListingType,Bedrooms,baths,price,dayslisted,street,[state],city,zipcode,yearbuilt,lat,lng,Area,logoname,uemailid,PropImageName,FullAddress,Amount)  
   SELECT  TOP(1) prop.PropID, prop.RealtorID,PropStatus,ListingType,Bedrooms,baths,price,datediff(day,CreatedDate,getdate()) as dayslistedcalc,street,prop.[state],prop.city,zipcode,yearbuilt,prop.lat,prop.lng,prop.Area,
   CASE WHEN users.logoname='' THEN 'nologo.gif' ELSE ISNULL(users.logoname,'nologo.gif')END logoname,users.uemailid,  
   ISNULL(PropImageName,'noHome.png')PropImageName,street + '+' + prop.[state] + '+' + prop.city + '+' + zipcode as FullAddress, PARSENAME(Convert(varchar,Convert(money,Amount),1),2)
   FROM  tblProperties prop  
   LEFT OUTER JOIN  tblPropertyImage propImages ON prop.propid = propImages.propid  
   JOIN tblusers users ON users.userid = prop.realtorID  
   WHERE prop.PropID=@PropID  AND prop.isactive <> 0
   SET @COUNT=(SELECT COUNT('A') FROM  tblPropertyImage  WHERE propid=@PropID )
   Update #Temp SET ImageCount=@COUNT WHERE PropID=@PropID
     
     FETCH NEXT FROM db_cursor INTO @PropID     
 END     
  
 SET @PageRecord=@PageNo*4   
  
 IF(@PageRecord=0)  
 BEGIN  
  IF(@SortID = 0)   
   BEGIN   
    SELECT * FROM #Temp ORDER BY DaysListed asc  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 1)   
   BEGIN   
    SELECT * FROM #Temp ORDER BY CONVERT(DECIMAL(18,2),REPLACE(Amount,',','')) DESC  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 2)   
   BEGIN   
    SELECT * FROM #Temp ORDER BY BedRooms DESC  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 3)   
   BEGIN   
    SELECT * FROM #Temp ORDER BY Baths DESC  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 4)   
   BEGIN   
    SELECT * FROM #Temp ORDER BY Area DESC  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 5)   
   BEGIN   
    SELECT * FROM #Temp ORDER BY YearBuilt DESC  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
 END  
 ELSE  
 BEGIN  
  IF(@SortID = 0)   
   BEGIN   
    WITH [#Temp ORDERED BY ROWID] AS (SELECT ROW_NUMBER() OVER (ORDER BY DaysListed DESC) AS ROWID, * FROM #Temp)SELECT * FROM [#Temp ORDERED BY ROWID] WHERE ROWID >@PageRecord AND ROWID <@PageRecord+4  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 1)   
   BEGIN   
    WITH [#Temp ORDERED BY ROWID] AS (SELECT ROW_NUMBER() OVER (ORDER BY  CONVERT(DECIMAL(18,2),REPLACE(Amount,',','')) DESC ) AS ROWID, * FROM #Temp)SELECT * FROM [#Temp ORDERED BY ROWID] WHERE ROWID >@PageRecord AND ROWID <@PageRecord+4  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 2)   
   BEGIN   
    WITH [#Temp ORDERED BY ROWID] AS (SELECT ROW_NUMBER() OVER (ORDER BY BedRooms DESC) AS ROWID, * FROM #Temp)SELECT * FROM [#Temp ORDERED BY ROWID] WHERE ROWID >@PageRecord AND ROWID <@PageRecord+4  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 3)   
   BEGIN   
    WITH [#Temp ORDERED BY ROWID] AS (SELECT ROW_NUMBER() OVER (ORDER BY Baths DESC) AS ROWID, * FROM #Temp)SELECT * FROM [#Temp ORDERED BY ROWID] WHERE ROWID >@PageRecord AND ROWID <@PageRecord+4  
    SELECT COUNT('A')PageRecord FROM  #Temp    
   END  
  ELSE IF(@SortID = 4)   
   BEGIN   
    WITH [#Temp ORDERED BY ROWID] AS (SELECT ROW_NUMBER() OVER (ORDER BY Area DESC) AS ROWID, * FROM #Temp)SELECT * FROM [#Temp ORDERED BY ROWID] WHERE ROWID >@PageRecord AND ROWID <@PageRecord+4  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
  ELSE IF(@SortID = 5)   
   BEGIN   
    WITH [#Temp ORDERED BY ROWID] AS (SELECT ROW_NUMBER() OVER (ORDER BY YearBuilt DESC) AS ROWID, * FROM #Temp)SELECT * FROM [#Temp ORDERED BY ROWID] WHERE ROWID >@PageRecord AND ROWID <@PageRecord+4  
    SELECT COUNT('A')PageRecord FROM  #Temp  
   END  
 END  
 SELECT * INTO 	#Temp1 FROM #Temp 

IF(@SortID = 0)   
   BEGIN   
     SELECT * FROM 	#Temp1 WHERE PropID IN ((SELECT VAL FROM dbo.getTableByIDs(@PropIDS))) ORDER BY DaysListed asc  
   END  
  ELSE IF(@SortID = 1)   
   BEGIN   
     SELECT * FROM 	#Temp1 WHERE PropID IN ((SELECT VAL FROM dbo.getTableByIDs(@PropIDS))) ORDER BY  CONVERT(DECIMAL(18,2),REPLACE(Amount,',','')) DESC  
   END  
  ELSE IF(@SortID = 2)   
   BEGIN   
     SELECT * FROM 	#Temp1 WHERE PropID IN ((SELECT VAL FROM dbo.getTableByIDs(@PropIDS))) ORDER BY BedRooms DESC  
   END  
  ELSE IF(@SortID = 3)   
   BEGIN   
     SELECT * FROM 	#Temp1 WHERE PropID IN ((SELECT VAL FROM dbo.getTableByIDs(@PropIDS))) ORDER BY Baths DESC  
   END  
  ELSE IF(@SortID = 4)   
   BEGIN   
     SELECT * FROM 	#Temp1 WHERE PropID IN ((SELECT VAL FROM dbo.getTableByIDs(@PropIDS))) ORDER BY Area DESC  
   END  
  ELSE IF(@SortID = 5)   
   BEGIN   
     SELECT * FROM 	#Temp1 WHERE PropID IN ((SELECT VAL FROM dbo.getTableByIDs(@PropIDS))) ORDER BY YearBuilt DESC  
   END  
 


 



   
 DROP TABLE #Temp  
 DROP TABLE #Temp1 
 CLOSE db_cursor     
 DEALLOCATE db_cursor    
 END  

GO

