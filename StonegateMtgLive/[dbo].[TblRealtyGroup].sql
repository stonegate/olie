USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TblRealtyGroup]') AND type in (N'U'))
DROP TABLE [dbo].[TblRealtyGroup]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblRealtyGroup](	  [GroupId] BIGINT NOT NULL IDENTITY(1,1)	, [GroupName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO

