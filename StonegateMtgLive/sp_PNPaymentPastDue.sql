USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PNPaymentPastDue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PNPaymentPastDue]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Harsh Pandit
-- Create date: 25 March 2010
-- Description:	to get data for Payment Past Due Reminder
-- modify : Vipul Thakkar
-- Date : 05 OCT 2011
-- =============================================
CREATE PROCEDURE [dbo].[sp_PNPaymentPastDue] 
AS
BEGIN
	SET NOCOUNT ON;
select convert(varchar(10), dateadd(day,- PaymentPastDueReminderDay * 1 ,dbo.GetNotificationDueDate() ),101)as PaymentPastDueNotificationDate
, PaymentPastDueReminderDay, 
dbo.GetNotificationDueDate() as NotificationDuedateforMonth , record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,   serviceinfo.userid,
	isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt,  loanid,convert(varchar, duedate, 1) as duedate,  
	DATEDIFF(DAY,getdate(),duedate) as pastDueDays,  IsSMSPaymentPastDue,IsEmailPaymentPastDue,IsPaymentPastDue,
	PaymentPastDueReminderDay,PaymentPastDueDate,    
	mobile_ph, carrierid, (select carrieremailsms from carrier where id = carrierid) as smsgateway
from [winserver].[service].dbo.loan     
	join tblServiceInfo as serviceInfo on loan.loanid = serviceInfo.loanno    
	join tblUsers as users on users.userid = serviceinfo.userid    
--where 
where 
-- loanid='0000106071'
	--DATEDIFF(DAY,getdate(),duedate) <= 0    
	 IsPaymentPastDue = 1   and
	 dateadd(day,- PaymentPastDueReminderDay * 1 ,dbo.GetNotificationDueDate()) = convert(varchar(10), getdate(),101)
	--and DATEDIFF(DAY,getdate(),duedate) = (PaymentPastDueReminderDay * -1)
	and DATEDIFF(DAY,lastpmtRecvdDate,duedate) > datediff(day,getdate(),dateadd(month,1,getdate()))
and  month(Duedate)=month(getDate()) and year(DueDate)=year(getdate())
---- select record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,   serviceinfo.userid,
----isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt,  loanid,convert(varchar, lastpmtRecvdDate, 1) as lastpmtRecvdDate,  
----DATEDIFF(DAY,getdate(),lastpmtRecvdDate) as pastDueDays,  IsSMSPaymentPastDue,IsEmailPaymentPastDue,IsPaymentPastDue,
----PaymentPastDueReminderDay,PaymentPastDueDate,    
----mobile_ph, carrierid,
----	(select carrieremailsms from carrier where id = carrierid) as smsgateway
----from [WINSERVER].[service].dbo.loan     
----join tblServiceInfo as serviceInfo on loan.loanid = serviceInfo.loanno    
----join tblUsers as users on users.userid = serviceinfo.userid    
----where (month(PaymentPastDueDate) <> month(getdate()) or PaymentPastDueDate is null)     
----and DATEDIFF(DAY,getdate(),lastpmtRecvdDate) <= 0    
----and IsPaymentPastDue = 1   
----and DATEDIFF(DAY,getdate(),lastpmtRecvdDate) >= (PaymentPastDueReminderDay * -1)

	
--	select  record_id,ufirstname + ' ' + ulastname as fullname, uemailid as userEmail,   serviceinfo.userid,  
--	isnull(PIPmt, 0) + isnull(EscrowPmt,0) as TotalPmt,  loanid,convert(varchar, lastpmtRecvdDate, 1) as lastpmtRecvdDate,    
--	DATEDIFF(DAY,getdate(),lastpmtRecvdDate) as pastDueDays,  IsSMSPaymentPastDue,IsEmailPaymentPastDue,  IsPaymentPastDue,
--	PaymentPastDueReminderDay,PaymentPastDueDate ,    
--	mobile_ph, carrierid,
--	(select carrieremailsms from carrier where id = carrierid) as smsgateway     
--	from [WINSERVER].[service].dbo.loan       
--	join tblServiceInfo as serviceInfo on loan.loanid = serviceInfo.loanno      
--	join tblUsers as users on users.userid = serviceinfo.userid   
--	where
--	DATEADD(DAY,CAST(ISNULL(PaymentPastDueReminderDay,0) AS INT),ISNULL(duedate,GETDATE()-CAST(ISNULL(PaymentPastDueReminderDay,0)  AS INT))) = GETDATE()  
--	AND IsPaymentPastDue = 1  
--	and  month(lastPmtRecvdDate) < month(getdate()) and year(lastPmtRecvdDate) = year(getdate())
--	and  DATEADD(DAY,CAST(ISNULL(PaymentPastDueReminderDay,0) AS INT),ISNULL(duedate,GETDATE()-CAST(ISNULL(PaymentPastDueReminderDay,0)  AS INT))) <> PaymentPastDueDate

END






GO

