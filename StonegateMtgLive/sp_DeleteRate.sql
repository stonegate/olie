USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteRate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteRate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsRates.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_DeleteRate]
(

@rateid varchar(Max)
)
AS
BEGIN

delete from tblrates where rateid =@rateid
 
END

GO

