USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_add_jobschedule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_add_jobschedule]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Created By: Amit Kumar
-- Create Date : 04/09/13

-- =============================================
CREATE PROC [dbo].[sp_add_jobschedule]

AS
BEGIN
----------------Start (to move from active to archive message)---------------------------
update tblServiceBanner set Status=3 where expiredate=convert(varchar(10),getdate(),101)
----------------End (to move from active to archive message)---------------------------
declare
@count int,
@ServiceId int,
@Counter int,
@PDate datetime,
@EDate Datetime,

@Status char(1)

CREATE TABLE #TempTable(
TempID int IDENTITY(1,1),
TempServiceId int,
TempStatus char(1),
TempPublishDate datetime,
TempExpireDate Datetime
)

insert into #TempTable(TempServiceId,TempStatus,
TempPublishDate,
TempExpireDate ) select ServiceBannerId,Status,PublishDate,ExpireDate from tblservicebanner where status in(2,3)

set @count=0
select @count=count(TempID) from #TempTable
set @Counter=1

if(@count>0)


begin
while(@count<>0)

begin


select @ServiceId=TempServiceId,@PDate=TempPublishDate,@Status=TempStatus from #TempTable where TempID=@Counter
-----------Start (to move from pending to active message)------------------------------------
if(@Status=2)
begin
if(convert(varchar(10),@PDate,101)=convert(varchar(10),getdate(),101))
begin
update tblservicebanner set Status=1 where ServiceBannerId=@ServiceId
end
end
------------End(to move from pending to active message)------------------------------------
------------Start (to move from archve to expire message)------------------------------------

if(@Status=3)
begin
if(convert(varchar(10),dateadd(month,12,@EDate),101)=convert(varchar(10),getdate(),101))
begin
update tblServiceBanner set Status=0 where ServiceBannerId=@ServiceId
end
end
------------End (to move from archve to expire message)--------------------------------------
set @Counter=@Counter+1
set @count=@count-1
end
end

end


GO

