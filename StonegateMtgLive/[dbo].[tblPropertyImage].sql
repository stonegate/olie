USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPropertyImage]') AND type in (N'U'))
DROP TABLE [dbo].[tblPropertyImage]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPropertyImage](	  [PropImageID] INT NOT NULL IDENTITY(1,1)	, [PropID] INT NULL	, [PropImageName] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [PropDesc] NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [IsPropImageDel] BIT NULL DEFAULT((1))	, CONSTRAINT [PK_tblPropertyImage] PRIMARY KEY ([PropImageID] ASC))USE [HomeLendingExperts]
GO

