USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GetReport4RetailType_office]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GetReport4RetailType_office]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
        
CREATE PROCEDURE [dbo].[SP_GetReport4RetailType_office]         
(            
@RoleID int,           
@office varchar(2000),            
@strID varchar(2000),   
@CurrentStatus varchar(3000),         
@StatusDesc varchar(100),     
@oPuid varchar(100)     
)            
AS            
BEGIN            
            
Declare @SQL nvarchar(max)            
Set @SQL =''            
Set @SQL ='select  ''E3'' as DB,loanapp.casenum as FHANumber,loanapp.ChannelType,loanapp.OriginatorName,Processor.StringValue as ProcessorName,  
   loanapp.currentStatus as LoanStatus, loanapp.CurrPipeStatusDate, CurrDecStatusDate as CurrDecisionstatus,loanapp.DecisionStatus ,  
   loanapp.ID as record_id,'''' as LT_USR_ASSGNDRAWER,LoanNumber as loan_no,CloserName as Closer,borr.firstname as BorrowerFirstName,  
   borr.lastname as BorrowerLastName,loanapp.CurrentStatus as LoanStatus,'''' as LU_FINAL_HUD_STATUS,appStat.StatusDateTime AS SubmittedDate,  
   approvalStat.StatusDateTime as DateApproved,case when TransType is null then '''' when TransTYpe = ''P'' then ''Purchase''  
   when TransTYpe = ''R'' then ''Refinance'' end as TransTYpe,  
   case LoanData.FinancingType WHEN ''F'' THEN ''FHA'' WHEN ''C'' THEN ''CONVENTIONAL'' WHEN ''V'' THEN  ''VA'' End as   
   LoanProgram,loanData.AdjustedNoteAmt as loan_amt,LockRecord.Status as LockStatus,  
   LockRecord.LockExpirationDate,LockRecord.LockDateTime,UnderwriterName as UnderWriter,CASE ISNULL(RTRIM(Broker.Company),'''')  
   WHEN '''' THEN RTRIM(Corres.Company) ELSE RTRIM(Broker.Company) END as BrokerName,Lookups.DisplayString as CashOut,  
   CAST(borr.CompositeCreditScore AS CHAR(5)) as CreditScoreUsed, convert(varchar(35),EstCloseDate,101) AS ScheduleDate ,   
   CASE WHEN (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where DueBy=''Prior to Closing'' and Category !=''LENDER''  
   and a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber)) = 0 then 0  
   WHEN (SELECT count(a.ID) as TotalCleared FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'')   
   and (CurrentState=''CLEARED'' OR CurrentState=''SUBMITTED'')  
   and a.objOwner_ID IN (SELECT id FROM dbo.mwlLoanApp  as loan2  WHERE loan2.LoanNumber=LoanApp.Loannumber)) = 0 THEN 0   
   ELSE (SELECT count(a.ID) as TotalCleared   
   FROM dbo.mwlCondition a where (DueBy=''Prior to Closing'' and Category !=''LENDER'') and (CurrentState=''CLEARED'' OR   
   CurrentState=''SUBMITTED'') and a.objOwner_ID IN   
   (SELECT ID FROM dbo.mwlLoanApp as loan2  WHERE loan2.LoanNumber=LoanApp.LoanNumber)) * 100 /   
   (SELECT count(a.ID) as TotalConditions FROM dbo.mwlCondition a where ( DueBy=''Prior to Closing'' and Category !=''LENDER'') and   
   a.objOwner_ID IN ( SELECT ID FROM dbo.mwlLoanApp as loan1  WHERE loan1.LoanNumber=LoanApp.LoanNumber))END AS PER   
   from mwlLoanApp  loanapp  Inner join mwlBorrower as borr on borr.loanapp_id = loanapp.id  Inner join   
   dbo.mwlloandata as loanData on loanData.ObjOwner_id = loanapp.id    
   left join dbo.mwlUnderwritingSummary as uSummary on uSummary.LoanApp_id = loanapp.id  Left join dbo.mwlInstitution as Broker  
   on Broker.ObjOwner_id = loanapp.id   
   and Broker.InstitutionType = ''BROKER'' and Broker.objownerName=''Broker'' left join dbo.mwlLockRecord as LockRecord on   
   LockRecord.LoanApp_ID = loanapp.id and  LockRecord.LockType=''LOCK'' and  LockRecord.Status<>''CANCELED''   
   Left join mwlAppStatus appStat on loanapp.ID=appStat.LoanApp_id and appStat.StatusDesc='''  + @StatusDesc +'''  
   Left join mwlApprovalStatus approvalStat on loanapp.ID=approvalStat.LoanApp_id and approvalStat.StatusDesc =''Approved''  
   Left outer join mwlLookups Lookups on LoanData.refipurpose=Lookups.BOCode  and ObjectName=''mwlLoanData'' and fieldname=''refipurpose''    
   Left join dbo.mwlInstitution as Corres on Corres.ObjOwner_id = loanapp.id and Corres.InstitutionType = ''Branch'' and   
   Corres.objownerName=''BranchInstitution''   
   left join mwlcustomfield as Processor on Processor.loanapp_id= loanapp.ID and CustFieldDef_id  
   in(select Id from mwsCustFieldDef where CustomFieldName=''Retail Processor'')  
   Left join dbo.mwlInstitution as Branch on Branch.ObjOwner_id = loanapp.id and Branch.InstitutionType = ''Branch''  
   and Branch.objownerName=''BranchInstitution'' where borr.sequencenum=1 and LoanNumber is not null and LoanNumber <> ''''  and '  
  
                if (@RoleID = 16)  
                begin  
                    if (@office != '')  
                       
                          Set @SQL = @SQL + '  Corres.office in ('''+ @office +''') and'   
                       
                end  
                if (@RoleID = 11)  
                begin  
                    if (@oPuid != '')  
                       
                            
                          Set @SQL = @SQL + '  (Originator_id in (select * from dbo.SplitString('''+ @oPuid +''','','')) or loanapp.id in (select * from dbo.SplitString('''+ @strID +''','',''))  or  Branch.office in(select * from dbo.SplitString('''+ @oPuid +''','',''))) and '  
                       
                end  
                   
  
                 Set @SQL = @SQL + ' loanapp.ChannelType like ''%RETAIL%'' and CurrentStatus  IN (select * from dbo.SplitString('''+@CurrentStatus+''','',''))  order by Per desc'  
  
   
Print @SQL  
exec sp_executesql @SQL            
END   


GO

