USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_DeleteDoc]
(
@loanregid varchar(max),
@loandocid varchar(max)
)
AS
BEGIN
update tblLoanReg set filename= NULL where loanregid = @loanregid ; delete from tblloandoc where loandocid=@loandocid

END



GO

