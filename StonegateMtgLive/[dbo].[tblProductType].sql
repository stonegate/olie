USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblProductType]') AND type in (N'U'))
DROP TABLE [dbo].[tblProductType]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblProductType](	  [ID] INT NOT NULL IDENTITY(1,1)	, [producttypeid] INT NULL	, [name] NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [term] INT NULL	, [isarm] INT NULL)USE [HomeLendingExperts]
GO

