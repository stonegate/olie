USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateAlert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UpdateAlert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/05/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsServiceInfo.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_UpdateAlert]
(
 @IsSMSPaymentReminderAlert varchar (max),
 @IsSMSPaymentPostedAlert varchar (max),
 @IsSMSPaymentPastDue varchar (max),
 @IsEmailPaymentReminderAlert varchar (max),
 @IsEmailPaymentPostedAlert  varchar (max),
 @IsEmailPaymentPastDue  varchar (max),
 @IsPaymentReminderAlert  varchar (max),
 @IsPaymentPostedReminderAlert  varchar (max),
 @IsPaymentPastDue varchar (max),
 @PaymentReminderDay varchar (max),
 @PaymentPastDueReminderDay varchar (max),
 @record_id  varchar (max)
)
AS
BEGIN
update tblServiceInfo set 

 IsSMSPaymentReminderAlert=@IsSMSPaymentReminderAlert,
 IsSMSPaymentPostedAlert=@IsSMSPaymentPostedAlert,
 IsSMSPaymentPastDue=@IsSMSPaymentPastDue,
 IsEmailPaymentReminderAlert=@IsEmailPaymentReminderAlert,
 IsEmailPaymentPostedAlert =@IsEmailPaymentPostedAlert,
 IsEmailPaymentPastDue =@IsEmailPaymentPastDue,
 IsPaymentReminderAlert =@IsPaymentReminderAlert,
 IsPaymentPostedReminderAlert =@IsPaymentPostedReminderAlert,
 IsPaymentPastDue=@IsPaymentPastDue,
 PaymentReminderDay=@PaymentReminderDay,
 PaymentPastDueReminderDay=@PaymentPastDueReminderDay
 where record_id =@record_id
     
END


GO

