USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Select_tblGetAuthenticateValue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Select_tblGetAuthenticateValue]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Select_tblGetAuthenticateValue]
(
@strPsCellNumber VARCHAR(Max)
)
AS
BEGIN

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT
	     userid
		,urole
		,UserLoginid
	FROM tblUsers
	WHERE (
			(	uemailid = @strPsCellNumber
				AND UserLoginid IS NULL
			)
			OR UserLoginid = @strPsCellNumber
			)
		AND userid NOT IN (
			SELECT refuserid
			FROM tblTempUser WHERE ISACTIVE = 1
			)
		AND urole NOT IN (6,9) -- Added to block Text My Rate User from log in
END



GO

