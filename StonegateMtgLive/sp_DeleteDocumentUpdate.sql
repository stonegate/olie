USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteDocumentUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteDocumentUpdate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_DeleteDocumentUpdate]
(
@record_id VARCHAR(MAX)
)
AS
BEGIN
update condits  set cond_recvd_date = NULL where record_id = @record_id
END




GO

