USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanDocs]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanDocs]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanDocs](	  [ctID] INT NOT NULL IDENTITY(1,1)	, [loan_no] CHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL DEFAULT(' ')	, [docs_text] TEXT(16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL DEFAULT(' ')	, [FileName] VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [dttime] DATETIME NULL	, [userid] INT NULL	, [Assigned] BIT NULL	, [AssignedBy] BIGINT NULL	, [AssignDate] DATETIME NULL	, [Completed] BIT NULL	, [CompletedBy] BIGINT NULL	, [TimeStamp] DATETIME NULL	, CONSTRAINT [PK_tblLoanDocs] PRIMARY KEY ([ctID] ASC))USE [HomeLendingExperts]
GO

