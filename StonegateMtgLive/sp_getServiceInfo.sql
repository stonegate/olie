USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_getServiceInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_getServiceInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_getServiceInfo]
(
@loanid VARCHAR(MAX)
)
AS
BEGIN
SELECT a.LoanCloseDate, a.MaturityDate, a.OriginalAmt, a.OriginalTerm, a.InterestRate, a.PrincipalBal, a.UnappliedBal, a.LCBal, a.OthFeeBal, a.DueDate, a.NextPmtNum, a.PIPmt, 
a.EscrowPmt,isnull(a.PIPmt, 0) + isnull(a.EscrowPmt,0) as TotalPmt, b.principal,b.grossinterest,b.insurancedisb, 
isnull(c.addressline1,'') as addressline1,isnull(c.addressline2,'') as addressline2,c.city,c.county,c.state,c.zip 
FROM Loan a,yeartodate b,property c where c.loanid = a.loanid and a.loanid = b.loanid and a.loanid = @loanid
END




GO

