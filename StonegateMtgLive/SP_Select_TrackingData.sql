USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Select_TrackingData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Select_TrackingData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_Select_TrackingData](  
@trackid int  
 )                
AS                
BEGIN           
  select tblTracking.id, ufirstname, ulastname, uemailid, Case When urole=0 then 'Admin' when urole=1 then 'Director of Retail' when urole=2 then 'Mortgage Advisor' when urole=3 then 'Branch Manager' when urole=4 then 'Customer' when urole=5 then 'Realtor
' when urole=6 then 'Team' when urole=7 then 'Area Manager' when urole=8 then 'Doc Management' end as Roles,LoginTime, LogoutTime,IPAddress  from tblTracking   
  inner join tblUsers ON tblUsers.UserID = tblTracking.UserID where logintime between dateadd(day, datediff(day, 0 ,getdate())-90, 0) and getdate()  
  order by LoginTime desc, LogoutTime desc  
     
END 

GO

