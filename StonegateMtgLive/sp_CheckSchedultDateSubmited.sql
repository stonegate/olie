USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckSchedultDateSubmited]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckSchedultDateSubmited]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_CheckSchedultDateSubmited]
(
@LoanNo VARCHAR(MAX)
)
AS
BEGIN
select count(LoanNo) as totalrec from tblScheduleClosing where LoanNo =@LoanNo

END

GO

