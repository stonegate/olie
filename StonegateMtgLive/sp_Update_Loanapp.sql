USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Update_Loanapp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Update_Loanapp]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Update_Loanapp]
	-- Add the parameters for the stored procedure here
	
(
@Fname varchar(45),
@Mname varchar(45),
@LName varchar(45),
@Email varchar(200),
@CEmail varchar(200),
@DOBM varchar(10),
@DOBDT int,
@DOBYR int,
@SSN varchar(45),
@Phone varchar(45),
@Suffix varchar(5),
@COFNAME varchar(45),
@COMNAME varchar(45),
@COLNAME varchar(45),
@COEMAIL varchar(200),
@COCEmail varchar(200),
@CODOBM varchar(10),
@CODOBDT int,
@CODOBYR int,
@COSSN varchar(45),
@COPHONE varchar(45),
@STADDR varchar(45),
@CITY varchar(45),
@MState int,
@ZipCode varchar(10),
@RSTATUS varchar(25),
@RentAmt varchar(100),
@LMHOLDER varchar(45),
@STADDR2 varchar(45),
@CITY2 varchar(45),
@STATE2 int,
@COSuffix varchar(45),
@ZipCode2 varchar(10),
@RLengthY2 int,
@RLengthM2 int,
@RSTATUS2 varchar(25),
@RentAmt2 varchar(100),
@LMHOLDER2 varchar(45),
@AEMPL varchar(100),
@AOCCU varchar(100),
@AEMPPHONE varchar(45),
@AGMI varchar(100),
@ALEMY int,
@ALEMM int,
@AEMPL2 varchar(100),
@AOCCU2 varchar(100),
@AEMPPHONE2 varchar(45),
@AGMI2 int,
@ALEMY2 int,
@ALEMM2 int,
@CAEMPL varchar(100),
@CAOCCU varchar(100),
@CAEMPPHONE varchar(45),
@CAGMI varchar(100),
@CALEMY int,
@CALEMM int,
@CAEMPL2 varchar(100),
@CAOCCU2 varchar(100),
@CAEMPPHONE2 varchar(45),
@CAGMI2 varchar(100),
@CALEMY2 int,
@CALEMM2 int,
@OtherComments varchar(1000),
@DigiSig varchar(100),
@SigDate varchar(15),
@IsAckno bit ,
@DateEntered datetime,
@RLengthM int,
@RlengthY int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

Update TBLLOANAPP set 
Fname=@Fname,Mname=@Mname,LName=@LName,Email=@Email,CEmail=@CEmail,DOBM=@DOBM,DOBDT=@DOBDT,DOBYR=@DOBYR,SSN=@SSN,Phone=@Phone,Suffix=@Suffix,COFNAME=@COFNAME,COMNAME=@COMNAME,COLNAME=@COLNAME,COEMAIL=@COEMAIL,COCEmail=@COCEmail,CODOBM=@CODOBM,CODOBDT=@CODOBDT,CODOBYR=@CODOBYR,COSSN=@COSSN,COPHONE=@COPHONE,STADDR=@STADDR,CITY=@CITY,MState=@MState,ZipCode=@ZipCode,RSTATUS=@RSTATUS,RentAmt=@RentAmt,LMHOLDER=@LMHOLDER,STADDR2=@STADDR2,CITY2=@CITY2,STATE2=@STATE2,COSuffix=@COSuffix,ZipCode2=@ZipCode2,RLengthY2=@RLengthY2,RLengthM2=@RLengthM2,RSTATUS2=@RSTATUS2,RentAmt2=@RentAmt2,LMHOLDER2=@LMHOLDER2,AEMPL=@AEMPL,AOCCU=@AOCCU,AEMPPHONE=@AEMPPHONE,AGMI=@AGMI,ALEMY=@ALEMY,ALEMM=@ALEMM,AEMPL2=@AEMPL2,AOCCU2=@AOCCU2,AEMPPHONE2=@AEMPPHONE2,AGMI2=@AGMI2,ALEMY2=@ALEMY2,ALEMM2=@ALEMM2,CAEMPL=@CAEMPL,CAOCCU=@CAOCCU,CAEMPPHONE=@CAEMPPHONE,CAGMI=@CAGMI,CALEMY=@CALEMY,CALEMM=@CALEMM,CAEMPL2=@CAEMPL2,CAOCCU2=@CAOCCU2,CAEMPPHONE2=@CAEMPPHONE2,CAGMI2=@CAGMI2,CALEMY2=@CALEMY2,CALEMM2=@CALEMM2,OtherComments=@OtherComments,DigiSig=@DigiSig,SigDate=@SigDate,IsAckno=@IsAckno,DateEntered=@DateEntered,RLengthM=@RLengthM,RlengthY=@RlengthY
END


GO

