USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_PublishedTestimonial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_PublishedTestimonial]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Get_PublishedTestimonial] 
(
@stringmode varchar(50)
)
AS
BEGIN
select Testimionial  from tblPublicTestimonial where IsPublish = 1
END




GO

