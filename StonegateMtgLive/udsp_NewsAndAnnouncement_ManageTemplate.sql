USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udsp_NewsAndAnnouncement_ManageTemplate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udsp_NewsAndAnnouncement_ManageTemplate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
CREATE procEDURE [dbo].[udsp_NewsAndAnnouncement_ManageTemplate]    --0              
                   
      
      
-- Add the parameters for the stored procedure here                  
                   
      
      
@uRole varchar(2)                  
              
      
      
AS                  
BEGIN      
-- SET NOCOUNT ON added to prevent extra result sets from                  
-- interfering with SELECT statements.                  
SET NOCOUNT ON;      
      
SELECT      
 [ID],      
 [Title],      
 [Body],      
 [IsActive],      
 [IsManageTemplate] AS [IsNewsAnnouncement],      
 [CreatedBy],      
 [CreatedDate],      
 '' AS IPAddress,      
 [Role],      
 [ManageTempVisbleAd] AS [NewsAnnousVisbleAd],      
 [ManageTempVisbleDR] AS [NewsAnnousVisbleDR],      
 [ManageTempVisbleBR] AS [NewsAnnousVisbleBR],      
 [ManageTempVisbleLO] AS [NewsAnnousVisbleLO],      
 [ManageTempVisbleCU] AS [NewsAnnousVisbleCU],      
 [ManageTempVisbleRE] AS [NewsAnnousVisbleRE],      
 [ManageTempVisbleRM] AS [NewsAnnousVisbleRM],      
 [ManageTempVisbleDP] AS [NewsAnnousVisbleDP],      
 [ManageTempVisbleCP] AS [NewsAnnousVisbleCP],      
 [ManageTempVisbleHY] AS [NewsAnnousVisbleHY],      
 [ManageTempVisbleDCFI] AS [NewsAnnousVisbleDCFI],      
 [ManageTempVisibleSCFI] AS [NewsAnnousVisbleSCFI],      
 [ManageTempVisbleUM] AS [NewsAnnousVisbleUM],      
 [ModifiedBy],      
 [ModifiedDate]      
 ,Convert(bit,0) AS NewsAnnousVisbleAM,      
  Convert(bit,0) AS NewsAnnousVisbleDM,      
  Convert(bit,0) AS NewsAnnousVisbleROM,      
  Convert(bit,0) AS NewsAnnousVisbleCO,      
  Convert(bit,0) AS NewsAnnousVisbleCRM,      
  Convert(bit,0) AS NewsAnnousVisbleLP,      
  Convert(bit,0) AS NewsAnnousVisbleCL,      
  Convert(bit,0) AS NewsAnnousVisbleRTL,      
  CC_ID,      
  SiteStatus                  
   ,[Body] as News                  
      ,[IsManageTemplate]              
      ,'M' as Popup       
      
FROM [Tpo_tblManageTemplate]      
WHERE PATINDEX('%,' + @uRole + ',%', ','+ role+',') > 0  and IsActive=1    
--and IsManageTemplate!=0            
ORDER BY id DESC                
END 





GO

