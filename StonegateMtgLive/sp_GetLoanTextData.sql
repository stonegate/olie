USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanTextData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanTextData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetLoanTextData]
(
@loan_no VARCHAR(MAX)
)
AS
BEGIN
select loan_no,docs_text,filename,dttime from tblLoanDocs
where rtrim(loan_no)=@loan_no
END



GO

