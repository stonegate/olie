USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanSuspendedData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanSuspendedData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetLoanSuspendedData]
(
@LoanNo VARCHAR(MAX)
)
AS
BEGIN
select ScheduleID as SchId,LoanNo as Loan_No ,Comments, FeeSheet as filename,convert(varchar(35),SubmitedDate,101) as dttime  from tblScheduleClosing
where IsFromLoanSuspendedDoc =1 and  LoanNo=@LoanNo

END

GO

