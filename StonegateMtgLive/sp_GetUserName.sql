USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetUserName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetUserName]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUserName]
	-- Add the parameters for the stored procedure here
(
	@uid varchar(5) ,
	@username varchar(20)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select UserName=@username from tbluser where uid=@uid
    -- Insert statements for procedure here
	
END


GO

