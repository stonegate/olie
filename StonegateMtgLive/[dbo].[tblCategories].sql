USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCategories]') AND type in (N'U'))
DROP TABLE [dbo].[tblCategories]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCategories](	  [CatID] INT NOT NULL IDENTITY(1,1)	, [CateName] VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT(NULL))USE [HomeLendingExperts]
GO

