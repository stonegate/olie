USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStoneGateLoanApplicationAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStoneGateLoanApplicationAdd]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetStoneGateLoanApplicationAdd]
(
@userid VARCHAR(MAX)
)
AS
BEGIN
SELECT userid, ufirstname, ulastname, uemailid, urole FROM tblUsers where userid =@userid
END



GO

