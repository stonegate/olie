USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InsertLoanRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_InsertLoanRegister]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertLoanRegister]

(
@borrLast varchar(100),
@transType int,
@loanAmt numeric(14,2),
@lockStatus varchar(20),
@loanPgm varchar(20),
@estCloseDate datetime,
@userid int,
@filetype varchar(50),
@filename varchar(100) 
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into tblLoanReg
(borrLast,transType,loanAmt,lockStatus,loanPgm,estCloseDate,userid,filetype,filename) 

Values(@borrLast,@transType,@loanAmt,@lockStatus,@loanPgm,@estCloseDate,@userid,@filetype,@filename)
END


GO

