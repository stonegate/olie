USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStoneGateOfficeData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStoneGateOfficeData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsUserRegistrationE3.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetStoneGateOfficeData]
(
@userid varchar (100)
)
AS
BEGIN
	 select * from tblOffices where userid =@userid order by recordid desc
END

GO

