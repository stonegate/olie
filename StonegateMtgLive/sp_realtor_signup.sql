USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_realtor_signup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_realtor_signup]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_realtor_signup] 
	-- Add the parameters for the stored procedure here
(	
@ufirstname varchar(100), 
@ulastname varchar(100),
@mobile_ph varchar(50),
@urole int,
@isadmin bit,
@isactive bit,
@carrierid VARCHAR(5),
@upassword VARCHAR(50),

@CompName varchar(50),
@address  varchar(200),
@address2  varchar(200),

@zip varchar(50)


)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
insert into tblusers (ufirstname,ulastname,mobile_ph,urole,isadmin,isactive,carrierid,upassword,CompName,address,address2,zip)
values(@ufirstname,@ulastname,@mobile_ph,@urole,@isadmin,@isactive,@carrierid,@upassword,@CompName,@address,@address2,@zip)
END

GO

