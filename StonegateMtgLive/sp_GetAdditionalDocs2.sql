USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAdditionalDocs2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAdditionalDocs2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/04/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetAdditionalDocs2]
(
@ctid VARCHAR(MAX)
)
AS
BEGIN

select * from tblAdditionalUWDoc where  ctid=@ctid
 
END


GO

