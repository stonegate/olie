USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Delete_Managers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Delete_Managers]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Rakesh Kumar Mohanty>
-- Create date: <Create Date,9th May 2014>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Delete_Managers]
	@Userid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	declare @rows_affected_output int 
    -- Insert statements for procedure here
	DELETE FROM [dbo].[tblUserManagers] WHERE USERID = @Userid
	SELECT @rows_affected_output = @@rowcount
	if(@rows_affected_output is null)
	BEGIN
		SELECT @rows_affected_output=0
	END
END



GO

