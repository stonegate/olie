USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetHUDApprovedDoc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetHUDApprovedDoc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetHUDApprovedDoc]
(
@parent_id VARCHAR(MAX)
)
AS
BEGIN
select parent_id from prolend_di..docdata
where parent_id=@parent_id and doc_descriptn1='HUD-1 Approved' and group_id=6
END



GO

