USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Get_PublishedPostings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Get_PublishedPostings]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[sp_Get_PublishedPostings] 
(
@stringmode varchar(50)
)
AS
BEGIN
select CareerId,PositionName,Description from tblCareerPosting where IsPublished = 1
END




GO

