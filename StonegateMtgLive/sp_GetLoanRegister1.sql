USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLoanRegister1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLoanRegister1]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetLoanRegister1]

@userid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select LR.*,LK.Description as TranscationType from tblloanreg LR Left outer join tbllookups LK on LR.TransType=LK.Lookupid where userid=@userid ORDER BY CreatedDate DESC 
END

GO

