USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetStoneGateLoanApplicationData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetStoneGateLoanApplicationData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Abhishek Rastogi
-- Create Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsLoanApplication.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_GetStoneGateLoanApplicationData]

AS
BEGIN
select * from tblLoanApplication order by LoanApplicationID desc

END

GO

