USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DeleteDocument]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DeleteDocument]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Abhishek Rastogi
-- ALTER Date : 10/06/2012
-- Edited By : --
-- Edited Date : --
-- Description: for clsPipeLine.cs BLL
-- =============================================

CREATE PROCEDURE [dbo].[sp_DeleteDocument]
(
@ctid VARCHAR(MAX)
)
AS
BEGIN
delete from tblCondText_History where ctid = @ctid
END




GO

