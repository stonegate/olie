USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SelectGetLoanType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SelectGetLoanType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nitin
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_SelectGetLoanType]
	
AS
BEGIN

	SET NOCOUNT ON;

 select ID, LoanTypeId,LoanType from tblLoanType
END

GO

