USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Update_BUserProfile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Update_BUserProfile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Created By: Chandresh Patel
-- Create Date : 10/04/2012  
-- Description: Procedure for  
-- =============================================  
CREATE PROC [dbo].[SP_Update_BUserProfile](         
@UserID int,            
@UFirstName VARCHAR(50),                  
@ULastName  VARCHAR(50),                    
@UEmailId  VARCHAR(200),         
@experience VARCHAR(100),     
@phonenum nvarchar(20),    
@mobile_ph nvarchar(50),    
@address nvarchar(300),    
@Address2 varchar(100),    
@city nvarchar(100),    
@state nvarchar(10),    
@zip nvarchar(50),    
@fax nvarchar(50),    
@logoname nvarchar(50),    
@FaceBookID nvarchar(50),    
@TwitterID   nvarchar(50),    
@StateBelongTo nvarchar(100)=null,                  
@MyBranches VARCHAR(50),     
@MAID int,        
@LoanOfficer int,    
@bio ntext,    
@AreaOfExpertise ntext,    
@BranchID int,    
@MI varchar(10),    
@ClientTestimonial ntext,    
@RealtorTestimonial ntext,    
@TeamName nvarchar(100),    
@photoname nvarchar(50),    
@RealtorGroupID int,       
@Shorturl varchar(50)=null,    
@lat decimal(18,6),    
@lng decimal(18,6),    
@zlat decimal(18,6),    
@zlng decimal(18,6),    
@compName varchar(100)           
)                  
AS                  
BEGIN              
	update tblusers        
	set ufirstname=@UFirstName,ulastname=@ULastName,uemailid=@UEmailId,    
	experience=@experience,phonenum=@phonenum,mobile_ph=@mobile_ph,address=@address,Address2=@Address2,city=@city,state=@state,    
	zip=@zip,fax=@fax,logoname=@logoname,FaceBookID=@FaceBookID,TwitterID=@TwitterID,StateBelongTo=@StateBelongTo,    
	MyBranches=@MyBranches,MAID=@MAID,LoanOfficer=@LoanOfficer,bio=@bio,AreaOfExpertise=@AreaOfExpertise,    
	BranchID=@BranchID,MI=@MI,ClientTestimonial=@ClientTestimonial,RealtorTestimonial=@RealtorTestimonial,    
	TeamName=@TeamName,photoname=@photoname,RealtorGroupID=@RealtorGroupID,shorturl=@shorturl,lat=@lat,    
	lng=@lng,zlat=@zlat,zlng=@zlng,compName=@compName    
	where userid=@UserID       
           
END 

GO

