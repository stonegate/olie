USE [HomeLendingExperts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLoanApply]') AND type in (N'U'))
DROP TABLE [dbo].[tblLoanApply]
GO
USE [HomeLendingExperts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLoanApply](	  [LoanApplicationID] INT NOT NULL IDENTITY(1,1)	, [LoanReasonID] INT NULL	, [HometypeID] INT NULL	, [PropertyTypeID] INT NULL	, [Producttypeid] INT NULL	, [StateID] INT NULL	, [CreditScoreID] INT NULL	, [AnnulIncomeBorrower] MONEY NULL	, [AssetBorrower] MONEY NULL	, [RetirementAssetsBorrower] MONEY NULL	, [PropertyLocated] BIT NULL	, [HomeOwned] BIT NULL	, [PurchasePrise] MONEY NULL	, [DownPaymentAmt] MONEY NULL	, [PartOfTotalAssets] BIT NULL	, [DownPaymentSourceID] INT NULL	, [HaveRealtor] BIT NULL	, [RealtorContactName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RealtorPhone] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [RealtorEmail] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppFName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppLName] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppStateID] INT NULL	, [AppZip] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppPrimaryPhone] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL	, [AppSecondaryPhone] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [AppEmail] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [ContactTimeID] INT NULL	, [CurrentPropertyValue] MONEY NULL	, [PropertyPurchaseMonth] NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL	, [YearID] INT NULL	, [ExistingPuchasePrice] MONEY NULL	, [CurrentMortageBal] MONEY NULL	, [MonthlyPayment] MONEY NULL	, [SecondMortage] BIT NULL	, [SecondMortageBal] MONEY NULL	, [DateEntered] DATETIME NULL DEFAULT(getdate())	, [ModifiedBy] INT NULL	, [ModifiedDate] DATETIME NULL	, [UserAppearUrl] NVARCHAR(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)USE [HomeLendingExperts]
GO

